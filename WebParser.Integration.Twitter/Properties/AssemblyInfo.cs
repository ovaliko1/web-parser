﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("WebParser.Integration.Twitter")]
[assembly: Guid("dc5a1b5a-cad7-46e8-a4d7-77732e7fa738")]

[assembly: InternalsVisibleTo("WebParser.Configuration")]
[assembly: InternalsVisibleTo("WebParser.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
