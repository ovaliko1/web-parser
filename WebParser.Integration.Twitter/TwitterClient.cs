﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using Tweetinvi;
using Tweetinvi.Exceptions;
using Tweetinvi.Models;
using Tweetinvi.Parameters;
using WebParser.Application.Interfaces.Services.Settings;
using WebParser.Integration.Twitter.Entities;

using WebParser.Common.Extensions;

namespace WebParser.Integration.Twitter
{
    internal class TwitterClient : ITwitterClient
    {
        private const int DelayBetweenArticlePostInSeconds = 120;
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ICommonSettings Settings { get; }

        public TwitterClient(ICommonSettings settings)
        {
            Settings = settings;
        }

        public async Task Export(TwitterArticleToExport[] articles, Action<int, int, string> logProgressAction)
        {
            if (articles.Length == 0)
            {
                return;
            }

            var articleGroups = articles.GroupBy(
                x => x.ArticlesGroupInfo,
                new EqualityComparer<TwitterArticlesGroupInfo>((x, y) => x.ArticleGroupId == y.ArticleGroupId, x => x.ArticleGroupId.GetHashCode()));

            var articlePreviewUrlFormat = Settings.GetViewerArticlePreviewUrlFormat();

            foreach (var articleGroup in articleGroups)
            {
                var parsedArticles = articleGroup.ToArray(x => x);
                int successCount = 0;
                foreach (var article in parsedArticles)
                {
                    try
                    {
                        PostArticle(article, articleGroup.Key, articlePreviewUrlFormat);
                        successCount++;
                    }
                    catch (TwitterException ex)
                    {
                        Log.Error($"Unhandled TwitterException during Twitter Export. {ex.StatusCode} - {ex.TwitterDescription} Infos: {string.Join("; ", ex.TwitterExceptionInfos.ToArray(x => x.Code + " " + x.Label + " " + x.Message))}", ex);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Unhandled exception during Twitter Export", ex);
                    }

                    await Task.Delay(TimeSpan.FromSeconds(DelayBetweenArticlePostInSeconds));
                }

                logProgressAction?.Invoke(articleGroup.Key.SiteId, articleGroup.Key.UserId,
                    $"TWITTER EXPORT: Posted {successCount}/{parsedArticles.Length} articles to Twitter. Article Group Name {articleGroup.Key.ArticleGroupName}, from site {articleGroup.Key.SiteName}");
            }
        }

        private void PostArticle(TwitterArticleToExport article, TwitterArticlesGroupInfo articlesGroupInfo, string articlePreviewUrlFormat)
        {
            var creds = new TwitterCredentials(articlesGroupInfo.TwitterConsumerKey, articlesGroupInfo.TwitterConsumerSecret, articlesGroupInfo.TwitterAccessToken, articlesGroupInfo.TwitterAccessTokenSecret);

            var tweet = Auth.ExecuteOperationWithCredentials(creds, () =>
            {
                var parameters = new PublishTweetOptionalParameters
                {
                    Coordinates = article.ApartmentDetails.Longitude.HasValue && article.ApartmentDetails.Latitude.HasValue ?
                        new Coordinates((double)article.ApartmentDetails.Latitude, (double)article.ApartmentDetails.Longitude) :
                        null,
                    DisplayExactCoordinates = true,
                };

                return Tweet.PublishTweet(GeneratePostBody(article, articlesGroupInfo, articlePreviewUrlFormat), parameters);
            });
        }

        private string GeneratePostBody(TwitterArticleToExport article, TwitterArticlesGroupInfo articlesGroupInfo, string articlePreviewUrlFormat)
        { 
            var apartmentDetails = article.ApartmentDetails;
            var url = string.Format(articlePreviewUrlFormat, apartmentDetails.Id, "twitter", "export_to_social_media", $"{articlesGroupInfo.UserId}_{articlesGroupInfo.ArticleGroupName}");
            var metroString = $"{apartmentDetails.Metro} {apartmentDetails.DistanceToMetroText} {apartmentDetails.DurationToMetroText}";

            var message =
$"Объект: {articlesGroupInfo.ArticleGroupName} / " +
$"Цена: {apartmentDetails.PriceStr} / " +
$"Метро: {metroString} / " +
$"Описание: {apartmentDetails.Description}";

            return message.Cut(105) + " " + url;
        }
    }
}
