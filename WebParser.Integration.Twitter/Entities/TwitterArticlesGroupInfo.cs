﻿namespace WebParser.Integration.Twitter.Entities
{
    public class TwitterArticlesGroupInfo
    {
        public int SiteId { get; set; }

        public int UserId { get; set; }

        public string SiteName { get; set; }

        public int ArticleGroupId { get; set; }

        public string ArticleGroupName { get; set; }

        public string TwitterConsumerKey { get; set; }

        public string TwitterConsumerSecret { get; set; }

        public string TwitterAccessToken { get; set; }

        public string TwitterAccessTokenSecret { get; set; }
    }
}
