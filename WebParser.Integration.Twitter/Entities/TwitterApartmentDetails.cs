﻿namespace WebParser.Integration.Twitter.Entities
{
    public class TwitterApartmentDetails
    {
        public int Id { get; set; }

        public string ObjectName { get; set; }

        public string PriceStr { get; set; }

        public string Metro { get; set; }

        public string DurationToMetroText { get; set; }

        public string DistanceToMetroText { get; set; }

        public decimal? Longitude { get; set; }

        public decimal? Latitude { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }
    }
}