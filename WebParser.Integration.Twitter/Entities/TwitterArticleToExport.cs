﻿namespace WebParser.Integration.Twitter.Entities
{
    public class TwitterArticleToExport
    {
        public TwitterArticlesGroupInfo ArticlesGroupInfo { get; set; }

        public TwitterApartmentDetails ApartmentDetails { get; set; }
    }
}