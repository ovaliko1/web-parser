﻿using System;
using System.Threading.Tasks;
using WebParser.Integration.Twitter.Entities;

namespace WebParser.Integration.Twitter
{
    public interface ITwitterClient
    {
        Task Export(TwitterArticleToExport[] articles, Action<int, int, string> logProgressAction);
    }
}