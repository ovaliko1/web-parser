# Setup Console output encoding #

## Wnd + R -> regedit ##

* Go ot HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Command Processor
* Add/Edit Autorun sting value to chcp 1251

## Wnd + R -> cmd ##

* Click by properties


![2016-02-20 10-21-08 Скриншот экрана.png](https://bitbucket.org/repo/Eqooje/images/3344459704-2016-02-20%2010-21-08%20%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%20%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0.png)

* Select 'Lucida Console' font



![select console font.exe.png](https://bitbucket.org/repo/Eqooje/images/1543788017-select%20console%20font.exe.png)