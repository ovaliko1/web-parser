﻿using System;
using System.Data.Entity.Migrations;
using System.Data.Entity.Migrations.Infrastructure;
using System.Text.RegularExpressions;

namespace WebParser.DataAccess.Migration
{
    internal class Migrator
    {
        public void RunMigration(bool showScript = false)
        {
            WriteLineWhite("Migration started...");

            var configuration = new Configuration();

            var migrator = new DbMigrator(configuration);

            OutputPendingMigrations(migrator);

            var scriptor = new MigratorScriptingDecorator(migrator);
            var script = scriptor.ScriptUpdate(sourceMigration: null, targetMigration: null);
            
            WriteLineWhite("Result migration script:");

            WriteLineGreen(Regex.Replace(script, "N'WebParser\\.DataAccess\\.Migration\\.Configuration',(.*), N'", "N'WebParser.DataAccess.Migration.Configuration', ... , N'"));

            if (!showScript)
            {
                migrator = new DbMigrator(configuration);
                migrator.Update();
            }

            WriteLineWhite("Migration succeeded...");

            if (showScript)
            {
                Console.ReadLine();
            }
        }

        private static void OutputPendingMigrations(DbMigrator migrator)
        {
            WriteLineWhite("List of pending migrations:");
            var pendingMigrationList = migrator.GetPendingMigrations();
            foreach (var pendingMigrationName in pendingMigrationList)
            {
                WriteLineGreen(pendingMigrationName);
            }
        }

        private static void WriteLineWhite(string message)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(message);
        }

        private static void WriteLineGreen(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
        }
    }
}
