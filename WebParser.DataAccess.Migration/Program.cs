﻿using System;
using System.Linq;
using System.Reflection;
using log4net;

namespace WebParser.DataAccess.Migration
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            try
            {
                new Migrator().RunMigration(args.Contains("showScript"));
            }
            catch (Exception ex)
            {
                Log.Error("Exception during run migration.", ex);
                Console.WriteLine($"Megration failed. Exception: {ex.GetType()}, message: {ex.Message}");
            }
        }
    }
}
