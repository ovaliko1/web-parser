﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WebParser.ExportTool.Windows;

namespace WebParser.ExportTool
{
    /// <summary>
    /// Interaction logic for WindowsContainer.xaml
    /// </summary>
    public partial class WindowsContainer : Window
    {
        private WindowsSwitcher WindowsSwitcher { get; set; }

        public WindowsContainer()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }

        public void Navigate(UserControl nextPage)
        {
            Content = nextPage;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            WindowsSwitcher = new WindowsSwitcher(this);

            WindowsSwitcher.SwitchContent(new Login(WindowsSwitcher)
            {
                DataContext = new LoginViewModel(WindowsSwitcher),
            });
            //var config = ConfigHelper.GetConfig(false);
            //if (config == null || config.ConfigState == ConfigState.AuthenticationFailed || config.ConfigState == ConfigState.AppGuidGenerated)
            //{
            //    ViewModelService.SwitchContent(new Login(ViewModelService));
            //}
            //else
            //{
            //    ViewModelService.SwitchContent(new Config(ViewModelService));
            //}
        }
    }
}
