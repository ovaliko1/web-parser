﻿using System.Configuration;

namespace WebParser.ExportTool.Utilities
{
    public static class Settings
    {
        public static string GetSiteUrl()
        {
            return ConfigurationManager.AppSettings["SiteUrl"];
        }

        public static string GetTokenUrl()
        {
            return "api/Token";
        }

        public static string GetAvailableSitesUrl()
        {
            return "api/SiteStructure/GetAvailableSites";
        }

        public static string GetArticleGroupsBySite()
        {
            return "api/SiteStructure/GetArticleGroupsBySite";
        }

        public static string GetParsedArticlesUrl()
        {
            return "api/ParsedContent/GetParsedArticles";
        }
    }
}
