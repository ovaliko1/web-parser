﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebParser.ExportTool.Application;
using WebParser.ExportTool.Application.Export;
using WebParser.ExportTool.Application.Network;
using UserControl = System.Windows.Controls.UserControl;

namespace WebParser.ExportTool.Windows
{
    /// <summary>
    /// Interaction logic for ExportSettings.xaml
    /// </summary>
    public partial class ExportSettings : UserControl
    {
        public ExportSettings()
        {
            InitializeComponent();
        }
        
        private void ClearFolderButton_Click(object sender, RoutedEventArgs e)
        {
            FolderPathLabel.Content = string.Empty;
            ClearFolderButton.Visibility = Visibility.Hidden;
            StartExportButton.IsEnabled = false;
        }
    }
}
