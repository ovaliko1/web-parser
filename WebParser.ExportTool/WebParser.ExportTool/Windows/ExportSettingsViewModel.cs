﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using log4net;
using WebParser.ExportTool.Application.Export;
using WebParser.ExportTool.Application.Network;
using WebParser.ExportTool.Utilities;

namespace WebParser.ExportTool.Windows
{
    public class ExportSettingsViewModel : INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public event PropertyChangedEventHandler PropertyChanged;

        private FolderBrowserDialog FolderBrowserDialog { get; set; }

        private string Token { get; set; }

        public ExportSettingsViewModel(string token, Site[] sitesResult)
        {
            Token = token;

            ExportType = ExportType.Excel;

            FolderBrowserDialog = new FolderBrowserDialog
            {
                RootFolder = Environment.SpecialFolder.MyComputer
            };

            sites = new ObservableCollection<Site>(sitesResult);

            if (sitesResult.Length > 0)
            {
                selectedSite = sitesResult[0];
            }

            exportCommand = new DelegateCommand(x => OnStartExport());
            chooseFolderCommand = new DelegateCommand(x => OnChooseFolder());
        }

        private void OnStartExport()
        {
            var site = selectedSite;
            var exportPath = FolderPath;

            ExecuteExportAsync(site, exportPath);
        }

        private async void ExecuteExportAsync(Site site, string exportPath)
        {
            ExportFailed = false;
            ExportErrorMessage = string.Empty;

            ShowPreloader = Visibility.Visible;
            try
            {
                await ExportService.StartExport(Token, site, exportPath, FromDate, ExportType);
            }
            catch (Exception ex)
            {
                Log.Error(string.Format("Error during export. Site: {0}; Export Type: {1}", site.Link, ExportType), ex);

                ExportFailed = true;
                ExportErrorMessage = ex.Message;
            }

            ShowPreloader = Visibility.Hidden;
        }

        private void OnChooseFolder()
        {
            FolderBrowserDialog.Description = Properties.Resources.ChooseExportFolder;
            var result = FolderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                FolderPath = FolderBrowserDialog.SelectedPath;
            }
        }

        private readonly ICommand exportCommand;
        public ICommand ExportCommand
        {
            get { return exportCommand; }
        }

        private readonly ICommand chooseFolderCommand;
        public ICommand ChooseFolderCommand
        {
            get { return chooseFolderCommand; }
        }

        private string folderPath;
        public string FolderPath
        {
            get { return folderPath; }
            set
            {
                if (folderPath == value)
                {
                    return;
                }
                folderPath = value;

                if (!string.IsNullOrEmpty(folderPath))
                {
                    ShowExportButton = true;
                }

                OnPropertyChanged();
            }
        }

        private ExportType ExportType { get; set; }

        public bool ExcelChecked
        {
            get { return ExportType == ExportType.Excel; }
            set
            {
                if (value)
                {
                    ExportType = ExportType.Excel;
                    XmlChecked = false;
                    CsvChecked = false;
                }
                OnPropertyChanged();
            }
        }

        public bool XmlChecked
        {
            get { return ExportType == ExportType.Xml; }
            set
            {
                if (value)
                {
                    ExportType = ExportType.Xml;
                    CsvChecked = false;
                    ExcelChecked = false;
                }
                OnPropertyChanged();
            }
        }

        public bool CsvChecked
        {
            get { return ExportType == ExportType.Csv; }
            set
            {
                if (value)
                {
                    ExportType = ExportType.Csv;
                    XmlChecked = false;
                    ExcelChecked = false;
                }
                
                OnPropertyChanged();
            }
        }

        private ObservableCollection<Site>  sites;
        public ObservableCollection<Site> Sites
        {
            get { return sites; }
            set
            {
                if (sites == value)
                {
                    return;
                }
                sites = value;
                OnPropertyChanged();
            }
        }

        private Site selectedSite;
        public Site SelectedSite
        {
            get { return selectedSite; }
            set
            {
                if (selectedSite == value)
                {
                    return;
                }
                selectedSite = value;
                OnPropertyChanged();
            }
        }

        private bool showExportButton;
        public bool ShowExportButton
        {
            get { return showExportButton; }
            set
            {
                if (showExportButton == value)
                {
                    return;
                }
                showExportButton = value;
                OnPropertyChanged();
            }
        }

        private bool exportFailed;
        public bool ExportFailed
        {
            get { return exportFailed; }
            set
            {
                if (exportFailed == value)
                {
                    return;
                }
                exportFailed = value;
                OnPropertyChanged();
            }
        }

        private string exportErrorMessage;
        public string ExportErrorMessage
        {
            get { return exportErrorMessage; }
            set
            {
                if (exportErrorMessage == value)
                {
                    return;
                }
                exportErrorMessage = value;
                OnPropertyChanged();
            }
        }

        private DateTime? fromDate;
        public DateTime? FromDate
        {
            get { return fromDate; }
            set
            {
                if (fromDate == value)
                {
                    return;
                }
                fromDate = value;
                OnPropertyChanged();
            }
        }

        private Visibility showPreloader = Visibility.Hidden;
        public Visibility ShowPreloader
        {
            get { return showPreloader; }
            set
            {
                if (showPreloader == value)
                {
                    return;
                }
                showPreloader = value;

                OnPropertyChanged();
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
