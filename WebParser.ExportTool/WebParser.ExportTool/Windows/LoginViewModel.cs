﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WebParser.ExportTool.Application.Network;
using WebParser.ExportTool.Utilities;

namespace WebParser.ExportTool.Windows
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WindowsSwitcher WindowsSwitcher { get; set; }

        public LoginViewModel(WindowsSwitcher windowsSwitcher)
        {
            WindowsSwitcher = windowsSwitcher;

            LoginError = Visibility.Hidden;
            PasswordError = Visibility.Hidden;
            LoginFaled = Visibility.Hidden;

            loginCommand = new DelegateCommand(x => OnTryLogin());
        }

        private void OnTryLogin()
        {
            ShowFieldsErrors();
            if (string.IsNullOrEmpty(Login) && string.IsNullOrEmpty(Password))
            {
                return;
            }

            ExecuteLoginAsync();
        }

        private async void ExecuteLoginAsync()
        {
            ShowPreloader = Visibility.Visible;

            var loginResult = await WebClient.GetToken(Login, Password);

            ShowPreloader = Visibility.Hidden;
            if (loginResult.HasErrors)
            {
                LoginError = Visibility.Visible;
                PasswordError = Visibility.Visible;
                LoginFaled = Visibility.Visible;
                LoginErrorMessage = loginResult.Errors;
                return;
            }

            var sites = await WebClient.GetSites(loginResult.Result.AccessToken);
            WindowsSwitcher.SwitchContent(new ExportSettings
            {
                DataContext = new ExportSettingsViewModel(loginResult.Result.AccessToken, sites),
            });
        }

        private void ShowFieldsErrors()
        {
            LoginError = string.IsNullOrEmpty(Login) ? Visibility.Visible : Visibility.Hidden;
            PasswordError = string.IsNullOrEmpty(Password) ? Visibility.Visible : Visibility.Hidden;
        }

        private readonly ICommand loginCommand;
        public ICommand LoginCommand
        {
            get { return loginCommand; }
        }

        private string login;
        public string Login
        {
            get { return login; }
            set
            {
                if (login == value)
                {
                    return;
                }
                login = value;

                OnPropertyChanged();
            }
        }


        private Visibility loginError;
        public Visibility LoginError
        {
            get { return loginError; }
            set
            {
                if (loginError == value)
                {
                    return;
                }
                loginError = value;

                OnPropertyChanged();
            }
        }

        private Visibility passwordError;
        public Visibility PasswordError
        {
            get { return passwordError; }
            set
            {
                if (passwordError == value)
                {
                    return;
                }
                passwordError = value;

                OnPropertyChanged();
            }
        }

        private Visibility loginFaled;
        public Visibility LoginFaled
        {
            get { return loginFaled; }
            set
            {
                if (loginFaled == value)
                {
                    return;
                }
                loginFaled = value;

                OnPropertyChanged();
            }
        }

        private string loginErrorMessage;
        public string LoginErrorMessage
        {
            get { return loginErrorMessage; }
            set
            {
                if (loginErrorMessage == value)
                {
                    return;
                }
                loginErrorMessage = value;

                OnPropertyChanged();
            }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                if (password == value)
                {
                    return;
                }
                password = value;

                OnPropertyChanged();
            }
        }

        private Visibility showPreloader = Visibility.Hidden;
        public Visibility ShowPreloader
        {
            get { return showPreloader; }
            set
            {
                if (showPreloader == value)
                {
                    return;
                }
                showPreloader = value;

                OnPropertyChanged();
            }
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
