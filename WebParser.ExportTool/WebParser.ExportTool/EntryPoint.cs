﻿using System;
using System.Threading;
using System.Windows;

namespace WebParser.ExportTool
{
    public class EntryPoint
    {
        private static string appGuid = "F273A31B-5AEB-4EB9-A614-C1A7EA29AEA9";

        [STAThread]
        public static void Main(string[] args)
        {
            using (Mutex mutex = new Mutex(false, "Global\\" + appGuid))
            {
                if (!mutex.WaitOne(0, false))
                {
                    MessageBox.Show(Properties.Resources.ApplicationAlreadyRunned);
                    return;
                }
                var app = new App();
                var window = new WindowsContainer();
                app.Run(window);
            }
        }
    }
}
