﻿namespace WebParser.ExportTool.Application.Network
{
    public class RequestResult<T>
    {
        private RequestResult() {} 

        public static RequestResult<T> Success(T value)
        {
            return new RequestResult<T>
            {
                Result = value,
            };
        }

        public static RequestResult<T> Error(string errorMessage)
        {
            return new RequestResult<T>
            {
                HasErrors = true,
                Errors = errorMessage,
            };
        }

        public T Result { get; private set; }

        public bool HasErrors { get; private set; }

        public string Errors { get; private set; }
    }
}
