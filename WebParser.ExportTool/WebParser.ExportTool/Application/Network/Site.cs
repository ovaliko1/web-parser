﻿namespace WebParser.ExportTool.Application.Network
{
    public class Site
    {
        public string Id { get; set; }

        public string Link { get; set; }

        public string Name { get; set; }

        public bool IsEnabled { get; set; }

        public int? SchedulerStartHours { get; set; }

        public int? SchedulerStartMinutes { get; set; }
    }
}
