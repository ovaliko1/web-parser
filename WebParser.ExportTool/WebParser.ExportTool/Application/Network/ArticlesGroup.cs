﻿namespace WebParser.ExportTool.Application.Network
{
    public class ArticlesGroup
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
