﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using WebParser.ExportTool.Utilities;

namespace WebParser.ExportTool.Application.Network
{
    public static class WebClient
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static async Task<RequestResult<Token>> GetToken(string login, string password)
        {
            var response = await MakeRequest<Token>(null, Settings.GetTokenUrl(), Method.POST,
                request =>
                {
                    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    request.AddParameter("grant_type", "password");
                    request.AddParameter("username", login);
                    request.AddParameter("password", password);
                });
            return response;
        }

        public static async Task<Site[]> GetSites(string token)
        {
            var response = await MakeRequest<List<Site>>(token, Settings.GetAvailableSitesUrl(), Method.GET);
            return response.Result.ToArray();
        }

        public static async Task<ArticlesGroup[]> GetArticleGroupsBySite(string token, string siteId)
        {
            var response = await MakeRequest<List<ArticlesGroup>>(token, Settings.GetArticleGroupsBySite(), Method.GET,
                request =>
                {
                    request.AddQueryParameter("siteId", siteId);
                });
            return response.Result.ToArray();
        }

        private class ParsedArticleJson
        {
            public int Id { get; set; }

            public int? SiteId { get; set; }

            public int? ArticleGroupId { get; set; }

            public string ParsedDate { get; set; }

            public string Link { get; set; }

            public Dictionary<string, ParsedValueResultJson> Members { get; set; }
        }

        private class ParsedValueResultJson
        {
            public string Value { get; set; }

            public string ValueType { get; set; }
        }

        public static async Task<PagingResult<ParsedArticle>> GetParsedArticles(string token, string articleGroupId, int page, int pageSize, DateTime? from)
        {
            var response = await MakeRequest<PagingResult<ParsedArticleJson>>(token, Settings.GetParsedArticlesUrl(), Method.GET,
                request =>
                {
                    request.AddQueryParameter("articleGroupId", articleGroupId);
                    request.AddQueryParameter("page", page.ToString());
                    request.AddQueryParameter("pageSize", pageSize.ToString());
                    request.AddQueryParameter("from", from?.ToString("dd-MM-yyyy", CultureInfo.InvariantCulture));
                });

            if (response.HasErrors)
            {
                return new PagingResult<ParsedArticle>
                {
                    TotalCount = 0,
                    Items = new List<ParsedArticle>(0),
                };
            }

            var result = new PagingResult<ParsedArticle>
            {
                TotalCount = response.Result.TotalCount,
                Items = new List<ParsedArticle>(response.Result.Items
                .Select(x => new ParsedArticle
                {
                    Id = x.Id,
                    SiteId = x.SiteId,
                    ArticleGroupId = x.ArticleGroupId,
                    Link = x.Link,
                    ParsedDate = x.ParsedDate,
                    Members = x.Members.Select(y => new ParsedValueResult
                    {
                       Name = y.Key,
                       ValueType = y.Value?.ValueType,
                       Values = JsonConvert.DeserializeObject<string[]>(y.Value?.Value),
                    }).ToArray(),
                })),
            };

            return result;
        }

        private static async Task<RequestResult<T>> MakeRequest<T>(string token, string apiPath, Method method) where T : class, new()
        {
            return await MakeRequest<T>(token, apiPath, method, null);
        }

        private static async Task<RequestResult<T>> MakeRequest<T>(string token, string apiPath, Method method, Action<IRestRequest> customizeRequest) where T : class, new()
        {
            var request = new RestRequest
            {
                Resource = apiPath,
                Method = method,
            };

            if (customizeRequest != null)
            {
                customizeRequest(request);
            }

            if (!string.IsNullOrEmpty(token))
            {
                request.AddHeader("Authorization", string.Format("Bearer {0}", token));
            }

            var client = new RestClient(Settings.GetSiteUrl());

            var response = await client.ExecuteTaskAsync<T>(request);

            if (response.StatusCode != HttpStatusCode.OK || response.ErrorException != null)
            {
                var errorMessage = response.ErrorException != null ? response.ErrorException.Message : string.Format("Somethink went wrong during request. Status Code: {0}", response.StatusCode);
                Log.Error(string.Format("Request error. Url: {0}; Message: {1}", apiPath, errorMessage), response.ErrorException);

                return RequestResult<T>.Error(errorMessage);
            }

            return RequestResult<T>.Success(response.Data);
        }
    }
}
