﻿namespace WebParser.ExportTool.Application.Network
{
    public class ParsedValueResult
    {
        public string Name { get; set; }

        public string[] Values { get; set; }

        public string ValueType { get; set; }
    }
}