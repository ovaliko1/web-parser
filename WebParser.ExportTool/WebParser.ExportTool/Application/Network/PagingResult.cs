﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebParser.ExportTool.Application.Network
{
    public class PagingResult<T>
    {
        public List<T> Items { get; set; }

        public int TotalCount { get; set; }
    }
}
