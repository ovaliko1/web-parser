﻿using RestSharp.Deserializers;

namespace WebParser.ExportTool.Application.Network
{
    public class Token
    {
        [DeserializeAs(Name = "access_token")]
        public string AccessToken { get; set; }

        [DeserializeAs(Name = "token_type")]
        public string TokenType { get; set; }

        [DeserializeAs(Name = "expires_in")]
        public string ExpiresIn { get; set; }

        [DeserializeAs(Name = "userName")]
        public string UserName { get; set; }

        [DeserializeAs(Name = ".issued")]
        public string Issued { get; set; }

        [DeserializeAs(Name = ".expires")]
        public string Expires { get; set; }
    }
}
