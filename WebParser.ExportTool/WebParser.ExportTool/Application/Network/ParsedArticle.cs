﻿using System.Collections.Generic;

namespace WebParser.ExportTool.Application.Network
{
    public class ParsedArticle
    {
        public int Id { get; set; }

        public int? SiteId { get; set; }

        public int? ArticleGroupId { get; set; }

        public string ParsedDate { get; set; }

        public string Link { get; set; }

        public ParsedValueResult[] Members { get; set; }
    }
}