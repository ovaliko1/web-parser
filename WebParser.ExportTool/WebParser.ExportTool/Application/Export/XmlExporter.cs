﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Xml;
using WebParser.ExportTool.Application.Network;

namespace WebParser.ExportTool.Application.Export
{
    internal class XmlExporter : Exporter
    {
        protected override async Task Export(string fileName, string token, DateTime? fromDate, Site site, ArticlesGroup articleGroup)
        {
            fileName = string.Format("{0}.xml", fileName);

            XmlWriterSettings setting = new XmlWriterSettings();
            setting.ConformanceLevel = ConformanceLevel.Auto;

            using (XmlWriter writer = XmlWriter.Create(fileName, setting))
            {
                writer.WriteProcessingInstruction("xml", "version='1.0' encoding='utf-8'");

                writer.WriteStartElement("Items");

                int page = -1;
                int pageSize = 10;
                int? totalCount = null;

                do
                {
                    page++;

                    var parsedArticles = await WebClient.GetParsedArticles(token, articleGroup.Id, page, pageSize, fromDate);
                    await Task.Delay(20);

                    foreach (var article in parsedArticles.Items)
                    {
                        writer.WriteStartElement("Item");

                        writer.WriteElementString("Id", article.Id.ToString(CultureInfo.InvariantCulture));
                        writer.WriteElementString("ParsedDate", article.ParsedDate);
                        writer.WriteElementString("Url", article.Link);

                        writer.WriteStartElement("Members");
                        foreach (var member in article.Members)
                        {
                            writer.WriteStartElement("Member");
                            writer.WriteAttributeString("Name", member.Name);

                            writer.WriteString(string.Join("; ", member.Values).RemoveInvalidXmlChars());

                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement(); 

                        writer.WriteEndElement(); 
                    }
                    
                    totalCount = totalCount ?? parsedArticles.TotalCount;
                } while ((page * pageSize + pageSize) <= totalCount);

                writer.WriteEndElement();
                writer.Flush();
            }
        }
    }
}
