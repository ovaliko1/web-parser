﻿using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using WebParser.ExportTool.Application.Network;

namespace WebParser.ExportTool.Application.Export
{
    public abstract class Exporter
    {
        public static Exporter Create(ExportType exportType)
        {
            switch (exportType)
            {
                case ExportType.Xml:
                    return new XmlExporter();

                case ExportType.Excel:
                    return new ExcelExporter();

                case ExportType.Csv:
                    return new CsvExporter();

                default:
                    throw new ArgumentOutOfRangeException("exportType", exportType, null);
            }
        }

        public async Task ExportParsedArticles(string exportFolder, string token, DateTime? fromDate, Site site, ArticlesGroup articlesGroup)
        {
            var fileName = GetFileName(exportFolder, site.Name, articlesGroup.Name);
            await Export(fileName, token, fromDate, site, articlesGroup);
        }

        protected abstract Task Export(string fileName, string token, DateTime? fromDate, Site site, ArticlesGroup articleGroup);

        private string GetFileName(string exportFolder, string siteName, string articleGroupName)
        {
            var now = DateTime.Now;

            var fileName = string.Format("{0}_{1}_{2}", siteName, articleGroupName, now.ToString("yy-MM-dd-HH-mm", CultureInfo.InvariantCulture)).Replace("/", "_");
            fileName = Path.Combine(exportFolder, fileName);

            return fileName;
        }
    }
}
