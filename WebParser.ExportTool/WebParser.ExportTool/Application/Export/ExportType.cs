﻿namespace WebParser.ExportTool.Application.Export
{
    public enum ExportType
    {
        Xml,
        Excel,
        Csv
    }
}
