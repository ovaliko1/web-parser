﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using WebParser.ExportTool.Application.Network;

namespace WebParser.ExportTool.Application.Export
{
    public class CsvExporter : Exporter
    {
        protected override async Task Export(string fileName, string token, DateTime? fromDate, Site site, ArticlesGroup articleGroup)
        {
            fileName = string.Format("{0}.csv", fileName);

            using (StreamWriter writer = File.CreateText(fileName))
            using (var csvWriter = new CsvWriter(writer, new CsvConfiguration
            {
                Encoding = Encoding.UTF8,
            }))
            {
                int page = -1;
                int pageSize = 10;
                int totalCount;

                do
                {
                    page++;

                    var parsedArticles = await WebClient.GetParsedArticles(token, articleGroup.Id, page, pageSize, fromDate);
                    var items = parsedArticles.Items;
                    if (items.Count == 0)
                    {
                        break;
                    }

                    if (page == 0)
                    {
                        var headerRow = items[0].GetHeaderCellValues();
                        csvWriter.WriteRow(headerRow);
                    }

                    foreach (var item in items)
                    {
                        var row = item.GetRowValues();
                        csvWriter.WriteRow(row);
                    }

                    totalCount = parsedArticles.TotalCount;
                } while ((page * pageSize + pageSize) <= totalCount);

            }
        }
    }
}
