﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

using WebParser.ExportTool.Application.Network;

namespace WebParser.ExportTool.Application.Export
{
    internal class ExcelExporter : Exporter
    {
        protected override async Task Export(string fileName, string token, DateTime? fromDate, Site site, ArticlesGroup articleGroup)
        {
            fileName = string.Format("{0}.xlsx", fileName);

            using (var document = SpreadsheetDocument.Create(fileName, SpreadsheetDocumentType.Workbook))
            {
                document.AddWorkbookPart();
                document.WorkbookPart.Workbook = new Workbook
                {
                    Sheets = new Sheets(),
                };

                var sheetPart = document.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheet = new Sheet
                {
                    Id = document.WorkbookPart.GetIdOfPart(sheetPart),
                    SheetId = 1,
                    Name = "Data"
                };

                document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Append(sheet);

                var sheetData = new SheetData();
                sheetPart.Worksheet = new Worksheet(sheetData);

                document.Close();
            }

            int page = -1;
            int pageSize = 10;
            int totalCount;

            do
            {
                page++;

                var parsedArticles = await WebClient.GetParsedArticles(token, articleGroup.Id, page, pageSize, fromDate);
                var items = parsedArticles.Items;
                if (items.Count == 0)
                {
                    break;
                }

                using (var spreadsheetDocument = SpreadsheetDocument.Open(fileName, true))
                {
                    var sheetPart = spreadsheetDocument.WorkbookPart.WorksheetParts.First();

                    var sheetData = sheetPart.Worksheet.GetFirstChild<SheetData>();

                    if (page == 0)
                    {
                        var headerRow = items[0].GetHeaderRow();
                        sheetData.AppendChild(headerRow);
                    }

                    foreach (var item in items)
                    {
                        var row = item.GetRow();
                        sheetData.AppendChild(row);
                    }

                    totalCount = parsedArticles.TotalCount;
                }

            } while ((page * pageSize + pageSize) <= totalCount);
        }
    }
}
