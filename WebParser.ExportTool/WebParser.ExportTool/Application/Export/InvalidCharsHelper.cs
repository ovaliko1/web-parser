﻿using System.Text.RegularExpressions;

namespace WebParser.ExportTool.Application.Export
{
    internal static class InvalidCharsHelper
    {
        private static readonly Regex InvalidXmlChars = new Regex(@"(?<![\uD800-\uDBFF])[\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F\uFEFF\uFFFE\uFFFF]", RegexOptions.Compiled);

        public static string RemoveInvalidXmlChars(this string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }
            return InvalidXmlChars.Replace(text, string.Empty);
        }
    }
}
