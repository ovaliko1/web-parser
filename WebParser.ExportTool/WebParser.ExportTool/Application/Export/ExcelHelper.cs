﻿using System.Globalization;
using System.Linq;
using DocumentFormat.OpenXml.Spreadsheet;
using WebParser.ExportTool.Application.Network;

namespace WebParser.ExportTool.Application.Export
{
    internal static class ExcelHelper
    {
        public static Row GetHeaderRow(this ParsedArticle article)
        {
            var headerCellValues = new []
                {
                  "Id",
                  "Parsed Date",   
                }
                .Concat(article
                    .Members
                    .Select(x => x.Name)
                    .Concat(new [] { "Url" }))
                .ToArray();

            var result = GetRow(headerCellValues);
            return result;
        }

        public static Row GetRow(this ParsedArticle article)
        {
            var cellValues = new[]
                {
                  article.Id.ToString(CultureInfo.InvariantCulture),
                  article.ParsedDate,
                }.Concat(article
                    .Members
                    .Select(x => string.Join("; ", x.Values))
                    .Concat(new[] { article.Link }))
                .ToArray();

            var result = GetRow(cellValues);
            return result;
        }

        private static Row GetRow(params string[] cellsValues)
        {
            var result = new Row { Collapsed = false, };

            var cells = cellsValues.Select(x => new Cell
            {
                CellValue = new CellValue(x.RemoveInvalidXmlChars()),
                DataType = CellValues.String,
            });

            foreach (var cell in cells)
            {
                result.AppendChild(cell);
            }

            return result;
        }
    }
}
