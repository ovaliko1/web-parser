﻿using System.Linq;
using CsvHelper;
using WebParser.ExportTool.Application.Network;

namespace WebParser.ExportTool.Application.Export
{
    internal static class CsvHelper
    {
        public static string[] GetHeaderCellValues(this ParsedArticle article)
        {
            var headerCellValues = article
                .Members
                .Select(x => x.Name)
                .Concat(new [] { "Url" })
                .ToArray();

            return headerCellValues;
        }

        public static string[] GetRowValues(this ParsedArticle article)
        {
            var cellValues = article
                .Members
                .Select(x => string.Join("; ", x.Values))
                .Concat(new[] { article.Link })
                .ToArray();

            return cellValues;
        }

        public static void WriteRow(this CsvWriter writer, string[] rowValues)
        {
            foreach (var rowValue in rowValues)
            {
                writer.WriteField(rowValue);
            }
            writer.NextRecord();
        }
    }
}
