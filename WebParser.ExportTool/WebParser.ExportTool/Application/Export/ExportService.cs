﻿using System;
using System.Threading.Tasks;
using WebParser.ExportTool.Application.Network;

namespace WebParser.ExportTool.Application.Export
{
    public static class ExportService
    {
        public static async Task StartExport(string token, Site site, string path, DateTime? fromDate, ExportType exportType)
        {
            var exporter = Exporter.Create(exportType);

            var articleGroups = await WebClient.GetArticleGroupsBySite(token, site.Id);
            foreach (var articleGroup in articleGroups)
            {
                await exporter.ExportParsedArticles(path, token, fromDate, site, articleGroup);
            }
        }
    }
}