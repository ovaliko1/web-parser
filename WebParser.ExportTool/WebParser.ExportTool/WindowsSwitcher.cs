﻿using System.Windows.Controls;

namespace WebParser.ExportTool
{
    public class WindowsSwitcher
    {
        private WindowsContainer WindowsContainer { get; set; }

        public WindowsSwitcher(WindowsContainer windowsContainer)
        {
            WindowsContainer = windowsContainer;
        }

        public void SwitchContent(UserControl newPage)
        {
            WindowsContainer.Navigate(newPage);
        }
    }
}
