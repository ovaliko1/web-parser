﻿namespace WebParser.WebApi.Results
{
    /// <summary>
    /// </summary>
    public class SiteResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SchedulerStartHours { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int? SchedulerStartMinutes { get; set; }
    }
}