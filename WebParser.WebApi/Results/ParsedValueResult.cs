﻿namespace WebParser.WebApi.Results
{
    /// <summary>
    /// 
    /// </summary>
    public class ParsedValueResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ValueType { get; set; }
    }
}
