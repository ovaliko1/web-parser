﻿using System.Collections.Generic;

namespace WebParser.WebApi.Results
{
    /// <summary>
    /// 
    /// </summary>
    public class ParsedArticleResult
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ArticleGroupId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParsedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, ParsedValueResult> Members { get; set; }
    }
}
