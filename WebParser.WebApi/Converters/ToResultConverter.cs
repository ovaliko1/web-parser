﻿using System;
using System.Globalization;
using System.Linq;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Common.Extensions;
using WebParser.WebApi.Results;

namespace WebParser.WebApi.Converters
{
    // TODO: Rewrite To Automapper
    internal static class ToResultConverter
    {
        public static SiteResult ToSiteResult(this Site entity)
        {
            return new SiteResult
            {
                Id = entity.Id.ToString(CultureInfo.InvariantCulture),
                Link = entity.Link,
                Name = entity.Name,
                IsEnabled = entity.IsEnabled,
                SchedulerStartHours = entity.SchedulerStartHours,
                SchedulerStartMinutes = entity.SchedulerStartMinutes,
            };
        }

        public static AdminSiteResult ToAdminSiteResult(this AdminSite entity)
        {
            return new AdminSiteResult
            {
                Id = entity.Id.ToString(CultureInfo.InvariantCulture),
                Link = entity.Link,
                Name = entity.Name,
                UserEmail = entity.UserEmail,
                IsEnabled = entity.IsEnabled,
                SchedulerStartHours = entity.SchedulerStartHours,
                SchedulerStartMinutes = entity.SchedulerStartMinutes,
            };
        }

        public static ArticlesGroupResult ToArticlesGroupResult(this ArticlesGroup entity)
        {
            return new ArticlesGroupResult
            {
                Id = entity.Id.ToString(CultureInfo.InvariantCulture),
                Name = entity.InternalName,
            };
        }

        public static PagingResult<TResultModel> ToPagingResult<TEntity, TResultModel>(this Application.Interfaces.DomainEntities.PagingResult<TEntity> servicePagingResult, Func<TEntity, TResultModel> convert)
        {
            return new PagingResult<TResultModel>
            {
                Items = servicePagingResult.Items.ToArray(convert),
                TotalCount = servicePagingResult.TotalCount,
            };
        }
    }
}
