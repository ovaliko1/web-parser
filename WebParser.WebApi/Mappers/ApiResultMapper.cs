﻿using System.Linq;
using AutoMapper;
using Newtonsoft.Json;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.Publishing.Entities;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.WebApi.Models;
using WebParser.WebApi.Results;
using ParsedArticleResult = WebParser.WebApi.Results.ParsedArticleResult;

namespace WebParser.WebApi.Mappers
{
    internal static class ApiResultMapper
    {
        public const string DateTimeFormat = "dd-MMMM-yyyy HH:mm";

        public const string DateFormat = "dd-MM-yyyy";

        static ApiResultMapper()
        {
            Mapper.CreateMap<ParsedValue, ParsedValueResult>()
                  .ForMember(x => x.Value, opt => opt.MapFrom(x => JsonConvert.SerializeObject(x.FormatValues(DateTimeFormat))))
                  .ForMember(x => x.ValueType, opt => opt.MapFrom(x => x.ParsedValueType.GetName()));

            Mapper.CreateMap<ApiParsedArticle, ParsedArticleResult>()
                  .ForMember(x => x.ParsedDate, opt => opt.MapFrom(x => x.ParsedDate.ToStringInvariant(DateTimeFormat)))
                  .ForMember(x => x.Members, opt => opt.MapFrom(x => x.ParsedValues.ToDictionary(y => y.Name, y => y.Map<ParsedValue, ParsedValueResult>())));

            Mapper.CreateMap<ArticleModel, PublisherArticle>();
        }

        public static TMap Map<TSource, TMap>(this TSource source)
        {
            return Mapper.Map<TSource, TMap>(source);
        }
    }
}
