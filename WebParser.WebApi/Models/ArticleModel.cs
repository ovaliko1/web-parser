﻿using System.ComponentModel.DataAnnotations;

namespace WebParser.WebApi.Models
{
    /// <summary>
    /// Ad model
    /// </summary>
    public class ArticleModel
    {
        /// <summary>
        /// User Name who has created ad
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Ad description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Vk link in format https://vk.com/idXXXX
        /// </summary>
        [Required]
        public string Identity { get; set; }

        /// <summary>
        /// Metro station Name
        /// </summary>
        public string MetroStation { get; set; }

        /// <summary>
        /// URLs to images
        /// </summary>
        public string[] Images { get; set; }
    }
}