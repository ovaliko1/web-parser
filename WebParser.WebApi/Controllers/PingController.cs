﻿using System.Web.Http;
using System.Web.Http.Description;

namespace WebParser.WebApi.Controllers
{
    /// <summary>
    /// Ping end point
    /// </summary>
    public class PingController : ApiController
    {
        /// <summary>
        /// Ping
        /// </summary>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        public IHttpActionResult Ping()
        {
            return Ok();
        }

        /// <summary>
        /// Check Authorization
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Publisher")]
        [HttpGet]
        public IHttpActionResult AuthorizedPing()
        {
            return Ok();
        }
    }
}
