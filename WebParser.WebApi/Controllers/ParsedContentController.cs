﻿using System.Web.Http;
using System.Web.Http.Description;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Common.Extensions;
using WebParser.WebApi.Converters;
using WebParser.WebApi.Mappers;
using WebParser.WebApi.Results;

namespace WebParser.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [Authorize(Roles = "User")]
    public class ParsedContentController : ApiController
    {
        private IParsedContentService ParsedContentService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parsedContentService"></param>
        public ParsedContentController(IParsedContentService parsedContentService)
        {
            ParsedContentService = parsedContentService;
        }

        /// <summary>
        /// Return Parsed Articles. Sorting by Posted Date. If Posted Date isn't presented then by Parsed Date
        /// </summary>
        /// <param name="articleGroupId">Article Group Id</param>
        /// <param name="page">Page. Starts from zero</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="from">Date which will be used as FROM filter by Parsed Date. Optional. Format: dd-MMMM-yyyy</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetParsedArticles(int articleGroupId, int page, int pageSize, string from = null)
        {
            var result = ParsedContentService
                .GetArticles(articleGroupId, page, pageSize, from.ToNullableDateTime(ApiResultMapper.DateFormat))
                .ToPagingResult(x => x.Map<ApiParsedArticle, ParsedArticleResult>());
            return Ok(result);
        }
    }
}
