﻿using System.Threading.Tasks;
using System.Web.Http;
using WebParser.Application.Interfaces.Services.Publishing;
using WebParser.Application.Interfaces.Services.Publishing.Entities;
using WebParser.WebApi.Mappers;
using WebParser.WebApi.Models;

namespace WebParser.WebApi.Controllers
{
    /// <summary>
    /// Publishing endpoint
    /// </summary>
    [Authorize(Roles = "Publisher")]
    public class PublishingController : ApiController
    {
        private IPublishingService PublishingService { get; }

        /// <summary>
        /// PublishingController constructor
        /// </summary>
        /// <param name="publishingService"></param>
        public PublishingController(IPublishingService publishingService)
        {
            PublishingService = publishingService;
        }

        /// <summary>
        /// Endpoint to publish ads
        /// </summary>
        /// <param name="model">Ad model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> Publish([FromBody]ArticleModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await PublishingService.SaveArticle(model.Map<ArticleModel, PublisherArticle>());
            return Ok();
        }
    }
}
