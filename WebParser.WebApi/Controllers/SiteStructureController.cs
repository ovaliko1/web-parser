﻿using System.Web.Http;
using System.Web.Http.Description;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Common.Extensions;
using WebParser.WebApi.Converters;

namespace WebParser.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [Authorize(Roles = "User")]
    public class SiteStructureController : ApiController
    {
        private ISiteStructureService SiteStructureService { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteStructureService"></param>
        public SiteStructureController(
            ISiteStructureService siteStructureService)
        {
            SiteStructureService = siteStructureService;
        }

        /// <summary>
        /// Return all sites available for user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetAvailableSites()
        {
            var result = SiteStructureService.GetAvailableSites().ToArray(x => x.ToSiteResult());
            return Ok(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IHttpActionResult GetAllSites()
        {
            var result = SiteStructureService.GetAllSites().ToArray(x => x.ToAdminSiteResult());
            return Ok(result);
        }

        /// <summary>
        /// Return ArticleGroups by siteId
        /// </summary>
        /// <param name="siteId">Site Id</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetArticleGroupsBySite(int siteId)
        {
            var result = SiteStructureService.GetArticleGroupsBySite(siteId).ToArray(x => x.ToArticlesGroupResult());
            return Ok(result);
        }
    }
}
