﻿using System.Globalization;

namespace WebParser.Application.Interfaces.DomainEntities.Search.ParsingRule
{
    public class DateTimeParsingRule
    {
        public DateTimeParsingRule()
        {
            CultureInfo = CultureInfo.InvariantCulture;
        }

        public CultureInfo CultureInfo { get; set; }

        public string DateFormat { get; set; }
    }
}
