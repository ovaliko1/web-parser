﻿using WebParser.Common;

namespace WebParser.Application.Interfaces.DomainEntities.Search
{
    public struct FoundValue
    {
        public FoundValueType FoundValueType { get; set; }

        public string AttrName { get; set; }
    }
}
