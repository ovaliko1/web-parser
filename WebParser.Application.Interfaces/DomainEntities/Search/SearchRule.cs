﻿using WebParser.Application.Interfaces.DomainEntities.Search.ParsingRule;
using WebParser.Common;

namespace WebParser.Application.Interfaces.DomainEntities.Search
{
    public class SearchRule
    {
        public ActionType ActionType { get; set; }

        public int? WaitMilseconds { get; set; }

        public SearchType SearchType { get; set; }

        public FoundValue FoundValue { get; set; }

        public bool IsImage { get; set; }

        public bool IsDateTime { get; set; }

        public bool DoNotSaveToInternalStorage { get; set; }

        public DateTimeParsingRule DateTimeParsingRule { get; set; }

        public string FieldName { get; set; }

        public string Selectors { get; set; }

        public string ReplaceRegexp { get; set; }

        public string BackgroundColor { get; set; }

        public string FilterFunction { get; set; }

        public bool ShowOnVoteForm { get; set; }

        public bool HideOnVkPost { get; set; }

        public int ParsedKeyTypeId { get; set; }
    }
}
