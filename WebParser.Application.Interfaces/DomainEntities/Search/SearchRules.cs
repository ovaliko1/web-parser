﻿namespace WebParser.Application.Interfaces.DomainEntities.Search
{
    public class SearchRules
    {
        public SearchRule[] Rules { get; set; }
    }
}
