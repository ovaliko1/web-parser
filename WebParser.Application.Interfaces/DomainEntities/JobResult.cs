﻿namespace WebParser.Application.Interfaces.DomainEntities
{
    public class JobResult
    {
        public bool Success { get; set; }

        public string Log { get; set; }
    }
}
