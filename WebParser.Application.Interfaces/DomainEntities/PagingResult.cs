﻿namespace WebParser.Application.Interfaces.DomainEntities
{
    public class PagingResult<T>
    {
        public T[] Items { get; set; }

        public int TotalCount { get; set; }
    }
}
