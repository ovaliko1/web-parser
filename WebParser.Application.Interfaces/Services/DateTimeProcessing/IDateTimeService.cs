﻿using System;

namespace WebParser.Application.Interfaces.Services.DateTimeProcessing
{
    public interface IDateTimeService
    {
        DateTime GetCurrentDateTimeUtc();
    }
}
