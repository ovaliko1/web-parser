﻿namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public interface ICityService
    {
        City[] GetCities();

        City GetUserCity(int userId);

        int? GetUserIdByCity(int cityId);
    }
}
