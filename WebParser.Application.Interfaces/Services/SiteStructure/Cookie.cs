﻿namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public class Cookie
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
