﻿using WebParser.Common;

namespace WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings
{
    public class ParsedSiteDetails
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int SchedulerStartHours { get; set; }

        public int SchedulerStartMinutes { get; set; }

        public int SchedulerEndHours { get; set; }

        public int SchedulerEndMinutes { get; set; }

        public int RepeatIntervalInMunutes { get; set; }

        public bool IsOneTimeExported { get; set; }

        public JobType JobType { get; set; }
    }
}
