﻿using System;

namespace WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings
{
    public class Site
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public int? CityId { get; set; }

        public string Link { get; set; }

        public string Name { get; set; }

        public bool OneTimeExport { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsInParsing { get; set; }

        public bool NeedStartOneTimeParsing { get; set; }

        public int? SchedulerStartHours { get; set; }

        public int? SchedulerStartMinutes { get; set; }

        public int? SchedulerEndHours { get; set; }

        public int? SchedulerEndMinutes { get; set; }

        public int? RepeatIntervalInMunutes { get; set; }

        public string ProxyAddress { get; set; }

        public string ProxyUsername { get; set; }

        public string ProxyPassword { get; set; }

        public string WebHookUrl { get; set; }

        public Cookie[] Cookies { get; set; }

        public int? WatermarkHeight { get; set; }

        public int? WatermarkWidth { get; set; }

        public int? WatermarkFontSize { get; set; }

        public string WatermarkRectX { get; set; }

        public string WatermarkRectY { get; set; }

        public string WatermarkTextX { get; set; }

        public string WatermarkTextY { get; set; }

        public string UserAgent { get; set; }

        public int JobTypeId { get; set; }

        public int? BlackListIdentitySiteId { get; set; }

        public string GeoBounds { get; set; }

        public bool UseSourceLinkAsContact { get; set; }

        public int CropImageTop { get; set; }

        public int CropImageBottom { get; set; }

        public int CropImageLeft { get; set; }

        public int CropImageRight { get; set; }

        public string RegionCode { get; set; }

        public string LanguageCode { get; set; }

        public TimeSpan? GetStartTime()
        {
            return SchedulerStartHours.HasValue && SchedulerStartMinutes.HasValue
                ? new TimeSpan(0, SchedulerStartHours.Value, SchedulerStartMinutes.Value, 0)
                : (TimeSpan?)null;
        }

        public TimeSpan? GetEndTime()
        {
            return SchedulerEndHours.HasValue && SchedulerEndMinutes.HasValue
                ? new TimeSpan(0, SchedulerEndHours.Value, SchedulerEndMinutes.Value, 0)
                : (TimeSpan?)null;
        }
    }
}
