﻿namespace WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings
{
    public interface ISiteService
    {
        Site GetSite(int siteId);

        void StartSiteParsing(int siteId);

        void SetSiteInParsingState(int siteId);

        void ResetSiteParsingState(int siteId);

        ParsedSiteDetails[] GetDailyParsedSites();

        ParsedSiteDetails[] GetOneTimeParsedSites();
    }
}
