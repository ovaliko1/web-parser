﻿namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public class City
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
