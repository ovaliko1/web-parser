﻿namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public interface IArticlesGroupService
    {
        ViewerSourceUser[] GetAvailableForViewerSourceUsers();

        int[] GetArticleGroupIds(int cityId, string articleGroupInternalNames);
    }
}