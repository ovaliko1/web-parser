﻿namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public struct AdminSite
    {
        public int Id { get; set; }

        public string Link { get; set; }

        public string Name { get; set; }

        public string UserEmail { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsInParsing { get; set; }

        public int SchedulerStartHours { get; set; }

        public int SchedulerStartMinutes { get; set; }
    }
}
