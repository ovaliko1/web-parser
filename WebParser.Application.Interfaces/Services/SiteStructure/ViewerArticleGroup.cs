﻿namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public class ViewerArticleGroup
    {
        public int[] Ids { get; set; }

        public string Name { get; set; }
    }
}
