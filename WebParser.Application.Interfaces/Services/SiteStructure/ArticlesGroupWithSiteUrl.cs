﻿namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public class ArticlesGroupWithSiteUrl : ArticlesGroup
    {
        public string SiteUrl { get; set; }
    }
}
