﻿using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Common.MetroStations;

namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public interface IMetroMapService
    {
        MetroStation[] GetStations(int? cityId);

        MetroStation[] GetStationsByPublisherId(int publisherId);

        MetroStation[] GetStationsByIds(string[] ids);

        MetroStation GetStationByJsonIds(string jsonStationIds);

        MetroStation[] GetStationsByJsonIds(string jsonStationIds);

        MetroStation[] ExtractMetroStations(int? cityId, ParsedValue parsedValue);

        MetroStation FindMetroStation(int? cityId, string name);
    }
}
