﻿namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public class ArticlesGroupDetails
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
