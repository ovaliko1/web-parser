﻿namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public class ViewerSourceUser
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? CityId { get; set; }

        public ViewerArticleGroup[] ArticleGroups { get; set; }
    }
}
