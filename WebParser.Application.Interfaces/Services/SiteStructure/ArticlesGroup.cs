﻿using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.Services.File;

namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public class ArticlesGroup
    {
        public int Id { get; set; }

        public string InternalName { get; set; }

        public string PublicName { get; set; }

        public string[] Links { get; set; }

        public string LinksFormat { get; set; }

        public int? StartPage { get; set; }

        public int? StepSize { get; set; }

        public int? LinksFileId { get; set; }

        public long? VkGroupId { get; set; }

        public long? VkTopicId { get; set; }

        public long? VkTopicGroupId { get; set; }

        public int SpiderTypeId { get; set; }

        public int LinksToArticlesSearchRuleSpiderTypeId { get; set; }

        public bool SpiderDoNotLoadImages { get; set; }

        public FileInfo LinksFile { get; set; }

        public SearchRule LinksToArticlesSearchRule { get; set; }

        public SearchRule[] ArticleSearchRules { get; set; }

        public int BatchSize { get; set; }

        public int BatchDelayInSeconds { get; set; }

        public int DownloadFileDelayInMillseconds { get; set; }

        public int? WaitMilliseconds { get; set; }

        public string TwitterConsumerKey { get; set; }

        public string TwitterConsumerSecret { get; set; }

        public string TwitterAccessToken { get; set; }

        public string TwitterAccessTokenSecret { get; set; }
    }
}
