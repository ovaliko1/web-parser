﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Common.Mappers;

namespace WebParser.Application.Interfaces.Services.SiteStructure
{
    public interface ISiteStructureService
    {
        int CreateSite(Site site);

        void EditSite(Site site);

        void DeleteSite(int siteId);

        Site GetSiteById(int siteId);

        int CreateArticlesGroup(int siteId, ArticlesGroup articlesGroup);

        void EditArticlesGroup(ArticlesGroup articlesGroup);

        void DeleteArticlesGroup(int articlesGroupId);

        AdminSite[] GetAllSites();

        Site[] GetAvailableSites();

        Site[] GetViewerAvailableSites();

        Item[] GetViewerAvailableSites(int sourceUserId);

        PagingResult<Site> GetAvailableSites<T>(RequestFilters<T> filters);

        ArticlesGroup[] GetArticleGroupsBySite(int siteId);

        ArticlesGroupWithSiteUrl GetArticlesGroupById(int articlesGroupId);

        PagingResult<ArticlesGroup> GetArticleGroupsBySite<T>(int siteId, RequestFilters<T> filters);

        Task<Item[]> GetArticleGroupItemBySite(int siteId);
    }
}
