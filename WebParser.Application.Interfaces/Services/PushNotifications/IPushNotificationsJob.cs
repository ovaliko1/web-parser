﻿using System.Threading.Tasks;

namespace WebParser.Application.Interfaces.Services.PushNotifications
{
    public interface IPushNotificationsJob
    {
        Task<bool> RunPing();
    }
}