﻿using System.Threading.Tasks;

namespace WebParser.Application.Interfaces.Services.Ping
{
    public interface IPingService
    {
        Task<bool> Ping();
    }
}
