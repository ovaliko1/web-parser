﻿using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.WhiteList.Entities;

namespace WebParser.Application.Interfaces.Services.WhiteList
{
    public interface IWhiteListReportService
    {
        PagingResult<WhiteListItem> ReadWhiteListItems<T>(RequestFilters<T> filters);

        PagingResult<HitedWhiteArticle> ReadHitedArticles<T>(RequestFilters<T> filters, int itemId);

        void DeleteWhiteListItem(int id);

        void AddWhiteListItem(WhiteListItem item);
    }
}