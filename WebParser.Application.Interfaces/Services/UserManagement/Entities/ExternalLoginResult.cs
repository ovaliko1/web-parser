﻿namespace WebParser.Application.Interfaces.Services.UserManagement.Entities
{
    public class ExternalLoginResult
    {
        public bool Success { get; set; }

        public string[] Errors { get; set; }

        public static ExternalLoginResult Failed()
        {
            return Failed(string.Empty);
        }

        public static ExternalLoginResult Failed(string errorMessage)
        {
            return Failed(new[] { errorMessage });
        }

        public static ExternalLoginResult Failed(string[] errorMessages)
        {
            return new ExternalLoginResult
            {
                Success = false,
                Errors = errorMessages,
            };
        }

        public static ExternalLoginResult Succeed()
        {
            return new ExternalLoginResult
            {
                Success = true,
            };
        }
    }
}
