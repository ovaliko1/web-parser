﻿using System;

namespace WebParser.Application.Interfaces.Services.UserManagement.Entities
{
    public class ViewerDetails
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public System.DateTime AccessDateTime { get; set; }

        public int OpeningBalance { get; set; }

        public bool EmailConfirmed { get; set; }

        public bool SeenForbidden { get; set; }

        public int VotesCount { get; set; }

        public long? VkId { get; set; }

        public bool PurchasedAccount { get; set; }

        public bool WaitingForPayment { get; set; }

        public Guid PublicId { get; set; }

        public YandexPaymentConfirmation[] PaymentConfirmations { get; set; }
    }
}