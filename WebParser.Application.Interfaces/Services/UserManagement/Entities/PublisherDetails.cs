﻿using WebParser.Common.Mappers;

namespace WebParser.Application.Interfaces.Services.UserManagement.Entities
{
    public class PublisherDetails
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public System.DateTime AccessDateTime { get; set; }

        public Item Site { get; set; }

        public Item ArticleGroup { get; set; }
    }
}