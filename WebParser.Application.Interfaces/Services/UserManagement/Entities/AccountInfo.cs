﻿namespace WebParser.Application.Interfaces.Services.UserManagement.Entities
{
    public class AccountInfo
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Ip { get; set; }
    }
}