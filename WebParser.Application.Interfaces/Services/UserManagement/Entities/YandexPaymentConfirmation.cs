﻿namespace WebParser.Application.Interfaces.Services.UserManagement.Entities
{
    public class YandexPaymentConfirmation
    {
        public string NotificationType { get; set; }

        public string OperationId { get; set; }

        public string Amount { get; set; }

        public string WithdrawAmount { get; set; }

        public string Currency { get; set; }

        public string DateTime { get; set; }

        public string Sender { get; set; }

        public bool CodePro { get; set; }

        public string Label { get; set; }

        public string Sha1Hash { get; set; }

        public bool TestNotification { get; set; }

        public bool Unaccepted { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string FathersName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string Building { get; set; }

        public string Suite { get; set; }

        public string Flat { get; set; }

        public string Zip { get; set; }

        public string AccessDateBefore { get; set; }

        public string AccessDateAfter { get; set; }
    }
}