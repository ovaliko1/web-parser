﻿using WebParser.Common;

namespace WebParser.Application.Interfaces.Services.UserManagement.Entities
{
    public class UserDetails
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public int MaxSiteCount { get; set; }

        public string NameForViewers { get; set; }

        public int? CityId { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public System.DateTime AccessDateTime { get; set; }

        public Role[] Roles { get; set; }
    }
}
