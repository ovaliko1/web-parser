﻿namespace WebParser.Application.Interfaces.Services.UserManagement.Entities
{
    public class ExternalLoginDetails
    {
        public string AuthenticationType { get; set; }

        public string Caption { get; set; }
    }
}