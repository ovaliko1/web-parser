﻿namespace WebParser.Application.Interfaces.Services.UserManagement.Entities
{
    public class UserItem
    {
        public int Id { get; set; }

        public string Email { get; set; }
    }
}
