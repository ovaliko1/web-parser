﻿using System;
using WebParser.Common;

namespace WebParser.Application.Interfaces.Services.UserManagement.Entities
{
    public class User
    {
        public Role[] Roles { get; set; }

        public int Id { get; set; }

        public string Email { get; set; }

        public int MaxSiteCount { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime AccessDateTime { get; set; }

        public int OpeningBalance { get; set; }

        public bool EmailConfirmed { get; set; }

        public bool PurchasedAccount { get; set; }

        public int FreeHours { get; set; }

        public int FreeOpenings { get; set; }

        public int ClickerCautionCount { get; set; }

        public string UserName { get; set; }

        public long? VkId { get; set; }

        public bool WaitingForPayment { get; set; }

        public Guid PublicId { get; set; }
    }
}