﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Common;

namespace WebParser.Application.Interfaces.Services.UserManagement
{
    public interface IUserService
    {
        Task<string[]> Login(string name, string password, bool rememberMe);

        Task<int> CreateUser(UserDetails user, string callbackUrlFormat);

        Task<int> CreatePublisher(PublisherDetails publisher, string callbackUrlFormat);

        Task<string[]> CreateViewer(AccountInfo accountInfo, string callbackUrlFormat);

        Task<bool> ConfirmEmail(int userId, string code);

        Task<string[]> ResetPassword(string email, string code, string password);

        Task ForgotPassword(string email, string callbackUrlFormat);

        Task ChangePassword(string oldPassword, string newPassword);

        void SignOut();

        PagingResult<UserDetails> ReadUsers<T>(RequestFilters<T> filters);

        PagingResult<ViewerDetails> ReadViewers<T>(RequestFilters<T> filters);

        PagingResult<PublisherDetails> ReadPublishers<T>(RequestFilters<T> filters);

        PublisherDetails GetPublisherById(int id);

        UserItem[] GetAllUsers();

        User GetCurrentUser();

        bool UserAccessExpired(Entities.User user);

        RateType? GetUserRateType(Entities.User user);

        void UpdateWaitingForPaymentStatus();

        void TryAcceptYandexPaymentConfirmation(YandexPaymentConfirmation paymentConfirmation);

        UserVoteInfo GetCurrentUserVoteInfo();

        int? GetCurrentUserId();

        void EditUser(UserDetails user);

        void SetSeenForbidden(int userId);

        ExternalLoginDetails GetExternalLoginInfo();

        Task<ExternalLoginResult> ExternalLogin(string ip);

        void TryExtendUserAccessForSharing();

        void EditViewer(ViewerDetails viewer);

        void EditPublisher(PublisherDetails publisher);

        Site GetPublisherSite(int publisherId);
    }
}
