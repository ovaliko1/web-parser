﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Interfaces.Services.SiteStructure;

namespace WebParser.Application.Interfaces.Services.WebClient
{
    public interface IWebClient
    {
        Task<FileDetails> DownloadFile(int siteId, int userId, string baseUrl, string relativeUrl, Cookie[] cookies);
    }
}
