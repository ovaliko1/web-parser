﻿using System;
using WebParser.Application.Interfaces.Services.Seo.Entities;
using WebParser.Common;

namespace WebParser.Application.Interfaces.Services.Seo
{
    public interface ISeoRoutingService
    {
        SeoRoutingResult Route(Uri currentUrl);

        SeoDetails GetSeoDetails(string url, SeoRoutingResult routingResult);
    }
}