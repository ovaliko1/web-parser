﻿using System.Threading.Tasks;

namespace WebParser.Application.Interfaces.Services.Seo
{
    public interface ISiteMapService
    {
        Task<string> GetSiteMapIndex(bool debug);

        string GetSiteMap(string articleGroupUrlSection, string country, string city, bool debug);
    }
}