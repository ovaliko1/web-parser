﻿using WebParser.Common;

namespace WebParser.Application.Interfaces.Services.Seo.Entities
{
    public class SeoDetails
    {
        public SeoRoutingResult RoutingResult { get; set; }

        public BreadcrumbItem[] Breadcrumbs { get; set; }

        public string CanonicalUrl { get; set; }

        public string CityName { get; set; }

        public string ArticleGroup { get; set; }

        public string District { get; set; }

        public string CountryRegionCode { get; set; }

        public string LanguageCode { get; set; }

        public int? CityUserId { get; set; }
    }
}
