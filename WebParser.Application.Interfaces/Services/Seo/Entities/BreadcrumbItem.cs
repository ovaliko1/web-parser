﻿namespace WebParser.Application.Interfaces.Services.Seo.Entities
{
    public class BreadcrumbItem
    {
        public string Title { get; set; }

        public string Url { get; set; }
    }
}
