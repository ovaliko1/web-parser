﻿namespace WebParser.Application.Interfaces.Services.Settings
{
    public interface ICommonSettings
    {
        string GetSupportEmail();

        string GetPaymentEmail();

        bool GetRenderMetrics();

        string GetYandexMetrikaCounterName();

        string GetViewerArticlePreviewUrlFormat();

        string GetYandexPaymentReceiver();

        long GetVkApplicationId();

        string GetVkLogin();

        string GetVkPassword();

        long GetVkImageUploadGroupId();

        long GetVkImageUploadAlbumId();

        int GetFirstRateOpeningsCount();

        int GetFirstRateOpeningsPrice();

        int GetSecondRateOpeningsCount();

        int GetSecondRateOpeningsPrice();

        int GetThirdRateOpeningsCount();

        int GetThirdRateOpeningsPrice();

        int GetFourthRateOpeningsCount();

        int GetFourthRateOpeningsPrice();

        int GetFirstRateDaysCount();

        int GetFirstRateDaysPrice();

        int GetSecondRateDaysCount();

        int GetSecondRateDaysPrice();

        int GetThirdRateDaysCount();

        int GetThirdRateDaysPrice();

        int GetFourthRateDaysCount();

        int GetFourthRateDaysPrice();
    }
}