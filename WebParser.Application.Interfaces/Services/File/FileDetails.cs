﻿namespace WebParser.Application.Interfaces.Services.File
{
    public class FileDetails
    {
        public string FileName { get; set;  }

        public byte[] FileData { get; set; }

        public string ContentType { get; set; }

        public string FileExtension { get; set; }
    }
}
