﻿namespace WebParser.Application.Interfaces.Services.File
{
    public class CreatedFile
    {
        public int Id { get; set; }

        public string FileUrl { get; set; }

        public byte[] FileData { get; set; }
    }
}
