﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebParser.Application.Interfaces.Services.File
{
    public interface IFileService
    {
        Task<CreatedFile> CreatePersistentFile(int userId, int? siteId, FileDetails fileDetails);

        Task<CreatedFile[]> CreateTemporaryFiles(int userId, int? siteId, FileDetails[] fileDetails);

        Task<KeyValuePair<int, CreatedFile>[]> CreateTemporaryFiles(int userId, int? siteId, KeyValuePair<int, FileDetails>[] fileDetails);
    }
}
