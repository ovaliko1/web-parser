﻿namespace WebParser.Application.Interfaces.Services.File
{
    public class FileInfo
    {
        public string FileName { get; set; }

        public string Extensions { get; set; }
    }
}
