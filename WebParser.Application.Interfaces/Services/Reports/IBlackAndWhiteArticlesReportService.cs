﻿using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.Reports.Entities;

namespace WebParser.Application.Interfaces.Services.Reports
{
    public interface IBlackAndWhiteArticlesReportService
    {
        PagingResult<BlackAndWhiteArticle> ReadBlackAndWhiteArticles<T>(RequestFilters<T> filters);
    }
}