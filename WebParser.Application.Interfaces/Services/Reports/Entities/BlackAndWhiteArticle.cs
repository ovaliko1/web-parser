﻿using System;

namespace WebParser.Application.Interfaces.Services.Reports.Entities
{
    public class BlackAndWhiteArticle
    {
        public int? BlackListItemId { get; set; }

        public int? WhiteListItemId { get; set; }

        public int Id { get; set; }

        public int? SiteId { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public string ArticleGroup { get; set; }

        public Guid? DuplicateGroupId { get; set; }

        public string Identity { get; set; }

        public int IdentityConfidencePercentage { get; set; }

        public bool IsDuplicate { get; set; }

        public string Price { get; set; }

        public string FormattedAddress { get; set; }

        public string Metro { get; set; }

        public string MetroDistance { get; set; }

        public string MetroDuration { get; set; }

        public string Link { get; set; }
    }
}