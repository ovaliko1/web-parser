﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.DomainEntities;

namespace WebParser.Application.Interfaces.Services.ExpiredArticlesJob
{
    public interface IExpiredArticlesJobService
    {
        Task<JobResult> DeleteExpiredArticles();
    }
}