﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.Publishing.Entities;

namespace WebParser.Application.Interfaces.Services.Publishing
{
    public interface IPublishingService
    {
        Task SaveArticle(PublisherArticle article);
    }
}