﻿namespace WebParser.Application.Interfaces.Services.Publishing.Entities
{
    public class PublisherArticle
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Identity { get; set; }

        public string MetroStation { get; set; }

        public string[] Images { get; set; }
    }
}