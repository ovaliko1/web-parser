﻿using WebParser.Application.Interfaces.DomainEntities;

namespace WebParser.Application.Interfaces.Services.MarkingJob
{
    public interface IMarkingJobService
    {
        JobResult RunBlackListArticleMarking();

        JobResult RunWhiteListArticleMarking();
    }
}
