﻿namespace WebParser.Application.Interfaces.Services.BlackList.Entities
{
    public class BlackListItem
    {
        public int Id { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public string Identity { get; set; }

        public int IdentityConfidencePercentage { get; set; }

        public int Hits { get; set; }

        public int? SiteId { get; set; }

        public int? ArticleSiteId { get; set; }

        public int? SourceArticleId { get; set; }

        public int CreatedBy { get; set; }
    }
}