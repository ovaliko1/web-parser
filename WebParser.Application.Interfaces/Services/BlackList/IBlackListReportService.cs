﻿using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.BlackList.Entities;

namespace WebParser.Application.Interfaces.Services.BlackList
{
    public interface IBlackListReportService
    {
        PagingResult<BlackListItem> ReadBlackListItems<T>(RequestFilters<T> filters);

        PagingResult<HitedArticle> ReadHitedArticles<T>(RequestFilters<T> filters, int blackListItemId);

        void DeleteBlackListItem(int id);

        void AddBlackListItem(BlackListItem item);
    }
}
