﻿namespace WebParser.Application.Interfaces.Services.ArticleRating
{
    // TODO: Move to appSettings
    public static class ArticleRatingConstants
    {
        public static readonly int FreeHourGoodVotesPrice = 5;

        public static readonly int FreeHourBadVotesPrice = 1;

        public static readonly int FreeHourOutdateVotesPrice = 3;

        public static readonly int FreeOpeningGoodVotesPrice = 5;

        public static readonly int FreeOpeningBadVotesPrice = 1;

        public static readonly int FreeOpeningOutdateVotesPrice = 3;

        public static readonly int DoNotShowArticleRating = -3;

        public static readonly int DoNotShowArticleOutdateRating = 2;

        public static readonly int PostToSocialMediaArticleRating = 0;

        public static readonly int MaxFreeHoursForTrialAccount = 24;

        public static readonly int MaxFreeHoursForPurchasedAccount = 24 * 3;

        public static readonly int MaxFreeOpenings = 5;

        public static readonly int MaxClickerCautionCount = 3;
    }
}
