﻿using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Common;

namespace WebParser.Application.Interfaces.Services.ArticleRating
{
    public interface IArticleRatingService
    {
        PagingResult<UnwatchedVote> ReadUnwatchedVotes<T>(RequestFilters<T> filters);

        void SaveVoteReviewResult(int voteId, ReviewResult reviewResult);

        VoteResultType Vote(int articleId, ArticleRatingType ratingType);
    }
}
