﻿namespace WebParser.Application.Interfaces.Services.Apartments.Entities
{
    public class ApartmentList
    {
        public ApartmentItem[] Items { get; set; }

        public bool HasMore { get; set; }
    }
}