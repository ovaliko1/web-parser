﻿using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;
using WebParser.Common;

namespace WebParser.Application.Interfaces.Services.Apartments.Entities
{
    public class Apartment
    {
        public int Id { get; set; }

        public string Subject { get; set; }

        public string Price { get; set; }

        public MetroItem[] MetroItems { get; set; }

        public string[] SourceMetro { get; set; }

        public string Address { get; set; }

        public string[] SourceAddress { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public string[] OwnerNames { get; set; }

        public PhoneState PhoneState { get; set; }

        public PhoneItem[] Phones { get; set; }

        public string[] Images { get; set; }

        public string[] Description { get; set; }

        public string[] Characteristics { get; set; }

        public string[] SourceDates { get; set; }

        public bool PossiblyRented { get; set; }

        public bool InBlackList { get; set; }

        public bool InWhiteList { get; set; }

        public bool HideBlacklistWarranty { get; set; }

        public ImageInfo[] FullSizeImages { get; set; }
    }
}