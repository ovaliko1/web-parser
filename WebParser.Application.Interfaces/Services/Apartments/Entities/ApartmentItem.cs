﻿using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;

namespace WebParser.Application.Interfaces.Services.Apartments.Entities
{
    public class ApartmentItem
    {
        public int Id { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public int? SiteId { get; set; }

        public string ArticlesGroup { get; set; }

        public int? Price { get; set; }

        public string PriceStr { get; set; }

        public MetroItem Metro { get; set; }

        public int GoodRating { get; set; }

        public int BadRating { get; set; }

        public int OutdateRating { get; set; }

        public string Source { get; set; }

        public bool ShowRating { get; set; }

        public bool InWhiteList { get; set; }

        public bool PossiblyRented { get; set; }

        public bool HideBlacklistWarranty { get; set; }

        public ImageInfo[] ImagesPreview { get; set; }
    }
}