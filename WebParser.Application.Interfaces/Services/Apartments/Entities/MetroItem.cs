﻿namespace WebParser.Application.Interfaces.Services.Apartments.Entities
{
    public class MetroItem
    {
        public MetroItem()
        {
        }

        public MetroItem(string metro, string metroDistance, string metroDuration)
        {
            Metro = metro;
            MetroDistance = metroDistance;
            MetroDuration = metroDuration;
        }

        public string Metro { get; set; }

        public string MetroDistance { get; set; }

        public string MetroDuration { get; set; }
    }
}