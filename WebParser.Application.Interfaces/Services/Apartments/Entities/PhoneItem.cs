﻿using WebParser.Common;

namespace WebParser.Application.Interfaces.Services.Apartments.Entities
{
    public class PhoneItem
    {
        public PhoneValueType Type { get; set; }

        public string Value { get; set; }

        public string BackgroundColor { get; set; }
    }
}