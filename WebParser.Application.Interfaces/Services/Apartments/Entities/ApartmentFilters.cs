﻿namespace WebParser.Application.Interfaces.Services.Apartments.Entities
{
    public class ApartmentFilters
    {
        public int? ApartmentId { get; set; }

        // TODO: Rename to UserId or support real CityId here
        public int CityId { get; set; }

        public int[] SourceIds { get; set; }

        public int[] MetroStationIds { get; set; }

        public int[] ArticleGroupIds { get; set; }

        public int? MinutesToMetro { get; set; }

        public int? PriceFrom { get; set; }

        public int? PriceTo { get; set; }

        public bool ShowWithEmptyMetro { get; set; }

        public bool ShowWithEmptyPrice { get; set; }
    }
}