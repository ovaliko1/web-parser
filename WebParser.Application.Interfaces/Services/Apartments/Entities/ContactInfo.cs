﻿namespace WebParser.Application.Interfaces.Services.Apartments.Entities
{
    public class ContactInfo
    {
        public PhoneItem[] Phones { get; set; }

        public bool ShowForbidden { get; set; }

        public bool CanBeVoted { get; set; }
    }
}