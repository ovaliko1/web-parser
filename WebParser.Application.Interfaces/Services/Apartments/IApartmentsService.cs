﻿using WebParser.Application.Interfaces.Services.Apartments.Entities;

namespace WebParser.Application.Interfaces.Services.Apartments
{
    public interface IApartmentsService
    {
        ApartmentList ReadApartments(int page, int pageSize, ApartmentFilters filters);

        Apartment GetApartment(int id);

        ContactInfo GetContactInfo(int apartmentId);
    }
}
