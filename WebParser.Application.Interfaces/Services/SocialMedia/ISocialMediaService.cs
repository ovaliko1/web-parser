﻿using System.Threading.Tasks;

namespace WebParser.Application.Interfaces.Services.SocialMedia
{
    public interface ISocialMediaService
    {
        Task<bool> RunExportParsingResultToSocialMedia(System.DateTime previousRunTime);
    }
}