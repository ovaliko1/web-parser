﻿using System.Globalization;
using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;

namespace WebParser.Application.Interfaces.Services.Culture
{
    public interface ICultureService
    {
        CultureInfo GetPriceCultureByString(string price);

        Task<CultureInfo> GetCultureBySiteAsync(Site site);
    }
}
