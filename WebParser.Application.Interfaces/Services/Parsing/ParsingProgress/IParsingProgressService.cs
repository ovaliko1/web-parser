﻿using WebParser.Application.Interfaces.DomainEntities;

namespace WebParser.Application.Interfaces.Services.Parsing.ParsingProgress
{
    public interface IParsingProgressService
    {
        PagingResult<LogEntry> GetParsingLog<T>(RequestFilters<T> filters);
    }
}