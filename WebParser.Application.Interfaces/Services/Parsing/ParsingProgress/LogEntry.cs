﻿namespace WebParser.Application.Interfaces.Services.Parsing.ParsingProgress
{
    public class LogEntry
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public System.DateTime Date { get; set; }

        public int? SiteId { get; set; }

        public int Status { get; set; }

        public string Message { get; set; }
    }
}