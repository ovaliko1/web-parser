﻿using System.Threading.Tasks;

namespace WebParser.Application.Interfaces.Services.Parsing
{
    public interface IParsingJob
    {
        Task<bool> Run(int siteId);
    }
}
