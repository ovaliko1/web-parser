﻿namespace WebParser.Application.Interfaces.Services.Parsing.AddressRecognizing
{
    public class AddressInfo
    {
        public string FormattedAddress { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public NearestMetroStation NearestMetroStation { get; set; }
    }
}