﻿namespace WebParser.Application.Interfaces.Services.Parsing.AddressRecognizing
{
    public class NearestMetroStation
    {
        public string Name { get; set; }

        public string DistanceText { get; set; }

        public int DistanceValue { get; set; }

        public string DurationText { get; set; }

        public int DurationValue { get; set; }
    }
}