﻿namespace WebParser.Application.Interfaces.Services.Parsing.ImageProcessing
{
    public class ImageInfo
    {
        public int? FileId { get; set; }

        public byte[] FileData { get; set; }

        public string ImageUrl { get; set; }

        public int? Height { get; set; }

        public int? Width { get; set; }
    }
}