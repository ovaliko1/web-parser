﻿namespace WebParser.Application.Interfaces.Services.Parsing.ImageProcessing
{
    public class ImageProcessingResult
    {
        public ImageProcessingResult()
        {
        }

        public ImageProcessingResult(ImageInfo[] fullSizeImages, ImageInfo[] imagesPreview)
        {
            FullSizeImages = fullSizeImages;
            ImagesPreview = imagesPreview;
        }

        public ImageInfo[] FullSizeImages { get; set; }

        public ImageInfo[] ImagesPreview { get; set; }
    }
}