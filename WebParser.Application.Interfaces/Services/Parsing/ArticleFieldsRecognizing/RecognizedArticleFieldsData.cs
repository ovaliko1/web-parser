﻿using WebParser.Application.Interfaces.Services.Parsing.AddressRecognizing;
using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using MetroStation = WebParser.Common.MetroStations.MetroStation;

namespace WebParser.Application.Interfaces.Services.Parsing.ArticleFieldsRecognizing
{
    public class RecognizedArticleFieldsData
    {
        public ArticleIdentity ArticleIdentity { get; set; }

        public MetroStation[] MetroStations { get; set; }

        public int? Price { get; set; }

        public string PriceStr { get; set; }

        public AddressInfo AddressInfo { get; set; }

        public ImageProcessingResult ImageProcessingResult { get; set; }
    }
}