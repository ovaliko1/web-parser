﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;

namespace WebParser.Application.Interfaces.Services.Parsing
{
    public interface IParsingService
    {
        Task<ParsedArticle> GetParsingResultPreview(int siteId, ArticlesGroup articlesGroup);
    }
}
