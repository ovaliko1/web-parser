﻿using System;
using WebParser.Common.MetroStations;

namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class ViewerParsedArticleItem
    {
        public int Id { get; set; }

        public int? SiteId { get; set; }

        public string ArticlesGroup { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public int GoodRating { get; set; }

        public int BadRating { get; set; }

        public int OutdateRating { get; set; }

        public bool ShowRating { get; set; }

        public int? Price { get; set; }

        public string PriceStr { get; set; }

        public bool IsDuplicate { get; set; }

        public bool InBlackList { get; set; }

        public bool InWhiteList { get; set; }

        public string Link { get; set; }

        public Guid? DuplicateGroupId { get; set; }

        public int? ParsingFailedCount { get; set; }

        public string ParsingFailedReason { get; set; }

        public MetroStation[] MetroStations { get; set; }
    }
}
