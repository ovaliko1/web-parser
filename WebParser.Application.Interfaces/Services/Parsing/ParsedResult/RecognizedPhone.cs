﻿namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class RecognizedPhone
    {
        public RecognizedPhone()
        {
        }

        public RecognizedPhone(string recognizedText, string fixedText, int meanConfidencePercentage)
        {
            RecognizedText = recognizedText;
            FixedText = fixedText;
            MeanConfidencePercentage = meanConfidencePercentage;
        }

        public string RecognizedText { get; set; }

        public string FixedText { get; set; }

        public int MeanConfidencePercentage { get; set; }
    }
}