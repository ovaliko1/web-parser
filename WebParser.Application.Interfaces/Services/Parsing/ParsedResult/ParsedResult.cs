﻿using System.Collections.Generic;
using System.Linq;

namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class ParsedResult
    {
        public string Link { get; set; }

        protected Dictionary<string, ParsedValue> parsedValues;

        public ParsedValue[] ParsedValues => parsedValues.Values.ToArray();

        public ParsedResult(ParsedValue[] values)
        {
            parsedValues = values.ToDictionary(x => x.Name, x => x);
        }
    }
}
