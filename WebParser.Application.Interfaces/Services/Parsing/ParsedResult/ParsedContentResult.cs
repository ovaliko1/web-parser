﻿namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class ParsedContentResult<T> where T : ParsedResult
    {
        public T[] Result { get; set; }

        public string SpiderOutput { get; set; }
    }
}