﻿using System;
using System.Globalization;
using System.Linq;
using WebParser.Common;
using WebParser.Common.Extensions;

namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class ParsedValue
    {
        public string Name { get; set; }

        public string[] Values { get; private set; }

        public System.DateTime[] DateTimeValues { get; private set; }

        public ParsedValueType ParsedValueType { get; private set; }

        public ParsedKeyType ParsedKeyType { get; set; }

        public string BackgroundColor { get; set; }

        public bool ShowOnVoteForm { get; set; }

        public bool HideOnVkPost { get; set; }

        public bool IsLink => ParsedValueType == ParsedValueType.LinkToFile ||
                              ParsedValueType == ParsedValueType.LinkToInternalStorage;

        public bool IsEmpty
        {
            get
            {
                var values = FormatValues("dd MM yy hh:mm:ss");
                return values.Length == 0 || values.All(string.IsNullOrEmpty);
            }
        }

        public RecognizedPhone[] RecognizedPhones { get; set; }

        private ParsedValue()
        {
            ParsedKeyType = ParsedKeyType.Common;
        }

        public string[] FormatValues(string dateTimeFormat)
        {
            return FormatValues(dateTimeFormat, null);
        }

        public string[] FormatValues(string dateTimeFormat, Func<string, string> formatLink)
        {
            switch (ParsedValueType)
            {
                case ParsedValueType.DateTime:
                    return DateTimeValues.ToArray(x => x.ToStringInvariant(dateTimeFormat));
                
                case ParsedValueType.String:
                case ParsedValueType.LinkToFile:
                case ParsedValueType.LinkToInternalStorage:
                    return formatLink != null ? Values.ToArray(formatLink) : Values;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static ParsedValue Create(string[] values, ParsedValueType valueType, string dateTimeFormat)
        {
            var result = new ParsedValue
            {
                ParsedValueType = valueType,
            };
            if (valueType == ParsedValueType.DateTime)
            {
                result.DateTimeValues = values.ToArray(y => System.DateTime.ParseExact(y, dateTimeFormat, CultureInfo.InvariantCulture));
            }
            else
            {
                result.Values = values.ToArray(x => x);
            }
            return result;
        }

        public static ParsedValue CreateLinkToFile(string value, bool isLinkToInternalStorage)
        {
            return new ParsedValue
            {
                Values = Utilities.CreateArray(value),
                ParsedValueType = isLinkToInternalStorage ? ParsedValueType.LinkToInternalStorage : ParsedValueType.LinkToFile,
            };
        }

        public static ParsedValue CreateLinkToFile(bool isLinkToInternalStorage, params string[] values)
        {
            return CreateLinkToFile(values, isLinkToInternalStorage);
        }

        public static ParsedValue CreateLinkToFile(string[] values, bool isLinkToInternalStorage)
        {
            return new ParsedValue
            {
                Values = values.ToArray(x => x),
                ParsedValueType = isLinkToInternalStorage ? ParsedValueType.LinkToInternalStorage : ParsedValueType.LinkToFile,
            };
        }

        public static ParsedValue Create(string value)
        {
            return new ParsedValue
            {
                Values = Utilities.CreateArray(value),
                ParsedValueType = ParsedValueType.String,
            };
        }

        public static ParsedValue Create(params string[] values)
        {
            return new ParsedValue
            {
                Values = values.ToArray(x => x),
                ParsedValueType = ParsedValueType.String,
            };
        }

        public static ParsedValue Create(System.DateTime value)
        {
            return new ParsedValue
            {
                DateTimeValues = Utilities.CreateArray(value),
                ParsedValueType = ParsedValueType.DateTime,
            };
        }

        public static ParsedValue Create(params System.DateTime[] values)
        {
            return new ParsedValue
            {
                DateTimeValues = values.ToArray(x => x),
                ParsedValueType = ParsedValueType.DateTime,
            };
        }
    }
}