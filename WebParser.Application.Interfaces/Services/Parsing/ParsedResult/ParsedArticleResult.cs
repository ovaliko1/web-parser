﻿using System.Linq;
using WebParser.Application.Interfaces.Services.Parsing.ArticleFieldsRecognizing;

namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class ParsedArticleResult : ParsedResult
    {
        public ParsedArticleResult(ParsedValue[] parsedValues) : base(parsedValues)
        {
        }

        public RecognizedArticleFieldsData RecognizedFieldsData { get; set; }

        public bool IsEmpty => parsedValues.All(x => x.Value.IsEmpty);
    }
}
