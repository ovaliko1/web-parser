﻿using WebParser.Application.Interfaces.Services.Parsing.ArticleFieldsRecognizing;

namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class ViewerParsedArticle : ParsedResult
    {
        public ViewerParsedArticle(ParsedValue[] values) : base(values)
        {
        }

        public int Id { get; set; }

        public bool CanBeVoted { get; set; }

        public string[] FoundArticleLinks { get; set; }

        public bool InBlackList { get; set; }

        public bool InWhiteList { get; set; }

        public int? WatermarkHeight { get; set; }

        public int? WatermarkWidth { get; set; }

        public int? WatermarkFontSize { get; set; }

        public string WatermarkRectX { get; set; }

        public string WatermarkRectY { get; set; }

        public string WatermarkTextX { get; set; }

        public string WatermarkTextY { get; set; }

        public bool HidePhone { get; set; }

        public RecognizedArticleFieldsData RecognizedFieldsData { get; set; }
    }
}
