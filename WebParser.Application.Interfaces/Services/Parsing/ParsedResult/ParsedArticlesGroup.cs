﻿namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class ParsedArticlesGroup : ParsedResult
    {
        public ParsedArticlesGroup(ParsedValue[] parsedValues)
            :base(parsedValues)
        {
        }

        public int Id { get; set; }

        public int SiteId { get; set; }

        public int ArticlesGroupId { get; set; }

        public string[] LinksToArticles
        {
            get
            {
                ParsedValue result;
                return parsedValues.TryGetValue("LinksToArticles", out result) ? result.Values : null;
            }
            set { parsedValues["LinksToArticles"] = ParsedValue.Create(value); }
        }
    }
}
