﻿using WebParser.Application.Interfaces.Services.Parsing.ArticleFieldsRecognizing;

namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class ParsedArticle : ParsedResult
    {
        public ParsedArticle(ParsedValue[] values) : base(values)
        {
        }

        public int Id { get; set; }

        public string[] FoundArticleLinks { get; set; }

        public bool InBlackList { get; set; }

        public RecognizedArticleFieldsData RecognizedFieldsData { get; set; }
    }
}
