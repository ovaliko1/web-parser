﻿namespace WebParser.Application.Interfaces.Services.Parsing.ParsedResult
{
    public class ViewerFilter
    {
        public int? CityId { get; set; }

        public int[] ArticleGroupIds { get; set; }

        public int[] MetroStationIds { get; set; }
    }
}