﻿namespace WebParser.Application.Interfaces.Services.Parsing.ParsedContent
{
    public class ArticleIdentity
    {
        public ArticleIdentity()
        {
        }

        public ArticleIdentity(string identity, int identityConfidencePercentage)
        {
            Identity = identity;
            IdentityConfidencePercentage = identityConfidencePercentage;
        }

        public string Identity { get; set; }

        public int IdentityConfidencePercentage { get; set; }
    }
}