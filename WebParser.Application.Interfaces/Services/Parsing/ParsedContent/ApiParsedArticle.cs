﻿using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;

namespace WebParser.Application.Interfaces.Services.Parsing.ParsedContent
{
    public class ApiParsedArticle
    {
        public int Id { get; set; }

        public int? SiteId { get; set; }

        public int? ArticlesGroupId { get; set; }

        public System.DateTime ParsedDate { get; set; }

        public string Link { get; set; }

        public ParsedValue[] ParsedValues { get; set; }
    }
}