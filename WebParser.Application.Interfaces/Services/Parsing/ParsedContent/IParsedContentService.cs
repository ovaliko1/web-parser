﻿using System;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;

namespace WebParser.Application.Interfaces.Services.Parsing.ParsedContent
{
    public interface IParsedContentService
    {
        Uri[] GetNotParsedArticleLinks(int siteId, int articlesGroupId, Uri[] links);

        void SaveParsedArticle(ParsedArticleResult parsedArticle, int userId, int siteId, int articlesGroupId);

        void SaveParsedArticle(ParsedArticleResult parsedArticle, int userId, int siteId, int articlesGroupId, int? whiteListItemId);

        void SaveFailedParsedArticle(ParsedArticleResult parsedArticle, int userId, int siteId, int articlesGroupId, string parsingFailedReason);

        PagingResult<ApiParsedArticle> GetArticles(int articlesGroupId, int page, int pageSize, System.DateTime? from);

        PagingResult<ViewerParsedArticleItem> ReadViewerArticles<T>(ViewerFilter viewerFilter, RequestFilters<T> filters);

        ViewerParsedArticle GetViewerParsedArticle(int parsedArticleId);
    }
}
