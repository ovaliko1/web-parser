Import-Module WebAdministration

function Main(
[string] $siteName = $(Throw "Value cannot be null: siteName"),
[string] $sitePath = $(Throw "Value cannot be null: sitePath"),
[string] $servicePath = $(Throw "Value cannot be null: sitePath"),
[string] $serviceName = $(Throw "Value cannot be null: sitePath"),
[string] $buildConfiguration = $(Throw "Value cannot be null: sitePath"))
{
    Write-Host "------------------------------------------------------------------------------"
    $rootDirectory = (get-item $MyInvocation.ScriptName).Directory.parent
    Write-Host "Root Directory: $($rootDirectory.FullName)`r`n"

    #Set-Location $rootDirectory.FullName
    #Set-Location .\WebParser.Web

    #$sourcePath = (Get-Location).Path
    #Write-Host "Directory of source files to copy: $($sourcePath)`r`n"
    
    #StopSite $siteName
    #CopyFiles $sourcePath $sitePath
    #StartSite $siteName

    Write-Host "------------------------------------------------------------------------------"

    Set-Location $rootDirectory.FullName
    Set-Location .\WebParser.WindowsService

    $serviceBinPath = Join-Path (Get-Location).Path "\bin\"
    $serviceBinPath = Join-Path $serviceBinPath $buildConfiguration
    Write-Host "Directory of Windows Service : $($serviceBinPath )`r`n"

    StopWindowsService $serviceName

    Start-Sleep -s 60

    KillPhantomJSProcesses
    KillSlimerJSProcesses

    CopyFiles $serviceBinPath $servicePath
    StartWindowsService $serviceName
}

function StopSite(
    [string] $siteName = $(Throw "Value cannot be null: siteName"))
{
    $site = Get-Item ("IIS:\Sites\" + $siteName) -EA 0

    If ($site -ne $null -and $site.Name -eq $siteName)
    {
        Write-Host "Stopping website ($siteName)...`r`n"
        Stop-Website -Name $siteName
        Write-Host "Successfully stopped website ($siteName).`r`n"
    }
}

function StartSite(
    [string] $siteName = $(Throw "Value cannot be null: siteName"))
{
    $site = Get-Item ("IIS:\Sites\" + $siteName) -EA 0

    If ($site -ne $null -and $site.Name -eq $siteName)
    {
        Write-Host "Starting website ($siteName)...`r`n"
        Start-Website -Name $siteName
        Write-Host "Successfully started website ($siteName).`r`n"
    }
}

function KillPhantomJSProcesses()
{ 
    Write-Host "Try to kill PhantomJS processes...`r`n"
    
    $phantomJSProcesses = Get-Process PhantomJS -ErrorAction SilentlyContinue
    if($phantomJSProcesses)
    {
        Write-Host "PhantomJS processes count: $($phantomJSProcesses.Length)...`r`n"
        
        $phantomJSProcesses.Kill();

        Write-Host "PhantomJS processes successfully killed...`r`n"
    }
    else
    {
        Write-Host "No one running PhantomJS processes...`r`n"
    }
}

function KillSlimerJSProcesses()
{ 
    Write-Host "Try to kill SlimerJS processes...`r`n"
    
    $slimerJSProcesses = Get-Process firefox -ErrorAction SilentlyContinue
    if($slimerJSProcesses)
    {
        Write-Host "SlimerJS processes count: $($slimerJSProcesses.Length)...`r`n"
        
        $slimerJSProcesses.Kill();

        Write-Host "SlimerJS processes successfully killed...`r`n"
    }
    else
    {
        Write-Host "No one running SlimerJS processes...`r`n"
    }
}


function StopWindowsService(
    [string] $serviceName = $(Throw "Value cannot be null: siteName"))
{
    $serviceBefore = Get-Service $serviceName
    Write-Host "($serviceName) is now ($($serviceBefore.status))...`r`n"
    
    Write-Host "Stopping Windows Service ($serviceName)...`r`n"
    Stop-Service $serviceName
    Write-Host "Successfully stopped Windows Service ($serviceName)...`r`n"

    $serviceAfter = Get-Service $serviceName
    Write-Host "($serviceName) is now ($($serviceAfter.status))...`r`n"
}

function StartWindowsService(
    [string] $serviceName = $(Throw "Value cannot be null: siteName"))
{
    $serviceBefore = Get-Service $serviceName
    Write-Host "($serviceName) is now ($($serviceBefore.status))...`r`n"
    
    Write-Host "Starting Windows Service ($serviceName)...`r`n"
    Start-Service $serviceName
    Write-Host "Successfully started Windows Service ($serviceName)...`r`n"

    $serviceAfter = Get-Service $serviceName
    Write-Host "($serviceName) is now ($($serviceAfter.status))...`r`n"
}

function RemoveFiles(
    [string] $path = $(Throw "Value cannot be null: sitePath"))
{
    If (Test-Path $path)
    {
        Write-Host "Removing folder ($path)...`r`n"
        Remove-Item -Recurse -Force "$($path)*"
        Write-Host "Successfully removed website folder ($path)...`r`n"        
    }
}

function CopyFiles(
    [string] $sourcePath = $(Throw "Value cannot be null: sitePath"),
    [string] $destinationPath = $(Throw "Value cannot be null: sitePath"),
	[bool]$writeLog = $true)
{
	If ($writeLog)
	{
		Write-Host "Copy files from ($sourcePath) to folder ($destinationPath)...`r`n"
	}
    
    If ((Test-Path $sourcePath))
    {
        If (!(Test-Path $destinationPath)) 
		{
            New-Item -Path $destinationPath -ItemType Directory
        }

		$files = Get-ChildItem -Path "$($sourcePath)" -File
		foreach($file in $Files)
		{
			Copy-Item "$($sourcePath + "\" + $file.Name)" -Destination "$($destinationPath + "\" +$file.Name)" -Force
		}

		$subDirectories = Get-ChildItem -Path "$($sourcePath)" -Directory
		foreach($subDirectory in $subDirectories)
		{
			CopyFiles "$($subDirectory.FullName)" "$($destinationPath + "\" + $subDirectory.Name)" $false
		}

        If ($writeLog)
		{
			Write-Host "Successfully copied files from ($sourcePath).`r`n"
		}
    }
}

$siteName = "webparser.test"
$sitePath = "c:\Projects\WebParser\Test\Web\"
$servicePath = "C:\Projects\WebParser\Test\WindowsService\bin\"
$serviceName = "WebParser Service"
$buildConfiguration = "ReleaseTest"

Main $siteName $sitePath $servicePath $serviceName $buildConfiguration