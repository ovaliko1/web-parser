﻿using System;
using log4net;
using Quartz;
using WebParser.Application.Interfaces.Services.MarkingJob;
using WebParser.Configuration;

namespace WebParser.WindowsService.Jobs
{
    public class BlackListArticleMarkingJob : IJob
    {
        private static readonly ILog SchedulerLog = LogManager.GetLogger("schedulerLog");

        public static readonly string Key = "BlackListArticleMarkingJob";
        public static readonly string GroupName = "BlackListArticleMarkingGroup";
        public static readonly string TriggerKey = "BlackListArticleMarkingTrigger";

        public static readonly int IntervalInSeconds = 60*60*6;

        public BlackListArticleMarkingJob()
        {
            BlackListJob = ContainerConfig.Resolve<IMarkingJobService>();
        }

        private IMarkingJobService BlackListJob { get; }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var result = BlackListJob.RunBlackListArticleMarking();

                var logMessage = $"BlackListArticleMarkingJob: BLACKLIST ARTICLE MARKING result: {result.Success}, log: {result.Log}";
                SchedulerLog.Info(logMessage);
#if DEBUG
                Console.WriteLine(logMessage);
                Console.WriteLine();
#endif
            }
            catch (Exception ex)
            {
                SchedulerLog.Error("BlackListArticleMarkingJob exception", ex);
            }
        }
    }
}