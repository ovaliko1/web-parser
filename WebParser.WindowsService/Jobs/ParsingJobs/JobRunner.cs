﻿using System;
using System.Reflection;
using log4net;
using Quartz;
using WebParser.Application.Interfaces.Services.Parsing;
using WebParser.Configuration;

namespace WebParser.WindowsService.Jobs.ParsingJobs
{
    [DisallowConcurrentExecution]
    public class JobRunner : IJob
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IParsingJob ParsingJob { get; }

        public JobRunner()
        {
            ParsingJob = ContainerConfig.Resolve<IParsingJob>();
        }
        public void Execute(IJobExecutionContext context)
        {

            try
            {
                var siteId = context.JobDetail.JobDataMap.GetIntValue("siteId");
                if (siteId <= 0)
                {
                    Log.Error($"JobRunner: site id: {siteId}");
                    return;
                }

                var result = ParsingJob.Run(siteId).Result;
                Log.Info($"JobRunner result: {result}");
            }
            catch (Exception ex)
            {
                Log.Error("JobRunner exception", ex);
            }
        }
    }
}
