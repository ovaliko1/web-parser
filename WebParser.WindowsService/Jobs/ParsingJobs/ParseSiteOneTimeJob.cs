﻿using System;
using System.Reflection;
using log4net;
using Quartz;
using WebParser.WindowsService.DomainEntities;

namespace WebParser.WindowsService.Jobs.ParsingJobs
{
    public class ParseSiteOneTimeJob : IJob
    {
        private const string JobKeyFormat = "{0}_{1}";
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog SchedulerLog = LogManager.GetLogger("schedulerLog");

        public static readonly string Key = "parseSiteOneTimeJob";
        public static readonly string GroupName = "ParseSiteOneTimeGroup";
        public static readonly string TriggerKey = "ParseSiteOneTimeTrigger";

        public static readonly int IntervalInSeconds = 60;

        private SchedulerService SchedulerService { get; } = new SchedulerService();

        public void Execute(IJobExecutionContext context)
        {
            var scheduler = context.JobDetail.JobDataMap["Scheduler"] as IScheduler;
            if (scheduler == null)
            {
                throw new InvalidOperationException("Scheduler is null");
            }

            var scheduleData = SchedulerService.GetOneTimeParsedSites();

            foreach (var job in scheduleData)
            {
                scheduler.ScheduleJob(job.JobDetail, job.Trigger);

                LogTrigger(job);
            }
        }

        private void LogTrigger(OneTimeParsedSiteJobDetails job)
        {
            var triggerName = SchedulerService.GetTriggerName(job.JobType);

            var log = $"ParseSiteOneTimeJob: {triggerName} SiteName: {job.SiteName} SiteId {job.SiteId}";

            SchedulerLog.Debug(log);

            #if DEBUG
            Console.WriteLine(log);
            Console.WriteLine();
            #endif
        }
    }
}
