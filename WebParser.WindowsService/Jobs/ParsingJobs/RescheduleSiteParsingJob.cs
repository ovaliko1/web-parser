﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using Quartz;
using Quartz.Impl.Matchers;
using Quartz.Impl.Triggers;
using Quartz.Spi;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Common.Extensions;
using WebParser.Configuration;
using WebParser.WindowsService.DomainEntities;

namespace WebParser.WindowsService.Jobs.ParsingJobs
{
    [DisallowConcurrentExecution]
    public class RescheduleSiteParsingJob : IJob
    {
        private const string JobKeyFormat = "{0}_{1}";
        private static readonly ILog SchedulerLog = LogManager.GetLogger("schedulerLog");

        public static readonly string Key = "rescheduleJob";
        public static readonly string GroupName = "RescheduleJobGroup";
        public static readonly string TriggerKey = "rescheduleTrigger";

        public static readonly int IntervalInSeconds = 60;

        private IDateTimeService DateTimeService { get; }

        private SchedulerService SchedulerService { get; } = new SchedulerService();

        public RescheduleSiteParsingJob()
        {
            DateTimeService = ContainerConfig.Resolve<IDateTimeService>();
        }

        public void Execute(IJobExecutionContext context)
        {
            var scheduler = context.JobDetail.JobDataMap["Scheduler"] as IScheduler;
            if (scheduler == null)
            {
                throw new InvalidOperationException("Scheduler is null");
            }

            var scheduleData = SchedulerService.GetDailyParsedSites();

            var jobKeys = scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(SchedulerService.DailySiteParsingGroupName));

            var currentJobs = jobKeys.Select(x => scheduler.GetJobDetail(x)).ToDictionary(ToJobKey, x => x);

            RescheduleOldJobs(scheduler, currentJobs, scheduleData);
            ScheduleNewJobs(scheduler, currentJobs, scheduleData);
            DeleteDisabledJobs(scheduler, currentJobs, scheduleData);
        }
        private static string ToJobKey(IJobDetail x)
        {
            return string.Format(JobKeyFormat, x.Key.Name, x.Key.Group);
        }

        private void RescheduleOldJobs(IScheduler scheduler, Dictionary<string, IJobDetail> currentJobs, DailyParsedSiteJobDetails[] scheduleData)
        {
            var alreadyScheduledJobs = scheduleData
                .Where(x => currentJobs.ContainsKey(ToJobKey(x.JobDetail)))
                .ToArray();

            foreach (var scheduledJob in alreadyScheduledJobs)
            {
                var scheduleDataItem = currentJobs[ToJobKey(scheduledJob.JobDetail)];
                var oldTrigger = (DailyTimeIntervalTriggerImpl)scheduler.GetTriggersOfJob(scheduleDataItem.Key).First();

                if (TriggerIsNotChanged(oldTrigger, scheduledJob))
                {
                    LogTriggerUpdate(scheduledJob, oldTrigger, oldTrigger, true);
                    continue;
                }

                var triggerBuilder = oldTrigger.GetTriggerBuilder();

                var newTrigger = triggerBuilder
                    .WithDailyTimeIntervalSchedule(x =>
                        x.OnEveryDay()
                         .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(scheduledJob.SchedulerStartHours, scheduledJob.SchedulerStartMinutes))
                         .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(scheduledJob.SchedulerEndHours, scheduledJob.SchedulerEndMinutes))
                         .WithIntervalInMinutes(scheduledJob.RepeatIntervalInMunutes))
                    .Build();

                scheduler.RescheduleJob(scheduledJob.Trigger.Key, newTrigger);

                LogTriggerUpdate(scheduledJob, newTrigger, oldTrigger, false);
            }
        }

        private static bool TriggerIsNotChanged(DailyTimeIntervalTriggerImpl oldTrigger, DailyParsedSiteJobDetails scheduledJob)
        {
            return oldTrigger.StartTimeOfDay.Hour == scheduledJob.SchedulerStartHours &&
                   oldTrigger.StartTimeOfDay.Minute == scheduledJob.SchedulerStartMinutes &&
                   oldTrigger.EndTimeOfDay.Hour == scheduledJob.SchedulerEndHours &&
                   oldTrigger.EndTimeOfDay.Minute == scheduledJob.SchedulerEndMinutes &&
                   oldTrigger.RepeatInterval == scheduledJob.RepeatIntervalInMunutes;
        }

        private static void DeleteDisabledJobs(IScheduler scheduler, Dictionary<string, IJobDetail> currentJobs, DailyParsedSiteJobDetails[] scheduleData)
        {
            var enabledJobKeys = new HashSet<string>(scheduleData.Select(x => ToJobKey(x.JobDetail)));
            var jobsToDelete = currentJobs.Where(x => !enabledJobKeys.Contains(x.Key)).Select(x => x.Value.Key).ToList();

            if (jobsToDelete.Count > 0)
            {
                scheduler.DeleteJobs(jobsToDelete);

                LogTriggerDelete(jobsToDelete);
            }
        }

        private void ScheduleNewJobs(IScheduler scheduler, Dictionary<string, IJobDetail> currentJobs, DailyParsedSiteJobDetails[] scheduleData)
        {
            var newJobs = scheduleData
                .Where(x => !currentJobs.ContainsKey(string.Format(JobKeyFormat, x.JobDetail.Key.Name, x.JobDetail.Key.Group)))
                .ToArray();

            foreach (var job in newJobs)
            {
                scheduler.ScheduleJob(job.JobDetail, job.Trigger);
                LogTriggerNew(job);
            }
        }

        private void LogTriggerNew(DailyParsedSiteJobDetails job)
        {
            var newTimes = TriggerUtils.ComputeFireTimes(job.Trigger as IOperableTrigger, null, 3);

            var triggerName = SchedulerService.GetTriggerName(job.JobType);

            var newTimesMessage = $"RescheduleJob: {triggerName} NEW JOB: SiteName: {job.SiteName} SiteId {job.SiteId} Times: {string.Join("; ", newTimes.ToArray(x => x.ToString("dd-MM-yy HH:mm:ss")))};";
            SchedulerLog.Debug(newTimesMessage);

            #if DEBUG
            Console.WriteLine(newTimesMessage);
            Console.WriteLine();
            #endif
        }

        private void LogTriggerUpdate(DailyParsedSiteJobDetails job, ITrigger newTrigger, ITrigger oldTrigger, bool notChanged)
        {
            var newTimes = TriggerUtils.ComputeFireTimes(newTrigger as IOperableTrigger, null, 3);
            var oldTimes = TriggerUtils.ComputeFireTimes(oldTrigger as IOperableTrigger, null, 3);

            var triggerName = SchedulerService.GetTriggerName(job.JobType);
            var notChangedMessage = notChanged ? "Not Changed" : string.Empty;

            var newTimesMessage = $"RescheduleJob: {triggerName} UPDATED JOB: SiteName: {job.SiteName} SiteId {job.SiteId} {notChangedMessage} Times: {string.Join("; ", newTimes.ToArray(x => x.ToString("dd-MM-yy HH:mm:ss")))}; " + $"Old Firing Times: {string.Join("; ", oldTimes.ToArray(x => x.ToString("dd-MM-yy HH:mm:ss")))};";
            SchedulerLog.Debug(newTimesMessage);

#if DEBUG
            Console.WriteLine(newTimesMessage);
            Console.WriteLine();
#endif
        }

        private static void LogTriggerDelete(List<JobKey> jobsToDelete)
        {
            var log = $"RescheduleJob: DELETED JOBS: Job names: {string.Join(", ", jobsToDelete.Select(x => x.Name))}";

            SchedulerLog.Debug(log);

#if DEBUG
            Console.WriteLine(log);
            Console.WriteLine();
#endif
        }
    }
}
