﻿using System;
using log4net;
using Quartz;
using WebParser.Application.Interfaces.Services.ExpiredArticlesJob;
using WebParser.Configuration;
using WebParser.WindowsService.DomainEntities;

namespace WebParser.WindowsService.Jobs
{
    internal class ExpiredArticlesJob : IJob
    {
        private static readonly ILog SchedulerLog = LogManager.GetLogger("schedulerLog");

        public static readonly string Key = "ExpiredArticlesJob";
        public static readonly string GroupName = "ExpiredArticlesGroup";
        public static readonly string TriggerKey = "ExpiredArticlesTrigger";

        public static readonly int IntervalInSeconds = 60 * 60 * 24;

        public static readonly JobRunWindow RunWindow = new JobRunWindow(0, 1, 3, 1);

        private IExpiredArticlesJobService ExpiredArticlesJobService { get; }

        public ExpiredArticlesJob()
        {
            ExpiredArticlesJobService = ContainerConfig.Resolve<IExpiredArticlesJobService>();
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var result = ExpiredArticlesJobService.DeleteExpiredArticles().GetAwaiter().GetResult();

                var log = $"ExpiredArticlesJob result: {result.Log}";
                SchedulerLog.Info(log);
#if DEBUG
                Console.WriteLine(log);
                Console.WriteLine();
#endif
            }
            catch (Exception ex)
            {
                SchedulerLog.Error("ExpiredArticlesJob exception", ex);
            }
        }
    }
}