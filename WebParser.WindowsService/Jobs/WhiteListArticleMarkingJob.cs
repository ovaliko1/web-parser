﻿using System;
using log4net;
using Quartz;
using WebParser.Application.Interfaces.Services.MarkingJob;
using WebParser.Configuration;

namespace WebParser.WindowsService.Jobs
{
    public class WhiteListArticleMarkingJob : IJob
    {
        private static readonly ILog SchedulerLog = LogManager.GetLogger("schedulerLog");

        public static readonly string Key = "WhiteListArticleMarkingJob";
        public static readonly string GroupName = "WhiteListArticleMarkingGroup";
        public static readonly string TriggerKey = "WhiteListArticleMarkingTrigger";

        public static readonly int IntervalInSeconds = 60 * 5;

        private IMarkingJobService WhiteListJob { get; }

        public WhiteListArticleMarkingJob()
        {
            WhiteListJob = ContainerConfig.Resolve<IMarkingJobService>();
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var result = WhiteListJob.RunWhiteListArticleMarking();

                var logMessage = $"WhiteListArticleMarkingJob: WHITELIST ARTICLE MARKING result: {result.Success}, log: {result.Log}";
                SchedulerLog.Info(logMessage);
#if DEBUG
                Console.WriteLine(logMessage);
                Console.WriteLine();
#endif
            }
            catch (Exception ex)
            {
                SchedulerLog.Error("WhiteListArticleMarkingJob exception", ex);
            }
        }
    }
}
