﻿using System;
using System.Reflection;
using log4net;
using Quartz;
using WebParser.Application.Interfaces.Services.Ping;
using WebParser.Configuration;

namespace WebParser.WindowsService.Jobs
{
    public class PingJob : IJob
    {
        private const string JobKeyFormat = "{0}_{1}";
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static readonly string Key = "pingJob";
        public static readonly string GroupName = "PingJobGroup";
        public static readonly string TriggerKey = "PingJobTrigger";

        public static readonly int IntervalInSeconds = 60;

        private IPingService PingService { get; }

        public PingJob()
        {
            PingService = ContainerConfig.Resolve<IPingService>();
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var result = PingService.Ping().Result;

                var log = $"Ping result: {result}";
                Log.Info(log);
                #if DEBUG
                Console.WriteLine(log);
                Console.WriteLine();
                #endif
            }
            catch (Exception ex)
            {
                Log.Error("Ping Job exception", ex);
            }
        }
    }
}
