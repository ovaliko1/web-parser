﻿using System;
using log4net;
using Quartz;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.SocialMedia;
using WebParser.Configuration;

namespace WebParser.WindowsService.Jobs
{
    public class SocialMediaJob : IJob
    {
        private const string JobKeyFormat = "{0}_{1}";
        private static readonly ILog SchedulerLog = LogManager.GetLogger("schedulerLog");

        public static readonly string Key = "socialMediaJob";
        public static readonly string GroupName = "socialMediaGroup";
        public static readonly string TriggerKey = "socialMediaTrigger";

        public static readonly int IntervalInSeconds = 60 * 60 * 3;

        private ISocialMediaService SocialMediaService { get; }

        private IDateTimeService DateTimeService { get; }

        public SocialMediaJob()
        {
            SocialMediaService = ContainerConfig.Resolve<ISocialMediaService>();
            DateTimeService = ContainerConfig.Resolve<IDateTimeService>();
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var previousRunTime = DateTimeService.GetCurrentDateTimeUtc().AddSeconds(-IntervalInSeconds);
                var result = SocialMediaService.RunExportParsingResultToSocialMedia(previousRunTime).Result;

                var log = $"Social Media sync result: {result}";
                SchedulerLog.Info(log);
#if DEBUG
                Console.WriteLine(log);
                Console.WriteLine();
#endif
            }
            catch (Exception ex)
            {
                SchedulerLog.Error("Social Media sync exception", ex);
            }
        }
    }
}
