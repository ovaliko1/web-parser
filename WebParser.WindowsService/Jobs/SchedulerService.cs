﻿using System;
using System.Collections.Generic;
using Common.Logging;
using Quartz;
using Quartz.Spi;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Configuration;
using WebParser.WindowsService.DomainEntities;
using WebParser.WindowsService.Jobs.ParsingJobs;

namespace WebParser.WindowsService.Jobs
{
    public class SchedulerService
    {
        private static readonly ILog SchedulerLog = LogManager.GetLogger("schedulerLog");

        private class Job
        {
            public string Name { get; set; }

            public Action<IScheduler> RunAction { get; set; }
        }

        private const string JobDetailIdentityFormat = "{0}job";

        private const string TriggerDetailIdentityFormat = "{0}trigger";

        public static readonly string DailySiteParsingGroupName = "DailySiteParsingGroupName";

        public static readonly string OneTimeSiteParsingGroupName = "OneTimeSiteParsingGroupName";

        private ISiteService SiteService { get; }

        public SchedulerService()
        {
            SiteService = ContainerConfig.Resolve<ISiteService>();
        }

        private static readonly Dictionary<int, Job> Jobs = new Dictionary<int, Job>
        {
            [1] = new Job { Name = "RescheduleSiteParsing", RunAction = ScheduleRescheduleSiteParsingJob },
            [2] = new Job { Name = "ParseSiteOneTime", RunAction = ScheduleParseSiteOneTimeJob },
            [3] = new Job { Name = "Ping", RunAction = SchedulePingJob },
            [4] = new Job { Name = "SocialMediaJob", RunAction = ScheduleSocialMediaJob },
            [5] = new Job { Name = "BlackListArticleMarkingJob", RunAction = ScheduleBlackListArticleMarkingJob },
            [6] = new Job { Name = "WhiteListArticleMarkingJob", RunAction = ScheduleWhiteListArticleMarkingJob },
            [7] = new Job { Name = "ExpiredArticlesJob ", RunAction = ScheduleExpiredArticlesJob },
            [8] = new Job { Name = "PushNotificationsJob ", RunAction = SchedulePushNotificationsJob },
        };

        public JobDetails[] GetJobs()
        {
            return Jobs.ToArray(x => new JobDetails
            {
                JobKey = x.Key,
                JobName = x.Value.Name,
            });
        }

        public bool RunJobByKey(int jobKey, IScheduler scheduler)
        {
            if (!Jobs.ContainsKey(jobKey))
            {
                return false;
            }

            Jobs[jobKey].RunAction(scheduler);
            return true;
        }

        public void RunAllJobs(IScheduler scheduler)
        {
            foreach (var job in Jobs)
            {
                job.Value.RunAction(scheduler);
            }
        }

        private static void ScheduleRescheduleSiteParsingJob(IScheduler scheduler)
        {
            ScheduleJob<RescheduleSiteParsingJob>(scheduler, RescheduleSiteParsingJob.Key, RescheduleSiteParsingJob.GroupName, RescheduleSiteParsingJob.TriggerKey, RescheduleSiteParsingJob.IntervalInSeconds);
        }

        private static void ScheduleParseSiteOneTimeJob(IScheduler scheduler)
        {
            ScheduleJob<ParseSiteOneTimeJob>(scheduler, ParseSiteOneTimeJob.Key, ParseSiteOneTimeJob.GroupName, ParseSiteOneTimeJob.TriggerKey, ParseSiteOneTimeJob.IntervalInSeconds);
        }

        private static void SchedulePingJob(IScheduler scheduler)
        {
            ScheduleJob<PingJob>(scheduler, PingJob.Key, PingJob.GroupName, PingJob.TriggerKey, PingJob.IntervalInSeconds);
        }

        private static void ScheduleSocialMediaJob(IScheduler scheduler)
        {
            ScheduleJob<SocialMediaJob>(scheduler, SocialMediaJob.Key, SocialMediaJob.GroupName, SocialMediaJob.TriggerKey, SocialMediaJob.IntervalInSeconds);
        }

        private static void ScheduleBlackListArticleMarkingJob(IScheduler scheduler)
        {
            ScheduleJob<BlackListArticleMarkingJob>(scheduler, BlackListArticleMarkingJob.Key, BlackListArticleMarkingJob.GroupName, BlackListArticleMarkingJob.TriggerKey, BlackListArticleMarkingJob.IntervalInSeconds);
        }

        private static void ScheduleWhiteListArticleMarkingJob(IScheduler scheduler)
        {
            ScheduleJob<WhiteListArticleMarkingJob>(scheduler, WhiteListArticleMarkingJob.Key, WhiteListArticleMarkingJob.GroupName, WhiteListArticleMarkingJob.TriggerKey, WhiteListArticleMarkingJob.IntervalInSeconds);
        }

        private static void ScheduleExpiredArticlesJob(IScheduler scheduler)
        {
            ScheduleJob<ExpiredArticlesJob>(scheduler, ExpiredArticlesJob.Key, ExpiredArticlesJob.GroupName, ExpiredArticlesJob.TriggerKey, ExpiredArticlesJob.IntervalInSeconds, ExpiredArticlesJob.RunWindow);
        }

        private static void SchedulePushNotificationsJob(IScheduler scheduler)
        {
            ScheduleJob<PushNotificationsJob>(scheduler, PushNotificationsJob.Key, PushNotificationsJob.GroupName, PushNotificationsJob.TriggerKey, PushNotificationsJob.IntervalInSeconds, PushNotificationsJob.RunWindow);
        }

        private static void ScheduleJob<T>(
            IScheduler scheduler,
            string key,
            string groupName,
            string triggerKey,
            int intervalInSeconds,
            JobRunWindow runWindow = null) where T : IJob
        {
            IJobDetail job = JobBuilder.Create<T>()
                .WithIdentity(key, groupName)
                .SetJobData(new JobDataMap
                {
                    {"Scheduler", scheduler},
                })
                .Build();

            ITrigger trigger;
            if (runWindow == null)
            {
                trigger = TriggerBuilder.Create()
                    .WithIdentity(triggerKey, groupName)
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(intervalInSeconds)
                        .RepeatForever())
                    .Build();
            }
            else
            {
                trigger = TriggerBuilder.Create()
                    .WithIdentity(triggerKey, groupName)
                    .WithDailyTimeIntervalSchedule(x => SetupTriggerBuilder(x, intervalInSeconds, runWindow))
                    .Build();

                var newTimes = TriggerUtils.ComputeFireTimes(trigger as IOperableTrigger, null, 3);

                var runTimesMessage = $"{groupName}_{triggerKey} scheduled: SiteName: Times: {string.Join("; ", newTimes.ToArray(x => x.ToString("dd-MM-yy HH:mm:ss")))};";
                SchedulerLog.Debug(runTimesMessage);

#if DEBUG
                Console.WriteLine(runTimesMessage);
                Console.WriteLine();
#endif
            }

            scheduler.ScheduleJob(job, trigger);
        }

        public DailyParsedSiteJobDetails[] GetDailyParsedSites()
        {
            var sites = SiteService.GetDailyParsedSites();
            var result = sites.ToArray(x =>
            {
                IJobDetail jobDetail = JobBuilder.Create<JobRunner>()
                    .WithIdentity(string.Format(JobDetailIdentityFormat, x.Id), DailySiteParsingGroupName)
                    .SetJobData(new JobDataMap
                    {
                        { "siteId", x.Id }
                    })
                    .Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity(string.Format(TriggerDetailIdentityFormat, x.Id), DailySiteParsingGroupName)
                    .WithDailyTimeIntervalSchedule(
                        y =>
                            SetupTriggerBuilder(
                                y,
                                x.RepeatIntervalInMunutes * 60,
                                new JobRunWindow(x.SchedulerStartHours, x.SchedulerStartMinutes, x.SchedulerEndHours, x.SchedulerEndMinutes)))
                    .Build();

                return new DailyParsedSiteJobDetails
                {
                    SiteId = x.Id,
                    SiteName = x.Name,
                    JobDetail = jobDetail,
                    Trigger = trigger,
                    SchedulerStartHours = x.SchedulerStartHours,
                    SchedulerStartMinutes = x.SchedulerStartMinutes,
                    SchedulerEndHours = x.SchedulerEndHours,
                    SchedulerEndMinutes = x.SchedulerEndMinutes,
                    RepeatIntervalInMunutes = x.RepeatIntervalInMunutes,
                    JobType = x.JobType,
                };
            });

            return result;
        }

        private static void SetupTriggerBuilder(
            DailyTimeIntervalScheduleBuilder builder,
            int intervalInSeconds,
            JobRunWindow jobRunWindow)
        {
            builder = builder
                .OnEveryDay()
                .WithIntervalInSeconds(intervalInSeconds);

            builder
                .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(jobRunWindow.StartHours, jobRunWindow.StartMinutes))
                .EndingDailyAt(TimeOfDay.HourAndMinuteOfDay(jobRunWindow.EndHours, jobRunWindow.EndMinutes));
        }

        public OneTimeParsedSiteJobDetails[] GetOneTimeParsedSites()
        {
            var sites = SiteService.GetOneTimeParsedSites();
            var result = sites.ToArray(x =>
            {
                IJobDetail jobDetail = JobBuilder.Create<JobRunner>()
                    .WithIdentity(string.Format(JobDetailIdentityFormat, x.Id), OneTimeSiteParsingGroupName)
                    .SetJobData(new JobDataMap
                    {
                        { "siteId", x.Id }
                    })
                    .Build();

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity(string.Format(TriggerDetailIdentityFormat, x.Id), OneTimeSiteParsingGroupName)
                    .StartNow()
                    .Build();

                return new OneTimeParsedSiteJobDetails
                {
                    SiteId = x.Id,
                    SiteName = x.Name,
                    JobDetail = jobDetail,
                    Trigger = trigger,
                    JobType = x.JobType,
                };
            });
            return result;
        }

        public string GetTriggerName(JobType jobType)
        {
            switch (jobType)
            {
                case JobType.Parsing:
                    return "PARSING SITE TRIGGER";

                case JobType.BlackList:
                    return "BLACK LIST SITE TRIGGER";

                case JobType.WhiteList:
                    return "WHITE LIST SITE TRIGGER";

                case JobType.ParsingAndWhiteList:
                    return "PARSING AND WHITELIST SITE TRIGGER";

                default:
                    throw new ArgumentOutOfRangeException(nameof(jobType), jobType, null);
            }
        }
    }
}
