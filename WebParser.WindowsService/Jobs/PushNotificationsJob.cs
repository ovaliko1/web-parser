﻿using System;
using System.Reflection;
using log4net;
using Quartz;
using WebParser.Application.Interfaces.Services.PushNotifications;
using WebParser.Configuration;
using WebParser.WindowsService.DomainEntities;

namespace WebParser.WindowsService.Jobs
{
    internal class PushNotificationsJob : IJob
    {
        private const string JobKeyFormat = "{0}_{1}";
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static readonly string Key = "pushNotificationsJob";
        public static readonly string GroupName = "PushNotificationsJobGroup";
        public static readonly string TriggerKey = "PushNotificationsJobTrigger";

        public static readonly int IntervalInSeconds = 5 * 60;

        public static readonly JobRunWindow RunWindow = new JobRunWindow(5, 0, 23, 30);

        private IPushNotificationsJob Job { get; }

        public PushNotificationsJob()
        {
            Job = ContainerConfig.Resolve<IPushNotificationsJob>();
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var result = Job.RunPing().Result;

                var log = $"Push Notification Ping result: {result}";
                Log.Info(log);
                #if DEBUG
                Console.WriteLine(log);
                Console.WriteLine();
                #endif
            }
            catch (Exception ex)
            {
                Log.Error("Push Notification Ping Job exception", ex);
            }
        }
    }
}
