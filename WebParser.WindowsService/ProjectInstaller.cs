﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;

namespace WebParser.WindowsService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private readonly ServiceProcessInstaller serviceProcessInstaller;
        private readonly ServiceInstaller serviceInstaller;

        public ProjectInstaller()
        {
            serviceProcessInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();

            var serviceName = GetServiceName();
            serviceInstaller.ServiceName = serviceName;
            serviceInstaller.DisplayName = serviceName;
            serviceInstaller.Description = "Running Scheduled Tasks";

            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Password = null;
            serviceProcessInstaller.Username = null;

            serviceInstaller.StartType = ServiceStartMode.Automatic;

            Installers.AddRange(new Installer[] { serviceProcessInstaller, serviceInstaller });
        }

        // Required during installation procedure because InstallUtil config file is default exe config there
        private static string GetServiceName()
        {
            var assembly = Assembly.GetAssembly(typeof(ProjectInstaller));
            var config = ConfigurationManager.OpenExeConfiguration(assembly.Location);
            var serviceName = config.AppSettings.Settings["ServiceName"].Value;

            if (serviceName == null)
            {
                throw new IndexOutOfRangeException("Settings collection does not contain the requested key: ServiceName");
            }

            return serviceName;
        }
    }
}
