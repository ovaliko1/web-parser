﻿namespace WebParser.WindowsService.DomainEntities
{
    public class JobDetails
    {
        public int JobKey { get; set; }

        public string JobName { get; set; }
    }
}
