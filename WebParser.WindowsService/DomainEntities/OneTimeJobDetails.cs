﻿using Quartz;
using WebParser.Common;

namespace WebParser.WindowsService.DomainEntities
{
    public class OneTimeParsedSiteJobDetails
    {
        public int SiteId { get; set; }

        public string SiteName { get; set; }

        public IJobDetail JobDetail { get; set; }

        public ITrigger Trigger { get; set; }

        public JobType JobType { get; set; }
    }
}
