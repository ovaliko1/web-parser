﻿namespace WebParser.WindowsService.DomainEntities
{
    public class TriggerData
    {
        public int StartHours { get; set; }

        public int StartMinutes { get; set; }
    }
}
