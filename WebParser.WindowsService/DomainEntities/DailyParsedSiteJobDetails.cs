﻿using Quartz;
using WebParser.Common;

namespace WebParser.WindowsService.DomainEntities
{
    public class DailyParsedSiteJobDetails
    {
        public int SiteId { get; set; }

        public string SiteName { get; set; }

        public int SchedulerStartHours { get; set; }

        public int SchedulerStartMinutes { get; set; }

        public int SchedulerEndHours { get; set; }

        public int SchedulerEndMinutes { get; set; }

        public int RepeatIntervalInMunutes { get; set; }

        public IJobDetail JobDetail { get; set; }

        public ITrigger Trigger { get; set; }

        public JobType JobType { get; set; }
    }
}
