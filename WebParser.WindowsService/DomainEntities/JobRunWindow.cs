﻿namespace WebParser.WindowsService.DomainEntities
{
    internal class JobRunWindow
    {
        public JobRunWindow(int startHours, int startMinutes, int endHours, int endMinutes)
        {
            StartHours = startHours;
            StartMinutes = startMinutes;
            EndHours = endHours;
            EndMinutes = endMinutes;
        }

        public int StartHours { get; }

        public int StartMinutes { get; }

        public int EndHours { get; }

        public int EndMinutes { get; }
    }
}