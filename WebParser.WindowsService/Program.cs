﻿using System;
using System.Reflection;
using System.ServiceProcess;
using log4net;
using WebParser.Configuration;

namespace WebParser.WindowsService
{
    static class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            try
            {
                ContainerConfig.InitContainer();
                var service = new Service();
                
                if (Environment.UserInteractive)
                {
                    Console.WriteLine("#################### Hooray!!! Let's start!!! #############################");

                    service.Start(args);

                    Console.WriteLine("\n- Working started --\n");
                    Console.WriteLine("#################### Press any key to stop program ########################");
                    Console.Read();
                    service.Stop();
                }
                else
                {
                    var servicesToRun = new ServiceBase[] { service };
                    ServiceBase.Run(servicesToRun);
                }

            }
            catch (Exception ex)
            {
                Log.Error("Global exception of Scheduler", ex);
                throw;
            }
        }
    }
}
