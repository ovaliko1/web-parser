﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Reflection;
using System.ServiceProcess;
using log4net;
using Quartz;
using Quartz.Impl;
using WebParser.WindowsService.Jobs;

namespace WebParser.WindowsService
{
    public partial class Service : ServiceBase
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IScheduler Scheduler { get; set; }

        public Service()
        {
            ServiceName = ConfigurationManager.AppSettings["ServiceName"];
        }

        public void Start(string[] args)
        {
            OnStart(args);
        }

        public new void Stop()
        {
            OnStop();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                LoadScheduler();
            }
            catch (Exception ex)
            {
                Log.Error(ServiceName + " failed to start", ex);
            }

            Log.Info(ServiceName + " started");
        }

        private void LoadScheduler()
        {
            Scheduler = StdSchedulerFactory.GetDefaultScheduler();
            Scheduler.Start();

            try
            {
                Log.Info("Scheduler Started");
                ScheduleJobs();
            }
            catch (Exception ex)
            {
                Log.Error("Error during schedule jobs", ex);
                Scheduler.Shutdown();
                throw;
            }
        }

        private void ScheduleJobs()
        {
            var schedulerService = new SchedulerService();
            if (Environment.UserInteractive)
            {
                var jobs = schedulerService.GetJobs();

                foreach (var job in jobs)
                {
                    Console.WriteLine($"-- {job.JobKey} : {job.JobName}");
                }
                Console.WriteLine("Choose jobs to Run. Press 0 - for start");

                var jobsToRun = new List<int>(jobs.Length);

                int jobKey = -1;
                while (!int.TryParse(Console.ReadLine(), out jobKey) || jobKey != 0)
                {
                   jobsToRun.Add(jobKey); 
                }

                if (jobsToRun.Count == 0)
                {
                    schedulerService.RunAllJobs(Scheduler);
                }

                foreach (var jobToRun in jobsToRun)
                {
                    schedulerService.RunJobByKey(jobToRun, Scheduler);
                }
            }
            else
            {
                schedulerService.RunAllJobs(Scheduler);   
            }
        }

        protected override void OnStop()
        {
            Scheduler.Shutdown();
            Log.Info(ServiceName + " stopped");
        }
    }
}
