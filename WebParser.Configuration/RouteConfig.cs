﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebParser.Configuration
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "sitemaps",
                url: "sitemap_index.xml",
                defaults: new { controller = "Seo", action = "SitemapIndex" }
            );

            routes.MapRoute(
                "sitemap",
                "sitemap/{country}/{articleGroup}/{city}.xml",
                new { controller = "Seo", action = "Sitemap", country = UrlParameter.Optional, articleGroup = UrlParameter.Optional, city = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Dashboard", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
