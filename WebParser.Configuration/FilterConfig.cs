﻿using System.Web.Mvc;
using WebParser.Controllers.Filters;

namespace WebParser.Configuration
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ExceptionFilterAttribute());
        }
    }
}
