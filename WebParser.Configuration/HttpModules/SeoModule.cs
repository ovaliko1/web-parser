﻿using System;
using System.Web;
using WebParser.Application.Interfaces.Services.Seo;

namespace WebParser.Configuration.HttpModules
{
    public class SeoModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.BeginRequest += ContextOnBeginRequest;
        }

        private void ContextOnBeginRequest(object sender, EventArgs eventArgs)
        {
            var httpContext = ((HttpApplication)sender).Context;

            if (httpContext.Request.Url.AbsolutePath != "/" && httpContext.Request.Url.AbsolutePath.EndsWith("/", StringComparison.Ordinal))
            {
                var redirect = httpContext.Request.Url.AbsolutePath;
                redirect = redirect.Remove(redirect.Length - 1);
                httpContext.Response.Clear();
                httpContext.Response.Status = "301 Moved Permanently";
                httpContext.Response.StatusCode = 301;
                httpContext.Response.AddHeader("Location", redirect);
                httpContext.Response.End();
                return;
            }

            var service = ContainerConfig.Resolve<ISeoRoutingService>();

            var result = service.Route(httpContext.Request.Url);
            if (result == null)
            {
                return;
            }

            if (result.ForceNotFoundStatus &&
                httpContext.Request.ServerVariables["HTTP_USER_AGENT"].IndexOf("Googlebot", StringComparison.InvariantCultureIgnoreCase) != -1)
            {
                httpContext.Response.Clear();
                httpContext.Response.Status = "404 Not found";
                httpContext.Response.StatusCode = 404;
                httpContext.Response.End();
                return;
            }

            httpContext.Items["HTTPCONTEXT:SEOROUTING:RESULT"] = result;

            httpContext.RewritePath("/seo");
        }

        public void Dispose()
        {
        }
    }
}