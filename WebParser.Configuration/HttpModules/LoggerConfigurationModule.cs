﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Web;
using System.Web.Http.WebHost;
using System.Web.Mvc;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Common.Extensions;

namespace WebParser.Configuration.HttpModules
{
    public class LoggerConfigurationModule : IHttpModule
    {
        private const string Anonymous = "<anonymous>";
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void Init(HttpApplication context)
        {
            context.BeginRequest += OnBeginRequest;
            context.PostMapRequestHandler += OnPostMapRequestHandler;
            context.EndRequest += OnEndRequest;
        }

        private void OnBeginRequest(object sender, EventArgs eventArgs)
        {
            var httpContext = ((HttpApplication)sender).Context;
            httpContext.Items["RequestCache::RequestStopwatch"] = Stopwatch.StartNew();

            InitLoggerProperties(httpContext);
        }

        private void OnPostMapRequestHandler(object sender, EventArgs eventArgs)
        {
            InitUserSpecificLoggerProperties(((HttpApplication)sender).Context);
        }

        private static bool IsMvcOrWebApiHandler(HttpContext httpContext)
        {
            return httpContext.Handler is MvcHandler ||
                   httpContext.Handler is HttpControllerHandler;
        }

        private void OnEndRequest(object sender, EventArgs eventArgs)
        {
            var httpContext = ((HttpApplication)sender).Context;

            var stopwatch = httpContext.Items["RequestCache::RequestStopwatch"] as Stopwatch;

            var requestLog = "REQUESTLOG";
            if (stopwatch != null)
            {
                Log.Debug($"STATISTICS: ElapsedMilliseconds: {stopwatch.ElapsedMilliseconds}");
            }

            Log.Debug(requestLog);

            if (IsMvcOrWebApiHandler(httpContext))
            {
                Log.Info(requestLog);
            }
        }

        public void Dispose()
        {
        }

        private static void InitLoggerProperties(HttpContext httpContext)
        {
            log4net.ThreadContext.Properties["ipAddress"] = httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? httpContext.Request.ServerVariables["REMOTE_ADDR"];
            log4net.ThreadContext.Properties["userAgent"] = httpContext.Request.ServerVariables["HTTP_USER_AGENT"];
            log4net.ThreadContext.Properties["httpMethod"] = httpContext.Request.HttpMethod;
            log4net.ThreadContext.Properties["requestUrl"] = httpContext.Request.RawUrl;
            log4net.ThreadContext.Properties["currentUser"] = Anonymous;
        }

        private static void InitUserSpecificLoggerProperties(HttpContext httpContext)
        {
            if (IsMvcOrWebApiHandler(httpContext))
            {
                var userService = ContainerConfig.Resolve<IUserService>();
                var userId = userService.GetCurrentUserId();
                log4net.ThreadContext.Properties["currentUser"] = userId == null ? Anonymous : userId.ToStringInvariant();
            }
            log4net.ThreadContext.Properties["handler"] = httpContext.Handler?.GetType();
        }
    }
}
