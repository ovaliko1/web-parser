﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using WebParser.Application.Services.File;
using WebParser.Application.Services.Parsing.AddressRecognizing;
using WebParser.Application.Services.Parsing.ParsedContent;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Application.Services.Parsing.SiteContent;
using WebParser.Application.Services.Parsing.Spider;
using WebParser.Controllers;
using WebParser.DataAccess;
using WebParser.Integration.Twitter;
using WebParser.Integration.Vk;
using WebParser.WebApi.Controllers;

namespace WebParser.Configuration
{
    public class ContainerConfig
    {
        private static IContainer Container { get; set; }

        public static void RegisterContainer()
        {
            InitContainer();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(Container);
        }

        public static void InitContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof (DashboardController).Assembly);
            builder.RegisterApiControllers(typeof (SiteStructureController).Assembly);
            builder.RegisterModule(new AutofacWebTypesModule());
            RegisterTypes(builder);
            Container = builder.Build();
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            var applicationAssembly = typeof (ParsedContentService).Assembly;
            builder.RegisterAssemblyTypes(applicationAssembly)
                .Where(x => x.Name.EndsWith("Service") || x.Name.EndsWith("Command") || x.Name.EndsWith("Job") || x.Name.EndsWith("Settings"))
                .AsImplementedInterfaces()
                .SingleInstance();

            var twitterClientIntegrationAssembly = typeof(TwitterClient).Assembly;
            builder.RegisterAssemblyTypes(twitterClientIntegrationAssembly)
                .Where(x => x.Name.EndsWith("Client") || x.Name.EndsWith("Settings"))
                .AsImplementedInterfaces()
                .SingleInstance();


            builder.RegisterType<ParsingProgressErrorsHandler>()
                .As<IParsingProgressErrorsHandler>()
                .SingleInstance();

            builder.RegisterType<Spider>()
                .As<ISpider>()
                .SingleInstance();

            builder.RegisterType<Repository>()
                .As<IRepository>()
                .SingleInstance();

            builder.RegisterType<AmazonStorage>()
                .As<IStorage>()
                .SingleInstance();

            builder.RegisterType<ParsingLogger>()
                .As<IParsingLogger>()
                .SingleInstance();

            builder.RegisterType<VkClient>()
                .As<IVkClient>()
                .SingleInstance();

            builder.RegisterType<FileDownloader>()
                .As<IFileDownloader>()
                .SingleInstance();

            builder.RegisterType<YandexAddressRecognizingService>()
                .As<IAddressRecognizingService>()
                .SingleInstance();
        }

        public static void RegisterInstance<T>(T instance) where T : class
        {
            var newBuilder = new ContainerBuilder();
            newBuilder.RegisterInstance(instance).As<T>().SingleInstance();
#pragma warning disable 618
            newBuilder.Update(Container);
#pragma warning restore 618
        }

        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}