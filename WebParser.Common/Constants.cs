﻿namespace WebParser.Common
{
    public static class Constants
    {
        public static readonly int AdminSiteCount = -1;

        public static readonly string DefaultTimeZoneId = "Russian Standard Time";
    }
}
