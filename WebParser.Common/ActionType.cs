﻿namespace WebParser.Common
{
    public enum ActionType
    {
        Fetch = 0,
        Click = 1,
        Wait = 2,
    }
}