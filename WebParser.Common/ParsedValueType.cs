﻿namespace WebParser.Common
{
    public enum ParsedValueType
    {
        String = 1,
        DateTime = 2,
        LinkToFile = 3,
        LinkToInternalStorage = 4,
    }
}