﻿namespace WebParser.Common
{
    public class SeoRoutingResult
    {
        public bool ForceNotFoundStatus { get; set; }

        public string SeoTitle { get; set; }

        public int? CountryId { get; set; }

        public int? CityId { get; set; }

        public string ArticleGroupInternalName { get; set; }

        public int? DistrictId { get; set; }

        public int? PriceFrom { get; set; }

        public int? PriceTo { get; set; }
    }
}