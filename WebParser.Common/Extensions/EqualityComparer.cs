﻿using System;
using System.Collections.Generic;

namespace WebParser.Common.Extensions
{
    public class EqualityComparer<T> : IEqualityComparer<T>
    {
        private Func<T, T, bool> CompareFunc { get; }

        private Func<T, int> GetHashCodeFunc { get; }
        
        public EqualityComparer(Func<T, T, bool> compareFunc, Func<T, int> getHashCodeFunc)
        {
            CompareFunc = compareFunc ?? ((x, y) => x.Equals(y));
            GetHashCodeFunc = getHashCodeFunc ?? (x => x.GetHashCode());
        }
        public EqualityComparer(Func<T, T, bool> compareFunc) : this(compareFunc, null)
        {
        }

        public bool Equals(T x, T y)
        {
            return CompareFunc(x, y);
        }

        public int GetHashCode(T obj)
        {
            return GetHashCodeFunc(obj);
        }
    }
}
