﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebParser.Common.Extensions
{
    public static class Utilities
    {
        public static TOut IfNotNull<TOut, TIn>(this TIn entity, Func<TIn, TOut> func)
            where TIn : class
            where TOut : class
        {
            return IfNotNull(entity, func, null);
        }

        public static TOut IfNotNull<TOut, TIn>(this TIn entity, Func<TIn, TOut> func, TOut @default)
            where TIn : class
            where TOut : class
        {
            return entity == null ? @default : func(entity);
        }

        public static TOut? IfNotNull<TOut, TIn>(this TIn? entity, Func<TIn?, TOut?> func)
            where TIn : struct 
            where TOut : struct
        {
            return entity == null ? null : func(entity);
        }

        public static TOut? IfNotNull<TOut, TIn>(this TIn entity, Func<TIn, TOut?> func)
            where TIn : class
            where TOut : struct
        {
            return entity == null ? null : func(entity);
        }

        public static TOut[] ToArray<TOut, TIn>(this IEnumerable<TIn> array, Func<TIn, TOut> converter)
        {
            var result = array?.Select(converter).ToArray();
            if (result == null || result.All(x => x == null))
            {
                return new TOut[0];
            }
            return result;
        }

        public static void Foreach<TIn>(this IEnumerable<TIn> array, Action<TIn> body)
        {
            foreach (var item in array)
            {
                body?.Invoke(item);
            }
        }

        public static TIn[] CreateArray<TIn>(params TIn[] args)
        {
            return args.ToArray(x => x);
        }

        public static bool IsEmpty<T>(this T[] array)
        {
            return array == null || array.Length == 0;
        }

        public static string Cut(this string str, int length)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            str = str.Trim();
            return str.Substring(0, length > str.Length ? str.Length : length);
        }

        public static bool Contains(this string source, string value, StringComparison comparison)
        {
            return source.IndexOf(value, comparison) >= 0;
        }

        public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
        {
            return dictionary.GetValueOr(key, default(TValue));
        }

        public static TValue GetValueOr<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue @default)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : @default;
        }

        public static bool CompareIgnoreCase(this string strA, string strB)
        {
            return string.Compare(strA, strB, StringComparison.InvariantCultureIgnoreCase) == 0;
        }

        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static IEnumerable<T> Concat<T>(this IEnumerable<T> enumerable, T element)
        {
            return enumerable.Concat(new [] { element });
        }

        public static T Update<T>(this T entity, Action<T> update)
        {
            update?.Invoke(entity);
            return entity;
        }

        public static T[] MoveItemsToBegin<T>(this T[] items, Func<T, bool> compare)
        {
            return items
                .Select(item => new
                {
                    item,
                    order = compare(item) ? 0 : 1
                })
                .OrderBy(x => x.order)
                .Select(x => x.item)
                .ToArray();
        }
    }
}
