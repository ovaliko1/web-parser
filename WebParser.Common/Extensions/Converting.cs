using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using WebParser.Common.Exceptions;

namespace WebParser.Common.Extensions
{
    public static class Converting
    {
        public static string ToStringInvariant(this int value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        public static string ToStringInvariant(this int? value)
        {
            return value?.ToStringInvariant() ?? string.Empty;
        }

        public static string ToStringInvariant(this DateTime? date, string format)
        {
            return date?.ToStringInvariant(format) ?? string.Empty;
        }

        public static string ToStringInvariant(this DateTime date, string format)
        {
            return date.ToString(format, CultureInfo.InvariantCulture);
        }

        public static string ToStringInvariant(this TimeSpan? time, string format)
        {
            return time?.ToString(format, CultureInfo.InvariantCulture) ?? string.Empty;
        }

        public static string ToStringInvariant(this decimal value)
        {
            return value.ToString(CultureInfo.InvariantCulture);
        }

        public static string ToStringInvariant(this decimal? value)
        {
            return value?.ToStringInvariant() ?? string.Empty;
        }

        public static string ToStringInvariant(this double value)
        {
            return value.ToString(CultureInfo.InvariantCulture) ?? string.Empty;
        }

        public static DateTime? ToNullableDateTime(this string dateTime, string dateFormat)
        {
            if (string.IsNullOrEmpty(dateTime))
            {
                return null;
            }
            return dateTime.ToDateTime(dateFormat);
        }

        public static int ToId(this string id)
        {
            int result = id.ToInt();
            if (result > 0)
            {
                return result;
            }

            throw new ConvertException($"Wrong id to convert: {id}");
        }

        public static int ToInt(this string str)
        {
            int result;
            if (int.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                return result;
            }

            throw new ConvertException($"Wrong int to convert: {str}");
        }

        public static int? ToNullableInt(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            return str.ToInt();
        }

        public static decimal ToDecimal(this string str)
        {
            decimal result;
            if (decimal.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                return result;
            }

            throw new ConvertException($"Wrong decimal to convert: {str}");
        }

        public static long ToLong(this string id)
        {
            long result;
            if (long.TryParse(id, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                return result;
            }

            throw new ConvertException($"Wrong long to convert: {id}");
        }

        public static DateTime ToDateTime(this string dateTime, string dateFormat)
        {
            DateTime result;
            if(DateTime.TryParseExact(dateTime, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
            {
                return result;
            }

            throw new ConvertException($"Wrong date to convert: {dateTime}");
        }

        public static Guid ToGuid(this string guid)
        {
            Guid result;
            if (Guid.TryParse(guid, out result))
            {
                return result;
            }

            throw new ConvertException($"Wrong GUID to convert: {guid}");
        }

        public static DateTime ConvertUtcDateTimeToTimeZone(this DateTime dateTime, string timeZoneId)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, TimeZoneInfo.FindSystemTimeZoneById(timeZoneId));
        }

        public static DateTime ConvertDateTimeToUtc(this DateTime dateTime, string timeZoneId)
        {
            return TimeZoneInfo.ConvertTimeToUtc(dateTime, TimeZoneInfo.FindSystemTimeZoneById(timeZoneId));
        }

        public static string ToNullIfEmpty(this string str)
        {
            return string.IsNullOrEmpty(str) ? null : str;
        }

        /// <summary>
        /// Regex based replace
        /// </summary>
        /// <param name="str"></param>
        /// <param name="oldValue">Shouldn't be Regex</param>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public static string ReplaceIgnoreCase(this string str, string oldValue, string newValue)
        {
            return Regex.Replace(str, oldValue, newValue, RegexOptions.IgnoreCase);
        }

        public static T? FirstOrNullableDefault<T>(this IEnumerable<T> range, Func<T?, bool> predicate) where T : struct
        {
            return range.Select(x => (T?)x).FirstOrDefault(predicate);
        }

        public static T? FirstOrNullableDefault<T>(this IEnumerable<T> range) where T : struct
        {
            return range.Select(x => (T?)x).FirstOrDefault();
        }

        public static IEnumerable<T> ToEmptyIfNull<T>(this IEnumerable<T> range)
        {
            return range ?? Enumerable.Empty<T>();
        }
    }
}
