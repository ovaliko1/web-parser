﻿using System.IO;

namespace WebParser.Common.Extensions
{
    public static class StreamHelper
    {
        public static byte[] GetBytes(this Stream stream)
        {
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);

                var result = memoryStream.ToArray();
                return result;
            }
        }
    }
}
