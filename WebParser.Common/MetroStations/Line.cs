﻿namespace WebParser.Common.MetroStations
{
    public class Line
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }
    }
}