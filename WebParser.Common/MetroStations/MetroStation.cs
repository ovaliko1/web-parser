﻿namespace WebParser.Common.MetroStations
{
    public class MetroStation
    {
        public int Id { get; set; }

        public int CityId { get; set; }

        public string Name { get; set; }

        public string[] NamesToSearch { get; set; }

        public Line Line { get; set; }
    }
}