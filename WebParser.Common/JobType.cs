﻿namespace WebParser.Common
{
    public enum JobType
    {
        Parsing = 1,
        BlackList = 2,
        WhiteList = 3,
        ParsingAndWhiteList = 4,
    }
}