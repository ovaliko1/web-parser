﻿namespace WebParser.Common
{
    public enum SearchType
    {
        SingleNode = 1,
        ManyNodes  = 2,
    }
}
