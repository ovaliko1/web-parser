﻿namespace WebParser.Common
{
    public enum VoteResultType
    {
        Success = 1,

        MaxFreeDaysCountReached = 2,

        ClickerCaution = 3,

        AccessExpired = 4,
    }
}
