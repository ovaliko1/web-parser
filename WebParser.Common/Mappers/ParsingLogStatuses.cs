﻿using System.Collections.Generic;
using WebParser.Common.Extensions;

namespace WebParser.Common.Mappers
{
    public class ParsingLogStatuses
    {
        private static readonly Dictionary<ParsingLogStatus, string> ValueDictionary = new Dictionary<ParsingLogStatus, string>
        {
            { ParsingLogStatus.Success, "Success" },
            { ParsingLogStatus.Fail, "Fail" }
        };

        public static Item[] GetItems()
        {
            return ValueDictionary.ToArray(x => new Item
            {
                Id = (int) x.Key,
                Name = x.Value
            });
        }
    }
}