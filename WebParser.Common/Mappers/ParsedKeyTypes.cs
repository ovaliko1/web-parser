﻿using System.Collections.Generic;
using WebParser.Common.Extensions;

namespace WebParser.Common.Mappers
{
    public static class ParsedKeyTypes
    {
        private static readonly Dictionary<ParsedKeyType, string> ValueDictionary = new Dictionary<ParsedKeyType, string>
        {
            { ParsedKeyType.Common, "Common" },
            { ParsedKeyType.Phone, "Phone" },
            { ParsedKeyType.Metro, "Metro" },
            { ParsedKeyType.Price, "Price" },
            { ParsedKeyType.Address, "Address" },
            { ParsedKeyType.Object, "Object" },
            { ParsedKeyType.SourceDate, "Source Date" },
            { ParsedKeyType.Name, "Name" },
            { ParsedKeyType.Description, "Description" },
            { ParsedKeyType.Characteristics, "Characteristics" },
            { ParsedKeyType.Images, "Images" },
            { ParsedKeyType.Location, "Location" },
        };                  

        public static Item[] GetItems()
        {
            return ValueDictionary.ToArray(x => new Item
            {
                Id = (int)x.Key,
                Name = x.Value
            });
        }
    }
}
