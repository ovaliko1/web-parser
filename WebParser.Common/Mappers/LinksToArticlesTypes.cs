﻿using System.Collections.Generic;
using WebParser.Common.Extensions;

namespace WebParser.Common.Mappers
{
    public static class LinksToArticlesTypes
    {
        private static readonly Dictionary<LinksToArticlesType, string> ValueDictionary = new Dictionary<LinksToArticlesType, string>
        {
            { LinksToArticlesType.SpecificLinks, "Specific Links" },
            { LinksToArticlesType.LinksFormat, "Links Format" },
            { LinksToArticlesType.LinksFile, "Links File" }
        };

        public static Item[] GetItems()
        {
            return ValueDictionary.ToArray(x => new Item
            {
                Id = (int)x.Key,
                Name = x.Value
            });
        }
    }
}
