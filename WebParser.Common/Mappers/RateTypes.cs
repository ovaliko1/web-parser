﻿using System.Collections.Generic;
using WebParser.Common.Extensions;

namespace WebParser.Common.Mappers
{
    public static class RateTypes
    {
        private static readonly Dictionary<RateType, string> ValueDictionary = new Dictionary<RateType, string>
        {
            { RateType.Days, "Days" },
            { RateType.Openings, "Openings" },
        };

        public static Item[] GetItems()
        {
            return ValueDictionary.ToArray(x => new Item
            {
                Id = (int)x.Key,
                Name = x.Value
            });
        }
    }
}
