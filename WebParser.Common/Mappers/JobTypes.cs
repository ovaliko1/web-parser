﻿using System.Collections.Generic;
using WebParser.Common.Extensions;

namespace WebParser.Common.Mappers
{
    public static class JobTypes
    {
        private static readonly Dictionary<JobType, string> ValueDictionary = new Dictionary<JobType, string>
        {
            { JobType.Parsing, "Parsing" },
            { JobType.BlackList, "BlackList" },
            { JobType.WhiteList, "WhiteList" },
            { JobType.ParsingAndWhiteList, "ParsingAndWhiteList" },
        };

        public static Item[] GetItems()
        {
            return ValueDictionary.ToArray(x => new Item
            {
                Id = (int)x.Key,
                Name = x.Value
            });
        }
    }
}
