﻿using System.Collections.Generic;
using System.Linq;
using WebParser.Common.Extensions;

namespace WebParser.Common.Mappers
{
    public static class Roles
    {
        private static readonly Dictionary<string, Role> RolesDictionary = new Dictionary<string, Role>
        {
            { "Admin", Role.Admin },
            { "User", Role.User },
            { "Viewer", Role.Viewer },
            { "Publisher", Role.Publisher },
        };

        private static readonly Dictionary<Role, string> RoleNamesDictionary = new Dictionary<Role, string>
        {
            { Role.Admin, "Admin" },
            { Role.User, "User" },
            { Role.Viewer, "Viewer" },
            { Role.Publisher, "Publisher" },
        };

        public static Role[] AllRoles => RoleNamesDictionary.Keys.ToArray(x => x);

        public static string GetRoleName(this Role role) => RoleNamesDictionary[role];

        public static bool HasOnlyRole(this Role[] roles, Role role) => roles.Length == 1 && roles.HasRole(role);

        public static bool HasRole(this Role[] roles, Role role) => roles.Contains(role);

        public static bool HasAnyRole(this Role[] roles, params Role[] anyRoles) => anyRoles.Any(roles.Contains);

        public static Role ToRole(this string roleName)
        {
            Role result;
            return RolesDictionary.TryGetValue(roleName, out result) ? result : Role.Unknown;
        }
    }
}
