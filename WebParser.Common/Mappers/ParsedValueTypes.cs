﻿using System.Collections.Generic;
using WebParser.Common.Extensions;

namespace WebParser.Common.Mappers
{
    public static class ParsedValueTypes
    {
        private static readonly Dictionary<ParsedValueType, string> ValueDictionary = new Dictionary<ParsedValueType, string>
        {
            { ParsedValueType.DateTime, "DateTime" },
            { ParsedValueType.LinkToFile, "LinkToFile" },
            { ParsedValueType.LinkToInternalStorage, "LinkToInternalStorage" },
            { ParsedValueType.String, "String" },
        };

        public static Item[] GetItems()
        {
            return ValueDictionary.ToArray(x => new Item
            {
                Id = (int)x.Key,
                Name = x.Value
            });
        }

        public static string GetName(this ParsedValueType item)
        {
            return ValueDictionary[item];
        }
    }
}
