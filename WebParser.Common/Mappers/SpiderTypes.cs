﻿using System.Collections.Generic;
using WebParser.Common.Extensions;

namespace WebParser.Common.Mappers
{
    public static class SpiderTypes
    {
        private static readonly Dictionary<SpiderType, string> ValueDictionary = new Dictionary<SpiderType, string>
        {
            { SpiderType.PhantomJs, "PhantomJs" },
            { SpiderType.SlimerJs, "SlimerJs" },
            { SpiderType.Puppeter, "Puppeter" },
        };

        public static Item[] GetItems()
        {
            return ValueDictionary.ToArray(x => new Item
            {
                Id = (int)x.Key,
                Name = x.Value
            });
        }
    }
}
