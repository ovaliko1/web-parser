﻿using System.Collections.Generic;
using WebParser.Common.Extensions;

namespace WebParser.Common.Mappers
{
    public static class ArticleRatingTypes
    {
        private static readonly Dictionary<ArticleRatingType, string> ValueDictionary = new Dictionary<ArticleRatingType, string>
        {
            { ArticleRatingType.Good, "Good" },
            { ArticleRatingType.Bad, "Bad" },
            { ArticleRatingType.Outdate, "Outdate" },
        };

        public static Item[] GetItems()
        {
            return ValueDictionary.ToArray(x => new Item
            {
                Id = (int)x.Key,
                Name = x.Value
            });
        }
    }
}
