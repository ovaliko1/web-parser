﻿namespace WebParser.Common
{
    public enum SeoParameterType
    {
        Unknown,
        City,
        District,
        ArticleGroup,
        Phrase,
        Alias,
    }
}