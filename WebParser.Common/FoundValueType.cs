﻿namespace WebParser.Common
{
    public enum FoundValueType
    {
        TagTextContent     = 1,
        AttributeContent   = 2,
        TagContent         = 3,
    }
}
