﻿namespace WebParser.Common
{
    public enum ParsedKeyType
    {
        Common = 1,
        Phone = 2,
        Metro = 3,
        Price = 4,
        Address = 5,
        Object = 6,
        SourceDate = 7,
        Name = 8,
        Description = 9,
        Characteristics = 10,
        Images = 11,
        Location = 12,
    }
}