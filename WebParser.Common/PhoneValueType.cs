﻿namespace WebParser.Common
{
    public enum PhoneValueType
    {
        String = 1,
        Image = 2,
        Link = 3,
    }
}