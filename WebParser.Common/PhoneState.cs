﻿namespace WebParser.Common
{
    public enum PhoneState
    {
        InBlackList = 1,
        ShowContactsAndVotePossibility = 2,
        ShowContacts = 3,
        ConnectToContact = 4,
        OpenLink = 5,
    }
}