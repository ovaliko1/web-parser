﻿namespace WebParser.Common
{
    public enum ReviewResult
    {
        Confirmed = 1,
        Rejected = 2,
    }
}