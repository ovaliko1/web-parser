﻿namespace WebParser.Common
{
    public enum ArticleRatingType
    {
        Good = 1,
        Bad = 2,
        Outdate = 3,
    }
}