﻿namespace WebParser.Common
{
    public enum SpiderType
    {
        PhantomJs = 1,
        SlimerJs = 2,
        Puppeter = 3,
    }
}