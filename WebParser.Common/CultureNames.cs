﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebParser.Common
{
    public static class CultureNames
    {
        public static readonly string Russian = "ru-RU";

        public static readonly string English = "en-US";
    }
}
