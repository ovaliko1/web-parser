﻿namespace WebParser.Common.Exceptions
{
    public class ConvertException : CoreException
    {
        public ConvertException(string message) : base(message)
        {
        }
    }
}