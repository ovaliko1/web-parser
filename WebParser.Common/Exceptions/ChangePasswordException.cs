﻿using System.Collections.Generic;

namespace WebParser.Common.Exceptions
{
    public class ChangePasswordException : AggregateException
    {
        public ChangePasswordException(IEnumerable<string> errors) : base(errors)
        {
        }
    }
}
