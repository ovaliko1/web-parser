﻿namespace WebParser.Common.Exceptions
{
    public class FileNotFoundException : CoreException
    {
        public FileNotFoundException(string message) : base(message)
        {
        }
    }
}
