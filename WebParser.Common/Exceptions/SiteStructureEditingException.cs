﻿namespace WebParser.Common.Exceptions
{
    public class SiteStructureEditingException : CoreException
    {
        public SiteStructureEditingException(string message) : base(message)
        {
        }
    }
}
