﻿namespace WebParser.Common.Exceptions
{
    public class FetchContentException : CoreException
    {
        public FetchContentException(string message) : base(message)
        {
        }
    }
}
