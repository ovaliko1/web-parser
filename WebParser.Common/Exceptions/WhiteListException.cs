﻿namespace WebParser.Common.Exceptions
{
    public class WhiteListException : CoreException
    {
        public WhiteListException(string message) : base(message)
        {
        }
    }
}