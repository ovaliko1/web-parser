﻿namespace WebParser.Common.Exceptions
{
    public class ParsingException : CoreException
    {
        public ParsingException(string message) : base(message)
        {
        }
    }
}
