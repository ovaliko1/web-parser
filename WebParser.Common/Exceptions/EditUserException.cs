﻿using System.Collections.Generic;

namespace WebParser.Common.Exceptions
{
    public class EditUserException : AggregateException
    {
        public EditUserException(IEnumerable<string> errors) : base(errors)
        {
        }       
    }
}
