﻿namespace WebParser.Common.Exceptions
{
    public class SiteStructureException : CoreException
    {
        public SiteStructureException(string message) : base(message)
        {
        }
    }
}
