﻿namespace WebParser.Common.Exceptions
{
    public class DataBaseConstraintException : CoreException
    {
        public DataBaseConstraintException(string message) : base(message)
        {
        }
    }
}
