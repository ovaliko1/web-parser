﻿namespace WebParser.Common.Exceptions
{
    public class ValidationException : CoreException
    {
        public string FieldName { get; set; }

        public ValidationException(string fieldName, string message) : base(message)
        {
            FieldName = fieldName;
        }
    }
}
