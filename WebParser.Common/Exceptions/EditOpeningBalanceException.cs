﻿namespace WebParser.Common.Exceptions
{
    public class EditOpeningBalanceException : CoreException
    {
        public EditOpeningBalanceException(string message) : base(message)
        {
        }
    }
}
