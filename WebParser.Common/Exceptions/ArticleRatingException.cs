﻿namespace WebParser.Common.Exceptions
{
    public class BlackListException : CoreException
    {
        public BlackListException(string message) : base(message)
        {
        }
    }
}