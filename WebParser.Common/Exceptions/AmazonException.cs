﻿namespace WebParser.Common.Exceptions
{
    public class AmazonException : CoreException
    {
        public AmazonException(string message) : base(message)
        {
        }
    }
}
