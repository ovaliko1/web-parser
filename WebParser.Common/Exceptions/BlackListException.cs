﻿namespace WebParser.Common.Exceptions
{
    public class ArticleRatingException : CoreException
    {
        public ArticleRatingException(string message) : base(message)
        {
        }
    }
}