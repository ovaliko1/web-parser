﻿namespace WebParser.Common.Exceptions
{
    public class ParsedContentException : CoreException
    {
        public ParsedContentException(string message) : base(message)
        {
        }
    }
}
