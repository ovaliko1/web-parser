﻿using System.Collections.Generic;
using System.Linq;

namespace WebParser.Common.Exceptions
{
    public abstract class AggregateException : CoreException
    {
        protected AggregateException(IEnumerable<string> errors)
        {
            Errors = errors.ToArray();
        }

        public IEnumerable<string> Errors { get; set; }
    }
}
