﻿namespace WebParser.Common.Exceptions
{
    public class PublishingException : CoreException
    {
        public PublishingException(string message) : base(message)
        {
        }
    }
}