﻿using System;
using System.Runtime.Serialization;
using System.Security;

namespace WebParser.Common.Exceptions
{
    public abstract class CoreException : Exception
    {
        [SecuritySafeCritical]
        protected CoreException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        protected CoreException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected CoreException(string message)
            : base(message)
        {
        }

        protected CoreException()
            : base(string.Empty)
        {
        }
    }
}
