﻿namespace WebParser.Common.Exceptions
{
    public class NotFoundException : CoreException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
