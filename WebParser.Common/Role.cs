﻿namespace WebParser.Common
{
    public enum Role
    {
        Unknown = 0,
        Admin = 1,
        User = 2,
        Viewer = 3,
        Publisher = 4,
    }
}
