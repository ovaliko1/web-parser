﻿namespace WebParser.Common
{
    public enum LinksToArticlesType
    {
        SpecificLinks = 1,
        LinksFormat = 2,
        LinksFile = 3
    }
}