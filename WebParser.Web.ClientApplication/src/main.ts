import 'polyfills';
import './styles.scss';

import "../mockdata/apartment-list.json";
import "../mockdata/71077.json";
import "../mockdata/71078.json";
import "../mockdata/71079.json";
import "../mockdata/71080.json";
import "../mockdata/71081.json";
import "../mockdata/71082.json";
import "../mockdata/71083.json";
import "../mockdata/spb-filters-init-data.json";
import "../mockdata/msk-filters-init-data.json";
import "../mockdata/user-counters.json";
import "../mockdata/vote-result-71078.json";
import "../mockdata/vote-result-71080.json";
import "../mockdata/vote-result-71083.json";
import "../mockdata/payment-form.json";
import "../mockdata/publics.json";
import "../mockdata/contactInfo_71080.json";
import "../mockdata/contactInfo_71082.json";
import "../mockdata/contactInfo_71083.json";

import { enableProdMode } from '@angular/core';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

if (process.env.ENV === 'prod') {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
