interface String {
  cut(length:number, end:string): string;
}

if (!String.prototype.cut) {
    String.prototype.cut = function (length:number, end:string) {
        let self = (String)(this);
        let originalLength = self.length;

        if (originalLength > length){
            return self.slice(0, length) + end;
        }
        return self;
    }
}