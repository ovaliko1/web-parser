import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Routes, RouterModule } from "@angular/router";

import { SharedModule } from "./modules/shared/shared.module";
import { ApartmentsModule } from "./modules/apartments/apartments.module";
import { SaleModule } from "./modules/sale/sale.module";

import { AppComponent } from "./app.component";
import { Apartments } from "./modules/apartments/components/apartments.component";
import { AccessExpired } from "./modules/sale/components/access-expired.component";

import "./utilities/array.extensions";
import "./utilities/string.extensions";

const appRoutes: Routes = [
  { path: 'apartments', component: Apartments },
  { path: 'Apartments', redirectTo: '/apartments' },
  { path: 'apartments/index', redirectTo: '/apartments' },
  { path: 'Apartments/Index', redirectTo: '/apartments' },

  { path: 'forbidden', component: AccessExpired },
  { path: 'Forbidden', redirectTo: '/forbidden' },
  { path: 'forbidden/index', redirectTo: '/forbidden' },
  { path: 'Forbidden/Index', redirectTo: '/forbidden' },

  { path: '**', redirectTo: '/apartments' },
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    SharedModule,
    ApartmentsModule,
    SaleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
