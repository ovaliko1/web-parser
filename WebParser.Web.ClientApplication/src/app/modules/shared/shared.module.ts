import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";

import { HttpClient } from "./api/http-client ";
import { ApiController } from "./api/api.controller";
import { HttpClientModule } from '@angular/common/http';

import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatSelectModule,
  MatSidenavModule,
  MatToolbarModule,
  MatTooltipModule,
  MatSliderModule,
  MatRadioModule,
  MatDialogModule,
  MatTabsModule,
  MatExpansionModule,
} from '@angular/material';

import { TopBar } from "./components/top-bar.component";
import { Empty } from "./components/empty.component";
import { Gallery } from "./components/gallery.component";
import { MetricGoalDirective } from "./directives/metric-goal.directive";
import { MetricGoal } from "./components/metric-goal.component";
import { PublicsPopupContent } from "./components/publics-popup-content.component";
import { SupportWidget } from "./components/support-widget.component";

import { WindowSizeService } from "./services/window-size.service";
import { UserCountersService } from "./services/user-counters.service";
import { LocalStorageService } from "./services/local-storage.service";
import { PopupService } from "./services/popup.service";
import { IndexedDBService } from "./services/Indexed-db-service";

@NgModule({
  declarations: [
    TopBar,
    Empty,
    Gallery,
    MetricGoalDirective,
    MetricGoal,
    PublicsPopupContent,
    SupportWidget,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,

    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSelectModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSliderModule,
    MatRadioModule,
    MatDialogModule,
    MatTabsModule,
    MatExpansionModule,
  ],
  providers: [ApiController, HttpClient, WindowSizeService, UserCountersService, LocalStorageService, PopupService, IndexedDBService],
  exports: [
    TopBar,
    Gallery,
    MetricGoalDirective,
    MetricGoal,
    SupportWidget,

    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,

    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSelectModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSliderModule,
    MatRadioModule,
    MatDialogModule,
    MatTabsModule,
    MatExpansionModule,
  ],
  entryComponents: [PublicsPopupContent],
  bootstrap: [TopBar]
})
export class SharedModule { }
