
export class ImageInfo {
  constructor(
    public imageUrl: string,
    public width: number,
    public height: number,
  )
  { }
}
