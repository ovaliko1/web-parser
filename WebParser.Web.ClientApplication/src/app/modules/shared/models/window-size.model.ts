export class WindowSizeModel {
  constructor(
    public width: number,
    public height: number)
  { }
}