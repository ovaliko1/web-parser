import { RateType } from "../enums/rate-type";

export class UserCountersModel {
  userName: string;
  goodVotes: number;
  badVotes: number;
  outdateVotes: number;
  rateType: RateType;
  accessDateTime: string;
  openingBalance: string;
  freeDays: number;
  freeOpenings: number;
  maxFreeDaysCount: number;
  maxFreeOpeningsCount: number;
  blackListItemsCount: string;
}
