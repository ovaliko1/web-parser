import { PublicItemModel } from "./public-item.model";

export class PublicsModel {
  sourceUserId: number;
  publics: PublicItemModel[];
}