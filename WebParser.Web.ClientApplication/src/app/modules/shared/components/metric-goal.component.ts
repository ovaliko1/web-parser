import { Component, Input, OnInit } from "@angular/core";

declare var application: any;
declare var yaCounter42384909: any;

@Component({
  selector: "metric-goal",
  template: ``
})
export class MetricGoal implements OnInit  {

  @Input("goalName") goalName: string;

  constructor(){
  }

  ngOnInit(){
    if(application.settings.renderMetrics){
      let reachGoalScript = `${application.settings.yandexMetrikaCounterName}.reachGoal('${this.goalName}')`;
      eval(reachGoalScript);
    }
  }
}
