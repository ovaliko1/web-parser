import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { Subscription } from "rxjs/Subscription";

import { WindowSizeService } from "../services/window-size.service";
import { UserCountersService } from "../services/user-counters.service";
import { UserCountersModel } from "../models/user-counters.model";
import { PopupService } from "../services/popup.service";
import { RateType } from "../enums/rate-type";

declare var shareButtons: any;
declare var application: any;

@Component({
  selector: "top-bar",
  templateUrl: "../templates/top-bar.component.html",
  styles: ["../templates/styles/top-bar.scss"]
})
export class TopBar implements OnInit, OnDestroy {
  RateType: any = RateType;  

  public shareButtonInitialized: boolean = false;
  public showUserSpecificControls: boolean;
  public logoSrc:string;
  public mobileView: boolean = false;

  private mobileViewSubscription: Subscription;
  private currentUserCountersSubscription: Subscription;

  public userCounters: UserCountersModel = new UserCountersModel();

  @ViewChild('sidenav') sidenavControl: any;
  public showSideBar: boolean = false;

  constructor(
    private windowSizeService: WindowSizeService,
    private userCountersService: UserCountersService,
    private publicsPopupService: PopupService) {
  }

  async ngOnInit() {
    let self = this;

    this.showUserSpecificControls = !application.settings.isAnonymous;

    this.mobileView = this.windowSizeService.getMobileMode();
    this.mobileViewSubscription = this.windowSizeService.mobileView$.subscribe(x => {
      self.mobileView = x;
      self.setLogoSrc(self);
    });

    self.setLogoSrc(self);
    
    if (this.showUserSpecificControls){
      this.currentUserCountersSubscription = this.userCountersService.currentUserCounters$.subscribe((x) => {
        self.userCounters = x;
      })
      await this.refreshUserCounters();
    }

    document.addEventListener('click', (e) => this.closeSidenavOnOutsideClick(e));
  }

  async refreshUserCounters(){
    this.userCounters = await this.userCountersService.getUserCounters();
  }

  onShareMenuOpen(): void{
    if (!this.shareButtonInitialized){
      var self = this;
      shareButtons.init(x => {
        setTimeout(() => self.onShareClick(x), 10000);
      });
    } 
    this.shareButtonInitialized = true;
  }

  async onShareClick(e){
    await this.userCountersService.tryExtendUserAccessForSharing();
    await this.refreshUserCounters();
  }

  public openSideBar(){
    this.showSideBar = true;
    this.sidenavControl.open();
  }

  public closeSideBar(){
    this.sidenavControl.close();
    this.showSideBar = false;
  }

  closeSidenavOnOutsideClick(e) {
    if (!this.sidenavControl.opened) {
      return;
    }

    var clickedComponent = e.target;
    var inside = false;

    do {
      if (clickedComponent.classList.contains('js-top-bar-side-nav') ||
          clickedComponent.classList.contains('sidenav-toggler') ||
          clickedComponent.classList.contains('cdk-overlay-container')) {
        inside = true;
        break;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent && clickedComponent.classList);
    
    if (!inside) {
      this.closeSideBar();
    }
  }

  setLogoSrc(self){
    self.logoSrc = self.mobileView ?
      "/static/img/logo48x48.png":
      "/static/img/pull-me-out-logo-black.png";
  }

  async showPopup(sourceUserId){
    await this.publicsPopupService.showPublicsPopup(sourceUserId);
  }
  // prevent memory leak when component destroyed
  ngOnDestroy() {
    this.mobileViewSubscription.unsubscribe();
    if(this.currentUserCountersSubscription){
      this.currentUserCountersSubscription.unsubscribe();
    }
  }
}