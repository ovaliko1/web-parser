import { Component, OnInit } from "@angular/core";

@Component({
  selector: "support-widget",
  templateUrl: "../templates/support-widget.html",
})
export class SupportWidget implements OnInit {

  ngOnInit(): void {
    let script =
    `VK.Widgets.CommunityMessages("vk_community_messages", 126773354, {
      tooltipButtonText: "Есть вопрос?",
      disableExpandChatSound: 1
    });`;
    eval(script);
  }
}
