import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ImageInfo } from "../models/image-info.model";
import { WindowSizeService } from "../services/window-size.service";

declare var PhotoSwipe: any;
declare var PhotoSwipeUI_Default: any;

@Component({
  selector: "gallery",
  templateUrl: "../templates/gallery.component.html",
  styles: ["../templates/styles/gallery.scss"]
})
export class Gallery implements OnInit {
  imagesModel: any = new Object();
  images = [];

  photoSwipeRoot: HTMLElement;

  rowHeight:number = 80;

  @ViewChild('container') container: any;

  ngOnInit(): void {
    let self = this;
    this.imagesModel = this.getImages(null);
    this.photoSwipeRoot = document.getElementById('photoSwipeRoot');
  }

  showImage(event) {
    event.preventDefault();

    let index = event.currentTarget.attributes['data-index'];
    let photoSwipe = new PhotoSwipe(this.photoSwipeRoot, PhotoSwipeUI_Default, this.images, {
      index: parseInt(index.value)
    });
    photoSwipe.init();
  }

  public async setImages(sourceImages: ImageInfo[]){
    this.imagesModel = this.getImages(sourceImages);

    let self = this;
    self.images = [];

    for (let i = 0; i < sourceImages.length; i++) {
      let image = sourceImages[i];
      let imageDetails = await this.getImageDetails(image);
      this.images.push(imageDetails);
    }
  }

  private async getImageDetails(image: ImageInfo){
    return new Promise<any>(resolve => {

      if (image.height > 0 && image.width > 0){
        resolve({
          src: image.imageUrl,
          w: image.width,
          h: image.height,
        });
        return;
      }

      let img = new Image();
      img.onload = function() {
        resolve({
          src: image.imageUrl,
          w: (this as any).width,
          h: (this as any).height,
        });
      };
      img.onerror = function() {
        resolve({
          src: '',
          w: 0,
          h: 0,
        })
      }

      img.src = image.imageUrl;
    });
  }

  private getImages(images: ImageInfo[]) {
    if (images == null || images.length == 0){
      return { 
        cols: 0,
        images:[]
      };
    }

    let width = this.container.nativeElement.clientWidth;

    let imageLinks = images;

    let cols = Math.floor(width/(this.rowHeight + 3));
    let colsInHalf = Math.floor(cols / 2)

    let result = {
      cols: cols,
      images: imageLinks.toArray((x, i) => {
        let colspan = i < colsInHalf ? 2 : 1;
        let rowspan = colspan;
        return {
          colspan: colspan,
          rowspan: rowspan,
          link: x.imageUrl,
          index: i,
        };
      }),
    };

    return result;
  }
}
