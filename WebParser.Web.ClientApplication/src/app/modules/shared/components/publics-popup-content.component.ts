import { Component, Input, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { PublicItemModel } from "../models/public-item.model";

@Component({
  selector: "publics-popup-content",
  templateUrl: "../templates/publics-popup-content.html",
  styles: ["../templates/styles/publics-popup.scss"],
})
export class PublicsPopupContent implements OnInit  {

  constructor(
    public dialogRef: MatDialogRef<PublicsPopupContent>,
    @Inject(MAT_DIALOG_DATA) public data: PublicItemModel[]){
  }

  ngOnInit(){

  }
}
