import { Injectable } from "@angular/core";
import { ChosenFiltersModel, ChosenApartmentTypeModel } from "app/modules/apartments/models/chosen-filters.model";

@Injectable()
export class LocalStorageService {

  public saveFilters(filters:ChosenFiltersModel){
    this.saveObject("key:apartmentFilters", filters);
  }

  public getFilters(): ChosenFiltersModel{
    return this.getObject("key:apartmentFilters") as ChosenFiltersModel;
  }

  public getPublicsPopupAlreadySeen():boolean{
    return this.getObject("key:publicsPopupAlreadySeen") as boolean;
  }

  public savePublicsPopupAlreadySeen(){
    this.saveObject("key:publicsPopupAlreadySeen", true);
  }

  public getNotificationAskingCount(): number{
    let result = this.getObject("key:notificationAskingCount") as number;
    return result || 0;
  }

  public incrementNotificationAskingCount(){
    let currentCount = this.getNotificationAskingCount();
    this.saveObject("key:notificationAskingCount", ++currentCount);
  }

  public preventNotificationAskingCount(){
    let currentCount = this.getNotificationAskingCount();
    this.saveObject("key:notificationAskingCount", -1);
  }

  private saveObject(key:string, object:Object){
    localStorage.setItem(key, JSON.stringify(object));
  }

  private getObject(key:string): Object{
    try {
      let storedValue:string = localStorage.getItem(key);
      let result = JSON.parse(storedValue);
      return result;
    }
    catch(ex) {
      console.error(ex);
    }
    return null;
  }
}
