import { Injectable } from "@angular/core";
import { ChosenFiltersModel, ChosenApartmentTypeModel } from "app/modules/apartments/models/chosen-filters.model";

@Injectable()
export class IndexedDBService {
  private readonly dbVersion:number = 1;
  private readonly dbName:string = "pullmeout";
  private readonly stores: any = {
    apartmentFilters: "store:apartmentFilters",
  };
  
  public saveNewestApartmentId(apartmentId:string): Promise<boolean>{
    return this.getFilters()
      .then(filters => {
        if(filters){
          return this.saveFiltersCore(apartmentId, filters);
        }
        return false;
      });
  }

  public saveFilters(filters:ChosenFiltersModel): Promise<boolean>{
    return this.getFilters()
    .then(x => {
      // preventing updating apartmentId on source model
      let copy = JSON.parse(JSON.stringify(filters));
      if (x) {
        copy.apartmentId = x.apartmentId;
      }
      return this.saveFiltersCore(null, copy);
    });
  }

  private saveFiltersCore(apartmentId:string, filters:ChosenFiltersModel): Promise<boolean>{
    filters.apartmentId = apartmentId || filters.apartmentId;
    return this.saveObject(this.stores.apartmentFilters, {
      "PK_id": "1",
      filters: filters,
    });
  }

  public getFilters(): Promise<ChosenFiltersModel>{
    return this.getObject(this.stores.apartmentFilters, "1")
      .then(x => x != null ? x.filters : null);
  }

  private saveObject(storeName:string, object:Object): Promise<boolean>{
    return new Promise<boolean>((resolve, reject) => {
      if (this.indexedDbDoesNotSupported()){
        return resolve(false);
      }

      this.getTransactionStore(storeName)
        .then(store => {
          let request = store.put(object);

          request.onsuccess = function(e){
            resolve(true);
          };
          request.onerror = function(e){
            console.log('saveObject error : ' + e);
            resolve(false);
          };
        });
    });
  }

  private getObject(storeName:string, id:string): Promise<any>{
    return new Promise<boolean>((resolve, reject) => {
      if (this.indexedDbDoesNotSupported()){
        return resolve(null);
      }

      this.getTransactionStore(storeName)
        .then(store => {
          let request = store.get(id);
          request.onsuccess = function(event) {
            resolve(request.result);
          };
          request.onerror = function(e) {
            console.log('getObject error : ' + e);
            resolve(false);
          };
        });
    });
  }

  private getTransactionStore(storeName:string): Promise<any>{
    return new Promise<boolean>((resolve, reject) => {
      this.initDatabase()
        .then(request =>{
          request = indexedDB.open(this.dbName, this.dbVersion);
          request.onsuccess = function(event:any) {
            resolve(event.target.result.transaction([storeName], "readwrite").objectStore(storeName));
          };
          request.onerror = function(e) {
            console.log('getTransactionStore error : ' + e);
            reject(false);
          };
        });
    });
  }

  public initDatabase(): Promise<IDBOpenDBRequest>{
    return new Promise<IDBOpenDBRequest>((resolve, reject) => {
      if (this.indexedDbDoesNotSupported()) {
        console.log("IndexedDB is not supported.")
        return resolve(null);
      }

      let self = this;
      let request = indexedDB.open(this.dbName, this.dbVersion);
      request.onupgradeneeded = function(event:any) {
        let db = event.target.result;
        db.createObjectStore(self.stores.apartmentFilters, { keyPath: "PK_id" });
        
        resolve(request);
      };
      request.onerror = function(event) {
        console.log("IndexedDB access denied.")
        resolve(null);
      };
      resolve(request);
    });
  }

  private indexedDbDoesNotSupported(): boolean{
    return !window.indexedDB;
  }

}
