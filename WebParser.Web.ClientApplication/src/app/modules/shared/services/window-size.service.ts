import { Injectable, NgZone } from "@angular/core";
import { Observable, BehaviorSubject, Subject } from 'rxjs';

import { WindowSizeModel } from "../models/window-size.model"

@Injectable()
export class WindowSizeService {
  public readonly minDesktopWindowWidth:number = 768;

  public width$: Observable<number>;
  public height$: Observable<number>;

  private mobileView = new Subject<boolean>();
  mobileView$ = this.mobileView.asObservable();

  constructor(ngZone: NgZone) {
    let windowSize$ = new BehaviorSubject(this.getWindowSize());
    this.width$ = (windowSize$.pluck('width') as Observable<number>).distinctUntilChanged();
    this.height$ = (windowSize$.pluck('height') as Observable<number>).distinctUntilChanged();

    let self = this;
    window.onresize = (e) =>
    {
      ngZone.run(() => {
        windowSize$.next(self.getWindowSize());
      });
    };

    this.width$
      .subscribe(x => self.setMobileView(self, x));
  }

  public getWindowSize(): WindowSizeModel {
    return new WindowSizeModel(window.innerWidth, window.innerHeight);
  }

  public getMobileMode():boolean{
    return this.getWindowSize().width < this.minDesktopWindowWidth;
  }
  
  private setMobileView(self: WindowSizeService, windowWidth:number){
    if (windowWidth < self.minDesktopWindowWidth) {
      self.mobileView.next(true);
    } else {
      self.mobileView.next(false);
    }
  }
}

