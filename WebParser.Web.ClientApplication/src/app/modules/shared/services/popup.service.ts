import { Injectable, TemplateRef } from "@angular/core";
import { ComponentType } from '@angular/cdk/portal';

import { LocalStorageService } from "app/modules/shared/services/local-storage.service";
import { ApiController } from "app/modules/shared/api/api.controller";
import { PublicsModel } from "../models/publics.model";
import { PublicItemModel } from "../models/public-item.model";
import { MatDialog } from "@angular/material";
import { PublicsPopupContent } from "../components/publics-popup-content.component";

@Injectable()
export class PopupService {

  constructor(
    private dialog: MatDialog,
    private localStorageService: LocalStorageService,
    private apiController: ApiController) {
  }

  public showConfirmationPopup<TComponent>(componentOrTemplateRef: ComponentType<TComponent> | TemplateRef<TComponent>, data: any = null): Promise<any>{
    let self = this;
    return new Promise((resolve, reject) => {

      let dialogRef = self.dialog.open(componentOrTemplateRef, {
        width: "500px",
        data: data,
      });

      dialogRef.afterClosed().subscribe(result => {
        resolve(result);
      });
    });
  }

  public async showPublicsPopup(sourceUserId){
    let publics = await this.getPublics(sourceUserId);
    let dialogRef = this.dialog.open(PublicsPopupContent, {
      width: "500px",
      data: publics,
    });

    let self = this;
    dialogRef.afterClosed().subscribe(result => {
      self.localStorageService.savePublicsPopupAlreadySeen();
    });
  }

  private async getPublics(sourceUserId: number): Promise<PublicItemModel[]> {
    let publics = await this.apiController.getPublics() as PublicsModel[];
    let foundPublics = publics.firstOrDefault(x => x.sourceUserId == sourceUserId);
    return foundPublics == null ? [] : foundPublics.publics;
  }
}