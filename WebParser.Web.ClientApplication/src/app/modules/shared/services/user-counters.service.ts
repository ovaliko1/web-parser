import { Injectable, EventEmitter } from "@angular/core";
import { Response } from "@angular/http";
import "rxjs/add/operator/map";
import 'rxjs/add/operator/toPromise';

import { UserCountersModel } from "../models/user-counters.model";

import { ApiController } from "app/modules/shared/api/api.controller";
import { Subject } from "rxjs/Subject";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class UserCountersService {
  private currentUserCounters = new BehaviorSubject<UserCountersModel>(new UserCountersModel())
  
  // Observable object stream
  public currentUserCounters$ = this.currentUserCounters.asObservable();

  constructor(private apiController: ApiController) {}

  public async getUserCounters(): Promise<UserCountersModel> {
    let lastValue = await this.apiController
      .getUserCounters();
    this.currentUserCounters.next(lastValue);
    return lastValue;
  }

  public getLastFetchedUserCounters(): UserCountersModel {
    return this.currentUserCounters.getValue();
  }

  public async tryExtendUserAccessForSharing() {
    return await this.apiController
      .tryExtendUserAccessForSharing();
  }

  public refreshUserCounters(){
    this.getUserCounters();
  }
}

