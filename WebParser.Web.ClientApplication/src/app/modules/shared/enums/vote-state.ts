export enum VoteState {
  Hidden,
  ShowVote,
  VoteCompletedThankYou,
  VoteCompletedThankYouPossiblyRented,
  VoteCompletedWillCheckAndAddDays,
  VoteCompletedWillCheckAndAddOpenings,
  ClickerCaution,
}