export enum ApartmentsScreenState {
  Unknown = 0,
  ApartmentList = 1,
  ApartmentDetails = 2,
  ApartmentListAndDetails = 3,
}