export enum VoteResultType {
  Success = 1,
  MaxFreeDaysCountReached = 2,
  ClickerCaution = 3,
}