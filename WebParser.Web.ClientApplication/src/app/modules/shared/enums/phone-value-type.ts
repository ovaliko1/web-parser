export enum PhoneValueType {
  String = 1,
  Image = 2,
  Link = 3,
}