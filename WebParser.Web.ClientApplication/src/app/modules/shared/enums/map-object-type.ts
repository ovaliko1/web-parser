export enum MapObjectType {
  Metro = 1,
  Grocery = 2,
  School = 3,
  Pharmacy = 4,
}