import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

declare var application: any;

@Directive({
    selector: '[metric-goal]'
})
export class MetricGoalDirective {

  @Input("metric-goal") goalName: string;

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2){
  }

  ngOnInit(){
    if (application.settings.renderMetrics) {
      this.renderer.setAttribute(this.elementRef.nativeElement, "onclick", `${application.settings.yandexMetrikaCounterName}.reachGoal('${this.goalName}'); return true;`);
    }
  }
}