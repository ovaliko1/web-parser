import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { VoteType } from "app/modules/shared/enums/vote-type";
import { HttpClient } from "./http-client ";

@Injectable()
export class ApiController {

  constructor(private http: HttpClient) {}

  public getApartments(page: number, pageSize: number, filters) : Promise<any> {
    if (process.env.ENV === 'mockdata') {
      return this.http.get("/app-public/apartment-list.json");
    }

    return this.http.post("/Apartments/GetApartments", {
      page: page,
      pageSize: pageSize,
      filters: filters
    });
  }

  public getApartmentById(id: string) : Promise<any> {
    if (process.env.ENV === 'mockdata') {
      return this.http.get(`/app-public/${id}.json`);
    }

    return this.http.post("/Apartments/GetApartment", {
      id: id,
    });
  }

  public getContactInfo(apartmentId: string): Promise<any> {
    if (process.env.ENV === 'mockdata') {
      return this.http.get(`/app-public/contactInfo_${apartmentId}.json`);
    }

    return this.http.post("/Apartments/GetContactInfo", {
      apartmentId: apartmentId,
    });
  }

  public getFiltersInitData(cityId?: number) : Promise<any> {
    if (process.env.ENV === 'mockdata') {
      return cityId == 2 ? this.http.get("/app-public/msk-filters-init-data.json") : this.http.get("/app-public/spb-filters-init-data.json") ;
    }

    return this.http.post("/Apartments/GetFiltersInitData", {
      cityId: cityId
    });
  }

  public getUserCounters() : Promise<any> {
    if (process.env.ENV === 'mockdata') {
      return this.http.get("/app-public/user-counters.json");
    }

    return this.http.get("/Data/GetUserCounters");
  }

  public tryExtendUserAccessForSharing() : Promise<any> {
    if (process.env.ENV === 'mockdata') {
      return;
    }

    return this.http.post("/Data/TryExtendUserAccessForSharing", null);
  }

  public voteForApartment(apartmentId: number, voteType: VoteType) : Promise<any>{
    if (process.env.ENV === 'mockdata') {
      return this.http.get(`/app-public/vote-result-${apartmentId}.json`);
    }

    return this.http.post("/Vote/VoteForApartment", {
      apartmentId: apartmentId,
      voteType: voteType
    });
  }

  public getPaymentFormSettings() : Promise<any> {
    if (process.env.ENV === 'mockdata') {
      return this.http.get("/app-public/payment-form.json");
    }

    return this.http.post("/Forbidden/GetPaymentFormSettings", null);
  }

  public getPublics() : Promise<any> {
    if (process.env.ENV === 'mockdata') {
      return this.http.get("/app-public/publics.json");
    }

    // TODO: create server logic for this
    return this.http.get("/app-public/publics.json");
  }
}
