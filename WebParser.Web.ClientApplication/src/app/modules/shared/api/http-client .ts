import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { Response } from "@angular/http";

@Injectable()
export class HttpClient {

  constructor(private http: Http) {}

  get(url) : Promise<any> {
    let self = this;
    return this.http.get(url, this.getAjaxHeader())
      .map((res: Response) => self.handleErrors(res.json()))
      .toPromise()
  }

  post(url, data) : Promise<any>{
    let self = this;
    return this.http.post(url, data, this.getAjaxHeader())
      .map((res: Response) => self.handleErrors(res.json()))
      .toPromise()
  }

  private getAjaxHeader() {
    let headers = new Headers();
    headers.append("X-Requested-With", "XMLHttpRequest");
    return {
      headers: headers
    };
  }

  private handleErrors(json: any) : Promise<any>{
    if (json.HasErrors) {
      return Promise.reject(json.SummaryErrors);
    }
    return Promise.resolve(json);
  }
}