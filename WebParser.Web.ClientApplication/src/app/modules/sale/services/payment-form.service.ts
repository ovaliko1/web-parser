import { Injectable } from "@angular/core";
import { Response } from "@angular/http";

import "rxjs/add/operator/map";
import 'rxjs/add/operator/toPromise';

import { PaymentFormSettings } from "../models/payment-form-settings.model";

import { ApiController } from "app/modules/shared/api/api.controller";

@Injectable()
export class PaymentFormService {

  constructor(
    private apiController: ApiController)
  {}

  async getPaymentFormSettings(): Promise<PaymentFormSettings> {
    return await this.apiController
      .getPaymentFormSettings();
  }
}