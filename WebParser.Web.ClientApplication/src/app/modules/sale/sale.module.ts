import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SharedModule } from "../shared/shared.module";

import { AccessExpired } from "./components/access-expired.component";
import { PaymentForm } from "./components/payment-form.component";
import { PaymentFormService } from "./services/payment-form.service";

@NgModule({
  declarations: [
    AccessExpired,
    PaymentForm,
  ],
  imports: [
    SharedModule,
  ],
  providers: [PaymentFormService],
  exports: [
    AccessExpired,
    PaymentForm,
  ],
  bootstrap: [AccessExpired]
})
export class SaleModule { }
