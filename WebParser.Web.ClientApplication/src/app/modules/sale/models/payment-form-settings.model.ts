export class PaymentFormSettings {
  firstRateOpeningsCount:number;
  firstRateOpeningsPrice:number;
  secondRateOpeningsCount:number;
  secondRateOpeningsPrice:number;
  thirdRateOpeningsCount:number;
  thirdRateOpeningsPrice:number;
  fourthRateOpeningsCount:number;
  fourthRateOpeningsPrice:number;
  
  firstRateDaysCount:number;
  firstRateDaysPrice:number;
  secondRateDaysCount:number;
  secondRateDaysPrice:number;
  thirdRateDaysCount:number;
  thirdRateDaysPrice:number;
  fourthRateDaysCount:number;
  fourthRateDaysPrice:number;

  successUrl:string;
  receiver:string;
  label:string;
}
