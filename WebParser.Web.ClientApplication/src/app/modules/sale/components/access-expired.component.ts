import { Component } from "@angular/core";

@Component({
  selector: "access-expired",
  templateUrl: "../templates/access-expired.component.html",
  styles: ["../templates/styles/access-expired.scss"]
})
export class AccessExpired {
}
