import { Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MatSliderChange, MatRadioGroup } from "@angular/material";
import { PaymentFormSettings } from "../models/payment-form-settings.model";
import { PaymentFormService } from "../services/payment-form.service";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { RateType } from "app/modules/shared/enums/rate-type";

@Component({
  selector: "payment-form",
  templateUrl: "../templates/payment-form.component.html",
  styles: ["../templates/styles/payment-form.scss"]
})
export class PaymentForm implements OnInit {
  RateType: any = RateType;

  public paymentType: string = "AC";

  public settings: PaymentFormSettings = new PaymentFormSettings();

  constructor(
    private paymentFormService: PaymentFormService,
    private activatedRoute: ActivatedRoute,
  ) {}

  async ngOnInit() {
    let self = this;
    this.settings = await this.paymentFormService.getPaymentFormSettings();
  }

  public pricePerItem(price: number, itemsCount: number){
    return price / itemsCount >> 0;
  }

  public submit(rateType: RateType, price: number, itemsCount: number){
    this.post("https://money.yandex.ru/quickpay/confirm.xml", {
      "receiver" : this.settings.receiver,
      "formcomment" : "«Pullmeout» - Первый агрегатор объявлений от собственников",
      "short-dest" : "Продление доступа",
      "label" : `${this.settings.label}_${rateType}_${price}_${itemsCount}`,
      "quickpay-form" : "shop",
      "targets" : "Доступ к объявлениям от собственников",
      "comment" : "Доступ к объявлениям от собственников",
      "need-fio" : "false",
      "need-email" : "false",
      "need-phone" : "false",
      "need-address" : "false",
      "successURL" : this.settings.successUrl,
      "paymentType": this.paymentType,
      "sum": price,
    })
  }

  private post(path, params):void {
    let method = "post";

    let form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    form.setAttribute("style", "display:none");

    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);

        form.appendChild(hiddenField);
      }
    }

    document.body.appendChild(form);
    form.submit();
  }
}
