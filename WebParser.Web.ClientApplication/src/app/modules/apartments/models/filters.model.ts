import { FiltersInitModel } from "./filters-init.model";
import { ChosenFiltersModel } from "./chosen-filters.model";

export class FiltersModel{
  public chosenFiltersModel: ChosenFiltersModel;
  public filtersInitModel: FiltersInitModel;  
}