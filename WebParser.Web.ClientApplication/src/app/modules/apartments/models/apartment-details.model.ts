import { MetroItemModel } from "./metro-item.model";
import { PhoneItemModel } from "./phone-item.model";
import { ImageInfo } from "app/modules/shared/models/image-info.model";

export class ApartmentDetailsModel {
  id: string;
  subject: string;
  price: string;
  metroItems: MetroItemModel[];
  sourceMetro: string[];
  address: string;
  sourceAddress:string[];
  latitude:number;
  longitude:number;
  mapUrl:string;
  createdDate: string;
  ownerNames: string[];
  phoneState: number;
  openPhoneUrl: string;
  phones: PhoneItemModel[];
  description: string[];
  characteristics: string[];
  sourceDates: string[];
  possiblyRented: boolean;
  inBlackList: boolean;
  inWhiteList: boolean;
  hideBlacklistWarranty: boolean;
  fullSizeImages: ImageInfo[];
}
