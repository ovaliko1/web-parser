import { ItemModel } from "./item.model"

export class ApartmentTypeModel{
  ids: string;
  name: string;
}

export class FiltersInitModel{
  defaultCityId: number
  cities: Array<ItemModel>;
  sources: Array<ItemModel>;
  apartmentTypes: Array<ApartmentTypeModel>;
  metroStations: Array<ItemModel>;
}