import { ApartmentModel } from "app/modules/apartments/models/apartment.model";

export class ApartmentsModel {
  hasMore: boolean;
  items: ApartmentModel[];
}
