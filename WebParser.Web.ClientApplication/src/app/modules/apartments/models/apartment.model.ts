import { ImageInfo } from "app/modules/shared/models/image-info.model";

export class ApartmentModel {
  id: string;
  createdDate: string;
  createdDateStr: string;
  siteId: number;
  articlesGroup: string;
  price: string;
  metro: string;
  metroDistance: string;
  metroDuration: string;
  goodRating: number;
  badRating: number;
  outdateRating: number;
  source: string;
  showRating: boolean;
  inWhiteList: boolean;
  possiblyRented: boolean;
  hideBlacklistWarranty: boolean;
  imagesPreview: ImageInfo[];
}
