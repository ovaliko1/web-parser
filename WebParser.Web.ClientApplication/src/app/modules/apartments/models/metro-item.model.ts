export class MetroItemModel {
  metro: string;
  metroDuration: string;
  metroDistance: string;
}
