import { VoteResultType } from "app/modules/shared/enums/vote-result-type";

export class VoteResultModel {
  voteResultType: VoteResultType;
}