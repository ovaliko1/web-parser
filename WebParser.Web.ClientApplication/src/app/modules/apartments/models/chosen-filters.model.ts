export class ChosenApartmentTypeModel {
  constructor(
      public value: string,
      public name: string,
      public checked: boolean)
    { }
}

export class ChosenFiltersModel{
  public apartmentId: string;
  public cityId: number;
  public sourceIds: Array<string>;
  public metroStationIds: Array<string>;
  public apartmentTypes: Array<ChosenApartmentTypeModel>;
  public minutesToMetro: number;
  public priceFrom: number;
  public priceTo: number;
  public showWithEmptyMetro: boolean;
  public showWithEmptyPrice: boolean;
}

export class CurrentChosenFiltersModel{
  constructor(
    public chosenFilters: ChosenFiltersModel,
    public chosenByUser: boolean)
  { }
}