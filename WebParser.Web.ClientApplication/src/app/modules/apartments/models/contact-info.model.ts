import { PhoneItemModel } from "./phone-item.model";

export class ContactInfoModel {
  phones: PhoneItemModel[];
  showForbidden: boolean;
  canBeVoted: boolean;
}