import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";

import { Apartments } from "./components/apartments.component";
import { ApartmentsSideNavigation } from "./components/apartments-side-navigation.component";
import { ApartmentsList } from "./components/apartments-list.component";
import { ApartmentsItem } from "./components/apartments-item.component";
import { ApartmentsDetail } from "./components/apartments-detail.component";
import { ApartmentsFilters } from "./components/apartments-filters.component";
import { ApartmentsVote } from "./components/apartments-vote.component";
import { ApartmentsContactInfo } from "./components/apartments-contact-info.component";

import { ApartmentsListService } from "./services/apartments-list.service";
import { ApartmentsDetailService } from "./services/apartments-detail.service";
import { ApartmentsFiltersService } from "./services/apartments-filters.service";
import { NotificationsService } from "./services/notifications.service";

import { ApartmentsVoteService } from "./services/apartments-vote.service";
import { PublicsPopup } from "./components/publics-popup.component";

import { Empty } from "../shared/components/empty.component";
import { OpenContactConfirmation } from "./components/open-contact-confirmation.component";
import { NotificationsConfirmation } from "./components/notifications-confirmation.component";
import { ApartmentsScreenService } from "./services/apartments-screen.service";

@NgModule({
  declarations: [
    Apartments,
    ApartmentsSideNavigation,
    ApartmentsList,
    ApartmentsItem,
    ApartmentsDetail,
    ApartmentsFilters,
    ApartmentsVote,
    ApartmentsContactInfo,
    PublicsPopup,
    OpenContactConfirmation,
    NotificationsConfirmation,
  ],
  imports: [
    SharedModule,
  ],
  providers: [ApartmentsDetailService, ApartmentsListService, ApartmentsFiltersService, ApartmentsVoteService, NotificationsService, ApartmentsScreenService],
  exports: [Apartments],
  entryComponents: [OpenContactConfirmation, NotificationsConfirmation],
  bootstrap: [Apartments]
})
export class ApartmentsModule { }
