import { Component, SimpleChanges, Input, OnInit, OnDestroy } from "@angular/core";
import { trigger, state, style, animate, transition } from '@angular/animations';
import { NgModel} from "@angular/forms";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";

import { FiltersModel,  } from "../models/filters.model";
import { ChosenFiltersModel, ChosenApartmentTypeModel } from "../models/chosen-filters.model";
import { FiltersInitModel, ApartmentTypeModel } from "../models/filters-init.model";
import { ItemModel } from "../models/item.model";
import { ApartmentsFiltersService } from "../services/apartments-filters.service";
import { Router } from "@angular/router";
import { WindowSizeService } from "app/modules/shared/services/window-size.service";
import { Subscription } from "rxjs";
import { MatSelectChange, MatOption } from "@angular/material";

@Component({
  selector: "apartments-filters",
  templateUrl: "../templates/apartments-filters.component.html",
  styles: ["../templates/styles/apartments-filters.scss"],
  animations: [
    trigger('toggleState', [
      state('true' , style({  })),
      state('false', style({ maxHeight: 0, padding: 0, display: 'none' })),
      // transition
      transition('* => *', animate('300ms')),
    ])
  ]
})
export class ApartmentsFilters implements OnInit, OnDestroy {
  public filters: ChosenFiltersModel = new ChosenFiltersModel();
  public cities: Array<ItemModel>;
  public sources: Array<ItemModel>;
  public metroStations: Array<ItemModel>;
  public applyButtonTitle: string = 'Применить';
  public applyButtonChevronIcon: string = 'keyboard_arrow_up';
  public tooltipPosition = "right";
  public display: string = 'block';
  public apartmentTypes: Array<ChosenApartmentTypeModel>;
  public apartmentTypeIds: Array<string>;
  public mobileView: boolean;

  private closedFiltersButtonTitle: string = 'Фильтры';
  private openedFiltersButtonTitle: string = 'Применить';

  private closedFiltersButtonIcon: string = 'keyboard_arrow_down';
  private openedFiltersButtonIcon: string = 'keyboard_arrow_up';

  private mobileViewSubscription: Subscription;
  
  constructor(
    private apartmentsFiltersService: ApartmentsFiltersService,
    private router: Router,
    private windowSizeService: WindowSizeService) {}

  async ngOnInit() {
    await this.initFilters();
    await this.applyFilters(true);

    let self = this;
    this.mobileView = this.windowSizeService.getMobileMode();
    this.mobileViewSubscription = this.windowSizeService.mobileView$.subscribe(x => {
      self.mobileView = x
      self.setTooltipPosition(self);
    });

    this.setTooltipPosition(this);
  }

  ngOnDestroy(): void {
    this.mobileViewSubscription.unsubscribe();
  }

  public async cityChanged(event: MatSelectChange){
    await this.initFilters(event.value);
  }

  private async initFilters(cityId?: number){
    let currentFilters = await this.apartmentsFiltersService.getCurrentFilters(cityId);
    this.filters = currentFilters.chosenFiltersModel;
    let initData = currentFilters.filtersInitModel;
    this.cities = initData.cities;
    this.sources = initData.sources;
    this.metroStations = initData.metroStations;
    this.apartmentTypeIds = this.filters.apartmentTypes.filter(x => x.checked).map(x => x.value);
  }

  public closeFilters() {
    this.applyButtonTitle = this.closedFiltersButtonTitle;
    this.applyButtonChevronIcon = 'keyboard_arrow_down';
    this.display = 'none';
  }

  cutInput(length, modelFieldName) {
    var input = this.filters[modelFieldName];
    if(input == null){
      return;
    }

    var inputStr = input.toString();
    if(inputStr.length > length){
      this.filters[modelFieldName] = inputStr.substring(0, length);
    }
  }

  async applyFilters(force?:boolean){
    if (this.display === 'block' || force){
      await this.apartmentsFiltersService.setFilters(this.filters, !force);
      if (!force){
        this.router.navigate(['/apartments']);
        this.closeFilters();
      }
    } else {
      this.applyButtonTitle = this.openedFiltersButtonTitle;
      this.applyButtonChevronIcon = this.openedFiltersButtonIcon;
      this.display = 'block';
    }
  }

  apartmentTypeToggled(event){
    var apartmentType = this.filters.apartmentTypes.find(x => x.value == event.value);
    apartmentType.checked = event.source.checked;
  }

  apartmentTypesSelectionChange(event:MatSelectChange){
    this.filters.apartmentTypes.forEach(apartmentType => {
      apartmentType.checked = event.value.indexOf(apartmentType.value) != -1;  
    });
  }

  setTooltipPosition(self: ApartmentsFilters) {
    self.tooltipPosition = self.mobileView ? "below" : "right";
  }
}