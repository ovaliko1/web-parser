import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { WindowSizeService } from "app/modules/shared/services/window-size.service";
import { Subscription } from "rxjs/Subscription";
import { ApartmentsList } from "./apartments-list.component";
import { ApartmentsFilters } from "./apartments-filters.component";
import { ApartmentsScreenService } from "../services/apartments-screen.service";
import { ApartmentsScreenState } from "app/modules/shared/enums/apartments-screen-state";

@Component({
  selector: "side-navigation",
  templateUrl: "../templates/apartments-side-navigation.component.html",
  styles: ["../templates/styles/apartments-side-navigation.scss"]
})
export class ApartmentsSideNavigation implements OnInit, OnDestroy {
  @ViewChild('sidenav') sidenavControl;
  @ViewChild('apartmentsList') apartmentsList: ApartmentsList;
  @ViewChild('appContent') desktopAppContent:ElementRef;
  @ViewChild('desktopFilters') apartmentsFilters:ApartmentsFilters;

  public sidenavMode:string = "side";
  public sidenavWidth:number = 240;
  public mobileView: boolean;

  private subscription: Subscription;
  private isInLoading: boolean = false;
  private apartmentListOpened: boolean;

  constructor(
    private windowSizeService: WindowSizeService,
    private apartmentsScreenService: ApartmentsScreenService) {
  }

  async onScroll(event: Event) {
    if (this.isInLoading) {
      return;
    }

    let target = event.srcElement;

    if (target.scrollHeight - (target.scrollTop + target.clientHeight) <= 100) {
      this.isInLoading = true;
      await this.apartmentsList.appendApartments();
      this.isInLoading = false;
    }
  }

  ngOnInit(): void {
    var self = this;

    this.mobileView = this.windowSizeService.getMobileMode();
    this.apartmentListOpened = window.location.search.indexOf("apartmentId") == -1;

    this.setApartmentsScreenState(this.apartmentListOpened);

    this.subscription = this.windowSizeService.width$
      .subscribe(x => self.setMobileView(self, x));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  setMobileView(self, windowWidth:number){
    if (windowWidth < self.windowSizeService.minDesktopWindowWidth) {
      self.mobileView = true;
      self.sidenavMode = 'side';
      self.sidenavWidth = windowWidth;
    } else {
      self.mobileView = false;
      self.sidenavMode = 'side';
      self.sidenavWidth = 400;
    }
  }

  onBack() {
    this.sidenavControl.open();
    this.setApartmentsScreenState(true);
  }

  onMobileApartmentClick(){
    this.sidenavControl.close();
    this.setApartmentsScreenState(false);
  }

  onDesktopApartmentClick(){
    this.apartmentsFilters.closeFilters();
    this.desktopAppContent.nativeElement.parentElement.scrollTop = 0;
  }

  private setApartmentsScreenState(apartmentListOpened: boolean){
    this.apartmentsScreenService.setApartmentsScreenState(
      this.mobileView
        ? apartmentListOpened ? ApartmentsScreenState.ApartmentList : ApartmentsScreenState.ApartmentDetails
        : ApartmentsScreenState.ApartmentListAndDetails);
  }
}
