import { Component, Input, OnInit, OnDestroy, Inject } from "@angular/core";
import { MatDialog, MatDialogActions, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "open-contact-confirmation",
  templateUrl: "../templates/notifications-confirmation.component.html",
  styles: ["../templates/styles/apartments-detail.scss"]
})
export class NotificationsConfirmation {
  constructor(
    public dialogRef: MatDialogRef<NotificationsConfirmation>)
  {}

  onNoClick(): void {
    this.dialogRef.close(false);
  }
}
