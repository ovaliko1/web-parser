import { Component, Input } from "@angular/core";
import { PhoneItemModel } from "../models/phone-item.model";

import { PhoneValueType } from "app/modules/shared/enums/phone-value-type";

@Component({
  selector: "apartments-contact-info",
  templateUrl: "../templates/apartments-contact-info.component.html",
  styles: ["../templates/styles/apartments-contact-info.scss"]
})
export class ApartmentsContactInfo {
  @Input() phones: PhoneItemModel[];

  PhoneValueType: any = PhoneValueType;
}
