import { Component, Input, OnInit, OnDestroy, Inject } from "@angular/core";
import { MatDialog, MatDialogActions, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: "open-contact-confirmation",
  templateUrl: "../templates/open-contact-confirmation.component.html",
  styles: ["../templates/styles/apartments-detail.scss"]
})
export class OpenContactConfirmation {
  constructor(
    public dialogRef: MatDialogRef<OpenContactConfirmation>,
    @Inject(MAT_DIALOG_DATA) public data: boolean)
  {}

  onNoClick(): void {
    this.dialogRef.close(false);
  }
}
