import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, OnInit, OnDestroy } from "@angular/core";
import { ApartmentModel } from "../models/apartment.model";
import { MatTabGroup } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from 'rxjs/Subscription';
import { NotificationsService } from "../services/notifications.service";

@Component({
  selector: "apartments-item",
  templateUrl: "../templates/apartments-item.component.html"
})
export class ApartmentsItem implements OnInit, OnDestroy {

  @Input() apartment: ApartmentModel;
  @Output() onItemClick = new EventEmitter();

  @ViewChild('apartmentLink') apartmentLink: any;
  @ViewChild('imagesPreview') imagesPreview: MatTabGroup;

  public active: boolean = false;

  private querySubscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private notificationsService: NotificationsService)
  {
  }

  ngOnInit(): void {
    let self = this;
    this.querySubscription = this.activatedRoute.queryParams.subscribe(x => {
      self.active = x['apartmentId'] == self.apartment.id;
    });
  }

  ngOnDestroy(): void {
    this.querySubscription.unsubscribe();
  }

  public raiseOnItemClick(){
    this.onItemClick.emit();
    this.notificationsService.showNotificationsRequest();
  }

  public raiseLinkClick(){
    this.apartmentLink._getHostElement().click();
  }

  public showPreviousImage(){
    let selectedIndex = this.imagesPreview.selectedIndex;
    if (selectedIndex > 0){
      this.imagesPreview.selectedIndex--;
    }
  }

  public showNextImage(){
    let selectedIndex = this.imagesPreview.selectedIndex;
    if (selectedIndex < this.apartment.imagesPreview.length - 1){
      this.imagesPreview.selectedIndex++;
    }
  }
}
