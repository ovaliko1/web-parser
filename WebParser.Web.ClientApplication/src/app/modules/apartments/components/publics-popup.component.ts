import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { MatDialog, MatDialogActions } from "@angular/material";
import { ApartmentsFiltersService } from "../services/apartments-filters.service";
import { Subscription } from "rxjs";
import { LocalStorageService } from "app/modules/shared/services/local-storage.service";
import { PopupService } from "app/modules/shared/services/popup.service";

@Component({
  selector: "publics-popup",
  template: ``
})
export class PublicsPopup implements OnInit, OnDestroy  {

  private subscription: Subscription;

  constructor(
    private publicsDialog: MatDialog,
    private publicsPopupService: PopupService,
    private apartmentsFiltersService: ApartmentsFiltersService,
    private localStorageService: LocalStorageService){
  }

  async ngOnInit(){
    let self = this;
    this.subscription = this.apartmentsFiltersService.currentFilters$.subscribe(x => {
      if (!x.chosenByUser || self.localStorageService.getPublicsPopupAlreadySeen()){
        return;
      }

      setTimeout(() => self.publicsPopupService.showPublicsPopup(x.chosenFilters.cityId), 10000);
      this.subscription.unsubscribe();
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
