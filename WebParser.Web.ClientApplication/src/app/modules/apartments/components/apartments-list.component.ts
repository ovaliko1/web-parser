import { Directive, Component, Output, EventEmitter, OnInit, OnDestroy, ElementRef } from "@angular/core";
import { Router, ActivatedRoute, Params} from '@angular/router';

import { ApartmentModel } from "../models/apartment.model";
import { ApartmentsModel } from "../models/apartments.model";
import { FiltersModel } from "../models/filters.model";

import { ApartmentsListService } from "../services/apartments-list.service";
import { ApartmentsDetailService } from "../services/apartments-detail.service";
import { ApartmentsFiltersService } from "../services/apartments-filters.service";
import { Subscription } from 'rxjs/Subscription';
import { ChosenFiltersModel } from "app/modules/apartments/models/chosen-filters.model";
import { IndexedDBService } from "app/modules/shared/services/Indexed-db-service";
import { WindowSizeService } from "../../shared/services/window-size.service";

@Component({
  selector: "apartments-list",
  templateUrl: "../templates/apartments-list.component.html",
  styles: ["../templates/styles/apartments-list.scss"],
})
export class ApartmentsList implements OnInit, OnDestroy {
  private currentPage:number = 0;
  private readonly pageSize: number = 25;
  private subscription: Subscription;
  private refreshListSubscription: Subscription;
  
  list: ApartmentModel[];
  hasMore: boolean = false;

  @Output() onApartmentClick = new EventEmitter();

  constructor(
    private listService: ApartmentsListService,
    private detailService: ApartmentsDetailService,
    private apartmentsFiltersService: ApartmentsFiltersService,
    private indexedDBService: IndexedDBService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private hostElement:ElementRef,
    private windowSizeService: WindowSizeService) {
  }

  async ngOnInit() {
    let self = this;
    this.subscription = this.apartmentsFiltersService.currentFilters$.subscribe(x => {
      self.hostElement.nativeElement.parentElement.scrollTop = 0
      self.readApartments(self, x.chosenFilters)
    });
    this.refreshListSubscription = this.listService.needToRefreshApartmentsList.subscribe(() => self.refresh());
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.refreshListSubscription.unsubscribe();
  }

  public async appendApartments() {
    if(!this.hasMore){
      return;
    }

    let filters = await this.apartmentsFiltersService.getCurrentFilters();
    let apartments = await this.listService.getApartments(this.currentPage, this.pageSize, filters.chosenFiltersModel);

    this.list.addRange(apartments.items);
    this.hasMore = apartments.hasMore
    this.currentPage++;
  }

  async refresh() {
    let filters = await this.apartmentsFiltersService.getCurrentFilters();
    await this.readApartments(this, filters.chosenFiltersModel);
  }

  async readApartments(self: ApartmentsList, filters: ChosenFiltersModel) {
    self.currentPage = 0;
    let apartments = await self.listService.getApartments(self.currentPage, self.pageSize, filters);

    self.list = apartments.items;
    self.hasMore = apartments.hasMore
    self.currentPage++;

    let firstItem = self.list.firstOrDefault();
    let activeApartmentId = self.activatedRoute.snapshot.queryParams['apartmentId'];
    if(firstItem && !activeApartmentId && !self.windowSizeService.getMobileMode()) {
      self.router.navigate(['/apartments'], {
          queryParams:{
            'apartmentId': firstItem.id,
          }
        }
      );
    }

    if(firstItem) {
      self.indexedDBService.saveNewestApartmentId(firstItem.id);
    }
  }

  raiseOnApartmentClick(){
    this.onApartmentClick.emit();
  }
}
