import { Component, Input, Output, OnInit, OnDestroy, EventEmitter } from "@angular/core";
import { PhoneItemModel } from "../models/phone-item.model";
import { Subscription } from "rxjs/Subscription";

import { WindowSizeService } from "app/modules/shared/services/window-size.service";
import { UserCountersService } from "app/modules/shared/services/user-counters.service";
import { ApartmentsVoteService } from "../services/apartments-vote.service";
import { ApartmentsListService } from "../services/apartments-list.service";

import { VoteState } from "app/modules/shared/enums/vote-state";
import { VoteType } from "app/modules/shared/enums/vote-type";
import { PhoneState } from "app/modules/shared/enums/phone-state";
import { PhoneValueType } from "app/modules/shared/enums/phone-value-type";
import { VoteResultType } from "app/modules/shared/enums/vote-result-type";
import { RateType } from "../../shared/enums/rate-type";

@Component({
  selector: "apartments-vote",
  templateUrl: "../templates/apartments-vote.component.html",
  styles: ["../templates/styles/apartments-vote.scss"]
})
export class ApartmentsVote implements OnInit, OnDestroy {
  @Input() phones: PhoneItemModel[] = [];
  @Input() voteState: VoteState;
  @Input() phoneState: PhoneState;
  @Input() apartmentId: number;
  @Input() passBlackListState: boolean;
  @Input() possiblyRented: boolean;
  @Input() rateType: RateType;

  VoteType:any = VoteType;
  VoteState:any = VoteState;
  PhoneState:any = PhoneState;
  PhoneValueType:any = PhoneValueType;
  RateType: any = RateType;
  
  private mobileView: boolean;
  private subscription: Subscription;
  private voteType: VoteType;
  private supportEmail: string = "email@email.com";

  constructor(
    private windowSizeService: WindowSizeService,
    private apartmentsVoteService: ApartmentsVoteService,
    private userCountersService: UserCountersService,
    private apartmentsListService: ApartmentsListService) {
  }

  ngOnInit(): void {
    var self = this;

    this.mobileView = this.windowSizeService.getMobileMode();
    this.subscription = this.windowSizeService.mobileView$.subscribe(x => {
      self.mobileView = x
    });
  }

  public async vote(voteType: VoteType){
    let result = await this.apartmentsVoteService.voteForApartment(this.apartmentId, voteType);

    switch(result.voteResultType){
      case VoteResultType.Success:
      case VoteResultType.MaxFreeDaysCountReached:
        this.setVoteState(voteType);
        break;

      case VoteResultType.ClickerCaution:
        this.voteState = VoteState.ClickerCaution;
        break;

      default:
        console.log(`Unknown voteResultType: ${result.voteResultType}`);
        break;
    }

    this.apartmentsListService.refreshApartmentsList();
  }

  private setVoteState(voteType: VoteType){

    switch (voteType) {
      case (VoteType.Good):
        this.voteState = VoteState.VoteCompletedThankYou;
        break;

      case (VoteType.Bad):
        switch (this.rateType) {
          case RateType.Days:
            this.voteState = VoteState.VoteCompletedWillCheckAndAddDays;
            break;

          case RateType.Openings:
            this.voteState = VoteState.VoteCompletedWillCheckAndAddOpenings;
            break;

          default:
            this.voteState = VoteState.VoteCompletedThankYou;
            console.error("Unknown rateType");
            break;
        }
        break;

      case (VoteType.Outdate):
        switch (this.rateType) {
          case RateType.Days:
            this.voteState = this.possiblyRented ? VoteState.VoteCompletedThankYouPossiblyRented : VoteState.VoteCompletedWillCheckAndAddDays;
            break;

          case RateType.Openings:
            this.voteState = this.possiblyRented ? VoteState.VoteCompletedThankYouPossiblyRented : VoteState.VoteCompletedWillCheckAndAddOpenings;
            break;

          default:
            this.voteState = VoteState.VoteCompletedThankYou;
            console.error("Unknown rateType");
            break;
        }
        break;

      default:
          this.voteState = VoteState.VoteCompletedThankYou;
          console.error("Unknown voteType");
          break;
    }
  }

  public getBadVoteButtonText(): string {
    return this.passBlackListState ? "Агентское" : "Агентское";
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
