import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, Params} from '@angular/router';
import { MatButtonToggleChange } from "@angular/material";

import { Subscription } from 'rxjs/Subscription';

import { ApartmentModel } from "../models/apartment.model";
import { ApartmentsDetailService } from "../services/apartments-detail.service";
import { Gallery } from "app/modules/shared/components/gallery.component";
import { WindowSizeService } from "app/modules/shared/services/window-size.service";
import { VoteState } from "app/modules/shared/enums/vote-state";
import { PhoneState } from "app/modules/shared/enums/phone-state";
import { PhoneValueType } from "app/modules/shared/enums/phone-value-type";
import { ApartmentDetailsModel } from "../models/apartment-details.model";
import { ImageInfo } from "app/modules/shared/models/image-info.model";
import { PopupService } from "app/modules/shared/services/popup.service";
import { OpenContactConfirmation } from "./open-contact-confirmation.component";
import { UserCountersService } from "app/modules/shared/services/user-counters.service";
import { RateType } from "app/modules/shared/enums/rate-type";
import { MapObjectType } from "app/modules/shared/enums/map-object-type";
import { ApartmentsScreenService } from "../services/apartments-screen.service";
import { ApartmentsScreenState } from "app/modules/shared/enums/apartments-screen-state";

declare var ymaps: any;

@Component({
  selector: "apartments-detail",
  templateUrl: "../templates/apartments-detail.component.html",
  styles: ["../templates/styles/apartments-detail.scss"]
})
export class ApartmentsDetail implements OnInit, OnDestroy {
  @Output() onBack = new EventEmitter();

  VoteState: any = VoteState;
  PhoneState: any = PhoneState;
  PhoneValueType: any = PhoneValueType;
  MapObjectType: any = MapObjectType;

  public tooltipPosition = "right";

  public mobileView: boolean;
  public apartment: ApartmentDetailsModel = new ApartmentDetailsModel();
  public querySubscription: Subscription;
  public showImages: boolean = true;
  public showMap: boolean = true;
  public voteState: VoteState = VoteState.Hidden;
  public rateType: RateType;
  public map: any;

  private mobileViewSubscription: Subscription;
  @ViewChild('gallery') gallery: Gallery;

  constructor(
    private apartmentsDetailService: ApartmentsDetailService,
    private activatedRoute: ActivatedRoute,
    private windowSizeService: WindowSizeService,
    private router: Router,
    private popupService: PopupService,
    private userCountersService: UserCountersService,
    private apartmentsScreenService: ApartmentsScreenService 
  )
  {
    this.apartment.subject = "Объявление не найдено";
  }

  ngOnInit(): void {
    let self = this;
    this.querySubscription = this.activatedRoute.queryParams.subscribe(x => self.showApartment(self, x));

    this.mobileView = this.windowSizeService.getMobileMode();
    this.mobileViewSubscription = this.windowSizeService.mobileView$.subscribe(x => {
      self.mobileView = x
      self.setTooltipPosition(self);
    });

    this.setTooltipPosition(this);
  }

  ngOnDestroy() {
    this.querySubscription.unsubscribe();
    this.mobileViewSubscription.unsubscribe();
  }

  setTooltipPosition(self: ApartmentsDetail) {
    self.tooltipPosition = self.mobileView ? "below" : "right";
  }

  async showApartment(self: ApartmentsDetail, query: any){
    self.voteState = VoteState.Hidden;

    let apartmentId = query['apartmentId'];
    if (apartmentId) {
      self.apartment = await self.apartmentsDetailService.getApartmentById(apartmentId);

      if(self.apartment.phoneState == PhoneState.ShowContactsAndVotePossibility){
        self.voteState = VoteState.ShowVote;
      }

      let images = self.apartment.fullSizeImages;

      self.showImages = images.length > 0;

      await self.gallery.setImages(images);
    }

    self.resetMap(self);
  }

  private resetMap(self:ApartmentsDetail){
    let state = this.apartmentsScreenService.getApartmentsScreenState();
    if (state == ApartmentsScreenState.ApartmentList){
      return;
    }

    self.showMap = self.apartment && !!self.apartment.longitude && !!self.apartment.latitude;
    if(!self.showMap){
      return;
    }

    let center = [self.apartment.latitude, self.apartment.longitude];
    let zoom = 14;

    if (self.map) {
      self.map.geoObjects.removeAll();
      self.map.setCenter(center);
      self.map.setZoom(zoom);
      self.setApartmentPlacemark(self, center);
    } else {
      ymaps.ready(() => {
        self.map = new ymaps.Map('map', {
          center: center,
          zoom: zoom,
          controls: ['fullscreenControl', 'typeSelector', 'routeButtonControl', 'zoomControl', 'rulerControl']
        });
        self.map.behaviors.disable('scrollZoom');
        self.setApartmentPlacemark(self, center);
      });
    }
  }

  private setApartmentPlacemark(self, center){
    let placeMark = new ymaps.Placemark(
      center, {
          balloonContentHeader: self.apartment.subject,
          balloonContentBody: self.apartment.metroItems.map(metro => `${metro.metro} ${metro.metroDuration} ${metro.metroDistance}`).join(' '),
          balloonContentFooter: self.apartment.address,
          hintContent: self.apartment.subject
      }, {
        preset: 'islands#greenHomeIcon'
      }
    );

    self.map.geoObjects.add(placeMark);
  }

  public async showContact(){
    let self = this;
    this.rateType = this.userCountersService.getLastFetchedUserCounters().rateType;
    if(this.rateType == RateType.Openings){
      this.popupService.showConfirmationPopup(OpenContactConfirmation, this.apartment.possiblyRented).then(async confirmed => {
        if(!confirmed){
          return;
        }
        self.showContactCore(self);
      });
    }else{
      self.showContactCore(self);
    }
  }

  public showMapObjects(event:MatButtonToggleChange){
    console.log(event);
  }

  private async showContactCore(self){
    let contactInfo = await self.apartmentsDetailService.getContactInfo(self.apartment.id);
    if (contactInfo.showForbidden){
      self.router.navigate(['forbidden']);
      return;
    }

    self.apartment.phones = contactInfo.phones;
    if(contactInfo.canBeVoted){
      self.voteState = VoteState.ShowVote;
    }else{
      self.apartment.phoneState = PhoneState.ShowContacts;
    }

    this.userCountersService.refreshUserCounters();
  }

  onBackClick(){
    this.onBack.emit();
  }

  public passBlackListState(): boolean {
    return !this.apartment.inWhiteList && !this.apartment.inBlackList;
  }
}
