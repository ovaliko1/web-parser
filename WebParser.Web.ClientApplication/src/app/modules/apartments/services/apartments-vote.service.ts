import { Injectable } from "@angular/core";
import { Response } from "@angular/http";

import "rxjs/add/operator/map";
import 'rxjs/add/operator/toPromise';

import { VoteType } from "app/modules/shared/enums/vote-type";
import { VoteResultModel } from "../models/vote-result.model";

import { ApiController } from "app/modules/shared/api/api.controller";

@Injectable()
export class ApartmentsVoteService {

  constructor(
    private apiController: ApiController)
  {}

  async voteForApartment(apartmentId: number, voteType: VoteType): Promise<VoteResultModel> {
    return await this.apiController
      .voteForApartment(apartmentId, voteType);
  }
}