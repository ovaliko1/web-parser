import { Injectable, EventEmitter } from "@angular/core";
import { Response } from "@angular/http";

import { ApartmentsFiltersService } from "app/modules/apartments/services/apartments-filters.service";

import { ApartmentModel } from "../models/apartment.model";
import { FiltersModel } from "../models/filters.model";
import { ChosenFiltersModel } from "../models/chosen-filters.model";
import { ApartmentsModel } from "../models/apartments.model";

import "rxjs/add/operator/map";
import 'rxjs/add/operator/toPromise';

import { ApiController } from "app/modules/shared/api/api.controller";


@Injectable()
export class ApartmentsListService {

  public needToRefreshApartmentsList: EventEmitter<any> = new EventEmitter();

  constructor(
    private apiController: ApiController,
    private apartmentsFiltersService: ApartmentsFiltersService)
  {}

  public async getApartments(page: number, pageSize: number, filters: ChosenFiltersModel): Promise<ApartmentsModel> {
    return await this.apiController
      .getApartments(page, pageSize, filters);
  }

  public refreshApartmentsList(){
    this.needToRefreshApartmentsList.emit();
  }
}