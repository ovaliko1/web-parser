import { Injectable } from "@angular/core";

import { ApartmentsScreenState } from "app/modules/shared/enums/apartments-screen-state";

@Injectable()
export class ApartmentsScreenService {

  private apartmentsScreenState: ApartmentsScreenState = ApartmentsScreenState.Unknown;

  constructor()
  {}

  public setApartmentsScreenState(apartmentsScreenState: ApartmentsScreenState): void {
    this.apartmentsScreenState = apartmentsScreenState;
  }

  public getApartmentsScreenState(): ApartmentsScreenState {
    return this.apartmentsScreenState;
  }
}