import { Injectable } from "@angular/core";
import { LocalStorageService } from "app/modules/shared/services/local-storage.service";
import { PopupService } from "app/modules/shared/services/popup.service";
import { NotificationsConfirmation } from "app/modules/apartments/components/notifications-confirmation.component";

declare var OneSignal: any;

@Injectable()
export class NotificationsService {

  constructor(
    private localStorageService: LocalStorageService,
    private popupService: PopupService)
  {}

  public showNotificationsRequest(): Promise<any>{
    const self = this;
    return new Promise<any>((resolve, reject) => {
      if (self.notificationsDoesNotSupported()) {
        console.log(`Not supported: Notifications: ${!!Notification} ServiceWorker: ${!!navigator.serviceWorker}`);
        return resolve(); 
      }

      this.needToShowNotificationsRequest()
      .then(need => {
        if(!need){
          return resolve();
        }

        navigator.serviceWorker.getRegistration()
          .then(registration => {
            console.log(registration);
            if (!registration) {
              self.popupService.showConfirmationPopup(NotificationsConfirmation)
                .then(x => self.registerShowingNotificationsCore(x))
                .then(resolve)
                .catch(reject);
            } else {
              resolve();
            }
          })
          .catch(reject);
      });
    });
  }

  public async pingNotificationsServiceWorker() {
    if (this.notificationsDoesNotSupported()) {
      return;
    }

    for(;;){
      let registration = await navigator.serviceWorker.getRegistration();
      if (registration) {
        const serviceWorker = registration.installing || registration.waiting || registration.active;
          serviceWorker.postMessage("ping");
      }
      await this.sleep(20 * 1000);
    }
  }

  private sleep(milliseconds): Promise<any>{
    return new Promise(resolve => {
      setTimeout(resolve, milliseconds);
    })
  }

  private notificationsDoesNotSupported(): boolean{
    return !Notification || !navigator.serviceWorker || !window.indexedDB;
  }

  private registerShowingNotifications():Promise<any>{
    return this.registerShowingNotificationsCore(true);
  }

  private registerShowingNotificationsCore(requestPerm: boolean):Promise<any>{
    return new Promise((resolve, reject) => {
      this.requestPermission(requestPerm)
      .then(x => this.tryRegisterServiceWorker(x))
      .then(resolve)
      .catch(reject);
    });
  }

  private tryRegisterServiceWorker(notificationPermission: NotificationPermission):Promise<any>{
    return new Promise((resolve, reject) => {
      switch (notificationPermission){
        case "default":
          this.localStorageService.incrementNotificationAskingCount();
          resolve();
          break;

        case "denied":
          this.localStorageService.preventNotificationAskingCount();
          resolve();
          break;

        case "granted":
          this.localStorageService.preventNotificationAskingCount();  
          resolve();
          setTimeout(() => OneSignal.setSubscription(true), 200);
          break;

        default:
          break;
      }
    });
  }

  private requestPermission(requestPermission: boolean): Promise<NotificationPermission> {
    return new Promise((resolve, reject) => {
      if(!requestPermission) {
        return resolve("default");
      }

      OneSignal.on('notificationPermissionChange', function(permissionChange) {
        var currentPermission = permissionChange.to;
        console.log('New permission state:', currentPermission);

        resolve(currentPermission);
      });

      OneSignal.registerForPushNotifications();
    })
  }

  private needToShowNotificationsRequest(): Promise<boolean> {
    return new Promise<any>((resolve, reject) => {
      let count = this.localStorageService.getNotificationAskingCount();
      switch(count){
        case 0:
        case 6:
        case 13:
        case 22:
          return resolve(true);
        case -1:
        default:
          if (count > 0 && count < 22){
            this.localStorageService.incrementNotificationAskingCount();
          }
          return resolve(false);
      }
    });
  }

  private registerServiceWorker(): Promise<void | ServiceWorkerRegistration>{
    return navigator.serviceWorker.register('service-worker.js')
    .then(registration => {
      console.log('Service worker successfully registered.');
      return registration;
    })
    .catch(ex => console.error('Unable to register service worker.', ex));
  }
}
