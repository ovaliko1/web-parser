import { Injectable } from "@angular/core";
import { Response } from "@angular/http";

import { Subject } from 'rxjs/Subject';
import { ActivatedRoute } from "@angular/router";

import { FiltersModel } from "../models/filters.model";
import { FiltersInitModel, ApartmentTypeModel } from "../models/filters-init.model";
import { ChosenFiltersModel, ChosenApartmentTypeModel, CurrentChosenFiltersModel } from "../models/chosen-filters.model";

import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";
import { ApiController } from "app/modules/shared/api/api.controller";
import { LocalStorageService } from "app/modules/shared/services/local-storage.service";
import { NotificationsConfirmation } from "../components/notifications-confirmation.component";
import { NotificationsService } from "app/modules/apartments/services/notifications.service";
import { IndexedDBService } from "app/modules/shared/services/Indexed-db-service";

@Injectable()
export class ApartmentsFiltersService {

  private filters: FiltersModel;
  private filtersInitData: FiltersInitModel

  // Observable object source
  private currentFiltersSource = new Subject<CurrentChosenFiltersModel>();
  
  // Observable object stream
  public currentFilters$ = this.currentFiltersSource.asObservable();

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiController: ApiController,
    private localStorageService: LocalStorageService,
    private notificationsService: NotificationsService,
    private indexedDBService: IndexedDBService) {
  }

  public async getCurrentFilters(cityId?: number): Promise<FiltersModel> {
    let chosenFilters = this.localStorageService.getFilters();

    if (chosenFilters != null && cityId != null){
      chosenFilters = chosenFilters.cityId != cityId ? null : chosenFilters;
    } else if(chosenFilters != null && cityId == null){
      cityId = chosenFilters.cityId;
    }
    
    let targetCityId = chosenFilters == null ? Number(this.getQueryParam("cityId")) || cityId : cityId;
    let filtersInitData = await this.getFiltersInitModel(targetCityId);

    let result = new FiltersModel();

    if (chosenFilters == null) {
      const chosenApartmentTypes = this.getQueryParam("articleGroupIds");

      result.chosenFiltersModel = new ChosenFiltersModel();
      result.chosenFiltersModel.apartmentId = null,
      result.chosenFiltersModel.sourceIds = filtersInitData.sources.map(x => x.id),
      result.chosenFiltersModel.metroStationIds = [],
      result.chosenFiltersModel.apartmentTypes = filtersInitData.apartmentTypes.map(x => new ChosenApartmentTypeModel(x.ids, x.name, x.ids == chosenApartmentTypes)),
      result.chosenFiltersModel.showWithEmptyMetro = false,
      result.chosenFiltersModel.showWithEmptyPrice = true;
      result.chosenFiltersModel.priceFrom = Number(this.getQueryParam("priceFrom")) || null;
      result.chosenFiltersModel.priceTo = Number(this.getQueryParam("priceTo")) || null;
    } else {
      result.chosenFiltersModel = chosenFilters;

      // course ChosenApartmentTypeModel.ids by name can be changed during one session on the server
      let savedApartmentTypeName = chosenFilters.apartmentTypes.filter(x => x.checked).map(x => x.name);
      result.chosenFiltersModel.apartmentTypes = filtersInitData
        .apartmentTypes
        .map(x => new ChosenApartmentTypeModel(x.ids, x.name, savedApartmentTypeName.indexOf(x.name) != -1));
    }

    result.chosenFiltersModel.cityId = targetCityId || filtersInitData.defaultCityId;

    result.filtersInitModel = filtersInitData;
    return result;
  }

  private async getFiltersInitModel(cityId?: number): Promise<FiltersInitModel> {
    if (this.filtersInitData == null || cityId == null || this.filtersInitData.defaultCityId != cityId) {
      this.filtersInitData = await this.apiController
        .getFiltersInitData(cityId);
    }
    return this.filtersInitData;
  }

  public async setFilters(filters: ChosenFiltersModel, chosenByUser: boolean) {
    this.localStorageService.saveFilters(filters);
    this.currentFiltersSource.next(new CurrentChosenFiltersModel(filters, chosenByUser));
    let self = this;

    await this.indexedDBService.saveFilters(filters);

    if (chosenByUser){
      this.notificationsService.showNotificationsRequest();
    };
  }

  private getQueryParam(key: string): string{
    return this.activatedRoute.snapshot.queryParams[key];
  }
}
