import { Subject } from 'rxjs/Subject';

import { Component, Injectable, Input, Output, EventEmitter } from "@angular/core";
import { Response } from "@angular/http";

import { ApartmentModel } from "../models/apartment.model";

import "rxjs/add/operator/map";
import { ApiController } from "app/modules/shared/api/api.controller";
import { ApartmentDetailsModel } from '../models/apartment-details.model';
import { ContactInfoModel } from '../models/contact-info.model';

@Injectable()
export class ApartmentsDetailService {

  constructor(private apiController: ApiController) {}

  async getApartmentById(id: string): Promise<ApartmentDetailsModel> {
    return await this.apiController
      .getApartmentById(id);
  }

  async getContactInfo(apartmentId: string): Promise<ContactInfoModel> {
    return await this.apiController
       .getContactInfo(apartmentId);
  }
}
