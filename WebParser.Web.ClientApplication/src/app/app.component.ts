import { Component, NgModule, OnInit } from "@angular/core";
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { IndexedDBService } from "app/modules/shared/services/Indexed-db-service";
import { NotificationsService } from "app/modules/apartments/services/notifications.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styles: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = "app works!";

  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private indexedDBService: IndexedDBService,
    private notificationsService: NotificationsService
  ) {
    iconRegistry.addSvgIcon(
      'vk-icon',
      sanitizer.bypassSecurityTrustResourceUrl('/app-public/assets/img/vk-icon.svg'));
    iconRegistry.addSvgIcon(
      'twitter-icon',
      sanitizer.bypassSecurityTrustResourceUrl('/app-public/assets/img/twitter-icon.svg'));
    iconRegistry.addSvgIcon(
      'guarantee-icon',
      sanitizer.bypassSecurityTrustResourceUrl('/app-public/assets/img/guarantee-icon.svg'));
  }

  async ngOnInit() {
    this.notificationsService.pingNotificationsServiceWorker();
    this.indexedDBService.initDatabase();
  }
}
