
const webpack = require("webpack");
const path = require('path');
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const { NoEmitOnErrorsPlugin } = require('webpack');
const { AngularCompilerPlugin }= require('@ngtools/webpack');

module.exports = (envOptions) => {
  envOptions = envOptions || {};
  const config = {
    "devtool": envOptions.MODE === "prod" ? "source-map" : "cheap-module-eval-source-map",

    "devServer": {
      contentBase: path.join(__dirname, "../WebParser.Web/"),
      publicPath: "/app-public/",

      compress: true,
      hot: true,

      port: 9000
    },

    "resolve": {
      "extensions": [
        ".ts",
        ".js"
      ],
      "modules": [
        "./node_modules"
      ]
    },

    "resolveLoader": {
      "modules": [
        "./node_modules"
      ]
    },

    "entry": {
      "main": [
        "./src/main.ts"
      ]
    },

    "output": {
      "path": path.join(__dirname, "../WebParser.Web/app-public"),
      "filename": "[name].bundle.js"
    },

    "module": {
      "rules": [
        {
          "enforce": "pre",
          "test": /\.js$/,
          "loader": "source-map-loader",
          "exclude": [
            /\/node_modules\//
          ]
        },
        {
          "test": /\.json$/,
          "loader": "file-loader?name=[name].[ext]"
        },
        {
          "test": /\.html$/,
          "loader": "raw-loader"
        },
        {
          "test": /\.(eot|svg)$/,
          "loader": "file-loader?name=[name].[hash:20].[ext]"
        },
        {
          "test": /\.(jpg|png|gif|otf|ttf|woff|woff2|cur|ani)$/,
          "loader": "url-loader?name=[name].[hash:20].[ext]&limit=10000"
        },
        {
          "test": /\.scss$|\.sass$/,
          "loaders": ExtractTextPlugin.extract({
            "fallback": "style-loader",
            "use": [
              "css-loader?{\"sourceMap\":true,\"importLoaders\":1}",
              "postcss-loader",
              "sass-loader"
            ]
          })
        },
        {
          test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
          loader: '@ngtools/webpack'
        }
      ]
    },

    "plugins": [
      new NoEmitOnErrorsPlugin(),
      new ProgressPlugin(),
      new ExtractTextPlugin("[name].bundle.css"),

      new webpack.HotModuleReplacementPlugin(),

      new AngularCompilerPlugin({
        tsConfigPath: 'src/tsconfig.app.json',
        mainPath: "main.ts",
        sourceMap: true
      }),

      new webpack.DefinePlugin({
        'process.env': {
          'ENV': JSON.stringify(envOptions.MODE)
        }
      })
    ],

    "node": {
      "fs": "empty",
      "global": true,
      "crypto": "empty",
      "tls": "empty",
      "net": "empty",
      "process": true,
      "module": false,
      "clearImmediate": false,
      "setImmediate": false
    }
  }

  if(envOptions.MODE === "prod"){
      config.plugins.push(new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        mangle: {
          screw_ie8: true,
          keep_fnames: true
        },
        compress: {
          warnings: false,
          screw_ie8: true
        },
        comments: false
      }));
  }

  return config;
};
