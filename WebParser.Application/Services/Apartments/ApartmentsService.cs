﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using log4net;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.Apartments;
using WebParser.Application.Interfaces.Services.Apartments.Entities;
using WebParser.Application.Interfaces.Services.ArticleRating;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Application.Mappers;
using WebParser.Application.Services.Parsing.ParsedContent.Entities;
using WebParser.Application.Services.Parsing.ParsedContent.JsonEntities;
using WebParser.Application.Services.UserManagement;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;
using WebParser.DataAccess.Helpers;

namespace WebParser.Application.Services.Apartments
{
    internal class ApartmentsService : IApartmentsService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private class ArticleData
        {
            public DbParsedArticle Article { get; set; }

            public bool UseSourceLinkAsContact { get; set; }

            public bool? FreeAccess { get; set; }
        }

        private class ContactsAndState
        {
            public PhoneState PhoneState { get; set; }

            public PhoneItem[] Contacts { get; set; }
        }

        private const int PossiblyRentedDays = 3;
        private const int MaxSourceMetroStationsCount = 3;

        private IRepository Repository { get; }

        private IDateTimeService DateTimeService { get; }

        private IUserService UserService { get; }

        private IInternalUserService InternalUserService { get; }

        private ISiteStructureService SiteStructureService { get; }

        private IMetroMapService MetroMapService { get; }

        public ApartmentsService(
            IRepository repository,
            IDateTimeService dateTimeService,
            IUserService userService,
            ISiteStructureService siteStructureService,
            IMetroMapService metroMapService,
            IInternalUserService internalUserService)
        {
            Repository = repository;
            DateTimeService = dateTimeService;
            UserService = userService;
            SiteStructureService = siteStructureService;
            MetroMapService = metroMapService;
            InternalUserService = internalUserService;
        }

        public ApartmentList ReadApartments(int page, int pageSize, ApartmentFilters filters)
        {
            var currentUser = UserService.GetCurrentUser();
            ApartmentList result;
            using (var context = Repository.GetContext())
            {
                var availableArticleGroupIds = new HashSet<int>(
                    context
                        .Set<DbArticlesGroup>()
                        .Where(x => x.User.NameForViewers != null)
                        .Select(x => x.Id)
                        .Execute());

                var articleGroupIds = filters.ArticleGroupIds?.Length > 0 ?
                    filters
                        .ArticleGroupIds
                        .Where(x => availableArticleGroupIds.Contains(x))
                        .ToArray(x => (int?)x) :
                    availableArticleGroupIds.ToArray(x => (int?)x);

                var showFromBlackList = ShowArticlesFromBlackList(context, filters.CityId);

                var query = context
                    .Set<DbParsedArticle>()
                    .Where(
                        x =>
                            // TODO: Rename to UserId or support real CityId here
                            x.Site.User.Id == filters.CityId &&
                            articleGroupIds.Contains(x.ArticlesGroupId) &&
                            !x.IsDuplicate &&
                            (showFromBlackList || x.BlackListItemId == null) &&
                            (x.ParsingFailedCount == null) &&
                            (x.OutdateRating < ArticleRatingConstants.DoNotShowArticleOutdateRating));

                if (filters.SourceIds?.Length > 0)
                {
                    var sites = filters.SourceIds.ToArray(x => (int?)x);
                    query = query.Where(x => sites.Contains(x.SiteId));
                }
                if (filters.PriceFrom.HasValue)
                {
                    query = query.Where(x => x.Price != null ? x.Price >= filters.PriceFrom : filters.ShowWithEmptyPrice);
                }
                if (filters.PriceTo.HasValue)
                {
                    query = query.Where(x => x.Price != null ? x.Price <= filters.PriceTo : filters.ShowWithEmptyPrice);
                }
                if (filters.MinutesToMetro.HasValue)
                {
                    var maxDurationToMetroValue = filters.MinutesToMetro.Value;
                    query = query.Where(x => x.DurationToMetroValue == null || x.DurationToMetroValue <= maxDurationToMetroValue);
                }
                if (filters.MetroStationIds.Length > 0)
                {
                    // TODO: Process ShowWithEmptyMetro flag
                    query = ExpressionsHelper.SetMetroFilterPredicate(query, filters.MetroStationIds);
                }
                if (filters.ApartmentId.HasValue)
                {
                    var after = context
                        .Set<DbParsedArticle>()
                        .Where(x => x.Id == filters.ApartmentId)
                        .Select(x => (DateTime?) x.CreatedDate)
                        .FirstOrDefault();

                    if (after.HasValue)
                    {
                        query = query.Where(x => x.CreatedDate > after.Value);
                    }
                }

                var apartments = query
                    .Select(x => new ApartmentItemProjection
                    {
                        Id = x.Id,
                        CreatedDate = x.CreatedDate,
                        SiteId = x.SiteId,
                        ArticlesGroup = x.ArticlesGroup.PublicName,
                        Price = x.Price,
                        PriceStr = x.PriceStr,
                        Metro = x.Metro,
                        GoodRating = x.GoodRating,
                        BadRating = x.BadRating,
                        OutdateRating = x.OutdateRating,
                        InWhiteList = x.WhiteListItemId != null,
                        MetroDistance = x.DistanceToMetroText,
                        MetroDuration = x.DurationToMetroText,
                        ImagesPreview = x.ImagesPreview,
                        HideBlacklistWarranty = showFromBlackList,
                    })
                    .OrderByDescending(x => x.CreatedDate)
                    .Skip(page * pageSize)
                    .Take(pageSize + 1)
                    .ExecuteAndConvert(x => x.Map<ApartmentItemProjection, ApartmentItem>());

                result = new ApartmentList
                {
                    Items = SetShowRatingFlag(context, currentUser, apartments.Take(pageSize).ToArray()).ToArray(x =>
                    {
                        x.PossiblyRented = GetPossiblyRentedFlag(x.CreatedDate);
                        return x;
                    }),
                    HasMore = apartments.Length == pageSize + 1,
                };
            }

            var availableSites = SiteStructureService.GetViewerAvailableSites(filters.CityId);
            result.Items.Foreach(y => y.Source = availableSites.FirstOrDefault(z => z.Id == y.SiteId).IfNotNull(z => z.Name, string.Empty));

            return result;
        }

        private IEnumerable<ApartmentItem> SetShowRatingFlag(DataBaseContext context, User currentUser, ApartmentItem[] filteredApartments)
        {
            if (currentUser == null)
            {
                return filteredApartments;
            }

            if (InternalUserService.UserCanNotVote(currentUser))
            {
                return filteredApartments.Select(x =>
                {
                    x.ShowRating = true;
                    return x;
                });
            }

            var articleIds = filteredApartments.ToArray(x => (int?)x.Id);
            var votedArticles = new HashSet<int>(context
                    .Set<DbArticleRatingVote>()
                    .Where(x => x.UserId == currentUser.Id && articleIds.Contains(x.ArticleId))
                    .Select(x => x.ArticleId ?? -1)
                    .Execute());

            return filteredApartments.Select(x =>
            {
                x.ShowRating = votedArticles.Contains(x.Id);
                return x;
            });
        }

        private bool GetPossiblyRentedFlag(DateTime createdDate)
        {
            return (DateTimeService.GetCurrentDateTimeUtc() - createdDate).Days >= PossiblyRentedDays;
        }

        public Apartment GetApartment(int id)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var articleData = context
                    .Set<DbParsedArticle>()
                    .Where(x => x.Id == id)
                    .Select(x => new ArticleData
                    {
                        Article = x,
                        UseSourceLinkAsContact = x.Site.UseSourceLinkAsContact,
                    })
                    .FirstOrDefault();

                if (articleData == null)
                {
                    throw new ParsedContentException($"Apartment with Id {id} not found.");
                }

                var article = articleData.Article;

                var parsedValues = article.ParsedContent.Deserialize<JsonParsedContent>().ToParsedValues();
                var showFromBlackList = ShowArticlesFromBlackList(context, article.UserId);

                var contactsInfo = GetContactsAndState(context, currentUser, article, GetPhoneItems(parsedValues, articleData), showFromBlackList);

                var metro = MetroMapService.GetStationByJsonIds(article.Metro);
                var metroItems = metro == null ? new MetroItem[0] : new[]
                {
                    new MetroItem(metro.Name, article.DistanceToMetroText, article.DurationToMetroText)
                };

                var result = new Apartment
                {
                    Id = article.Id,
                    Subject = parsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Object)?.Values.FirstOrDefault(),
                    Price = article.PriceStr,
                    MetroItems = metroItems,
                    SourceMetro = parsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Metro)?.Values?.Take(MaxSourceMetroStationsCount).ToArray() ?? new string[0],
                    Address = !string.IsNullOrEmpty(article.FormattedAddress) ? article.FormattedAddress : parsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Address)?.Values.FirstOrDefault(),
                    SourceAddress = parsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Address)?.Values ?? new string[0],
                    CreatedDate = article.CreatedDate,
                    Latitude = article.Latitude,
                    Longitude = article.Longitude,
                    OwnerNames = parsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Name)?.Values ?? new string[0],
                    Images = parsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Images)?.Values ?? new string[0],
                    Characteristics = parsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Characteristics)?.Values ?? new string[0],
                    Description = parsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Description)?.Values ?? new string[0],
                    SourceDates = parsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.SourceDate)?.Values ?? new string[0],
                    PhoneState = contactsInfo.PhoneState,
                    Phones = contactsInfo.Contacts,
                    PossiblyRented = GetPossiblyRentedFlag(article.CreatedDate),
                    InBlackList = article.BlackListItemId.HasValue,
                    InWhiteList = !article.BlackListItemId.HasValue && article.WhiteListItemId.HasValue,
                    FullSizeImages = article.FullSizeImages.ToImageInfo(),
                    HideBlacklistWarranty = showFromBlackList,
                };
                return result;
            }
        }

        public ContactInfo GetContactInfo(int apartmentId)
        {
            var currentUser = UserService.GetCurrentUser();
            ArticleData articleData;
            bool canBeVoted;
            using (var context = Repository.GetContext())
            {
                articleData = context
                    .Set<DbParsedArticle>()
                    .Where(x => x.Id == apartmentId)
                    .Select(x => new ArticleData
                    {
                        Article = x,
                        UseSourceLinkAsContact = x.Site.UseSourceLinkAsContact,
                        FreeAccess = x.Site.User.City.FreeAccess,
                    })
                    .FirstOrDefault();

                if (articleData == null ||
                    (!ShowArticlesFromBlackList(context, articleData.Article.UserId) &&
                      articleData.Article.BlackListItemId.HasValue))
                {
                    return new ContactInfo
                    {
                        Phones = new PhoneItem[0],
                    };
                }
                canBeVoted =
                    !InternalUserService.UserCanNotVote(currentUser) &&
                    !context
                        .Set<DbArticleRatingVote>()
                        .Any(x => x.UserId == currentUser.Id && x.ArticleId == apartmentId);
            }

            if (!(articleData.FreeAccess ?? false))
            {
                var rateType = UserService.GetUserRateType(currentUser);
                if (!rateType.HasValue)
                {
                    return new ContactInfo
                    {
                        Phones = new PhoneItem[0],
                        ShowForbidden = true,
                    };
                }
                SaveOpenedContact(currentUser, articleData.Article.Id, articleData.Article.UserId, rateType.Value);
            }
            
            var parsedValues = articleData.Article.ParsedContent.Deserialize<JsonParsedContent>().ToParsedValues();

            return new ContactInfo
            {
                Phones = GetPhoneItems(parsedValues, articleData).ToArray(),
                CanBeVoted = canBeVoted,
            };
        }

        private void SaveOpenedContact(User currentUser, int articleId, int articleUserId, RateType rateType)
        {
            using (var context = Repository.GetContext())
            using (var transaction = context.BeginTransaction(IsolationLevel.Serializable))
            {
                var now = DateTimeService.GetCurrentDateTimeUtc();

                var userFromDb = context.FindFirst<DbUser, NotFoundException>(x => x.Id == currentUser.Id, "User for contact open not found");
                if (rateType == RateType.Openings)
                {
                    if (userFromDb.OpeningBalance == 0)
                    {
                        Log.Warn(
                            $"OpeningBalance is 0. User id: {currentUser.Id} article id: {articleId} articleUserId: {articleUserId}");
                        return;
                    }

                    userFromDb.OpeningBalance--;
                }
                
                context.Set<DbOpenedContact>().Add(new DbOpenedContact
                {
                    ArticleId = articleId,
                    ArticleUserId = articleUserId,
                    CreatedDate = now,
                    UserId = currentUser.Id,
                    RateTypeId = (int)rateType,
                });

                try
                {
                    context.ValidateAndSave();
                    transaction.Commit();
                }
                catch (DataBaseConstraintException ex)
                {
                    if (ex.Message == DbHelper.ContactAlreadyOpenedMessage)
                    {
                        Log.Info($"ContactAlreadyOpened. User id: {currentUser.Id} article id: {articleId} articleUserId: {articleUserId}");
                        return;
                    }
                    if (ex.Message == DbHelper.OpeningBalanceMessage)
                    {
                        Log.Error($"OpeningBalance contstaint executed. User id: {currentUser.Id} article id: {articleId} articleUserId: {articleUserId}");
                        return;
                    }
                    throw;
                }
            }
        }

        private ContactsAndState GetContactsAndState(
            DataBaseContext context,
            User currentUser,
            DbParsedArticle article,
            IEnumerable<PhoneItem> contacts,
            bool showFromBlackList)
        {
            if (!showFromBlackList && article.BlackListItemId.HasValue)
            {
                return new ContactsAndState
                {
                    PhoneState = PhoneState.InBlackList,
                    Contacts = new PhoneItem[0],
                };
            }

            if (currentUser == null)
            {
                return new ContactsAndState
                {
                    PhoneState = PhoneState.OpenLink,
                    Contacts = new PhoneItem[0],
                };
            }

            var canBeVoted =
                !InternalUserService.UserCanNotVote(currentUser) &&
                !context
                    .Set<DbArticleRatingVote>()
                    .Any(x => x.UserId == currentUser.Id && x.ArticleId == article.Id);

            var contactIsOpened = context
                .Set<DbOpenedContact>()
                .Any(x => x.UserId == currentUser.Id && x.ArticleId == article.Id);

            if (contactIsOpened)
            {
                return new ContactsAndState
                {
                    PhoneState = canBeVoted ? PhoneState.ShowContactsAndVotePossibility : PhoneState.ShowContacts,
                    Contacts = contacts.ToArray(),
                };
            }

            var accessExpired = UserService.UserAccessExpired(currentUser);
            if (accessExpired)
            {
                return new ContactsAndState
                {
                    PhoneState = PhoneState.OpenLink,
                    Contacts = new PhoneItem[0],
                };
            }

            return new ContactsAndState
            {
                PhoneState = PhoneState.ConnectToContact,
                Contacts = new PhoneItem[0],
            };
        }

        private static IEnumerable<PhoneItem> GetPhoneItems(ParsedValue[] parsedValues, ArticleData articleData)
        {
            if (articleData.UseSourceLinkAsContact)
            {
                yield return
                    new PhoneItem
                    {
                        Type = PhoneValueType.Link,
                        Value = articleData.Article.Link,
                    };
                yield break;
            }

            foreach (var phone in parsedValues
                .Where(x => x.ParsedKeyType == ParsedKeyType.Phone)
                .SelectMany(x =>
                {
                    if (x.IsLink)
                    {
                        return x.Values.ToArray(y => new PhoneItem
                        {
                            Type = PhoneValueType.Image,
                            Value = y,
                            BackgroundColor = x.BackgroundColor,
                        });
                    }

                    return x.Values.ToArray(y => new PhoneItem
                    {
                        Type = IsUrl(y) ? PhoneValueType.Link : PhoneValueType.String,
                        Value = y,
                        BackgroundColor = x.BackgroundColor,
                    });
                }))
            {
                yield return phone;
            }
        }

        private static bool ShowArticlesFromBlackList(DataBaseContext context, int cityId)
        {
            var showFromBlackList = context.Set<DbUser>()
                // TODO: Rename to UserId or support real CityId here
                .Where(x => x.Id == cityId)
                .Select(x => x.City.ShowArticlesFromBlackList)
                .FirstOrDefault();
            return showFromBlackList;
        }


        private static bool IsUrl(string str)
        {
            Uri uri;
            return Uri.TryCreate(str, UriKind.Absolute, out uri);
        }
    }
}
