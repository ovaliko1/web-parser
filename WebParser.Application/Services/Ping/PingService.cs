﻿using System.Threading.Tasks;
using RestSharp;
using WebParser.Application.Interfaces.Services.Ping;
using WebParser.Application.Services.Settings;
using WebParser.Application.Services.WebClient;

namespace WebParser.Application.Services.Ping
{
    internal class PingService : IPingService
    {
        private IWebCommand WebCommand { get; }

        private ISettings Settings { get; }

        public PingService(
            IWebCommand webCommand,
            ISettings settings)
        {
            WebCommand = webCommand;
            Settings = settings;
        }

        public async Task<bool> Ping()
        {
            return await WebCommand.MakeRequest(Settings.GetPingUrl(), Method.POST, new (string, string)[0]);
        }
    }
}
