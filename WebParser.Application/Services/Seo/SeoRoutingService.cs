﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.Seo;
using WebParser.Application.Interfaces.Services.Seo.Entities;
using WebParser.Common;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Seo
{
    internal class SeoRoutingService : ISeoRoutingService
    {
        private static readonly Dictionary<string, SeoParameterType> SeoParameterTypes = Enum.GetValues(typeof(SeoParameterType))
            .Cast<SeoParameterType>()
            .Select(x => (x.ToString(), x))
            .ToDictionary(x => x.Item1, x => x.Item2, StringComparer.InvariantCultureIgnoreCase);


        private static readonly Dictionary<SeoParameterType, Func<DataBaseContext, IQueryable<ISeoParameter>>> DbSets = new Dictionary<SeoParameterType, Func<DataBaseContext, IQueryable<ISeoParameter>>>
        {
            [SeoParameterType.City] = GetDbSet<DbCitySeoParameter>(),
            [SeoParameterType.District] = GetDbSet<DbDistrictSeoParameter>(),
            [SeoParameterType.ArticleGroup] = GetDbSet<DbArticleGroupSeoParameter>(),
            [SeoParameterType.Phrase] = GetDbSet<DbPhraseSeoParameter>(),
            [SeoParameterType.Alias] = GetDbSet<DbAliasSeoParameter>(),
        };

        private static Func<DataBaseContext, IQueryable<T>> GetDbSet<T>() where T : class, ISeoParameter
        {
            return context => context.Set<T>();
        }

        private IRepository Repository { get; }

        public SeoRoutingService(IRepository repository)
        {
            Repository = repository;
        }

        public SeoDetails GetSeoDetails(string url, SeoRoutingResult routingResult)
        {
            using (var context = Repository.GetContext())
            {
                string citySection = string.Empty;
                string articleGroupSection = string.Empty;
                string countrySection = string.Empty;
                var breadcrumbs = new List<BreadcrumbItem>();

                var result = new SeoDetails
                {
                    CanonicalUrl = url.ToSeoUrl(),
                    RoutingResult = routingResult,
                };

                if (routingResult == null)
                {
                    return result;
                }

                if (routingResult.CityId.HasValue)
                {
                    var seoCity = context.Set<DbCitySeoParameter>().Where(x => x.CityId == routingResult.CityId.Value).OrderBy(x => x.Id).First();
                    var country = seoCity.City.Country;

                    citySection = seoCity.Alias + "-" + SeoParameterType.City;
                    countrySection = country.Iso;

                    breadcrumbs.Add(new BreadcrumbItem
                    {
                        Title = seoCity.Value,
                        Url = $"/{countrySection}/{citySection}".ToSeoUrl(),
                    });

                    result.CityName = seoCity.Value;
                    result.CountryRegionCode = country.CountryRegionCode;
                    result.LanguageCode = country.LanguageCode;
                }

                if (!string.IsNullOrEmpty(routingResult.ArticleGroupInternalName))
                {
                    var seoArticleGroup = context
                        .Set<DbArticleGroupSeoParameter>()
                        .Where(x =>
                            x.CountryId == routingResult.CountryId &&
                            x.ArticleGroupInternalName == routingResult.ArticleGroupInternalName)
                        .OrderBy(x => x.Id)
                        .FirstOrDefault();

                    if (seoArticleGroup != null)
                    {
                        articleGroupSection = seoArticleGroup.Alias + "-" + SeoParameterType.ArticleGroup;
                        breadcrumbs.Add(new BreadcrumbItem
                        {
                            Title = seoArticleGroup.Value,
                            Url = $"/{countrySection}/{articleGroupSection}/{citySection}/".ToSeoUrl(),
                        });

                        result.ArticleGroup = seoArticleGroup.Value;
                    }
                }

                if (routingResult.DistrictId.HasValue)
                {
                    var seoDistrict = context.Set<DbDistrictSeoParameter>().Where(x => x.DistrictId == routingResult.DistrictId.Value).OrderBy(x => x.Id).First();
                    var seoDistrictSection = seoDistrict.Alias + "-" + SeoParameterType.District;
                    breadcrumbs.Add(new BreadcrumbItem
                    {
                        Title = seoDistrict.Value,
                        Url = $"/{countrySection}/{articleGroupSection}/{citySection}/{seoDistrictSection}".ToSeoUrl(),
                    });

                    result.District = seoDistrict.Value;
                }

                result.Breadcrumbs = breadcrumbs.ToArray();
                return result;
            }
        }

        public SeoRoutingResult Route(Uri currentUrl)
        {
            var segments = currentUrl
                .Segments
                .Select(x => x.Trim('/'))
                .Where(x => !string.IsNullOrEmpty(x))
                .Select(
                    x =>
                    {
                        var seoParameterTypeString = x.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                        if (seoParameterTypeString.Length < 2)
                        {
                            return (section: x, type: SeoParameterType.Unknown);
                        }

                        if (!SeoParameterTypes.TryGetValue(seoParameterTypeString[seoParameterTypeString.Length - 1], out var seoParameterType))
                        {
                            return (section: x, type: SeoParameterType.Unknown);
                        }

                        return (section: string.Join("-", seoParameterTypeString.Take(seoParameterTypeString.Length - 1)), type: seoParameterType);
                    })
                .Where(x => x.type != SeoParameterType.Unknown)
                .ToArray();

            if (segments.Length == 0 || segments.All(x => x.type == SeoParameterType.Unknown))
            {
                return null;
            }

            var result = new SeoRoutingResult();
            var titleSegments = new List<string>(segments.Length);

            using (var context = Repository.GetContext())
            {
                foreach (var segment in segments)
                {
                    var seoParameter = DbSets[segment.type](context)
                        .AsNoTracking()
                        .FirstOrDefault(x => x.Alias == segment.section);

                    if (seoParameter == null)
                    {
                        continue;
                    }

                    seoParameter.UpdateRoutingResult(result);
                    titleSegments.Add(seoParameter.Value);
                }

                if (result.CityId.HasValue)
                {
                    result.CountryId = context
                        .Set<DbCity>()
                        .Where(x => x.Id == result.CityId.Value)
                        .Select(x => (int?)x.CountryId)
                        .FirstOrDefault();
                }
                else
                {
                    result.ForceNotFoundStatus = true;
                }
            }

            result.SeoTitle = string.Join(" ", titleSegments);

            return result;
        }
    }
}