﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.Seo;
using WebParser.Application.Services.Settings;
using WebParser.Common;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Seo
{
    internal class SiteMapService : ISiteMapService
    {
        private class CountryDetails
        {
            public string Iso { get; set; }

            public string[] CityNames { get; set; }
        }

        private class SiteMapDetails
        {
            public SiteMapDetails(Uri url, DateTime lastModifiedDate, string iso)
            {
                Url = url;
                LastModifiedDate = lastModifiedDate;
            }

            public Uri Url { get; }

            public DateTime LastModifiedDate { get; }

            public int Count { get; set; }

            public bool Exceed { get; set; }

            public bool DuplicatesExist { get; set; }

            public int DuplicatesCount { get; set; }

            public string Duplicates { get; set; }

            public string Iso { get; set; }
        }

        private class SiteMapNode
        {
            public string Location { get; set; }

            public string LastModified { get; set; }

            public string Priority { get; set; }

            public string ChangeFrequency { get; set; }

            public string SeaTitle { get; set; }

            public string Types { get; set; }
        }

        private class SearchParams
        {
            public SearchParams(string articleGroupName, int cityId, int countryId)
            {
                ArticleGroupName = articleGroupName;
                CityId = cityId;
                CountryId = countryId;
            }

            public string ArticleGroupName { get; }

            public int CityId { get; }

            public int CountryId { get; }
        }

        private IRepository Repository { get; }

        private ISettings Settings { get; }

        public SiteMapService(
            IRepository repository,
            ISettings settings)
        {
            Repository = repository;
            Settings = settings;
        }

        private static readonly string[] ArticleGroupNames = { "1k", "2k", "3k", "K", "C", "1k,K", "1k,C", "1k,2k", "2k,3k", };

        private readonly Dictionary<SeoParameterType, Func<DataBaseContext, SearchParams, IQueryable<ISeoParameter>>> DbSets = new Dictionary<SeoParameterType, Func<DataBaseContext, SearchParams, IQueryable<ISeoParameter>>>
        {
            [SeoParameterType.City] = (context, searchParams) => GetDbSet<DbCitySeoParameter>()(context).Where(x => x.CityId == searchParams.CityId),
            [SeoParameterType.District] = (context, searchParams) => GetDbSet<DbDistrictSeoParameter>()(context).Where(x => x.CityId == searchParams.CityId),
            [SeoParameterType.ArticleGroup] = (context, searchParams) => GetDbSet<DbArticleGroupSeoParameter>()(context).Where(x => x.CountryId == searchParams.CountryId && x.ArticleGroupInternalName == searchParams.ArticleGroupName),
            [SeoParameterType.Phrase] = (context, searchParams) => GetDbSet<DbPhraseSeoParameter>()(context).Where(x => x.CountryId == searchParams.CountryId),
            [SeoParameterType.Alias] = (context, searchParams) => GetDbSet<DbAliasSeoParameter>()(context).Where(x => (x.CityId == null || x.CityId == searchParams.CityId) && (x.ArticleGroupInternalName == null || x.ArticleGroupInternalName == searchParams.ArticleGroupName)),
        };

        private readonly Dictionary<SeoParameterType, string> UrlSectionPostfixes = new Dictionary<SeoParameterType, string>
        {
            [SeoParameterType.City] = SeoParameterType.City.ToString().ToLowerInvariant(),
            [SeoParameterType.District] = SeoParameterType.District.ToString().ToLowerInvariant(),
            [SeoParameterType.ArticleGroup] = SeoParameterType.ArticleGroup.ToString().ToLowerInvariant(),
            [SeoParameterType.Phrase] = SeoParameterType.Phrase.ToString().ToLowerInvariant(),
            [SeoParameterType.Alias] = SeoParameterType.Alias.ToString().ToLowerInvariant(),
        };

        private static Func<DataBaseContext, IQueryable<T>> GetDbSet<T>() where T : class, ISeoParameter
        {
            return context => context.Set<T>();
        }

        public async Task<string> GetSiteMapIndex(bool debug)
        {
            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XElement root = new XElement(xmlns + "sitemapindex");

            var nodes = await GetSiteMaps(debug);

            foreach (var sitemap in nodes)
            {
                var content = new List<object>
                {
                    new XElement(xmlns + "loc", sitemap.Url.ToString().ToSeoUrl() + (debug ? "?debug=true" : string.Empty)),
                    new XElement(xmlns + "lastmod", Uri.EscapeUriString(sitemap.LastModifiedDate.ToString("yyyy-MM-dd"))),
                };

                if (debug)
                {
                    content.AddRange(new []
                    {
                        new XElement(xmlns + "count", sitemap.Count),
                        new XElement(xmlns + "exceeded", sitemap.Exceed),
                        new XElement(xmlns + "duplicatesExist", sitemap.DuplicatesExist),
                        new XElement(xmlns + "duplicatesCount", sitemap.DuplicatesCount),
                        new XElement(xmlns + "duplicates", sitemap.Duplicates),
                    });
                }

                root.Add(new XElement(xmlns + "sitemap", content));
            }

            XDocument document = new XDocument(root);
            return ToStringWithDeclaration(document);
        }

        private async Task<SiteMapDetails[]> GetSiteMaps(bool debug)
        {
            var countryDetails = await GetCountryDetails();

            var urlSections = countryDetails
                .SelectMany(
                    x => ArticleGroupNames.SelectMany(
                        y => x.CityNames.Select(
                            z => (iso: x.Iso, articleGroupName: ToUrlSection(y), cityName: z))));

            var baseUri = GetBaseUri();
            var now = DateTime.Now;

            var result = urlSections
                .Select(x =>
                {
                    var url = new Uri(baseUri, $"/sitemap/{x.iso}/{x.articleGroupName}/{x.cityName}.xml");
                    var lastModifiedDate = now;
                    if (!debug)
                    {
                        return new SiteMapDetails(url, lastModifiedDate, x.iso);
                    }

                    var nodes = GetNodes(x.articleGroupName, x.cityName).ToArray();
                    var count = nodes.Length;
                    var duplicates = nodes.GroupBy(y => (y.Location, y.Types)).ToArray();
                    var duplicatesNodes = duplicates.Where(y => y.Count() > 1).ToArray();

                    var item = new SiteMapDetails(url, lastModifiedDate, x.iso)
                    {
                        Count = count,
                        Exceed = count > 50000,
                        DuplicatesExist = duplicatesNodes.Length > 0,
                        DuplicatesCount = duplicatesNodes.Length,
                        Duplicates = string.Join("\n", duplicatesNodes.Select(y => $"count: {y.Count()} location: {y.Key.Item1} types: {y.Key.Item2}"))
                    };
                    return item;
                })
                .OrderBy(x => x.Iso)
                .ThenByDescending(x => x.DuplicatesCount)
                .ThenByDescending(x => x.Count)
                .ToArray();
            return result;
        }

        private async Task<CountryDetails[]> GetCountryDetails()
        {
            using (var context = Repository.GetContext())
            {
                var countries = (await context.Set<DbCountry>()
                        .Select(x => new
                        {
                            x.Id,
                            x.Iso,
                        })
                        .ExecuteAsync())
                    .ToDictionary(x => x.Id, x => x.Iso);

                var cities = await context.Set<DbUser>()
                    .Where(x => !string.IsNullOrEmpty(x.NameForViewers) && x.CityId != null)
                    .Select(x => new
                    {
                        x.City.InternalName,
                        x.City.CountryId,
                    })
                    .ExecuteAsync();

                return cities
                    .GroupBy(x => x.CountryId)
                    .Select(x => new CountryDetails
                    {
                        Iso = countries[x.Key],
                        CityNames = x.Select(y => y.InternalName).ToArray(),
                    })
                    .ToArray();
            }
        }

        public string GetSiteMap(string articleGroupUrlSection, string country, string city, bool debug)
        {
            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XElement root = new XElement(xmlns + "urlset");

            var nodes = GetNodes(articleGroupUrlSection, city).ToArray();
            var baseUri = GetBaseUri();
            foreach (var sitemapNode in nodes)
            {
                var content = new List<object>
                {
                    new XElement(xmlns + "loc", new Uri(baseUri, $"/{country}/{sitemapNode.Location}").ToString().ToSeoUrl()),
                    new XElement(xmlns + "lastmod", Uri.EscapeUriString(sitemapNode.LastModified)),
                    new XElement(xmlns + "changefreq", Uri.EscapeUriString(sitemapNode.ChangeFrequency)),
                    new XElement(xmlns + "priority", Uri.EscapeUriString(sitemapNode.Priority)),
                };

                if (debug)
                {
                    content.AddRange(new []
                    {
                        new XElement(xmlns + "seatitle", sitemapNode.SeaTitle),
                    });
                }

                root.Add(new XElement(xmlns + "url", content));
            }

            XDocument document = new XDocument(root);
            return ToStringWithDeclaration(document);
        }

        private IEnumerable<SiteMapNode> GetNodes(string articleGroupUrlSection, string city)
        {
            var formats = new[]
            {
                new[] { SeoParameterType.ArticleGroup, SeoParameterType.City, SeoParameterType.District, SeoParameterType.Phrase, },
                new[] { SeoParameterType.ArticleGroup, SeoParameterType.City, SeoParameterType.Phrase, },
                new[] { SeoParameterType.ArticleGroup, SeoParameterType.City, SeoParameterType.District, },
                new[] { SeoParameterType.ArticleGroup, SeoParameterType.District, SeoParameterType.Phrase, },
                new[] { SeoParameterType.ArticleGroup, SeoParameterType.City, },
                new[] { SeoParameterType.ArticleGroup, SeoParameterType.District, },
                new[] { SeoParameterType.Alias, },
            };

            var cityId = SearchCityId(city);
            if (!cityId.HasValue)
            {
                yield break;
            }

            var countryId = SearchCountryId(cityId.Value);
            if (!countryId.HasValue)
            {
                yield break;
            }

            var articleGroupName = ToArticleGroupName(articleGroupUrlSection);

            Dictionary<SeoParameterType, ISeoParameter[]> dataSets;
            using (var context = Repository.GetContext())
            {
                dataSets = DbSets
                    .Select(x => (
                        x.Key,
                        x.Value(context, new SearchParams(articleGroupName, cityId.Value, countryId.Value)).AsNoTracking().ToArray()
                    ))
                    .ToDictionary(x => x.Item1, x => x.Item2);
            }

            var combinations = formats
                .Select(x =>
                {
                    return x.Select(y => (seoParameterType: y, dataSet: dataSets[y])).ToArray();
                })
                .ToArray();

            var now = DateTime.Now.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            foreach (var combination in combinations)
            {
                var nodePriority = string.Format("{0:0.0}", (0.5 + ((.5 / 4) * combination.Length)));

                foreach (var placement in Placements(combination))
                {
                    var node = new SiteMapNode();
                    var seoTitle = string.Join(" ", placement.Select(x => x.seoParameter.Value));
                    var url = string.Join("/", placement.Select(x => x.seoParameter.Alias + "-" + UrlSectionPostfixes[x.seoParameterType]));

                    node.Location = url;
                    node.LastModified = now;
                    node.Priority = nodePriority;
                    node.ChangeFrequency = "hourly";

                    node.SeaTitle = seoTitle;
                    node.Types = string.Join(",", combination.Select(x => x.seoParameterType));

                    yield return node;
                }
            }
        }

        private int? SearchCityId(string city)
        {
            using (var context = Repository.GetContext())
            {
                return context
                    .Set<DbCity>()
                    .Where(x => x.InternalName == city)
                    .Select(x => (int?)x.Id)
                    .FirstOrDefault();
            }
        }

        private int? SearchCountryId(int cityId)
        {
            using (var context = Repository.GetContext())
            {
                return context
                    .Set<DbCity>()
                    .Where(x => x.Id == cityId)
                    .Select(x => (int?)x.CountryId)
                    .FirstOrDefault();
            }
        }

        private IEnumerable<(SeoParameterType seoParameterType, ISeoParameter seoParameter)[]> Placements((SeoParameterType seoParameterType, ISeoParameter[] dataSet)[] combination)
        {
            var combinationMaxIndexes = combination.Select(x => x.dataSet.Length).ToArray();

            foreach (var placementIndexes in PlacementIndexes(combinationMaxIndexes))
            {
                var placement = new (SeoParameterType seoParameterType, ISeoParameter seoParameter)[combination.Length];

                for (int i = 0; i < combination.Length; i++)
                {
                    placement[i] = (combination[i].seoParameterType, combination[i].dataSet[placementIndexes[i]]);
                }

                var seoRoutingResult = new SeoRoutingResult();
                var possiblePlacement = placement.All(x => !x.seoParameter.UpdateRoutingResult(seoRoutingResult));

                if (possiblePlacement)
                {
                    yield return placement;
                }
            }
        }

        private IEnumerable<int[]> PlacementIndexes(int[] maxIndexes)
        {
            // Because we know that the max combination length is 4 is mush easy to make it as known number of nested loops
            var positions = maxIndexes.Length;

            for (int i = 0; i < maxIndexes[0]; i++)
            {
                if (positions > 1)
                {
                    for (int j = 0; j < maxIndexes[1]; j++)
                    {
                        if (positions > 2)
                        {
                            for (int k = 0; k < maxIndexes[2]; k++)
                            {
                                if (positions > 3)
                                {
                                    for (int l = 0; l < maxIndexes[3]; l++)
                                    {
                                        yield return new[] { i, j, k, l };
                                    }
                                }
                                else
                                {
                                    yield return new[] { i, j, k };
                                }
                            }
                        }
                        else
                        {
                            yield return new[] { i, j };
                        }
                    }
                }
                else
                {
                    yield return new[] { i, };
                }
            }
        }

        private Uri GetBaseUri()
        {
            return new Uri(Settings.GetSiteUrl());
        }

        private string ToUrlSection(string articleGroupName)
        {
            return articleGroupName.Replace(",", "-");
        }

        private string ToArticleGroupName(string urlSection)
        {
            return urlSection.Replace("-", ",");
        }

        private string ToStringWithDeclaration(XDocument doc)
        {
            StringBuilder sb = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.Append(doc.ToString(SaveOptions.DisableFormatting));
            return sb.ToString();
        }
    }
}