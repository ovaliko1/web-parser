﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Mappers;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;
using WebParser.DataAccess.Helpers;

namespace WebParser.Application.Services.File
{
    internal class FileService : IInternalFileService
    {
        private class DbFileAndFileData
        {
            public DbFile DbFile { get; set; }

            public byte[] FileData { get; set; }
        }

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IRepository Repository { get; }

        private IStorage Storage { get; }

        private IDateTimeService DateTimeService { get; }

        public FileService(
            IRepository repository,
            IStorage storage,
            IDateTimeService dateTimeService)
        {
            Repository = repository;
            Storage = storage;
            DateTimeService = dateTimeService;
        }

        public async Task<CreatedFile[]> CreateTemporaryFiles(int userId, int? siteId, FileDetails[] fileDetails)
        {
            return (await CreateTemporaryFiles(userId, siteId, fileDetails.ToArray(x => new KeyValuePair<int, FileDetails>(-1, x))))
                .ToArray(x => x.Value);
        }

        public async Task<KeyValuePair<int, CreatedFile>[]> CreateTemporaryFiles(int userId, int? siteId, KeyValuePair<int, FileDetails>[] fileDetails)
        {
            var filesToSave = await UploadTemporaryFilesToInternalStorage(userId, siteId, fileDetails);

            using (var context = Repository.GetContext())
            using (var transaction = context.BeginTransaction())
            {
                context.Set<DbFile>().AddRange(filesToSave.Select(x => x.Value.DbFile));

                context.ValidateAndSave();
                transaction.Commit();
            }

            return filesToSave.ToArray(x => new KeyValuePair<int, CreatedFile>(x.Key, new CreatedFile
            {
                Id = x.Value.DbFile.Id,
                FileUrl = x.Value.DbFile.FileUrl,
                FileData = x.Value.FileData,
            }));
        }

        private async Task<KeyValuePair<int, DbFileAndFileData>[]> UploadTemporaryFilesToInternalStorage(int userId, int? siteId, KeyValuePair<int, FileDetails>[] fileDetails)
        {
            var filesToSave = new List<KeyValuePair<int, DbFileAndFileData>>(fileDetails.Length);
            foreach (var fileDetail in fileDetails)
            {
                var fileKey = $"{Guid.NewGuid()}.{fileDetail.Value.FileExtension}";
                string fileUrl;
                try
                {
                    fileUrl = await Storage.UploadToTemporaryStorage(fileKey, fileDetail.Value.ContentType, fileDetail.Value.FileData);
                }
                catch (Exception ex)
                {
                    Log.Error($"Upload file to internal storage. UserId: {userId}, siteId: {siteId}, fileName {fileDetail.Value.FileName}", ex);
                    continue;
                }

                var now = DateTimeService.GetCurrentDateTimeUtc();
                filesToSave.Add(new KeyValuePair<int, DbFileAndFileData>(fileDetail.Key, new DbFileAndFileData
                {
                    DbFile = new DbFile
                    {
                        UserId = userId,
                        SiteId = siteId,
                        ContentType = fileDetail.Value.ContentType,
                        OriginalFileName = fileDetail.Value.FileName,
                        FileExtension = fileDetail.Value.FileExtension,
                        FileKey = fileKey,
                        FileUrl = fileUrl,
                        CreatedDate = now,
                    },
                    FileData = fileDetail.Value.FileData,
                }));
            }
            return filesToSave.ToArray();
        }

        public async Task<CreatedFile> CreatePersistentFile(int userId, int? siteId, FileDetails fileDetails)
        {
            var fileExtension = Path.GetExtension(fileDetails.FileName);
            var fileKey = $"{Guid.NewGuid()}.{fileExtension}";

            var fileUrl = await Storage.UploadToPersistentStorage(fileKey, fileDetails.ContentType, fileDetails.FileData);

            using (var context = Repository.GetContext())
            {
                var newFile = context.Set<DbFile>().Add(new DbFile
                {
                   UserId = userId,
                   SiteId = siteId,
                   ContentType = fileDetails.ContentType,
                   OriginalFileName = fileDetails.FileName,
                   FileExtension = fileExtension,
                   FileKey = fileKey,
                   FileUrl = fileUrl,
                   CreatedDate = DateTimeService.GetCurrentDateTimeUtc(),
                });
                context.ValidateAndSave();

                return ToCreatedFile(newFile, fileDetails.FileData);
            }
        }

        private static CreatedFile ToCreatedFile(DbFile newFile, byte[] fileData)
        {
            var result = newFile.Map<DbFile, CreatedFile>();
            result.FileData = fileData;
            return result;
        }

        public async Task<FileDetails> GetPersistentFile(int fileId)
        {
            using (var context = Repository.GetContext())
            {
                var file = context.FindFirst<DbFile, Common.Exceptions.FileNotFoundException>(x => x.Id == fileId, $"File with id {fileId} not found.");
                var fileData = await Storage.FetchPersistentFileContent(file.FileKey);

                return new FileDetails
                {
                    FileName = file.OriginalFileName,
                    ContentType = file.ContentType,
                    FileData = fileData,
                };
            }
        }
    }
}
