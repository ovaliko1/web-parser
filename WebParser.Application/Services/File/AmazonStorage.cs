﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Model;
using WebParser.Application.Services.Settings;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.File
{
    internal class AmazonStorage : IStorage
    {
        private const string AmazonEveryoneGrantee = "http://acs.amazonaws.com/groups/global/AllUsers";

        private ISettings Settings { get; }

        public AmazonStorage(ISettings settings)
        {
            Settings = settings;
        }

        public async Task<string> UploadToPersistentStorage(string fileKey, string contentType, byte[] data)
        {
            return await Upload(Settings.GetAmazonPersistentFilesBucketName(), fileKey, contentType, data);
        }

        public async Task<string> UploadToTemporaryStorage(string fileKey, string contentType, byte[] data)
        {
            return await Upload(Settings.GetAmazonTemporaryFilesBucketName(), fileKey, contentType, data);
        }

        private async Task<string> Upload(string bucketName, string fileKey, string contentType, byte[] data)
        {
            using (var memoryStream = new MemoryStream(data))
            using (var amazonClient = new AmazonS3Client())
            {
                var everyone = new S3Grant
                {
                    Grantee = new S3Grantee { URI = AmazonEveryoneGrantee },
                    Permission = S3Permission.READ,
                };

                var putRequest = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = fileKey,
                    ContentType = contentType,
                    InputStream = memoryStream,
                    Grants = new List<S3Grant> { everyone }
                };

                var response = await amazonClient.PutObjectAsync(putRequest);
                if (response.HttpStatusCode != HttpStatusCode.OK)
                {
                    throw new AmazonException($"Response status code: {response.HttpStatusCode}");
                }

                return $"{Settings.GetAmazonS3FileBaseUrl()}{bucketName}/{fileKey}";
            }
        }

        public async Task<byte[]> FetchPersistentFileContent(string fileKey)
        {
            var bucketName = Settings.GetAmazonPersistentFilesBucketName();
            var request = new GetObjectRequest
            {
                BucketName = bucketName,
                Key = fileKey
            };

            using (var amazonClient = new AmazonS3Client(Amazon.RegionEndpoint.USEast1))
            using (var response = await amazonClient.GetObjectAsync(request))
            using (var responseStream = response.ResponseStream)
            {
                var result = responseStream.GetBytes();
                return result;
            }
        }
    }
}
