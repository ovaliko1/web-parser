﻿using System.Threading.Tasks;

namespace WebParser.Application.Services.File
{
    internal interface IStorage
    {
        Task<string> UploadToTemporaryStorage(string fileKey, string contentType, byte[] data);

        Task<string> UploadToPersistentStorage(string fileKey, string contentType, byte[] data);

        Task<byte[]> FetchPersistentFileContent(string fileKey);
    }
}
