﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.File;

namespace WebParser.Application.Services.File
{
    internal interface IInternalFileService : IFileService
    {
        Task<FileDetails> GetPersistentFile(int fileId);
    }
}
