﻿namespace WebParser.Application.Services.Settings
{
    internal interface ISettings
    {
        string GetAmazonS3FileBaseUrl();

        string GetAmazonTemporaryFilesBucketName();

        string GetAmazonPersistentFilesBucketName();

        string GetSiteUrl();

        string GetPingUrl();

        string GetRegisterViewerUrl();

        string GetViewerArticlePreviewUrlFormat();

        int GetTrialAccessInMinutes();

        int GetHoursForSharing();

        string GetTempFilesPath();

        string GetAllowedRegistrationIp();

        string GetTesseractTraningDataPath();

        string GetGoogleGeocodeApiKey();

        string GetGoogleNearBySearchApiKey();

        string GetGoogleDirectionsApiKey();

        string GetPuppeterToolPath();

        int GetArticlesExpirationDays();

        int GetPreviewImageMaxWidth();

        int GetPreviewImageMaxHeight();

        string GetOneSignalAppId();

        string GetOneSignalRestApiKey();

        int GetMaxParsingFailedCount();

        string GetOneSignalProxyAddress();

        string GetOneSignalProxyUser();

        string GetOneSignalProxyPassword();

        string GetYandexGeoCodeApiKey();
    }
}
