﻿using System.Configuration;
using System.IO;
using WebParser.Application.Interfaces.Services.Settings;

namespace WebParser.Application.Services.Settings
{
    internal class Settings : ISettings, ICommonSettings
    {
        public string GetAmazonS3FileBaseUrl()
        {
            return ConfigurationManager.AppSettings["AmazonS3FileBaseUrl"] ?? string.Empty;
        }

        public string GetAmazonTemporaryFilesBucketName()
        {
            return ConfigurationManager.AppSettings["AmazonS3TemporaryFilesBucketName"] ?? string.Empty;
        }

        public string GetAmazonPersistentFilesBucketName()
        {
            return ConfigurationManager.AppSettings["AmazonS3PersistentFilesBucketName"] ?? string.Empty;
        }

        public string GetSiteUrl()
        {
            return ConfigurationManager.AppSettings["SiteUrl"] ?? string.Empty;
        }

        public string GetVkLogin()
        {
            return ConfigurationManager.AppSettings["VkLogin"] ?? string.Empty;
        }

        public string GetVkPassword()
        {
            return ConfigurationManager.AppSettings["VkPassword"] ?? string.Empty;
        }

        public long GetVkImageUploadGroupId()
        {
            return GetLong("VkImageUploadGroupId");
        }

        public long GetVkImageUploadAlbumId()
        {
            return GetLong("VkImageUploadAlbumId");
        }

        public string GetVkApplicationSecretKey()
        {
            return ConfigurationManager.AppSettings["ApplicationSecretKey"] ?? string.Empty;
        }

        public int GetFirstRateOpeningsCount()
        {
            return GetInt("FirstRateOpeningsCount");
        }

        public int GetFirstRateOpeningsPrice()
        {
            return GetInt("FirstRateOpeningsPrice");
        }

        public int GetSecondRateOpeningsCount()
        {
            return GetInt("SecondRateOpeningsCount");
        }

        public int GetSecondRateOpeningsPrice()
        {
            return GetInt("SecondRateOpeningsPrice");
        }

        public int GetThirdRateOpeningsCount()
        {
            return GetInt("ThirdRateOpeningsCount");
        }

        public int GetThirdRateOpeningsPrice()
        {
            return GetInt("ThirdRateOpeningsPrice");
        }

        public int GetFourthRateOpeningsCount()
        {
            return GetInt("FourthRateOpeningsCount");
        }

        public int GetFourthRateOpeningsPrice()
        {
            return GetInt("FourthRateOpeningsPrice");
        }

        public int GetFirstRateDaysCount()
        {
            return GetInt("FirstRateDaysCount");
        }

        public int GetFirstRateDaysPrice()
        {
            return GetInt("FirstRateDaysPrice");
        }

        public int GetSecondRateDaysCount()
        {
            return GetInt("SecondRateDaysCount");
        }

        public int GetSecondRateDaysPrice()
        {
            return GetInt("SecondRateDaysPrice");
        }

        public int GetThirdRateDaysCount()
        {
            return GetInt("ThirdRateDaysCount");
        }

        public int GetThirdRateDaysPrice()
        {
            return GetInt("ThirdRateDaysPrice");
        }

        public int GetFourthRateDaysCount()
        {
            return GetInt("FourthRateDaysCount");
        }

        public int GetFourthRateDaysPrice()
        {
            return GetInt("FourthRateDaysPrice");
        }

        public long GetVkApplicationId()
        {
            return GetLong("VkApplicationId");
        }

        public int GetTrialAccessInMinutes()
        {
            return GetInt("TrialAccessInMinutes");
        }

        public int GetHoursForSharing()
        {
            return GetInt("HoursForSharing");
        }

        public string GetTempFilesPath()
        {
            return ConfigurationManager.AppSettings["TempFilesPath"] ?? string.Empty;
        }

        public string GetAllowedRegistrationIp()
        {
            return ConfigurationManager.AppSettings["AllowedRegistrationIp"] ?? string.Empty;
        }

        public string GetSupportEmail()
        {
            return ConfigurationManager.AppSettings["SupportEmail"] ?? string.Empty;
        }

        public string GetPaymentEmail()
        {
            return ConfigurationManager.AppSettings["PaymentEmail"] ?? string.Empty;
        }

        public string GetTesseractTraningDataPath()
        {
            return ConfigurationManager.AppSettings["TesseractTraningDataPath"] ?? string.Empty;
        }

        public string GetGoogleGeocodeApiKey()
        {
            return ConfigurationManager.AppSettings["GoogleGeocodeApiKey"] ?? string.Empty;
        }

        public string GetGoogleNearBySearchApiKey()
        {
            return ConfigurationManager.AppSettings["GoogleNearBySearchApiKey"] ?? string.Empty;
        }

        public string GetGoogleDirectionsApiKey()
        {
            return ConfigurationManager.AppSettings["GoogleDirectionsApiKey"] ?? string.Empty;
        }

        public string GetPuppeterToolPath()
        {
            return ConfigurationManager.AppSettings["PuppeterToolPath"] ?? string.Empty;
        }

        public bool GetRenderMetrics()
        {
            return GetBool("RenderMetrics", true);
        }

        public string GetYandexMetrikaCounterName()
        {
            return GetValue("YandexMetrikaCounterName");
        }

        public string GetYandexGeoCodeApiKey()
        {
            return GetValue("YandexGeoCodeApiKey");
        }

        public string GetPingUrl()
        {
            return Path.Combine(GetSiteUrl(), "api/ping/ping");
        }

        public string GetRegisterViewerUrl()
        {
            return Path.Combine(GetSiteUrl(), "account/registerviewer/");
        }

        public string GetViewerArticlePreviewUrlFormat()
        {
            return Path.Combine(GetSiteUrl(), GetValue("ViewerArticlePreviewUrlFormat"));
        }

        public string GetYandexPaymentReceiver()
        {
            return GetValue("YandexPaymentReceiver");
        }

        public int GetArticlesExpirationDays()
        {
            return GetInt("ArticlesExpirationDays");
        }

        public int GetPreviewImageMaxWidth()
        {
            return GetInt("PreviewImageMaxWidth", 280);
        }

        public int GetPreviewImageMaxHeight()
        {
            return GetInt("PreviewImageMaxHeight", 186);
        }

        public string GetOneSignalAppId()
        {
            return ConfigurationManager.AppSettings["OneSignalAppId"] ?? string.Empty;
        }

        public string GetOneSignalRestApiKey()
        {
            return ConfigurationManager.AppSettings["OneSignalRestApiKey"] ?? string.Empty;
        }

        public string GetOneSignalProxyAddress()
        {
            return ConfigurationManager.AppSettings["OneSignalProxyAddress"] ?? string.Empty;
        }

        public string GetOneSignalProxyUser()
        {
            return ConfigurationManager.AppSettings["OneSignalProxyUser"] ?? string.Empty;
        }

        public string GetOneSignalProxyPassword()
        {
            return ConfigurationManager.AppSettings["OneSignalProxyPassword"] ?? string.Empty;
        }

        public int GetMaxParsingFailedCount()
        {
            return GetInt("MaxParsingFailedCount", 3);
        }

        private static long GetLong(string key)
        {
            long result;
            return long.TryParse(GetValue(key), out result) ? result : -1;
        }

        private static int GetInt(string key)
        {
            return GetInt(key, -1);
        }

        private static int GetInt(string key, int @deafult)
        {
            int result;
            return int.TryParse(GetValue(key), out result) ? result : @deafult;
        }

        private static bool GetBool(string key, bool @deafult)
        {
            bool result;
            return bool.TryParse(GetValue(key), out result) ? result : @deafult;
        }

        private static string GetValue(string key)
        {
            return ConfigurationManager.AppSettings[key] ?? string.Empty;
        }
    }
}
