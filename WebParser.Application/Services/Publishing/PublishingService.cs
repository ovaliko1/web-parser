﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.Publishing;
using WebParser.Application.Interfaces.Services.Publishing.Entities;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Services.Parsing.ArticleFieldsRecognizing;
using WebParser.Application.Services.Parsing.ImageRecognizing;
using WebParser.Application.Services.Parsing.ParsedContent;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Publishing
{
    internal class PublishingService : IPublishingService
    {
        private IParsedContentService ParsedContentService { get; }

        private IArticleFieldsRecognizingService ArticleFieldsRecognizingService { get; }

        private IUserService UserService { get; }

        public PublishingService(
            IParsedContentService parsedContentService,
            IArticleFieldsRecognizingService articleFieldsRecognizingService,
            IUserService userService)
        {
            ParsedContentService = parsedContentService;
            ArticleFieldsRecognizingService = articleFieldsRecognizingService;
            UserService = userService;
        }

        public async Task SaveArticle(PublisherArticle article)
        {
            var currentUserId = UserService.GetCurrentUserId();
            if (!currentUserId.HasValue)
            {
                throw new PublishingException("Current UserId is empty");
            }

            var site = UserService.GetPublisherSite(currentUserId.Value);
            if (site == null)
            {
                throw new PublishingException($"Publisher Site not found by publisher id: {currentUserId}");
            }

            var publisherDetails = UserService.GetPublisherById(currentUserId.Value);
            if (publisherDetails == null)
            {
                throw new PublishingException($"Publisher not found by publisher id: {currentUserId}");
            }

            var parsedArticleResult = new ParsedArticleResult(new[]
            {
                ParsedValue.Create(article.Name).Update(x =>
                {
                    x.Name = "Имя";
                    x.ParsedKeyType = ParsedKeyType.Name;
                    x.ShowOnVoteForm = true;
                }),
                ParsedValue.Create(article.Description).Update(x =>
                {
                    x.Name = "Описание";
                    x.ParsedKeyType = ParsedKeyType.Description;
                    x.ShowOnVoteForm = true;
                }),
                ParsedValue.Create(article.MetroStation).Update(x =>
                {
                    x.Name = "Метро";
                    x.ParsedKeyType = ParsedKeyType.Metro;
                    x.ShowOnVoteForm = true;
                }),
                ParsedValue.Create(article.Identity).Update(x =>
                {
                    x.Name = "Контакт";
                    x.ParsedKeyType = ParsedKeyType.Phone;
                    x.ShowOnVoteForm = true;
                    x.HideOnVkPost = true;
                    x.RecognizedPhones = new[] { new RecognizedPhone(article.Identity, article.Identity.ExtractDigits(), 100) };
                }),
                ParsedValue.CreateLinkToFile(article.Images, false).Update(x =>
                {
                    x.Name = "Изображения";
                    x.ParsedKeyType = ParsedKeyType.Images;
                    x.HideOnVkPost = true;
                }),
            });

            parsedArticleResult.RecognizedFieldsData = await ArticleFieldsRecognizingService.RecognizeArticleFields(parsedArticleResult, site);
            ParsedContentService.SaveParsedArticle(parsedArticleResult, site.UserId, site.Id, publisherDetails.ArticleGroup.Id);
        }
    }
}
