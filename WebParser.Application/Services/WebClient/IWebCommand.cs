﻿using System.Threading.Tasks;
using RestSharp;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Services.File;
using WebParser.Application.Services.Parsing;
using WebParser.Application.Services.SiteStructure;

namespace WebParser.Application.Services.WebClient
{
    internal interface IWebCommand
    {
        Task<bool> MakeRequest(string url, Method method, (string name, string value)[] headers, params Parameter[] parameters);

        Task<bool> MakeRequest(
            string url,
            Method method,
            (string name, string value)[] headers,
            (string address, string user, string password) proxy,
            params Parameter[] parameters);

        Task<FileDetails> DownloadFile(int siteId, int userId, string baseUrl, string relativeUrl, Cookie[] cookies);

        Task UploadArticleToWebHook(int siteId, int userId, ParsedArticleResult[] articles, ParseArticleSettings articleSettings);
    }
}
