﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebParser.Application.Services.WebClient
{
    public class WebHookParsedContent
    {
        [JsonExtensionData]
        public Dictionary<string, WebHookParsedValue> Members { get; set; }
    }
}
