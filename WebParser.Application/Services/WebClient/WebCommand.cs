﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using log4net;
using RestSharp;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.WebClient;
using WebParser.Application.Mappers;
using WebParser.Application.Services.Parsing;
using WebParser.Application.Services.Parsing.ParsingProgress;
using Cookie = WebParser.Application.Interfaces.Services.SiteStructure.Cookie;

namespace WebParser.Application.Services.WebClient
{
    internal class WebCommand : IWebCommand, IWebClient
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly Regex DataUrlRegex = new Regex(@"data:(?<contenttype>image/(?<type>.+?));base64,(?<data>.+)");

        private IParsingProgressErrorsHandler ParsingProgressErrorsHandler { get; }

        public WebCommand(IParsingProgressErrorsHandler parsingProgressErrorsHandler)
        {
            ParsingProgressErrorsHandler = parsingProgressErrorsHandler;
        }

        private class WebHookBodyJson
        {
            public string SiteUrl { get; set; }

            public string Category { get; set; }

            public string Link { get; set; }

            public WebHookParsedContent ParsedContent { get; set; }
        }

        public Task<bool> MakeRequest(
            string url,
            Method method,
            (string name, string value)[] headers,
            params Parameter[] parameters)
        {
            return MakeRequest(url, method, headers, default((string, string, string)), parameters);
        }

        public async Task<bool> MakeRequest(
            string url,
            Method method,
            (string name, string value)[] headers,
            (string address, string user, string password) proxy,
            params Parameter[] parameters)
        {
            var client = new RestClient(url);

            if (!string.IsNullOrEmpty(proxy.address) &&
                !string.IsNullOrEmpty(proxy.user) &&
                !string.IsNullOrEmpty(proxy.password))
            {
                client.Proxy = new WebProxy
                {
                    Address = new Uri(proxy.address),
                    Credentials = new NetworkCredential(proxy.user, proxy.password),
                };
            }

            var request = new RestRequest(method);
            foreach (var header in headers)
            {
                request.AddHeader(header.name, header.value);
            }

            foreach (var parameter in parameters)
            {
                request.AddParameter(parameter);
            }

            var response = await client.ExecuteTaskAsync(request);
            if (response.ErrorException != null)
            {
                Log.Error($"MakeRequest exception: {url} status: {response.StatusCode}", response.ErrorException);
                return false;
            } 

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }

            Log.Error($"MakeRequest: {url} status: {response.StatusCode}");
            return false;
        }

        public async Task<FileDetails> DownloadFile(int siteId, int userId, string baseUrl, string relativeUrl, Cookie[] cookies)
        {
            return await ParsingProgressErrorsHandler.HandleErrors(
            () => DownloadFileCore(siteId, userId, baseUrl, relativeUrl, cookies),
            siteId, userId,
            $"Error during DownloadFile. BaseUrl: {baseUrl} relativeUrl: {relativeUrl}");
        }

        private async Task<FileDetails> DownloadFileCore(int siteId, int userId, string baseUrl, string relativeUrl, Cookie[] cookies)
        {
            Uri uri = UrlHelper.ConstructFullUri(baseUrl, relativeUrl);
            if (uri == null)
            {
                return null;
            }

            if (uri.Scheme == "data")
            {
                return GetResultFromDataUri(uri);
            }

            var client = new RestClient(uri);
            SetCookie(client, uri, cookies);

            var request = new RestRequest(Method.GET);

            var response = await client.ExecuteTaskAsync(request);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var result = new FileDetails
                {
                    FileData = response.RawBytes,
                    ContentType = response.ContentType,
                    FileName = Path.GetFileName(UrlHelper.ConstructFullUri(baseUrl, relativeUrl).ToString()),
                    FileExtension = GetFileExtension(response.ContentType),
                };
                return result;
            }
            if (response.ErrorException != null)
            {
                ParsingProgressErrorsHandler.LogParsingProgressError(siteId, userId, $"Error during DownloadFile. BaseUrl: {baseUrl} relativeUrl: {relativeUrl}", response.ErrorException);
            }

            return null;
        }

        private static string GetFileExtension(string contentType)
        {
            // var extension = Path.GetExtension(fileDetail.Value.FileName);
            var contentTypeSplit = contentType?.Split('/');
            var extensionFromContentType = contentTypeSplit?.Length == 2 ? contentTypeSplit[1] : null;

            // TODO: Extension by bytes
            var fileExtension = extensionFromContentType ?? "png";
            return fileExtension;
        }

        private static void SetCookie(RestClient client, Uri uri, Cookie[] cookies)
        {
            if (cookies?.Length > 0)
            {
                client.CookieContainer = new CookieContainer();

                foreach (var cookie in cookies)
                {
                    client.CookieContainer.Add(new System.Net.Cookie(cookie.Name, cookie.Value, "/", uri.GetCookieDomain()));
                }
            }
        }

        private static FileDetails GetResultFromDataUri(Uri uri)
        {
            var matchResult = DataUrlRegex.Match(uri.AbsoluteUri);

            if (!matchResult.Success)
            {
                return null;
            }

            var base64Data = matchResult.Groups["data"].Value;
            var contentType = matchResult.Groups["contenttype"].Value;
            var type = matchResult.Groups["type"].Value;

            if (string.IsNullOrEmpty(base64Data) ||
                string.IsNullOrEmpty(contentType) ||
                string.IsNullOrEmpty(type))
            {
                return null;
            }

            return new FileDetails
            {
                FileName = $"{Guid.NewGuid()}.{type}",
                ContentType = contentType,
                FileData = Convert.FromBase64String(base64Data),
            };
        }

        public async Task UploadArticleToWebHook(int siteId, int userId, ParsedArticleResult[] articles, ParseArticleSettings articleSettings)
        {
            if (string.IsNullOrEmpty(articleSettings.Site.WebHookUrl))
            {
                return;
            }

            var tasks = articles.Select(x => UploadArticleToWebHook(siteId, userId, x, articleSettings));
            await Task.WhenAll(tasks);
        }

        private async Task UploadArticleToWebHook(int siteId, int userId, ParsedArticleResult article, ParseArticleSettings articleSettings)
        {
            await ParsingProgressErrorsHandler.HandleErrors(
            () => UploadArticleToWebHookCore(siteId, userId, article, articleSettings),
            siteId, userId,
            $"Error during UploadArticleToWebHookCore. WebHookUrl: {articleSettings.Site.WebHookUrl} Article.Link: {article.Link}");
        }

        private async Task UploadArticleToWebHookCore(int siteId, int userId, ParsedArticleResult article, ParseArticleSettings articleSettings)
        {
            Uri uri;
            if (!Uri.TryCreate(articleSettings.Site.WebHookUrl, UriKind.Absolute, out uri))
            {
                ParsingProgressErrorsHandler.LogParsingProgressError(siteId, userId, 
                $"Not valid UploadToWebHook Url. Articles Group Id: {articleSettings.ArticlesGroup.Id}, Name: {articleSettings.ArticlesGroup.InternalName}; WebHookUrl: {articleSettings.Site.WebHookUrl}");
                return;
            }

            var client = new RestClient(uri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-type", "application/json");
            request.AddHeader("Authorization", "Basic YWRtaW46ZXhlY29tYmF0");
            request.AddJsonBody(new WebHookBodyJson
            {
                SiteUrl = articleSettings.Site.Link,
                Category = articleSettings.ArticlesGroup.InternalName,
                Link = article.Link,
                ParsedContent = new WebHookParsedContent
                {
                    Members = article.ParsedValues.ToDictionary(x => x.Name, x => x.Map<ParsedValue, WebHookParsedValue>()),
                },
            });

            var response = await client.ExecuteTaskAsync(request);

            if (response.StatusCode != HttpStatusCode.OK || response.ErrorException != null)
            {
                ParsingProgressErrorsHandler.LogParsingProgressError(
                siteId, userId,
                $"Error during upload parsed article to WebHook. HttpStatusCode: {response.StatusCode}; Articles Group Id: {articleSettings.ArticlesGroup.Id}, Name: {articleSettings.ArticlesGroup.InternalName}; WebHookUrl: {articleSettings.Site.WebHookUrl}; Error Message: {response.ErrorMessage}; Exception Message: {response.ErrorException?.Message}", 
                response.ErrorException);
            }
        }
    }
}
