﻿namespace WebParser.Application.Services.WebClient
{
    public class WebHookParsedValue
    {
        public string Value { get; set; }

        public string ValueType { get; set; }
    }
}