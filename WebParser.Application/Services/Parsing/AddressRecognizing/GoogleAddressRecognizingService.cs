﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using RestSharp;
using WebParser.Application.Interfaces.Services.Parsing.AddressRecognizing;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities;
using WebParser.Application.Services.Settings;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.AddressRecognizing
{
    internal class GoogleAddressRecognizingService : IAddressRecognizingService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ISettings Settings { get; }

        public GoogleAddressRecognizingService(ISettings settings)
        {
            Settings = settings;
        }

        private enum Status
        {
            Unknown,
            Ok,
            ZeroResults,
            OverQueryLimit,
            RequestDenied,
            InvalidRequest,
            UnknownError,
        }

        private static readonly Dictionary<string, Status> Statuses = new Dictionary<string, Status>
        {
            ["OK"] = Status.Ok,
            ["ZERO_RESULTS"] = Status.ZeroResults,
            ["OVER_QUERY_LIMIT"] = Status.OverQueryLimit,
            ["REQUEST_DENIED"] = Status.RequestDenied,
            ["INVALID_REQUEST"] = Status.InvalidRequest,
            ["UNKNOWN_ERROR"] = Status.UnknownError,
        };

        public Task<AddressInfo> RecognizeAddressByLocation(string locationStr, AddressInfo previousAddressInfo, Site site)
        {
            throw new System.NotImplementedException();
        }

        public async Task<AddressInfo> RecognizeAddress(string address, Site site)
        {
            var client = new RestClient("https://maps.googleapis.com/maps/api/geocode/");

            var request = new RestRequest("json", Method.GET);
            request.AddQueryParameter("key", Settings.GetGoogleGeocodeApiKey());
            request.AddQueryParameter("address", address);
            request.AddQueryParameter("components", $"country:{site.RegionCode}");
            request.AddQueryParameter("language", site.LanguageCode);
            request.AddQueryParameter("region", site.RegionCode);
            request.AddQueryParameter("bounds", site.GeoBounds);

            var response = await client.ExecuteTaskAsync<MapApiResultJson>(request);
            if (HasErrors(response, response.Data.Status))
            {
                return null;
            }

            var result = new AddressInfo();
            var addressResult = response.Data.AddressResults.FirstOrDefault();
            if (addressResult == null)
            {
                Log.Error($"Google api call Address result is null: {response.ResponseUri}");
                return null;
            }

            result.FormattedAddress = addressResult.FormattedAddress;
            result.Latitude = addressResult.GeometryInfo.Location.Latitude;
            result.Longitude = addressResult.GeometryInfo.Location.Longitude;

            result.NearestMetroStation = await RecognizeNearestMetroStations(addressResult.GeometryInfo.Location, site);

            return result;
        }

        private async Task<NearestMetroStation> RecognizeNearestMetroStations(GeoCoordinates from, Site site)
        {
            var client = new RestClient("https://maps.googleapis.com/maps/api/place/nearbysearch/");

            var request = new RestRequest("json", Method.GET);
            request.AddQueryParameter("key", Settings.GetGoogleNearBySearchApiKey());
            request.AddQueryParameter("location", string.Format(CultureInfo.InvariantCulture, "{0:0.000000},{1:0.000000}", from.Latitude, from.Longitude));
            request.AddQueryParameter("rankby", "distance");
            request.AddQueryParameter("language", site.LanguageCode);
            request.AddQueryParameter("type", "subway_station");

            var response = await client.ExecuteTaskAsync<MetroStationsResultJson>(request);
            if (HasErrors(response, response.Data.Status))
            {
                return null;
            }

            var result = new NearestMetroStation();

            var metroStationResult = response.Data.Results.FirstOrDefault();
            if (metroStationResult == null)
            {
                return null;
            }

            result.Name = metroStationResult.Name;

            var leg = await RecognizeDistanceMetroStations(from, metroStationResult.GeometryInfo.Location, site);
            if (leg == null)
            {
                return result;
            }

            result.DistanceText = leg.Distance.Text;
            result.DistanceValue = leg.Distance.Value;
            result.DurationText = leg.Duration.Text;
            result.DurationValue = leg.Duration.Value / 60;

            return result;
        }

        private async Task<Leg> RecognizeDistanceMetroStations(GeoCoordinates from, GeoCoordinates to, Site site)
        {
            var client = new RestClient("https://maps.googleapis.com/maps/api/directions/");

            var request = new RestRequest("json", Method.GET);
            request.AddQueryParameter("key", Settings.GetGoogleDirectionsApiKey());
            request.AddQueryParameter("origin", string.Format(CultureInfo.InvariantCulture, "{0:0.000000},{1:0.000000}", from.Latitude, from.Longitude));
            request.AddQueryParameter("destination", string.Format(CultureInfo.InvariantCulture, "{0:0.000000},{1:0.000000}", to.Latitude, to.Longitude));
            request.AddQueryParameter("mode", "walking");
            request.AddQueryParameter("language", site.LanguageCode);
            request.AddQueryParameter("unit", "metric");

            var response = await client.ExecuteTaskAsync<DistanceResultJson>(request);
            if (HasErrors(response, response.Data.Status))
            {
                return null;
            }

            return response.Data.Routes.FirstOrDefault()?.Legs.FirstOrDefault();
        }

        private static bool HasErrors(IRestResponse response, string statusStr)
        {
            if (response.StatusCode != HttpStatusCode.OK ||
                response.ErrorException != null)
            {
                Log.Error($"Google api call: {response.ResponseUri}", response.ErrorException);
                return true;
            }

            var status = Statuses.GetValueOr(statusStr, Status.Unknown);
            if (status != Status.Ok)
            {
                Log.Warn($"Google api call: {response.ResponseUri}; Response status source/parsed: {statusStr}/{status}; Response content: {response.Content}");
                return true;
            }

            return false;
        }
    }
}
