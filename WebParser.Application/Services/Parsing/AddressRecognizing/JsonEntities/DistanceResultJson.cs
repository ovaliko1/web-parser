﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class MetroStationsResultJson
    {
        [DeserializeAs(Name = "results")]
        public List<MetroStationResult> Results { get; set; }

        [DeserializeAs(Name = "status")]
        public string Status { get; set; }
    }
}