﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class DistanceResultJson
    {
        [DeserializeAs(Name = "routes")]
        public List<Route> Routes { get; set; }

        [DeserializeAs(Name = "status")]
        public string Status { get; set; }
    }
}