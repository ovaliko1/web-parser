﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class Route
    {
        [DeserializeAs(Name = "bounds")]
        public Bounds Bounds { get; set; }

        [DeserializeAs(Name = "legs")]
        public List<Leg> Legs { get; set; }
    }
}