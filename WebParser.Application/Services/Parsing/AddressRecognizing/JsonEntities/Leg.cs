﻿using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class Leg
    {
        [DeserializeAs(Name = "distance")]
        public Step Distance { get; set; }

        [DeserializeAs(Name = "duration")]
        public Step Duration { get; set; }
    }
}