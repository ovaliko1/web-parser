﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class MapApiResultJson
    {
        [DeserializeAs(Name = "results")]
        public List<AddressResultJson> AddressResults { get; set; }

        [DeserializeAs(Name = "status")]
        public string Status { get; set; }
    }
}