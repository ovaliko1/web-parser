﻿using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class Bounds
    {
        [DeserializeAs(Name = "northeast")]
        public GeoCoordinates Northeast { get; set; }

        [DeserializeAs(Name = "southwest")]
        public GeoCoordinates Southwest { get; set; }
    }
}