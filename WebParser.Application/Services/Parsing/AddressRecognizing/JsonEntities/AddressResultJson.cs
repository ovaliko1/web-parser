﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class AddressResultJson
    {
        [DeserializeAs(Name = "address_components")]
        public List<AddressComponent> AddressComponents { get; set; }

        [DeserializeAs(Name = "formatted_address")]
        public string FormattedAddress { get; set; }

        [DeserializeAs(Name = "geometry")]
        public GeometryInfo GeometryInfo { get; set; }

        [DeserializeAs(Name = "place_id")]
        public string PlaceId { get; set; }

        [DeserializeAs(Name = "types")]
        public List<string> Types { get; set; }
    }
}