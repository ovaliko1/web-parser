﻿using System.Collections.Generic;
using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class AddressComponent
    {
        [DeserializeAs(Name = "long_name")]
        public string LongName { get; set; }

        [DeserializeAs(Name = "short_name")]
        public string ShortName { get; set; }

        [DeserializeAs(Name = "types")]
        public List<string> Types { get; set; }
    }
}