﻿using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class GeometryInfo
    {
        [DeserializeAs(Name = "bounds")]
        public Bounds Bounds { get; set; }

        [DeserializeAs(Name = "location")]
        public GeoCoordinates Location { get; set; }

        [DeserializeAs(Name = "location_type")]
        public string LocationType { get; set; }

        [DeserializeAs(Name = "viewport")]
        public Bounds Viewport { get; set; }
    }
}