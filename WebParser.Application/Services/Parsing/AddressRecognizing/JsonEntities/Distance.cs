﻿using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class Step
    {
        [DeserializeAs(Name = "text")]
        public string Text { get; set; }

        [DeserializeAs(Name = "value")]
        public int Value { get; set; }
    }
}