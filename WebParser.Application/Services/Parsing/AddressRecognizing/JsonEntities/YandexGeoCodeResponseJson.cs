﻿using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class YandexGeoCodeResponseJson
    {
        public GeoObjectCollectionElement Response { get; set; }

        public Error Error { get; set; }
    }

    internal class Error
    {
        public int Status { get; set; }

        public string Message { get; set; }
    }

    internal class GeoObjectCollectionElement
    {
        public GeoObjectCollection GeoObjectCollection { get; set; }
    }

    internal class GeoObjectCollection
    {
        public List<GeoObjectElement> FeatureMember { get; set; }
    }

    internal class GeoObjectElement
    {
        public GeoObject GeoObject { get; set; }
    }

    internal class GeoObject
    {
        public Point Point { get; set; }

        public MetaDataProperty MetaDataProperty { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public BoundedBy BoundedBy { get; set; }
    }

    internal class Point
    {
        public string Pos { get; set; }

        public GeoCoordinate GetGeoCoordinate()
        {
            var longitude = (double?)Pos?.Split(' ').FirstOrDefault()?.ToDecimal();
            var latitude = (double?)Pos?.Split(' ').LastOrDefault()?.ToDecimal();
            return longitude != null && latitude != null
                ? new GeoCoordinate(latitude.Value, longitude.Value)
                : null;
        }
    }

    internal class BoundedBy
    {
        public Envelope Envelope { get; set; }
    }

    internal class Envelope
    {
        public string LowerCorner { get; set; }

        public string UpperCorner { get; set; }
    }

    internal class MetaDataProperty
    {
        public GeocoderMetaData GeocoderMetaData { get; set; }
    }

    internal class GeocoderMetaData
    {
        public string Kind { get; set; }

        public string Text { get; set; }
    }
}
