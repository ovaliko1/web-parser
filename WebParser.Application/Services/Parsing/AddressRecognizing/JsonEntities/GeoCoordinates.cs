﻿using RestSharp.Deserializers;

namespace WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities
{
    internal class GeoCoordinates
    {
        [DeserializeAs(Name = "lat")]
        public decimal Latitude { get; set; }

        [DeserializeAs(Name = "lng")]
        public decimal Longitude { get; set; }
    }
}