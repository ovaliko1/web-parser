﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.Parsing.AddressRecognizing;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;

namespace WebParser.Application.Services.Parsing.AddressRecognizing
{
    internal interface IAddressRecognizingService
    {
        Task<AddressInfo> RecognizeAddressByLocation(string locationStr, AddressInfo previousAddressInfo, Site site);

        Task<AddressInfo> RecognizeAddress(string address, Site site);
    }
}