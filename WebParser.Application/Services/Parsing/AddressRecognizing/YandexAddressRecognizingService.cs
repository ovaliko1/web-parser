﻿using System;
using System.Device.Location;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using RestSharp;
using WebParser.Application.Interfaces.Services.Parsing.AddressRecognizing;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.Parsing.AddressRecognizing.JsonEntities;
using WebParser.Application.Services.Settings;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.AddressRecognizing
{
    internal class YandexAddressRecognizingService : IAddressRecognizingService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ISettings Settings { get; }

        public YandexAddressRecognizingService(ISettings settings)
        {
            Settings = settings;
        }

        public async Task<AddressInfo> RecognizeAddressByLocation(string locationStr, AddressInfo previousAddressInfo, Site site)
        {
            if (string.IsNullOrEmpty(locationStr))
            {
                return previousAddressInfo;
            }

            var locationArray = locationStr.Split(',');
            if (locationArray.Length != 2)
            {
                return previousAddressInfo;
            }

            (decimal latitude, decimal longitude) = (locationArray[0].ToDecimal(), locationArray[1].ToDecimal());

            var result = await RecognizeAddressCore($"{longitude},{latitude}", site);
            if (result == null)
            {
                result = previousAddressInfo ?? new AddressInfo();
            }

            // Cover case when reverse geocode result just city name (just one word)
            var formattedAddress = result.FormattedAddress ?? string.Empty;
            formattedAddress = formattedAddress.Split(' ').Length == 1 && !string.IsNullOrEmpty(previousAddressInfo?.FormattedAddress)
                ? previousAddressInfo.FormattedAddress
                : formattedAddress;

            result.FormattedAddress = formattedAddress;
            result.Latitude = latitude;
            result.Longitude = longitude;

            return result;
        }

        public async Task<AddressInfo> RecognizeAddress(string address, Site site)
        {
            if (string.IsNullOrWhiteSpace(address))
            {
                return null;
            }

            var geoBoundsArray = site.GeoBounds?.Split('|') ?? new string[0];
            return geoBoundsArray.Length == 2
                ? await RecognizeAddressCore(address, site, (geoBoundsArray[0], geoBoundsArray[1]))
                : await RecognizeAddressCore(address, site);
        }

        private async Task<AddressInfo> RecognizeAddressCore(string address, Site site, (string leftDown, string rightUp)? geoBounds = null)
        {
            var client = new RestClient("https://geocode-maps.yandex.ru/1.x/");
            var request = new RestRequest(Method.GET);
            request.AddQueryParameter("geocode", address);
            request.AddQueryParameter("apikey", Settings.GetYandexGeoCodeApiKey());
            request.AddQueryParameter("format", "json");
            request.AddQueryParameter("lang", $"{site.LanguageCode}_{site.RegionCode}");
            if (geoBounds.HasValue)
            {
                request.AddQueryParameter("bbox", $"{geoBounds.Value.leftDown}~{geoBounds.Value.rightUp}");
            }

            var (hasErrors, twoManyRequests, response) = await MakRequest(client, request);
            if (hasErrors)
            {
                if (twoManyRequests)
                {
                    await Task.Delay(3000);
                }

                return null;
            }

            var result = new AddressInfo();
            var addressResult = response.Data?.Response?.GeoObjectCollection?.FeatureMember?.FirstOrDefault()?.GeoObject;
            if (addressResult == null)
            {
                Log.Warn($"Yandex api call Address result is null: {response.ResponseUri}");
                return null;
            }

            result.FormattedAddress = string.IsNullOrWhiteSpace(addressResult.Name) ? address : addressResult.Name;

            var point = addressResult.Point?.GetGeoCoordinate();
            if (point != null)
            {
                result.Latitude = (decimal)point.Latitude;
                result.Longitude = (decimal)point.Longitude;
                result.NearestMetroStation = await RecognizeNearestMetroStations(point, site, geoBounds);
            }

            return result;
        }

        private async Task<NearestMetroStation> RecognizeNearestMetroStations(GeoCoordinate from, Site site, (string leftDown, string rightUp)? geoBounds)
        {
            var client = new RestClient("https://geocode-maps.yandex.ru/1.x/");

            var request = new RestRequest(Method.GET);
            request.AddQueryParameter("geocode", string.Format(CultureInfo.InvariantCulture, "{0:0.000000},{1:0.000000}", from.Latitude, from.Longitude));
            request.AddQueryParameter("apikey", Settings.GetYandexGeoCodeApiKey());
            request.AddQueryParameter("sco", "latlong");
            request.AddQueryParameter("format", "json");
            request.AddQueryParameter("lang", $"{site.LanguageCode}_{site.RegionCode}");
            request.AddQueryParameter("kind", "metro");
            if (geoBounds.HasValue)
            {
                request.AddQueryParameter("bbox", $"{geoBounds.Value.leftDown}~{geoBounds.Value.rightUp}");
            }

            var (hasErrors, twoManyRequests, response) = await MakRequest(client, request);
            if (hasErrors)
            {
                if (twoManyRequests)
                {
                    await Task.Delay(3000);
                }

                return null;
            }

            var result = new NearestMetroStation();

            var metroStationResult = response.Data?.Response?.GeoObjectCollection?.FeatureMember?.FirstOrDefault()?.GeoObject;
            if (metroStationResult?.Name == null)
            {
                return null;
            }

            result.Name = metroStationResult.Name.Replace("метро ", string.Empty);

            var to = metroStationResult.Point?.GetGeoCoordinate();
            if (to == null)
            {
                return result;
            }

            var leg = RecognizeDistanceMetroStations(from, to);
            if (leg == null)
            {
                return result;
            }

            result.DistanceText = leg.Distance.Text;
            result.DistanceValue = leg.Distance.Value;
            result.DurationText = leg.Duration.Text;
            result.DurationValue = leg.Duration.Value;

            return result;
        }

        private Leg RecognizeDistanceMetroStations(GeoCoordinate from, GeoCoordinate to)
        {
            var distance = (int)(from.GetDistanceTo(to) * 1.2); // in meters; 1.2 - curvilinear factor   
            var duration = distance / 83; // in minutes: approximating human walk speed ~ 83 meters per minutes

            if (distance > 0)
            {
                return new Leg
                {
                    Distance = new Step
                    {
                        Value = distance,
                        Text = string.Format("{0:N1} км", distance / 1000.0f),
                    },
                    Duration = new Step
                    {
                        Value = duration,
                        Text = $"{duration} мин. пешком",
                    }
                };
            }

            return null;
        }

        private static async Task<(
                bool hasErrors,
                bool tooManyRequests,
                IRestResponse<YandexGeoCodeResponseJson> response)>
                    MakRequest(RestClient client, RestRequest request)
        {
            try
            {
                var response = await client.ExecuteTaskAsync<YandexGeoCodeResponseJson>(request);

                if (response.StatusCode != HttpStatusCode.OK ||
                    response.ErrorException != null ||
                    response.Data?.Error != null)
                {
                    var message = "Yandex api call: " +
                                  $"StatusCode: {response.StatusCode} " +
                                  $"ResponseUri: {response.ResponseUri} " +
                                  $"Error?.Status: {response.Data?.Error?.Status} " +
                                  $"Error?.Message: {response.Data?.Error?.Message} " +
                                  $"ErrorException.Message: {response.ErrorException?.Message}";

                    if (response.StatusCode == (HttpStatusCode)429 || response.Data?.Error?.Status == 429)
                    {
                        Log.Warn(message);
                        return (true, true, null);
                    }

                    Log.Error(message, response.ErrorException);
                    return (true, false, null);
                }

                return (false, false, response);
            }
            catch (Exception ex)
            {
                var message = $"Unhandled exception during Yandex api call: \n{string.Join("\n", request.Parameters.Select(x => $"{x.Name} : {x.Value}"))}";
                Log.Error(message, ex);
                return (true, false, null);
            }
        }
    }
}
