﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.Services.Parsing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.BlackList;
using WebParser.Application.Services.File;
using WebParser.Application.Services.Parsing.ParsedContent;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Application.Services.Parsing.SiteContent;
using WebParser.Application.Services.Parsing.SiteContent.Entities;
using WebParser.Application.Services.WebClient;
using WebParser.Application.Services.WhiteList;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing
{
    internal class ParsingJob : IParsingJob, IParsingService
    {
        private enum FullnessCheckResult
        {
            Pass,
            PhoneIsEmpty,
            Empty,
        }

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ISiteStructureService SiteStructureService { get; }

        private ISiteService SiteService { get; }

        private IParsedContentService ParsedContentService { get; }

        private IWebCommand WebCommand { get; }

        private ISiteContentService SiteContentService { get; }

        private IInternalFileService InternalFileService { get; }

        private IParsingLogger ParsingLogger { get; }

        private IBlackListService BlackListService { get; }

        private IWhiteListService WhiteListService { get; }

        public ParsingJob(
            ISiteStructureService siteStructureService,
            IParsedContentService parsedContentService,
            IWebCommand webCommand,
            ISiteContentService siteContentService,
            ISiteService siteService,
            IInternalFileService internalFileService,
            IParsingLogger parsingLogger,
            IBlackListService blackListService,
            IWhiteListService whiteListService)
        {
            SiteStructureService = siteStructureService;
            ParsedContentService = parsedContentService;
            WebCommand = webCommand;
            SiteContentService = siteContentService;
            SiteService = siteService;
            InternalFileService = internalFileService;
            ParsingLogger = parsingLogger;
            BlackListService = blackListService;
            WhiteListService = whiteListService;
        }

        public async Task<ParsedArticle> GetParsingResultPreview(int siteId, ArticlesGroup articlesGroup)
        {
            var site = SiteStructureService.GetSiteById(siteId);

            var articleLinks = await GetArticleLinksForPreview(articlesGroup, site);
            var firstArticleLink = articleLinks[0];

            var parseSettings = ParseArticleSettings.Preview(site, articlesGroup);
            var parsedArticle = (await ParseArticlesByBatches(parseSettings, firstArticleLink)).FirstOrDefault();

            if (parsedArticle == null)
            {
                throw new ParsingException($"Failed to download article content by url: {UrlHelper.ConstructFullUri(site.Link, firstArticleLink, true)}");
            }

            var result = new ParsedArticle(parsedArticle.ParsedValues)
            {
                Link = firstArticleLink,
                FoundArticleLinks = articleLinks,
                RecognizedFieldsData = parsedArticle.RecognizedFieldsData,
            };
            return result;
        }

        private async Task<string[]> GetArticleLinksForPreview(ArticlesGroup articlesGroup, Site site)
        {
            var linksToArticlesType = articlesGroup.GetLinksToArticlesType();
            string[] articleLinks;
            switch (linksToArticlesType)
            {
                case LinksToArticlesType.SpecificLinks:
                case LinksToArticlesType.LinksFormat:
                    var linkToArticles = articlesGroup.GetFirstArticlesGroupLink();
                    if (string.IsNullOrEmpty(linkToArticles))
                    {
                        throw new ParsingException("No one links to download articles group");
                    }

                    var parsedArticlesGroup = ParseArticlesGroupLink(site, articlesGroup, linkToArticles, true);
                    if (parsedArticlesGroup == null)
                    {
                        throw new ParsingException($"Failed to download articles group content by url: {UrlHelper.ConstructFullUri(site.Link, articlesGroup.Links[0], true)}");
                    }

                    if (parsedArticlesGroup.LinksToArticles == null || parsedArticlesGroup.LinksToArticles.Length == 0)
                    {
                        throw new ParsingException("Did not find Links to Articles.");
                    }

                    articleLinks = parsedArticlesGroup.LinksToArticles;
                    break;

                case LinksToArticlesType.LinksFile:
                    var articlesLinks = await articlesGroup.GetLinksToArticles(InternalFileService);
                    if (articlesLinks == null || articlesLinks.Length == 0)
                    {
                        throw new ParsingException("Did not find Links to Articles.");
                    }
                    articleLinks = articlesLinks;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return articleLinks;
        }

        public async Task<bool> Run(int siteId)
        {
            var site = SiteService.GetSite(siteId);
            try
            {
                SiteService.SetSiteInParsingState(siteId);

                ParsingLogger.Log(site.Id, site.UserId, ParsingLogStatus.Success, "Site parsing started.");

                var articleGroups = SiteStructureService.GetArticleGroupsBySite(site.Id);

                var results = new List<bool>();
                foreach (var articleGroup in articleGroups)
                {
                    results.Add(await ProcessArticleGroup(site, articleGroup));
                }

                ParsingLogger.Log(site.Id, site.UserId, ParsingLogStatus.Success, "Site parsing complete.");

                // TODO: Fill result and error
                return !results.Contains(false);
            }
            catch (Exception ex)
            {
                Log.Error($"Unhandled exception during site parsing. Site Id: {siteId}", ex);
                return false;
            }
            finally
            {
                SiteService.ResetSiteParsingState(siteId);
            }
        }

        private async Task<bool> ProcessArticleGroup(Site site, ArticlesGroup articlesGroup)
        {
            // TODO: Review and rewrite. Log any errors
            var processResult = true;

            ParsingLogger.Log(site.Id, site.UserId, ParsingLogStatus.Success, $"Articles group parsing started. Name: {articlesGroup.InternalName} Id: {articlesGroup.Id}");

            var linksToArticlesType = articlesGroup.GetLinksToArticlesType();

            switch (linksToArticlesType)
            {
                case LinksToArticlesType.SpecificLinks:
                    foreach (var link in articlesGroup.Links)
                    {
                        await ParseArticleGroup(site, articlesGroup, link);
                    }
                    break;

                case LinksToArticlesType.LinksFormat:
                    var continueParsing = true;
                    for (int i = articlesGroup.StartPage ?? 0; continueParsing; i += articlesGroup.StepSize ?? 0)
                    {
                        var linkToArticles = articlesGroup.ConstructArticlesGroupLinkByFormat(i);
                        var result = await ParseArticleGroup(site, articlesGroup, linkToArticles);
                        continueParsing = result;
                    }
                    break;

                case LinksToArticlesType.LinksFile:
                    var articleLinks = await articlesGroup.GetLinksToArticles(InternalFileService);
                    await ParseArticlesByBatches(ParseArticleSettings.RealParse(site, articlesGroup), articleLinks);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            return processResult;
        }

        private async Task<bool> ParseArticleGroup(Site site, ArticlesGroup articlesGroup, string link)
        {
            if (string.IsNullOrEmpty(link))
            {
                return false;
            }

            var parsedArticlesGroup = ParseArticlesGroupLink(site, articlesGroup, link, false);
            if (parsedArticlesGroup == null)
            {
                return false;
            }
            parsedArticlesGroup.ArticlesGroupId = articlesGroup.Id;
            parsedArticlesGroup.SiteId = site.Id;

            ParsingLogger.Log(
                site.Id,
                site.UserId,
                ParsingLogStatus.Success,
                $"Articles Group Name {articlesGroup.InternalName}: found {parsedArticlesGroup.LinksToArticles?.Length} links to articles by url: {link}");

            if (parsedArticlesGroup.LinksToArticles == null || parsedArticlesGroup.LinksToArticles.Length == 0)
            {
                return false;
            }

            var parseSettings = ParseArticleSettings.RealParse(site, articlesGroup);
            await ParseArticlesByBatches(parseSettings, parsedArticlesGroup.LinksToArticles);
            return true;
        }

        private async Task<ParsedArticleResult[]> ParseArticlesByBatches(ParseArticleSettings settings, params string[] articleLinks)
        {
            var links = GetLinks(settings, articleLinks);
            if (links.Length == 0)
            {
                return new ParsedArticleResult[0];
            }

            var result = new List<ParsedArticleResult>(links.Length);
            var batchesCount = ((links.Length - 1) / settings.ArticlesGroup.BatchSize) + 1;
            for (int i = 0; i < batchesCount; i++)
            {
                var from = i * settings.ArticlesGroup.BatchSize;
                var length = from + settings.ArticlesGroup.BatchSize < links.Length ? settings.ArticlesGroup.BatchSize : links.Length - from;
                var batch = new Uri[length];
                Array.Copy(links, from, batch, 0, length);

                var batchResult = await ParseArticleBatch(settings, batch, i);
                result.AddRange(batchResult);

                if (!settings.IsPreview && settings.ArticlesGroup.BatchDelayInSeconds > 0)
                {
                    await Task.Delay(TimeSpan.FromSeconds(settings.ArticlesGroup.BatchDelayInSeconds));
                }
            }
            return result.ToArray();
        }

        private async Task<ParsedArticleResult[]> ParseArticleBatch(ParseArticleSettings settings, Uri[] batch, int batchNumber)
        {
            if (!settings.IsPreview)
            {
                ParsingLogger.Log(
                    settings.Site.Id,
                    settings.Site.UserId,
                    ParsingLogStatus.Success,
                    $"Articles Group Name {settings.ArticlesGroup.InternalName}: started articles parsing by batches. Found {batch.Length} new articles, batch number {batchNumber + 1}, batch size: {settings.ArticlesGroup.BatchSize}");
            }

            try
            {
                var fetchConfiguration = CreateFetchConfiguration(settings, batch);
                return await ParseArticles(fetchConfiguration, settings);
            }
            catch (Exception ex)
            {
                ParsingLogger.Log(
                    settings.Site.Id,
                    settings.Site.UserId,
                    ParsingLogStatus.Fail,
                    $"Articles Group Name {settings.ArticlesGroup.InternalName}: Parsing Batch Exception. Message: {ex.Message}");

                Log.Error(
                    $"Exception during Parse ArticlesInBatches. Site: {settings.Site?.Id} - {settings.Site?.Name}, ArticlesGroup: {settings.ArticlesGroup?.Id} - {settings.ArticlesGroup?.InternalName}",
                    ex);

                return new ParsedArticleResult[0];
            }
        }

        private async Task<ParsedArticleResult[]> ParseArticles(FetchConfiguration fetchConfiguration, ParseArticleSettings settings)
        {
            if (fetchConfiguration.Links.Length == 0)
            {
                return new ParsedArticleResult[0];
            }

            var result = await SiteContentService.FetchArticlesContent(fetchConfiguration);

            if (settings.UploadToWebHook)
            {
                await WebCommand.UploadArticleToWebHook(fetchConfiguration.Site.Id, fetchConfiguration.Site.UserId, result.Result, settings);
            }

            if (settings.IsPreview)
            {
                return result.Result;
            }

            var fullnessCheckResult = new Dictionary<FullnessCheckResult, int>
            {
                { FullnessCheckResult.Pass, 0 },
                { FullnessCheckResult.Empty, 0 },
                { FullnessCheckResult.PhoneIsEmpty, 0 },
            };

            foreach (var parsedArticle in result.Result)
            {
                var checkResult = CheckOnFullness(parsedArticle, settings);
                fullnessCheckResult[checkResult]++;

                if (checkResult != FullnessCheckResult.Pass)
                {
                    ParsedContentService.SaveFailedParsedArticle(parsedArticle, fetchConfiguration.Site.UserId, fetchConfiguration.Site.Id, settings.ArticlesGroup.Id, checkResult.ToString());
                    continue;
                }

                switch ((JobType)settings.Site.JobTypeId)
                {
                    case JobType.Parsing:
                        ParsedContentService.SaveParsedArticle(parsedArticle, fetchConfiguration.Site.UserId, fetchConfiguration.Site.Id, settings.ArticlesGroup.Id);
                        break;

                    case JobType.BlackList:
                        BlackListService.SaveBlackListItems(parsedArticle.ParsedValues.GetArticleIdentities(), fetchConfiguration.Site.UserId, fetchConfiguration.Site.Id);
                        break;

                    case JobType.WhiteList:
                        WhiteListService.SaveWhiteListItems(parsedArticle.ParsedValues.GetArticleIdentities(), fetchConfiguration.Site.UserId, fetchConfiguration.Site.Id);
                        break;

                    case JobType.ParsingAndWhiteList:
                        var ids = WhiteListService.SaveWhiteListItems(parsedArticle.ParsedValues.GetArticleIdentities(), fetchConfiguration.Site.UserId, fetchConfiguration.Site.Id);
                        ParsedContentService.SaveParsedArticle(parsedArticle, fetchConfiguration.Site.UserId, fetchConfiguration.Site.Id, settings.ArticlesGroup.Id, ids.FirstOrNullableDefault());
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }

                // TODO: Refactor/Improve article save process (BulkInsert)
                await Task.Delay(100);
            }

            LogResult(result, settings, fetchConfiguration, fullnessCheckResult);

            return result.Result;
        }

        private void LogResult(
            ParsedContentResult<ParsedArticleResult> result,
            ParseArticleSettings settings,
            FetchConfiguration fetchConfiguration,
            Dictionary<FullnessCheckResult, int> fullnessCheckResult)
        {
            var logStatus = ParsingLogStatus.Success;
            if (fullnessCheckResult[FullnessCheckResult.Pass] == 0)
            {
                Log.Error(
                    "All articles do not pass check on fullness. " +
                    $"SpiderType: {fetchConfiguration.SpiderType} " +
                    $"Site: {settings.Site.Name} " +
                    $"Article Group: {settings.ArticlesGroup.InternalName}\n" +
                    $"Success/Empty/PhoneIsEmpty/Total: {fullnessCheckResult[FullnessCheckResult.Pass]}/{fullnessCheckResult[FullnessCheckResult.Empty]}/{fullnessCheckResult[FullnessCheckResult.PhoneIsEmpty]}/{fetchConfiguration.Links.Length}\n" +
                    $"Links:\n{string.Join<Uri>("\n", fetchConfiguration.Links)}\n" +
                    $"Spider output: {result.SpiderOutput}");
                logStatus = ParsingLogStatus.Fail;
            }
            else if (fullnessCheckResult.Any(x => x.Key != FullnessCheckResult.Pass && x.Value > 0))
            {
                Log.Info(
                    "Not all articles pass check on fullness. " +
                    $"SpiderType: {fetchConfiguration.SpiderType} " +
                    $"Site: {settings.Site.Name} " +
                    $"Article Group: {settings.ArticlesGroup.InternalName}\n" +
                    $"Success/Empty/PhoneIsEmpty/Total: {fullnessCheckResult[FullnessCheckResult.Pass]}/{fullnessCheckResult[FullnessCheckResult.Empty]}/{fullnessCheckResult[FullnessCheckResult.PhoneIsEmpty]}/{fetchConfiguration.Links.Length}\n" +
                    $"Links:\n{string.Join<Uri>("\n", fetchConfiguration.Links)}\n" +
                    $"Spider output: {result.SpiderOutput}");
            }

            ParsingLogger.Log(fetchConfiguration.Site.Id, fetchConfiguration.Site.UserId, logStatus,
                $"Articles Group Name {settings.ArticlesGroup.InternalName}: Parse article batch complete. " +
                $"SpiderType: {fetchConfiguration.SpiderType} " +
                $"Success/Empty/PhoneIsEmpty/Total {fullnessCheckResult[FullnessCheckResult.Pass]}/{fullnessCheckResult[FullnessCheckResult.Empty]}/{fullnessCheckResult[FullnessCheckResult.PhoneIsEmpty]}/{fetchConfiguration.Links.Length} articles.");
        }

        private static FullnessCheckResult CheckOnFullness(ParsedArticleResult parsedArticle, ParseArticleSettings settings)
        {
            if (parsedArticle.IsEmpty)
            {
                return FullnessCheckResult.Empty;
            }

            var phoneShouldBeParsed = settings.ArticlesGroup.ArticleSearchRules.Any(x => x.ParsedKeyTypeId == (int)ParsedKeyType.Phone);
            var phoneIsEmpty = parsedArticle.ParsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Phone)?.IsEmpty ?? true;

            if (phoneShouldBeParsed && phoneIsEmpty)
            {
                return FullnessCheckResult.PhoneIsEmpty;
            }
            return FullnessCheckResult.Pass;
        }

        private Uri[] GetLinks(ParseArticleSettings settings, string[] articleLinks)
        {
            var allLinks = articleLinks.Select(x => UrlHelper.ConstructFullUri(settings.Site.Link, x, true)).Where(x => x != null).ToArray();

            var links = settings.Site.JobTypeId == (int)JobType.Parsing || settings.Site.JobTypeId == (int)JobType.ParsingAndWhiteList
                ? ParsedContentService.GetNotParsedArticleLinks(settings.Site.Id, settings.ArticlesGroup.Id, allLinks)
                : allLinks;

            return links;
        }

        private ParsedArticlesGroup ParseArticlesGroupLink(Site site, ArticlesGroup articlesGroup, string link, bool isPreview)
        {
            try
            {
                var articlesGroupSearchRules = ConstructLinksToArticlesSearchRules(articlesGroup);
                var links = new[] { UrlHelper.ConstructFullUri(site.Link, link, true) };

                // TODO: Additional settings for SpiderType during ParseArticlesGroupLink?
                var fetchConfiguration = CreateFetchConfiguration(site, articlesGroup, articlesGroupSearchRules, links, (SpiderType)articlesGroup.LinksToArticlesSearchRuleSpiderTypeId, isPreview);

                var result = SiteContentService.FetchArticlesGroupContent(fetchConfiguration);
                if (result == null || result.Result.Length == 0 || result.Result[0].LinksToArticles == null || result.Result[0].LinksToArticles.Length == 0)
                {
                    if (!isPreview)
                    {
                        var errorMessage = $"Fetch Links to Articles FAILED. Site: {site.Name} Articles Group Name: {articlesGroup.InternalName}: Url: {links[0].AbsoluteUri}";

                        Log.Info($"{errorMessage} Output: {result?.SpiderOutput}");
                        ParsingLogger.Log(site.Id, site.UserId, ParsingLogStatus.Fail, errorMessage);
                    }
                    return null;
                }

                result.Result[0].Link = UrlHelper.ConstructFullUri(site.Link, link, true).ToString();
                return result.Result[0];
            }
            catch (Exception ex)
            {
                Log.Error($"Unhandled exception during Parse ArticlesGroupLink. Site: {site?.Id} - {site?.Name}, ArticlesGroup: {articlesGroup?.Id} - {articlesGroup?.InternalName}, link: {link}", ex);
                return null;
            }
        }

        private static SearchRule[] ConstructLinksToArticlesSearchRules(ArticlesGroup articlesGroup)
        {
            var result = new List<SearchRule>(2);

            if (articlesGroup.WaitMilliseconds.HasValue)
            {
                result.Add(new SearchRule
                {
                    ActionType = ActionType.Wait,
                    WaitMilseconds = articlesGroup.WaitMilliseconds,
                });
            }

            result.Add(articlesGroup.LinksToArticlesSearchRule.Update(x => x.FieldName = "LinksToArticles"));

            return result.ToArray();
        }

        private static FetchConfiguration CreateFetchConfiguration(ParseArticleSettings settings, Uri[] links)
        {
            return CreateFetchConfiguration(settings.Site, settings.ArticlesGroup, settings.ArticlesGroup.ArticleSearchRules, links, (SpiderType)settings.ArticlesGroup.SpiderTypeId, settings.IsPreview);
        }

        private static FetchConfiguration CreateFetchConfiguration(Site site, ArticlesGroup articlesGroup, SearchRule[] rules, Uri[] links, SpiderType spiderType, bool isPreview)
        {
            var fetchConfiguration = new FetchConfiguration
            {
                Site = site,
                Links = links,
                Rules = rules,
                SpiderType = spiderType,
                IsPreview = isPreview,
                DownloadFileDelayInMillseconds = articlesGroup.DownloadFileDelayInMillseconds,
                SpiderDoNotLoadImages = articlesGroup.SpiderDoNotLoadImages,
            };
            return fetchConfiguration;
        }
    }
}