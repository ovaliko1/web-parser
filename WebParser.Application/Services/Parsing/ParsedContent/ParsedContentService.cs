﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ArticleFieldsRecognizing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Mappers;
using WebParser.Application.Services.BlackList;
using WebParser.Application.Services.Parsing.BadArticleInfo;
using WebParser.Application.Services.Parsing.ParsedContent.JsonEntities;
using WebParser.Application.Services.Settings;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Parsing.ParsedContent
{
    internal class ParsedContentService : IParsedContentService
    {
        private IRepository Repository { get; }

        private IDateTimeService DateTimeService { get; }

        private IUserService UserService { get; }

        private IBlackListService BlackListService { get; }

        private IBadArticleInfoService BadArticleInfoService { get; }

        private ISiteStructureService SiteStructureService { get; }

        private IMetroMapService MetroMapService { get; }

        private ISettings Settings { get; }

        public ParsedContentService(
            IRepository repository,
            IDateTimeService dateTimeService,
            IUserService userService,
            IBlackListService blackListService,
            IBadArticleInfoService badArticleInfoService,
            ISiteStructureService siteStructureService,
            IMetroMapService metroMapService,
            ISettings settings)
        {
            Repository = repository;
            DateTimeService = dateTimeService;
            UserService = userService;
            BlackListService = blackListService;
            BadArticleInfoService = badArticleInfoService;
            SiteStructureService = siteStructureService;
            MetroMapService = metroMapService;
            Settings = settings;
        }

        public Uri[] GetNotParsedArticleLinks(int siteId, int articlesGroupId, Uri[] links)
        {
            using (var context = Repository.GetContext())
            {
                var strLinks = links.ToArray(x => x.ToString());
                var maxParsingFailedCount = Settings.GetMaxParsingFailedCount();

                var parsedLinks = context
                    .Set<DbParsedArticle>()
                    .Where(x =>
                        x.SiteId == siteId &&
                        x.ArticlesGroupId == articlesGroupId &&
                        strLinks.Contains(x.Link) &&
                        (x.ParsingFailedCount == null || x.ParsingFailedCount >= maxParsingFailedCount)
                    )
                    .Select(x => x.Link)
                    .Distinct()
                    .ToDictionary(x => x, x => x);

                var result = strLinks
                    .Where(x => !parsedLinks.ContainsKey(x))
                    .ToArray(x => new Uri(x));

                return result;
            }
        }

        public void SaveFailedParsedArticle(ParsedArticleResult parsedArticle, int userId, int siteId, int articlesGroupId, string parsingFailedReason)
        {
            int failedCount = 1;
            using (var context = Repository.GetContext())
            {
                var currentFailedCount = context
                    .Set<DbParsedArticle>()
                    .Where(x =>
                        x.SiteId == siteId &&
                        x.ArticlesGroupId == articlesGroupId &&
                        x.Link == parsedArticle.Link
                    )
                    .OrderByDescending(x => x.ParsingFailedCount)
                    .Select(x => x.ParsingFailedCount)
                    .FirstOrDefault();

                failedCount += currentFailedCount ?? 0;
            }

            SaveParsedArticle(parsedArticle, userId, siteId, articlesGroupId, null, failedCount, parsingFailedReason);
        }

        public void SaveParsedArticle(ParsedArticleResult parsedArticle, int userId, int siteId, int articlesGroupId)
        {
            SaveParsedArticle(parsedArticle, userId, siteId, articlesGroupId, null, null, null);
        }

        public void SaveParsedArticle(ParsedArticleResult parsedArticle, int userId, int siteId, int articlesGroupId, int? whiteListItemId)
        {
            SaveParsedArticle(parsedArticle, userId, siteId, articlesGroupId, whiteListItemId, null, null);
        }

        private void SaveParsedArticle(ParsedArticleResult parsedArticle, int userId, int siteId, int articlesGroupId, int? whiteListItemId, int? parsingFailedCount, string parsingFailedReason)
        {
            using (var context = Repository.GetContext())
            {
                var articleFieldsData = parsedArticle.RecognizedFieldsData;
                var badArticleInfo = BadArticleInfoService.GetBadArticleInfo(context, userId, siteId,
                    articleFieldsData.ArticleIdentity, articleFieldsData.AddressInfo?.FormattedAddress);

                var dbParsedArticle = new DbParsedArticle
                {
                    CreatedDate = DateTimeService.GetCurrentDateTimeUtc(),
                    UserId = userId,
                    SiteId = siteId,
                    ArticlesGroupId = articlesGroupId,
                    Link = parsedArticle.Link,
                    ParsedContent = parsedArticle.ParsedValues.ToJsonParsedContent().Serialize(),
                    DuplicateGroupId = badArticleInfo.DuplicateGroupId,
                    Identity = articleFieldsData.ArticleIdentity.Identity,
                    IdentityConfidencePercentage = articleFieldsData.ArticleIdentity.IdentityConfidencePercentage,
                    BlackListItemId = badArticleInfo.BlackListItemId,
                    Metro = articleFieldsData.MetroStations?.ToArray(x => x.Id.ToStringInvariant()).SerializeNonFormatting(),
                    PriceStr = articleFieldsData.PriceStr,
                    Price = articleFieldsData.Price,
                    FullSizeImages = articleFieldsData.ImageProcessingResult?.FullSizeImages.ToJsonImageInfo(),
                    ImagesPreview = articleFieldsData.ImageProcessingResult?.ImagesPreview.ToJsonImageInfo(),
                    WhiteListItemId = whiteListItemId,
                    HasImages = articleFieldsData.ImageProcessingResult?.FullSizeImages.Length > 0,
                    ParsingFailedCount = parsingFailedCount,
                    ParsingFailedReason = parsingFailedReason,
                };

                if (articleFieldsData.AddressInfo != null)
                {
                    dbParsedArticle.FormattedAddress = articleFieldsData.AddressInfo.FormattedAddress;
                    dbParsedArticle.Longitude = articleFieldsData.AddressInfo.Longitude;
                    dbParsedArticle.Latitude = articleFieldsData.AddressInfo.Latitude;

                    if (articleFieldsData.AddressInfo.NearestMetroStation != null)
                    {
                        dbParsedArticle.DistanceToMetroText = articleFieldsData.AddressInfo.NearestMetroStation.DistanceText;
                        dbParsedArticle.DistanceToMetroValue = articleFieldsData.AddressInfo.NearestMetroStation.DistanceValue;
                        dbParsedArticle.DurationToMetroText = articleFieldsData.AddressInfo.NearestMetroStation.DurationText;
                        dbParsedArticle.DurationToMetroValue = articleFieldsData.AddressInfo.NearestMetroStation.DurationValue;
                    }
                }

                context.Set<DbParsedArticle>().Add(dbParsedArticle);

                context.ValidateAndSave();
            }
        }

        public PagingResult<ApiParsedArticle> GetArticles(int articlesGroupId, int page, int pageSize, System.DateTime? from)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var countQuery = context.Set<DbParsedArticle>().Where(x => x.ArticlesGroupId == articlesGroupId && x.UserId == currentUser.Id);

                if (from.HasValue)
                {
                    countQuery = countQuery.Where(x => x.CreatedDate >= from);
                }

                var count = countQuery.Count();

                var skip = page * pageSize;

                var query = context
                    .Set<DbParsedArticle>()
                    .Where(x => x.ArticlesGroupId == articlesGroupId && x.UserId == currentUser.Id);

                if (from.HasValue)
                {
                    query = query.Where(x => x.CreatedDate >= from);
                }

                var articles = query
                    .OrderByDescending(x => x.CreatedDate)
                    .Skip(() => skip)
                    .Take(() => pageSize)
                    .ExecuteAndConvert(EntityMapper.Map<DbParsedArticle, ApiParsedArticle>);

                var result = new PagingResult<ApiParsedArticle>
                {
                    Items = articles,
                    TotalCount = count,
                };
                return result;
            }
        }

        public PagingResult<ViewerParsedArticleItem> ReadViewerArticles<T>(ViewerFilter viewerFilter, RequestFilters<T> filters)
        {
            using (var context = Repository.GetContext())
            {
                var availableArticleGroupIds = new HashSet<int>(
                    context
                        .Set<DbArticlesGroup>()
                        .Where(x => x.User.NameForViewers != null)
                        .Select(x => x.Id)
                        .Execute());

                var articleGroupIds = viewerFilter.ArticleGroupIds?.Length > 0 ?
                    viewerFilter
                        .ArticleGroupIds
                        .Where(x => availableArticleGroupIds.Contains(x))
                        .ToArray(x => (int?)x) :
                    availableArticleGroupIds.ToArray(x => (int?)x);

                var query = context
                    .Set<DbParsedArticle>()
                    .Where(
                        x =>
                            x.Site.User.CityId == viewerFilter.CityId &&
                            articleGroupIds.Contains(x.ArticlesGroupId));

                if (viewerFilter.MetroStationIds.Length > 0)
                {
                    query = ExpressionsHelper.SetMetroFilterPredicate(query, viewerFilter.MetroStationIds);
                }

                var articles = query
                    .Select(x => new ParsedArticleItemProjection
                    {
                        Id = x.Id,
                        CreatedDate = x.CreatedDate,
                        SiteId = x.SiteId,
                        ArticlesGroup = x.ArticlesGroup.InternalName,
                        Price = x.Price,
                        PriceStr = x.PriceStr,
                        Metro = x.Metro,
                        GoodRating = x.GoodRating,
                        BadRating = x.BadRating,
                        OutdateRating = x.OutdateRating,
                        IsDuplicate = x.IsDuplicate,
                        InBlackList = x.BlackListItemId != null,
                        InWhiteList = x.WhiteListItemId != null,
                        Link = x.Link,
                        DuplicateGroupId = x.DuplicateGroupId,
                        ParsingFailedCount = x.ParsingFailedCount,
                        ParsingFailedReason = x.ParsingFailedReason,
                    })
                    .ApplyFilters(filters, SetShowRatingFlag);

                return articles;
            }
        }

        private ViewerParsedArticleItem[] SetShowRatingFlag(ParsedArticleItemProjection[] articleProjections)
        {
            var filteredArticles = articleProjections.ToArray(x => x.Map<ParsedArticleItemProjection, ViewerParsedArticleItem>());
            return filteredArticles.ToArray(x =>
            {
                x.ShowRating = true;
                return x;
            });
        }

        public ViewerParsedArticle GetViewerParsedArticle(int parsedArticleId)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                IQueryable<DbParsedArticle> query = context
                    .Set<DbParsedArticle>();

                var article = query.FindFirst<DbParsedArticle, ParsedContentException>(x => x.Id == parsedArticleId, $"Parsed Article with Id {parsedArticleId} not found.");

                var watermarkInfo = context
                    .Set<DbSite>()
                    .Where(x => x.Id == article.SiteId)
                    .Select(x => new
                    {
                        x.WatermarkHeight,
                        x.WatermarkWidth,
                        x.WatermarkFontSize,
                        x.WatermarkRectX,
                        x.WatermarkRectY,
                        x.WatermarkTextX,
                        x.WatermarkTextY,
                    })
                    .FirstOrDefault();

                var canBeVoted = currentUser != null && (!context.Set<DbArticleRatingVote>().Any(x => x.UserId == currentUser.Id && x.ArticleId == parsedArticleId));

                var result = new ViewerParsedArticle(article.ParsedContent.Deserialize<JsonParsedContent>().ToParsedValues())
                {
                    Id = article.Id,
                    Link = article.Link,
                    CanBeVoted = canBeVoted,
                    HidePhone = currentUser == null,
                    InBlackList = article.BlackListItemId.HasValue,
                    InWhiteList = article.WhiteListItemId.HasValue,
                    WatermarkHeight = watermarkInfo?.WatermarkHeight,
                    WatermarkWidth = watermarkInfo?.WatermarkWidth,
                    WatermarkFontSize = watermarkInfo?.WatermarkFontSize,
                    WatermarkRectX = watermarkInfo?.WatermarkRectX,
                    WatermarkRectY = watermarkInfo?.WatermarkRectY,
                    WatermarkTextX = watermarkInfo?.WatermarkTextX,
                    WatermarkTextY = watermarkInfo?.WatermarkTextY,
                    RecognizedFieldsData = article.Map<DbParsedArticle, RecognizedArticleFieldsData>(),
                };

                return result;
            }
        }
    }
}
