﻿using System;
using System.Linq;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Mappers;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Common.MetroStations;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Parsing.ParsedContent
{
    internal static class ParsedContentHelper
    {
        public static ArticleIdentity[] GetArticleIdentities(this ParsedValue[] parsedValues)
        {
            return parsedValues
                .FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Phone)
                ?.RecognizedPhones
                ?.ToArray(x => x.Map<RecognizedPhone, ArticleIdentity>()) ?? new ArticleIdentity[0];
        }

        public static ArticleIdentity GetArticleIdentity(this ParsedValue[] parsedValues)
        {
            return parsedValues
                .FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Phone)
                ?.RecognizedPhones
                ?.FirstOrDefault()
                ?.Map<RecognizedPhone, ArticleIdentity>() ?? new ArticleIdentity();
        }

        public static ArticleIdentity ToArticleIdentity(this DbParsedArticle article)
        {
            return article == null ? null : new ArticleIdentity
            {
                Identity = article.Identity,
                IdentityConfidencePercentage = article.IdentityConfidencePercentage,
            };
        }
    }
}