﻿namespace WebParser.Application.Services.Parsing.ParsedContent.Entities
{
    internal class ApartmentItemProjection
    {
        public int Id { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public int? SiteId { get; set; }

        public string ArticlesGroup { get; set; }

        public int? Price { get; set; }

        public string PriceStr { get; set; }

        public int GoodRating { get; set; }

        public int BadRating { get; set; }

        public int OutdateRating { get; set; }

        public bool ShowRating { get; set; }

        public string MetroDistance { get; set; }

        public string MetroDuration { get; set; }

        public string Metro { get; set; }

        public bool InWhiteList { get; set; }

        public string ImagesPreview { get; set; }

        public bool HideBlacklistWarranty { get; set; }
    }
}
