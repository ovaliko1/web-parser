﻿namespace WebParser.Application.Services.Parsing.ParsedContent.JsonEntities
{
    internal class JsonParsedFieldValue
    {
        public string Value { get; set; }
    }
}