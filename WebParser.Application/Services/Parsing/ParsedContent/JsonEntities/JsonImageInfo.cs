﻿using WebParser.Application.DomainEntities;

namespace WebParser.Application.Services.Parsing.ParsedContent.JsonEntities
{
    public class JsonImageInfo : JsonBaseEntity
    {
        public int? FileId { get; set; }

        public string ImageUrl { get; set; }

        public int? Height { get; set; }

        public int? Width { get; set; }

        protected override int GetVersion()
        {
            return 1;
        }
    }
}