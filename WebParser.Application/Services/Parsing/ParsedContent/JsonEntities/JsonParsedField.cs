﻿namespace WebParser.Application.Services.Parsing.ParsedContent.JsonEntities
{
    internal class JsonParsedField
    {
        public string Name { get; set; }

        public int ParsedValueTypeId { get; set; }

        public string BackgroundColor { get; set; }

        public int ParsedKeyTypeId { get; set; }

        public bool ShowOnVoteForm { get; set; }

        public bool HideOnVkPost { get; set; }

        public JsonParsedFieldValue[] Values { get; set; }

        public JsonRecognizedPhone[] RecognizedPhones { get; set; }
    }
}