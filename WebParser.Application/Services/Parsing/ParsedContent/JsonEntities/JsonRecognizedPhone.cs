﻿namespace WebParser.Application.Services.Parsing.ParsedContent.JsonEntities
{
    public class JsonRecognizedPhone
    {
        public string RecognizedText { get; set; }

        public string FixedText { get; set; }

        public int MeanConfidencePercentage { get; set; }
    }
}