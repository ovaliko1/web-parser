﻿using WebParser.Application.DomainEntities;

namespace WebParser.Application.Services.Parsing.ParsedContent.JsonEntities
{
    internal class JsonParsedContent : JsonBaseEntity
    {
        protected override int GetVersion()
        {
            return 2;
        }

        public JsonParsedField[] Fields { get; set; }
    }
}
