﻿using System;
using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Services.File;
using WebParser.Application.Services.SiteStructure;
using WebParser.Common;

namespace WebParser.Application.Services.Parsing
{
    internal static class ArticlesGroupHelper
    {
        public static string ConstructArticlesGroupLinkByFormat(this ArticlesGroup articlesGroup, int? pageNumber)
        {
            if (string.IsNullOrEmpty(articlesGroup.LinksFormat) || !pageNumber.HasValue)
            {
                return string.Empty;
            }
            var linkToArticles = articlesGroup.LinksFormat.Replace("{page}", pageNumber.ToString());
            return linkToArticles;
        }

        public static string GetFirstArticlesGroupLink(this ArticlesGroup articlesGroup)
        {
            var linkToArticlesByFormat = articlesGroup.ConstructArticlesGroupLinkByFormat(articlesGroup.StartPage);

            if (string.IsNullOrEmpty(linkToArticlesByFormat) &&
               (articlesGroup.Links == null || articlesGroup.Links.Length == 0))
            {
                return string.Empty;
            }

            var linkToArticles = !string.IsNullOrEmpty(linkToArticlesByFormat) ? linkToArticlesByFormat : articlesGroup.Links?[0];
            return linkToArticles;
        }

        public static async Task<string[]> GetLinksToArticles(this ArticlesGroup articlesGroup, IInternalFileService fileService)
        {
            if (!articlesGroup.LinksFileId.HasValue)
            {
                throw new ArgumentException($"LinksFileId of passed articlesGroup {articlesGroup.Id} is empty.");
            }

            var file = await fileService.GetPersistentFile(articlesGroup.LinksFileId.Value);

            var str = System.Text.Encoding.Default.GetString(file.FileData);
            var result = str.Split(new [] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            return result;
        }

        public static LinksToArticlesType GetLinksToArticlesType(this ArticlesGroup articlesGroup)
        {
            if (articlesGroup.Links != null && articlesGroup.Links.Length > 0)
            {
                return LinksToArticlesType.SpecificLinks;
            }
            if (!string.IsNullOrEmpty(articlesGroup.LinksFormat))
            {
                return LinksToArticlesType.LinksFormat;
            }
            if (articlesGroup.LinksFileId.HasValue)
            {
                return LinksToArticlesType.LinksFile;
            }
            throw new ArgumentOutOfRangeException($"articlesGroup with id: {articlesGroup.Id} has inconsistent state of links to articles format");
        }
    }
}
