﻿using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Services.File;

namespace WebParser.Application.Services.Parsing.ImageRecognizing
{
    internal interface IImageRecognizingService
    {
        RecognizedImage[] RecognizedImages(CreatedFile[] images);
    }
}