﻿using System.Linq;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.ImageRecognizing
{
    internal static class RecognizedTextHelper
    {
        public static string ExtractDigits(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            return new string(text.Where(char.IsDigit).ToArray());
        }

        public static string ExtractPhoneNumber(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Empty;
            }

            var knownFixes = text
                .Replace('g', '8')
                .Replace("t1", "0")
                .Replace("tF", "0")
                .Replace("A", "4")
                .Replace("I", "1")
                .Replace("O", "0")
                .Replace("M", "4")
                .Replace("I", "1")
                .Replace("i", "6")
                .Replace("T", "7")
                .Replace("fV", "8")
                .Replace("&", "8");

            var onlyDigits = knownFixes.Where(char.IsDigit).ToArray();

            if (onlyDigits.Length > 11)
            {
                for (int i = 0; i < onlyDigits.Length; i++)
                {
                    // 8911000700700 -> 8911000 00 00
                    if ((i == 7 || i == 10) && onlyDigits[i] == '7')
                    {
                        onlyDigits[i] = ' ';
                    }
                }

                // remove spaces
                onlyDigits = knownFixes.Where(char.IsDigit).ToArray();
            }

            // 79110000000 -> 89110000000
            if (onlyDigits.Length == 11 && onlyDigits[0] == '7')
            {
                onlyDigits[0] = '8';
            }

            var result = new string(onlyDigits);
            return result;
        }

        public static int? ExtractPrice(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            var knownFixes = text
                .ReplaceIgnoreCase("тыс", "000")
                .ReplaceIgnoreCase("тысяч", "000")
                .ReplaceIgnoreCase("к", "000")
                .ReplaceIgnoreCase("тр", "000");

            var onlyDigits = knownFixes.Where(char.IsDigit).ToArray();

            // int.MaxValue = 2147483647 - 10 digits. 9 to be sure
            var digitStr = new string(onlyDigits).Cut(9);
            return digitStr.ToNullableInt();
        }
    }
}
