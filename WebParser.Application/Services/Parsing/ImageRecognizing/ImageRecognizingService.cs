﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using log4net;
using Tesseract;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Services.Settings;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.ImageRecognizing
{
    internal class ImageRecognizingService : IImageRecognizingService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ISettings Settings { get; }

        public ImageRecognizingService(ISettings settings)
        {
            Settings = settings;
        }

        public RecognizedImage[] RecognizedImages(CreatedFile[] images)
        {
            return images.ToArray(RecognizedImage);
        }

        private RecognizedImage RecognizedImage(CreatedFile image)
        {
            try
            {
                using (var stream = new MemoryStream(image.FileData))
                using (var source = (Bitmap)Image.FromStream(stream))
                using (var bitmap = source.Clone(new Rectangle(0, 0, source.Width, source.Height), PixelFormat.Format32bppArgb))
                using (var engine = new TesseractEngine(Settings.GetTesseractTraningDataPath(), "eng", EngineMode.TesseractAndCube))
                using (var page = engine.Process(bitmap))
                {
                    var text = page.GetText();
                    var meanConfidence = page.GetMeanConfidence();

                    return new RecognizedImage
                    {
                        Text = text,
                        MeanConfidencePercentage = (int)(meanConfidence * 100),
                    };
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unhandled Exception during RecognizedImage", ex);
                return new RecognizedImage
                {
                    MeanConfidencePercentage = 0,
                    Text = string.Empty,
                };
            }
        }
    }
}
