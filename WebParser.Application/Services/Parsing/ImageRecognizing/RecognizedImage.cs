﻿namespace WebParser.Application.Services.Parsing.ImageRecognizing
{
    internal class RecognizedImage
    {
        public string Text { get; set; }

        public int MeanConfidencePercentage { get; set; }
    }
}