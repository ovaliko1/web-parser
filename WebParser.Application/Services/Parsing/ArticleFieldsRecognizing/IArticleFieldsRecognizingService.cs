﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.Parsing.ArticleFieldsRecognizing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.SiteStructure.SiteSettings;

namespace WebParser.Application.Services.Parsing.ArticleFieldsRecognizing
{
    internal interface IArticleFieldsRecognizingService
    {
        Task<RecognizedArticleFieldsData> RecognizeArticleFields(ParsedArticleResult article, Site site);
    }
}