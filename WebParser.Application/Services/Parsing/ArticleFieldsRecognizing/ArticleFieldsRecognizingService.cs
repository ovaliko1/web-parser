﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.Culture;
using WebParser.Application.Interfaces.Services.Parsing.ArticleFieldsRecognizing;
using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Mappers;
using WebParser.Application.Services.Parsing.AddressRecognizing;
using WebParser.Application.Services.Parsing.ImageProcessing;
using WebParser.Application.Services.Parsing.ImageRecognizing;
using WebParser.Application.Services.Parsing.ParsedContent;
using WebParser.Common;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.ArticleFieldsRecognizing
{
    internal class ArticleFieldsRecognizingService : IArticleFieldsRecognizingService
    {
        private IAddressRecognizingService AddressRecognizingService { get; }

        private IMetroMapService MetroMapService { get; }

        private IImageProcessingService ImageProcessingService { get; }

        private ICultureService CultureService { get; }

        public ArticleFieldsRecognizingService(
            IAddressRecognizingService addressRecognizingService,
            IMetroMapService metroMapService,
            IImageProcessingService imageProcessingService,
            ICultureService cultureService)
        {
            AddressRecognizingService = addressRecognizingService;
            MetroMapService = metroMapService;
            ImageProcessingService = imageProcessingService;
            CultureService = cultureService;
        }

        public async Task<RecognizedArticleFieldsData> RecognizeArticleFields(ParsedArticleResult article, Site site)
        {
            var result = new RecognizedArticleFieldsData();

            var articleIdentity = article
                   .ParsedValues
                   .GetArticleIdentity();

            result.ArticleIdentity = new ArticleIdentity(
                articleIdentity?.Identity.Cut(20) ?? string.Empty,
                articleIdentity?.IdentityConfidencePercentage ?? 0);

            foreach (var parsedValue in article.ParsedValues)
            {
                await FillArticleFields(result, parsedValue, site);
            }

            await TryFillAddressByLocation(article, result, site);

            var nearestMetroStation = MetroMapService.FindMetroStation(site.CityId, result.AddressInfo?.NearestMetroStation?.Name);
            if (nearestMetroStation != null)
            {
                result.MetroStations = new[] { nearestMetroStation };
            }

            return result;
        }

        private async Task TryFillAddressByLocation(ParsedArticleResult article, RecognizedArticleFieldsData result, Site site)
        {
            var location = article.ParsedValues.FirstOrDefault(x => x.ParsedKeyType == ParsedKeyType.Location);
            if (location != null)
            {
                result.AddressInfo = await AddressRecognizingService
                    .RecognizeAddressByLocation(
                        location.FormatValues(EntityMapper.DateTimeFormat).FirstOrDefault(),
                        result.AddressInfo,
                        site);
            }
        }

        private async Task FillArticleFields(RecognizedArticleFieldsData fieldsData, ParsedValue parsedValue, Site site)
        {
            switch (parsedValue.ParsedKeyType)
            {
                case ParsedKeyType.Metro:
                    fieldsData.MetroStations = MetroMapService.ExtractMetroStations(site.CityId, parsedValue);
                    break;

                case ParsedKeyType.Price:
                    var priceStr = parsedValue.FormatValues(EntityMapper.DateTimeFormat).FirstOrDefault().Cut(256);
                    var price = priceStr.ExtractPrice();

                    var culture = CultureService.GetPriceCultureByString(priceStr);

                    // TODO: Site settings to set currency symbol
                    priceStr = price.HasValue
                        ? string.Format(culture, "{0:C0}", price)
                        : priceStr ?? string.Empty;

                    fieldsData.PriceStr = priceStr;
                    fieldsData.Price = price;
                    break;

                case ParsedKeyType.Address:
                    fieldsData.AddressInfo =
                        await AddressRecognizingService.RecognizeAddress(
                                parsedValue.FormatValues(EntityMapper.DateTimeFormat).FirstOrDefault().Cut(256), site);
                    break;

                case ParsedKeyType.Images:
                    await ProcessImages(parsedValue, fieldsData, site);
                    break;

                case ParsedKeyType.Common:
                case ParsedKeyType.Phone:
                case ParsedKeyType.Object:
                case ParsedKeyType.SourceDate:
                case ParsedKeyType.Name:
                case ParsedKeyType.Description:
                case ParsedKeyType.Characteristics:
                case ParsedKeyType.Location:
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private async Task ProcessImages(ParsedValue parsedValue, RecognizedArticleFieldsData fieldsData, Site site)
        {
            var links = parsedValue.FormatValues(EntityMapper.DateTimeFormat);
            if (parsedValue.ParsedValueType != ParsedValueType.LinkToInternalStorage)
            {
                fieldsData.ImageProcessingResult = new ImageProcessingResult
                {
                    FullSizeImages = links.ToArray(x => new ImageInfo
                    {
                        ImageUrl = x,
                        FileData = null,
                        FileId = null,
                        Height = null,
                        Width = null,
                    }),
                    ImagesPreview = links.ToArray(x => new ImageInfo
                    {
                        ImageUrl = x,
                        FileData = null,
                        FileId = null,
                        Height = null,
                        Width = null,
                    }),
                };
                return;
            }

            fieldsData.ImageProcessingResult = await ImageProcessingService.ProcessImages(links, site);
        }
    }
}
