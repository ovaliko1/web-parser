﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Common;

namespace WebParser.Application.Services.Parsing.Spider.Scripts
{
    internal abstract class ScriptsService
    {
        protected abstract string ScriptTemplateName { get; }

        public string ReadTemplateScript()
        {
            var result = ReadScript(ScriptTemplateName);
            return result;
        }

        public string MapToProxyServerSection(string proxyServerAddress)
        {
            return string.IsNullOrEmpty(proxyServerAddress) ? string.Empty : $@"
  args: [
    '--proxy-server={proxyServerAddress}'
  ],
";
        }

        public string MapToProxyServerCredentialsSection(string proxyUsername, string proxyPassword)
        {
            return string.IsNullOrEmpty(proxyUsername) || string.IsNullOrEmpty(proxyPassword) ? string.Empty : $@"
  page.authenticate({{
    username: '{proxyUsername}',
    password: '{proxyPassword}'
  }});
";
        }

        public string MapToCookiesSection(Uri siteUri, Cookie[] cookies)
        {
            if (cookies == null)
            {
                return string.Empty;
            }

            var domain = siteUri.GetCookieDomain();

            var sb = new StringBuilder();
            foreach (var cookie in cookies)
            {
                if (string.IsNullOrEmpty(cookie.Name) || string.IsNullOrEmpty(cookie.Value))
                {
                    continue;
                }

                sb.Append(GetCookieSection(cookie.Name, cookie.Value, domain));
            }
            return sb.ToString();
        }

        protected abstract string GetCookieSection(string cookieName, string cookieValue, string domain);

        public string MapToScriptLinks(Uri[] links)
        {
            var result = string.Join(",\n", links.Select(x => $"'{x.AbsoluteUri}'"));
            return result;
        }

        public abstract string MapToQueryScript(SearchRule[] rules);

        protected virtual bool AddAwaitBeforePageEvaluate => false;

        protected string GetFetchActionScript(SearchRule rule)
        {
            var replaceRegexp = string.IsNullOrEmpty(rule.ReplaceRegexp)
                ? string.Empty
                : $".replace(/{rule.ReplaceRegexp}/g, '')";

            var filterFunction = string.IsNullOrEmpty(rule.FilterFunction) ? string.Empty : $".filter({rule.FilterFunction})";
            var filterFunctionForSingleNode = rule.SearchType == SearchType.SingleNode ? filterFunction.Replace("{", "{{").Replace("}", "}}") : string.Empty;

            var fetchActions = new[]
            {
                new
                {
                    Key = FoundValueType.TagContent,
                    Title = "TagContent fetch action",
                    FetchActionFormat = "(function(){{ var val = $({0})" + filterFunctionForSingleNode + ".html(); return val ? val" + replaceRegexp + ".trim()" + " : ''; }})()",
                },
                new
                {
                    Key = FoundValueType.TagTextContent,
                    Title = "TagTextContent fetch action",
                    FetchActionFormat = "(function(){{ var val = $({0})" + filterFunctionForSingleNode + ".text(); return val ? val" + replaceRegexp + ".trim()" + " : ''; }})()",
                },
                new
                {
                    Key = FoundValueType.AttributeContent,
                    Title = "AttributeContent fetch action",
                    FetchActionFormat = "(function(){{ var val = $({0})" + filterFunctionForSingleNode + ".attr('{1}'); return val ? val" + replaceRegexp + ".trim()" + " : ''; }})()",
                }
            }.ToDictionary(x => x.Key, x => x);


            var fetchAction = fetchActions[rule.FoundValue.FoundValueType];

            switch (rule.SearchType)
            {
                case SearchType.SingleNode:
                    {
                        var result = $@"
        console.log('DEBUG: {fetchAction.Title}: {rule.FieldName}' + 'ENDDEBUG');
        results[index].result[results[index].result.length] = {GetAwaitKeyword()}page.evaluate(function () {{
          return {{ name: '{rule.FieldName}', value: {string.Format(fetchAction.FetchActionFormat, $"'{rule.Selectors}'", rule.FoundValue.AttrName)} }};
        }});
";
                        return result;
                    }
                case SearchType.ManyNodes:
                    {
                        var result = $@"
        console.log('DEBUG: {fetchAction.Title}: {rule.FieldName}' + 'ENDDEBUG');
        results[index].result[results[index].result.length] = {GetAwaitKeyword()}page.evaluate(function () {{
          return {{ name: '{rule.FieldName}', values: $.map($('{rule.Selectors}'){filterFunction}, function (value, i) {{ return {string.Format(fetchAction.FetchActionFormat, "value", rule.FoundValue.AttrName)}; }}) }};
        }});
";
                        return result;
                    }
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected string GetClickActionScript(SearchRule rule)
        {
            var result = $@"
        console.log('DEBUG: Click Action: {rule.Selectors}ENDDEBUG');
        {GetAwaitKeyword()}page.evaluate(function () {{
          $('{rule.Selectors}').click();
        }});
";
            return result;
        }

        private string GetAwaitKeyword()
        {
            return AddAwaitBeforePageEvaluate ? "await " : string.Empty;
        }

        private static string ReadScript(string scriptName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = $"{assembly.GetName().Name}.Resources.{scriptName}";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                var result = reader.ReadToEnd();
                return result;
            }
        }
    }
}
