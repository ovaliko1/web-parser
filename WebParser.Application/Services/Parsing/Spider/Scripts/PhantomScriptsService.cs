﻿using System;
using System.Collections.Generic;
using System.Text;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Common;

namespace WebParser.Application.Services.Parsing.Spider.Scripts
{
    internal class PhantomScriptsService : ScriptsService
    {
        protected override string ScriptTemplateName => "phantom-script-template.js";

        protected override string GetCookieSection(string cookieName, string cookieValue, string domain) =>
                $@"
                    phantom.addCookie({{
                      'name': '{cookieName}',
                      'value': '{cookieValue}',
                      'domain': '{domain}'
                    }});";

        public override string MapToQueryScript(SearchRule[] rules)
        {
            var actions = new Dictionary<ActionType, Func<SearchRule, string>>
            {
                [ActionType.Fetch] = GetFetchActionScript,
                [ActionType.Click] = GetClickActionScript,
                [ActionType.Wait] = GetWaitActionScript,
            };

            var sb = new StringBuilder();
            var timeoutFunctionEndStack = new Stack<string>();

            foreach (var rule in rules)
            {
                var script = actions[rule.ActionType](rule);
                sb.Append(script);

                if (rule.ActionType == ActionType.Wait)
                {
                    timeoutFunctionEndStack.Push($@"
        }});}}, {rule.WaitMilseconds});
");
                }
            }

            sb.Append($@"
        results[index].processes = true;

        console.log('DEBUG: Fetching end ENDDEBUG');
        
        checkEndOfProcess(page, index);
");
            var count = timeoutFunctionEndStack.Count;
            for (var i = 0; i < count; i++)
            {
                sb.Append(timeoutFunctionEndStack.Pop());
            }

            return sb.ToString();
        }

        private static string GetWaitActionScript(SearchRule rule)
        {
            var result = $@"
        console.log('DEBUG: Wait Action: {rule.WaitMilseconds}ENDDEBUG');
        setTimeout(function () {{handleErrors(page, url, index, function() {{
";
            return result;
        }
    }
}
