﻿using System;
using System.Collections.Generic;
using System.Text;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Common;

namespace WebParser.Application.Services.Parsing.Spider.Scripts
{
    internal class PuppeterScriptsService : ScriptsService
    {
        protected override string ScriptTemplateName => "puppeter-script-template.js";

        protected override string GetCookieSection(string cookieName, string cookieValue, string domain) =>
                $@"
                    await page.setCookie({{
                      'name': '{cookieName}',
                      'value': '{cookieValue}',
                      'domain': '{domain}'
                    }});";

        protected override bool AddAwaitBeforePageEvaluate => true;

        public override string MapToQueryScript(SearchRule[] rules)
        {
            var actions = new Dictionary<ActionType, Func<SearchRule, string>>
            {
                [ActionType.Fetch] = GetFetchActionScript,
                [ActionType.Click] = GetClickActionScript,
                [ActionType.Wait] = GetWaitActionScript,
            };

            var sb = new StringBuilder();

            foreach (var rule in rules)
            {
                var script = actions[rule.ActionType](rule);
                sb.Append(script);
            }

            sb.Append($@"
        results[index].processes = true;

        console.log('DEBUG: Fetching end ENDDEBUG');
");

            return sb.ToString();
        }

        private static string GetWaitActionScript(SearchRule rule)
        {
            var result = $@"
        console.log('DEBUG: Wait Action: {rule.WaitMilseconds}ENDDEBUG');
        await sleep({rule.WaitMilseconds});
";
            return result;
        }
    }
}
