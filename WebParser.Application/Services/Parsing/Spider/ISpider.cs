﻿using WebParser.Application.Services.Parsing.SiteContent.Entities;

namespace WebParser.Application.Services.Parsing.Spider
{
    internal interface ISpider
    {
        FetchResult FetchSiteContent(FetchConfiguration config);
    }
}