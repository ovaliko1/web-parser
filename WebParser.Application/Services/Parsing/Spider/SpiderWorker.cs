﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using WebParser.Common;

namespace WebParser.Application.Services.Parsing.Spider
{
    /// <summary>
    ///     SpiderJs Wrapper
    /// </summary>
    public class SpiderWorker : IDisposable
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly List<string> errorLines = new List<string>();

        /// <summary>
        ///     Create new instance of SpiderJs Wrapper
        /// </summary>
        private SpiderWorker(string executableFileName, string tempFilesPath)
        {
            ToolPath = ResolveAppBinPath();
            ExecutableFileName = executableFileName;
            ProcessPriority = ProcessPriorityClass.Normal;
            TempFilesPath = tempFilesPath;
        }

        private Process SpiderJsProcess { get; set; }

        /// <summary>
        ///     Get or set path where executable file is located
        /// </summary>
        /// <remarks>
        ///     By default this property initialized with assembly location (app bin folder).
        /// </remarks>
        public string ToolPath { get; set; }

        /// <summary>
        ///     Get or set location for temp files (default location: <see cref="M:System.IO.Path.GetTempPath" />)
        /// </summary>
        /// <remarks>
        ///     Temp file is created in
        ///     <see cref="M:WebParser.Application.Services.Parsing.Spider.SpiderJs.RunScript(System.String,System.String[])" />
        ///     and
        ///     <see cref="M:WebParser.Application.Services.Parsing.Spider.SpiderJs.RunScriptAsync(System.String,System.String[])" />
        ///     methods
        ///     (SpiderJs can read javascript code only from file).
        /// </remarks>
        public string TempFilesPath { get; set; }

        /// <summary>
        ///     Get or set SpiderJs tool executable file name ('executable file' by default)
        /// </summary>
        public string ExecutableFileName { get; set; }

        /// <summary>
        ///     Get or set extra SpiderJs switches/options
        /// </summary>
        public string CustomArgs { get; set; }

        /// <summary>
        ///     Get or set SpiderJs process priority (Normal by default)
        /// </summary>
        public ProcessPriorityClass ProcessPriority { get; set; }

        /// <summary>
        ///     Get or set maximum execution time for running SpiderJs process (null is by default = no timeout)
        /// </summary>
        public TimeSpan? ExecutionTimeout { get; set; }

        public void Dispose()
        {
            EnsureProcessStopped();
        }

        /// <summary>
        ///     Occurs when output data is received from SpiderJs process
        /// </summary>
        public event EventHandler<DataReceivedEventArgs> OutputReceived;

        /// <summary>
        ///     Occurs when error data is received from SpiderJs process
        /// </summary>
        public event EventHandler<DataReceivedEventArgs> ErrorReceived;

        public static SpiderWorker CreatePhantomSpider(string tempFilesPath)
        {
            return new SpiderWorker(@"Resources\PhantomJs\phantomjs.exe", tempFilesPath);
        }

        public static SpiderWorker CreateSlimerSpider(string tempFilesPath)
        {
            return new SpiderWorker(@"Resources\SlimerJs\slimerjs.bat", tempFilesPath);
        }

        public static SpiderWorker CreatePuppeterSpider(string tempFilesPath, string executableFileName, string toolPath)
        {
            return new SpiderWorker(executableFileName, tempFilesPath)
            {
                ToolPath = toolPath,
            };
        }

        private string ResolveAppBinPath()
        {
            var result = AppDomain.CurrentDomain.BaseDirectory;
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            for (var i = 0; i < assemblies.Length; i++)
            {
                var assembly = assemblies[i];
                var type = assembly.GetType("System.Web.HttpRuntime", false);
                if (type != null)
                {
                    var property = type.GetProperty("AppDomainId", BindingFlags.Static | BindingFlags.Public);
                    if (!(property == null) && property.GetValue(null, null) != null)
                    {
                        var property2 = type.GetProperty("BinDirectory", BindingFlags.Static | BindingFlags.Public);
                        if (!(property2 != null))
                        {
                            break;
                        }
                        var value = property2.GetValue(null, null);
                        if (value is string)
                        {
                            result = (string)value;
                        }
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        ///     Execute javascript code from specified file.
        /// </summary>
        /// <param name="jsFile">URL or local path to javascript file to execute</param>
        /// <param name="jsArgs">arguments for javascript code (optional; can be null)</param>
        public void Run(string jsFile, string[] jsArgs)
        {
            Run(jsFile, jsArgs, null, null);
        }

        /// <summary>
        ///     Execute javascript code from specified file with input/output interaction
        /// </summary>
        /// <param name="jsFile">URL or local path to javascript file to execute</param>
        /// <param name="jsArgs">arguments for javascript code (optional; can be null)</param>
        /// <param name="inputStream">input stream for stdin data (can be null)</param>
        /// <param name="outputStream">output stream for stdout data (can be null)</param>
        public void Run(string jsFile, string[] jsArgs, Stream inputStream, Stream outputStream)
        {
            if (jsFile == null)
            {
                throw new ArgumentNullException("jsFile");
            }
            RunInternal(jsFile, jsArgs, inputStream, outputStream);
            if (SpiderJsProcess != null)
            {
                WaitProcessForExit();
                CheckExitCode(SpiderJsProcess.ExitCode, errorLines);
            }
            SpiderJsProcess?.Close();
            SpiderJsProcess = null;
        }

        /// <summary>
        ///     Execute javascript code block
        /// </summary>
        /// <param name="javascriptCode">javascript code</param>
        /// <param name="jsArgs">arguments for javascript code (optional; can be null)</param>
        public void RunScript(string javascriptCode, string[] jsArgs)
        {
            RunScript(javascriptCode, jsArgs, null, null);
        }

        /// <summary>
        ///     Execute javascript code block
        /// </summary>
        /// <param name="javascriptCode">javascript code</param>
        /// <param name="jsArgs">arguments for javascript code (optional; can be null)</param>
        /// <param name="inputStream">input stream for stdin data (can be null)</param>
        /// <param name="outputStream">output stream for stdout data (can be null)</param>
        public void RunScript(string javascriptCode, string[] jsArgs, Stream inputStream, Stream outputStream)
        {
            var path = GetTempFilePath();
            try
            {
                System.IO.File.WriteAllBytes(path, Encoding.UTF8.GetBytes(javascriptCode));
                Run(path, jsArgs, inputStream, outputStream);
            }
            finally
            {
                DeleteFileIfExists(path);
            }
        }

        private string GetTempFilePath()
        {
            var tempPath = GetTempPath();
            var text = Path.Combine(tempPath, "spiderjs-" + Path.GetRandomFileName() + ".js");
            return text;
        }

        private string GetTempPath()
        {
            if (!string.IsNullOrEmpty(TempFilesPath) && !Directory.Exists(TempFilesPath))
            {
                Directory.CreateDirectory(TempFilesPath);
            }
            return TempFilesPath ?? Path.GetTempPath();
        }

        private void DeleteFileIfExists(string filePath)
        {
            if (filePath != null && System.IO.File.Exists(filePath))
            {
                try
                {
                    System.IO.File.Delete(filePath);
                }
                catch (Exception ex)
                {
                    Log.Error("Exception during Delete Js File", ex);
                }
            }
        }

        private string PrepareCmdArg(string arg)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append('"');
            stringBuilder.Append(arg.Replace("\"", "\\\""));
            stringBuilder.Append('"');
            return stringBuilder.ToString();
        }

        private void RunInternal(string jsFile, string[] jsArgs, Stream inputStream, Stream outputStream)
        {
            errorLines.Clear();
            try
            {
                var path = Path.Combine(ToolPath, ExecutableFileName);
                if (!System.IO.File.Exists(path))
                {
                    throw new FileNotFoundException(string.Format("Cannot find {0}: {1}", ExecutableFileName, path));
                }
                var stringBuilder = new StringBuilder();
                if (!string.IsNullOrEmpty(CustomArgs))
                {
                    stringBuilder.AppendFormat(" {0} ", CustomArgs);
                }
                stringBuilder.AppendFormat(" {0}", PrepareCmdArg(jsFile));
                if (jsArgs != null)
                {
                    for (var i = 0; i < jsArgs.Length; i++)
                    {
                        var arg = jsArgs[i];
                        stringBuilder.AppendFormat(" {0}", PrepareCmdArg(arg));
                    }
                }
                var processStartInfo = new ProcessStartInfo(path, stringBuilder.ToString());
                processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                processStartInfo.CreateNoWindow = true;
                processStartInfo.UseShellExecute = false;
                processStartInfo.WorkingDirectory = Path.GetDirectoryName(ToolPath);
                processStartInfo.RedirectStandardInput = true;
                processStartInfo.RedirectStandardOutput = true;
                processStartInfo.RedirectStandardError = true;
                SpiderJsProcess = new Process();
                SpiderJsProcess.StartInfo = processStartInfo;
                SpiderJsProcess.EnableRaisingEvents = true;

                SpiderJsProcess.Exited += SpiderJsProcessExited;
                SpiderJsProcess.Start();

                if (ProcessPriority != ProcessPriorityClass.Normal)
                {
                    SpiderJsProcess.PriorityClass = ProcessPriority;
                }
                SpiderJsProcess.ErrorDataReceived += delegate (object o, DataReceivedEventArgs args)
                {
                    if (args.Data == null)
                    {
                        return;
                    }
                    errorLines.Add(args.Data);
                    ErrorReceived?.Invoke(this, args);
                };
                SpiderJsProcess.BeginErrorReadLine();
                if (outputStream == null)
                {
                    SpiderJsProcess.OutputDataReceived +=
                        delegate (object o, DataReceivedEventArgs args) { OutputReceived?.Invoke(this, args); };
                    SpiderJsProcess.BeginOutputReadLine();
                }
                if (inputStream != null)
                {
                    CopyToStdIn(inputStream);
                }
                if (outputStream != null)
                {
                    ReadStdOutToStream(SpiderJsProcess, outputStream);
                }
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message.StartsWith("StandardOut has not been redirected"))
                {
                    EnsureProcessStopped();
                }
                else
                {
                    throw;
                }
            }
        }

        public event EventHandler<EventArgs> Exited;

        private void SpiderJsProcessExited(object sender, EventArgs e)
        {
            Exited?.Invoke(sender, e);
        }

        protected void CopyToStdIn(Stream inputStream)
        {
            var array = new byte[8192];
            while (true)
            {
                var num = inputStream.Read(array, 0, array.Length);
                if (num <= 0)
                {
                    break;
                }
                SpiderJsProcess.StandardInput.BaseStream.Write(array, 0, num);
                SpiderJsProcess.StandardInput.BaseStream.Flush();
            }
            SpiderJsProcess.StandardInput.Close();
        }

        /// <summary>
        ///     Writes a string followed by a line terminator to the SpiderJs standard input (stdin).
        /// </summary>
        /// <param name="s">The string to write.</param>
        /// <remarks>This method cannot be used if standard input data specified as Stream.</remarks>
        public void WriteLine(string s)
        {
            if (SpiderJsProcess == null)
            {
                throw new InvalidOperationException("SpiderJs is not running");
            }
            SpiderJsProcess.StandardInput.WriteLine(s);
            SpiderJsProcess.StandardInput.Flush();
        }

        /// <summary>
        ///     Closes SpiderJs process standard input stream.
        /// </summary>
        /// <remarks>This method cannot be used if standard input data specified as Stream.</remarks>
        public void WriteEnd()
        {
            SpiderJsProcess.StandardInput.Close();
        }

        /// <summary>
        ///     Abort SpiderJs process (if started)
        /// </summary>
        /// <remarks>This method IMMEDIATELY stops SpiderJs by killing the process.</remarks>
        public void Abort()
        {
            EnsureProcessStopped();
        }

        private void WaitProcessForExit()
        {
            var hasValue = ExecutionTimeout.HasValue;
            if (hasValue)
            {
                SpiderJsProcess.WaitForExit((int)ExecutionTimeout.Value.TotalMilliseconds);
            }
            else
            {
                SpiderJsProcess.WaitForExit();
            }
            if (SpiderJsProcess == null)
            {
                throw new SpiderException(-1, "FFMpeg process was aborted");
            }
            if (hasValue && !SpiderJsProcess.HasExited)
            {
                EnsureProcessStopped();
                throw new SpiderException(-2, $"FFMpeg process exceeded execution timeout ({ExecutionTimeout}) and was aborted");
            }
        }

        private void EnsureProcessStopped()
        {
            if (SpiderJsProcess != null && !SpiderJsProcess.HasExited)
            {
                try
                {
                    SpiderJsProcess.Kill();
                    SpiderJsProcess.Dispose();
                    SpiderJsProcess = null;
                }
                catch (Exception ex)
                {
                    Log.Error("Exception during Spider EnsureProcessStopped", ex);
                    throw;
                }
                finally
                {
                    SpiderJsProcess?.Close();
                    SpiderJsProcess = null;
                }
            }
        }

        private void ReadStdOutToStream(Process proc, Stream outputStream)
        {
            var array = new byte[32768];
            int count;
            while ((count = proc.StandardOutput.BaseStream.Read(array, 0, array.Length)) > 0)
            {
                outputStream.Write(array, 0, count);
            }
        }

        private void CheckExitCode(int exitCode, List<string> errLines)
        {
            if (exitCode != 0)
            {
                throw new SpiderException(exitCode, string.Join("\n", errLines.ToArray()));
            }
        }
    }
}