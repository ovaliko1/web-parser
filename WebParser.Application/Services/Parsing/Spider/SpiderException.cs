﻿using System;

namespace WebParser.Application.Services.Parsing.Spider
{
    public class SpiderException : Exception
    {
        public SpiderException(int errorCode, string message) : base(message)
        {
            ErrorCode = errorCode;
        }

        public int ErrorCode { get; private set; }
    }
}