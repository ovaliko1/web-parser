﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using WebParser.Application.Mappers;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Application.Services.Parsing.SiteContent.Entities;
using WebParser.Application.Services.Parsing.Spider.Scripts;
using WebParser.Application.Services.Settings;
using WebParser.Common;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.Spider
{
    internal class Spider : ISpider
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private const string ResultMarker = "RESULT:";
        private const string EndResultMarker = "ENDRESULT";

        private const string PageErrorMarker = "PAGEERROR:";
        private const string EndPageErrorMarker = "ENDPAGEERROR";

        private const string ErrorMarker = "ERROR:";
        private const string EndErrorMarker = "ENDERROR";

        // TODO: move to site/articlesGroup settings
        private const int TimeOutMilliseconds = 400;
        private const string DefaultUserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";

        private IParsingProgressErrorsHandler ParsingProgressErrorsHandler { get; }

        private ISettings Settings { get; }

        public Spider(
            IParsingProgressErrorsHandler parsingProgressErrorsHandler,
            ISettings settings)
        {
            ParsingProgressErrorsHandler = parsingProgressErrorsHandler;
            Settings = settings;
        }

        public FetchResult FetchSiteContent(FetchConfiguration config)
        {
            Uri siteUri;
            if (!Uri.TryCreate(config.Site.Link, UriKind.Absolute, out siteUri))
            {
                return new FetchResult
                {
                    Items = new FetchResultItem[0],
                };
            }

            var scriptService = CreateScriptsService(config.SpiderType);
            var scriptFormat = scriptService.ReadTemplateScript();
            var queryScript = scriptService.MapToQueryScript(config.Rules);
            var cookiesSection = scriptService.MapToCookiesSection(siteUri, config.Site.Cookies);
            var scriptLinks = scriptService.MapToScriptLinks(config.Links);
            var proxyServerSection = scriptService.MapToProxyServerSection(config.Site.ProxyAddress);
            var proxyServerCredentialsSection = scriptService.MapToProxyServerCredentialsSection(config.Site.ProxyUsername, config.Site.ProxyPassword);

            var script = scriptFormat
                .Replace("{links}", scriptLinks)
                .Replace("{script}", queryScript)
                .Replace("{cookiesSection}", cookiesSection)
                .Replace("{timeout}", TimeOutMilliseconds.ToString())
                .Replace("{loadImages}", config.SpiderDoNotLoadImages ? "false" : "true")
                .Replace("{userAgent}", string.IsNullOrEmpty(config.Site.UserAgent) ? DefaultUserAgent : config.Site.UserAgent)
                .Replace("{proxyServerAddress}", proxyServerSection)
                .Replace("{proxyServerCredentials}", proxyServerCredentialsSection);

            var resultSb = new StringBuilder();
            string outputString = string.Empty;
            SpiderWorker spiderWorker = null;
            try
            {
                using (spiderWorker = CreateSpider(config.SpiderType, Settings.GetTempFilesPath()))
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    if (config.SpiderType != SpiderType.Puppeter)
                    {
                        var customArgs = "";
                        if (!string.IsNullOrEmpty(config.Site.ProxyAddress))
                        {
                            customArgs += $" --proxy={config.Site.ProxyAddress}" +
                                (!string.IsNullOrEmpty(config.Site.ProxyUsername) &&
                                 !string.IsNullOrEmpty(config.Site.ProxyPassword)
                                    ? $" --proxy-auth={config.Site.ProxyUsername}:{config.Site.ProxyPassword}"
                                    : string.Empty);
                        }

                        spiderWorker.CustomArgs = customArgs;
                    }

                    // 120 seconds per link
                    var maxExecutionMillseconds = config.Links.Length * 2 * 60 * 1000;

                    CancellationTokenSource source = new CancellationTokenSource();
                    ShutdownSpider(x =>
                    {
                        x?.Abort();
                    },
                    spiderWorker,
                    source,
                    maxExecutionMillseconds,
                    maxExecutionMillseconds / config.Links.Length / 2 / 60);

                    try
                    {
                        spiderWorker.RunScript(script, null, null, memoryStream);
                    }
                    catch (Exception ex)
                    {
                        LogSpiderException(config, script, memoryStream, ex);
                    }

                    source.Cancel();

                    outputString = GetOutput(memoryStream);

                    ProcessOutputReceived(config.Site.Id, config.Site.UserId, outputString, siteUri);

                    var extractResult = ExtractFromString(outputString, ResultMarker, EndResultMarker).FirstOrDefault();

                    resultSb.Append(extractResult);
                }
            }
            catch (Exception ex)
            {
                LogSpiderException(config, script, null, ex);
            }
            finally
            {
                spiderWorker?.Abort();
            }

            var result = MapToFetchResult(resultSb.ToString());
            return new FetchResult
            {
                Items = result,
                SpiderOutput = $"{script}\n\n{outputString}",
            };
        }

        private void LogSpiderException(FetchConfiguration config, string script, MemoryStream memoryStream, Exception ex)
        {
            var output = memoryStream == null ? "output is not received" : GetOutput(memoryStream);
            Log.Error(
$@"Exception during Spider Js execution.
SiteId: {config.Site.Id} SiteName: {config.Site.Name}
SpiderType: {config.SpiderType}
Script:
{script}
Output:
{output}
", ex);
        }

        private SpiderWorker CreateSpider(SpiderType spiderType, string tempFilesPath)
        {
            switch (spiderType)
            {
                case SpiderType.PhantomJs:
                    return SpiderWorker.CreatePhantomSpider(tempFilesPath);

                case SpiderType.SlimerJs:
                    return SpiderWorker.CreateSlimerSpider(tempFilesPath);

                case SpiderType.Puppeter:
                    return SpiderWorker.CreatePuppeterSpider(tempFilesPath, "puppeter.bat", Settings.GetPuppeterToolPath());

                default:
                    throw new ArgumentOutOfRangeException(nameof(spiderType), spiderType, null);
            }
        }

        private static ScriptsService CreateScriptsService(SpiderType spiderType)
        {
            switch (spiderType)
            {
                case SpiderType.PhantomJs:
                case SpiderType.SlimerJs:
                    return new PhantomScriptsService();

                case SpiderType.Puppeter:
                    return new PuppeterScriptsService();

                default:
                    throw new ArgumentOutOfRangeException(nameof(spiderType), spiderType, null);
            }
        }

        private string GetOutput(MemoryStream memoryStream)
        {
            // TODO: Move to site/articleGroup Settings
            Encoding encoding = Encoding.UTF8;

            var resultBytes = memoryStream.ToArray();
            return encoding.GetString(resultBytes);
        }

        private static FetchResultItem[] MapToFetchResult(string fetchedString)
        {
            if (string.IsNullOrEmpty(fetchedString))
            {
                return new FetchResultItem[0];
            }

            var result = fetchedString.Deserialize<FetchResultItem[]>().Where(x => x != null).ToArray(x => new FetchResultItem
            {
                Url = x.Url,
                Result = x.Result.Where(y => y != null).ToArray(),
            });
            return result;
        }

        private static void ShutdownSpider(Action<SpiderWorker> action, SpiderWorker spider, CancellationTokenSource source, int timeoutInMilliseconds, int waitStep)
        {
            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < timeoutInMilliseconds; i += waitStep)
                {
                    Thread.Sleep(waitStep);
                    if (source.IsCancellationRequested)
                    {
                        return;
                    }
                }

                action(spider);
            }, source.Token);
        }

        private void ProcessOutputReceived(int siteId, int userId, string outputString, Uri siteUri)
        {
            var pageErrors = ExtractFromString(outputString, PageErrorMarker, EndPageErrorMarker);
            var errors = ExtractFromString(outputString, ErrorMarker, EndErrorMarker);

            foreach (var pageError in pageErrors)
            {
                Log.Info($"Page Error receivied during Spider Js evaluation. Site Url: {siteUri}. Message: {pageError}");
            }

            foreach (var error in errors)
            {
                Log.Info($"Error receivied during Spider Js evaluation. Site Url: {siteUri}. Message: {error}");
            }

#if DEBUG
            var debugMessages = ExtractFromString(outputString, "DEBUG:", "ENDDEBUG");
            foreach (var message in debugMessages)
            {
                Debug.WriteLine(message);
            }
#endif
        }

        private static IEnumerable<string> ExtractFromString(string outputString, string startMarker, string endMarker)
        {
            int indexStart = 0;
            int indexEnd = 0;

            for (;;)
            {
                indexStart = outputString.IndexOf(startMarker, StringComparison.InvariantCulture);
                indexEnd = outputString.IndexOf(endMarker, StringComparison.InvariantCulture);
                if (indexStart != -1 && indexEnd != -1)
                {
                    var matched = outputString.Substring(indexStart + startMarker.Length, indexEnd - indexStart - startMarker.Length);

                    yield return matched;

                    outputString = outputString.Substring(indexEnd + endMarker.Length);
                }
                else
                {
                    yield break;
                }
            }
        }
    }
}
