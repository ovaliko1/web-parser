﻿using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.SiteStructure;
using WebParser.Application.Services.SiteStructure.SiteSettings;

namespace WebParser.Application.Services.Parsing
{
    internal class ParseArticleSettings
    {
        private ParseArticleSettings() { }

        public static ParseArticleSettings Preview(Site site, ArticlesGroup articlesGroup)
        {
            return new ParseArticleSettings
            {
                Site = site,
                ArticlesGroup = articlesGroup,
                DownloadFiles = false,
                UploadToWebHook = false,
                IsPreview = true,
            };
        }

        public static ParseArticleSettings RealParse(Site site, ArticlesGroup articlesGroup)
        {
            return new ParseArticleSettings
            {
                Site = site,
                ArticlesGroup = articlesGroup,
                DownloadFiles = true,
                UploadToWebHook = true,
            };
        }

        public static ParseArticleSettings UploadToThirdParty(Site site, ArticlesGroup articlesGroup)
        {
            return new ParseArticleSettings
            {
                Site = site,
                ArticlesGroup = articlesGroup,
                DownloadFiles = false,
                UploadToWebHook = true,
            };
        }

        public Site Site { get; private set; }

        public ArticlesGroup ArticlesGroup { get; private set; }

        public bool UploadToWebHook { get; private set; }

        public bool DownloadFiles { get; private set; }

        public bool IsPreview { get; private set; }
    }
}
