﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;

namespace WebParser.Application.Services.Parsing.ImageProcessing
{
    internal interface IImageProcessingService
    {
        Task<ImageProcessingResult> ProcessImages(string[] links, Site site);
    }
}