﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.Settings;
using WebParser.Application.Services.WebClient;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.ImageProcessing
{
    internal class ImageProcessingService : IImageProcessingService
    {
        private class ImageData
        {
            public string SourceLink { get; set; }

            public FileDetails FileDetails { get; set; }

            public int? Width { get; set; }

            public int? Height { get; set; }
        }

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IWebCommand WebCommand { get; }

        private IFileService FileService { get; }

        private ISettings Settings { get; }

        public ImageProcessingService(
            IWebCommand webCommand,
            IFileService fileService,
            ISettings settings)
        {
            WebCommand = webCommand;
            FileService = fileService;
            Settings = settings;
        }

        public async Task<ImageProcessingResult> ProcessImages(string[] links, Site site)
        {
            var images = await GetImageData(site, links);
            var previewImages = GetPreviewImages(site, images);

            return new ImageProcessingResult(
                await ToImagesInfo(site, images),
                await ToImagesInfo(site, previewImages));
        }

        private Dictionary<int, ImageData> GetPreviewImages(Site site, Dictionary<int, ImageData> images)
        {
            var key = 0;
            var previewImages = new Dictionary<int, ImageData>(images.Count);
            foreach (var imageData in images)
            {
                if (!TryResizeImage(imageData.Value, out FileDetails fileDetails))
                {
                    continue;
                }

                previewImages.Add(key++, GetImageData(imageData.Value.SourceLink, site, fileDetails));
            }

            return previewImages;
        }

        private async Task<Dictionary<int, ImageData>> GetImageData(Site site, string[] links)
        {
            var images = new Dictionary<int, ImageData>(links.Length);
            int key = 0;
            foreach (var link in links)
            {
                var fileDetail = await WebCommand.DownloadFile(site.Id, site.UserId, site.Link, link, site.Cookies);
                if (fileDetail == null)
                {
                    continue;
                }

                if (!CropImage(link, site, fileDetail))
                {
                    continue;
                }

                images.Add(key++, GetImageData(link, site, fileDetail));
            }

            return images;
        }

        private bool TryResizeImage(ImageData imageData, out FileDetails fileDetails)
        {
            if (imageData.FileDetails.FileData.Length == 0)
            {
                fileDetails = null;
                return false;
            }

            fileDetails = new FileDetails
            {
                ContentType = imageData.FileDetails.ContentType,
                FileExtension = imageData.FileDetails.FileExtension,
                FileName = imageData.FileDetails.FileName,
            };

            using (var stream = new MemoryStream(imageData.FileDetails.FileData))
            using (var image = (Bitmap)Image.FromStream(stream))
            using (var scaledImage = ScaleImage(image, Settings.GetPreviewImageMaxWidth(), Settings.GetPreviewImageMaxHeight()))
            {
                fileDetails.FileData = ImageToBytes(scaledImage);
            }

            return true;
        }

        private static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
            {
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return newImage;
        }

        private static bool CropImage(string link, Site site, FileDetails fileDetail)
        {
            try
            {
                if (site.CropImageTop == 0 &&
                    site.CropImageBottom == 0 &&
                    site.CropImageLeft == 0 &&
                    site.CropImageRight == 0)
                {
                    return true;
                }

                using (var stream = new MemoryStream(fileDetail.FileData))
                using (var source = (Bitmap) Image.FromStream(stream))
                {
                    int x = site.CropImageLeft;
                    int y = site.CropImageTop;
                    int width = source.Width - site.CropImageLeft - site.CropImageRight;
                    int height = source.Height - site.CropImageBottom - site.CropImageTop;

                    Rectangle rectangle = new Rectangle(x, y, width, height);

                    using (var cropped = source.Clone(rectangle, PixelFormat.DontCare))
                    {
                        fileDetail.FileData = ImageToBytes(cropped);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.Error($"Unhandled Exception during Crop image: {link} site: {site.Name} fileDetail.bytelength: {fileDetail.FileData?.Length}", ex);
                return false;
            }
        }

        private async Task<ImageInfo[]> ToImagesInfo(Site site, Dictionary<int, ImageData> imageData)
        {
            var images = await FileService.CreateTemporaryFiles(site.UserId, site.Id, imageData.ToArray(x => new KeyValuePair<int, FileDetails>(x.Key, x.Value.FileDetails)));
            var result = images.ToArray(x => new ImageInfo
            {
                FileId = x.Value.Id,
                FileData = x.Value.FileData,
                ImageUrl = x.Value.FileUrl,
                Height = imageData[x.Key].Height,
                Width = imageData[x.Key].Width,
            });
            return result;
        }

        private ImageData GetImageData(string link, Site site, FileDetails fileDetail)
        {
            try
            {
                using (var stream = new MemoryStream(fileDetail.FileData))
                using (var source = (Bitmap)Image.FromStream(stream))
                {
                    return new ImageData
                    {
                        SourceLink = link,
                        FileDetails = fileDetail,
                        Height = source.Height,
                        Width = source.Width,
                    };
                }
            }
            catch (Exception ex)
            {
                Log.Error($"Unhandled Exception during Get Image Data: {link} site: {site.Name} fileDetail.bytelength: {fileDetail.FileData?.Length}", ex);
                return new ImageData
                {
                    SourceLink = link,
                    FileDetails = fileDetail,
                    Height = null,
                    Width = null,
                };
            }
        }

        private static byte[] ImageToBytes(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
    }
}
