﻿using System.Threading.Tasks;
using WebParser.Application.Services.Parsing.SiteContent.Entities;

namespace WebParser.Application.Services.Parsing.SiteContent
{
    internal interface IFileDownloader
    {
        Task DownloadFiles(FetchResultItem[] fetchedItems, FetchConfiguration config);
    }
}