﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Services.Parsing.SiteContent.Entities;
using WebParser.Application.Services.WebClient;
using WebParser.Common;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.SiteContent
{
    internal class FileDownloader : IFileDownloader
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IWebCommand WebCommand { get; }

        private IFileService FileService { get; }

        public FileDownloader(
            IWebCommand webCommand,
            IFileService fileService)
        {
            WebCommand = webCommand;
            FileService = fileService;
        }

        public async Task DownloadFiles(FetchResultItem[] fetchedItems, FetchConfiguration config)
        {
            // TODO: Rules should be unique by fieldName
            var rulesDict = config.Rules
                .Where(
                    x => x.ActionType == ActionType.Fetch && 
                    x.IsImage &&
                   !x.DoNotSaveToInternalStorage)
                .ToDictionary(x => x.FieldName, x => x);

            int currentId = 0;

            var allItems = fetchedItems.SelectMany(x => x.Result).ToArray();
            var filesToDownload = allItems
                .Where(x => rulesDict.ContainsKey(x.Name))
                .Where(x => rulesDict[x.Name].ParsedKeyTypeId != (int)ParsedKeyType.Images) // See ArticleFieldsRecognizingService to see image processing
                .ToArray(x => new FileToDownload
                {
                    Key = ++currentId,
                    SearchType = rulesDict[x.Name].SearchType,
                    Item = x,
                });

            var downloadedFiles = await DownloadFiles(config, filesToDownload);
            FillDownloadedResult(config.Site.Id, config.Site.UserId, downloadedFiles, filesToDownload);
        }

        private async Task<KeyValuePair<int, CreatedFile>[]> DownloadFiles(FetchConfiguration config, FileToDownload[] files)
        {
            var linksToDownloads = files
                .SelectMany(
                    x => x.SearchType == SearchType.SingleNode ?
                        Utilities.CreateArray(new KeyValuePair<int, string>(x.Key, x.Item.Value)) :
                        x.Item.Values.ToArray(y => new KeyValuePair<int, string>(x.Key, y)))
                .ToArray();

            var fileDetails = new List<KeyValuePair<int, FileDetails>>(linksToDownloads.Length);
            foreach (var linksToDownload in linksToDownloads)
            {
                var fileDetail = await WebCommand.DownloadFile(config.Site.Id, config.Site.UserId, config.Site.Link, linksToDownload.Value, config.Site.Cookies);
                if (fileDetail != null)
                {
                    fileDetails.Add(new KeyValuePair<int, FileDetails>(linksToDownload.Key, fileDetail));
                }

                if (config.DownloadFileDelayInMillseconds> 0)
                {
                    await Task.Delay(config.DownloadFileDelayInMillseconds);
                }
            }

            var result = await FileService.CreateTemporaryFiles(config.Site.UserId, config.Site.Id, fileDetails.ToArray());
            return result;
        }

        private static void FillDownloadedResult(int siteId, int userId, KeyValuePair<int, CreatedFile>[] downloadedFiles, FileToDownload[] filesToDownload)
        {
            var filesByKey = downloadedFiles.GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.ToArray(y => y.Value));

            foreach (var file in filesToDownload)
            {
                if (!filesByKey.ContainsKey(file.Key))
                {
                    continue;
                }

                var createdFiles = filesByKey[file.Key];

                file.Item.IsLinkToInternalStorage = true;
                file.Item.CreatedFiles = createdFiles;

                switch (file.SearchType)
                {
                    case SearchType.SingleNode:
                        if (createdFiles.Length != 1)
                        {
                            Log.Error($"fileUrls with Length {createdFiles.Length} for SearchType.SingleNode. siteId {siteId}, userId {userId}. Name: {file.Item.Name}, value: {file.Item.Value}");
                            continue;
                        }

                        file.Item.Value = createdFiles[0].FileUrl;
                        break;

                    case SearchType.ManyNodes:
                        file.Item.Values = createdFiles.ToArray(x => x.FileUrl);
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
