﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Services.Parsing.SiteContent.Entities;

namespace WebParser.Application.Services.Parsing.SiteContent
{
    internal interface ISiteContentService
    {
        ParsedContentResult<ParsedArticlesGroup> FetchArticlesGroupContent(FetchConfiguration config);

        Task<ParsedContentResult<ParsedArticleResult>> FetchArticlesContent(FetchConfiguration config);
    }
}