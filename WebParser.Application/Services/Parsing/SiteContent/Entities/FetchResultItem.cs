﻿namespace WebParser.Application.Services.Parsing.SiteContent.Entities
{
    internal class FetchResultItem
    {
        public string Url { get; set; }

        public FetchFieldValue[] Result { get; set; }
    }
}