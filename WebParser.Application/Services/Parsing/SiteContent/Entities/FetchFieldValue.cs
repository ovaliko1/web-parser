﻿using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Services.File;

namespace WebParser.Application.Services.Parsing.SiteContent.Entities
{
    internal class FetchFieldValue
    {
        public string Name { get; set; }

        // TODO: Escape from this field. Use Values instead
        public string Value { get; set; }

        public string[] Values { get; set; }

        public bool IsLinkToInternalStorage { get; set; }

        public CreatedFile[] CreatedFiles { get; set; }
    }
}