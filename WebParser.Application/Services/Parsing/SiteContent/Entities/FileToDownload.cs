﻿using WebParser.Common;

namespace WebParser.Application.Services.Parsing.SiteContent.Entities
{
    internal class FileToDownload
    {
        public int Key { get; set; }

        public SearchType SearchType { get; set; }

        public FetchFieldValue Item { get; set; }

        public FoundValueType FoundValueType { get; set; }
    }
}