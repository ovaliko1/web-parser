﻿namespace WebParser.Application.Services.Parsing.SiteContent.Entities
{
    internal class FetchResult
    {
        public FetchResultItem[] Items { get; set; }

        public string SpiderOutput { get; set; }
    }
}