﻿using System;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.SiteStructure.SiteSettings;
using WebParser.Common;

namespace WebParser.Application.Services.Parsing.SiteContent.Entities
{
    internal class FetchConfiguration
    {
        public Uri[] Links { get; set; }

        public SearchRule[] Rules { get; set; }

        public bool IsPreview { get; set; }

        public SpiderType SpiderType { get; set; }

        public int DownloadFileDelayInMillseconds { get; set; }

        public bool SpiderDoNotLoadImages { get; set; }

        public bool DownloadFiles => !IsPreview;

        public Site Site { get; set; }
    }
}