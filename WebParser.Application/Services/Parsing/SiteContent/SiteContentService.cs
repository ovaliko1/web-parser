﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using log4net;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Services.Parsing.ArticleFieldsRecognizing;
using WebParser.Application.Services.Parsing.ImageRecognizing;
using WebParser.Application.Services.Parsing.SiteContent.Entities;
using WebParser.Application.Services.Parsing.Spider;
using WebParser.Common;
using WebParser.Common.Extensions;

namespace WebParser.Application.Services.Parsing.SiteContent
{
    // TODO: Think about Builder Patter (https://en.wikipedia.org/wiki/Builder_pattern) to construct ParsedArticle
    // Move 
    internal class SiteContentService : ISiteContentService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ISpider Spider { get; }

        private IFileDownloader FileDownloader { get; }

        private IImageRecognizingService ImageRecognizingService { get; }

        private IArticleFieldsRecognizingService ArticleFieldsRecognizingService { get; }

        public SiteContentService(
            ISpider spider,
            IFileDownloader fileDownloader,
            IImageRecognizingService imageRecognizingService,
            IArticleFieldsRecognizingService articleFieldsRecognizingService)
        {
            Spider = spider;
            FileDownloader = fileDownloader;
            ImageRecognizingService = imageRecognizingService;
            ArticleFieldsRecognizingService = articleFieldsRecognizingService;
        }

        public ParsedContentResult<ParsedArticlesGroup> FetchArticlesGroupContent(FetchConfiguration config)
        {
            var fetchResult = Spider.FetchSiteContent(config);
            if (fetchResult.Items.Length == 0)
            {
                return new ParsedContentResult<ParsedArticlesGroup>
                {
                    Result = new ParsedArticlesGroup[0],
                    SpiderOutput = fetchResult.SpiderOutput,
                };
            }

            var result = FillParseModel<ParsedArticlesGroup>(config.Site.Link, fetchResult.Items, config.Rules);

            return new ParsedContentResult<ParsedArticlesGroup>
            {
                Result = result,
                SpiderOutput = fetchResult.SpiderOutput,
            };
        }

        public async Task<ParsedContentResult<ParsedArticleResult>> FetchArticlesContent(FetchConfiguration config)
        {
            var fetchResult = Spider.FetchSiteContent(config);
            if (fetchResult.Items.Length == 0)
            {
                return new ParsedContentResult<ParsedArticleResult>
                {
                    Result = new ParsedArticleResult[0],
                    SpiderOutput = fetchResult.SpiderOutput,
                };
            }

            if (config.DownloadFiles)
            {
                await FileDownloader.DownloadFiles(fetchResult.Items, config);
            }

            var result = FillParseModel<ParsedArticleResult>(config.Site.Link, fetchResult.Items, config.Rules);

            foreach (var article in result)
            {
                article.RecognizedFieldsData = await ArticleFieldsRecognizingService.RecognizeArticleFields(article, config.Site);
            }

            return new ParsedContentResult<ParsedArticleResult>
            {
                Result = result,
                SpiderOutput = fetchResult.SpiderOutput,
            };
        }

        private T[] FillParseModel<T>(string baseUrl, FetchResultItem[] fetchResultItemItem, SearchRule[] rules) where T : ParsedResult
        {
            var result = fetchResultItemItem.ToArray(x => FillParseModel<T>(baseUrl, x, rules));
            return result;
        }

        private T FillParseModel<T>(string baseUrl, FetchResultItem fetchResultItemItem, SearchRule[] rules) where T : ParsedResult
        {
            var parsedValues = new List<ParsedValue>();
            var rulesDictionary = rules.Where(x => x.ActionType == ActionType.Fetch).ToDictionary(x => x.FieldName, x => x);

            foreach (var item in fetchResultItemItem.Result)
            {
                var value = CreateParsedValue(baseUrl, item, rulesDictionary[item.Name]);
                parsedValues.Add(value);
            }

            var result = (T)Activator.CreateInstance(typeof(T), new object[] { parsedValues.ToArray() });
            result.Link = fetchResultItemItem.Url;
            return result;
        }

        private ParsedValue CreateParsedValue(string baseUrl, FetchFieldValue fetchFieldValue, SearchRule rule)
        {
            ParsedValue result;
            switch (rule.SearchType)
            {
                case SearchType.SingleNode:
                    if (rule.IsImage)
                    {
                        var value = UrlHelper.ConstructFullUri(baseUrl, fetchFieldValue.Value)?.ToString() ?? fetchFieldValue.Value;
                        result = ParsedValue.CreateLinkToFile(DecodeValue(value, rule.FoundValue.FoundValueType), !rule.DoNotSaveToInternalStorage);
                    }
                    else if (rule.IsDateTime)
                    {
                        result = ParseDateTime(fetchFieldValue.Value, rule);
                    }
                    else
                    {
                        result = ParsedValue.Create(DecodeValue(fetchFieldValue.Value, rule.FoundValue.FoundValueType));
                    }
                    break;

                case SearchType.ManyNodes:
                    if (rule.IsImage)
                    {
                        var values = fetchFieldValue.Values.ToArray(x => UrlHelper.ConstructFullUri(baseUrl, x)?.ToString() ?? x);
                        result = ParsedValue.CreateLinkToFile(values, !rule.DoNotSaveToInternalStorage);
                    }
                    else if (rule.IsDateTime)
                    {
                        result = ParseDateTime(fetchFieldValue.Values, rule);
                    }
                    else
                    {
                        result = ParsedValue.Create(DecodeValues(fetchFieldValue.Values, rule.FoundValue.FoundValueType));
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            result.Name = rule.FieldName;
            result.BackgroundColor = rule.BackgroundColor;
            result.ShowOnVoteForm = rule.ShowOnVoteForm;
            result.HideOnVkPost = rule.HideOnVkPost;

            result.ParsedKeyType = (ParsedKeyType)rule.ParsedKeyTypeId;

            if (result.ParsedKeyType == ParsedKeyType.Phone)
            {
                FillRecognizedPhones(result, fetchFieldValue, rule);
            }

            return result;
        }

        private void FillRecognizedPhones(ParsedValue result, FetchFieldValue fetchFieldValue, SearchRule rule)
        {
            if (rule.IsImage)
            {
                if (fetchFieldValue.CreatedFiles?.Length > 0)
                {
                    result.RecognizedPhones = ImageRecognizingService
                        .RecognizedImages(fetchFieldValue.CreatedFiles)
                        .ToArray(x => new RecognizedPhone
                        {
                            MeanConfidencePercentage = x.MeanConfidencePercentage,
                            RecognizedText = x.Text,
                            FixedText = x.Text.ExtractPhoneNumber(),
                        });
                }
            }
            else
            {
                result.RecognizedPhones = result.Values.ToArray(x => new RecognizedPhone
                {
                    RecognizedText = x,
                    FixedText = x.ExtractPhoneNumber(),
                    MeanConfidencePercentage = 100,
                });
            }
        }

        private static string[] DecodeValues(string[] values, FoundValueType foundValueType)
        {
            var result = values.ToArray(x => DecodeValueCore(x, foundValueType));
            return result;
        }

        private static string DecodeValue(string value, FoundValueType foundValueType)
        {
            var result = DecodeValueCore(value, foundValueType);
            return result;
        }

        private static string DecodeValueCore(string value, FoundValueType foundValueType)
        {
            switch (foundValueType)
            {
                case FoundValueType.TagTextContent:
                    return HttpUtility.HtmlDecode(value);

                default:
                    return value;
            }
        }

        private static ParsedValue ParseDateTime(string dateTime, SearchRule item)
        {
            System.DateTime date;
            return TryParseDateTimeCore(dateTime, item, out date) ?
                ParsedValue.Create(date) :
                ParsedValue.Create(dateTime, item.BackgroundColor);
        }

        private static ParsedValue ParseDateTime(string[] dateTimes, SearchRule item)
        {
            var result = new System.DateTime[dateTimes.Length];
            for (int i = 0; i < dateTimes.Length; i++)
            {
                var dateTime = dateTimes[i];
                System.DateTime parsedDate;
                if (TryParseDateTimeCore(dateTime, item, out parsedDate))
                {
                    result[i] = parsedDate;
                }
                else
                {
                    return ParsedValue.Create(dateTimes);
                }
            }

            return ParsedValue.Create(result);
        }

        private static bool TryParseDateTimeCore(string dateTime, SearchRule item, out System.DateTime result)
        {
            return System.DateTime.TryParseExact(dateTime?.Trim(), item.DateTimeParsingRule.DateFormat, item.DateTimeParsingRule.CultureInfo, DateTimeStyles.None, out result);
        }
    }
}
