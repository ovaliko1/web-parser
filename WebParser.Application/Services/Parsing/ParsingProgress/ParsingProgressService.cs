﻿using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsingProgress;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Common;
using WebParser.Common.Mappers;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Parsing.ParsingProgress
{
    internal class ParsingProgressService : IParsingProgressService
    {
        private IRepository Repository { get; }

        private IDateTimeService DateTimeService { get; }

        private IUserService UserService { get; }

        public ParsingProgressService(
            IRepository repository,
            IDateTimeService dateTimeService,
            IUserService userService)
        {
            Repository = repository;
            DateTimeService = dateTimeService;
            UserService = userService;
        }

        public PagingResult<LogEntry> GetParsingLog<T>(RequestFilters<T> filters)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                IQueryable<DbParsingLog> query = context.Set<DbParsingLog>();

                if (!currentUser.Roles.HasRole(Role.Admin))
                {
                    query = query.Where(x => x.UserId == currentUser.Id);
                }

                var articles = query
                    .Select(x => new LogEntry
                    {
                        Id = x.Id,
                        SiteId = x.SiteId,
                        UserId = x.UserId,
                        Date = x.Date,
                        Status = x.Status,
                        Message = x.Message,
                    })
                    .ApplyFilters(filters);
                return articles;
            }
        }
    }
}
