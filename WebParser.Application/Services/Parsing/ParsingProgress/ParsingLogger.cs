﻿using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Common;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Parsing.ParsingProgress
{
    internal class ParsingLogger : IParsingLogger
    {
        private IRepository Repository { get; }

        private IDateTimeService DateTimeService { get; }

        public ParsingLogger(
            IRepository repository,
            IDateTimeService dateTimeService)
        {
            Repository = repository;
            DateTimeService = dateTimeService;
        }

        public void Log(int? siteId, int userId, ParsingLogStatus status, string message)
        {
            using (var context = Repository.GetContext())
            {
                context.Set<DbParsingLog>()
                    .Add(new DbParsingLog
                    {
                        Date = DateTimeService.GetCurrentDateTimeUtc(),
                        SiteId = siteId,
                        UserId = userId,
                        Status = (int)status,
                        Message = message,
                    });
                context.ValidateAndSave();
            }
        }
    }
}
