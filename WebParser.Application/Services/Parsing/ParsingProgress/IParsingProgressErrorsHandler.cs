﻿using System;
using System.Threading.Tasks;

namespace WebParser.Application.Services.Parsing.ParsingProgress
{
    internal interface IParsingProgressErrorsHandler
    {
        Task HandleErrors(Func<Task> action, int siteId, int userId, string errorMessage);

        Task<T> HandleErrors<T>(Func<Task<T>> action, int siteId, int userId, string errorMessage);

        void LogParsingProgressError(int siteId, int userId, string error);

        void LogParsingProgressError(int siteId, int userId, string error, Exception ex);
    }
}
