﻿using WebParser.Common;

namespace WebParser.Application.Services.Parsing.ParsingProgress
{
    internal interface IParsingLogger
    {
        void Log(int? siteId, int userId, ParsingLogStatus status, string message);
    }
}