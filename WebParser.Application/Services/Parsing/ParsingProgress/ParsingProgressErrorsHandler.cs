﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using WebParser.Common;

namespace WebParser.Application.Services.Parsing.ParsingProgress
{
    internal class ParsingProgressErrorsHandler : IParsingProgressErrorsHandler
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IParsingLogger ParsingLogger { get; }

        public ParsingProgressErrorsHandler(IParsingLogger parsingLogger)
        {
            ParsingLogger = parsingLogger;
        }

        public async Task HandleErrors(Func<Task> action, int siteId, int userId, string errorMessage)
        {
            await HandleErrors<object>(
            async () =>
            {
                await action();
                return null;
            },
            siteId,
            userId,
            errorMessage);
        }

        public async Task<T> HandleErrors<T>(Func<Task<T>> action, int siteId, int userId, string errorMessage)
        {
            try
            {
                if (action != null)
                {
                    return await action();
                }
            }
            catch (Exception ex)
            {
                LogParsingProgressError(siteId, userId, errorMessage, ex);
            }
            return default(T);
        }

        public void LogParsingProgressError(int siteId, int userId, string error)
        {
            var message = $"ParsingProgressErrorsHandler: {error}";
            Log.Warn(message);

            ParsingLogger.Log(siteId, userId, ParsingLogStatus.Fail, message);
        }

        public void LogParsingProgressError(int siteId, int userId, string error, Exception ex)
        {
            var message = $"ParsingProgressErrorsHandler: {error}";
            Log.Warn(message, ex);

            ParsingLogger.Log(siteId, userId, ParsingLogStatus.Fail, message);
        }
    }
}
