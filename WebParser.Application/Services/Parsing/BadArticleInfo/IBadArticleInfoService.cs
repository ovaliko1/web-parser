﻿using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.DataAccess;

namespace WebParser.Application.Services.Parsing.BadArticleInfo
{
    internal interface IBadArticleInfoService
    {
        BadArticleInfo GetBadArticleInfo(DataBaseContext context, int userId, int siteId, ArticleIdentity articleIdentity, string formattedAddress);
    }
}