﻿using System;

namespace WebParser.Application.Services.Parsing.BadArticleInfo
{
    internal class BadArticleInfo
    {
        public Guid? DuplicateGroupId { get; set; }

        public int? BlackListItemId { get; set; }
    }
}