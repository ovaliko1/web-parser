﻿using System;
using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Services.BlackList;
using WebParser.Application.Services.Repository;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Parsing.BadArticleInfo
{
    internal class BadArticleInfoService : IBadArticleInfoService
    {
        private const int DuplicationCheckDaysBefore = -15;
        private const int BlackListCheckDaysBefore = -31;
        private const int MinArticleIdentityConsiderableLength = 7;

        private IRepositoryService RepositoryService { get; }

        private IBlackListService BlackListService { get; }

        private IDateTimeService DateTimeService { get; }

        public BadArticleInfoService(
            IRepositoryService repositoryService,
            IDateTimeService dateTimeService,
            IBlackListService blackListService)
        {
            RepositoryService = repositoryService;
            DateTimeService = dateTimeService;
            BlackListService = blackListService;
        }

        public BadArticleInfo GetBadArticleInfo(DataBaseContext context, int userId, int siteId, ArticleIdentity articleIdentity, string formattedAddress)
        {
            var result = new BadArticleInfo();

            if (articleIdentity == null ||
                articleIdentity.IdentityConfidencePercentage == 0 ||
                string.IsNullOrEmpty(articleIdentity.Identity) ||
                articleIdentity.Identity.Length < MinArticleIdentityConsiderableLength)
            {
                return result;
            }

            var blackListItemId = BlackListService.GetBlackListItemId(context, articleIdentity, siteId);

            var now = DateTimeService.GetCurrentDateTimeUtc();

            var after = now.AddDays(DuplicationCheckDaysBefore);

            var previousDuplicate = RepositoryService.GetPreviousDuplicate(context, articleIdentity, after, siteId);
            if (previousDuplicate == null)
            {
                result.BlackListItemId = blackListItemId;
                return result;
            }

            result.DuplicateGroupId = previousDuplicate.DuplicateGroupId ?? Guid.NewGuid();

            if (blackListItemId != null)
            {
                result.BlackListItemId = blackListItemId;

                MarkDuplicate(previousDuplicate, result);
                context.ValidateAndSave();
                
                return result;
            }

            var afterForBlackList = now.AddDays(BlackListCheckDaysBefore);
            var duplicatesByAddress = RepositoryService.GetPreviousDuplicates(context, articleIdentity, afterForBlackList, siteId)
                .GroupBy(x => x.FormattedAddress, StringComparer.InvariantCultureIgnoreCase)// GeoCoords? How? Precision?
                .ToArray();

            // Already 3 articles by month with same Identity with different addresses => add to black list
            if (duplicatesByAddress.Length > 2 ||
               (duplicatesByAddress.Length == 2 && duplicatesByAddress.All(x => !x.Key.CompareIgnoreCase(formattedAddress))))
            {
                blackListItemId = BlackListService.AddToBlackList(context, articleIdentity, userId, siteId);
                result.BlackListItemId = blackListItemId;
            }

            foreach (var duplicate in duplicatesByAddress.SelectMany(x => x).Concat(previousDuplicate))
            {
                MarkDuplicate(duplicate, result);
            }

            context.ValidateAndSave();

            return result;
        }

        private static void MarkDuplicate(DbParsedArticle duplicate, BadArticleInfo result)
        {
            duplicate.IsDuplicate = true;
            duplicate.BlackListItemId = result.BlackListItemId;
            duplicate.DuplicateGroupId = result.DuplicateGroupId;
        }
    }
}
