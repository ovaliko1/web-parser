﻿using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Common;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.UserManagement
{
    internal interface IInternalUserService
    {
        bool UserCanNotVote(User user);

        bool UserCanNotVote(DbUser user);

        bool UserCanNotVote(DbUser user, RateType? rateType);

        bool MaxFreeBonusItemsCountReached(DbUser user, RateType rateType);

        RateType? GetUserRateType(DbUser user);

        RateType? GetUserRateType(User user);
    }
}
