﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.ArticleRating;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.Settings;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Application.Mappers;
using WebParser.Application.Services.BlackList;
using WebParser.Application.Services.Settings;
using WebParser.Application.Services.UserManagement.JsonEntities;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;
using WebParser.DataAccess.Helpers;
using WebParser.DataAccess.Identity;
using Constants = WebParser.Common.Constants;

namespace WebParser.Application.Services.UserManagement
{
    internal class UserService : IUserService, IInternalUserService
    {
        private class ArticleGroupInfo
        {
            public int Id { get; set; }

            public int UserId { get; set; }
        }

        // internal for tests
        internal class ExtendAccessDetails
        {
            public Guid PublicUserId { get; set; }

            public int OpeningsToAdd { get; set; }

            public int DaysToAdd { get; set; }
        }

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly string VkIdentityProvider = "Vkontakte";

        private const char Plus = '+';
        private const char Space = ' ';

        private const string NewUserLetter = @"<h4>Добро пожаловать в Pullmeout!</h4>
<p>Для активации аккаунта кликните <a href='{0}'>здесь</a>.</p>
<p>Аккаунт должен быть активирован в течение 24 часов после получения этого письма.</p>";

        private const string ResetPasswordLetter = @"<h4>Pullmeout: Восстановление пароля</h4>
<p>Для восстановления пароля кликните <a href='{0}'>здесь</a>.</p>
<p>Ссылка доступна в течение 24 часов после получения этого письма.</p>";

        private const string WelcomeSubject = "Добро пожаловать в Pullmeout!";
        private const string ResetPasswordSubject = "Pullmeout: Восстановление пароля";

        private IRepository Repository { get; }

        private IDateTimeService DateTimeService { get; }

        private ISettings Settings { get; }

        private ICommonSettings CommonSettings { get; }

        private IBlackListService BlackListService { get; }

        public UserService(
            IRepository repository,
            IDateTimeService dateTimeService,
            ISettings settings,
            ICommonSettings commonSettings,
            IBlackListService blackListService)
        {
            Repository = repository;
            DateTimeService = dateTimeService;
            Settings = settings;
            CommonSettings = commonSettings;
            BlackListService = blackListService;
        }

        // TODO: Cache in request getting user details
        public int? GetCurrentUserId()
        {
            ClaimsPrincipal claimsPrincipal;
            return GetUserId(out claimsPrincipal);
        }

        private int? GetUserId(out ClaimsPrincipal userPrincipal)
        {
            userPrincipal = AuthManager.User;
            if (userPrincipal == null)
            {
                return null;
            }

            var identity = (ClaimsIdentity)userPrincipal.Identity;
            var userId = identity.GetUserId<int>();
            var result = userId == default(int) ? (int?)null : userId;
            return result;
        }

        // TODO: Cache in request getting user details
        public User GetCurrentUser()
        {
            ClaimsPrincipal userPrincipal;
            var userId = GetUserId(out userPrincipal);
            if (userId == null)
            {
                return null;
            }

            using (var context = Repository.GetContext())
            {
                var user = context.Set<DbUser>()
                    .Where(x => x.Id == userId)
                    .Select(x => new User
                    {
                        Id = x.Id,
                        VkId = x.VkId,
                        Email = x.Email,
                        UserName = x.UserName,
                        MaxSiteCount = x.MaxSiteCount,
                        CreatedDate = x.CreatedDate,
                        AccessDateTime = x.AccessDateTime,
                        OpeningBalance = x.OpeningBalance,
                        EmailConfirmed = x.EmailConfirmed,
                        FreeHours = x.FreeHours,
                        FreeOpenings = x.FreeOpenings,
                        PurchasedAccount = x.PurchasedAccount,
                        ClickerCautionCount = x.ClickerCautionCount,
                        WaitingForPayment = x.WaitingForPayment,
                        PublicId = x.PublicId,
                    })
                    .FirstOrDefault();

                if (user == null)
                {
                    return null;
                }

                var roles = userPrincipal
                        .Claims
                        .Where(x => x.Type == ((ClaimsIdentity)userPrincipal.Identity).RoleClaimType)
                        .ToArray(x => x.Value.ToRole());

                user.Roles = roles;
                return user;
            }
        }

        public bool UserAccessExpired(User user)
        {
            return !user.Roles.HasRole(Role.Admin) && !GetUserRateType(user).HasValue;
        }

        public RateType? GetUserRateType(User user)
        {
            return GetRateType(user.OpeningBalance, user.AccessDateTime);
        }

        public RateType? GetUserRateType(DbUser user)
        {
            return GetRateType(user.OpeningBalance, user.AccessDateTime);
        }

        private RateType? GetRateType(int openingBalance, DateTime accessDateTime)
        {
            return openingBalance > 0
                ? RateType.Openings
                : accessDateTime > DateTimeService.GetCurrentDateTimeUtc() ? RateType.Days : (RateType?)null;
        }

        public bool UserCanNotVote(User user)
        {
            return UserCanNotVote(GetUserRateType(user), user.ClickerCautionCount, user.FreeHours, user.FreeOpenings, user.PurchasedAccount);
        }

        public bool UserCanNotVote(DbUser user)
        {
            return UserCanNotVote(user, GetUserRateType(user));
        }

        public bool UserCanNotVote(DbUser user, RateType? rateType)
        {
            return UserCanNotVote(rateType, user.ClickerCautionCount, user.FreeHours, user.FreeOpenings, user.PurchasedAccount);
        }

        public bool MaxFreeBonusItemsCountReached(DbUser user, RateType rateType)
        {
            return MaxFreeDaysCountReached(rateType, user.FreeHours, user.FreeOpenings, user.PurchasedAccount);
        }

        private static bool UserCanNotVote(RateType? rateType, int clickerCautionCount, int freeHours, int freeOpenings, bool purchasedAccount)
        {
            return
                !rateType.HasValue ||
                clickerCautionCount >= ArticleRatingConstants.MaxClickerCautionCount ||
                MaxFreeDaysCountReached(rateType.Value, freeHours, freeOpenings, purchasedAccount);
        }

        private static bool MaxFreeDaysCountReached(RateType rateType, int freeHours, int freeOpenings, bool purchasedAccount)
        {
            switch (rateType)
            {
                case RateType.Openings:
                    return freeOpenings >= ArticleRatingConstants.MaxFreeOpenings;

                case RateType.Days:
                    return freeHours >= (purchasedAccount ?
                               ArticleRatingConstants.MaxFreeHoursForPurchasedAccount :
                               ArticleRatingConstants.MaxFreeHoursForTrialAccount);

                default:
                    throw new ArgumentOutOfRangeException(nameof(rateType), rateType, null);
            }
        }

        public void UpdateWaitingForPaymentStatus()
        {
            var userId = GetCurrentUserId();
            if (userId == null)
            {
                return;
            }

            using (var context = Repository.GetContext())
            using (var transaction = context.BeginTransaction())
            {
                var user = context.Set<DbUser>().FirstOrDefault(x => x.Id == userId && !x.PurchasedAccount && !x.WaitingForPayment);
                if (user == null)
                {
                    return;
                }

                user.WaitingForPayment = true;

                context.ValidateAndSave();
                transaction.Commit();
            }
        }

        public void TryAcceptYandexPaymentConfirmation(YandexPaymentConfirmation paymentConfirmation)
        {
            try
            {
                var jsonConfirmation = paymentConfirmation.Map<YandexPaymentConfirmation, JsonPaymentConfirmation>();

                var paymentAmout = paymentConfirmation.WithdrawAmount.ToDecimal();

                ExtendAccessDetails extendAccessDetails;
                string errorMessage;
                if (!TryGetExtendAccessDetails(paymentConfirmation.Label, paymentAmout, out extendAccessDetails, out errorMessage))
                {
                    Log.Error($"TryAcceptYandexPaymentConfirmation. Failed during received data validation: {errorMessage}\n" +
                              $"Received Data: {jsonConfirmation.Serialize()}");
                    return;
                }

                Log.Info($"TryAcceptYandexPaymentConfirmation. Payment data: {jsonConfirmation.Serialize()}");

                using (var context = Repository.GetContext())
                using (var transaction = context.BeginTransaction())
                {
                    var user = context.Set<DbUser>().FirstOrDefault(x => x.PublicId == extendAccessDetails.PublicUserId);
                    if (user == null)
                    {
                        Log.Error($"TryAcceptYandexPaymentConfirmation. User for payment not found Data: {jsonConfirmation.Serialize()}");
                        return;
                    }

                    if (!jsonConfirmation.Unaccepted)
                    {
                        var now = DateTimeService.GetCurrentDateTimeUtc();
                        var fromDaysToAddDate = now > user.AccessDateTime ? now : user.AccessDateTime;

                        var newAccessDateTime = extendAccessDetails.DaysToAdd > 0 ? fromDaysToAddDate.AddDays(extendAccessDetails.DaysToAdd) : user.AccessDateTime;
                        var newOpeningBalance = user.OpeningBalance + extendAccessDetails.OpeningsToAdd;

                        jsonConfirmation.AccessDateBefore = user.AccessDateTime.ToStringInvariant(JsonEntityMapper.LongDateTimeFormat);
                        jsonConfirmation.AccessDateAfter = newAccessDateTime.ToStringInvariant(JsonEntityMapper.LongDateTimeFormat);
                        jsonConfirmation.OpeningBalanceBefore = user.OpeningBalance;
                        jsonConfirmation.OpeningBalanceAfter = newOpeningBalance;

                        user.AccessDateTime = newAccessDateTime;
                        user.OpeningBalance = newOpeningBalance;

                        user.WaitingForPayment = false;
                        user.PurchasedAccount = true;
                    }
                    else
                    {
                        Log.Error($"TryAcceptYandexPaymentConfirmation. Payment received with Unaccepted == true. \nPosible resons:" +
                                  $"Codepro is: {jsonConfirmation.CodePro}\n" +
                                  $"Need to empty purse. See https://money.yandex.ru/doc.xml?id=526991\n" +
                                  $"Received Data: {jsonConfirmation.Serialize()}");
                    }

                    SavePaymentConfirmation(user, jsonConfirmation);

                    context.ValidateAndSave();
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                Log.Error($"TryAcceptYandexPaymentConfirmation unhandled exception. Data: {paymentConfirmation.Serialize()}", ex);
            }
        }

        // internal for tests
        internal bool TryGetExtendAccessDetails(string paymentConfirmationLabel, decimal paymentAmount, out ExtendAccessDetails extendAccessDetails, out string errorDetails)
        {
            errorDetails = string.Empty;
            extendAccessDetails = new ExtendAccessDetails();

            var paymentDetails = paymentConfirmationLabel.Split('_');
            if (paymentDetails.Length != 4)
            {
                errorDetails = $"Wrong paymentConfirmationLabel: {paymentConfirmationLabel}";
                return false;
            }

            Guid publicUserId;
            if (!Guid.TryParse(paymentDetails[0], out publicUserId))
            {
                errorDetails = $"Wrong publicUserId in label: {paymentDetails[0]}";
                return false;
            }

            extendAccessDetails.PublicUserId = publicUserId;

            int intRateType;
            if (!int.TryParse(paymentDetails[1], out intRateType))
            {
                errorDetails = $"Wrong rateType in label: {paymentDetails[1]}";
                return false;
            }

            int price;
            if (!int.TryParse(paymentDetails[2], out price) || price <= 0)
            {
                errorDetails = $"Wrong price in label: {paymentDetails[2]}";
                return false;
            }

            int itemsCount;
            if (!int.TryParse(paymentDetails[3], out itemsCount) || itemsCount <= 0)
            {
                errorDetails = $"Wrong itemsCount in label: {paymentDetails[3]}";
                return false;
            }

            int actualAmount = (int)Math.Floor(paymentAmount);
            int expectedAmount;
            int daysToAdd = 0;
            int openingsToAdd = 0;

            RateType rateType = (RateType)intRateType;
            switch (rateType)
            {
                case RateType.Openings:
                    if (itemsCount == CommonSettings.GetFirstRateOpeningsCount())
                    {
                        expectedAmount = CommonSettings.GetFirstRateOpeningsPrice();
                    }
                    else if (itemsCount == CommonSettings.GetSecondRateOpeningsCount())
                    {
                        expectedAmount = CommonSettings.GetSecondRateOpeningsPrice();
                    }
                    else if (itemsCount == CommonSettings.GetThirdRateOpeningsCount())
                    {
                        expectedAmount = CommonSettings.GetThirdRateOpeningsPrice();
                    }
                    else if (itemsCount == CommonSettings.GetFourthRateOpeningsCount())
                    {
                        expectedAmount = CommonSettings.GetFourthRateOpeningsPrice();
                    }
                    else
                    {
                        errorDetails = $"Wrong itemsCount in label. No suitable RateOpeningsCount found: {itemsCount}";
                        return false;
                    }
                    openingsToAdd = itemsCount;
                    break;

                case RateType.Days:
                    if (itemsCount == CommonSettings.GetFirstRateDaysCount())
                    {
                        expectedAmount = CommonSettings.GetFirstRateDaysPrice();
                    }
                    else if (itemsCount == CommonSettings.GetSecondRateDaysCount())
                    {
                        expectedAmount = CommonSettings.GetSecondRateDaysPrice();
                    }
                    else if (itemsCount == CommonSettings.GetThirdRateDaysCount())
                    {
                        expectedAmount = CommonSettings.GetThirdRateDaysPrice();
                    }
                    else if (itemsCount == CommonSettings.GetFourthRateDaysCount())
                    {
                        expectedAmount = CommonSettings.GetFourthRateDaysPrice();
                    }
                    else
                    {
                        errorDetails = $"Wrong itemsCount in label. No suitable RateDaysCount found: {itemsCount}";
                        return false;
                    }
                    daysToAdd = itemsCount;
                    break;

                default:
                    errorDetails = $"Wrong rateType in label: {rateType}";
                    return false;
            }

            if (expectedAmount != actualAmount)
            {
                errorDetails = $"expectedAmount({expectedAmount}) not matched to actualAmount({actualAmount})";
                return false;
            }

            extendAccessDetails.OpeningsToAdd = openingsToAdd;
            extendAccessDetails.DaysToAdd = daysToAdd;
            return true;
        }

        private static void SavePaymentConfirmation(DbUser user, JsonPaymentConfirmation jsonConfirmation)
        {
            var paymentConfirmations = user.SerializedPaymentConfirmations.Deserialize<List<JsonPaymentConfirmation>>();
            paymentConfirmations = paymentConfirmations ?? new List<JsonPaymentConfirmation>();

            paymentConfirmations.Add(jsonConfirmation);

            user.SerializedPaymentConfirmations = paymentConfirmations.Serialize();
        }

        public UserVoteInfo GetCurrentUserVoteInfo()
        {
            var userPrincipal = AuthManager.User;
            if (userPrincipal == null)
            {
                return null;
            }

            var identity = (ClaimsIdentity)userPrincipal.Identity;
            var userId = identity.GetUserId<int>();

            int blackListItemsCount = BlackListService.GetItemsCount();
            using (var context = Repository.GetContext())
            {
                var user = context.Set<DbUser>()
                    .Where(x => x.Id == userId)
                    .Select(x => new User
                    {
                        Id = x.Id,
                        Email = x.Email,
                        UserName = x.UserName,
                        AccessDateTime = x.AccessDateTime,
                        OpeningBalance = x.OpeningBalance,
                        FreeHours = x.FreeHours,
                        FreeOpenings = x.FreeOpenings,
                        PurchasedAccount = x.PurchasedAccount,
                    })
                    .FirstOrDefault();

                if (user == null)
                {
                    return null;
                }

                var votesCount = context
                    .Set<DbArticleRatingVote>()
                    .Where(x => x.UserId == user.Id)
                    .GroupBy(x => x.ArticleRatingTypeId)
                    .Select(x => new
                    {
                        RatingTypeId = x.Key,
                        Count = x.Count(),
                    })
                    .Execute();

                var result = new UserVoteInfo
                {
                    UserName = user.UserName,
                    AccessDateTime = user.AccessDateTime,
                    OpeningBalance = user.OpeningBalance,
                    GoodVotes = votesCount.FirstOrDefault(x => x.RatingTypeId == (int)ArticleRatingType.Good)?.Count ?? 0,
                    BadVotes = votesCount.FirstOrDefault(x => x.RatingTypeId == (int)ArticleRatingType.Bad)?.Count ?? 0,
                    OutdateVotes = votesCount.FirstOrDefault(x => x.RatingTypeId == (int)ArticleRatingType.Outdate)?.Count ?? 0,
                    FreeDays = user.FreeHours,
                    FreeOpenings = user.FreeOpenings,
                    MaxFreeDaysCount = user.PurchasedAccount ? ArticleRatingConstants.MaxFreeHoursForPurchasedAccount : ArticleRatingConstants.MaxFreeHoursForTrialAccount,
                    MaxFreeOpeningsCount = ArticleRatingConstants.MaxFreeOpenings,
                    BlackListItemsCount = blackListItemsCount,
                    RateType = GetUserRateType(user),
                };
                return result;
            }
        }

        public void SetSeenForbidden(int userId)
        {
            using (var context = Repository.GetContext())
            {
                var user = context.Set<DbUser>().FirstOrDefault(x => x.Id == userId);
                if (user != null)
                {
                    user.SeenForbidden = true;
                    user.PurchasedAccount = false;
                    context.ValidateAndSave();
                }
            }
        }

        public async Task<string[]> Login(string name, string password, bool rememberMe)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(password))
            {
                return new[] { "Логин и пароль - обязательные поля" };
            }

            return await ExecuteByUserManager(async x =>
            {
                var user = await x.FindAsync(name, password);
                if (user == null)
                {
                    return new[] { "Неверно введенный логин или пароль" };
                }
                if (!user.EmailConfirmed)
                {
                    return new[] { "Email не подтверждён. Пожалуйста перейдите по ссылке, которую Вам прислали на почту." };
                }

                await SignInUser(x, user, rememberMe);

                return new string[0];
            });
        }

        private async Task SignInUser(AppUserManager userManager, DbUser user, bool rememberMe)
        {
            ClaimsIdentity identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthManager.SignOut();
            AuthManager.SignIn(new AuthenticationProperties {IsPersistent = rememberMe}, identity);
        }

        public async Task<int> CreateUser(UserDetails user, string callbackUrlFormat)
        {
            var dbUser = new DbUser
            {
                UserName = user.Email,
                Email = user.Email,
                EmailConfirmed = true,
                MaxSiteCount = user.MaxSiteCount,
                NameForViewers = user.NameForViewers.ToNullIfEmpty(),
                CityId = user.CityId,
                AccessDateTime = user.AccessDateTime,
            };
            return await CreateUser(callbackUrlFormat, dbUser, Role.User);
        }

        public async Task<int> CreatePublisher(PublisherDetails publisher, string callbackUrlFormat)
        {
            var dbPublisher = GetPublisherSource(publisher);
            return await CreateUser(callbackUrlFormat, dbPublisher, Role.Publisher);
        }

        private DbUser GetPublisherSource(PublisherDetails publisher)
        {
            using (var context = Repository.GetContext())
            {
                var articleGroup = SelectArticleGroupInfo(context, publisher);

                return new DbUser
                {
                    UserName = publisher.Email,
                    Email = publisher.Email,
                    EmailConfirmed = true,
                    AccessDateTime = publisher.AccessDateTime,
                    PublisherArticleGroupId = articleGroup.Id,
                    PublisherArticleGroupUserId = articleGroup.UserId,
                };
            }
        }

        private async Task<int> CreateUser(string callbackUrlFormat, DbUser dbUser, Role role)
        {
            return await ExecuteByUserManager(async x =>
            {
                var result = await CreateUser(x, dbUser);

                if (!result.Succeeded)
                {
                    throw new EditUserException(result.Errors);
                }

                var newUser = await x.FindByEmailAsync(dbUser.Email);
                await x.AddToRoleAsync(newUser.Id, role.GetRoleName());
                await SendEmail(x, newUser, WelcomeSubject, NewUserLetter, callbackUrlFormat);

                return newUser.Id;
            });
        }

        public async Task<string[]> CreateViewer(AccountInfo accountInfo, string callbackUrlFormat)
        {
            if (RegistrationIpAlreadyUsed(accountInfo.Ip))
            {
                return new [] { GetBadIpErrorMessage() };
            }

            var now = DateTimeService.GetCurrentDateTimeUtc();
            var dbUser = new DbUser
            {
                UserName = accountInfo.Email,
                Email = accountInfo.Email,
                AccessDateTime = now.AddMinutes(Settings.GetTrialAccessInMinutes()),
                RegistrationIp = accountInfo.Ip,
            };

            return await ExecuteByUserManager(async x =>
            {
                var result = await CreateUser(x, dbUser, now, accountInfo.Password);

                if (!result.Succeeded)
                {
                    return result.Errors.ToArray();
                }

                var newUser = await x.FindByEmailAsync(accountInfo.Email);
                await x.AddToRoleAsync(newUser.Id, Role.Viewer.GetRoleName());

                var resetToken = await x.GenerateEmailConfirmationTokenAsync(newUser.Id);
                await SendEmail(resetToken, x, newUser, WelcomeSubject, NewUserLetter, callbackUrlFormat);

                return new string[0];
            });
        }

        public async Task<bool> ConfirmEmail(int userId, string code)
        {
            return await ExecuteByUserManager(async x =>
            {
                IdentityResult result;
                try
                {
                    result = await x.ConfirmEmailAsync(userId, code);
                }
                catch (InvalidOperationException ex)
                {
                    Log.Info("ConfirmEmail failed", ex);
                    return false;
                }

                if (result.Succeeded)
                {
                    var user = await x.FindByIdAsync(userId);
                    await SignInUser(x, user, false);
                    return true;
                }
                return false;
            });
        }

        public async Task<string[]> ResetPassword(string email, string code, string password)
        {
            return await ExecuteByUserManager(async x =>
            {
                var user = await x.FindByEmailAsync(email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist
                    return new string[0];
                }

                var result = await x.ResetPasswordAsync(user.Id, FixUrlEncoding(code), password);
                if (result.Succeeded)
                {
                    // Automatic sign in after password has been reset.
                    await ExecuteBySignInManager(async y => await y.SignInAsync(user, false, false));
                    return new string[0];
                }

                return result.Errors.ToArray();
            });  
        }

        public async Task ForgotPassword(string email, string callbackUrlFormat)
        {
            await ExecuteByUserManager(async x =>
            {
                var user = await x.FindByEmailAsync(email);
                if (user == null || !(await x.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist
                    return;
                }

                await SendEmail(x, user, ResetPasswordSubject, ResetPasswordLetter, callbackUrlFormat);
            });
        }

        public async Task ChangePassword(string oldPassword, string newPassword)
        {
            var currentUser = GetCurrentUser();
            var result = await ExecuteByUserManager(x => x.ChangePasswordAsync(currentUser.Id, oldPassword, newPassword));
            if (!result.Succeeded)
            {
                throw new ChangePasswordException(result.Errors);
            }
        }

        private static string FixUrlEncoding(string code)
        {
            return code.Replace(Space, Plus);
        }

        public void SignOut()
        {
            AuthManager.SignOut();
        }

        private async Task SendEmail(AppUserManager userManager, DbUser user, string subject, string body, string callbackUrlFormat)
        {
            var resetToken = await userManager.GeneratePasswordResetTokenAsync(user.Id);
            await SendEmail(resetToken, userManager, user, subject, body, callbackUrlFormat);
        }

        private static async Task SendEmail(string resetToken, AppUserManager userManager, DbUser user, string subject, string body, string callbackUrlFormat)
        {
            var callbackUrl = string.Format(callbackUrlFormat, resetToken, user.Id);
            body = string.Format(body, callbackUrl);
            await userManager.SendEmailAsync(user.Id, subject, body);
        }

        public PagingResult<UserDetails> ReadUsers<T>(RequestFilters<T> filters)
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbUser>()
                                    .Include(x => x.Roles)
                                    .Where(x => x.Roles.Any(y => y.RoleId == (int)Role.User))
                                    .ApplyFilters(filters, EntityMapper.Map<DbUser, UserDetails>);
                return result;
            }
        }

        public PagingResult<ViewerDetails> ReadViewers<T>(RequestFilters<T> filters)
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbUser>()
                                    .Where(x => x.Roles.Any(y => y.RoleId == (int)Role.Viewer))
                                    .ApplyFilters(filters, EntityMapper.Map<DbUser, ViewerDetails>);
                return result;
            }
        }

        public PagingResult<PublisherDetails> ReadPublishers<T>(RequestFilters<T> filters)
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbUser>()
                                    .Where(x => x.Roles.Any(y => y.RoleId == (int)Role.Publisher))
                                    .Select(x => new
                                    {
                                        x.Id,
                                        x.AccessDateTime,
                                        x.CreatedDate,
                                        x.Email,
                                        ArticleGroupId = x.PublisherArticlesGroup.Id,
                                        ArticleGroupName = x.PublisherArticlesGroup.InternalName,
                                        SiteId = x.PublisherArticlesGroup.SiteId,
                                        SiteName = x.PublisherArticlesGroup.Site.Name,
                                    })
                                    .ApplyFilters(filters, x => new PublisherDetails
                                    {
                                        Id = x.Id,
                                        AccessDateTime = x.AccessDateTime,
                                        CreatedDate = x.CreatedDate,
                                        Email = x.Email,
                                        ArticleGroup = new Item(x.ArticleGroupId, x.ArticleGroupName),
                                        Site = new Item(x.SiteId, x.SiteName),
                                    });
                return result;
            }
        }

        public PublisherDetails GetPublisherById(int id)
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbUser>()
                    .Where(x => x.Id == id && x.Roles.Any(y => y.RoleId == (int) Role.Publisher))
                    .Select(x => new
                    {
                        x.Id,
                        x.AccessDateTime,
                        x.Email,
                        ArticleGroupId = x.PublisherArticlesGroup.Id,
                        ArticleGroupName = x.PublisherArticlesGroup.InternalName,
                        SiteId = x.PublisherArticlesGroup.SiteId,
                        SiteName = x.PublisherArticlesGroup.Site.Name,
                    })
                    .Execute()
                    .ToArray(x => new PublisherDetails
                    {
                        Id = x.Id,
                        AccessDateTime = x.AccessDateTime,
                        Email = x.Email,
                        ArticleGroup = new Item(x.ArticleGroupId, x.ArticleGroupName),
                        Site = new Item(x.SiteId, x.SiteName),
                    })
                    .FirstOrDefault();
                return result;
            }
        }

        public UserItem[] GetAllUsers()
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbUser>()
                    .Where(x => x.Roles.Select(y => y.RoleId).Contains((int)Role.User))
                    .ExecuteAndConvert(x => new UserItem
                    {
                        Id = x.Id,
                        Email = x.Email,
                    });
                return result;
            }
        }

        public void EditUser(UserDetails user)
        {
            using (var context = Repository.GetContext())
            {
                var dbUser = context.Set<DbUser>().FirstOrDefault(x => x.Id == user.Id);
                if (dbUser == null)
                {
                    Log.Warn($"Try to change User with Id: {user.Id}");
                    return;
                }
                dbUser.MaxSiteCount = user.MaxSiteCount;
                dbUser.NameForViewers = user.NameForViewers.ToNullIfEmpty();
                dbUser.AccessDateTime = user.AccessDateTime.ConvertDateTimeToUtc(Constants.DefaultTimeZoneId);
                dbUser.CityId = user.CityId;

                context.ValidateAndSave();
            }
        }

        public void EditViewer(ViewerDetails viewer)
        {
            using (var context = Repository.GetContext())
            {
                var dbViewer = context.Set<DbUser>().FirstOrDefault(x => x.Id == viewer.Id);
                if (dbViewer == null)
                {
                    Log.Warn($"Try to change Viewer with Id: {viewer.Id}");
                    return;
                }

                dbViewer.AccessDateTime = viewer.AccessDateTime.ConvertDateTimeToUtc(Constants.DefaultTimeZoneId);
                dbViewer.OpeningBalance = viewer.OpeningBalance;
                dbViewer.PurchasedAccount = viewer.PurchasedAccount;
                dbViewer.WaitingForPayment = viewer.WaitingForPayment;

                context.ValidateAndSave();
            }
        }

        public void EditPublisher(PublisherDetails publisher)
        {
            using (var context = Repository.GetContext())
            {
                var dbViewer = context.Set<DbUser>().FirstOrDefault(x => x.Id == publisher.Id);
                if (dbViewer == null)
                {
                    Log.Warn($"Try to change Publisher with Id: {publisher.Id}");
                    return;
                }

                var articleGroup = SelectArticleGroupInfo(context, publisher);

                dbViewer.AccessDateTime = publisher.AccessDateTime.ConvertDateTimeToUtc(Constants.DefaultTimeZoneId);
                dbViewer.PublisherArticleGroupId = articleGroup.Id;
                dbViewer.PublisherArticleGroupUserId = articleGroup.UserId;

                context.ValidateAndSave();
            }
        }

        private static ArticleGroupInfo SelectArticleGroupInfo(DataBaseContext context, PublisherDetails publisher)
        {
            var articleGroup = context
                .Set<DbArticlesGroup>()
                .Where(x => x.Id == publisher.ArticleGroup.Id)
                .Select(x => new ArticleGroupInfo
                {
                    Id = x.Id,
                    UserId = x.UserId,
                })
                .First();
            return articleGroup;
        }

        public ExternalLoginDetails GetExternalLoginInfo()
        {
            var externalLoginProvider = AuthManager.GetExternalAuthenticationTypes().FirstOrDefault();
            return externalLoginProvider == null ? null : new ExternalLoginDetails
            {
                AuthenticationType = externalLoginProvider.AuthenticationType,
                Caption = externalLoginProvider.Caption,
            };
        }

        public async Task<ExternalLoginResult> ExternalLogin(string ip)
        {
            var loginInfo = await AuthManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                Log.Error($"Null loginInfo during ExternalLogin. Ip: {ip}");
                return ExternalLoginResult.Succeed();
            }

            return await ExecuteBySignInManager(async signInManager =>
            {
                var signInStatus = await signInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
                if (signInStatus != SignInStatus.Success)
                {
                    return await CreateUserByExternal(signInManager, loginInfo, ip);
                }
                return ExternalLoginResult.Succeed();
            });
        }

        public void TryExtendUserAccessForSharing()
        {
            var currentUserId = GetCurrentUserId();
            if (!currentUserId.HasValue)
            {
                return;
            }

            using (var context = Repository.GetContext())
            {
                var dbUser = context.Set<DbUser>().FirstOrDefault(x => x.Id == currentUserId);
                if (dbUser == null)
                {
                    return;
                }

                if (!dbUser.SharingMade)
                {
                    var now = DateTimeService.GetCurrentDateTimeUtc();
                    var from = now > dbUser.AccessDateTime ? now : dbUser.AccessDateTime;

                    dbUser.AccessDateTime = from.AddHours(Settings.GetHoursForSharing());
                    dbUser.SharingMade = true;
                    context.ValidateAndSave();
                }
            }
        }

        public Site GetPublisherSite(int publisherId)
        {
            using (var context = Repository.GetContext())
            {
                var publisherSite = context.Set<DbUser>()
                    .Where(x => x.Id == publisherId)
                    .Select(x => x.PublisherArticlesGroup.Site)
                    .FirstOrDefault();
                return publisherSite.Map<DbSite, Site>();
            }
        }

        private async Task<ExternalLoginResult> CreateUserByExternal(AppSignInManager signInManager, ExternalLoginInfo loginInfo, string ip)
        {
            if (RegistrationIpAlreadyUsed(ip))
            {
                Log.Info($"IP_USED_FOR_NEW_USER ip: {ip}; LoginProvider: {loginInfo.Login.LoginProvider}; ProviderId: {loginInfo.Login.ProviderKey}; DefaultUserName: {loginInfo.DefaultUserName}");
                return ExternalLoginResult.Failed(GetBadIpErrorMessage());
            }

            long? vkId;
            if (loginInfo.Login.LoginProvider == VkIdentityProvider)
            {
                vkId = loginInfo.Login.ProviderKey.ToLong();
            }
            else
            {
                throw new ArgumentOutOfRangeException("loginInfo.Login.LoginProvider");
            }

            var now = DateTimeService.GetCurrentDateTimeUtc();
            var newUser = new DbUser
            {
                UserName = loginInfo.DefaultUserName,
                Email = $"{Guid.NewGuid()}@vkontakte.ru",
                VkId = vkId,
                RegistrationIp = ip,
                AccessDateTime = now.AddMinutes(Settings.GetTrialAccessInMinutes()),
                EmailConfirmed = true,
            };

            return await ExecuteByUserManager(async x =>
            {
                var userCreationResult = await CreateUser(x, newUser, now);
                if (!userCreationResult.Succeeded)
                {
                    return ExternalLoginResult.Failed(userCreationResult.Errors.ToArray());
                }

                newUser = await x.FindByEmailAsync(newUser.Email);

                userCreationResult = await x.AddToRoleAsync(newUser.Id, Role.Viewer.GetRoleName());
                if (!userCreationResult.Succeeded)
                {
                    return ExternalLoginResult.Failed(userCreationResult.Errors.ToArray());
                }

                userCreationResult = await x.AddLoginAsync(newUser.Id, loginInfo.Login);
                if (!userCreationResult.Succeeded)
                {
                    return ExternalLoginResult.Failed(userCreationResult.Errors.ToArray());
                }

                await signInManager.SignInAsync(newUser, isPersistent: false, rememberBrowser: false);
                return ExternalLoginResult.Succeed();
            });
        }

        private async Task<IdentityResult> CreateUser(AppUserManager userManager, DbUser newUser, System.DateTime? now = null, string password = null)
        {
            now = now ?? DateTimeService.GetCurrentDateTimeUtc();
            newUser.CreatedDate = now.Value;
            newUser.PublicId = Guid.NewGuid();

            return string.IsNullOrEmpty(password) 
                ? await userManager.CreateAsync(newUser)
                : await userManager.CreateAsync(newUser, password);
        }

        private bool RegistrationIpAlreadyUsed(string ip)
        {
            if (!string.IsNullOrEmpty(ip) && string.Compare(ip, Settings.GetAllowedRegistrationIp(), StringComparison.InvariantCulture) != 0)
            {
                using (var context = Repository.GetContext())
                {
                    return context.Set<DbUser>().Any(x => x.RegistrationIp == ip && x.EmailConfirmed);
                }
            }
            return false;
        }

        private string GetBadIpErrorMessage()
        {
            return "Зафиксирована попытка регистрации нового аккаунта. Пожалуйста, используйте ранее зарегистрированный аккаунт. " +
                   "И оплатите доступ, если он закончился. Сервис не может существовать без Вашей финансовой поддержки. " +
                   "При дальнейших попытках регистрации новых аккаунтов доступ будет для Вас закрыт навсегда. " +
                   "Если это все не про Вас, пожалуйста обратитесь в поддержку:  " + CommonSettings.GetSupportEmail();
        }

        private async Task ExecuteByUserManager(Func<AppUserManager, Task> executor)
        {
            await ExecuteByUserManager<object>(async x =>
            {
                await executor(x);
                return null;
            });
        }

        // TODO !!! Managers Are Disposable !!!
        private async Task<T> ExecuteByUserManager<T>(Func<AppUserManager, Task<T>> executor)
        {
            var userManager = GetUserManager();
            {
                var result = await executor(userManager);
                return result;
            }
        }

        private async Task ExecuteBySignInManager(Func<AppSignInManager, Task> executor)
        {
            await ExecuteBySignInManager<object>(async x =>
            {
                await executor(x);
                return null;
            });
        }

        private async Task<T> ExecuteBySignInManager<T>(Func<AppSignInManager, Task<T>> executor)
        {
            var signInManager = GetSignInManager();
            {
                var result = await executor(signInManager);
                return result;
            }
        }

        private IAuthenticationManager AuthManager => OwinContextHelper.GetOwinContext().Authentication;

        private AppUserManager GetUserManager() => OwinContextHelper.GetOwinContext().GetUserManager<AppUserManager>();

        private AppSignInManager GetSignInManager() => OwinContextHelper.GetOwinContext().Get<AppSignInManager>();
    }      
}
