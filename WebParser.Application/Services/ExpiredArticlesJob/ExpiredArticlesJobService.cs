﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.ExpiredArticlesJob;
using WebParser.Application.Services.Settings;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;
using WebParser.DataAccess.Helpers;

namespace WebParser.Application.Services.ExpiredArticlesJob
{
    internal class ExpiredArticlesJobService : IExpiredArticlesJobService
    {
        private class DeletedArticleInfo
        {
            public string DeletedArticleIdentity { get; set; }

            public string DeletedArticleSummary { get; set; }
        }

        private const int BatchTimeOutMilliseconds = 5000;
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IRepository Repository { get; }

        private IDateTimeService DateTimeService { get; }

        private ISettings Settings { get; }

        public ExpiredArticlesJobService(
            IRepository repository,
            IDateTimeService dateTimeService,
            ISettings settings)
        {
            Repository = repository;
            DateTimeService = dateTimeService;
            Settings = settings;
        }

        public async Task<JobResult> DeleteExpiredArticles()
        {
            var expiredDate = DateTimeService.GetCurrentDateTimeUtc().AddDays(-Settings.GetArticlesExpirationDays());

            var log = new StringBuilder();

            for (int i = 0; i < 50; i++)
            {
                int deletedArticles = await DeleteExpiredArticles(expiredDate, log);
                if (deletedArticles == 0)
                {
                    break;
                }

                await Task.Delay(BatchTimeOutMilliseconds);
            }

            return new JobResult
            {
                Log = log.ToString(),
                Success = true,
            };
        }

        private async Task<int> DeleteExpiredArticles(System.DateTime expiredDate, StringBuilder log)
        {
            for (int tryNumber = 0; tryNumber < 5; tryNumber++)
            {
                try
                {
                    var result = await DeleteExpiredArticles(expiredDate);

                    log.Append($"DeleteOldArticles: {result} expired articles deleted by {tryNumber} attempt.\n");

                    return result;
                }
                catch (Exception ex)
                {
                    var errorMessage = $"Unhandled exception during DeleteOldArticles. {tryNumber} attempt.";
                    Log.Error(errorMessage, ex);

                    log.Append($"{errorMessage} ex.Message: {ex.Message}\n");
                }
                await Task.Delay(BatchTimeOutMilliseconds + (BatchTimeOutMilliseconds * tryNumber));
            }
            return 0;
        }

        private async Task<int> DeleteExpiredArticles(DateTime expiredDate)
        {
            using (var context = Repository.GetContext())
            {
                var articles = await context
                    .Set<DbParsedArticle>()
                    .Where(x => x.CreatedDate < expiredDate)
                    .Take(1000)
                    .ExecuteAsync();

                if (articles.Length == 0)
                {
                    return 0;
                }

                using (var transaction = context.BeginTransaction())
                {
                    var articleDictionary = articles.ToDictionary(x => x.Id, x => new DeletedArticleInfo
                    {
                        DeletedArticleIdentity = x.Identity,
                        DeletedArticleSummary =
                            $"SiteId{x.SiteId}_ArticlesGroupId{x.ArticlesGroupId}_CreatedDate{x.CreatedDate.ToString("YYMMdd HH:mm")}_{x.Link}".Cut(512),
                    });

                    var articleIds = articles.ToArray(x => (int?) x.Id);

                    await ClearTargetArticle<DbBlackListItem>(context, articleIds, articleDictionary, x =>
                    {
                        x.SiteId = x.ArticleSiteId;
                        x.ArticleSiteId = null;
                    });

                    await ClearTargetArticle<DbWhiteListItem>(context, articleIds, articleDictionary, x =>
                    {
                        x.SiteId = x.ArticleSiteId;
                        x.ArticleSiteId = null;
                    });

                    await ClearTargetArticle<DbWhiteListItem>(context, articleIds, articleDictionary, null);
                    await ClearTargetArticle<DbArticleRatingVote>(context, articleIds, articleDictionary, null);
                    await ClearTargetArticle<DbOpenedContact>(context, articleIds, articleDictionary, null);

                    context.Set<DbParsedArticle>().RemoveRange(articles);

                    context.ValidateAndSave();
                    transaction.Commit();
                }

                return articles.Length;
            }
        }

        private static async Task ClearTargetArticle<T>(
            DataBaseContext context,
            int?[] articleIds,
            Dictionary<int, DeletedArticleInfo> articleDictionary,
            Action<T> customCleanup)
            where T : class, IDbHaveTargetArticleId
        {
            var entities = await context
                .Set<T>()
                .Where(x => articleIds.Contains(x.ArticleId))
                .ExecuteAsync();
            foreach (var entity in entities)
            {
                var deletedArticle = articleDictionary[entity.ArticleId.Value];
                entity.DeletedArticleIdentity = deletedArticle.DeletedArticleIdentity;
                entity.DeletedArticleSummary = deletedArticle.DeletedArticleSummary;

                entity.ArticleId = null;
                entity.ArticleUserId = null;

                customCleanup?.Invoke(entity);
            }
            context.ValidateAndSave();
        }
    }
}
