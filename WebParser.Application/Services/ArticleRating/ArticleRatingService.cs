﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amazon.ECS.Model;
using log4net;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.ArticleRating;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Services.BlackList;
using WebParser.Application.Services.Settings;
using WebParser.Application.Services.UserManagement;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;
using WebParser.DataAccess.Helpers;

namespace WebParser.Application.Services.ArticleRating
{
    internal class ArticleRatingService : IArticleRatingService
    {
        private static readonly ILog Log = LogManager.GetLogger("infoSmtpLog");

        private class ClickerCautionCondition
        {
            public ArticleRatingType ArticleRatingType { get; set; }

            public int Value { get; set; }

            public bool ClickerDetected(DbParsedArticle article)
            {
                switch (ArticleRatingType)
                {
                    case ArticleRatingType.Good:
                        return article.GoodRating >= Value;

                    case ArticleRatingType.Bad:
                        return article.BadRating >= Value;

                    case ArticleRatingType.Outdate:
                        return article.OutdateRating >= Value;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private class RatingCount
        {
            public int RatingTypeId { get; set; }

            public int Count { get; set; }
        }

        private static readonly Dictionary<ArticleRatingType, ClickerCautionCondition[]> ClickerCautionConditions = new Dictionary<ArticleRatingType, ClickerCautionCondition[]>
        {
            [ArticleRatingType.Good] = new[]
            {
                new ClickerCautionCondition
                {
                    ArticleRatingType = ArticleRatingType.Bad, Value = 2,
                },
                new ClickerCautionCondition
                {
                    ArticleRatingType = ArticleRatingType.Outdate, Value = 2,
                },
            },
            [ArticleRatingType.Bad] = new[]
            {
                new ClickerCautionCondition
                {
                    ArticleRatingType = ArticleRatingType.Good, Value = 3,
                },
                new ClickerCautionCondition
                {
                    ArticleRatingType = ArticleRatingType.Outdate, Value = 2,
                },
            },
            [ArticleRatingType.Outdate] = new[]
            {
                new ClickerCautionCondition
                {
                    ArticleRatingType = ArticleRatingType.Bad, Value = 2,
                },
            },
        };

        private IRepository Repository { get; }

        private IDateTimeService DateTimeService { get; }

        private IUserService UserService { get; }

        private IInternalUserService InternalUserService { get; }

        private IBlackListService BlackListService { get; }

        private ISettings Settings { get; }

        public ArticleRatingService(
            IRepository repository, 
            IDateTimeService dateTimeService, 
            IUserService userService,
            IBlackListService blackListService,
            IInternalUserService internalUserService,
            ISettings settings)
        {
            Repository = repository;
            DateTimeService = dateTimeService;
            UserService = userService;
            BlackListService = blackListService;
            InternalUserService = internalUserService;
            Settings = settings;
        }

        public PagingResult<UnwatchedVote> ReadUnwatchedVotes<T>(RequestFilters<T> filters)
        {
            using (var context = Repository.GetContext())
            {
                return context
                    .Set<DbArticleRatingVote>()
                    .Where(x => x.ReviewResultId == null)
                    .Select(x => new UnwatchedVote
                    {
                        Id = x.Id,
                        VoteDate = x.VotedDate,
                        ArticleRatingTypeId = x.ArticleRatingTypeId,
                        ArticleId = x.ArticleId,
                        DeletedArticleIdentity = x.DeletedArticleIdentity,
                        DeletedArticleSummary = x.DeletedArticleSummary,
                        RateTypeId = x.RateTypeId,
                        UserVkId = x.User.VkId,
                        UserEmail = x.User.Email,
                        UserAccessDateTime = x.User.AccessDateTime,
                        UserFreeHours = x.User.FreeHours,
                        UserOpeningBalance = x.User.OpeningBalance,
                        UserFreeOpenings = x.User.FreeOpenings,
                    })
                    .ApplyFilters(filters);
            }
        }

        public void SaveVoteReviewResult(int voteId, ReviewResult reviewResult)
        {
            using (var context = Repository.GetContext())
            using (var transaction = context.BeginTransaction())
            {
                var vote = context.FindFirst<DbArticleRatingVote, ArticleRatingException>(x => x.Id == voteId, "Vote not found");               
                vote.ReviewResultId = (int)reviewResult;
                vote.ReviewerId = UserService.GetCurrentUserId().Value;
                vote.ReviewingDate = DateTimeService.GetCurrentDateTimeUtc();

                context.ValidateAndSave();

                if (reviewResult == ReviewResult.Rejected)
                {
                    transaction.Commit();
                    return;
                }

                var user = context.FindFirst<DbUser, ArticleRatingException>(x => x.Id == vote.UserId, "User not found");

                var rateType = (RateType)vote.RateTypeId;

                var counts = CalculateRatingCounts(context, user, rateType);

                switch (rateType)
                {
                    case RateType.Openings:
                        var freeOpenings = (int)counts.Sum(
                            x => CalculateFreeBonusItemsPerRatingType(
                                x,
                                ArticleRatingConstants.FreeOpeningGoodVotesPrice,
                                ArticleRatingConstants.FreeOpeningBadVotesPrice,
                                ArticleRatingConstants.FreeOpeningOutdateVotesPrice));

                        if (freeOpenings > user.FreeOpenings && !InternalUserService.MaxFreeBonusItemsCountReached(user, rateType))
                        {
                            user.OpeningBalance += freeOpenings - user.FreeOpenings;
                            user.FreeOpenings = freeOpenings;
                        }
                        break;

                    case RateType.Days:
                        var freeHours = (int) counts.Sum(
                            x => CalculateFreeBonusItemsPerRatingType(
                                x,
                                ArticleRatingConstants.FreeHourGoodVotesPrice,
                                ArticleRatingConstants.FreeHourBadVotesPrice,
                                ArticleRatingConstants.FreeHourOutdateVotesPrice));

                        if (freeHours > user.FreeHours && !InternalUserService.MaxFreeBonusItemsCountReached(user, rateType))
                        {
                            var daysToAdd = freeHours - user.FreeHours;
                            var now = DateTimeService.GetCurrentDateTimeUtc();
                            var from = user.AccessDateTime > now ? user.AccessDateTime : now;

                            user.AccessDateTime = from.AddHours(daysToAdd);
                            user.FreeHours = freeHours;
                        }
                        break;

                    default:
                        throw new ArgumentOutOfRangeException(nameof(rateType), rateType, null);
                }

                context.ValidateAndSave();
                transaction.Commit();
            }
        }

        public VoteResultType Vote(int articleId, ArticleRatingType ratingType)
        {
            var currentUser = UserService.GetCurrentUser();
            if (currentUser == null)
            {
                throw new ArticleRatingException("Current User is null");
            }

            using (var context = Repository.GetContext())
            using (var transaction = context.BeginTransaction())
            {
                var currentUserFromDb = context.FindFirst<DbUser, ArticleRatingException>(x => x.Id == currentUser.Id, "User not found");

                var rateType = context
                    .Set<DbOpenedContact>()
                    .Where(x => x.ArticleId == articleId && x.UserId == currentUser.Id)
                    .Select(x => (RateType?)x.RateTypeId)
                    .FirstOrDefault();

                if (!rateType.HasValue)
                {
                    Log.Error($"RateType not found by opened contact. articleId: {articleId}");
                    rateType = RateType.Openings;
                }

                if (InternalUserService.UserCanNotVote(currentUserFromDb, rateType.Value))
                {
                    return VoteResultType.Success;
                }

                var article = context.FindFirst<DbParsedArticle, ArticleRatingException>(x => x.Id == articleId, "ArticleForVote not found");

                if (CheckClickerAction(ratingType, article, currentUserFromDb))
                {
                    context.ValidateAndSave();
                    transaction.Commit();
                    return VoteResultType.ClickerCaution;
                }

                SaveVote(context, article, currentUserFromDb.Id, ratingType, rateType.Value);

                context.ValidateAndSave();
                transaction.Commit();

                System.Threading.Tasks.Task.Run(() =>
                {
                    Log.Info($"Vote submitted: Please review: {Path.Combine(Settings.GetSiteUrl(), "Vote/UnwatchedVotesReport")}");
                });

                return VoteResultType.Success;
            }
        }

        private static bool CheckClickerAction(ArticleRatingType ratingType, DbParsedArticle article, DbUser currentUser)
        {
            var conditions = ClickerCautionConditions[ratingType];
            if (conditions.Any(x => x.ClickerDetected(article)))
            {
                currentUser.ClickerCautionCount++;
                return true;
            }
            return false;
        }

        private RatingCount[] CalculateRatingCounts(DataBaseContext context, DbUser user, RateType rateType)
        {
            return context
                .Set<DbArticleRatingVote>()
                .Where(
                    x =>
                        x.UserId == user.Id &&
                        x.ReviewResultId == (int)ReviewResult.Confirmed &&
                        x.RateTypeId == (int)rateType)
                .GroupBy(x => x.ArticleRatingTypeId)
                .Select(x => new RatingCount
                {
                    RatingTypeId = x.Key,
                    Count = x.Count(),
                })
                .Execute();
        }

        private static decimal CalculateFreeBonusItemsPerRatingType(RatingCount ratingCount, decimal freeItemGoodVotesPrice, decimal freeItemBadVotesPrice, decimal freeItemOutedataVotesPrice)
        {
            switch ((ArticleRatingType)ratingCount.RatingTypeId)
            {
                case ArticleRatingType.Good:
                    return ratingCount.Count / freeItemGoodVotesPrice;

                case ArticleRatingType.Bad:
                    return ratingCount.Count / freeItemBadVotesPrice;

                case ArticleRatingType.Outdate:
                    return ratingCount.Count / freeItemOutedataVotesPrice;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SaveVote(DataBaseContext context, DbParsedArticle article, int currentUserId, ArticleRatingType ratingType, RateType rateType)
        {
            switch (ratingType)
            {
                case ArticleRatingType.Good:
                    article.GoodRating++;
                    break;

                case ArticleRatingType.Bad:
                    article.BadRating++;
                    if (article.GoodRating - article.BadRating <= ArticleRatingConstants.DoNotShowArticleRating)
                    {
                        BlackListService.AddToBlackList(context, article, currentUserId);
                    }
                    break;

                case ArticleRatingType.Outdate:
                    article.OutdateRating++;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(ratingType), ratingType, null);
            }

            context.Set<DbArticleRatingVote>().Add(new DbArticleRatingVote
            {
                UserId = currentUserId,
                ArticleId = article.Id,
                ArticleUserId = article.UserId,
                ArticleRatingTypeId = (int)ratingType,
                VotedDate = DateTimeService.GetCurrentDateTimeUtc(),
                RateTypeId = (int)rateType,
            });

            context.ValidateAndSave();
        }
    }
}
