﻿namespace WebParser.Application.Services.SocialMedia.Entities
{
    internal class ArticlesGroupInfo
    {
        public int SiteId { get; set; }

        public int UserId { get; set; }

        public string NameForViewers { get; set; }

        public string SiteName { get; set; }

        public int ArticleGroupId { get; set; }

        public long? VkGroupId { get; set; }

        public long? VkTopicId { get; set; }

        public long? VkTopicGroupId { get; set; }

        public string ArticleGroupName { get; set; }

        public string TwitterConsumerKey { get; set; }

        public string TwitterConsumerSecret { get; set; }

        public string TwitterAccessToken { get; set; }

        public string TwitterAccessTokenSecret { get; set; }
    }
}