﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.ArticleRating;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SocialMedia;
using WebParser.Application.Mappers;
using WebParser.Application.Services.Parsing.ParsedContent.JsonEntities;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Application.Services.SocialMedia.Entities;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;
using WebParser.Integration.Twitter;
using WebParser.Integration.Twitter.Entities;
using WebParser.Integration.Vk;
using WebParser.Integration.Vk.Entities;

namespace WebParser.Application.Services.SocialMedia
{
    internal class SocialMediaService : ISocialMediaService
    {
        private const int ArticlesPerGroupCount = 5;
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IRepository Repository { get; }

        private IParsingLogger ParsingLogger { get; }

        private IDateTimeService DateTimeService { get; }

        private IVkClient VkClient { get; }

        private ITwitterClient TwitterClient { get; }

        private IMetroMapService MetroMapService { get; }

        public SocialMediaService(
            IRepository repository,
            IParsingLogger parsingLogger,
            IDateTimeService dateTimeService,
            IVkClient vkClient,
            ITwitterClient twitterClient,
            IMetroMapService metroMapService)
        {
            Repository = repository;
            ParsingLogger = parsingLogger;
            DateTimeService = dateTimeService;
            VkClient = vkClient;
            TwitterClient = twitterClient;
            MetroMapService = metroMapService;
        }

        public async Task<bool> RunExportParsingResultToSocialMedia(DateTime previousRunTime)
        {
            try
            {
                await Run(previousRunTime);
            }
            catch (Exception ex)
            {
                Log.Error("Unhandled exception on Social Media Job Run", ex);
            }
            
            return true;
        }

        private async Task Run(DateTime previousRunTime)
        {
            using (var context = Repository.GetContext())
            {
                var vkArticlesGroupsToExprot = await GetArticlesGroupsToExport(context, x => x.VkGroupId != null && x.VkTopicId != null && x.VkTopicGroupId != null);
                var twitterArticlesGroupsToExprot = await GetArticlesGroupsToExport(context,
                    x => 
                        x.TwitterConsumerKey != null &&
                        x.TwitterConsumerSecret != null &&
                        x.TwitterAccessToken != null &&
                        x.TwitterAccessTokenSecret != null);

                var vkArticles = await GetArticlesToExport(context, vkArticlesGroupsToExprot, previousRunTime, x => ToVkArticleToExport(x, vkArticlesGroupsToExprot));
                var twitterArticles = await GetArticlesToExport(context, twitterArticlesGroupsToExprot, previousRunTime, x => ToTwitterArticleToExport(x, twitterArticlesGroupsToExprot));

                await Task.WhenAll(
                    VkClient.Export(vkArticles, (siteId, userId, message) => ParsingLogger.Log(siteId, userId, ParsingLogStatus.Success, message)),
                    TwitterClient.Export(twitterArticles, (siteId, userId, message) => ParsingLogger.Log(siteId, userId, ParsingLogStatus.Success, message)));
            }
        }

        private async Task<T[]> GetArticlesToExport<T>(DataBaseContext context, Dictionary<int, ArticlesGroupInfo> articlesGroupsToExprot, DateTime previousRunTime, Func<DbParsedArticle, T> convert)
        {
            var articlesGroupIds = articlesGroupsToExprot.ToArray(x => (int?) x.Key);
            if (articlesGroupIds.Length == 0)
            {
                return new T[0];
            }

            var query = context.Set<DbParsedArticle>()
                .Where(
                    x =>
                        !x.IsDuplicate &&
                        x.BlackListItemId == null &&
                        x.HasImages &&
                        x.CreatedDate > previousRunTime &&
                        x.GoodRating - x.BadRating >= ArticleRatingConstants.PostToSocialMediaArticleRating);

            var articles = new List<T>();
            foreach (var articlesGroupId in articlesGroupIds)
            {
                var articlesResult = await query
                    .Where(x => x.ArticlesGroupId == articlesGroupId)
                    .OrderByDescending(x => x.GoodRating - x.BadRating)
                    .ThenByDescending(x => x.CreatedDate)
                    .Take(ArticlesPerGroupCount)
                    .ExecuteAsync();

                articles.AddRange(articlesResult.ToArray(convert));
            }

            Shuffle(articles);

            return articles.ToArray();
        }

        private static async Task<Dictionary<int, ArticlesGroupInfo>> GetArticlesGroupsToExport(DataBaseContext context, Expression<Func<DbArticlesGroup, bool>> whereExpression)
        {
            var articlesGroupsToExprot = await context
                .Set<DbArticlesGroup>()
                .Where(x =>
                    x.Site.IsEnabled &&
                   (x.Site.JobTypeId == (int)JobType.Parsing || x.Site.JobTypeId == (int)JobType.ParsingAndWhiteList) &&
                    x.User.NameForViewers != null)
                .Where(whereExpression)
                .Select(x => new ArticlesGroupInfo
                {
                    SiteId = x.Site.Id,
                    UserId = x.UserId,
                    SiteName = x.Site.Name,
                    ArticleGroupId = x.Id,
                    ArticleGroupName = x.InternalName,
                    VkGroupId = x.VkGroupId,
                    VkTopicId = x.VkTopicId,
                    VkTopicGroupId = x.VkTopicGroupId,
                    TwitterAccessToken = x.TwitterAccessToken,
                    TwitterAccessTokenSecret = x.TwitterAccessTokenSecret,
                    TwitterConsumerKey = x.TwitterConsumerKey,
                    TwitterConsumerSecret = x.TwitterConsumerSecret,
                })
                .ToDictionaryAsync(x => x.ArticleGroupId, x => x);
            return articlesGroupsToExprot;
        }

        private VkArticleToExport ToVkArticleToExport(DbParsedArticle article, Dictionary<int, ArticlesGroupInfo> articlesGroupsToExprot)
        {
            var articleGroupInfo = articlesGroupsToExprot[article.ArticlesGroupId.Value];
            return new VkArticleToExport
            {
                ArticlesGroupInfo = articleGroupInfo.Map<ArticlesGroupInfo, VkArticlesGroupInfo>(),
                ApartmentDetails = ToVkApartmentDetails(article, article.ParsedContent.Deserialize<JsonParsedContent>().ToParsedValues(), articleGroupInfo.ArticleGroupName),
            };
        }

        private TwitterArticleToExport ToTwitterArticleToExport(DbParsedArticle article, Dictionary<int, ArticlesGroupInfo> articlesGroupsToExprot)
        {
            var articleGroupInfo = articlesGroupsToExprot[article.ArticlesGroupId.Value];
            return new TwitterArticleToExport
            {
                ArticlesGroupInfo = articleGroupInfo.Map<ArticlesGroupInfo, TwitterArticlesGroupInfo>(),
                ApartmentDetails = ToTwitterApartmentDetails(article, article.ParsedContent.Deserialize<JsonParsedContent>().ToParsedValues(), articleGroupInfo.ArticleGroupName),
            };
        }

        private static void Shuffle<T>(IList<T> list)
        {
            var random = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        private VkApartmentDetails ToVkApartmentDetails(DbParsedArticle article, ParsedValue[] parsedValues, string articleGropName)
        {
            return new VkApartmentDetails
            {
                Id = article.Id,
                ObjectName = GetParsedValue(parsedValues, ParsedKeyType.Object) ?? articleGropName,
                PriceStr = article.PriceStr,
                Address = article.FormattedAddress ?? GetParsedValue(parsedValues, ParsedKeyType.Address),
                Metro = MetroMapService.GetStationByJsonIds(article.Metro)?.Name,
                DistanceToMetroText = article.DistanceToMetroText,
                DurationToMetroText = article.DurationToMetroText,
                Description = GetParsedValue(parsedValues, ParsedKeyType.Description),
                Characteristics = GetParsedValue(parsedValues, ParsedKeyType.Characteristics),
                Images = article.FullSizeImages.ToImageInfo().ToArray(x => x.ImageUrl),
            };
        }

        private TwitterApartmentDetails ToTwitterApartmentDetails(DbParsedArticle article, ParsedValue[] parsedValues, string articleGropName)
        {
            return new TwitterApartmentDetails
            {
                Id = article.Id,
                ObjectName = GetParsedValue(parsedValues, ParsedKeyType.Object) ?? articleGropName,
                PriceStr = article.PriceStr,
                Address = article.FormattedAddress ?? GetParsedValue(parsedValues, ParsedKeyType.Address),
                Metro = MetroMapService.GetStationByJsonIds(article.Metro)?.Name,
                Description = GetParsedValue(parsedValues, ParsedKeyType.Description),
                DurationToMetroText = article.DurationToMetroText,
                DistanceToMetroText = article.DistanceToMetroText,
                Latitude = article.Latitude,
                Longitude = article.Longitude,
            };
        }

        private static string GetParsedValue(ParsedValue[] parsedValues, ParsedKeyType keyType)
        {
            return parsedValues.FirstOrDefault(x => x.ParsedKeyType == keyType)?.Values.FirstOrDefault();
        }
    }
}
