﻿using WebParser.Application.Interfaces.Services.DateTimeProcessing;

namespace WebParser.Application.Services.DateTimeProcessing
{
    internal class DateTimeService : IDateTimeService
    {
        public System.DateTime GetCurrentDateTimeUtc()
        {
            return System.DateTime.UtcNow;
        }
    }
}
