﻿using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.BlackList
{
    internal interface IBlackListService
    {
        void AddToBlackList(DataBaseContext context, DbParsedArticle article, int userId);

        int? AddToBlackList(DataBaseContext context, ArticleIdentity articleIdentity, int userId, int siteId);

        int? GetBlackListItemId(DataBaseContext context, ArticleIdentity articleIdentity, int? siteId);

        void SaveBlackListItems(ArticleIdentity[] articleIdentities, int userId, int siteId);

        int GetItemsCount();
    }
}
