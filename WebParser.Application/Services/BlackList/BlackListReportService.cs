﻿using System;
using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.BlackList;
using WebParser.Application.Interfaces.Services.BlackList.Entities;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Mappers;
using WebParser.Application.Services.BlackList.Entities;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.BlackList
{
    internal class BlackListReportService : IBlackListReportService
    {
        private IRepository Repository { get; }

        private IUserService UserService { get; }

        private IDateTimeService DateTimeService { get; }

        public BlackListReportService(
            IRepository repository,
            IUserService userService,
            IDateTimeService dateTimeService)
        {
            Repository = repository;
            UserService = userService;
            DateTimeService = dateTimeService;
        }

        public PagingResult<BlackListItem> ReadBlackListItems<T>(RequestFilters<T> filters)
        {
            using (var context = Repository.GetContext())
            {
                var result = context
                    .Set<DbBlackListItem>()
                    .Select(x => new BlackListItem
                    {
                        Id = x.Id,
                        CreatedDate = x.CreatedDate,
                        Identity = x.Identity,
                        IdentityConfidencePercentage = x.IdentityConfidencePercentage,
                        SiteId = x.SiteId,
                        ArticleSiteId = x.ArticleSiteId,
                        SourceArticleId = x.ArticleId,
                        CreatedBy = x.CreatedById,
                        Hits = x.ParsedArticles.Count(),
                    })
                    .ApplyFilters(filters, (BlackListItem x) => x);
                return result;
            }
        }

        public PagingResult<HitedArticle> ReadHitedArticles<T>(RequestFilters<T> filters, int blackListItemId)
        {
            using (var context = Repository.GetContext())
            {
                var result = context
                    .Set<DbParsedArticle>()
                    .Where(x => x.BlackListItemId == blackListItemId)
                    .Select(x => new HitedArticleProjection
                    {
                        Id = x.Id,
                        CreatedDate = x.CreatedDate,
                        SiteId = x.SiteId,
                        ArticleGroup = x.ArticlesGroup.InternalName,
                        DuplicateGroupId = x.DuplicateGroupId,
                        Identity = x.Identity,
                        IdentityConfidencePercentage = x.IdentityConfidencePercentage,
                        IsDuplicate = x.IsDuplicate,
                        Price = x.PriceStr,
                        Metro = x.Metro,
                        MetroDistance = x.DistanceToMetroText,
                        MetroDuration = x.DurationToMetroText,
                        FormattedAddress = x.FormattedAddress,
                        Link = x.Link,
                    })
                    .ApplyFilters(filters, x => x.Map<HitedArticleProjection, HitedArticle>());
                return result;
            }
        }

        public void DeleteBlackListItem(int id)
        {
            using (var context = Repository.GetContext())
            {
                var item = context
                    .FindFirst<DbBlackListItem, BlackListException>(
                        x => x.Id == id, $"DbBlackListItem with id {id} not found.");

                var articles = context
                    .Set<DbParsedArticle>()
                    .Where(x => x.BlackListItemId == id)
                    .Execute();

                articles.Foreach(x => x.BlackListItemId = null);
                context.ValidateAndSave();

                context.Set<DbBlackListItem>().Remove(item);
                context.ValidateAndSave();
            }
        }

        public void AddBlackListItem(BlackListItem item)
        {
            var currentUserId = UserService.GetCurrentUserId();
            if (!currentUserId.HasValue)
            {
                return;
            }

            using (var context = Repository.GetContext())
            {
                DbParsedArticle sourceArticle = null;
                if (item.SourceArticleId.HasValue)
                {
                    sourceArticle = context
                        .FindFirst<DbParsedArticle, BlackListException>(x => x.Id == item.SourceArticleId, $"Article by Id: {item.SourceArticleId} not found.");
                }

                var newBlackListItem = new DbBlackListItem
                {
                    ArticleId = sourceArticle?.Id,
                    ArticleSiteId = sourceArticle?.SiteId,
                    ArticleUserId = sourceArticle?.UserId,
                    SiteId = item.SiteId,
                    CreatedById = currentUserId.Value,
                    CreatedDate = DateTimeService.GetCurrentDateTimeUtc(),
                    Identity = item.Identity,
                    IdentityConfidencePercentage = 100,
                };

                newBlackListItem = context.Set<DbBlackListItem>().Add(newBlackListItem);
                context.ValidateAndSave();

                if (sourceArticle != null)
                {
                    sourceArticle.BlackListItemId = newBlackListItem.Id;
                    context.ValidateAndSave();
                }
            }
        }
    }
}
