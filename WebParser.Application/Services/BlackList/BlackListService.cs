﻿using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Mappers;
using WebParser.Application.Services.Parsing.ParsedContent;
using WebParser.Application.Services.Parsing.ParsedContent.JsonEntities;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.BlackList
{
    internal class BlackListService : IBlackListService
    {
        private const int BlackListArticleCount = 1000;
        private IDateTimeService DateTimeService { get; }

        private IRepository Repository { get; }

        private IParsingLogger ParsingLogger { get; }

        public BlackListService(
            IDateTimeService dateTimeService,
            IRepository repository,
            IParsingLogger parsingLogger)
        {
            DateTimeService = dateTimeService;
            Repository = repository;
            ParsingLogger = parsingLogger;
        }

        public void AddToBlackList(DataBaseContext context, DbParsedArticle article, int userId)
        {
            var articleIdentity = article.ParsedContent.Deserialize<JsonParsedContent>()
                .ToParsedValues()
                .GetArticleIdentity();

            var blackListItemId = AddToBlackList(context, articleIdentity, userId, null, article.Id, article.UserId, article.SiteId);

            article.BlackListItemId = blackListItemId;

            context.ValidateAndSave();
        }

        public int? AddToBlackList(DataBaseContext context, ArticleIdentity articleIdentity, int userId, int siteId)
        {
            return AddToBlackList(context, articleIdentity, userId, siteId, null, null, null);
        }

        private int? AddToBlackList(DataBaseContext context, ArticleIdentity articleIdentity, int createdById, int? siteId, int? articleId, int? articleUserId, int? articleSiteId)
        {
            if (articleIdentity == null ||
                articleIdentity.IdentityConfidencePercentage == 0 ||
                string.IsNullOrEmpty(articleIdentity.Identity))
            {
                return null;
            }

            var existedItemId = GetBlackListItemId(context, articleIdentity, articleSiteId ?? siteId);

            if (existedItemId.HasValue)
            {
                return existedItemId.Value;
            }

            var blackListItem = context.Set<DbBlackListItem>()
                .Add(new DbBlackListItem
                {
                    Identity = articleIdentity.Identity,
                    IdentityConfidencePercentage = articleIdentity.IdentityConfidencePercentage,
                    SiteId = siteId,
                    ArticleId = articleId,
                    ArticleUserId = articleUserId,
                    ArticleSiteId = articleSiteId,
                    CreatedById = createdById,
                    CreatedDate = DateTimeService.GetCurrentDateTimeUtc(),
                });

            context.ValidateAndSave();

            return blackListItem.Id;
        }

        public int? GetBlackListItemId(DataBaseContext context, ArticleIdentity articleIdentity, int? siteId)
        {
            if (articleIdentity == null || articleIdentity.IdentityConfidencePercentage == 0)
            {
                return null;
            }

            var result = context
                .Set<DbBlackListItem>()
                .Where(ArticleIdentityHelper.GetInBlackListPredicate(articleIdentity, siteId))
                .Select(x => (int?)x.Id)
                .FirstOrDefault();

            return result;
        }

        public void SaveBlackListItems(ArticleIdentity[] articleIdentities, int userId, int siteId)
        {
            if (articleIdentities.Length == 0)
            {
                throw new ParsingException($"articleIdentities is empty. SiteId {siteId}, UserId {userId}");
            }
            
            using (var context = Repository.GetContext())
            {
                var siteInfo = context
                    .Set<DbSite>()
                    .Where(x => x.Id == siteId && x.JobTypeId == (int)JobType.BlackList)
                    .Select(x => new
                    {
                        SiteId = x.Id,
                        x.BlackListIdentitySiteId,
                    })
                    .FirstOrDefault();

                if (siteInfo == null)
                {
                    throw new ParsingException($"Site not found to save Black List Item. SiteId: {siteId}");
                }

                var sourceSiteId = siteInfo.BlackListIdentitySiteId ?? siteInfo.SiteId;
                var now = DateTimeService.GetCurrentDateTimeUtc();

                var itemsToSave = articleIdentities
                    .Where(
                        x => 
                            x.IdentityConfidencePercentage > 0 &&
                            !string.IsNullOrEmpty(x.Identity) &&
                            !InBlackList(context, x, sourceSiteId))
                    .ToArray(x => new DbBlackListItem
                    {
                        Identity = x.Identity.Cut(20),
                        IdentityConfidencePercentage = x.IdentityConfidencePercentage,
                        SiteId = sourceSiteId,
                        CreatedById = userId,
                        CreatedDate = now,
                    });

                ParsingLogger.Log(
                    siteId,
                    userId,
                    ParsingLogStatus.Success,
                    $"SaveBlackListItems completed. Saved {itemsToSave.Length}/{articleIdentities.Length} identities.");

                if (itemsToSave.Length == 0)
                {
                    return;
                }

                context.Set<DbBlackListItem>().AddRange(itemsToSave);
                context.ValidateAndSave();
            }
        }

        private static bool InBlackList(DataBaseContext context, ArticleIdentity articleIdentity, int siteId)
        {
            return context
                .Set<DbBlackListItem>()
                .Where(ArticleIdentityHelper.GetInBlackListPredicate(articleIdentity, siteId))
                .Any();
        }

        public int GetItemsCount()
        {
            using (var context = Repository.GetContext())
            {
                return context
                    .Set<DbBlackListItem>()
                    .Count();
            }
        }
    }
}
