﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.Reports;
using WebParser.Application.Interfaces.Services.Reports.Entities;
using WebParser.Application.Interfaces.Services.WhiteList.Entities;
using WebParser.Application.Mappers;
using WebParser.Application.Services.Reports.Entities;
using WebParser.Application.Services.WhiteList.Entities;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Reports
{
    internal class BlackAndWhiteArticlesReportService : IBlackAndWhiteArticlesReportService
    {
        private IRepository Repository { get; }

        public BlackAndWhiteArticlesReportService(
            IRepository repository)
        {
            Repository = repository;
        }

        public PagingResult<BlackAndWhiteArticle> ReadBlackAndWhiteArticles<T>(RequestFilters<T> filters)
        {
            using (var context = Repository.GetContext())
            {
                var result = context
                    .Set<DbParsedArticle>()
                    .Where(x => x.BlackListItemId != null && x.WhiteListItemId != null)
                    .Select(x => new BlackAndWhiteArticleProjection
                    {
                        BlackListItemId = x.BlackListItemId,
                        WhiteListItemId = x.WhiteListItemId,
                        Id = x.Id,
                        CreatedDate = x.CreatedDate,
                        SiteId = x.SiteId,
                        ArticleGroup = x.ArticlesGroup.InternalName,
                        DuplicateGroupId = x.DuplicateGroupId,
                        Identity = x.Identity,
                        IdentityConfidencePercentage = x.IdentityConfidencePercentage,
                        IsDuplicate = x.IsDuplicate,
                        Price = x.PriceStr,
                        Metro = x.Metro,
                        MetroDistance = x.DistanceToMetroText,
                        MetroDuration = x.DurationToMetroText,
                        FormattedAddress = x.FormattedAddress,
                        Link = x.Link,
                    })
                    .ApplyFilters(filters, x => x.Map<BlackAndWhiteArticleProjection, BlackAndWhiteArticle>());
                return result;
            }
        }

    }
}
