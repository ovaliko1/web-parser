﻿using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Repository
{
    internal interface IMetroRepositoryService
    {
        DbMetroStation[] ReadStations();
    }
}
