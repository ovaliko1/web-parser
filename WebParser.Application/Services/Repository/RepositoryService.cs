﻿using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Repository
{
    using System;

    internal class RepositoryService : IRepositoryService
    {
        public DbParsedArticle GetPreviousDuplicate(DataBaseContext context, ArticleIdentity articleIdentity, DateTime after, int siteId)
        {
            return context
                .Set<DbParsedArticle>()
                .Where(ArticleIdentityHelper.GetDuplicationPredicate(articleIdentity, after, siteId))
                .OrderByDescending(x => x.CreatedDate)
                .FirstOrDefault();
        }

        public DbParsedArticle[] GetPreviousDuplicates(DataBaseContext context, ArticleIdentity articleIdentity, DateTime after, int siteId)
        {
            return context
                .Set<DbParsedArticle>()
                .Where(ArticleIdentityHelper.GetDuplicationPredicate(articleIdentity, after, siteId))
                .Where(x => x.SiteId == siteId)
                .Execute();
        }
    }
}
