﻿using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Services.Parsing.ParsedContent;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Repository
{
    internal interface IRepositoryService
    {
        DbParsedArticle GetPreviousDuplicate(DataBaseContext context, ArticleIdentity articleIdentity, System.DateTime after, int siteId);

        DbParsedArticle[] GetPreviousDuplicates(DataBaseContext context, ArticleIdentity articleIdentity, System.DateTime after, int siteId);
    }
}