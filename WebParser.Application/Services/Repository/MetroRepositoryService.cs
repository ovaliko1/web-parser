﻿using System.Data.Entity;
using System.Linq;
using WebParser.Application.Helpers;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.Repository
{
    internal class MetroRepositoryService : IMetroRepositoryService
    {
        private IRepository Repository { get; }

        public MetroRepositoryService(IRepository repository)
        {
            Repository = repository;
        }

        public DbMetroStation[] ReadStations()
        {
            using (var context = Repository.GetContext())
            {
                return context.Set<DbMetroStation>()
                    .Include(x => x.MetroLine)
                    .Execute();
            }
        }
    }
}
