﻿using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.PushNotifications;

namespace WebParser.Application.Services.PushNotifications
{
    internal class PushNotificationsJob : IPushNotificationsJob
    {
        public PushNotificationsJob(IPushNotificationsService pushNotificationsService)
        {
            PushNotificationsService = pushNotificationsService;
        }

        private IPushNotificationsService PushNotificationsService { get; }

        public async Task<bool> RunPing()
        {
            return await PushNotificationsService.SendPingNotification();
        }
    }
}