﻿using System.Threading.Tasks;
using RestSharp;
using WebParser.Application.Services.Settings;
using WebParser.Application.Services.WebClient;

namespace WebParser.Application.Services.PushNotifications
{
    internal class PushNotificationsService : IPushNotificationsService
    {
        private ISettings Settings { get; }

        private IWebCommand WebCommand { get; }

        public PushNotificationsService(
            ISettings settings,
            IWebCommand webCommand)
        {
            Settings = settings;
            WebCommand = webCommand;
        }

        public async Task<bool> SendPingNotification()
        {
            var result = await WebCommand.MakeRequest(
                "https://onesignal.com/api/v1/notifications",
                Method.POST,
                new[]
                {
                    ("ContentType", "application/json; charset=utf-8"),
                    ("authorization", $"Basic {Settings.GetOneSignalRestApiKey()}"),
                },
                (Settings.GetOneSignalProxyAddress(), Settings.GetOneSignalProxyUser(), Settings.GetOneSignalProxyPassword()),
                new Parameter
                {
                    Name = "application/json",
                    Type = ParameterType.RequestBody,
                    Value = $@"{{
  ""app_id"": ""{Settings.GetOneSignalAppId()}"",
  ""contents"": {{ ""en"": ""Ping"" }},
  ""included_segments"": [""Active Users""]
}}"
                });

            return result;
        }
    }
}
