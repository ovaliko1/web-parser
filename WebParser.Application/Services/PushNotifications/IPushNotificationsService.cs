﻿using System.Threading.Tasks;

namespace WebParser.Application.Services.PushNotifications
{
    internal interface IPushNotificationsService
    {
        Task<bool> SendPingNotification();
    }
}
