﻿using System;
using System.Data.Entity;
using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Mappers;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.SiteStructure
{
    internal class CityService : ICityService
    {
        private IRepository Repository { get; }

        public CityService(IRepository repository)
        {
            Repository = repository;
        }

        public City[] GetCities()
        {
            using (var context = Repository.GetContext())
            {
                return context.Set<DbCity>().ExecuteAndConvert(x => x.Map<DbCity, City>());
            }
        }

        public City GetUserCity(int userId)
        {
            using (var context = Repository.GetContext())
            {
                return context.Set<DbUser>()
                    .Include(x => x.City)
                    .FirstOrDefault(x => x.Id == userId)
                    ?.City.Map<DbCity, City>();
            }
        }

        public int? GetUserIdByCity(int cityId)
        {
            using (var context = Repository.GetContext())
            {
                return context.Set<DbUser>()
                    .Where(x => x.CityId == cityId)
                    .Select(x => (int?)x.Id)
                    .FirstOrDefault();
            }
        }
    }
}
