﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Mappers;
using WebParser.Application.Services.SiteStructure.JsonEntities;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.Common.Mappers;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.SiteStructure
{
    // TODO: Rewrite to several services
    // TODO: Be carefull!!! Some methods can be run by anonymous but some requires right current user!!!  
    public class SiteStructureService : ISiteStructureService
    {
        private IRepository Repository { get; }

        private IUserService UserService { get; }

        public SiteStructureService(
            IRepository repository,
            IUserService userService)
        {
            Repository = repository;
            UserService = userService;
        }

        public int CreateSite(Site site)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                if (!currentUser.Roles.HasRole(Role.Admin))
                {
                    var existedUserSitesCount = context.Set<DbSite>().Count(x => x.UserId == currentUser.Id);
                    if (existedUserSitesCount + 1 > currentUser.MaxSiteCount)
                    {
                       throw new SiteStructureEditingException($"You have exceeded the maximum number of sites({currentUser.MaxSiteCount}).");
                    }
                }

                var newSite = site.Map<Site, DbSite>();
                newSite.UserId = currentUser.Id;

                var result = context.Set<DbSite>().Add(newSite);
                context.ValidateAndSave();
                return result.Id;
            }
        }

        public void EditSite(Site site)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var siteToEdit = context.FindFirst<DbSite, SiteStructureEditingException>(
                    x => x.Id == site.Id && x.UserId == currentUser.Id, $"Site with id {site.Id} not found.");

                siteToEdit.Link = site.Link;
                siteToEdit.Name = site.Name;
                siteToEdit.OneTimeExport= site.OneTimeExport;
                siteToEdit.IsEnabled = site.IsEnabled;
                siteToEdit.SchedulerStartHours = site.SchedulerStartHours;
                siteToEdit.SchedulerStartMinutes = site.SchedulerStartMinutes;
                siteToEdit.SchedulerEndHours = site.SchedulerEndHours;
                siteToEdit.SchedulerEndMinutes = site.SchedulerEndMinutes;
                siteToEdit.RepeatIntervalInMunutes = site.RepeatIntervalInMunutes;
                siteToEdit.ProxyAddress = site.ProxyAddress;
                siteToEdit.ProxyUsername = site.ProxyUsername;
                siteToEdit.ProxyPassword = site.ProxyPassword;
                siteToEdit.Cookies = site.Cookies.Serialize();
                siteToEdit.WebHookUrl = site.WebHookUrl;

                siteToEdit.WatermarkHeight = site.WatermarkHeight;
                siteToEdit.WatermarkWidth = site.WatermarkWidth;
                siteToEdit.WatermarkFontSize = site.WatermarkFontSize ;
                siteToEdit.WatermarkRectX = site.WatermarkRectX;
                siteToEdit.WatermarkRectY = site.WatermarkRectY;
                siteToEdit.WatermarkTextX = site.WatermarkTextX;
                siteToEdit.WatermarkTextY = site.WatermarkTextY;

                siteToEdit.UserAgent = site.UserAgent;
                siteToEdit.JobTypeId = site.JobTypeId;

                siteToEdit.GeoBounds = site.GeoBounds;
                siteToEdit.UseSourceLinkAsContact = site.UseSourceLinkAsContact;

                siteToEdit.CropImageTop = site.CropImageTop;
                siteToEdit.CropImageBottom = site.CropImageBottom;
                siteToEdit.CropImageLeft = site.CropImageLeft;
                siteToEdit.CropImageRight = site.CropImageRight;

                if (site.BlackListIdentitySiteId.HasValue)
                {
                    var blackListIdentitySite = context.FindFirst<DbSite, SiteStructureEditingException>(
                        x => x.Id == site.BlackListIdentitySiteId, $"Black List IdentitySite with id {site.Id} not found.");

                    siteToEdit.BlackListIdentitySite = blackListIdentitySite;
                }
                else
                {
                    siteToEdit.BlackListIdentitySiteId = null;
                }

                context.ValidateAndSave();
            }
        }

        public void DeleteSite(int siteId)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var site = context
                    .Set<DbSite>()
                    .Include(x => x.ParsedArticles)
                    .FindFirst<DbSite, SiteStructureEditingException>(
                        x => x.Id == siteId && x.UserId == currentUser.Id, $"Site with id {siteId} not found.");
                context.Set<DbSite>().Remove(site);
                context.ValidateAndSave();
            }
        }

        // Authorized user method
        public Site GetSiteById(int siteId)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var site = context.Set<DbSite>()
                    .Include(x => x.User)
                    .Include(x => x.User.City)
                    .Include(x => x.User.City.Country)
                    .FindFirst<DbSite, SiteStructureEditingException>(x => x.Id == siteId && x.UserId == currentUser.Id, $"Site by id {siteId} not found. ");

                var result = site.Map<DbSite, Site>();
                return result;
            }
        }

        public int CreateArticlesGroup(int siteId, ArticlesGroup articlesGroup)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var site = context.FindFirst<DbSite, SiteStructureEditingException>(
                    x => x.Id == siteId && x.UserId == currentUser.Id, $"Site with id {siteId} not found.");

                var newArticlesGroup = articlesGroup.Map<ArticlesGroup, DbArticlesGroup>();
                newArticlesGroup.SiteId = site.Id;
                newArticlesGroup.UserId = currentUser.Id;

                // TODO: Implement UI validation
                newArticlesGroup.Links = !string.IsNullOrEmpty(articlesGroup.LinksFormat) ? null : articlesGroup.Links.ArrayToDbString();

                newArticlesGroup = context.Set<DbArticlesGroup>().Add(newArticlesGroup);
                context.ValidateAndSave();
                return newArticlesGroup.Id;
            }
        }

        public void EditArticlesGroup(ArticlesGroup articlesGroup)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var dbArticlesGroup = context.FindFirst<DbArticlesGroup, SiteStructureEditingException>(
                    x => x.Id == articlesGroup.Id && x.UserId == currentUser.Id, $"ArticlesGroup with id {articlesGroup.Id} not found.");

                dbArticlesGroup.InternalName = articlesGroup.InternalName;
                dbArticlesGroup.PublicName = articlesGroup.PublicName;

                // TODO: Implement UI validation
                dbArticlesGroup.Links = !string.IsNullOrEmpty(articlesGroup.LinksFormat) ? null : articlesGroup.Links.ArrayToDbString();
                dbArticlesGroup.LinksToArticlesSearchRule = articlesGroup.LinksToArticlesSearchRule.Map<SearchRule, JsonSearchRule>().Serialize();
                dbArticlesGroup.ArticleSearchRules = articlesGroup.ArticleSearchRules.ToJsonSearchRules().Serialize();
                dbArticlesGroup.LinksFormat = articlesGroup.LinksFormat;
                dbArticlesGroup.StartPage = articlesGroup.StartPage;
                dbArticlesGroup.StepSize = articlesGroup.StepSize;
                dbArticlesGroup.VkGroupId = articlesGroup.VkGroupId;
                dbArticlesGroup.VkTopicId = articlesGroup.VkTopicId;
                dbArticlesGroup.VkTopicGroupId = articlesGroup.VkTopicGroupId;
                dbArticlesGroup.SpiderTypeId = articlesGroup.SpiderTypeId;
                dbArticlesGroup.LinksToArticlesSearchRuleSpiderTypeId = articlesGroup.LinksToArticlesSearchRuleSpiderTypeId;
                dbArticlesGroup.SpiderDoNotLoadImages = articlesGroup.SpiderDoNotLoadImages;
                dbArticlesGroup.BatchSize = articlesGroup.BatchSize;
                dbArticlesGroup.BatchDelayInSeconds = articlesGroup.BatchDelayInSeconds;
                dbArticlesGroup.DownloadFileDelayInMillseconds = articlesGroup.DownloadFileDelayInMillseconds;
                dbArticlesGroup.WaitMilliseconds = articlesGroup.WaitMilliseconds;

                dbArticlesGroup.TwitterConsumerKey = articlesGroup.TwitterConsumerKey;
                dbArticlesGroup.TwitterConsumerSecret = articlesGroup.TwitterConsumerSecret;
                dbArticlesGroup.TwitterAccessToken = articlesGroup.TwitterAccessToken;
                dbArticlesGroup.TwitterAccessTokenSecret = articlesGroup.TwitterAccessTokenSecret;

                dbArticlesGroup.LinksFileId = articlesGroup.LinksFileId;

                context.ValidateAndSave();
            }
        }

        public void DeleteArticlesGroup(int articlesGroupId)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var articlesGroup = context
                    .Set<DbArticlesGroup>()
                    .Include(x => x.ParsedArticles)
                    .FindFirst<DbArticlesGroup, SiteStructureEditingException>(
                    x => x.Id == articlesGroupId && x.UserId == currentUser.Id, $"ArticlesGroup with id {articlesGroupId} not found.");

                context.Set<DbArticlesGroup>().Remove(articlesGroup);
                context.ValidateAndSave();
            }
        }

        public AdminSite[] GetAllSites()
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbSite>()
                    .OrderBy(x => x.UserId)
                    .ExecuteAndConvert(x => x.Map<DbSite, AdminSite>());
                return result;
            }
        }

        public Site[] GetAvailableSites()
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                IQueryable<DbSite> query = context.Set<DbSite>();

                if (!currentUser.Roles.HasRole(Role.Admin))
                {
                    query = query.Where(x => x.UserId == currentUser.Id);
                }

                var result = query.ExecuteAndConvert(x => x.Map<DbSite, Site>());
                return result;
            }
        }

        public Site[] GetViewerAvailableSites()
        {
            using (var context = Repository.GetContext())
            {
                var result = context
                    .Set<DbSite>()
                    .Where(x => x.User.NameForViewers != null && x.IsEnabled)
                    .ExecuteAndConvert(x => x.Map<DbSite, Site>());
                return result;
            }
        }

        public Item[] GetViewerAvailableSites(int sourceUserId)
        {
            var currentUser = UserService.GetCurrentUser();
            if (currentUser == null || currentUser.Roles.HasOnlyRole(Role.Viewer))
            {
                return new Item[0];
            }

            using (var context = Repository.GetContext())
            {
                var result = context
                    .Set<DbSite>()
                    .Where(x => x.UserId == sourceUserId && x.User.NameForViewers != null && x.IsEnabled)
                    .Select(x => new Item
                    {
                        Id = x.Id,
                        Name = x.Name,
                    })
                    .Execute();
                return result;
            }
        }

        public Site[] GetEnabledSites()
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbSite>()
                    .Where(x => x.IsEnabled)
                    .ExecuteAndConvert(x => x.Map<DbSite, Site>());
                return result;
            }
        }

        public PagingResult<Site> GetAvailableSites<T>(RequestFilters<T> filters)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var result = context
                    .Set<DbSite>()
                    .Where(x => x.UserId == currentUser.Id)
                    .ApplyFilters(filters, EntityMapper.Map<DbSite, Site>);
                return result;
            }
        }

        public ArticlesGroupWithSiteUrl GetArticlesGroupById(int articlesGroupId)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                // TODO: Rewrite using UserId as Foreign Key for all DbEntities
                var result = context.Set<DbArticlesGroup>()
                                    .Include(x => x.Site)
                                    .FirstOrDefault(x => x.UserId == currentUser.Id && x.Id == articlesGroupId);
                return result.Map<DbArticlesGroup, ArticlesGroupWithSiteUrl>();
            }
        }

        public ArticlesGroup[] GetArticleGroupsBySite(int siteId)
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbArticlesGroup>()
                                    .Where(x => x.SiteId == siteId)
                                    .ExecuteAndConvert(x => x.Map<DbArticlesGroup, ArticlesGroup>());
                return result;
            }
        }

        public PagingResult<ArticlesGroup> GetArticleGroupsBySite<T>(int siteId, RequestFilters<T> filters)
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbArticlesGroup>()
                                    .Include(x => x.LinksFile)
                                    .Where(x => x.SiteId == siteId)
                                    .ApplyFilters(filters, EntityMapper.Map<DbArticlesGroup, ArticlesGroup>);
                return result;
            }
        }

        public async Task<Item[]> GetArticleGroupItemBySite(int siteId)
        {
            using (var context = Repository.GetContext())
            {
                return await context.Set<DbArticlesGroup>()
                    .Where(x => x.SiteId == siteId)
                    .Select(x => new Item
                    {
                        Id = x.Id,
                        Name = x.InternalName,
                    })
                    .ExecuteAsync();
            }
        }
    }
}
