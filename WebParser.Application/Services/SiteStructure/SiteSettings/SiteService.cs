﻿using System.Data.Entity;
using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Mappers;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.Common.Mappers;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.SiteStructure.SiteSettings
{
    internal class SiteService : ISiteService
    {
        private IRepository Repository { get; }

        private IUserService UserService { get; }

        public SiteService(
            IRepository repository,
            IUserService userService)
        {
            Repository = repository;
            UserService = userService;
        }

        public Site GetSite(int siteId)
        {
            using (var context = Repository.GetContext())
            {
                var site = context.Set<DbSite>()
                    .Include(x => x.User)
                    .Include(x => x.User.City)
                    .Include(x => x.User.City.Country)
                    .FirstOrDefault(x => x.Id == siteId);
                return site.Map<DbSite, Site>();
            }
        }

        public void StartSiteParsing(int siteId)
        {
            var currentUser = UserService.GetCurrentUser();
            using (var context = Repository.GetContext())
            {
                var site = context.FindFirst<DbSite, ParsingException>(x => x.Id == siteId, $"Site with id {siteId} not found");
                if (!site.OneTimeExport || site.IsInParsing || !site.IsEnabled)
                {
                    throw new ParsingException($"Site with id {siteId} in not valid parsing state. IsInParsing == {site.IsInParsing}");
                }

                if (!currentUser.Roles.HasRole(Role.Admin) && currentUser.Id != site.UserId)
                {
                    throw new ParsingException($"Wrong user starts to parsing Site with id {siteId}.");
                }

                site.NeedStartOneTimeParsing = true;
                context.ValidateAndSave();
            }
        }

        public void SetSiteInParsingState(int siteId)
        {
            using (var context = Repository.GetContext())
            {
                var site = context.FindFirst<DbSite, ParsingException>(x => x.Id == siteId, $"Site with id {siteId} not found");
                
                site.NeedStartOneTimeParsing = false;
                site.IsInParsing = true;
                context.ValidateAndSave();
            }
        }

        public void ResetSiteParsingState(int siteId)
        {
            using (var context = Repository.GetContext())
            {
                var site = context.FindFirst<DbSite, ParsingException>(x => x.Id == siteId, $"Site with id {siteId} not found");

                site.NeedStartOneTimeParsing = false;
                site.IsInParsing = false;
                context.ValidateAndSave();
            }
        }

        public ParsedSiteDetails[] GetDailyParsedSites()
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbSite>()
                    .Where(x => x.IsEnabled && !x.OneTimeExport)
                    .ExecuteAndConvert(x => x.Map<DbSite, ParsedSiteDetails>());
                return result;
            }
        }

        public ParsedSiteDetails[] GetOneTimeParsedSites()
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbSite>()
                    .Where(x => x.IsEnabled && x.OneTimeExport && x.NeedStartOneTimeParsing)
                    .ExecuteAndConvert(x => x.Map<DbSite, ParsedSiteDetails>());
                return result;
            }
        }
    }
}
