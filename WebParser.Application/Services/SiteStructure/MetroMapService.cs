﻿using AutoMapper;
using System;
using System.Linq;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Mappers;
using WebParser.Application.Services.Repository;
using WebParser.Common.Extensions;
using WebParser.Common.MetroStations;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.SiteStructure
{
    internal class MetroMapService : IMetroMapService
    {
        private IMetroRepositoryService MetroRepositoryService { get; }

        private ICityService CityService { get; }

        private readonly Lazy<MetroStation[]> AllMetroStations;

        public MetroMapService(IMetroRepositoryService metroRepositoryService, ICityService cityService)
        {
            MetroRepositoryService = metroRepositoryService;
            CityService = cityService;

            Mapper.CreateMap<string, MetroStation[]>().ConstructUsing(ids => GetStationsByJsonIds(ids));
            Mapper.CreateMap<string, MetroStation>().ConstructUsing(ids => GetStationByJsonIds(ids));

            AllMetroStations = new Lazy<MetroStation[]>(ReadStations);
        }

        public MetroStation[] GetStations(int? cityId)
        {
            return cityId.HasValue
                ? AllMetroStations.Value
                    .Where(x => x.CityId == cityId)
                    .OrderBy(x => x.Line.Id)
                    .ThenBy(x => x.Id)
                    .ToArray()
                : new MetroStation[] { };
        }

        public MetroStation[] GetStationsByPublisherId(int publisherId)
        {
            var city = CityService.GetUserCity(publisherId);
            return GetStations(city?.Id);
        }

        private MetroStation[] ReadStations()
        {
            return MetroRepositoryService.ReadStations().Select(x => x.Map<DbMetroStation, MetroStation>()).ToArray();
        }

        public MetroStation[] GetStationsByIds(string[] ids)
        {
            var stations = AllMetroStations.Value.ToDictionary(x => x.Id, x => x);

            return ids == null
                ? new MetroStation[0]
                : ids.Select(x => stations.GetValueOrDefault(x.ToId())).Where(x => x != null).ToArray();
        }

        public MetroStation GetStationByJsonIds(string jsonStationIds)
        {
            return GetStationsByJsonIds(jsonStationIds).FirstOrDefault();
        }

        public MetroStation[] GetStationsByJsonIds(string jsonStationIds)
        {
            return GetStationsByIds(jsonStationIds.Deserialize<string[]>());
        }

        public MetroStation FindMetroStation(int? cityId, string name)
        {
            var stations = GetStations(cityId);
            return ToMetroStation(name, stations);
        }

        public MetroStation[] ExtractMetroStations(int? cityId, ParsedValue parsedValue)
        {
            var stations = GetStations(cityId);
            var result = parsedValue
                .FormatValues(EntityMapper.DateTimeFormat)
                .Select(x => ToMetroStation(x, stations))
                .Where(x => x != null)
                .ToArray();
            return result;
        }

        private MetroStation ToMetroStation(string name, MetroStation[] stations)
        {
            return string.IsNullOrEmpty(name)
                ? null
                : stations.FirstOrDefault(y => y.NamesToSearch.Any(z => name.Contains(z, StringComparison.InvariantCultureIgnoreCase)));
        }
    }
}
