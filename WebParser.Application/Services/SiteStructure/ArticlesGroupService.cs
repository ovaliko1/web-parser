﻿using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.SiteStructure
{
    internal class ArticlesGroupService : IArticlesGroupService
    {
        private IRepository Repository { get; }

        private IUserService UserService { get; }

        public ArticlesGroupService(
            IRepository repository,
            IUserService userService)
        {
            Repository = repository;
            UserService = userService;
        }

        public ViewerSourceUser[] GetAvailableForViewerSourceUsers()
        {
            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbArticlesGroup>()
                    .Where(x => x.User.NameForViewers != null)
                    .GroupBy(x => new
                    {
                        x.User.Id,
                        x.User.NameForViewers,
                        x.User.CityId,
                    })
                    .Select(x => new
                    {
                        Id = x.Key.Id,
                        Name = x.Key.NameForViewers,
                        CityId = x.Key.CityId,
                        ArticleGroups = x.Select(y => new
                        {
                            Id = y.Id,
                            Name = y.PublicName,
                        })
                    })
                    .Execute()
                    .ToArray(x => new ViewerSourceUser
                    {
                        Id = x.Id,
                        Name = x.Name,
                        CityId = x.CityId,
                        ArticleGroups = x.ArticleGroups
                            .GroupBy(y => y.Name)
                            .ToArray(y => new ViewerArticleGroup
                            {
                                Ids = y.ToArray(z => z.Id),
                                Name = y.Key,
                            }),
                    });

                return result;
            } 
        }

        public int[] GetArticleGroupIds(int cityId, string articleGroupInternalNames)
        {
            var names = articleGroupInternalNames?.Split(',') ?? new string[0];
            if (names.Length == 0)
            {
                return new int[0];
            }

            using (var context = Repository.GetContext())
            {
                var result = context.Set<DbArticlesGroup>()
                    .Where(x =>
                        x.User.NameForViewers != null &&
                        x.User.CityId == cityId &&
                        names.Contains(x.InternalName))
                    .Select(x => x.Id)
                    .Execute();
                return result;
            }
        }
    }
}
