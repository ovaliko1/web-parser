﻿namespace WebParser.Application.Services.SiteStructure.JsonEntities
{
    public class JsonDateTimeParsingRule
    {
        public string CultureInfo { get; set; }

        public string DateFormat { get; set; }
    }
}