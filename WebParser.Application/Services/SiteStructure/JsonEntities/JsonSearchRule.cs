﻿using WebParser.Application.DomainEntities;

namespace WebParser.Application.Services.SiteStructure.JsonEntities
{
    public class JsonSearchRule : JsonBaseEntity
    {
        public int ActionType { get; set; }

        public int? WaitMilseconds { get; set; }

        public int SearchType { get; set; }

        public JsonFoundValue FoundValue { get; set; }

        public bool IsImage { get; set; }

        public bool IsDateTime { get; set; }

        public bool DoNotSaveToInternalStorage { get; set; }

        public JsonDateTimeParsingRule DateTimeParsingRule { get; set; }

        public string FieldName { get; set; }

        public string Selectors { get; set; }

        public string ReplaceRegexp { get; set; }

        public string BackgroundColor { get; set; }

        public string FilterFunction { get; set; }

        public bool ShowOnVoteForm { get; set; }

        public bool HideOnVkPost { get; set; }

        public int ParsedKeyTypeId { get; set; }

        protected override int GetVersion()
        {
            return 1;
        }
    }
}
