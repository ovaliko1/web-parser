﻿using WebParser.Application.DomainEntities;

namespace WebParser.Application.Services.SiteStructure.JsonEntities
{
    public class JsonSearchRules : JsonBaseEntity
    {
        public JsonSearchRule[] Rules { get; set; }

        protected override int GetVersion()
        {
            return 1;
        }
    }
}
