﻿namespace WebParser.Application.Services.SiteStructure.JsonEntities
{
    public class JsonFoundValue
    {
        public int FoundValueType { get; set; }

        public string AttrName { get; set; }
    }
}