﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.MarkingJob;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Common;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.MarkingJob
{
    internal class MarkingJobService : IMarkingJobService
    {
        private const int WhiteListArticleCount = 1000;
        private const int BlackListArticleCount = 1000;

        private IDateTimeService DateTimeService { get; }

        private IRepository Repository { get; }

        private IParsingLogger ParsingLogger { get; }

        public MarkingJobService(
            IDateTimeService dateTimeService,
            IRepository repository,
            IParsingLogger parsingLogger)
        {
            DateTimeService = dateTimeService;
            Repository = repository;
            ParsingLogger = parsingLogger;
        }

        public JobResult RunBlackListArticleMarking()
        {
            return RunMarking<DbBlackListItem>(
                BlackListArticleCount,
                "BlackListArticleMarkingJob",
                (x, y) => x.BlackListItemId = y);
        }


        public JobResult RunWhiteListArticleMarking()
        {
            return RunMarking<DbWhiteListItem>(
                WhiteListArticleCount,
                "WhiteListArticleMarkingJob",
                (x, y) => x.WhiteListItemId = y);
        }

        private JobResult RunMarking<T>(int count, string jobName ,Action<DbParsedArticle, int> markAction)
            where T : class, IDbStainedListItem
        {
            using (var context = Repository.GetContext())
            {
                var totalCount = 0;
                var markedArticleCount = 0;

                var from = DateTimeService.GetCurrentDateTimeUtc().AddDays(-1).Date;

                var newArticles = context
                    .Set<DbParsedArticle>()
                    .Where(
                        x => x.CreatedDate > from &&
                        x.WhiteListItemId == null &&
                        x.IdentityConfidencePercentage > 0)
                    .Take(count)
                    .Execute();

                if (newArticles.Length == 0)
                {
                    return GetMarkingResult(0, 0);
                }

                totalCount = newArticles.Length;

                var allIdentities = newArticles
                    .GroupBy(x => x.Identity)
                    .ToDictionary(x => x.Key, x => x.ToArray())
                    .Keys
                    .ToArray();

                var allStainedListItems = context
                    .Set<T>()
                    .Where(x => allIdentities.Contains(x.Identity))
                    .AsNoTracking()
                    .Execute();

                var articlesToUpdateResult = new List<DbParsedArticle>(count);
                var articlesBySite = newArticles.GroupBy(x => new
                {
                    x.SiteId,
                    x.UserId,
                });

                foreach (var articles in articlesBySite)
                {
                    var identities = articles
                        .GroupBy(x => x.Identity)
                        .ToDictionary(x => x.Key, x => x.ToArray());

                    var stainedListItems = allStainedListItems
                        .Where(x => identities.Keys.Contains(x.Identity))
                        .ToArray();

                    int markedArticleCountPerSite = 0;
                    foreach (var item in stainedListItems)
                    {
                        var articlesToUpdate = identities[item.Identity]
                            .Where(
                                x =>
                                    x.IdentityConfidencePercentage > ArticleIdentityHelper.SufficientConfidencePercentage ||
                                    x.SiteId == item.SiteId)
                            .ToList();

                        articlesToUpdate.ForEach(x => markAction(x, item.Id));

                        articlesToUpdateResult.AddRange(articlesToUpdate);
                        markedArticleCount += articlesToUpdate.Count;
                        markedArticleCountPerSite += articlesToUpdate.Count;
                    }

                    ParsingLogger.Log(articles.Key.SiteId, articles.Key.UserId, ParsingLogStatus.Success, $"{jobName} Marked {markedArticleCountPerSite}/{articles.Count()} articles");
                }

                if (articlesToUpdateResult.Count > 0)
                {
                    context.ValidateAndSave();
                }

                return GetMarkingResult(markedArticleCount, totalCount);
            }
        }

        private static JobResult GetMarkingResult(int markedArticleCount, int totalCount)
        {
            return new JobResult
            {
                Success = true,
                Log = $"Marked {markedArticleCount}/{totalCount} articles",
            };
        }
    }
}
