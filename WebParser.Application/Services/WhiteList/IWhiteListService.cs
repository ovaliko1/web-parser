﻿using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;

namespace WebParser.Application.Services.WhiteList
{
    internal interface IWhiteListService
    {
        int[] SaveWhiteListItems(ArticleIdentity[] articleIdentities, int userId, int siteId);
    }
}
