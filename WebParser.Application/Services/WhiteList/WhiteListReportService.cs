﻿using System;
using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.WhiteList;
using WebParser.Application.Interfaces.Services.WhiteList.Entities;
using WebParser.Application.Mappers;
using WebParser.Application.Services.WhiteList.Entities;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.WhiteList
{
    internal class WhiteListReportService : IWhiteListReportService
    {
        private IRepository Repository { get; }

        private IUserService UserService { get; }

        private IDateTimeService DateTimeService { get; }

        public WhiteListReportService(
            IRepository repository,
            IUserService userService,
            IDateTimeService dateTimeService)
        {
            Repository = repository;
            UserService = userService;
            DateTimeService = dateTimeService;
        }

        public PagingResult<WhiteListItem> ReadWhiteListItems<T>(RequestFilters<T> filters)
        {
            using (var context = Repository.GetContext())
            {
                var result = context
                    .Set<DbWhiteListItem>()
                    .Select(x => new WhiteListItem
                    {
                        Id = x.Id,
                        CreatedDate = x.CreatedDate,
                        Identity = x.Identity,
                        IdentityConfidencePercentage = x.IdentityConfidencePercentage,
                        SiteId = x.SiteId,
                        ArticleSiteId = x.ArticleSiteId,
                        SourceArticleId = x.ArticleId,
                        CreatedBy = x.CreatedById,
                        Hits = x.ParsedArticles.Count(),
                    })
                    .ApplyFilters(filters, (WhiteListItem x) => x);
                return result;
            }
        }

        public PagingResult<HitedWhiteArticle> ReadHitedArticles<T>(RequestFilters<T> filters, int itemId)
        {
            using (var context = Repository.GetContext())
            {
                var result = context
                    .Set<DbParsedArticle>()
                    .Where(x => x.WhiteListItemId == itemId)
                    .Select(x => new HitedArticleProjection
                    {
                        Id = x.Id,
                        CreatedDate = x.CreatedDate,
                        SiteId = x.SiteId,
                        ArticleGroup = x.ArticlesGroup.InternalName,
                        DuplicateGroupId = x.DuplicateGroupId,
                        Identity = x.Identity,
                        IdentityConfidencePercentage = x.IdentityConfidencePercentage,
                        IsDuplicate = x.IsDuplicate,
                        Price = x.PriceStr,
                        Metro = x.Metro,
                        MetroDistance = x.DistanceToMetroText,
                        MetroDuration = x.DurationToMetroText,
                        FormattedAddress = x.FormattedAddress,
                        Link = x.Link,
                    })
                    .ApplyFilters(filters, x => x.Map<HitedArticleProjection, HitedWhiteArticle>());
                return result;
            }
        }

        public void DeleteWhiteListItem(int id)
        {
            using (var context = Repository.GetContext())
            {
                var item = context
                    .FindFirst<DbWhiteListItem, WhiteListException>(
                        x => x.Id == id, $"DbWhiteListItem with id {id} not found.");

                var articles = context
                    .Set<DbParsedArticle>()
                    .Where(x => x.WhiteListItemId == id)
                    .Execute();

                articles.Foreach(x => x.WhiteListItemId = null);
                context.ValidateAndSave();

                context.Set<DbWhiteListItem>().Remove(item);
                context.ValidateAndSave();
            }
        }

        public void AddWhiteListItem(WhiteListItem item)
        {
            var currentUserId = UserService.GetCurrentUserId();
            if (!currentUserId.HasValue)
            {
                return;
            }

            using (var context = Repository.GetContext())
            {
                DbParsedArticle sourceArticle = null;
                if (item.SourceArticleId.HasValue)
                {
                    sourceArticle = context
                        .FindFirst<DbParsedArticle, WhiteListException>(x => x.Id == item.SourceArticleId, $"Article by Id: {item.SourceArticleId} not found.");
                }

                var newWhiteListItem = new DbWhiteListItem
                {
                    ArticleId = sourceArticle?.Id,
                    ArticleSiteId = sourceArticle?.SiteId,
                    ArticleUserId = sourceArticle?.UserId,
                    SiteId = item.SiteId,
                    CreatedById = currentUserId.Value,
                    CreatedDate = DateTimeService.GetCurrentDateTimeUtc(),
                    Identity = item.Identity,
                    IdentityConfidencePercentage = 100,
                };

                newWhiteListItem = context.Set<DbWhiteListItem>().Add(newWhiteListItem);
                context.ValidateAndSave();

                if (sourceArticle != null)
                {
                    sourceArticle.WhiteListItemId = newWhiteListItem.Id;
                    context.ValidateAndSave();
                }
            }
        }
    }
}
