﻿using System.Linq;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services.WhiteList
{
    internal class WhiteListService : IWhiteListService
    {
        private const int WhiteListArticleCount = 1000;

        private IDateTimeService DateTimeService { get; }

        private IRepository Repository { get; }

        private IParsingLogger ParsingLogger { get; }

        public WhiteListService(
            IDateTimeService dateTimeService,
            IRepository repository,
            IParsingLogger parsingLogger)
        {
            DateTimeService = dateTimeService;
            Repository = repository;
            ParsingLogger = parsingLogger;
        }

        public int[] SaveWhiteListItems(ArticleIdentity[] articleIdentities, int userId, int siteId)
        {
            if (articleIdentities.Length == 0)
            {
                throw new ParsingException($"articleIdentities is empty. SiteId {siteId}, UserId {userId}");
            }

            var savedItems = SaveWhiteListItemsCore(articleIdentities, userId, siteId);

            ParsingLogger.Log(
                siteId,
                userId,
                ParsingLogStatus.Success,
                $"SaveWhiteListItems completed. Saved {savedItems.Length}/{articleIdentities.Length} identities.");

            return savedItems;
        }

        private int[] SaveWhiteListItemsCore(ArticleIdentity[] articleIdentities, int userId, int siteId)
        {
            using (var context = Repository.GetContext())
            {
                var now = DateTimeService.GetCurrentDateTimeUtc();

                var nonEmptyIdentities = articleIdentities
                    .Where(
                        x =>
                            x.IdentityConfidencePercentage > 0 &&
                            !string.IsNullOrEmpty(x.Identity))
                    .ToArray();

                var itemsToSave = nonEmptyIdentities
                    .Where(x => !InWhiteList(context, x, siteId).Any())
                    .ToArray(x => new DbWhiteListItem
                    {
                        Identity = x.Identity.Cut(20),
                        IdentityConfidencePercentage = x.IdentityConfidencePercentage,
                        SiteId = siteId,
                        CreatedById = userId,
                        CreatedDate = now,
                    });

                if (itemsToSave.Length == 0)
                {
                    return nonEmptyIdentities
                        .SelectMany(x => InWhiteList(context, x, siteId)
                            .Select(y => y.Id)
                            .Execute())
                        .ToArray();
                }

                context.Set<DbWhiteListItem>().AddRange(itemsToSave);
                context.ValidateAndSave();

                return itemsToSave.ToArray(x => x.Id);
            }
        }

        private static IQueryable<DbWhiteListItem> InWhiteList(DataBaseContext context, ArticleIdentity articleIdentity, int siteId)
        {
            return context
                .Set<DbWhiteListItem>()
                .Where(ArticleIdentityHelper.GetInWhiteListPredicate(articleIdentity, siteId));
        }

        public int GetItemsCount()
        {
            using (var context = Repository.GetContext())
            {
                return context
                    .Set<DbWhiteListItem>()
                    .Count();
            }
        }
    }
}
