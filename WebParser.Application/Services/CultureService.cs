﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebParser.Application.Interfaces.Services.Culture;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Common;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Services
{
    internal class CultureService : ICultureService
    {
        private IRepository Repository { get; }

        public CultureService(IRepository repository)
        {
            Repository = repository;
        }

        public CultureInfo GetPriceCultureByString(string price)
        {
            if (price.IndexOf("€", StringComparison.InvariantCultureIgnoreCase) != -1 ||
                price.IndexOf("euro", StringComparison.InvariantCultureIgnoreCase) != -1 ||
                price.IndexOf("eur", StringComparison.InvariantCultureIgnoreCase) != -1)
            {
                return CultureInfo.CreateSpecificCulture("fr-FR");
            }

            if (price.IndexOf("лв", StringComparison.InvariantCultureIgnoreCase) != -1)
            {
                return CultureInfo.CreateSpecificCulture("bg-BG");
            }

            return CultureInfo.CreateSpecificCulture(CultureNames.Russian);
        }

        public async Task<CultureInfo> GetCultureBySiteAsync(Site site)
        {
            using (var context = Repository.GetContext())
            {
                var cultureSource = await context.Set<DbCity>()
                    .Where(x => x.Id == site.CityId)
                    .Select(x => new {x.Country.LanguageCode, x.Country.CountryRegionCode})
                    .FirstOrDefaultAsync();

                    var result = cultureSource != null
                        ? CultureInfo.CreateSpecificCulture($"{cultureSource.LanguageCode}-{cultureSource.CountryRegionCode}")
                        : CultureInfo.CreateSpecificCulture(CultureNames.Russian);

                return result;
            }
        }
    }
}
