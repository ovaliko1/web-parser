﻿using System;
using AutoMapper;
using Newtonsoft.Json;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.Services.Apartments.Entities;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Interfaces.Services.Parsing.AddressRecognizing;
using WebParser.Application.Interfaces.Services.Parsing.ArticleFieldsRecognizing;
using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.Reports.Entities;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Application.Services.Parsing.ParsedContent;
using WebParser.Application.Services.Parsing.ParsedContent.Entities;
using WebParser.Application.Services.Parsing.ParsedContent.JsonEntities;
using WebParser.Application.Services.Reports.Entities;
using WebParser.Application.Services.SiteStructure.JsonEntities;
using WebParser.Application.Services.SocialMedia.Entities;
using WebParser.Application.Services.UserManagement.JsonEntities;
using WebParser.Application.Services.WebClient;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.DataAccess.Entities;
using WebParser.Integration.Twitter.Entities;
using WebParser.Common.MetroStations;
using WebParser.Integration.Vk.Entities;

namespace WebParser.Application.Mappers
{
    internal static class EntityMapper
    {
        public static readonly string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        static EntityMapper()
        {
            Mapper.CreateMap<DbSite, Site>()
                  .ForMember(x => x.Cookies, opt => opt.MapFrom(x => x.Cookies.ConvertFromDbCookies()))
                  .ForMember(x => x.CityId, opt => opt.MapFrom(x => x.User.CityId))
                  .ForMember(x => x.RegionCode, opt => opt.MapFrom(x => x.User.City.Country.CountryRegionCode))
                  .ForMember(x => x.LanguageCode, opt => opt.MapFrom(x => x.User.City.Country.LanguageCode));

            Mapper.CreateMap<Site, DbSite>()
                  .ForMember(x => x.Cookies, opt => opt.MapFrom(x => x.Cookies.Serialize()));

            Mapper.CreateMap<DbSite, AdminSite>()
                  .ForMember(x => x.UserEmail, opt => opt.MapFrom(x => x.User.Email));

            Mapper.CreateMap<DbSite, ParsedSiteDetails>()
                  .ForMember(x => x.IsOneTimeExported, opt => opt.MapFrom(x => x.OneTimeExport))
                  .ForMember(x => x.SchedulerStartHours, opt => opt.MapFrom(x => x.SchedulerStartHours.Value))
                  .ForMember(x => x.SchedulerStartMinutes, opt => opt.MapFrom(x => x.SchedulerStartMinutes.Value))
                  .ForMember(x => x.JobType, opt => opt.MapFrom(x => (JobType)x.JobTypeId));

            Mapper.CreateMap<DbUser, UserDetails>()
                  .ForMember(x => x.Roles, opt => opt.MapFrom(x => x.Roles.ToArray(y => (Role)y.RoleId)));

            Mapper.CreateMap<JsonPaymentConfirmation, YandexPaymentConfirmation>();
            Mapper.CreateMap<DbUser, ViewerDetails>()
                  .ForMember(
                    x => x.PaymentConfirmations,
                    opt => opt.MapFrom(
                        x =>
                        x.SerializedPaymentConfirmations.Deserialize<JsonPaymentConfirmation[]>().ToArray(y => y.Map<JsonPaymentConfirmation, YandexPaymentConfirmation>())));

            Mapper.CreateMap<DbFile, FileInfo>()
                  .ForMember(x => x.FileName, opt => opt.MapFrom(x => x.OriginalFileName))
                  .ForMember(x => x.Extensions, opt => opt.MapFrom(x => x.FileExtension));

            Mapper.CreateMap<DbArticlesGroup, ArticlesGroup>()
                  .ForMember(x => x.Links, opt => opt.MapFrom(x => x.Links.ArrayFromDbString()))
                  .ForMember(x => x.LinksToArticlesSearchRule, opt => opt.MapFrom(x => x.LinksToArticlesSearchRule.Deserialize<JsonSearchRule>().JsonMap<JsonSearchRule, SearchRule>()))
                  .ForMember(x => x.ArticleSearchRules, opt => opt.MapFrom(x => x.ArticleSearchRules.Deserialize<JsonSearchRules>().ToSearchRules()))
                  .ForMember(x => x.LinksFile, opt => opt.MapFrom(x => x.LinksFile.Map<DbFile, FileInfo>()));

            Mapper.CreateMap<DbArticlesGroup, ArticlesGroupWithSiteUrl>()
                  .ForMember(x => x.Links, opt => opt.MapFrom(x => x.Links.ArrayFromDbString()))
                  .ForMember(x => x.LinksToArticlesSearchRule, opt => opt.MapFrom(x => x.LinksToArticlesSearchRule.Deserialize<JsonSearchRule>().JsonMap<JsonSearchRule, SearchRule>()))
                  .ForMember(x => x.ArticleSearchRules, opt => opt.MapFrom(x => x.ArticleSearchRules.Deserialize<JsonSearchRules>().ToSearchRules()))
                  .ForMember(x => x.SiteUrl, opt => opt.MapFrom(x => x.Site.Link));

            Mapper.CreateMap<ArticlesGroup, DbArticlesGroup>()
                  .ForMember(x => x.Links, opt => opt.MapFrom(x => x.Links.ArrayToDbString()))
                  .ForMember(x => x.LinksToArticlesSearchRule, opt => opt.MapFrom(x => x.LinksToArticlesSearchRule.JsonMap<SearchRule, JsonSearchRule>().Serialize()))
                  .ForMember(x => x.ArticleSearchRules, opt => opt.MapFrom(x => x.ArticleSearchRules.ToJsonSearchRules().Serialize()));

            Mapper.CreateMap<DbArticlesGroup, ArticlesGroupDetails>();

            Mapper.CreateMap<DbCity, City>();

            Mapper.CreateMap<DbFile, CreatedFile>();

            // TODO: Move to smth like internal entities mapping??? DateTimeFormat here???
            Mapper.CreateMap<ParsedValue, WebHookParsedValue>()
                  .ForMember(x => x.Value, opt => opt.MapFrom(x => x.FormatValues(DateTimeFormat).Serialize()))
                  .ForMember(x => x.ValueType, opt => opt.MapFrom(x => x.ParsedValueType.GetName()));

            Mapper.CreateMap<DbParsedArticle, ApiParsedArticle>()
                .ForMember(x => x.ParsedDate, opt => opt.MapFrom(x => x.CreatedDate))
                .ForMember(x => x.ParsedValues, opt => opt.MapFrom(x => x.ParsedContent.Deserialize<JsonParsedContent>().ToParsedValues()));

            Mapper.CreateMap<RecognizedPhone, ArticleIdentity>()
                .ForMember(x => x.Identity, opt => opt.MapFrom(x => x.FixedText))
                .ForMember(x => x.IdentityConfidencePercentage, opt => opt.MapFrom(x => x.MeanConfidencePercentage));

            Mapper.CreateMap<ParsedArticleItemProjection, ViewerParsedArticleItem>()
                .ForMember(x => x.MetroStations, opt => opt.MapFrom(x => x.Metro.Map<string, MetroStation[]>()));

            Mapper.CreateMap<ApartmentItemProjection, ApartmentItem>()
                .ForMember(x => x.ImagesPreview, opt => opt.MapFrom(x => x.ImagesPreview.ToImageInfo()))
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => new MetroItem(x.Metro.Map<string, MetroStation>().IfNotNull(y => y.Name), x.MetroDistance, x.MetroDuration)));

            Mapper.CreateMap<YandexPaymentConfirmation, JsonPaymentConfirmation>();

            Mapper.CreateMap<DbParsedArticle, RecognizedArticleFieldsData>()
                .ForMember(x => x.Price, opt => opt.MapFrom(x => x.Price))
                .ForMember(x => x.PriceStr, opt => opt.MapFrom(x => x.PriceStr))
                .ForMember(x => x.ArticleIdentity, opt => opt.MapFrom(x => x.ToArticleIdentity()))
                .ForMember(x => x.MetroStations, opt => opt.MapFrom(x => x.Metro.Map<string, MetroStation[]>()))
                .ForMember(x => x.ImageProcessingResult, opt => opt.MapFrom(x => new ImageProcessingResult(x.FullSizeImages.ToImageInfo(), x.ImagesPreview.ToImageInfo())))
                .ForMember(x => x.AddressInfo, opt => opt.MapFrom(x => new AddressInfo
                {
                    FormattedAddress = x.FormattedAddress,
                    Latitude = x.Latitude ?? 0,
                    Longitude = x.Longitude ?? 0,
                    NearestMetroStation = new NearestMetroStation
                    {
                        Name = x.Metro.Map<string, MetroStation>().IfNotNull(y => y.Name),
                        DistanceText = x.DistanceToMetroText,
                        DistanceValue = x.DistanceToMetroValue ?? -1,
                        DurationText = x.DurationToMetroText,
                        DurationValue = x.DurationToMetroValue ?? -1,
                    },
                }));

            Mapper.CreateMap<ArticlesGroupInfo, VkArticlesGroupInfo>();
            Mapper.CreateMap<ArticlesGroupInfo, TwitterArticlesGroupInfo>();

            Mapper.CreateMap<Services.BlackList.Entities.HitedArticleProjection, Interfaces.Services.BlackList.Entities.HitedArticle>()
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => x.Metro.Map<string, MetroStation>().IfNotNull(y => y.Name)));

            Mapper.CreateMap<Services.WhiteList.Entities.HitedArticleProjection, Interfaces.Services.WhiteList.Entities.HitedWhiteArticle>()
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => x.Metro.Map<string, MetroStation>().IfNotNull(y => y.Name)));

            Mapper.CreateMap<BlackAndWhiteArticleProjection, BlackAndWhiteArticle>()
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => x.Metro.Map<string, MetroStation>().IfNotNull(y => y.Name)));

            Mapper.CreateMap<DbMetroStation, MetroStation>()
                .ForMember(x => x.Line, opt => opt.MapFrom(x => x.MetroLine))
                .ForMember(x => x.NamesToSearch, opt => opt.MapFrom(x => x.SearchKeywords.Split(',')));
            Mapper.CreateMap<DbMetroLine, Line>();
        }

        public static TMap Map<TSource, TMap>(this TSource source)
        {
            return Mapper.Map<TSource, TMap>(source);
        }

        public static string ArrayToDbString(this string[] array)
        {
            if (array == null)
            {
                return string.Empty;
            }
            return string.Join("|", array);
        }

        public static string[] ArrayFromDbString(this string dbString)
        {
            if (string.IsNullOrEmpty(dbString))
            {
                return new string[0];
            }
            return dbString.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static Cookie[] ConvertFromDbCookies(this string cookies)
        {
            if (string.IsNullOrEmpty(cookies))
            {
                return new Cookie[0];
            }

            try
            {
                var result = cookies.Deserialize<Cookie[]>();
                return result;
            }
            catch (JsonReaderException)
            {
                return new Cookie[0];
            }
        }
    }
}