﻿using System.Globalization;
using System.Linq;
using AutoMapper;
using Newtonsoft.Json;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.DomainEntities.Search.ParsingRule;
using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Services.Parsing.ParsedContent.JsonEntities;
using WebParser.Application.Services.SiteStructure.JsonEntities;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Common.MetroStations;

namespace WebParser.Application.Mappers
{
    internal static class JsonEntityMapper
    {
        public static readonly string LongDateTimeFormat = "dd-MMMM-yyyy HH:mm:ss";

        static JsonEntityMapper()
        {
            Mapper.CreateMap<FoundValue, JsonFoundValue>();
            Mapper.CreateMap<JsonFoundValue, FoundValue>();

            Mapper.CreateMap<DateTimeParsingRule, JsonDateTimeParsingRule>()
                .ForMember(x => x.CultureInfo, opt => opt.MapFrom(x => x.CultureInfo.Name));
            Mapper.CreateMap<JsonDateTimeParsingRule, DateTimeParsingRule>()
                .ForMember(x => x.CultureInfo, opt => opt.MapFrom(x => new CultureInfo(x.CultureInfo)));

            Mapper.CreateMap<SearchRule, JsonSearchRule>();
            Mapper.CreateMap<JsonSearchRule, SearchRule>();

            Mapper.CreateMap<ImageInfo, JsonImageInfo>();
            Mapper.CreateMap<JsonImageInfo, ImageInfo>();
        }

        public static TMap JsonMap<TSource, TMap>(this TSource source)
        {
            return Mapper.Map<TSource, TMap>(source);
        }

        public static T Deserialize<T>(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return default(T);
            }
            return JsonConvert.DeserializeObject<T>(value, (JsonSerializerSettings)null);
        }

        public static string Serialize<T>(this T entity)
        {
            return JsonConvert.SerializeObject(entity);
        }

        public static string SerializeNonFormatting<T>(this T entity)
        {
            return JsonConvert.SerializeObject(entity, Formatting.None);
        }

        public static JsonSearchRules ToJsonSearchRules(this SearchRule[] rules)
        {
            return new JsonSearchRules
            {
                Rules = rules.ToArray(x => x.JsonMap<SearchRule, JsonSearchRule>()),
            };
        }

        public static SearchRule[] ToSearchRules(this JsonSearchRules rules)
        {
            return rules.Rules.ToArray(x => x.JsonMap<JsonSearchRule, SearchRule>());
        }

        public static JsonParsedContent ToJsonParsedContent(this ParsedValue[] source)
        {
            return source == null ? null : new JsonParsedContent
            {
                Fields = source.ToArray(x => new JsonParsedField
                {
                    Name = x.Name,
                    BackgroundColor = x.BackgroundColor,
                    HideOnVkPost = x.HideOnVkPost,
                    ParsedKeyTypeId = (int)x.ParsedKeyType,
                    ParsedValueTypeId = (int)x.ParsedValueType,
                    ShowOnVoteForm = x.ShowOnVoteForm,
                    Values = x.FormatValues(LongDateTimeFormat).ToArray(y => new JsonParsedFieldValue
                    {
                        Value = y,
                    }),
                    RecognizedPhones = x.RecognizedPhones.ToArray(y => new JsonRecognizedPhone
                    {
                        FixedText = y.FixedText,
                        RecognizedText = y.RecognizedText,
                        MeanConfidencePercentage = y.MeanConfidencePercentage,
                    })
                }),
            };
        }

        public static ParsedValue[] ToParsedValues(this JsonParsedContent source)
        {
            return source?.Fields.ToArray(x =>
            {
                var parsedValue = ParsedValue.Create(x.Values.ToArray(y => y.Value), (ParsedValueType)x.ParsedValueTypeId, LongDateTimeFormat);
                parsedValue.Name = x.Name;
                parsedValue.BackgroundColor = x.BackgroundColor;
                parsedValue.ShowOnVoteForm = x.ShowOnVoteForm;
                parsedValue.HideOnVkPost = x.HideOnVkPost;
                parsedValue.ParsedKeyType = (ParsedKeyType)x.ParsedKeyTypeId;
                parsedValue.RecognizedPhones = x.RecognizedPhones.ToArray(y => new RecognizedPhone
                {
                    FixedText = y.FixedText,
                    RecognizedText = y.RecognizedText,
                    MeanConfidencePercentage = y.MeanConfidencePercentage,
                });
                return parsedValue;
            });
        }

        public static string ToJsonImageInfo(this ImageInfo[] source)
        {
            return source.ToArray(x => x.Map<ImageInfo, JsonImageInfo>()).Serialize();
        }

        public static ImageInfo[] ToImageInfo(this string source)
        {
            return source.Deserialize<JsonImageInfo[]>().ToArray(x => x.Map<JsonImageInfo, ImageInfo>());
        }
    }
}