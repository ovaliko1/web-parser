﻿namespace WebParser.Application.DomainEntities
{
    public abstract class JsonBaseEntity
    {
        protected abstract int GetVersion();

        private int? version;

        public int Version
        {
            get { return version ?? GetVersion(); }
            set { version = value; }
        }
    }
}