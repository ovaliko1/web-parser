﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Common.Extensions;

namespace WebParser.Application.Helpers
{
    internal static class FiltersHelper
    {
        public static PagingResult<TProjection> ApplyFilters<TProjection, TFiltersProvider>(this IEnumerable<TProjection> items, RequestFilters<TFiltersProvider> filters)
        {
            return ApplyFilters(items, filters, (TProjection x) => x);
        }

        public static PagingResult<TEntity> ApplyFilters<TProjection, TEntity, TFiltersProvider>(this IEnumerable<TProjection> items, RequestFilters<TFiltersProvider> filters, Func<TProjection, TEntity> convert)
        {
            return ApplyFilters(items, filters, x =>
            {
                var result = x.ToArray(convert);
                return result;
            });
        }

        public static PagingResult<TEntity> ApplyFilters<TProjection, TEntity, TFiltersProvider>(this IEnumerable<TProjection> items, RequestFilters<TFiltersProvider> filters, Func<TProjection[], TEntity[]> convert)
        {
            var filteredResult = items.ToDataSourceResult(filters.FiltersProvider as DataSourceRequest);
            var pagingResult = new PagingResult<TEntity>
            {
                Items = convert(filteredResult.Data.Cast<TProjection>().ToArray()),
                TotalCount = filteredResult.Total,
            };
            return pagingResult;
        }
    }
}
