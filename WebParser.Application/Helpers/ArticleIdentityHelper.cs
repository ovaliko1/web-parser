﻿using System;
using System.Linq.Expressions;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Helpers
{
    internal static class ArticleIdentityHelper
    {
        public static readonly int SufficientConfidencePercentage = 74;

        public static Expression<Func<DbParsedArticle, bool>> GetDuplicationPredicate(ArticleIdentity articleIdentity, DateTime after, int siteId)
        {
            Expression<Func<DbParsedArticle, bool>> predicateTrue = x =>
                x.CreatedDate > after &&
                x.Identity == articleIdentity.Identity &&
                x.IdentityConfidencePercentage > SufficientConfidencePercentage;

            Expression<Func<DbParsedArticle, bool>> predicateFalse = x =>
                x.CreatedDate > after &&
                x.Identity == articleIdentity.Identity &&
                x.SiteId == siteId;

            return articleIdentity.IdentityConfidencePercentage > SufficientConfidencePercentage ? predicateTrue : predicateFalse;
        }

        public static Expression<Func<DbBlackListItem, bool>> GetInBlackListPredicate(ArticleIdentity articleIdentity, int? siteId)
        {
            Expression<Func<DbBlackListItem, bool>> predicateTrue = x =>
                x.Identity == articleIdentity.Identity &&
                x.IdentityConfidencePercentage > SufficientConfidencePercentage;

            var predicateFalse = siteId.HasValue
                    ? (Expression<Func<DbBlackListItem, bool>>)(
                        x =>
                            x.Identity == articleIdentity.Identity && (x.ArticleSiteId ?? x.SiteId) == siteId)
                    : (Expression<Func<DbBlackListItem, bool>>)(
                        x =>
                            x.Identity == articleIdentity.Identity);

            return articleIdentity.IdentityConfidencePercentage > SufficientConfidencePercentage ? predicateTrue : predicateFalse;
        }

        public static Expression<Func<DbWhiteListItem, bool>> GetInWhiteListPredicate(ArticleIdentity articleIdentity, int? siteId)
        {
            Expression<Func<DbWhiteListItem, bool>> predicateTrue = x =>
                x.Identity == articleIdentity.Identity &&
                x.IdentityConfidencePercentage > SufficientConfidencePercentage;

            var predicateFalse = siteId.HasValue
                    ? (Expression<Func<DbWhiteListItem, bool>>)(
                        x =>
                            x.Identity == articleIdentity.Identity && (x.ArticleSiteId ?? x.SiteId) == siteId)
                    : (Expression<Func<DbWhiteListItem, bool>>)(
                        x =>
                            x.Identity == articleIdentity.Identity);

            return articleIdentity.IdentityConfidencePercentage > SufficientConfidencePercentage ? predicateTrue : predicateFalse;
        }
    }
}
