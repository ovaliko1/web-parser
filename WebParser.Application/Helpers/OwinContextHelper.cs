﻿using System.Web;

using Microsoft.Owin;

namespace WebParser.Application.Helpers
{
    public static class OwinContextHelper
    {
        public static IOwinContext GetOwinContext()
        {
            HttpContextBase httpContextBase = new HttpContextWrapper(HttpContext.Current);
            var context = httpContextBase.GetOwinContext();
            return context;
        }
    }
}
