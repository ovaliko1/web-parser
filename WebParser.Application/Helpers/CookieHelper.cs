﻿using System;

namespace WebParser.Application.Helpers
{
    internal static class CookieHelper
    {
        public static string GetCookieDomain(this Uri siteUri)
        {
            var splitHostName = siteUri.Host.Split('.');

            var result = splitHostName.Length >= 2
                ? $".{splitHostName[splitHostName.Length - 2]}.{splitHostName[splitHostName.Length - 1]}"
                : $".{siteUri.Host}";

            return result;
        }
    }
}