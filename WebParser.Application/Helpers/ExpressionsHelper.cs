﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using WebParser.Common.Extensions;
using WebParser.DataAccess.Entities;

namespace WebParser.Application.Helpers
{
    internal static class ExpressionsHelper
    {
        /// <summary>
        /// Given a lambda expression that calls a method, returns the method info.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static MethodInfo GetMethodInfo<T>(Expression<Action<T>> expression)
        {
            return GetMethodInfo((LambdaExpression)expression);
        }

        /// <summary>
        /// Given a lambda expression that calls a method, returns the method info.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static MethodInfo GetMethodInfo<T, TResult>(Expression<Func<T, TResult>> expression)
        {
            return GetMethodInfo((LambdaExpression)expression);
        }

        /// <summary>
        /// Given a lambda expression that calls a method, returns the method info.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static MethodInfo GetMethodInfo(LambdaExpression expression)
        {
            MethodCallExpression outermostExpression = expression.Body as MethodCallExpression;

            if (outermostExpression == null)
            {
                throw new ArgumentException("Invalid Expression. Expression should consist of a Method call only.");
            }

            return outermostExpression.Method;
        }

        public static IQueryable<DbParsedArticle> SetMetroFilterPredicate(IQueryable<DbParsedArticle> query, int[] metroStationIds)
        {
            var firstId = string.Format(CultureInfo.InvariantCulture, "\"{0}\"", metroStationIds[0].ToStringInvariant());

            ParameterExpression pe = Expression.Parameter(typeof(DbParsedArticle), "x");
            MemberExpression me = Expression.Property(pe, "Metro");

            ConstantExpression constant = Expression.Constant(firstId, typeof(string));
            MethodCallExpression callExpr = Expression.Call(me, ExpressionsHelper.GetMethodInfo<string, bool>(x => x.Contains(string.Empty)), constant);

            Expression body = callExpr;

            for (int i = 1; i < metroStationIds.Length; i++)
            {
                var nextId = string.Format(CultureInfo.InvariantCulture, "\"{0}\"", metroStationIds[i].ToStringInvariant());
                ConstantExpression nextConstant = Expression.Constant(nextId, typeof(string));
                MethodCallExpression nextCallExpr = Expression.Call(me, ExpressionsHelper.GetMethodInfo<string, bool>(x => x.Contains(string.Empty)), nextConstant);

                body = Expression.Or(body, nextCallExpr);
            }

            var expressionTree = Expression.Lambda<Func<DbParsedArticle, bool>>(body, pe);
            query = query.Where(expressionTree);
            return query;
        }
    }
}
