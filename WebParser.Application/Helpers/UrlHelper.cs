﻿using System;

namespace WebParser.Application.Helpers
{
    internal static class UrlHelper
    {
        public static Uri ConstructFullUri(string baseUrl, string relativeUrl)
        {
            return ConstructFullUri(baseUrl, relativeUrl, false);                                                                                
        }

        public static Uri ConstructFullUri(string baseUrl, string relativeUrl, bool useBaseUrlHost)
        {
            if (string.IsNullOrEmpty(baseUrl) || string.IsNullOrEmpty(relativeUrl))
            {
                return null;
            }

            baseUrl = baseUrl.EndsWith("/") ? baseUrl : $"{baseUrl}/";
            Uri baseUri;
            if (!Uri.TryCreate(baseUrl, UriKind.Absolute, out baseUri))
            {
                return null;
            }

            if (relativeUrl.StartsWith("//"))
            {
                relativeUrl = $"{baseUri.Scheme}:{relativeUrl}";
                Uri result;

                return Uri.TryCreate(relativeUrl, UriKind.Absolute, out result) ? result : null;
            }

            relativeUrl = relativeUrl.StartsWith("/") ? relativeUrl.TrimStart('/') : relativeUrl;
            relativeUrl = relativeUrl.StartsWith(baseUri.LocalPath.TrimStart('/'))
                ? relativeUrl.Substring(baseUri.LocalPath.Length - 1)
                : relativeUrl;

            Uri uri;
            if (Uri.TryCreate(relativeUrl, UriKind.Absolute, out uri))
            {
                if (!useBaseUrlHost)
                {
                    return uri;
                }

                var resultUrl = $"{baseUri.Scheme}://{baseUri.Host}{uri.PathAndQuery}";
                var result = new Uri(resultUrl, UriKind.Absolute);
                return result;
            }

            if (Uri.TryCreate(baseUri, relativeUrl, out uri))
            {
                return uri;
            }

            return null;
        }

        public static string ToSeoUrl(this string url)
        {
            return Uri.EscapeUriString(url.ToLowerInvariant().TrimEnd('/'));
        }
    }
}
