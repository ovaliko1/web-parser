﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebParser.Common.Exceptions;
using WebParser.DataAccess;

namespace WebParser.Application.Helpers
{
    internal static class DbHelper
    {
        public static readonly string YouHaveArticlesGroupsForThisSiteMessage = "You have Articles Groups for this Site. Please remove them at first.";
        public static readonly string YouHaveCategoriesMessage = "You have Categories for this Site. Please remove them at first.";
        public static readonly string YouHaveArticlesGroupsForThisCategoryMessage = "You have Article Groups for this Category. Please remove them at first.";
        public static readonly string YouAlredyVotedMessage = "Вы уже оставляли отзыв на это объявление. Спасибо!";
        public static readonly string SiteCannotBeForBlackListMessage = "Site cannot be for BlackList job and for WhiteList job simultaneously." ;
        public static readonly string ContactAlreadyOpenedMessage = "Contact already opened.";
        public static readonly string OpeningBalanceMessage = "OpeningBalance should be more that zero.";

        private static readonly Dictionary<string, string> ConstraintsToCheck = new Dictionary<string, string>
        {
            { "FK_dbo.ArticlesGroup_dbo.Site_SiteId_UserId", YouHaveArticlesGroupsForThisSiteMessage },
            { "FK_dbo.Category_dbo.Site_SiteId_UserId", YouHaveCategoriesMessage },
            { "FK_dbo.ArticlesGroup_dbo.Category_CategoryId_UserId", YouHaveArticlesGroupsForThisCategoryMessage },
            { "PK_dbo.ArticleRatingVote", YouAlredyVotedMessage },
            { "CK_tbl_Site_ForBlackListForWhiteList", SiteCannotBeForBlackListMessage },
            { "UQ_tbl_OpenedContact_UserId_ArticleId_ArticleUserId", ContactAlreadyOpenedMessage },
            { "CK_tbl_AspNetUsers_OpeningBalance", OpeningBalanceMessage }
        };

        public static void ValidateAndSave(this DataBaseContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                do
                {
                    foreach (var key in ConstraintsToCheck.Keys)
                    {
                        if (ex.Message.Contains(key))
                        {
                            throw new DataBaseConstraintException(ConstraintsToCheck[key]);
                        }
                    }
                    ex = ex.InnerException;
                } while (ex != null);
                throw;
            }
        }

        public static T[] Execute<T>(this IQueryable<T> query)
        {
            if (query == null)
            {
                return new T[0];
            }

            var result = query.ToArray();
            return result;
        }

        public static async Task<T[]> ExecuteAsync<T>(this IQueryable<T> query)
        {
            if (query == null)
            {
                return new T[0];
            }

            var result = await query.ToArrayAsync();
            return result;
        }

        public static async Task<TOut[]> ExecuteAndConvertAsync<TOut, TIn>(this IQueryable<TIn> query, Func<TIn, TOut> convert)
        {
            var result = await query.ExecuteAsync();
            return result.Select(convert).ToArray();
        }

        public static TOut[] ExecuteAndConvert<TOut, TIn>(this IQueryable<TIn> query, Func<TIn, TOut> convert)
        {
            if (query == null)
            {
                return new TOut[0];
            }

            var result = query.ToArray().Select(convert);
            return result.ToArray();
        }

        public static TDbEntity FindFirst<TDbEntity, TException>(this DataBaseContext context, Expression<Func<TDbEntity, bool>> predicate, string notFoundMessage) 
            where TException : CoreException 
            where TDbEntity : class
        {
            var result = context.Set<TDbEntity>().FirstOrDefault(predicate);
            CheckFoundResult<TDbEntity, TException>(result, notFoundMessage);
            return result;
        }

        public static TDbEntity FindFirst<TDbEntity, TException>(this IQueryable<TDbEntity> query, Expression<Func<TDbEntity, bool>> predicate, string notFoundMessage)
            where TException : CoreException
            where TDbEntity : class
        {
            var result = query.FirstOrDefault(predicate);
            CheckFoundResult<TDbEntity, TException>(result, notFoundMessage);
            return result;
        }

        private static void CheckFoundResult<TDbEntity, TException>(TDbEntity result, string notFoundMessage)
            where TException : CoreException where TDbEntity : class
        {
            if (result == null)
            {
                var constructor = typeof (TException).GetConstructor(new[] {typeof (string)});
                var messageArg = Expression.Parameter(typeof (string), notFoundMessage);
                NewExpression newExp = Expression.New(constructor, new Expression[] {messageArg});
                LambdaExpression lambda = Expression.Lambda(typeof (ObjectActivator<TException>), newExp, messageArg);
                ObjectActivator<TException> compiled = (ObjectActivator<TException>) lambda.Compile();

                throw compiled(notFoundMessage);
            }
        }

        private delegate T ObjectActivator<T>(string message);
    }
}
