﻿{cookiesSection}

phantom.outputEncoding = 'utf8';

var links = [
{links}
];

if (!links.length) {
  phantom.exit();
}

loadPage(0);

var results = [];

function handleErrors(page, url, index, func) {
  try {
    func();
  } catch (ex) {
    console.log('ERROR: Failed during Page evaluation. Site Url: ' + url + ' Ex: ' + ex + 'ENDERROR');
    results[index] = {
      url: url,
      result: [],
      processes: true
    };

    checkEndOfProcess(page, index);
  } finally {
  }
}

function checkEndOfProcess(page, index) {

  //console.log('DEBUG: Render img: ' + index + '.png - url: ' + links[index] + 'ENDDEBUG');
  //page.render(index + '.png');

  page.close();
  if (index < links.length - 1) {

    setTimeout(function() {
      loadPage(++index);
    }, {timeout});

  } else {

    console.log('RESULT:' + JSON.stringify(results) + 'ENDRESULT');
    phantom.exit();

  }
}

function loadPage(index) {

  var url = links[index];
  var page = require('webpage').create();

  page.settings.userAgent = '{userAgent}';
  page.settings.loadImages = {loadImages};

  registerPageEvents(page);

  page.open(url, function (status) {
    handleErrors(page, url, index, function () {

      if (status !== 'success') {
        console.log('ERROR: Site with url: ' + url + ' can not be loaded. Response status: ' + status + 'ENDERROR');
        return;
      }

      var jQueryExist = page.evaluate(function () {
        return typeof jQuery !== 'undefined';
      });

      if (jQueryExist || page.injectJs('jquery-1.10.2.min.js')) {
        // ########################################################### Start Fetching #######################################################        
        console.log('DEBUG: Fetch started: ' + index + ' url: ' + url + 'ENDDEBUG');

        results[index] = {
          url: url,
          result: []
        };

        {script}
        // ###########################################################  End Fetching  ######################################################
      } else {
        console.log('ERROR: Site url: ' + url + '. Failed during jQuery injecting' + 'ENDERROR');
        results[index] = {
          url: url,
          result: [],
          processes: true
        };

        checkEndOfProcess(page, index);
      }
    });
  });
}

function registerPageEvents(newPage) {
  newPage.onError = function (msg, trace) {
    console.log('PAGEERROR: ' + msg);
    trace.forEach(function (item) {
      console.log('  ', item.file, ':', item.line);
    });

    console.log('ENDPAGEERROR');
  };

  newPage.onConsoleMessage = function (msg, lineNum, sourceId) {
    //console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in ' + sourceId + ')');
  };
}