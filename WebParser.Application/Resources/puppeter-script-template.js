﻿(async () => {

process.stdout.setEncoding('utf8');

var links = [
{links}
];

if (!links.length) {
  return;
}

const puppeteer = require('puppeteer');
const browser = await puppeteer.launch({
{proxyServerAddress}
  ignoreHTTPSErrors: true
});
const results = [];

for (let i = 0; i < links.length; i++) {
  await loadPage(i);
}

await completeProcess();

async function loadPage(index) {

  var url = links[index];
  var page = await browser.newPage();
  {cookiesSection}

  {proxyServerCredentials}

  await page.setUserAgent('{userAgent}');
  registerPageEvents(page);

  if(!(await page.goto(url, {
    waitUntil: 'load'
  }).catch(ex => {
    handlePageError(url, index, ex);
  }))){
    return;
  }

  await handleErrors(url, index, async () => {

    let jQueryExist = await page.evaluate(() => {
      return typeof jQuery !== 'undefined';
    });

    console.log('DEBUG: jQueryExist: ' + jQueryExist + 'ENDDEBUG');

    if (jQueryExist || (await page.addScriptTag({
      path: 'jquery-3.1.1.min.js'
    }))) {
      // ########################################################### Start Fetching #######################################################        
      console.log('DEBUG: Fetch started: ' + index + ' url: ' + url + 'ENDDEBUG');

      results[index] = {
        url: url,
        result: []
      };

      {script}
      // ###########################################################  End Fetching  ######################################################
    } else {
      console.log('ERROR: Site url: ' + url + '. Failed during jQuery injecting' + 'ENDERROR');
      results[index] = {
        url: url,
        result: [],
        processes: true
      };
    }

    await page.close();
  });
}

function registerPageEvents(newPage) {
  newPage.on('error', msg => {
    console.log('PAGEERROR: ' + msg.message + '\nstack: ' + msg.stack + 'ENDPAGEERROR');
  });

  newPage.on('console', msg => {
    //console.log('CONSOLE: ' + msg.message + '\nstack: ' + msg.stack + 'ENDCONSOLE');
  });
}

async function handleErrors(url, index, func) {
  try {
    await func();
  } catch (ex) {
    handlePageError(url, index, 'handleErrors: ' + ex);
  } finally {
  }
}

function handlePageError(url, index, ex){
  console.log('ERROR: Failed during Page evaluation. Site Url: ' + url + ' Ex: ' + ex + 'ENDERROR');
  results[index] = {
    url: url,
    result: [],
    processes: true
  };
}

async function completeProcess() {
  console.log('RESULT:' + JSON.stringify(results) + 'ENDRESULT');
  await browser.close();

  process.exit(0);
}

function sleep(milliseconds){
  return new Promise(resolve => {
    setTimeout(resolve, milliseconds);
  })
}

})();

