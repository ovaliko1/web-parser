﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("WebParser.Application")]
[assembly: Guid("935adc1b-2016-42a2-9b06-43b1fc36f702")]

[assembly: InternalsVisibleTo("WebParser.Configuration")]
[assembly: InternalsVisibleTo("WebParser.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
