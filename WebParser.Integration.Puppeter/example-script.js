(async () => {

process.stdout.setEncoding('utf8');

var links = [
'https://spb.move.ru/objects/sdaetsya_1-komnatnaya_kvartiry_ploschadyu_38_kvm_sankt-peterburg_ulica_kustodieva_d22_6805570062',
'https://spb.move.ru/objects/sdaetsya_2-komnatnaya_kvartiry_ploschadyu_56_kvm_sankt-peterburg_prospekt_nastavnikov_d45k1_6805570059',
'https://spb.move.ru/objects/sdaetsya_1-komnatnaya_kvartiry_ploschadyu_33_kvm_sankt-peterburg_ulica_sedova_d31_6805570060',
'https://spb.move.ru/objects/sdaetsya_1-komnatnaya_kvartiry_ploschadyu_46_kvm_sankt-peterburg_jeleznovodskaya_ulica_d32_6805570061',
'https://spb.move.ru/objects/sdaetsya_1-komnatnaya_kvartiry_ploschadyu_36_kvm_sankt-peterburg_prospekt_geroev_d24k3_6804331265',
'https://spb.move.ru/objects/sdaetsya_1-komnatnaya_kvartiry_ploschadyu_31_kvm_sankt-peterburg_ulica_krasuckogo_d3v_6805570058',
'https://spb.move.ru/objects/sdaetsya_1-komnatnaya_kvartiry_ploschadyu_35_kvm_sankt-peterburg_ulica_engelsa_d_6804260904',
'https://spb.move.ru/objects/sdaetsya_2-komnatnaya_kvartiry_ploschadyu_47_kvm_sankt-peterburg_5-y_predportovyy_proezd_d4k1_6803511643',
'https://spb.move.ru/objects/sdaetsya_2-komnatnaya_kvartiry_ploschadyu_54_kvm_sankt-peterburg_kupchinskaya_ulica_d19k1_6803511487',
'https://spb.move.ru/objects/sdaetsya_1-komnatnaya_kvartiry_ploschadyu_40_kvm_sankt-peterburg_prospekt_prosvescheniya_d78_6805570054'
];

if (!links.length) {
  return;
}

const puppeteer = require('puppeteer');
const browser = await puppeteer.launch({

  ignoreHTTPSErrors: true
});
const results = [];

for (let i = 0; i < links.length; i++) {
  await loadPage(i);
}

await completeProcess();

async function loadPage(index) {

  var url = links[index];
  var page = await browser.newPage();
  

  

  await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36');
  registerPageEvents(page);

  if(!(await page.goto(url, {
    waitUntil: 'load'
  }).catch(ex => {
    handlePageError(url, index, ex);
  }))){
    return;
  }

  await handleErrors(url, index, async () => {

    let jQueryExist = await page.evaluate(() => {
      return typeof jQuery !== 'undefined';
    });

    console.log('DEBUG: jQueryExist: ' + jQueryExist + 'ENDDEBUG');

    if (jQueryExist || (await page.addScriptTag({
      path: 'jquery-3.1.1.min.js'
    }))) {
      // ########################################################### Start Fetching #######################################################        
      console.log('DEBUG: Fetch started: ' + index + ' url: ' + url + 'ENDDEBUG');

      results[index] = {
        url: url,
        result: []
      };

      
        console.log('DEBUG: Wait Action: 3000ENDDEBUG');
        await sleep(3000);

        console.log('DEBUG: Click Action: .object-page__user-block .block-user__show-telephone_buttonENDDEBUG');
        await page.evaluate(function () {
          $('.object-page__user-block .block-user__show-telephone_button').click();
        });

        console.log('DEBUG: Wait Action: 1000ENDDEBUG');
        await sleep(1000);

        console.log('DEBUG: TagTextContent fetch action: Phone' + 'ENDDEBUG');
        results[index].result[results[index].result.length] = await page.evaluate(function () {
          return { name: 'Phone', values: $.map($('.object-page__user-block .block-user__show-telephone_number a'), function (value, i) { return (function(){ var val = $(value).text(); return val ? val.trim() : ''; })(); }) };
        });

        console.log('DEBUG: Wait Action: 3000ENDDEBUG');
        await sleep(3000);

        results[index].processes = true;

        console.log('DEBUG: Fetching end ENDDEBUG');

      // ###########################################################  End Fetching  ######################################################
    } else {
        console.log('ERROR: Site url: ' + url + '. Failed during jQuery injecting' + 'ENDERROR');
      results[index] = {
        url: url,
        result: [],
        processes: true
      };
    }

    await page.close();
  });
}

function registerPageEvents(newPage) {
  newPage.on('error', msg => {
    console.log('PAGEERROR: ' + msg.message + '\nstack: ' + msg.stack + 'ENDPAGEERROR');
  });

  newPage.on('console', msg => {
    //console.log('CONSOLE: ' + msg.message + '\nstack: ' + msg.stack + 'ENDCONSOLE');
  });
}

async function handleErrors(url, index, func) {
  try {
    await func();
  } catch (ex) {
    handlePageError(url, index, 'handleErrors: ' + ex);
  } finally {
  }
}

function handlePageError(url, index, ex){
  console.log('ERROR: Failed during Page evaluation. Site Url: ' + url + ' Ex: ' + ex + 'ENDERROR');
  results[index] = {
    url: url,
    result: [],
    processes: true
  };
}

async function completeProcess() {
  console.log('RESULT:' + JSON.stringify(results) + 'ENDRESULT');
  await browser.close();

  process.exit(0);
}

function sleep(milliseconds){
  return new Promise(resolve => {
    setTimeout(resolve, milliseconds);
  })
}

})();

