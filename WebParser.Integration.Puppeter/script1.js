(async () => {

process.stdout.setEncoding('utf8');

var links = [
'https://www.olx.bg/ad/apartament-za-noschuvki-2-tsentar-parking-CID368-ID7I1sf.html#ba8413b246'
];

if (!links.length) {
  return;
}

const puppeteer = require('puppeteer');
const browser = await puppeteer.launch({

  ignoreHTTPSErrors: true
});
const results = [];

for (let i = 0; i < links.length; i++) {
  await loadPage(i);
}

await completeProcess();

async function loadPage(index) {

  var url = links[index];
  var page = await browser.newPage();
  await page.setCookie({
                      'name': 'PHPSESSID',
                      'value': '1110nioio7b9a6qs882spvb55tb',
                      'domain': '.olx.bg'
                    });

  

  await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36');
  registerPageEvents(page);

  if(!(await page.goto(url, {
    waitUntil: 'load'
  }).catch(ex => {
    handlePageError(url, index, ex);
  }))){
    return;
  }

  await handleErrors(url, index, async () => {

    let jQueryExist = await page.evaluate(() => {
      return typeof jQuery !== 'undefined';
    });

    console.log('DEBUG: jQueryExist: ' + jQueryExist + 'ENDDEBUG');

    if ((await page.addScriptTag({
      path: 'jquery-3.1.1.min.js'
    }))) {
      // ########################################################### Start Fetching #######################################################        
      console.log('DEBUG: Fetch started: ' + index + ' url: ' + url + 'ENDDEBUG');

      results[index] = {
        url: url,
        result: []
      };

      
        console.log('DEBUG: TagTextContent fetch action: Объект' + 'ENDDEBUG');
        results[index].result[results[index].result.length] = await page.evaluate(function () {
          return { name: 'Объект', value: (function(){ var val = $('h1').text(); return val ? val.trim() : ''; })() };
        });

        console.log('DEBUG: Click Action: #contact_methods div.contact-buttonENDDEBUG');
        await page.evaluate(function () {
          $('#contact_methods div.contact-button').click();
        });

        console.log('DEBUG: Wait Action: 500ENDDEBUG');
        await sleep(500);

        console.log('DEBUG: TagTextContent fetch action: Телефон' + 'ENDDEBUG');
        results[index].result[results[index].result.length] = await page.evaluate(function () {
          return { name: 'Телефон', value: (function(){ var val = $('#contact_methods div.contact-button strong').text(); return val ? val.trim() : ''; })() };
        });

        results[index].processes = true;

        console.log('DEBUG: Fetching end ENDDEBUG');

      // ###########################################################  End Fetching  ######################################################
    } else {
      console.log('ERROR: Site url: ' + url + '. Failed during jQuery injecting' + 'ENDERROR');
      results[index] = {
        url: url,
        result: [],
        processes: true
      };
    }

    await page.close();
  });
}

function registerPageEvents(newPage) {
  newPage.on('error', msg => {
    console.log('PAGEERROR: ' + msg.message + '\nstack: ' + msg.stack + 'ENDPAGEERROR');
  });

  newPage.on('console', msg => {
    //console.log('CONSOLE: ' + msg.message + '\nstack: ' + msg.stack + 'ENDCONSOLE');
  });
}

async function handleErrors(url, index, func) {
  try {
    await func();
  } catch (ex) {
    handlePageError(url, index, 'handleErrors: ' + ex);
  } finally {
  }
}

function handlePageError(url, index, ex){
  console.log('ERROR: Failed during Page evaluation. Site Url: ' + url + ' Ex: ' + ex + 'ENDERROR');
  results[index] = {
    url: url,
    result: [],
    processes: true
  };
}

async function completeProcess() {
  console.log('RESULT:' + JSON.stringify(results) + 'ENDRESULT');
  await browser.close();

  process.exit(0);
}

function sleep(milliseconds){
  return new Promise(resolve => {
    setTimeout(resolve, milliseconds);
  })
}

})();

