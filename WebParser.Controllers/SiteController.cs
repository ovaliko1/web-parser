﻿using System.Web.Mvc;

using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.Services.Parsing;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.Site;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Helpers;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "User,Admin")]
    public class SiteController : ViewController
    {
        private ISiteStructureService SiteStructureService { get; }

        private ISiteService SiteService { get; }

        private IParsingService ParsingService { get; }

        public SiteController(
            ISiteStructureService siteStructureService,
            IParsingService parsingService,
            ISiteService siteService)
        {
            SiteStructureService = siteStructureService;
            ParsingService = parsingService;
            SiteService = siteService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetAvailableSites([DataSourceRequest]DataSourceRequest request)
        {
            var site = SiteStructureService.GetAvailableSites(request.ToRequestFilters());
            return ConvertToPagingResult(site, x => x.Map<Site, EditSiteModel>());
        }

        [HttpPost]
        [ValidateAction]
        public ActionResult CreateSite(CreateSiteModel model)
        {
            var site = model.Map<CreateSiteModel, Site>();
            var siteId = SiteStructureService.CreateSite(site);
            return Json(new { siteId });
        }

        [HttpPost]
        [ValidateAction]
        public void EditSite(EditSiteModel model)
        {
            var site = model.Map<EditSiteModel, Site>();
            SiteStructureService.EditSite(site);
        }

        [HttpPost]
        [ValidateAction]
        public void DeleteSite(int siteId)
        {
            SiteStructureService.DeleteSite(siteId);
        }

        [HttpPost]
        [ValidateAction]
        public ActionResult ForceRunParsing(int siteId)
        {
            SiteService.StartSiteParsing(siteId);  
            return Json("Success!");
        }
    }
}
