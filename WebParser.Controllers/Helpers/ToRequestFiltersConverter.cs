﻿using System;
using System.Collections.Generic;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.DomainEntities;

namespace WebParser.Controllers.Helpers
{
    internal static class ToRequestFiltersConverter
    {
        public static RequestFilters<DataSourceRequest> ToRequestFilters(this DataSourceRequest request)
        {
            return ToRequestFilters(request, null);
        }

        public static RequestFilters<DataSourceRequest> ToRequestFilters(this DataSourceRequest request, Action<IList<IFilterDescriptor>> modifyFilters)
        {
            modifyFilters?.Invoke(request.Filters);
            return new RequestFilters<DataSourceRequest>
            {
                FiltersProvider = request
            };
        }
    }
}