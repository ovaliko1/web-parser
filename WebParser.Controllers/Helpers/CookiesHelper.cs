﻿using System;
using System.Linq;
using System.Web;

namespace WebParser.Controllers.Helpers
{
    internal static class CookiesHelper
    {
        private static string GetCookie(string name)
        {
            return HttpContext.Current.Request.Cookies[name]?.Value;
        }

        private static void SaveCookie(string name, string value)
        {
            SaveCookie(name, value, null);
        }

        private static void SaveCookie(string name, string value, DateTime? expires)
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }

            var cookie = new HttpCookie(name)
            {
                Value = value,
            };
            if (expires.HasValue)
            {
                cookie.Expires = expires.Value;
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        private static void RemoveCookie(string name)
        {
            var httpContext = HttpContext.Current;
            if (httpContext.Request.Cookies.AllKeys.Contains(name))
            {
                var cookie = httpContext.Request.Cookies[name];
                cookie.Expires = DateTime.Now.AddDays(-1);
                httpContext.Response.Cookies.Add(cookie);
            }
        }
    }
}
