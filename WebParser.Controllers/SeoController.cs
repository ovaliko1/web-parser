﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebParser.Application.Interfaces.Services.Apartments;
using WebParser.Application.Interfaces.Services.Apartments.Entities;
using WebParser.Application.Interfaces.Services.Seo;
using WebParser.Application.Interfaces.Services.Seo.Entities;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Common;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.Apartments;
using WebParser.Controllers.ViewModels.Seo;

namespace WebParser.Controllers
{
    [Localize]
    public class SeoController : ViewController
    {
        private ISiteMapService SiteMapService { get; }

        private ISeoRoutingService SeoRoutingService { get; }

        private IArticlesGroupService ArticlesGroupService { get; }

        private IApartmentsService ApartmentsService { get; }

        private ICityService CityService { get; }

        public SeoController(
            ISiteMapService siteMapService,
            ISeoRoutingService seoRoutingService,
            IArticlesGroupService articlesGroupService,
            IApartmentsService apartmentsService,
            ICityService cityService)
        {
            SiteMapService = siteMapService;
            SeoRoutingService = seoRoutingService;
            ArticlesGroupService = articlesGroupService;
            ApartmentsService = apartmentsService;
            CityService = cityService;
        }

        public ActionResult Index()
        {
            var seoRoutingResult = HttpContext.Items["HTTPCONTEXT:SEOROUTING:RESULT"] as SeoRoutingResult;
            var details = SeoRoutingService.GetSeoDetails(HttpContext.Request.RawUrl, seoRoutingResult);
            var model = details.Map<SeoDetails, SeoDetailsViewModel>();

            if (seoRoutingResult?.CityId.HasValue ?? false)
            {
                var groupIds = ArticlesGroupService.GetArticleGroupIds(seoRoutingResult.CityId.Value, seoRoutingResult.ArticleGroupInternalName);
                var userId = CityService.GetUserIdByCity(seoRoutingResult.CityId.Value);

                var apartmentFilters = new ApartmentFilters
                {
                    CityId = userId ?? 0,
                    ArticleGroupIds = groupIds,
                    PriceFrom = seoRoutingResult.PriceFrom,
                    PriceTo = seoRoutingResult.PriceTo,
                    ShowWithEmptyPrice = true,
                    ShowWithEmptyMetro = true,
                    MetroStationIds = new int[0],
                    SourceIds = new int[0],
                };

                var apartments = ApartmentsService.ReadApartments(0, 10, apartmentFilters);
                model.Apartments = apartments.Map<ApartmentList, ApartmentsModel>();
                model.ApartmentFilters = apartmentFilters.Map<ApartmentFilters, SeoApartmentFilters>();
            }
            else
            {
                model.Apartments = new ApartmentsModel
                {
                    HasMore = false,
                    Items = new ApartmentItemModel[0],
                };
                model.ApartmentFilters = new SeoApartmentFilters
                {
                    ArticleGroupIds = string.Empty,
                    MetroStationIds = string.Empty,
                    SourceIds = string.Empty,
                };
            }

            return View(model);
        }

        public async Task<ActionResult> SitemapIndex(bool debug = false)
        {
            var xml = await SiteMapService.GetSiteMapIndex(debug);
            return Content(xml, "text/xml", Encoding.UTF8);
        }

        public ActionResult Sitemap(string articleGroup, string country, string city, bool debug = false)
        {
            var xml = SiteMapService.GetSiteMap(articleGroup, country, city, debug);
            return Content(xml, "text/xml", Encoding.UTF8);
        }
    }
}