﻿using System.Net;
using System.Web.Mvc;

namespace WebParser.Controllers
{
    public class ErrorController : ViewController
    {
        public ErrorController()
        {
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult BadRequest()
        {
            return View();
        }

        public ActionResult NotFound()
        {
            return View();
        }

        public static string GetActionName(HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.BadRequest:
                    return nameof(BadRequest);

                case HttpStatusCode.NotFound:
                    return nameof(NotFound);

                default:
                    return nameof(Error);
            }
        }
    }
}
