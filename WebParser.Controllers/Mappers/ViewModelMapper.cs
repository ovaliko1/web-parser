﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Channels;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using log4net;
using Newtonsoft.Json;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.DomainEntities.Search.ParsingRule;
using WebParser.Application.Interfaces.Services.Apartments.Entities;
using WebParser.Application.Interfaces.Services.ArticleRating;
using WebParser.Application.Interfaces.Services.BlackList.Entities;
using WebParser.Application.Interfaces.Services.Parsing.AddressRecognizing;
using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.Parsing.ParsingProgress;
using WebParser.Application.Interfaces.Services.Reports.Entities;
using WebParser.Application.Interfaces.Services.Seo.Entities;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Application.Interfaces.Services.WhiteList.Entities;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.Common.MetroStations;
using WebParser.Controllers.ViewModels.Account;
using WebParser.Controllers.ViewModels.Apartments;
using WebParser.Controllers.ViewModels.ArticleGroup;
using WebParser.Controllers.ViewModels.BlackAndWhiteArticlesReport;
using WebParser.Controllers.ViewModels.BlackListReport;
using WebParser.Controllers.ViewModels.Dashboard;
using WebParser.Controllers.ViewModels.ParsedContent;
using WebParser.Controllers.ViewModels.Seo;
using WebParser.Controllers.ViewModels.Shared;
using WebParser.Controllers.ViewModels.Site;
using WebParser.Controllers.ViewModels.User;
using WebParser.Controllers.ViewModels.Vote;
using WebParser.Controllers.ViewModels.WhiteListReport;
using ArticlesGroupItemViewModel = WebParser.Controllers.ViewModels.ArticleGroup.ArticlesGroupItemViewModel;
using HitedArticle = WebParser.Application.Interfaces.Services.BlackList.Entities.HitedArticle;
using HitedArticleViewModel = WebParser.Controllers.ViewModels.BlackListReport.HitedArticleViewModel;

namespace WebParser.Controllers.Mappers
{
    public static class ViewModelMapper
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static readonly string DateTimeFormat = "dd-MMM-yyyy HH:mm";

        public static readonly string NumericDateTimeFormat = "dd-MM-yyyy HH:mm";

        public static readonly string TimeFormat = "HH:mm";

        private static readonly string ArraySeparator = "\n";

        static ViewModelMapper()
        {
            const string timeFormatForTimeStempToString = @"hh\:mm";

            Mapper.CreateMap<Item, ItemViewModel>();
            
            Mapper.CreateMap<Site, SelectListItem>()
                  .ForMember(x => x.Text, opt => opt.MapFrom(x => x.Name))
                  .ForMember(x => x.Value, opt => opt.MapFrom(x => x.Id));

            Mapper.CreateMap<Cookie, CookieViewModel>();
            Mapper.CreateMap<CookieViewModel, Cookie>();

            Mapper.CreateMap<Site, EditSiteModel>()
                  .ForMember(x => x.CookieName, opt => opt.MapFrom(x => x.Cookies.IfNotNull(y => y.FirstOrDefault().IfNotNull(z => z.Name))))
                  .ForMember(x => x.CookieValue, opt => opt.MapFrom(x => x.Cookies.IfNotNull(y => y.FirstOrDefault().IfNotNull(z => z.Value))))
                  .ForMember(x => x.SchedulerStartTime, opt => opt.MapFrom(x => x.GetStartTime()))
                  .ForMember(x => x.SchedulerEndTime, opt => opt.MapFrom(x => x.GetEndTime()))
                  .ForMember(x => x.SchedulerStartTimeStr, opt => opt.MapFrom(x => x.GetStartTime().ToStringInvariant(timeFormatForTimeStempToString)))
                  .ForMember(x => x.SchedulerEndTimeStr, opt => opt.MapFrom(x => x.GetEndTime().ToStringInvariant(timeFormatForTimeStempToString)));

            Mapper.CreateMap<CreateSiteModel, Site>()
                .ForMember(x => x.Cookies, opt => opt.MapFrom(x => x.CookieName.IsNullOrEmpty() && x.CookieValue.IsNullOrEmpty() ? null : new[] { new Cookie { Name = x.CookieName, Value = x.CookieValue } }))
                .ForMember(x => x.SchedulerStartHours, opt => opt.MapFrom(x => x.SchedulerStartTime.IfNotNull(y => (int?)y.Value.Hours)))
                .ForMember(x => x.SchedulerStartMinutes, opt => opt.MapFrom(x => x.SchedulerStartTime.IfNotNull(y => (int?)y.Value.Minutes)))
                .ForMember(x => x.SchedulerEndHours, opt => opt.MapFrom(x => x.SchedulerEndTime.IfNotNull(y => (int?)y.Value.Hours)))
                .ForMember(x => x.SchedulerEndMinutes, opt => opt.MapFrom(x => x.SchedulerEndTime.IfNotNull(y => (int?)y.Value.Minutes)));

            Mapper.CreateMap<EditSiteModel, Site>()
                .ForMember(x => x.Cookies, opt => opt.MapFrom(x => x.CookieName.IsNullOrEmpty() && x.CookieValue.IsNullOrEmpty() ? null : new[] { new Cookie { Name = x.CookieName, Value = x.CookieValue } }))
                .ForMember(x => x.SchedulerStartHours, opt => opt.MapFrom(x => x.SchedulerStartTime.IfNotNull(y => (int?)y.Value.Hours)))
                .ForMember(x => x.SchedulerStartMinutes, opt => opt.MapFrom(x => x.SchedulerStartTime.IfNotNull(y => (int?)y.Value.Minutes)))
                .ForMember(x => x.SchedulerEndHours, opt => opt.MapFrom(x => x.SchedulerEndTime.IfNotNull(y => (int?)y.Value.Hours)))
                .ForMember(x => x.SchedulerEndMinutes, opt => opt.MapFrom(x => x.SchedulerEndTime.IfNotNull(y => (int?)y.Value.Minutes)));

            Mapper.CreateMap<UserItemViewModel, UserDetails>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id.ToInt()))
                .ForMember(x => x.CityId, opt => opt.MapFrom(x => x.CityId.ToNullableInt()))
                .ForMember(x => x.Roles, opt => opt.Ignore());

            Mapper.CreateMap<PublisherItemViewModel, PublisherDetails>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id.ToInt()))
                .ForMember(x => x.Site, opt => opt.MapFrom(x => new Item { Id = x.SiteId }))
                .ForMember(x => x.ArticleGroup, opt => opt.MapFrom(x => new Item { Id = x.ArticlesGroupId }));

            Mapper.CreateMap<ViewerItemViewModel, ViewerDetails>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id.ToId()));

            Mapper.CreateMap<UserDetails, UserItemViewModel>()
                .ForMember(x => x.Roles, opt => opt.MapFrom(x => string.Join(", ", x.Roles.ToArray(y => y.GetRoleName()))))
                .ForMember(x => x.AccessDateTime, opt => opt.MapFrom(x => x.AccessDateTime.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)))
                .ForMember(x => x.AccessDateTimeStr, opt => opt.MapFrom(x => x.AccessDateTime.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToStringInvariant(DateTimeFormat)))
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)));

            Mapper.CreateMap<PublisherDetails, PublisherItemViewModel>()
                .ForMember(x => x.SiteId, opt => opt.MapFrom(x => x.Site.Id))
                .ForMember(x => x.ArticlesGroupId, opt => opt.MapFrom(x => x.ArticleGroup.Id))
                .ForMember(x => x.AccessDateTime, opt => opt.MapFrom(x => x.AccessDateTime.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)))
                .ForMember(x => x.AccessDateTimeStr, opt => opt.MapFrom(x => x.AccessDateTime.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToStringInvariant(DateTimeFormat)))
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)));

            Mapper.CreateMap<SearchRule, SearchRuleViewModel>()
                .ForMember(x => x.FoundValueType, opt => opt.MapFrom(x => x.FoundValue.FoundValueType))
                .ForMember(x => x.AttributeName, opt => opt.MapFrom(x => x.FoundValue.AttrName))
                .ForMember(x => x.DateTimeMask, opt => opt.MapFrom(x => x.DateTimeParsingRule.DateFormat))
                .ForMember(x => x.DateCultureName, opt => opt.MapFrom(x => x.DateTimeParsingRule.CultureInfo.Name));

            Mapper.CreateMap<SearchRule, SearchRuleWithoutNameModel>()
                .ForMember(x => x.FoundValueType, opt => opt.MapFrom(x => x.FoundValue.FoundValueType))
                .ForMember(x => x.AttributeName, opt => opt.MapFrom(x => x.FoundValue.AttrName))
                .ForMember(x => x.DateTimeMask, opt => opt.MapFrom(x => x.DateTimeParsingRule.DateFormat))
                .ForMember(x => x.DateCultureName, opt => opt.MapFrom(x => x.DateTimeParsingRule.CultureInfo.Name));

            Mapper.CreateMap<ArticlesGroup, ArticlesGroupItemViewModel>()
                .ForMember(x => x.LinksFileName, opt => opt.MapFrom(x => x.LinksFile.IfNotNull(y => y.FileName)))
                .ForMember(x => x.LinksFileExtension, opt => opt.MapFrom(x => x.LinksFile.IfNotNull(y => y.Extensions)));

            Mapper.CreateMap<SearchRuleViewModel, SearchRule>()
                .ForMember(x => x.DateTimeParsingRule, opt => opt.MapFrom(x => new DateTimeParsingRule { DateFormat = x.DateTimeMask, CultureInfo = x.DateCultureName.ParseCulture() }))
                .ForMember(x => x.FoundValue, opt => opt.MapFrom(x => new FoundValue { FoundValueType = x.FoundValueType, AttrName = x.AttributeName }));

            Mapper.CreateMap<SearchRuleWithoutNameModel, SearchRule>()
                .ForMember(x => x.DateTimeParsingRule, opt => opt.MapFrom(x => new DateTimeParsingRule { DateFormat = x.DateTimeMask, CultureInfo = x.DateCultureName.ParseCulture() }))
                .ForMember(x => x.FoundValue, opt => opt.MapFrom(x => new FoundValue { FoundValueType = x.FoundValueType, AttrName = x.AttributeName }));

            Mapper.CreateMap<CreatePreviewArticlesGroupModel, ArticlesGroup>()
                .ForMember(x => x.ArticleSearchRules, opt => opt.MapFrom(x => x.ArticleSearchRules.ToArray(y => y.Map<SearchRuleViewModel, SearchRule>())));
            
            Mapper.CreateMap<EditArticlesGroupModel, ArticlesGroup>()
                .ForMember(x => x.ArticleSearchRules, opt => opt.MapFrom(x => x.ArticleSearchRules.ToArray(y => y.Map<SearchRuleViewModel, SearchRule>())));

            Mapper.CreateMap<ViewerParsedArticleItem, ViewerParsedArticleItemViewModel>()
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)))
                .ForMember(x => x.CreatedDateStr, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToStringInvariant(NumericDateTimeFormat)))
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => x.MetroStations.ToArray(y => y.Name).FirstOrDefault() ?? string.Empty));

            Mapper.CreateMap<NearestMetroStation, NearestMetroStationModel>();
            Mapper.CreateMap<AddressInfo, AddressInfoModel>();
            Mapper.CreateMap<ParsedArticle, ParsedArticleModel>()
                .ForMember(x => x.ArticleFields , opt => opt.MapFrom(x => x.ParsedValues.FormatMembers(DateTimeFormat)))
                .ForMember(x => x.AddressInfo, opt => opt.MapFrom(x => x.RecognizedFieldsData.AddressInfo.Map<AddressInfo, AddressInfoModel>()));

            Mapper.CreateMap<ImageInfo, ImageInfoModel>();
            Mapper.CreateMap<ImageProcessingResult, ImageProcessingResultModel>();

            Mapper.CreateMap<ViewerParsedArticle, ViewerParsedArticleModel>()
                .ForMember(x => x.ArticleFields, opt => opt.MapFrom(x => x.ParsedValues.FormatMembers(DateTimeFormat, x.HidePhone)))
                .ForMember(x => x.AddressInfo, opt => opt.MapFrom(x => x.RecognizedFieldsData.AddressInfo.Map<AddressInfo, AddressInfoModel>()))
                .ForMember(x => x.ImageProcessingResult, opt => opt.MapFrom(x => x.RecognizedFieldsData.ImageProcessingResult.Map<ImageProcessingResult, ImageProcessingResultModel>()));

            Mapper.CreateMap<Site, ItemViewModel>();
            Mapper.CreateMap<UserItem, ItemViewModel>()
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Email));
            Mapper.CreateMap<City, ItemViewModel>();

            Mapper.CreateMap<LogEntry, LogEntryViewModel>()
                .ForMember(x => x.Date, opt => opt.MapFrom(x => x.Date.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)));

            Mapper.CreateMap<ViewerArticleGroup, ViewerArticleGroupViewModel>();
            
            Mapper.CreateMap<ViewerDetails, ViewerItemViewModel>()
                .ForMember(x => x.AccessDateTime, opt => opt.MapFrom(x => x.AccessDateTime.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)))
                .ForMember(x => x.AccessDateTimeStr, opt => opt.MapFrom(x => x.AccessDateTime.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToStringInvariant(DateTimeFormat)))
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)))
                .ForMember(x => x.PublicId, opt => opt.MapFrom(x => x.PublicId.ToString()))
                .ForMember(
                    x => x.PaymentConfirmations,
                    opt => opt.MapFrom(
                        x =>
                        x.PaymentConfirmations.ToArray(y => y.Map<YandexPaymentConfirmation, YandexPaymentConfirmationViewModel>())));

            Mapper.CreateMap<ViewerArticleGroup, ViewerArticleGroupViewModel>();
            Mapper.CreateMap<ViewerSourceUser, ViewerSourceUserViewModel>();

            Mapper.CreateMap<UserVoteInfo, UserMenuViewModel>()
                .ForMember(x => x.AccessDateTime, opt => opt.MapFrom(x => x.AccessDateTime.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToStringInvariant(NumericDateTimeFormat)));

            Mapper.CreateMap<RecognizedPhone, RecognizedPhoneModel>();

            Mapper.CreateMap<MetroStation, ItemViewModel>();

            Mapper.CreateMap<ExternalLoginDetails, ExternalLoginViewModel>();

            Mapper.CreateMap<YandexPaymentConfirmation, YandexPaymentConfirmationViewModel>();
            Mapper.CreateMap<YandexPaymentConfirmationViewModel, YandexPaymentConfirmation>();

            Mapper.CreateMap<ViewerSourceUser, ItemViewModel>();

            Mapper.CreateMap<ViewerArticleGroup, ApartmentType>()
                .ForMember(x => x.Ids, opt => opt.MapFrom(x => string.Join(",", x.Ids)));

            Mapper.CreateMap<ApartmentFiltersModel, ApartmentFilters>()
                .ForMember(x => x.CityId, opt => opt.MapFrom(x => x.CityId.ToId()))
                .ForMember(x => x.MetroStationIds, opt => opt.MapFrom(x => x.MetroStationIds.ToArray(y => y.ToInt())))
                .ForMember(x => x.SourceIds, opt => opt.MapFrom(x => x.SourceIds.ToArray(y => y.ToInt())))
                .ForMember(x => x.ArticleGroupIds, opt => opt.MapFrom(x => x.ApartmentTypes.Where(y => y.Checked).SelectMany(y => y.Value.Split(',').ToArray(z => z.ToId())).ToArray()));

            Mapper.CreateMap<ApartmentItem, ApartmentItemModel>()
                .ForMember(x => x.CreatedDateStr, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToStringInvariant(NumericDateTimeFormat)))
                .ForMember(x => x.Price, opt => opt.MapFrom(x => x.PriceStr))
                .ForMember(x => x.PriceValue, opt => opt.MapFrom(x => x.Price))
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => x.Metro.IfNotNull(y => y.Metro, string.Empty)))
                .ForMember(x => x.MetroDistance, opt => opt.MapFrom(x => x.Metro.IfNotNull(y => y.MetroDistance, string.Empty)))
                .ForMember(x => x.MetroDuration, opt => opt.MapFrom(x => x.Metro.IfNotNull(y => y.MetroDuration, string.Empty)));

            Mapper.CreateMap<ApartmentList, ApartmentsModel>();

            Mapper.CreateMap<MetroItem, MetroItemModel>();
            Mapper.CreateMap<PhoneItem, PhoneItemModel>();
            Mapper.CreateMap<ImageInfo, ApartmentImageInfoModel>();

            Mapper.CreateMap<Apartment, ApartmentModel>()
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToStringInvariant(NumericDateTimeFormat)))
                .ForMember(x => x.OpenPhoneUrl, opt => opt.MapFrom(x => $"/Account/RegisterViewer?returnUrl={HttpUtility.UrlEncode("/apartments?apartmentId=" + x.Id)}"))
                .ForMember(x => x.MapUrl, opt => opt.MapFrom(x => string.Format(
                    "https://maps.apple.com/?q={0}",
                    x.Latitude.HasValue && x.Longitude.HasValue ?
                        x.Latitude.ToStringInvariant() + "%2C" +   x.Longitude.ToStringInvariant() :
                        HttpUtility.UrlEncode(x.Address))));

            Mapper.CreateMap<AdminSite, ItemViewModel>();

            Mapper.CreateMap<BlackListItem, BlackListItemViewModel>()
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)));

            Mapper.CreateMap<HitedArticle, HitedArticleViewModel>()
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)))
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => $"{x.Metro} {x.MetroDistance} {x.MetroDuration}"));

            Mapper.CreateMap<HitedWhiteArticle, HitedWhiteArticleViewModel>()
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)))
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => $"{x.Metro} {x.MetroDistance} {x.MetroDuration}"));

            Mapper.CreateMap<BlackListItemViewModel, BlackListItem>();

            Mapper.CreateMap<WhiteListItem, WhiteListItemViewModel>()
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)));

            Mapper.CreateMap<Application.Interfaces.Services.WhiteList.Entities.HitedWhiteArticle, ViewModels.WhiteListReport.HitedWhiteArticleViewModel>()
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)))
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => $"{x.Metro} {x.MetroDistance} {x.MetroDuration}"));

            Mapper.CreateMap<WhiteListItemViewModel, WhiteListItem>();

            Mapper.CreateMap<BlackAndWhiteArticle, BlackAndWhiteArticleViewModel>()
                .ForMember(x => x.CreatedDate, opt => opt.MapFrom(x => x.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId)))
                .ForMember(x => x.Metro, opt => opt.MapFrom(x => $"{x.Metro} {x.MetroDistance} {x.MetroDuration}"));

            Mapper.CreateMap<ContactInfo, ContactInfoModel>();

            Mapper.CreateMap<UnwatchedVote, UnwatchedVoteViewModel>();

            Mapper.CreateMap<SeoRoutingResult, SeoRoutingResultViewModel>();
            Mapper.CreateMap<BreadcrumbItem, BreadcrumbItemViewModel>();
            Mapper.CreateMap<SeoDetails, SeoDetailsViewModel>()
                .ForMember(x => x.Language, opt => opt.MapFrom(x => $"{x.LanguageCode}-{x.CountryRegionCode}".ToLowerInvariant()));

            Mapper.CreateMap<ApartmentFilters, SeoApartmentFilters>()
                .ForMember(x => x.ArticleGroupIds, opt => opt.MapFrom(x => string.Join(",", x.ArticleGroupIds)))
                .ForMember(x => x.MetroStationIds, opt => opt.MapFrom(x => string.Join(",", x.MetroStationIds)));
        }

        public static TMap Map<TSource, TMap>(this TSource source)
        {
            return Mapper.Map<TSource, TMap>(source);
        }

        public static TMap SetAdditionalData<TMap>(this TMap source, Action<TMap> setAdditional)
        {
            setAdditional(source);
            return source;
        }

        private static ParsedValueModel[] FormatMembers(this ParsedValue[] members, string dateTimeFormat)
        {
            return members.FormatMembers(dateTimeFormat, false);
        }

        private static ParsedValueModel[] FormatMembers(this ParsedValue[] members, string dateTimeFormat, bool hidePhone)
        {
            try
            {
                var result = new List<ParsedValueModel>(members.Length);
                foreach (var member in members)
                {
                    if (member == null)
                    {
                        result.Add(new ParsedValueModel
                        {
                            Name = member.Name,
                            Value = string.Empty
                        });
                        continue;
                    }

                    string value;

                    if (member.ParsedKeyType == ParsedKeyType.Phone && hidePhone)
                    {
                        value = string.Empty;
                    }
                    else if (member.ParsedKeyType == ParsedKeyType.Phone || !member.IsLink)
                    {
                        value = string.Join(ArraySeparator, member.FormatValues(dateTimeFormat, x => FormatUrl(x, member.ParsedValueType == ParsedValueType.String)));
                    }
                    else
                    {
                        value = JsonConvert.SerializeObject(member.FormatValues(dateTimeFormat, x => x));
                    }

                    result.Add(ToParsedValue(value, member, hidePhone));
                }
                return result.ToArray();
            }
            catch (Exception ex)
            {
                Log.Error("Error during DateMembers mapping", ex);
                throw;
            }
        }

        private static ParsedValueModel ToParsedValue(string value, ParsedValue member, bool hidePhone)
        {
            return new ParsedValueModel
            {
                Name = member.Name,
                Value = value, 
                BackgroundColor = member.BackgroundColor,
                IsPhone = member.ParsedKeyType == ParsedKeyType.Phone,
                ShowOnVoteForm = member.ShowOnVoteForm,
                IsImage = member.IsLink,
                RecognizedPhones = hidePhone ? new RecognizedPhoneModel[0] : member.RecognizedPhones.ToArray(x => x.Map<RecognizedPhone, RecognizedPhoneModel>()),
            };
        }

        private static string FormatUrl(string value, bool itCanBeLink)
        {
            var stringValue = value;
            Uri uri;
            bool isUrl = Uri.TryCreate(stringValue, UriKind.Absolute, out uri);

            return isUrl ? 
                itCanBeLink ? $"<a href='{uri}' target='_blank'>{uri}</a>" :
                              $"<img src='{uri}' target='_blank' alt={uri}></img>"
                : stringValue;
        }

        private static string FormatUrlForImageWithWatermark(string value, ViewerParsedArticle watermarkInfo)
        {
            var stringValue = value;
            Uri uri;
            bool isUrl = Uri.TryCreate(stringValue, UriKind.Absolute, out uri);

            return isUrl ? $"<img src='{uri}' target='_blank' alt={uri} class='clip-path' " +
            $"data-watermark-height='{watermarkInfo.WatermarkHeight ?? 0}' data-watermark-width='{watermarkInfo.WatermarkWidth ?? 0}' data-watermark-rectX='{watermarkInfo.WatermarkRectX ?? "''"}' data-watermark-rectY='{watermarkInfo.WatermarkRectY ?? "''"}' data-watermark-textX='{watermarkInfo.WatermarkTextX ?? "''"}' data-watermark-textY='{watermarkInfo.WatermarkTextY ?? "''"}' data-watermark-fontsize='{watermarkInfo.WatermarkFontSize ?? 0}' data-watermark-number='{watermarkInfo.Id}' "
            + " ></img>" : stringValue;
        }

        private static CultureInfo ParseCulture(this string cultureName)
        {
            CultureInfo result;
            try
            {
                result = CultureInfo.CreateSpecificCulture(cultureName);
            }
            catch (Exception)
            {
                result = CultureInfo.InvariantCulture;
            }
            return result;
        }
    }
}
