﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebParser.Controllers.ViewModels.Seo
{
    public class BreadcrumbItemViewModel
    {
        public string Title { get; set; }

        public string Url { get; set; }
    }
}
