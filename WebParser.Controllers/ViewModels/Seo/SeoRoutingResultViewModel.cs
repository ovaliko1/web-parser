﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebParser.Controllers.ViewModels.Seo
{
    public class SeoRoutingResultViewModel
    {
        public string SeoTitle { get; set; }

        public int? CityId { get; set; }

        public string ArticleGroupInternalName { get; set; }

        public int? DistrictId { get; set; }

        public int? PriceFrom { get; set; }

        public int? PriceTo { get; set; }
    }
}
