﻿using WebParser.Controllers.ViewModels.Apartments;

namespace WebParser.Controllers.ViewModels.Seo
{
    public class SeoDetailsViewModel
    {
        public SeoRoutingResultViewModel RoutingResult { get; set; }

        public BreadcrumbItemViewModel[] Breadcrumbs { get; set; }

        public ApartmentsModel Apartments { get; set; }

        public string CanonicalUrl { get; set; }

        public string CityName { get; set; }

        public string ArticleGroup { get; set; }

        public string District { get; set; }

        public string Language { get; set; }

        public SeoApartmentFilters ApartmentFilters { get; set; }
    }
}
