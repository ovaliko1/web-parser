﻿namespace WebParser.Controllers.ViewModels.Seo
{
    public class SeoApartmentFilters
    {
        // TODO: Rename to UserId or support real CityId here
        public int? CityId { get; set; }

        public string SourceIds { get; set; }

        public string MetroStationIds { get; set; }

        public string ArticleGroupIds { get; set; }

        public int? MinutesToMetro { get; set; }

        public int? PriceFrom { get; set; }

        public int? PriceTo { get; set; }

        public bool ShowWithEmptyMetro { get; set; }

        public bool ShowWithEmptyPrice { get; set; }
    }
}
