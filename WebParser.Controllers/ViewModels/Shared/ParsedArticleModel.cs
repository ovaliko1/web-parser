﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class ParsedArticleModel
    {
        public int Id { get; set; }

        public ParsedValueModel[] ArticleFields { get; set; }

        public string[] FoundArticleLinks { get; set; }

        public bool InBlackList { get; set; }

        public AddressInfoModel AddressInfo { get; set; }
    }
}