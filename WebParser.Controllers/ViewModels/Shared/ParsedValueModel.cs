﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class ParsedValueModel
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public string BackgroundColor { get; set; }

        public bool IsPhone { get; set; }

        public bool ShowOnVoteForm { get; set; }

        public bool IsImage { get; set; }

        public RecognizedPhoneModel[] RecognizedPhones { get; set; }
    }
}