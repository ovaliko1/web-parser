﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class LeftSideBarViewModel
    {
        public bool ShowAdminMenuItems { get; set; }

        public bool ShowUserMenuItems { get; set; }

        public bool ShowViewerMenuItems { get; set; }
    }
}
