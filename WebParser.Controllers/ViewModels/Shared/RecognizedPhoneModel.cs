﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class RecognizedPhoneModel
    {
        public string RecognizedText { get; set; }

        public string FixedText { get; set; }

        public int MeanConfidencePercentage { get; set; }
    }
}