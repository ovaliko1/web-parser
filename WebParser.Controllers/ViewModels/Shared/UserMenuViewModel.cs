﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class UserMenuViewModel
    {
        public string UserName { get; set; }

        public int GoodVotes { get; set; }

        public int BadVotes { get; set; }

        public int OutdateVotes { get; set; }

        public string AccessDateTime { get; set; }

        public int FreeDays { get; set; }

        public int MaxFreeDaysCount { get; set; }

        public int BlackListItemsCount { get; set; }

        public int OpeningBalance { get; set; }

        public int FreeOpenings { get; set; }

        public int MaxFreeOpeningsCount { get; set; }

        public int? RateType { get; set; }
    }
}
