﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class AddressInfoModel
    {
        public string FormattedAddress { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public NearestMetroStationModel NearestMetroStation { get; set; }
    }
}