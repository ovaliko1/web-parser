﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class YandexMetrikaUserInfoViewModel
    {
        public string CounterName { get; set; }

        public string UserId { get; set; }

        public string PurchasedAccount { get; set; }

        public string CreatedDate { get; set; }

        public string AccessDateTime { get; set; }

        public long? VkId { get; set; }

        public string Email { get; set; }

        public int FreeHours { get; set; }

        public string WaitingForPayment { get; set; }
    }
}