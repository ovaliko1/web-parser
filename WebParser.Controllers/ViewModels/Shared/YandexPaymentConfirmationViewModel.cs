﻿using WebParser.Controllers.ViewModels.ModelBinding;

namespace WebParser.Controllers.ViewModels.Shared
{
    public class YandexPaymentConfirmationViewModel
    {
        [BindAlias("notification_type")]
        public string NotificationType { get; set; }

        [BindAlias("operation_id")]
        public string OperationId { get; set; }

        //amount
        public string Amount { get; set; }

        [BindAlias("withdraw_amount")]
        public string WithdrawAmount { get; set; }

        //currency
        public string Currency { get; set; }

        //datetime
        public string DateTime { get; set; }

        //sender
        public string Sender { get; set; }

        //codepro
        public bool CodePro { get; set; }

        //label
        public string Label { get; set; }

        [BindAlias("sha1_hash")]
        public string Sha1Hash { get; set; }

        [BindAlias("test_notification")]
        public bool TestNotification { get; set; }

        //unaccepted
        public bool Unaccepted { get; set; }

        //lastname
        public string LastName { get; set; }

        //firstname
        public string FirstName { get; set; }

        //fathersname
        public string FathersName { get; set; }

        //email
        public string Email { get; set; }

        //phone
        public string Phone { get; set; }

        //city
        public string City { get; set; }

        //street
        public string Street { get; set; }

        //building
        public string Building { get; set; }

        //suite
        public string Suite { get; set; }

        //flat
        public string Flat { get; set; }

        //zip
        public string Zip { get; set; }

        public string AccessDateBefore { get; set; }

        public string AccessDateAfter { get; set; }
    }
}