﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class ClientSettingsViewModel
    {
        public string SupportEmail { get; set; }
    }
}