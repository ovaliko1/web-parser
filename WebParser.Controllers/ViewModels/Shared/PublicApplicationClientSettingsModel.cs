﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class PublicApplicationClientSettingsModel
    {
        public string SupportEmail { get; set; }

        public bool IsAnonymous { get; set; }

        public bool RenderMetrics { get; set; }

        public string YandexMetrikaCounterName { get; set; }
    }
}