﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class NearestMetroStationModel
    {
        public string Name { get; set; }

        public string DistanceText { get; set; }

        public int DistanceValue { get; set; }

        public string DurationText { get; set; }

        public int DurationValue { get; set; }
    }
}