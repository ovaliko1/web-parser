﻿namespace WebParser.Controllers.ViewModels.Shared
{
    public class ItemViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
