﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WebParser.Common;
using WebParser.Controllers.ViewModels.Validation;

namespace WebParser.Controllers.ViewModels.Shared
{
    public class SearchRuleViewModel
    {
        [Required]
        [DisplayName("ActionType")]
        public ActionType ActionType { get; set; }

        [RequiredIf("ActionType", ActionType.Wait, ErrorMessage = "The Wait Milseconds field is required.", DependentPropertySelector = "#ActionType-Wait")]
        [DisplayName("Wait Milseconds")]
        public int? WaitMilseconds { get; set; }

        [RequiredIf("ActionType", ActionType.Fetch, ErrorMessage = "The Field Name field is required.", DependentPropertySelector = "#ActionType-Fetch")]
        [DisplayName("Field Name")]
        public string FieldName { get; set; }

        [Required]
        [DisplayName("Selectors")]
        public string Selectors { get; set; }

        [DisplayName("Filter Function")]
        [AllowHtml]
        public string FilterFunction { get; set; }

        [DisplayName("RegExp Replace")]
        public string ReplaceRegexp { get; set; }

        [Required]
        [DisplayName("Parsed Key Type")]
        public int ParsedKeyTypeId { get; set; }

        [Required]
        [DisplayName("Search Type")]
        public SearchType SearchType { get; set; }

        [Required]
        [DisplayName("Found Value Type")]
        public FoundValueType FoundValueType { get; set; }

        [RequiredIf("FoundValueType", FoundValueType.AttributeContent, ErrorMessage = "The Attribute Name field is required.", DependentPropertySelector = "#FoundValueType-AttributeContent")]
        [DisplayName("Attribute Name")]
        public string AttributeName { get; set; }

        [DisplayName("Background Color")]
        public string BackgroundColor { get; set; }

        [DisplayName("Is Image")]
        public bool IsImage { get; set; }

        [DisplayName("Do Not Save To Internal Storage")]
        public bool DoNotSaveToInternalStorage { get; set; }

        [DisplayName("Is Date Time")]
        public bool IsDateTime { get; set; }

        [DisplayName("Date Time Mask")]
        [RequiredIf("IsDateTime", true, ErrorMessage = "The Date Time Mask field is required.")]
        public string DateTimeMask { get; set; }

        [DisplayName("Date Language")]
        public string DateCultureName { get; set; }

        [DisplayName("Show On Vote Form")]
        public bool ShowOnVoteForm { get; set; }

        [DisplayName("Hide On Vk Post")]
        public bool HideOnVkPost { get; set; }

        public SelectListItem[] DateLanguages { get; set; }

        public ItemViewModel[] ParsedKeyTypes { get; set; }
    }
}
