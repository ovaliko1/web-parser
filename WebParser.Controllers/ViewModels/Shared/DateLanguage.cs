﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebParser.Controllers.ViewModels.Shared
{
    public class DateLanguage
    {
        public string CultureName { get; set; }

        public string Name { get; set; }
    }
}
