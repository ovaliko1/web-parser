﻿namespace WebParser.Controllers.ViewModels.Error
{
    public class ForbiddenViewModel
    {
        public string SupportEmail { get; set; }

        public string UserIdentity { get; set; }

        public int FirstPrice { get; set; }

        public int SecondPrice { get; set; }

        public int ThirdPrice { get; set; }
    }
}