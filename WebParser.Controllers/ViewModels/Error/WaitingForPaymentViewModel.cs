﻿namespace WebParser.Controllers.ViewModels.Error
{
    public class WaitingForPaymentViewModel
    {
        public string SupportEmail { get; set; }

        public string UserIdentity { get; set; }
    }
}