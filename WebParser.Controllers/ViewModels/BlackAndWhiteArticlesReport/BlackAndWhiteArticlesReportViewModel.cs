﻿using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.BlackAndWhiteArticlesReport
{
    public class BlackAndWhiteArticlesReportViewModel
    {
        public ItemViewModel[] Sites { get; set; }
    }
}