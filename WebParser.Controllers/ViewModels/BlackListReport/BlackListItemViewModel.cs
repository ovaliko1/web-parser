﻿using System;
using System.ComponentModel;

namespace WebParser.Controllers.ViewModels.BlackListReport
{
    public class BlackListItemViewModel
    {
        [DisplayName("Created Date")]
        public DateTime CreatedDate { get; set; }

        [DisplayName("Identity")]
        public string Identity { get; set; }

        [DisplayName("Confidence Percentage")]
        public int IdentityConfidencePercentage { get; set; }

        [DisplayName("Hits")]
        public int Hits { get; set; }

        [DisplayName("Created By")]
        public int CreatedBy { get; set; }

        [DisplayName("Black List Site")]
        public int? SiteId { get; set; }

        [DisplayName("Article Site")]
        public int? ArticleSiteId { get; set; }

        [DisplayName("Source Article")]
        public int? SourceArticleId { get; set; }

        [DisplayName("Id")]
        public int Id { get; set; }
    }
}