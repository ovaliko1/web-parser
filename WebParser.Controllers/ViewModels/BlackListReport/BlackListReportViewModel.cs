﻿using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.BlackListReport
{
    public class BlackListReportViewModel
    {
        public int? Id { get; set; }

        public ItemViewModel[] Sites { get; set; }

        public ItemViewModel[] Users { get; set; }
    }
}