﻿using System;

namespace WebParser.Controllers.ViewModels.WhiteListReport
{
    public class HitedWhiteArticleViewModel
    {
        public int Id { get; set; }

        public int? SiteId { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ArticleGroup { get; set; }

        public string DuplicateGroupId { get; set; }

        public string Identity { get; set; }

        public int IdentityConfidencePercentage { get; set; }

        public bool IsDuplicate { get; set; }

        public string Price { get; set; }

        public string FormattedAddress { get; set; }

        public string Metro { get; set; }

        public string Link { get; set; }
    }
}