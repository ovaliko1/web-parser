﻿using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.WhiteListReport
{
    public class WhiteListReportViewModel
    {
        public int? Id { get; set; }

        public ItemViewModel[] Sites { get; set; }

        public ItemViewModel[] Users { get; set; }
    }
}