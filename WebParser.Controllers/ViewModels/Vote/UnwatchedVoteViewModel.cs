﻿using System;

namespace WebParser.Controllers.ViewModels.Vote
{
    public class UnwatchedVoteViewModel
    {
        public int Id { get; set; }

        public DateTime VoteDate { get; set; }

        public int ArticleRatingTypeId { get; set; }

        public int? ArticleId { get; set; }

        public string DeletedArticleIdentity { get; set; }

        public string DeletedArticleSummary { get; set; }

        public int RateTypeId { get; set; }

        public long? UserVkId { get; set; }

        public string UserEmail { get; set; }

        public DateTime UserAccessDateTime { get; set; }

        public int UserFreeHours { get; set; }

        public int UserOpeningBalance { get; set; }

        public int UserFreeOpenings { get; set; }
    }
}
