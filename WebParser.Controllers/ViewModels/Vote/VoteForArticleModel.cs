﻿using System.ComponentModel.DataAnnotations;

namespace WebParser.Controllers.ViewModels.Vote
{
    public class VoteForApartmentModel
    {
        [Required]
        public int ApartmentId { get; set; }

        [Required]
        public int VoteType { get; set; }
    }
}