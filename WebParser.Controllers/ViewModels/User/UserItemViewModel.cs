﻿using System;
using System.ComponentModel;

namespace WebParser.Controllers.ViewModels.User
{
    public class UserItemViewModel
    {
        public string Id { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Roles")]
        public string Roles { get; set; }

        [DisplayName("Max Site Count")]
        public int MaxSiteCount { get; set; }

        [DisplayName("Name For Viewers")]
        public string NameForViewers { get; set; }

        [DisplayName("City")]
        public string CityId { get; set; }

        [DisplayName("Access Date Time")]
        public DateTime AccessDateTime { get; set; }

        public string AccessDateTimeStr { get; set; }

        [DisplayName("Created Date")]
        public DateTime CreatedDate { get; set; }
    }
}
