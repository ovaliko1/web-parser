﻿using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.User
{
    public class UserListViewModel
    {
        public ItemViewModel[] Cities { get; set; }
    }
}
