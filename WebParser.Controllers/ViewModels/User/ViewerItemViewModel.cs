﻿using System;
using System.ComponentModel;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.User
{
    public class ViewerItemViewModel
    {
        [DisplayName("Id")]
        public string Id { get; set; }

        [DisplayName("Public Id")]
        public string PublicId { get; set; }

        [DisplayName("Vk Id")]
        public string VkId { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Created Date")]
        public DateTime CreatedDate { get; set; }

        [DisplayName("Access Date Time")]
        public DateTime AccessDateTime { get; set; }

        public string AccessDateTimeStr { get; set; }

        [DisplayName("Opening Balance")]
        public int OpeningBalance { get; set; }

        [DisplayName("Email Confirmed")]
        public bool EmailConfirmed { get; set; }

        [DisplayName("Seen Forbidden")]
        public bool SeenForbidden { get; set; }

        [DisplayName("Votes Count")]
        public int VotesCount { get; set; }

        [DisplayName("Purchased Account")]
        public bool PurchasedAccount { get; set; }

        [DisplayName("Waiting For Payment")]
        public bool WaitingForPayment { get; set; }

        public YandexPaymentConfirmationViewModel[] PaymentConfirmations { get; set; }
    }
}
