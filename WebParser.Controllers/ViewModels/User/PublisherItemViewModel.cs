﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.User
{
    public class PublisherItemViewModel
    {
        public string Id { get; set; }

        [DisplayName("Access Date Time")]
        public DateTime AccessDateTime { get; set; }

        public string AccessDateTimeStr { get; set; }

        [DisplayName("Created Date")]
        public DateTime CreatedDate { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DisplayName("Site")]
        public int SiteId { get; set; }

        [Required]
        [DisplayName("Articles Group")]
        public int ArticlesGroupId { get; set; }

        public ItemViewModel[] Sites { get; set; }
    }
}