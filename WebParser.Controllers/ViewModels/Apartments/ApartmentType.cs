﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class ApartmentType
    {
        public string Ids { get; set; }

        public string Name { get; set; }
    }
}