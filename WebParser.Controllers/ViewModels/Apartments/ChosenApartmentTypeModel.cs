﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class ChosenApartmentTypeModel
    {
        public string Value { get; set; }

        public string Name { get; set; }

        public bool Checked { get; set; }
    }
}