﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class ApartmentImageInfoModel
    {
        public string ImageUrl { get; set; }

        public int? Height { get; set; }

        public int? Width { get; set; }
    }
}