﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class ApartmentsModel
    {
        public ApartmentItemModel[] Items { get; set; }

        public bool HasMore { get; set; }
    }
}