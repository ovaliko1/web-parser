﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class ApartmentModel
    {
        public string Id { get; set; }

        public string Subject { get; set; }

        public string Price { get; set; }

        public MetroItemModel[] MetroItems { get; set; }

        public string[] SourceMetro { get; set; }

        public string Address { get; set; }

        public string[] SourceAddress { get; set; }

        public string CreatedDate { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string MapUrl { get; set; }

        public string[] OwnerNames { get; set; }

        public int PhoneState { get; set; }

        public string OpenPhoneUrl { get; set; }

        public PhoneItemModel[] Phones { get; set; }

        public string[] Images { get; set; }

        public string[] Description { get; set; }

        public string[] Characteristics { get; set; }

        public string[] SourceDates { get; set; }

        public bool PossiblyRented { get; set; }

        public bool InBlackList { get; set; }

        public bool InWhiteList { get; set; }

        public bool HideBlacklistWarranty { get; set; }

        public ApartmentImageInfoModel[] FullSizeImages { get; set; }
    }
}