﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class ApartmentItemModel
    {
        public int Id { get; set; }

        public string CreatedDateStr { get; set; }

        public int? SiteId { get; set; }

        public string ArticlesGroup { get; set; }

        public string Price { get; set; }

        public int? PriceValue { get; set; }

        public string Metro { get; set; }

        public string MetroDistance { get; set; }

        public string MetroDuration { get; set; }

        public int GoodRating { get; set; }

        public int BadRating { get; set; }

        public int OutdateRating { get; set; }

        public string Source { get; set; }

        public bool ShowRating { get; set; }

        public bool InWhiteList { get; set; }

        public bool PossiblyRented { get; set; }

        public bool HideBlacklistWarranty { get; set; }

        public ApartmentImageInfoModel[] ImagesPreview { get; set; }
    }
}