﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class ApartmentFiltersModel
    {
        public int? ApartmentId { get; set; }

        // TODO: Rename to UserId or support real CityId here
        public string CityId { get; set; }

        public string[] SourceIds { get; set; }

        public string[] MetroStationIds { get; set; }

        public ChosenApartmentTypeModel[] ApartmentTypes { get; set; }

        public int? MinutesToMetro { get; set; }

        public int? PriceFrom { get; set; }

        public int? PriceTo { get; set; }

        public bool ShowWithEmptyMetro { get; set; }

        public bool ShowWithEmptyPrice { get; set; }
    }
}