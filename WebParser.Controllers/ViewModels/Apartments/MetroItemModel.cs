﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class MetroItemModel
    {
        public string Metro { get; set; }

        public string MetroDistance { get; set; }

        public string MetroDuration { get; set; }
    }
}