﻿using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.Apartments
{
    public class FiltersInitDataModel
    {
        public ItemViewModel[] Cities { get; set; }

        public ItemViewModel[] Sources { get; set; }

        public ItemViewModel[] MetroStations { get; set; }

        public ApartmentType[] ApartmentTypes { get; set; }

        public int DefaultCityId { get; set; }
    }
}