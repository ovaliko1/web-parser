﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class ContactInfoModel
    {
        public PhoneItemModel[] Phones { get; set; }

        public bool ShowForbidden { get; set; }

        public bool CanBeVoted { get; set; }
    }
}