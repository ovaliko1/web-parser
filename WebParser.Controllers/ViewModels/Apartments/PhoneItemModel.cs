﻿namespace WebParser.Controllers.ViewModels.Apartments
{
    public class PhoneItemModel
    {
        public int Type { get; set; }

        public string Value { get; set; }

        public string BackgroundColor { get; set; }
    }
}