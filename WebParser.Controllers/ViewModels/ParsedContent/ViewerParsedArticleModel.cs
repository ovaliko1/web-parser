﻿using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class ViewerParsedArticleModel
    {
        public int Id { get; set; }

        public ParsedValueModel[] ArticleFields { get; set; }

        public bool CanBeVoted { get; set; }

        public bool InBlackList { get; set; }

        public bool InWhiteList { get; set; }

        public bool HidePhone { get; set; }

        public AddressInfoModel AddressInfo { get; set; }

        public ImageProcessingResultModel ImageProcessingResult { get; set; }
    }
}