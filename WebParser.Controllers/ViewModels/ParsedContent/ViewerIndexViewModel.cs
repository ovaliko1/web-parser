﻿using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class ViewerIndexViewModel
    {
        public int? ArticleId { get; set; }

        public ViewerSourceUserViewModel[] SourceUsers { get; set; }

        public string SelectedSourceUserId { get; set; }

        public string SelectedCityId { get; set; }

        public ItemViewModel[] Sites { get; set; }
    }
}