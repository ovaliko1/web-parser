﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class ViewerParsedArticleItemViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "CreatedDate")]
        public DateTime CreatedDate { get; set; }

        public string CreatedDateStr { get; set; }

        [Display(Name = "Site")]
        public int? SiteId { get; set; }

        [Display(Name = "Articles Group")]
        public string ArticlesGroup { get; set; }

        [Display(Name = "Price")]
        public int? Price { get; set; }

        public string PriceStr { get; set; }

        [Display(Name = "Metro")]
        public string Metro { get; set; }

        [Display(Name = "Rating")]
        public string Rating { get; set; }

        [Display(Name = "Good Rating")]
        public int GoodRating { get; set; }

        [Display(Name = "Bad Rating")]
        public int BadRating { get; set; }

        [Display(Name = "Outdate Rating")]
        public int OutdateRating { get; set; }

        public bool ShowRating { get; set; }

        public bool Selected { get; set; }

        [Display(Name = "Is Duplicate")]
        public bool IsDuplicate { get; set; }

        [Display(Name = "In Black List")]
        public bool InBlackList { get; set; }

        [Display(Name = "In White List")]
        public bool InWhiteList { get; set; }

        [Display(Name = "Link")]
        public string Link { get; set; }

        [Display(Name = "DuplicateGroupId")]
        public Guid? DuplicateGroupId { get; set; }

        [Display(Name = "ParsingFailedCount")]
        public int? ParsingFailedCount { get; set; }

        [Display(Name = "ParsingFailedReason")]
        public string ParsingFailedReason { get; set; }
    }
}
