﻿namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class ImageProcessingResultModel
    {
        public ImageProcessingResultModel()
        {
        }

        public ImageProcessingResultModel(ImageInfoModel[] fullSizeImages, ImageInfoModel[] imagesPreview)
        {
            FullSizeImages = fullSizeImages;
            ImagesPreview = imagesPreview;
        }

        public ImageInfoModel[] FullSizeImages { get; set; }

        public ImageInfoModel[] ImagesPreview { get; set; }
    }
}