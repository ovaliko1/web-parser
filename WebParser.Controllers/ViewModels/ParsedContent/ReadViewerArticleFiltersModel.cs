﻿namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class ReadViewerArticleFiltersModel
    {
        public string CityId { get; set; }

        public string[] ArticleGroupIds { get; set; }

        public string[] MetroStationIds { get; set; }
    }
}
