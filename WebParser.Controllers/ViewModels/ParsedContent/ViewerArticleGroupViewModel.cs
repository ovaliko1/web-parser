﻿namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class ViewerArticleGroupViewModel
    {
        public int[] Ids { get; set; }

        public string Name { get; set; }

        public bool Checked { get; set; }
    }
}