﻿namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class ArticlesGroupItemViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
