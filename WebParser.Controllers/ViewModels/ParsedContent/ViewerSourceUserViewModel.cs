﻿namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class ViewerSourceUserViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? CityId { get; set; }

        public ViewerArticleGroupViewModel[] ArticleGroups { get; set; }
    }
}
