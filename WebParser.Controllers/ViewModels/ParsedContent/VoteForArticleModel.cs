﻿using System.ComponentModel.DataAnnotations;

namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class VoteForArticleModel
    {
        [Required]
        public int ArticleId { get; set; }

        [Required]
        public int RatingTypeId { get; set; }

        [Required]
        public bool ConsiderVote { get; set; }
    }
}