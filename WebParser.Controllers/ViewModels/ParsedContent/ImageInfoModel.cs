﻿namespace WebParser.Controllers.ViewModels.ParsedContent
{
    public class ImageInfoModel
    {
        public int? FileId { get; set; }

        public string ImageUrl { get; set; }

        public int? Height { get; set; }

        public int? Width { get; set; }
    }
}