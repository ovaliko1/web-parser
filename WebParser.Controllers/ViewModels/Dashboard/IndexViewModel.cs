﻿using WebParser.Controllers.ViewModels.ParsedContent;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.Dashboard
{
    public class IndexViewModel
    {
        public ItemViewModel[] Sites { get; set; }

        public ItemViewModel[] Users { get; set; }

        public bool ShowUserColumn { get; set; }
    }
}
