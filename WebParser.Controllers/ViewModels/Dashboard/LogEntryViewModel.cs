﻿using System;
using System.ComponentModel;

namespace WebParser.Controllers.ViewModels.Dashboard
{
    public class LogEntryViewModel
    {
        public int Id { get; set; }

        [DisplayName("User")]
        public int UserId { get; set; }

        [DisplayName("Date")]
        public DateTime Date { get; set; }

        [DisplayName("Site")]
        public int? SiteId { get; set; }

        [DisplayName("Status")]
        public int Status { get; set; }

        [DisplayName("Message")]
        public string Message { get; set; }
    }
}
