﻿namespace WebParser.Controllers.ViewModels.Account
{
    public class RegisterFailedViewModel
    {
        public string SupportEmail { get; set; }

        public string Code { get; set; }

        public string UserId { get; set; }

        public string Now { get; set; }

        public string ReturnUrl { get; set; }
    }
}