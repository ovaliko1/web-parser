﻿using System.ComponentModel.DataAnnotations;

namespace WebParser.Controllers.ViewModels.Account
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить?")]
        public bool RememberMe { get; set; }

        public ExternalLoginViewModel ExternalLoginViewModel { get; set; }
    }
}
