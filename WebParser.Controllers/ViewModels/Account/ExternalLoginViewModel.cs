﻿namespace WebParser.Controllers.ViewModels.Account
{
    public class ExternalLoginViewModel
    {
        public string AuthenticationType { get; set; }

        public string Caption { get; set; }
    }
}