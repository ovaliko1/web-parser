﻿namespace WebParser.Controllers.ViewModels.Account
{
    public class ExternalLoginFailedViewModel
    {
        public string SupportEmail { get; set; }

        public string[] ErrorMessages { get; set; }
    }
}
