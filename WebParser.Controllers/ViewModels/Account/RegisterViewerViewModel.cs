﻿using System.ComponentModel.DataAnnotations;

namespace WebParser.Controllers.ViewModels.Account
{
    public class RegisterViewerViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} должен иметь минимум {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Повторите пароль")]
        [Compare("Password", ErrorMessage = "Пароль и подтверждающий пароль не совпадают.")]
        public string ConfirmPassword { get; set; }

        public ExternalLoginViewModel ExternalLoginViewModel { get; set; }
    }
}