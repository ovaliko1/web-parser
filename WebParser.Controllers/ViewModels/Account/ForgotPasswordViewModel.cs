﻿using System.ComponentModel.DataAnnotations;

namespace WebParser.Controllers.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
