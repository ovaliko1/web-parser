﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace WebParser.Controllers.ViewModels.ModelBinding
{
    public class CustomModelBinder : DefaultModelBinder
    {
        protected override PropertyDescriptorCollection GetModelProperties(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var result = base.GetModelProperties(controllerContext, bindingContext);

            var additional = new List<PropertyDescriptor>();

            foreach (var descriptor in GetTypeDescriptor(controllerContext, bindingContext).GetProperties().Cast<PropertyDescriptor>())
            {
                foreach (var attr in descriptor.Attributes.OfType<BindAliasAttribute>())
                {
                    additional.Add(new AliasedPropertyDescriptor(attr.Alias, descriptor));

                    if (bindingContext.PropertyMetadata.ContainsKey(descriptor.Name))
                    {
                        bindingContext.PropertyMetadata.Add(attr.Alias, bindingContext.PropertyMetadata[descriptor.Name]);
                    }
                }
            }

            return new PropertyDescriptorCollection(result.Cast<PropertyDescriptor>().Concat(additional).ToArray());
        }
    }
}