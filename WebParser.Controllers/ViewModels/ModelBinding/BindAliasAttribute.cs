﻿using System;

namespace WebParser.Controllers.ViewModels.ModelBinding
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class BindAliasAttribute : Attribute
    {
        public BindAliasAttribute(string alias)
        {
            Alias = alias;
        }
        public string Alias { get; private set; }
    }
}
