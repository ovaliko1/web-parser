﻿namespace WebParser.Controllers.ViewModels.Forbidden
{
    public class WaitingForPaymentViewModel
    {
        public string SupportEmail { get; set; }

        public string UserIdentity { get; set; }
    }
}