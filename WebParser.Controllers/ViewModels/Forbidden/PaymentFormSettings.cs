﻿namespace WebParser.Controllers.ViewModels.Forbidden
{
    public class PaymentFormSettings
    {
        public int FirstRateOpeningsCount { get; set; }
        public int FirstRateOpeningsPrice { get; set; }
        public int SecondRateOpeningsCount { get; set; }
        public int SecondRateOpeningsPrice { get; set; }
        public int ThirdRateOpeningsCount { get; set; }
        public int ThirdRateOpeningsPrice { get; set; }
        public int FourthRateOpeningsCount { get; set; }
        public int FourthRateOpeningsPrice { get; set; }

        public int FirstRateDaysCount { get; set; }
        public int FirstRateDaysPrice { get; set; }
        public int SecondRateDaysCount { get; set; }
        public int SecondRateDaysPrice { get; set; }
        public int ThirdRateDaysCount { get; set; }
        public int ThirdRateDaysPrice { get; set; }
        public int FourthRateDaysCount { get; set; }
        public int FourthRateDaysPrice { get; set; }

        public string SuccessUrl { get; set; }

        public string Receiver { get; set; }

        public string Label { get; set; }

        public string SupportEmail { get; set; }
    }
}