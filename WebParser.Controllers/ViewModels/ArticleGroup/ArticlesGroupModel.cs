﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using WebParser.Common;
using WebParser.Controllers.ViewModels.Shared;
using WebParser.Controllers.ViewModels.Validation;

namespace WebParser.Controllers.ViewModels.ArticleGroup
{
    public class ArticlesGroupModel
    {
        [DisplayName("Internal Name")]
        [Required]
        public string InternalName { get; set; }

        [DisplayName("Public Name")]
        [Required]
        public string PublicName { get; set; }

        [RequiredIf("LinksFormatType", LinksToArticlesType.SpecificLinks, ErrorMessage = "The Links field is required.", DependentPropertySelector = "#LinksFormat-SpecificLinks")]
        public string[] Links { get; set; }
        
        [RequiredIf("LinksFormatType", LinksToArticlesType.LinksFormat, ErrorMessage = "The Links Format field is required.", DependentPropertySelector = "#LinksFormat-LinksFormat")]
        [DisplayName("Links format")]
        public string LinksFormat { get; set; }

        [RequiredIf("LinksFormatType", LinksToArticlesType.LinksFormat, ErrorMessage = "The Start Page field is required.", DependentPropertySelector = "#LinksFormat-LinksFormat")]
        [DisplayName("Start Page")]
        public int? StartPage { get; set; }

        [RequiredIf("LinksFormatType", LinksToArticlesType.LinksFormat, ErrorMessage = "The Step Size field is required.", DependentPropertySelector = "#LinksFormat-LinksFormat")]
        [DisplayName("Step Size")]
        public int? StepSize { get; set; }

        [RequiredIf("LinksFormatType", LinksToArticlesType.LinksFile, ErrorMessage = "The Links File is required.", DependentPropertySelector = "#LinksFormat-LinksFile")]
        [DisplayName("Choose File With Links")]
        public int? LinksFileId { get; set; }

        [DisplayName("Vk Group Id")]
        public long? VkGroupId { get; set; }

        [DisplayName("Vk Topic Id")]
        public long? VkTopicId { get; set; }

        [DisplayName("Vk Topic Group Id")]
        public long? VkTopicGroupId { get; set; }

        [Required]
        [DisplayName("Article Parsing Spider Type")]
        public int SpiderTypeId { get; set; }

        [Required]
        [DisplayName("Links To Articles Parsing Spider Type")]
        public int LinksToArticlesSearchRuleSpiderTypeId { get; set; }

        [Required]
        [DisplayName("Do Not Load Images")]
        public bool SpiderDoNotLoadImages { get; set; }

        [Required]
        [Range(1, 100)]
        [DisplayName("Batch Size")]
        public int BatchSize { get; set; }

        [Required]
        [DisplayName("Batch Delay In Seconds")]
        public int BatchDelayInSeconds { get; set; }

        [Required]
        [DisplayName("Download File Delay In Millseconds ")]
        public int DownloadFileDelayInMillseconds { get; set; }

        [DisplayName("Wait Milliseconds Rule")]
        public int? WaitMilliseconds { get; set; }

        [DisplayName("Twitter Consumer Key")]
        public string TwitterConsumerKey { get; set; }

        [DisplayName("Twitter Consumer Secret")]
        public string TwitterConsumerSecret { get; set; }

        [DisplayName("Twitter Access Token")]
        public string TwitterAccessToken { get; set; }

        [DisplayName("Twitter Access Token Secret")]
        public string TwitterAccessTokenSecret { get; set; }

        public ItemViewModel[] SpiderTypes { get; set; }

        [Required]
        [DisplayName("Links Format Type")]
        public LinksToArticlesType LinksFormatType { get; set; }

        [Required]
        public SearchRuleWithoutNameModel LinksToArticlesSearchRule { get; set; }

        [Required]
        public SearchRuleViewModel[] ArticleSearchRules { get; set; }
    }
}