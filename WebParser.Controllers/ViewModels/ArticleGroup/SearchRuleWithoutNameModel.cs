﻿using System.ComponentModel.DataAnnotations;
using WebParser.Common;
using WebParser.Controllers.ViewModels.Validation;

namespace WebParser.Controllers.ViewModels.ArticleGroup
{
    public class SearchRuleWithoutNameModel
    {
        [Required]            
        public string Selectors { get; set; }

        public string FilterFunction { get; set; }

        public string ReplaceRegexp { get; set; }

        [Required]
        public SearchType SearchType { get; set; }

        [Required]
        public FoundValueType FoundValueType { get; set; }

        [RequiredIf("FoundValueType", FoundValueType.AttributeContent, ErrorMessage = "The Attribute Name field is required.")]
        public string AttributeName { get; set; }

        public bool IsImage { get; set; }

        public bool DoNotSaveToInternalStorage { get; set; }

        public bool IsDateTime { get; set; }

        [RequiredIf("IsDateTime", true, ErrorMessage = "The Date Time Mask field is required.")]
        public string DateTimeMask { get; set; }

        public string DateCultureName { get; set; }
    }
}
