﻿using System.ComponentModel.DataAnnotations;

namespace WebParser.Controllers.ViewModels.ArticleGroup
{
    public class CreatePreviewArticlesGroupModel : ArticlesGroupModel
    {
        [Required]
        public int SiteId { get; set; }
    }
}
