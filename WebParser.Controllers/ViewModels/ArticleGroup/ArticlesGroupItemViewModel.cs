﻿using System.ComponentModel;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.ArticleGroup
{
    public class ArticlesGroupItemViewModel
    {
        public int Id { get; set; }

        [DisplayName("Internal Name")]
        public string InternalName { get; set; }

        [DisplayName("Public Name")]
        public string PublicName { get; set; }

        public string[] Links { get; set; }
        
        public string LinksFormat { get; set; }

        public int? StartPage { get; set; }

        public int? StepSize { get; set; }

        public int? LinksFileId { get; set; }

        public string LinksFileName { get; set; }

        public string LinksFileExtension { get; set; }

        public long? VkGroupId { get; set; }

        public long? VkTopicId { get; set; }

        public long? VkTopicGroupId { get; set; }

        public int SpiderTypeId { get; set; }

        public int LinksToArticlesSearchRuleSpiderTypeId { get; set; }

        public bool SpiderDoNotLoadImages { get; set; }

        public int BatchSize { get; set; }

        public int BatchDelayInSeconds { get; set; }

        public int DownloadFileDelayInMillseconds { get; set; }

        public int? WaitMilliseconds { get; set; }

        public string TwitterConsumerKey { get; set; }

        public string TwitterConsumerSecret { get; set; }

        public string TwitterAccessToken { get; set; }

        public string TwitterAccessTokenSecret { get; set; }

        public SearchRuleWithoutNameModel LinksToArticlesSearchRule { get; set; }

        public SearchRuleViewModel[] ArticleSearchRules { get; set; }

        public SearchRuleWithoutNameModel ArticlePostedDateSearchRule { get; set; }
    }
}
