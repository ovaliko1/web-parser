﻿using System.ComponentModel.DataAnnotations;

namespace WebParser.Controllers.ViewModels.ArticleGroup
{
    public class EditArticlesGroupModel : ArticlesGroupModel
    {
        [Required]
        public int Id { get; set; }
    }
}
