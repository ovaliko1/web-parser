﻿namespace WebParser.Controllers.ViewModels.Site
{
    public class CookieViewModel
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}
