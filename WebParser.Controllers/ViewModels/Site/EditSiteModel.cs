﻿using System.ComponentModel.DataAnnotations;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers.ViewModels.Site
{
    public class EditSiteModel : SiteModel
    {
        [Required]
        public int Id { get; set; }

        public ItemViewModel[] JobTypes { get; set; }
    }
}
