﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebParser.Controllers.ViewModels.Site
{
    public class SiteItemViewModel
    {
        [Required] 
        public int Id { get; set; }

        [DisplayName("Link")]
        [Required]
        public string Link { get; set; }
        
        [DisplayName("Name")]
        [Required]
        public string Name { get; set; }

        [DisplayName("Is Enabled")]
        [Required]
        public bool IsEnabled { get; set; }

        [DisplayName("One Time Export")]
        public bool OneTimeExport { get; set; }

        [DisplayName("Scheduler Start Hours")]
        [Required]
        public int SchedulerStartHours { get; set; }

        [DisplayName("Scheduler Start Minutes")]
        [Required]
        public int SchedulerStartMinutes { get; set; }

        public bool IsInParsing { get; set; }

        public bool NeedStartOneTimeParsing { get; set; }

        public string ProxyAddress { get; set; }

        public string WebHookUrl { get; set; }

        [DisplayName("Cookie Name")]
        public string CookieName { get; set; }

        [DisplayName("Cookie Value")]
        public string CookieValue { get; set; }
    }
}
