﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using WebParser.Controllers.ViewModels.Shared;
using WebParser.Controllers.ViewModels.Validation;

namespace WebParser.Controllers.ViewModels.Site
{
    public class SiteModel
    {
        [Required]
        [Url]
        public string Link { get; set; }

        [Required]
        public string Name { get; set; }

        [DisplayName("One Time Export")]
        public bool OneTimeExport { get; set; }

        [DisplayName("Is Enabled")]
        public bool IsEnabled { get; set; }

        [RequiredIf("OneTimeExport", false, ErrorMessage = "The Parsing Start Time field is required.", DependentPropertySelector = "#OneTimeExport")]
        [DisplayName("Parsing Start Time")]
        public TimeSpan? SchedulerStartTime { get; set; }

        [DisplayName("Parsing Start Time")]
        public string SchedulerStartTimeStr { get; set; }

        [RequiredIf("OneTimeExport", false, ErrorMessage = "The Parsing End Time field is required.", DependentPropertySelector = "#OneTimeExport")]
        [DisplayName("Parsing End Time")]
        public TimeSpan? SchedulerEndTime { get; set; }

        [DisplayName("Parsing End Time")]
        public string SchedulerEndTimeStr { get; set; }

        [RequiredIf("OneTimeExport", false, ErrorMessage = "The Repeat Interval field is required.", DependentPropertySelector = "#OneTimeExport")]
        [DisplayName("Repeat Interval In Minutes")]
        public int? RepeatIntervalInMunutes { get; set; }

        [DisplayName("Proxy Address")]
        public string ProxyAddress { get; set; }

        [DisplayName("Proxy Username")]
        public string ProxyUsername { get; set; }

        [DisplayName("Proxy Password")]
        public string ProxyPassword { get; set; }

        [DisplayName("Cookie Name")]
        public string CookieName { get; set; }

        [DisplayName("Cookie Value")]
        public string CookieValue { get; set; }

        [Url]
        [DisplayName("Web Hook Url")]
        public string WebHookUrl { get; set; }

        public bool IsInParsing { get; set; }

        public bool NeedStartOneTimeParsing { get; set; }

        [DisplayName("Watermark Height")]
        public int? WatermarkHeight { get; set; }

        [DisplayName("Watermark Width")]
        public int? WatermarkWidth { get; set; }

        [DisplayName("Watermark FontSize")]
        public int? WatermarkFontSize { get; set; }

        [DisplayName("Watermark RectX")]
        public string WatermarkRectX { get; set; }

        [DisplayName("Watermark RectY")]
        public string WatermarkRectY { get; set; }

        [DisplayName("Watermark TextX")]
        public string WatermarkTextX { get; set; }

        [DisplayName("Watermark TextY")]
        public string WatermarkTextY { get; set; }

        [DisplayName("User Agent")]
        public string UserAgent { get; set; }

        [DisplayName("Job Type")]
        public int JobTypeId { get; set; }

        [DisplayName("Black List Identity Site")]
        public int? BlackListIdentitySiteId { get; set; }

        [DisplayName("Geo Bounds")]
        public string GeoBounds { get; set; }

        [DisplayName("Use Source Link As Contact")]
        public bool UseSourceLinkAsContact { get; set; }

        [DisplayName("Crop Image Top")]
        public int CropImageTop { get; set; }

        [DisplayName("Crop Image Bottom")]
        public int CropImageBottom { get; set; }

        [DisplayName("Crop Image Left")]
        public int CropImageLeft { get; set; }

        [DisplayName("Crop Image Right")]
        public int CropImageRight { get; set; }

        public ItemViewModel[] ViewerSites { get; set; }
    }
}
