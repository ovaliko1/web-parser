﻿using System.Linq;
using System.Web.Mvc;
using WebParser.Application.Interfaces.Services.Settings;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Common.Extensions;
using WebParser.Common.MetroStations;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "Viewer,User,Admin")]
    public class DataController : ViewController
    {
        private IUserService UserService { get; }

        private IMetroMapService MetroMapService { get; }

        public DataController(
            IUserService userService,
            IMetroMapService metroMapService)
        {
            UserService = userService;
            MetroMapService = metroMapService;
        }

        [AllowWithExpiredAccess]
        [HttpGet]
        public JsonResult GetUserCounters()
        {
            var model = UserService.GetCurrentUserVoteInfo().Map<UserVoteInfo, UserMenuViewModel>();
            return JsonCamelCase(model, JsonRequestBehavior.AllowGet);
        }

        [AllowWithExpiredAccess]
        [HttpPost]
        public JsonResult TryExtendUserAccessForSharing()
        {
            UserService.TryExtendUserAccessForSharing();
            return JsonCamelCase("ok");
        }

        [AllowWithExpiredAccess]
        [HttpPost]
        public JsonResult GetMetroStations(int? cityId)
        {
            var model = MetroMapService.GetStations(cityId).ToArray(x => x.Map<MetroStation, ItemViewModel>()).ToArray();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}