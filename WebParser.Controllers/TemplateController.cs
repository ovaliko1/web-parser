﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.ArticleGroup;
using WebParser.Controllers.ViewModels.Shared;
using WebParser.Controllers.ViewModels.Site;

namespace WebParser.Controllers
{
    // TODO: remove
    [WebParserAuthorize(Roles = "User,Admin")]
    public class TemplateController : ViewController
    {
        private static readonly Dictionary<string, string> DateLanguages = new Dictionary<string, string>
        {
            { CultureNames.Russian, "Русский" },
            { CultureNames.English, "English" },
        };

        private ISiteStructureService SiteStructureService { get; }

        public TemplateController(
            ISiteStructureService siteStructureService)
        {
            SiteStructureService = siteStructureService;
        }

        [ChildActionOnly]
        public PartialViewResult SiteTemplate()
        {
            var model = new EditSiteModel
            {
                ViewerSites = SiteStructureService.GetViewerAvailableSites().ToArray(x => x.Map<Site, ItemViewModel>()),
                JobTypes = JobTypes.GetItems().ToArray(ViewModelMapper.Map<Item, ItemViewModel>),
            };
            return PartialView("~/Views/Site/_SiteTemplate.cshtml", model);
        }

        [ChildActionOnly]
        public PartialViewResult SearchRuleTemplate()
        {
            var model = new SearchRuleViewModel
            {
                DateLanguages = DateLanguages.ToArray(x => new SelectListItem
                {
                    Text = x.Value,
                    Value = x.Key,
                }),
                ParsedKeyTypes = ParsedKeyTypes.GetItems().ToArray(ViewModelMapper.Map<Item, ItemViewModel>),
            };
            model.DateLanguages[0].Selected = true;

            return PartialView("~/Views/Shared/_SearchRule.cshtml", model);
        }

        [ChildActionOnly]
        public PartialViewResult ArticlesGroupTemplate()
        {
            var model = new EditArticlesGroupModel
            {
                SpiderTypes = SpiderTypes.GetItems().ToArray(ViewModelMapper.Map<Item, ItemViewModel>)
            };
            return PartialView("~/Views/ArticlesGroup/_ArticlesGroupSettingTab.cshtml", model);
        }
    }
}
