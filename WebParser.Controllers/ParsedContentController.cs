﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc;
using Kendo.Mvc.Infrastructure.Implementation;
using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.Services.ArticleRating;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.Common.MetroStations;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Helpers;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.ParsedContent;
using WebParser.Controllers.ViewModels.Shared;
using VoteResultType = WebParser.Common.VoteResultType;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "User,Admin")]
    public class ParsedContentController : ViewController
    {
        private IParsedContentService ParsedContentService { get; }

        private ISiteStructureService SiteStructureService { get; }

        private IArticlesGroupService ArticlesGroupService { get; }

        private IArticleRatingService ArticleRatingService { get; }

        private IUserService UserService { get; }

        private IMetroMapService MetroMapService { get; }

        public ParsedContentController(
            IParsedContentService parsedContentService,
            ISiteStructureService siteStructureService,
            IArticlesGroupService articlesGroupService,
            IArticleRatingService articleRatingService,
            IUserService userService,
            IMetroMapService metroMapService)
        {
            ParsedContentService = parsedContentService;
            SiteStructureService = siteStructureService;
            ArticlesGroupService = articlesGroupService;
            ArticleRatingService = articleRatingService;
            UserService = userService;
            MetroMapService = metroMapService;
        }

        [ActionName("ViewerIndex")]
        [HttpGet]
        public ActionResult ViewerIndex(ViewerIndexViewModel model)
        {
            var sourceUsers = ArticlesGroupService.GetAvailableForViewerSourceUsers();
            var firstSourceUser = sourceUsers.LastOrDefault();

            model.SelectedSourceUserId = firstSourceUser?.Id.ToStringInvariant();
            model.SourceUsers = sourceUsers.ToArray(x => x.Map<ViewerSourceUser, ViewerSourceUserViewModel>());

            var sites = SiteStructureService.GetViewerAvailableSites();
            var firstSite = sites.FirstOrDefault(x => x.UserId == firstSourceUser?.Id);

            model.Sites = sites.ToArray(x => x.Map<Site, ItemViewModel>());
            model.SelectedCityId = firstSite?.CityId.ToStringInvariant();

            return View(model);
        }

        [HttpPost]
        public JsonResult ReadViewerArticles(ReadViewerArticleFiltersModel filters, [DataSourceRequest]DataSourceRequest request)
        {
            var parsedArticleGroupIds = filters.ArticleGroupIds.ToArray(x => x.ToId());
            var site = ParsedContentService.ReadViewerArticles(
                new ViewerFilter
                {
                    CityId = filters.CityId.ToNullableInt(),
                    ArticleGroupIds = parsedArticleGroupIds,
                    MetroStationIds = filters.MetroStationIds.ToArray(x => x.ToId()),
                },
                request.ToRequestFilters(ModifyPriceFilters));
            return ConvertToPagingResult(site, x => x.Map<ViewerParsedArticleItem, ViewerParsedArticleItemViewModel>());
        }

        private void ModifyPriceFilters(IList<IFilterDescriptor> filters)
        {
            var columnName = nameof(ViewerParsedArticleItemViewModel.Price);
            var priceFilter = filters.FirstOrDefault(x => (x as FilterDescriptor)?.Member == columnName);
            if (priceFilter != null)
            {
                var aggregate = new CompositeFilterDescriptor
                {
                    LogicalOperator = FilterCompositionLogicalOperator.Or,
                    FilterDescriptors = new FilterDescriptorCollection
                    {
                        priceFilter,
                        new FilterDescriptor(columnName, FilterOperator.IsEqualTo, null),
                    }
                };

                filters.Remove(priceFilter);
                filters.Add(aggregate);
            }
        }

        [HttpPost]
        [ValidateAction]
        public JsonResult GetViewerParsedArticle(int parsedArticleId)
        {
            var article = ParsedContentService.GetViewerParsedArticle(parsedArticleId).Map<ViewerParsedArticle, ViewerParsedArticleModel>();
            return Json(article);
        }

        // TODO: Consider to remove this functionality or think about aplication for it
        [WebParserAuthorize(Roles = "Viewer,User,Admin")]
        [HttpPost]
        [ValidateAction]
        public JsonResult VoteForArticle(VoteForArticleModel model)
        {
            return Json(new
            {
                VoteResultType = (int)VoteResultType.Success,
            });
        }
    }
}
