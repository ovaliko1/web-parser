﻿using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using WebParser.Application.Interfaces.Services.Seo;
using WebParser.Common;

namespace WebParser.Controllers.Filters
{
    public class LocalizeAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var seoRoutingResult = filterContext.HttpContext.Items["HTTPCONTEXT:SEOROUTING:RESULT"] as SeoRoutingResult;

            var details = DependencyResolver.Current.GetService<ISeoRoutingService>().GetSeoDetails(filterContext.HttpContext.Request.RawUrl, seoRoutingResult);

            if (!string.IsNullOrEmpty(details.LanguageCode) &&
                !string.IsNullOrEmpty(details.CountryRegionCode))
            {
                var cultureName = $"{details.LanguageCode}-{details.CountryRegionCode}";
                var culture = CultureInfo.CreateSpecificCulture(cultureName);
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;
            }
        }
    }
}
