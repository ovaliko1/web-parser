﻿using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using WebParser.Common.Exceptions;
using AggregateException = WebParser.Common.Exceptions.AggregateException;

namespace WebParser.Controllers.Filters
{
    public class ExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void OnException(ExceptionContext exceptionContext)
        {
            if (exceptionContext.ExceptionHandled)
            {
                return;
            }

            if (!new HttpRequestWrapper(HttpContext.Current.Request).IsAjaxRequest())
            {
                HandleNonAjaxException(exceptionContext);
                return;
            }

            if (exceptionContext.Exception is AggregateException)
            {
                var exception = exceptionContext.Exception as AggregateException;
                exceptionContext.Result = new JsonResult
                {
                    Data = new ErrorResultModel(exception.Errors),
                };
                exceptionContext.ExceptionHandled = true;
                return;
            }

            if (exceptionContext.Exception is ValidationException)
            {
                var exception = exceptionContext.Exception as ValidationException;
                exceptionContext.Result = new JsonResult
                {
                    Data = new ErrorResultModel(new Dictionary<string, string[]>
                    {
                        { exception.FieldName, new [] { exception.Message } },
                    }),
                };
                exceptionContext.ExceptionHandled = true;
                return;
            }

            if (exceptionContext.Exception is CoreException)
            {
                var exception = exceptionContext.Exception as CoreException;
                exceptionContext.Result = new JsonResult
                {
                    Data = new ErrorResultModel(new[] { exception.Message })
                };
                exceptionContext.ExceptionHandled = true;
                return;
            }

            if (exceptionContext.Exception is HttpException && ((HttpException)exceptionContext.Exception).GetHttpCode() == (int)HttpStatusCode.Forbidden)
            {
                exceptionContext.Result = new JsonResult
                {
                    Data = new ErrorResultModel(new[] { exceptionContext.Exception.Message }),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                };
                exceptionContext.ExceptionHandled = true;
                return;
            }

            Log.Error("Unhandled exception during Service ajax request", exceptionContext.Exception);

            exceptionContext.Result = new JsonResult
            {
                Data = new ErrorResultModel(new[] { exceptionContext.Exception.Message })
            };
            exceptionContext.ExceptionHandled = true;
        }

        private void HandleNonAjaxException(ExceptionContext exceptionContext)
        {
            var exception = exceptionContext.Exception;
            do
            {
                var httpException = exception as HttpException;
                if (httpException != null)
                {
                    var httpStatusCode = (HttpStatusCode)httpException.GetHttpCode();

                    if (httpStatusCode == HttpStatusCode.BadRequest ||
                        httpStatusCode == HttpStatusCode.NotFound ||
                        httpStatusCode == HttpStatusCode.Unauthorized)
                    {
                        Log.Warn($"Status code: {httpStatusCode}", httpException);
                        DoRedirect(exceptionContext, ErrorController.GetActionName(httpStatusCode), "Error");
                        return;
                    }

                    if (httpStatusCode == HttpStatusCode.PaymentRequired ||
                        httpStatusCode == HttpStatusCode.Forbidden)
                    {
                        DoRedirect(exceptionContext, ForbiddenController.GetActionName(httpStatusCode), "Forbidden");
                        return;
                    }
                }

                exception = exception.InnerException;
            } while (exception != null);

            Log.Error("Unhandled exception occured", exceptionContext.Exception);

            if (HttpContext.Current?.IsCustomErrorEnabled == true)
            {
                DoRedirect(exceptionContext, ErrorController.GetActionName(HttpStatusCode.InternalServerError), "Error");
            }
        }

        private void DoRedirect(ExceptionContext exceptionContext, string action, string controller)
        {
            exceptionContext.Result = new RedirectToRouteResult(new RouteValueDictionary(
                new
                {
                    action = action,
                    controller = controller
                }));

            exceptionContext.ExceptionHandled = true;
        }
    }
}
