﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WebParser.Controllers.Filters
{
    public class ErrorResultModel
    {
        public ErrorResultModel(IEnumerable<string> summaryErrors)
        {
            SummaryErrors = new List<string>(summaryErrors);
            Errors = new Dictionary<string, string[]>();
        }

        public ErrorResultModel(IDictionary<string, string[]> errors)
        {
            SummaryErrors = new Collection<string>();
            Errors = new ReadOnlyDictionary<string, string[]>(errors);
        }

        public ErrorResultModel(IDictionary<string, string[]> errors, IEnumerable<string> summaryErrors)
        {
            Errors = new ReadOnlyDictionary<string, string[]>(errors);
            SummaryErrors = new List<string>(summaryErrors);
        }

        public bool HasErrors { get { return true; } }

        public IReadOnlyDictionary<string, string[]> Errors { get; }

        public IReadOnlyCollection<string> SummaryErrors { get; }
    }
}
