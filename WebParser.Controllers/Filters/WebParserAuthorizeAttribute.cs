﻿using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using log4net;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Common;
using WebParser.Common.Mappers;

namespace WebParser.Controllers.Filters
{
    public class WebParserAuthorizeAttribute : AuthorizeAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true) ||
                                     filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

            var userService = DependencyResolver.Current.GetService<IUserService>();
            var currentUser = userService.GetCurrentUser();

            if (skipAuthorization && (currentUser == null || filterContext.IsChildAction))
            {
                return;
            }

            if (currentUser == null || !currentUser.EmailConfirmed)
            {
                filterContext.Result = new HttpUnauthorizedResult();
                return;
            }

            if (currentUser.Roles.HasRole(Role.Admin))
            {
                return;
            }

            if (userService.UserAccessExpired(currentUser))
            {
                bool allowWithExpiredAccess = filterContext.ActionDescriptor.IsDefined(typeof(AllowWithExpiredAccessAttribute), inherit: true) ||
                                              filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowWithExpiredAccessAttribute), inherit: true);
                if (allowWithExpiredAccess)
                {
                    return;
                }

                userService.SetSeenForbidden(currentUser.Id);
                HandleUnauthorizedRequestCore(filterContext, currentUser.WaitingForPayment ? HttpStatusCode.PaymentRequired : HttpStatusCode.Forbidden);
            }
        }

        protected void HandleUnauthorizedRequestCore(AuthorizationContext filterContext, HttpStatusCode statusCode)
        {
            throw new HttpException((int)statusCode, statusCode.ToString());
        }
    }
}
