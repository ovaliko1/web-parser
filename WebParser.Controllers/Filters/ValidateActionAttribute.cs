﻿using System.Linq;
using System.Web.Mvc;
using WebParser.Common.Extensions;

namespace WebParser.Controllers.Filters
{
    public class ValidateActionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var modelState = filterContext.Controller.ViewData.ModelState;
            if (modelState.IsValid)
            {
                base.OnActionExecuting(filterContext);
                return;
            }

            var errors = modelState
                .Where(x => x.Value.Errors.Any())
                .ToDictionary(x => x.Key, x => x.Value.Errors.ToArray(y => y.ErrorMessage));

            filterContext.Result = new JsonResult
            {
                Data = new ErrorResultModel(errors),
            };

            base.OnActionExecuting(filterContext);
        }
    }
}
