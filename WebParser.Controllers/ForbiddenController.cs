﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using WebParser.Application.Interfaces.Services.Settings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.Forbidden;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers
{
    [AllowWithExpiredAccess]
    [WebParserAuthorize(Roles = "Viewer,User,Admin")]
    public class ForbiddenController : ViewController
    {
        private ICommonSettings CommonSettings { get; }

        private IUserService UserService { get; }

        public ForbiddenController(
            ICommonSettings commonSettings,
            IUserService userService)
        {
            CommonSettings = commonSettings;
            UserService = userService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult WaitingForPayment()
        {
            var currentUser = UserService.GetCurrentUser();
            if (currentUser.PurchasedAccount)
            {
                return Redirect("/");
            }

            UserService.UpdateWaitingForPaymentStatus();

            var model = new WaitingForPaymentViewModel
            {
                SupportEmail = CommonSettings.GetSupportEmail(),
                UserIdentity = currentUser.PublicId.ToString(),
            };

            return View(model);
        }

        [HttpPost]
        public JsonResult GetPaymentFormSettings()
        {
            var model = new PaymentFormSettings
            {
                FirstRateOpeningsCount = CommonSettings.GetFirstRateOpeningsCount(),
                FirstRateOpeningsPrice = CommonSettings.GetFirstRateOpeningsPrice(),
                SecondRateOpeningsCount = CommonSettings.GetSecondRateOpeningsCount(),
                SecondRateOpeningsPrice = CommonSettings.GetSecondRateOpeningsPrice(),
                ThirdRateOpeningsCount = CommonSettings.GetThirdRateOpeningsCount(),
                ThirdRateOpeningsPrice = CommonSettings.GetThirdRateOpeningsPrice(),
                FourthRateOpeningsCount = CommonSettings.GetFourthRateOpeningsCount(),
                FourthRateOpeningsPrice = CommonSettings.GetFourthRateOpeningsPrice(),
                FirstRateDaysCount = CommonSettings.GetFirstRateDaysCount(),
                FirstRateDaysPrice = CommonSettings.GetFirstRateDaysPrice(),
                SecondRateDaysCount = CommonSettings.GetSecondRateDaysCount(),
                SecondRateDaysPrice = CommonSettings.GetSecondRateDaysPrice(),
                ThirdRateDaysCount = CommonSettings.GetThirdRateDaysCount(),
                ThirdRateDaysPrice = CommonSettings.GetThirdRateDaysPrice(),
                FourthRateDaysCount = CommonSettings.GetFourthRateDaysCount(),
                FourthRateDaysPrice = CommonSettings.GetFourthRateDaysPrice(),
                Label = UserService.GetCurrentUser().PublicId.ToString(),
                Receiver = CommonSettings.GetYandexPaymentReceiver(),
                SuccessUrl = Url.Action(nameof(WaitingForPayment), "Forbidden", new RouteValueDictionary(), "http"),
                SupportEmail = CommonSettings.GetSupportEmail(),
            };
            return JsonCamelCase(model);
        }

        [ActionName("ConfirmYandexPayment")]
        [AllowAnonymous]
        [ValidateAction]
        [HttpPost]
        public void ConfirmYandexPayment(YandexPaymentConfirmationViewModel model)
        {
            UserService.TryAcceptYandexPaymentConfirmation(model.Map<YandexPaymentConfirmationViewModel, YandexPaymentConfirmation>());
        }

        public static string GetActionName(HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.Forbidden:
                    return nameof(Index);

                case HttpStatusCode.PaymentRequired:
                    return nameof(WaitingForPayment);

                default:
                    throw new ArgumentOutOfRangeException(nameof(statusCode));
            }
        }
    }
}
