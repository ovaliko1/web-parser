﻿using System;
using System.Linq;
using System.Web.Mvc;
using WebParser.Application.Interfaces.Services.Apartments;
using WebParser.Application.Interfaces.Services.Apartments.Entities;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.Common.MetroStations;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.Apartments;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers
{
    public class ApartmentsController : ViewController
    {
        private IApartmentsService ApartmentsService { get; }

        private ISiteStructureService SiteStructureService { get; }

        private IArticlesGroupService ArticlesGroupService { get; }

        private IMetroMapService MetroMapService { get; }

        public ApartmentsController(
            IApartmentsService apartmentsService,
            ISiteStructureService siteStructureService,
            IArticlesGroupService articlesGroupService,
            IMetroMapService metroMapService)
        {
            ApartmentsService = apartmentsService;
            SiteStructureService = siteStructureService;
            ArticlesGroupService = articlesGroupService;
            MetroMapService = metroMapService;
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetFiltersInitData(int? cityId)
        {
            var sourceUsers = ArticlesGroupService.GetAvailableForViewerSourceUsers();

            var sourceUser = sourceUsers.FirstOrDefault(x => x.Id == cityId) ?? sourceUsers[0];

            var sources = SiteStructureService.GetViewerAvailableSites(sourceUser.Id).ToArray(x => x.Map<Item, ItemViewModel>());
            var metroStations = MetroMapService.GetStations(sourceUser.CityId).ToArray(x => x.Map<MetroStation, ItemViewModel>());

            return JsonCamelCase(new FiltersInitDataModel
            {
                Cities =  sourceUsers.MoveItemsToBegin(x => x == sourceUser).ToArray(x => x.Map<ViewerSourceUser, ItemViewModel>()),
                Sources = sources,
                MetroStations = metroStations,
                ApartmentTypes = sourceUser.ArticleGroups.ToArray(x => x.Map<ViewerArticleGroup, ApartmentType>()),
                DefaultCityId = sourceUser.Id,
            }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAction]
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetApartments(int page, int pageSize, ApartmentFiltersModel filters)
        {
            var result = ApartmentsService
                .ReadApartments(page, pageSize, filters.Map<ApartmentFiltersModel, ApartmentFilters>())
                .Map<ApartmentList, ApartmentsModel>();

            return JsonCamelCase(result);
        }

        [ValidateAction]
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetApartment(int id)
        {
            var apartment = ApartmentsService.GetApartment(id).Map<Apartment, ApartmentModel>();
            return JsonCamelCase(apartment);
        }

        [AllowWithExpiredAccess]
        [WebParserAuthorize(Roles = "Viewer,User,Admin")]
        [ValidateAction]
        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetContactInfo(int apartmentId)
        {
            var contactInfo = ApartmentsService.GetContactInfo(apartmentId).Map<ContactInfo, ContactInfoModel>();
            return JsonCamelCase(contactInfo);
        }
    }
}
