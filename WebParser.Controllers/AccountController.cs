﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using log4net;
using WebParser.Application.Interfaces.Services.DateTimeProcessing;
using WebParser.Application.Interfaces.Services.Settings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Common;
using WebParser.Common.Exceptions;
using WebParser.Common.Extensions;
using WebParser.Controllers.ViewModels.Account;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.Results;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "Viewer,User,Admin")]
    public class AccountController : ViewController
    {
        private IUserService UserService { get; }

        private ICommonSettings CommonSettings { get; }

        private IDateTimeService DateTimeService { get; }

        public AccountController(
            IUserService userService,
            ICommonSettings commonSettings,
            IDateTimeService dateTimeService)
        {
            UserService = userService;
            CommonSettings = commonSettings;
            DateTimeService = dateTimeService;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (UserService.GetCurrentUserId().HasValue)
            {
                return Redirect("/");
            }

            ViewBag.ReturnUrl = returnUrl;
            var model = new LoginViewModel
            {
                ExternalLoginViewModel = UserService.GetExternalLoginInfo().Map<ExternalLoginDetails, ExternalLoginViewModel>(),
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await UserService.Login(model.Email, model.Password, model.RememberMe);
            if (result.Length > 0)
            {
                AddErrors(result);
                return View(model);
                
            }
            return Redirect(string.IsNullOrEmpty(returnUrl) ? "/" : returnUrl);
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl, string error)
        {
            if (!string.IsNullOrEmpty(error))
            {
                if (error.CompareIgnoreCase("access_denied"))
                {
                    return View("ExternalLoginFailed", new ExternalLoginFailedViewModel
                    {
                        SupportEmail = CommonSettings.GetSupportEmail(),
                        ErrorMessages = new[] { "Вход не был произведен." },
                    });
                }
                Log.Error($"An error occurs during ExternalLoginCallback. Error: {error}");
                return Redirect(string.IsNullOrEmpty(returnUrl) ? "/" : returnUrl);
            }

            var result = await UserService.ExternalLogin(GetRequestIp());
            if (result.Success)
            {
                return Redirect(string.IsNullOrEmpty(returnUrl) ? "/" : returnUrl);
            }

            return View("ExternalLoginFailed", new ExternalLoginFailedViewModel
            {
                SupportEmail = CommonSettings.GetSupportEmail(),
                ErrorMessages = result.Errors,
            });
        }

        public ActionResult Logout()
        {
            UserService.SignOut();
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var callbackUrlFormat = HttpUtility.UrlDecode(Url.Action("ResetPassword", "Account", new { code = "{0}" }, protocol: Request?.Url?.Scheme));
            await UserService.ForgotPassword(model.Email, callbackUrlFormat);
            return View("ForgotPasswordConfirmation");
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View(new ResetPasswordViewModel
            {
                Code = code,
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var errors = await UserService.ResetPassword(model.Email, model.Code, model.Password);
            if (errors.Length > 0)
            {
                AddErrors(errors);
                return View();
            }

            return RedirectToAction("ResetPasswordConfirmation", "Account");
        }

        private void AddErrors(string[] errors)
        {
            foreach (var error in errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAction]
        public async Task ChangePassword(ChangePasswordViewModel model)
        {
            await UserService.ChangePassword(model.OldPassword, model.NewPassword);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [ActionName("RegisterViewer")]
        [HttpGet]
        [AllowAnonymous]
        public ActionResult RegisterViewer(string returnUrl)
        {
            if (UserService.GetCurrentUserId().HasValue)
            {
                return Redirect("/");
            }

            ViewBag.ReturnUrl = returnUrl;

            var model = new RegisterViewerViewModel
            {
                ExternalLoginViewModel = UserService.GetExternalLoginInfo().Map<ExternalLoginDetails, ExternalLoginViewModel>(),
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> RegisterViewer(RegisterViewerViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var callbackUrlFormat = HttpUtility.UrlDecode(Url.Action("ConfirmEmail", "Account", new { code = "{0}", userId = "{1}", returnUrl }, protocol: Request?.Url?.Scheme));
            var accountInfo = new AccountInfo
            {
                Email = model.Email,
                Password = model.Password,
                Ip = GetRequestIp(),
            };

            var result = await UserService.CreateViewer(accountInfo, callbackUrlFormat);
            if (result.Length > 0)
            {
                AddErrors(result);
                return View(model);
            }

            return View("RegisterConfirmation");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code, string returnUrl)
        {
            var model = new RegisterFailedViewModel
            {
                UserId = userId,
                Code = code,
                SupportEmail = CommonSettings.GetSupportEmail(),
                Now = DateTimeService.GetCurrentDateTimeUtc().ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToStringInvariant(ViewModelMapper.NumericDateTimeFormat),
                ReturnUrl = returnUrl,
            };

            try
            {
                var id = userId.ToNullableInt();
                if (!id.HasValue || string.IsNullOrEmpty(code))
                {
                    return View("RegisterFailed", model);
                }

                if (!await UserService.ConfirmEmail(id.Value, code.Replace(' ', '+')))
                {
                    return View("RegisterFailed", model);
                }
            }
            catch (ConvertException ex)
            {
                Log.Info($"ConvertException - userId: {userId}", ex);
                return View("RegisterFailed", model);
            }
            catch (Exception ex)
            {
                Log.Error("Unhandled exception during ConfirmEmail", ex);
                return View("RegisterFailed", model);
            }

            return Redirect(string.IsNullOrEmpty(returnUrl) ? "/" : returnUrl);
        }
    }
}
