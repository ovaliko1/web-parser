﻿using System.Web.Mvc;

using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.Services.BlackList;
using WebParser.Application.Interfaces.Services.BlackList.Entities;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Common.Extensions;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Helpers;
using WebParser.Controllers.ViewModels.BlackListReport;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "Admin,User")]
    public class BlackListReportController : ViewController
    {
        private ISiteStructureService SiteStructureService { get; }

        private IBlackListReportService BlackListReportService { get; }

        private IUserService UserService { get; }

        public BlackListReportController(
            ISiteStructureService siteStructureService,
            IBlackListReportService blackListReportService,
            IUserService userService)
        {
            SiteStructureService = siteStructureService;
            BlackListReportService = blackListReportService;
            UserService = userService;
        }

        public ActionResult Index(int? id)
        {
            var sites = SiteStructureService.GetAllSites();
            var model = new BlackListReportViewModel
            {
                Id = id,
                Sites = sites.ToArray(x => x.Map<AdminSite, ItemViewModel>()),
                Users = UserService.GetAllUsers().ToArray(x => x.Map<UserItem, ItemViewModel>())
            };
            return View(model);
        }

        [HttpPost]
        public JsonResult ReadBlackList([DataSourceRequest]DataSourceRequest request)
        {
            var items = BlackListReportService.ReadBlackListItems(request.ToRequestFilters());
            return ConvertToPagingResult(items, x => x.Map<BlackListItem, BlackListItemViewModel>());
        }

        [HttpPost]
        public JsonResult ReadHitedArticles([DataSourceRequest]DataSourceRequest request, int blackListItemId)
        {
            var items = BlackListReportService.ReadHitedArticles(request.ToRequestFilters(), blackListItemId);
            return ConvertToPagingResult(items, x => x.Map<HitedArticle, HitedArticleViewModel>());
        }

        [HttpPost]
        [ValidateAction]
        public void AddBlackListItem(BlackListItemViewModel model)
        {
            var item = model.Map<BlackListItemViewModel, BlackListItem>();
            BlackListReportService.AddBlackListItem(item);
        }

        [HttpPost]
        [ValidateAction]
        public void DeleteBlackListItem(int id)
        {
            BlackListReportService.DeleteBlackListItem(id);
        }

        [HttpPost]
        [ValidateAction]
        public void UpdateBlackListItem(BlackListItemViewModel model)
        {
            var item = model.Map<BlackListItemViewModel, BlackListItem>();
            // TODO: Implement it.
        }
    }
}
