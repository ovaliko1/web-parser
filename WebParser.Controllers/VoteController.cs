﻿using System.Web.Mvc;
using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.Services.ArticleRating;
using WebParser.Common;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Helpers;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.Vote;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "Viewer,User,Admin")]
    public class VoteController : ViewController
    {
        public VoteController(
            IArticleRatingService articleRatingService)
        {
            ArticleRatingService = articleRatingService;
        }

        private IArticleRatingService ArticleRatingService { get; }

        public ActionResult UnwatchedVotesReport()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ReadUnwatchedVotes([DataSourceRequest]DataSourceRequest request)
        {
            var items = ArticleRatingService.ReadUnwatchedVotes(request.ToRequestFilters());
            return ConvertToPagingResult(items, x => x.Map<UnwatchedVote, UnwatchedVoteViewModel>());
        }

        [HttpPost]
        [ValidateAction]
        public void ConfirmVote(int voteId)
        {
            ArticleRatingService.SaveVoteReviewResult(voteId, ReviewResult.Confirmed);
        }

        [HttpPost]
        [ValidateAction]
        public void RejectVote(int voteId)
        {
            ArticleRatingService.SaveVoteReviewResult(voteId, ReviewResult.Rejected);
        }

        [AllowWithExpiredAccess]
        [HttpPost]
        [ValidateAction]
        public JsonResult VoteForApartment(VoteForApartmentModel model)
        {
            var voteResult = ArticleRatingService.Vote(model.ApartmentId, (ArticleRatingType)model.VoteType);
            return JsonCamelCase(new
            {
                VoteResultType = (int)voteResult
            });
        }
    }
}