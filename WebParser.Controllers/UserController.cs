﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Helpers;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.User;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "Admin")]
    public class UserController : ViewController
    {
        private IUserService UserService { get; }
        private ICityService CityService { get; }

        public UserController(
            IUserService userService,
            ICityService cityService)
        {
            UserService = userService;
            CityService = cityService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new UserListViewModel
            {
                Cities = CityService.GetCities().Select(x => x.Map<City, ItemViewModel>()).ToArray(),
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAction]
        public async Task<ActionResult> Create(UserItemViewModel model)
        {
            var user = model.Map<UserItemViewModel, UserDetails>();
            var callbackUrlFormat = HttpUtility.UrlDecode(Url.Action("ResetPassword", "Account", new { code = "{0}" }, protocol: Request?.Url?.Scheme));

            var userId = await UserService.CreateUser(user, callbackUrlFormat);
            return Json(new { userId });
        }

        [HttpPost]
        [ValidateAction]
        public async Task<ActionResult> CreatePublisher(PublisherItemViewModel model)
        {
            var publisher = model.Map<PublisherItemViewModel, PublisherDetails>();
            var callbackUrlFormat = HttpUtility.UrlDecode(Url.Action("ResetPassword", "Account", new { code = "{0}" }, protocol: Request?.Url?.Scheme));

            var userId = await UserService.CreatePublisher(publisher, callbackUrlFormat);
            return Json(new { userId });
        }

        [HttpPost]
        [ValidateAction]
        public void Edit(UserItemViewModel model)
        {
            UserService.EditUser(model.Map<UserItemViewModel, UserDetails>());
        }

        [HttpPost]
        [ValidateAction]
        public void EditViewer(ViewerItemViewModel model)
        {
            UserService.EditViewer(model.Map<ViewerItemViewModel, ViewerDetails>());
        }

        [HttpPost]
        [ValidateAction]
        public void EditPublisher(PublisherItemViewModel model)
        {
            UserService.EditPublisher(model.Map<PublisherItemViewModel, PublisherDetails>());
        }

        [HttpPost]
        public JsonResult ReadUsers([DataSourceRequest]DataSourceRequest request)
        {
            var users = UserService.ReadUsers(request.ToRequestFilters());
            return ConvertToPagingResult(users, ViewModelMapper.Map<UserDetails, UserItemViewModel>);
        }

        [HttpPost]
        public JsonResult ReadViewers([DataSourceRequest]DataSourceRequest request)
        {
            var users = UserService.ReadViewers(request.ToRequestFilters());
            return ConvertToPagingResult(users, ViewModelMapper.Map<ViewerDetails, ViewerItemViewModel>);
        }

        [HttpPost]
        public JsonResult ReadPublishers([DataSourceRequest]DataSourceRequest request)
        {
            var users = UserService.ReadPublishers(request.ToRequestFilters());
            return ConvertToPagingResult(users, ViewModelMapper.Map<PublisherDetails, PublisherItemViewModel>);
        }
    }
}
