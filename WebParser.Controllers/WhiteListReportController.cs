﻿using System.Web.Mvc;

using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.Services.WhiteList;
using WebParser.Application.Interfaces.Services.WhiteList.Entities;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Common.Extensions;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Helpers;
using WebParser.Controllers.ViewModels.WhiteListReport;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "Admin,User")]
    public class WhiteListReportController : ViewController
    {
        private ISiteStructureService SiteStructureService { get; }

        private IWhiteListReportService WhiteListReportService { get; }

        private IUserService UserService { get; }

        public WhiteListReportController(
            ISiteStructureService siteStructureService,
            IWhiteListReportService whiteListReportService,
            IUserService userService)
        {
            SiteStructureService = siteStructureService;
            WhiteListReportService = whiteListReportService;
            UserService = userService;
        }

        public ActionResult Index(int? id)
        {
            var sites = SiteStructureService.GetAllSites();
            var model = new WhiteListReportViewModel
            {
                Id = id,
                Sites = sites.ToArray(x => x.Map<AdminSite, ItemViewModel>()),
                Users = UserService.GetAllUsers().ToArray(x => x.Map<UserItem, ItemViewModel>())
            };
            return View(model);
        }

        [HttpPost]
        public JsonResult ReadWhiteList([DataSourceRequest]DataSourceRequest request)
        {
            var items = WhiteListReportService.ReadWhiteListItems(request.ToRequestFilters());
            return ConvertToPagingResult(items, x => x.Map<WhiteListItem, WhiteListItemViewModel>());
        }

        [HttpPost]
        public JsonResult ReadHitedArticles([DataSourceRequest]DataSourceRequest request, int itemId)
        {
            var items = WhiteListReportService.ReadHitedArticles(request.ToRequestFilters(), itemId);
            return ConvertToPagingResult(items, x => x.Map<HitedWhiteArticle, HitedWhiteArticleViewModel>());
        }

        [HttpPost]
        [ValidateAction]
        public void AddWhiteListItem(WhiteListItemViewModel model)
        {
            var item = model.Map<WhiteListItemViewModel, WhiteListItem>();
            WhiteListReportService.AddWhiteListItem(item);
        }

        [HttpPost]
        [ValidateAction]
        public void DeleteWhiteListItem(int id)
        {
            WhiteListReportService.DeleteWhiteListItem(id);
        }

        [HttpPost]
        [ValidateAction]
        public void UpdateWhiteListItem(WhiteListItemViewModel model)
        {
            var item = model.Map<WhiteListItemViewModel, WhiteListItem>();
            // TODO: Implement it.
        }
    }
}
