﻿using System;
using System.Reflection;
using System.Web.Mvc;
using log4net;
using WebParser.Application.Interfaces.DomainEntities;
using WebParser.Common.Extensions;
using WebParser.Controllers.Results;

namespace WebParser.Controllers
{
    public abstract class ViewController : Controller
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected JsonResult ConvertToPagingResult<TEntity, TViewModel>(PagingResult<TEntity> pagingResult, Func<TEntity, TViewModel> convert)
        {
            var result = new
            {
                Data = pagingResult.Items.ToArray(convert),
                Total = pagingResult.TotalCount,
            };
            return Json(result);
        }

        protected JsonResult ConvertToPagingResult<TEntity, TViewModel>(TEntity[] items, Func<TEntity, TViewModel> convert)
        {
            return ConvertToPagingResult(items.ToArray(convert));
        }

        protected JsonResult ConvertToPagingResult<TEntity>(TEntity[] items)
        {
            return Json(new
            {
                Data = items,
                Total = items.Length,
            });
        }

        protected JsonResult JsonCamelCase(object data)
        {
            return JsonCamelCase(data, JsonRequestBehavior.DenyGet);
        }

        protected JsonResult JsonCamelCase(object data, JsonRequestBehavior behavior)
        {
            return new CamelCaseJsonResult
            {
                Data = data,
                JsonRequestBehavior = behavior
            };
        }

        protected string GetRequestIp()
        {
            return HttpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? HttpContext.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}
