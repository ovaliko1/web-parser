﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Interfaces.Services.Parsing;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Helpers;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.ArticleGroup;
using WebParser.Controllers.ViewModels.Shared;
using ArticlesGroupItemViewModel = WebParser.Controllers.ViewModels.ArticleGroup.ArticlesGroupItemViewModel;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "User,Admin")]
    public class ArticlesGroupController : ViewController
    {
        private ISiteStructureService SiteStructureService { get; }

        private IParsingService ParsingService { get; }

        private IFileService FileService { get; }

        private IUserService UserService { get; }

        public ArticlesGroupController(
            ISiteStructureService siteStructureService,
            IParsingService parsingService,
            IFileService fileService,
            IUserService userService)
        {
            SiteStructureService = siteStructureService;
            ParsingService = parsingService;
            FileService = fileService;
            UserService = userService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = SiteStructureService.GetAvailableSites().ToArray(x => x.Map<Site, ItemViewModel>());
            return View(model);
        }

        [HttpPost]
        public JsonResult GetArticleGroupsBySite([DataSourceRequest]DataSourceRequest request, int siteId)
        {
            var categories = SiteStructureService.GetArticleGroupsBySite(siteId, request.ToRequestFilters());
            return ConvertToPagingResult(categories, x => x.Map<ArticlesGroup, ArticlesGroupItemViewModel>());
        }

        [HttpPost]
        public async Task<JsonResult> GetArticleGroupItemsBySite(int siteId)
        {
            var items = (await SiteStructureService.GetArticleGroupItemBySite(siteId))
                .ToArray(x => x.Map<Item, ItemViewModel>());
            return Json(items);
        }

        [HttpPost]
        [ValidateAction]
        public JsonResult CreateArticlesGroup(CreatePreviewArticlesGroupModel model)
        {
            var articlesGroup = model.Map<CreatePreviewArticlesGroupModel, ArticlesGroup>();
            var articlesGroupId = SiteStructureService.CreateArticlesGroup(model.SiteId, articlesGroup);
            return Json(new { success = true, message = "Success!", articlesGroupId });
        }

        [HttpPost]
        [ValidateAction]
        public JsonResult EditArticlesGroup(EditArticlesGroupModel model)
        {
            var articlesGroup = model.Map<EditArticlesGroupModel, ArticlesGroup>();
            SiteStructureService.EditArticlesGroup(articlesGroup);
            return Json(new { success = true, message = "Success!" });
        }

        [HttpPost]
        [ValidateAction]
        public JsonResult DeleteArticlesGroup(int articlesGroupId)
        {
            SiteStructureService.DeleteArticlesGroup(articlesGroupId);
            return Json("Success!");
        }

        [HttpPost]
        public async Task<JsonResult> UploadLinksFile(HttpPostedFileBase linksFile)
        {
            var user = UserService.GetCurrentUser();

            var file = await FileService.CreatePersistentFile(user.Id, null, new FileDetails
            {
                FileName = linksFile.FileName,
                ContentType = linksFile.ContentType,
                FileData = linksFile.InputStream.GetBytes(),
            });

            return Json(new
            {
                fileId = file.Id,
            });
        }

        [HttpPost]
        [ValidateAction]
        public async Task<JsonResult> PreviewParsingResult(CreatePreviewArticlesGroupModel model)
        {
            var articlesGroup = model.Map<CreatePreviewArticlesGroupModel, ArticlesGroup>();
            var parsingResult = await ParsingService.GetParsingResultPreview(model.SiteId, articlesGroup);
            var previewResult = parsingResult.Map<ParsedArticle, ParsedArticleModel>();
            return Json(previewResult);
        }
    }
}
