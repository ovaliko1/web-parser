﻿using System;
using System.Globalization;
using System.Web.Mvc;
using WebParser.Application.Interfaces.Services.Settings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "Viewer,User,Admin")]
    public class SharedController : ViewController
    {
        private IUserService UserService { get; }

        private ICommonSettings CommonSettings { get; }

        public SharedController(
            IUserService userService,
            ICommonSettings commonSettings)
        {
            UserService = userService;
            CommonSettings = commonSettings;
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public PartialViewResult UserMenu()
        {
            var model = UserService.GetCurrentUserVoteInfo().Map<UserVoteInfo, UserMenuViewModel>();
            return PartialView("~/Views/Shared/_UserMenu.cshtml", model);
        }

        [ChildActionOnly]
        public PartialViewResult LeftSideBar()
        {
            var currentUser = UserService.GetCurrentUser();
            var model = new LeftSideBarViewModel
            {
                ShowAdminMenuItems = currentUser.Roles.HasRole(Role.Admin),
                ShowUserMenuItems = currentUser.Roles.HasRole(Role.User),
                ShowViewerMenuItems = currentUser.Roles.HasRole(Role.Viewer),
            };
            return PartialView("~/Views/Shared/_LeftSideBar.cshtml", model);
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public PartialViewResult PublicApplicationClientSettings()
        {
            var model = new PublicApplicationClientSettingsModel
            {
                IsAnonymous = !UserService.GetCurrentUserId().HasValue,
                SupportEmail = CommonSettings.GetSupportEmail(),
                RenderMetrics = RenderMetrics(),
                YandexMetrikaCounterName = CommonSettings.GetYandexMetrikaCounterName(),
            };
            return PartialView("~/Views/Shared/_PublicApplicationClientSettings.cshtml", model);
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public PartialViewResult Metrics()
        {
            return PartialView("~/Views/Shared/_Metrics.cshtml", RenderMetrics());
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public PartialViewResult YandexMetikaUserInfo()
        {
            var currentUser = UserService.GetCurrentUser();
            if (!RenderMetrics(currentUser))
            {
                return PartialView("~/Views/Shared/_YandexMetikaUserInfo.cshtml", null);
            }
            
            if (currentUser == null)
            {
                return PartialView("~/Views/Shared/_YandexMetikaUserInfo.cshtml", null);
            }

            var model = new YandexMetrikaUserInfoViewModel
            {
                CounterName = CommonSettings.GetYandexMetrikaCounterName(),
                UserId = currentUser.PublicId.ToString(),
                PurchasedAccount = currentUser.PurchasedAccount.ToString(CultureInfo.InvariantCulture).ToLowerInvariant(),
                AccessDateTime = currentUser.AccessDateTime.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToString(ViewModelMapper.NumericDateTimeFormat),
                CreatedDate = currentUser.CreatedDate.ConvertUtcDateTimeToTimeZone(Constants.DefaultTimeZoneId).ToString(ViewModelMapper.NumericDateTimeFormat),
                VkId = currentUser.VkId,
                Email = currentUser.Email,
                FreeHours = currentUser.FreeHours,
                WaitingForPayment = currentUser.WaitingForPayment.ToString(CultureInfo.InvariantCulture).ToLowerInvariant(),
            };
            return PartialView("~/Views/Shared/_YandexMetikaUserInfo.cshtml", model);
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public PartialViewResult ClientApplicationPreloader()
        {
            return PartialView("~/Views/Shared/_ClientApplicationPreloader.cshtml");
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public PartialViewResult ClientSettings()
        {
            return PartialView("~/Views/Shared/_ClientSettings.cshtml", new ClientSettingsViewModel
            {
                SupportEmail = CommonSettings.GetSupportEmail(),
            });
        }

        private bool RenderMetrics()
        {
            return RenderMetrics(UserService.GetCurrentUser());
        }

        private bool RenderMetrics(User currentUser)
        {
            return (currentUser?.Roles.HasOnlyRole(Role.Viewer) ?? true) && CommonSettings.GetRenderMetrics();
        }

        // TODO: Move to DataController. Here should be child actions only.
        [Obsolete("Api for old public site")]
        [HttpPost]
        public JsonResult GetUserMenuCounts()
        {
            var model = UserService.GetCurrentUserVoteInfo().Map<UserVoteInfo, UserMenuViewModel>();
            return Json(model);
        }

        [HttpGet]
        public JsonResult GetUserCounters()
        {
            var model = UserService.GetCurrentUserVoteInfo().Map<UserVoteInfo, UserMenuViewModel>();
            return JsonCamelCase(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryExtendUserAccessForSharing()
        {
            UserService.TryExtendUserAccessForSharing();
            return JsonCamelCase("ok");
        }
    }
}
