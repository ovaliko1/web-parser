﻿using System.Web.Mvc;
using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.Services.Reports;
using WebParser.Application.Interfaces.Services.Reports.Entities;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Common.Extensions;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Helpers;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.BlackAndWhiteArticlesReport;
using WebParser.Controllers.ViewModels.Shared;

namespace WebParser.Controllers
{
    [WebParserAuthorize(Roles = "Admin,User")]
    public class BlackAndWhiteArticlesReportController : ViewController
    {
        private ISiteStructureService SiteStructureService { get; }

        private IBlackAndWhiteArticlesReportService BlackAndWhiteArticlesReportService { get; }

        public BlackAndWhiteArticlesReportController(
            ISiteStructureService siteStructureService,
            IBlackAndWhiteArticlesReportService blackAndWhiteArticlesReportService)
        {
            SiteStructureService = siteStructureService;
            BlackAndWhiteArticlesReportService = blackAndWhiteArticlesReportService;
        }

        public ActionResult Index()
        {
            var sites = SiteStructureService.GetAllSites();
            var model = new BlackAndWhiteArticlesReportViewModel
            {
                Sites = sites.ToArray(x => x.Map<AdminSite, ItemViewModel>()),
            };
            return View(model);
        }

        [HttpPost]
        public JsonResult ReadBlackAndWhiteArticles([DataSourceRequest]DataSourceRequest request)
        {
            var items = BlackAndWhiteArticlesReportService.ReadBlackAndWhiteArticles(request.ToRequestFilters());
            return ConvertToPagingResult(items, x => x.Map<BlackAndWhiteArticle, BlackAndWhiteArticleViewModel>());
        }
    }
}
