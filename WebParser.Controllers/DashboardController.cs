﻿using System.Web.Mvc;
using Kendo.Mvc.UI;
using WebParser.Application.Interfaces.Services.Parsing.ParsingProgress;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Interfaces.Services.UserManagement.Entities;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Common.Mappers;
using WebParser.Controllers.Filters;
using WebParser.Controllers.Helpers;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.Dashboard;
using WebParser.Controllers.ViewModels.Shared;
using IndexViewModel = WebParser.Controllers.ViewModels.Dashboard.IndexViewModel;

namespace WebParser.Controllers
{
    public class DashboardController : ViewController
    {
        private IParsingProgressService ParsingProgressService { get; }

        private ISiteStructureService SiteStructureService { get; }

        private IUserService UserService { get; }

        public DashboardController(
            IParsingProgressService parsingProgressService,
            ISiteStructureService siteStructureService,
            IUserService userService)
        {
            ParsingProgressService = parsingProgressService;
            SiteStructureService = siteStructureService;
            UserService = userService;
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            var currentUser = UserService.GetCurrentUser();
            if (currentUser == null || currentUser.Roles.HasOnlyRole(Role.Viewer))
            {
                return Redirect("apartments");
            }

            var isAdmin = currentUser.Roles.HasRole(Role.Admin);
            if (!isAdmin && UserService.UserAccessExpired(currentUser))
            {
                return RedirectToAction("Index", "Forbidden");
            }

            var model = new IndexViewModel
            {
                Sites = SiteStructureService.GetAvailableSites().ToArray(x => x.Map<Site, ItemViewModel>()),
                Users = isAdmin ? UserService.GetAllUsers().ToArray(x => x.Map<UserItem, ItemViewModel>()) : new ItemViewModel[0],
                ShowUserColumn = isAdmin,
            };

            return View(model);
        }

        [WebParserAuthorize(Roles = "User,Admin")]
        [HttpPost]
        public JsonResult ReadLog([DataSourceRequest]DataSourceRequest request)
        {
            var log = ParsingProgressService.GetParsingLog(request.ToRequestFilters());
            return ConvertToPagingResult(log, x => x.Map<LogEntry, LogEntryViewModel>());
        }
    }
}
