﻿using System.Web;
using System.Web.Optimization;

namespace WebParser.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            CreateCssBundles(bundles);
            CreateScriptBundles(bundles);
        }

        private static void CreateScriptBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/static/scripts/commonScripts").Include(
                "~/static/scripts/jquery/jquery-{version}.js",
                "~/static/scripts/jquery/jquery.validate*",
                "~/static/scripts/jquery/jquery-ui-{version}.js",
                "~/static/scripts/jquery/jquery.form.js",
                "~/static/scripts/jquery/jquery.blockUI.js",
                "~/static/scripts/bootstrap/bootstrap.js",
                "~/static/photoswipe/photoswipe-ui-default.js",
                "~/static/photoswipe/photoswipe.js",
                "~/static/scripts/jquery/respond.js",
                "~/static/scripts/framework/underscore.js",
                "~/static/scripts/framework/backbone.js",
                "~/static/scripts/framework/taggle.js",
                "~/static/scripts/framework/gallery.js"
                ));

            bundles.Add(new ScriptBundle("~/static/scripts/kendo").Include(
                "~/static/kendo/scripts/kendo.all.min.js",
                "~/static/kendo/scripts/kendo.aspnetmvc.min.js",
                "~/static/kendo/scripts/kendo.timezones.min.js"));

            bundles.Add(new ScriptBundle("~/static/scripts/siteScripts").Include(
                // Common scripts
                "~/static/scripts/common/initialization.js",
                "~/static/scripts/common/utilities.js",
                "~/static/scripts/common/additional-data-handlers.js",
                "~/static/scripts/common/validation-configuration.js",
                "~/static/scripts/common/transport.js",
                "~/static/scripts/common/validation-helpers.js",

                "~/static/scripts/common/share-buttons.js",

                // Site scripts
                "~/static/scripts/shared/search-rules-control.js",
                "~/static/scripts/behaviors/create-article-group.js",
                "~/static/scripts/behaviors/create-article.js",
                "~/static/scripts/behaviors/site-page.js",
                "~/static/scripts/behaviors/articles-group-page.js",
                "~/static/scripts/behaviors/user-page.js",
                "~/static/scripts/behaviors/change-password-page.js",
                "~/static/scripts/behaviors/viewer-parsed-content-page.js",
                "~/static/scripts/behaviors/user-counts.js",
                "~/static/scripts/behaviors/black-list-report-page.js",
                "~/static/scripts/behaviors/white-list-report-page.js",
                "~/static/scripts/behaviors/unwatched-votes-page.js"
                ));

            bundles.Add(new ScriptBundle("~/static/scripts/export/share-buttons.js").Include(
                "~/static/scripts/export/share-buttons.js"
            ));

            bundles.Add(new ScriptBundle("~/app-public/main-bundle.js").Include(
                "~/static/scripts/export/photoswipe/photoswipe.js",
                "~/static/scripts/export/photoswipe/photoswipe-ui-default.js",
                "~/app-public/main.bundle.js"
            ));
        }

        private static void CreateCssBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/static/css/common").Include(
                "~/static/css/bootstrap.css",
                "~/static/css/bootstrap-responsive.css",
                "~/static/css/jquery-ui-1.8.21.custom.css",
                "~/static/css/jquery.gritter.css",
                "~/static/css/jquery.iphone.toggle.css",
                "~/static/css/font-awesome.min.css",
                "~/static/css/glyphicons.css",
                "~/static/css/halflings.css",
                "~/static/css/uploadify.css",
                "~/static/css/style-forms.css",
                "~/static/css/taggle.css",
                "~/static/photoswipe/photoswipe.css",
                "~/static/photoswipe/default-skin/default-skin.css"
            ));

            bundles.Add(new StyleBundle("~/static/css/style").Include(
                "~/static/css/style.css",
                "~/static/css/style-responsive.css",
                "~/static/css/gallery.css",
                "~/static/css/share-buttons.css"));

            bundles.Add(new StyleBundle("~/static/css/ie").Include(
                "~/static/css/ie9.css"));

            bundles.Add(new StyleBundle("~/static/kendo/style/css").Include(
                //"~/static/kendo/style/kendo.common.min.css",
                //"~/static/kendo/style/kendo.flat.min.css",
                //"~/static/kendo/style/kendo.metro.min.css"));
                "~/static/kendo/style/kendo.common-material.min.css",
                "~/static/kendo/style/kendo.material.min.css",
                "~/static/kendo/style/kendo.material.mobile.min.css"));

            bundles.Add(new StyleBundle("~/static/css/account").Include(
                "~/static/css/account-bootstrap.css",
                "~/static/css/font-awesome.min.css",
                "~/static/css/account-style.css"
            ));

            bundles.Add(new StyleBundle("~/app-public/main-bundle.css").Include(
                "~/static/scripts/export/photoswipe/photoswipe.css",
                "~/static/scripts/export/photoswipe/default-skin/default-skin.css",
                "~/app-public/main.bundle.css"
            ));

            bundles.Add(new StyleBundle("~/static/css/seo").Include(
                "~/static/css/account-bootstrap.css",
                "~/static/css/font-awesome.min.css",
                "~/static/css/account-style.css"
            ));
        }
    }
}
