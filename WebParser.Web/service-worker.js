var dbVersion = 1;
var dbName = 'pullmeout';
var stores = {
  apartmentFilters: 'store:apartmentFilters',
};

var pollingPromise = null;

self.addEventListener('install', event => {
  console.log('installing…');
});

self.skipWaiting();

self.addEventListener('activate', event => {
  console.log('now ready to handle fetches!');

  pollingPromise = run(polResults, null, 60000);
});

self.addEventListener('message', function(event) {
  console.log('Received message from page.', event.data);
  console.log('Run promise: ', pollingPromise);
  if(!pollingPromise){
    pollingPromise = run(polResults, null, 60000);
  }
});

self.addEventListener('push', function(e) {
  console.log('Push event received', e);
  console.log('Run promise: ', pollingPromise);
  if (!pollingPromise) {
    pollingPromise = run(polResults, null, 60000);
  }
});

self.addEventListener('sync', function(e) {
  console.log('sync event received', e);
  console.log('Run promise: ', pollingPromise);
  if (!pollingPromise) {
    pollingPromise = run(polResults, null, 60000);
  }
});

var run = function (poll, data, milliseconds){
  return new Promise(resolve => {
    poll(data)
      .then(resolve)
      .then(() => runCore(poll, data, milliseconds));
  });
};

var runCore = function (poll, data, milliseconds){
  return new Promise(resolve => {
    setTimeout(() => {
      poll(data)
      .then(resolve)
      .then(() => runCore(poll, data, milliseconds))
      .catch((x) => {
        console.log(x);
        runCore(poll, data, milliseconds);
      });
    }, milliseconds);
  })
};

var polResults = function(filters){
  return filters
    ? polResultsCore(filters)
    : getObject(stores.apartmentFilters, '1')
        .then(x => x && polResultsCore(x.filters));
};

var polResultsCore = function(filters){
  if(!filters){
    return;
  }

  var request = {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'application/json'
    },
    mode: 'cors',
    credentials: 'include',
    body: JSON.stringify({
      page: 0,
      pageSize: 5,
      filters: filters
    })
  };

  var result = fetch('/Apartments/GetApartments', request)
    .then(function(response) {
      return response.json();
    })
    .then(data => {
      console.log(data);

      if (!data.items.length) {
        return;
      }
      var newest = data.items[0];
      return saveNewestApartmentId(newest.id)
        .then(() =>{
          var showNotificationPromise = showNotification(newest);
          for(var i = 1; i < data.items.length; i++){
            showNotificationPromise = appendShowNotification(showNotificationPromise, data.items[i]);
          }
          return showNotificationPromise;
        });
    });

  return result;
};

var appendShowNotification = function(showNotificationPromise, item){
  return showNotificationPromise.then(() => showNotification(item));
}

var showNotification = function(item){
  return new Promise(resolve => {
    var title = [item.articlesGroup, item.price].join(' ');
    var body = [item.metro, item.metroDistance, item.metroDuration].join(' ') + '\n' + item.createdDateStr                  
    var options = {
      body: body,
      image: item.imagesPreview.length > 0 ? item.imagesPreview[0].imageUrl : '',
      icon: '/favicon-270x270.png',
      badge: '/favicon-192x192.png',
      actions: [
        {
          action: 'open',
          title: 'Связаться'
        }
      ],
      data: {
        url: '/apartments?apartmentId=' + item.id + '&utm_source=notifications&utm_medium=notification_click&utm_campaign=notification_click',
      }
    };
    var notification = self.registration.showNotification(title, options);

    setTimeout(() => {
      resolve();
    }, 5000);
  });
}

self.addEventListener('notificationclick', function(event) {  
  console.log('On notification click: ', event.notification.tag);
  event.notification.close();
  event.waitUntil(
    clients.matchAll({
      type: "window"
    })
    .then(function(clientList) {
      for (var i = 0; i < clientList.length; i++) {  
        var client = clientList[i];  
        if (client.url == '/' && 'focus' in client)  
          return client.focus();
      }  
      if (clients.openWindow) {
        return clients.openWindow(event.notification.data.url);  
      }
    })
  );
});

var saveNewestApartmentId = function(apartmentId){
  return getObject(stores.apartmentFilters, "1")
    .then(data => {
      if(data && data.filters){
        data.filters.apartmentId = apartmentId;
        return saveObject(this.stores.apartmentFilters, {
          "PK_id": "1",
          filters: data.filters
        });
      }
      return false;
    });
}

var saveObject = function(storeName, object) {
  return new Promise((resolve, reject) => {
    if (indexedDbDoesNotSupported()){
      return resolve(false);
    }

    getTransactionStore(storeName)
      .then(store => {
        var request = store.put(object);

        request.onsuccess = function(e){
          resolve(true);
        };
        request.onerror = function(e){
          console.log('saveObject error : ' + e);
          resolve(false);
        };
      });
  });
}

var getObject = function (storeName, id){
  return new Promise((resolve, reject) => {
    if (indexedDbDoesNotSupported()){
      return resolve(null);
    }

    getTransactionStore(storeName)
      .then(store => {
        var request = store.get(id);
        request.onsuccess = function(event) {
          resolve(request.result);
        };
        request.onerror = function(e) {
          console.log('getObject error : ' + e);
          resolve(false);
        };
      });
  });
}

var getTransactionStore = function (storeName){
  return new Promise((resolve, reject) => {
    var request = indexedDB.open(this.dbName, this.dbVersion);
    request.onsuccess = function(event) {
      resolve(event.target.result.transaction([storeName], "readwrite").objectStore(storeName));
    };
    request.onerror = function(e) {
      console.log('getTransactionStore error : ' + e);
      reject(false);
    };
  });
}

function indexedDbDoesNotSupported(){
  return !self.indexedDB;
}