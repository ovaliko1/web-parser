﻿
application.additionalDataHandlers = (function () {
  var handlers = [];
  
  var addHandler = function (key, handler) {
    if (key in handlers) {
      throw new Error('Additional Data Handler with key ' + key + ' already registered');
    }
    handlers[key] = handler;
  }
  
  var removeHandler = function(key) {
    if (!(key in handlers)) {
      throw new Error('Additional Data Handler not registered with key: ' + key);
    }
    delete handlers[key];
  }

  var getHandler = function (key) {
    return function () {
      if (!(key in handlers)) {
        throw new Error('Additional Data Handler not registered');
      }
      return handlers[key]();
    }
  }

  var getSiteName = function (siteId) {
    var grid = $('#viewer-parsed-articles').data('kendoGrid');
    var sites = _.find(grid.options.columns, function (c) { return c.field === 'SiteId' }).values;
    var foundSite = _.find(sites, function(site) { return site.value == siteId; });
    var siteName = foundSite ? foundSite.text : '';
    return siteName;
  }

  return {
    addHandler: addHandler,
    removeHandler: removeHandler,
    getHandler: getHandler,
    getSiteName: getSiteName
  }
})();