﻿
Transport = function () {

  this.getParsedArticle = function (parsedArticleId) {
    return $.postAndValidate(application.settings.urls.viewParsedArticleUrl, { parsedArticleId: parsedArticleId }, '#viewer-parsed-articles');
  }

  this.getUserCounts = function () {
    return $.post(application.settings.urls.getUserCountsUrl);
  }

  this.submitVoteConfirmation = function (considerVote) {
    return $('.js-vote-confirmation-form')
      .formSubmit('#js-vote-confirmation-placeholder', { considerVote: considerVote }, true);
  }
}