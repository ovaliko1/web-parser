﻿var application = {};
application.shared = {};

(function ($) {
  $.blockUI.defaults.message = '<img src="/static/img/loader.gif" />';

  $.blockUI.defaults.css = {
    padding: 0,
    margin: 0,
    width: '30%',
    top: '40%',
    left: '35%',
    textAlign: 'center',
    color: '#000',
    border: 'none',
    backgroundColor: 'transparent'
  };

  _.templateSettings = {
    interpolate: /\{\{=(.+?)\}\}/g,
    evaluate: /\{\{(.+?)\}\}/g
  };
})(jQuery);




