﻿(function ($) {
  $.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
      if (o[this.name] !== undefined) {
        if ((o[this.name] === 'false' || o[this.name] === 'true') &&
            (this.value === 'false' || this.value === 'true')) {

          o[this.name] = o[this.name] === 'true' || this.value === 'true' ? 'true' : 'false';
          return;
        }

        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };

  $.fn.expandTepmlate = function (templateName, model) {
    var template = this[0].templates[templateName];
    var html = template(model);
    return removeEscaping(html);
  };

  $.fn.expandLazyTepmlate = function (templateName, model) {
    var template = this[0].templates[templateName];
    var html = template()(model);
    return removeEscaping(html);
  };

  $.fn.scrollToMe = function () {
    $('html,body').animate({
      scrollTop: $(this).offset().top
    },
      'slow');
  };

  function removeEscaping(html) {
    var htmlWithoutEscaping = html.replace(/\\#/g, '#').replace(/<\\\//g, '</');
    return htmlWithoutEscaping;
  }

  $.fn.clipPath = function () {

    // TODO: Consider to remove functionality
    return;

    var correctThis = this;

    setTimeout(function () {

      _.each($(correctThis), function (item, i) {

        var maskWidth = $(item).width();
        var maskHeight = $(item).height();

        var maskSrc = $(item).attr('src');
        var maskAlt = $(item).attr('alt');
        var maskTitle = $(item).attr('title');
        var uniqueId = i;

        var height = $(item).data('watermark-height');
        var width = $(item).data('watermark-width');

        var rectX = eval($(item).attr('data-watermark-rectX') || '""');
        var rectY = eval($(item).attr('data-watermark-rectY') || '""');
        var textX = eval($(item).attr('data-watermark-textX') || '""');
        var textY = eval($(item).attr('data-watermark-textY') || '""');

        var fontSize = $(item).data('watermark-fontsize');
        var number = $(item).data('watermark-number');

        //build SVG from our IMG attributes

        var svg = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="svgMask" width="'
          + maskWidth + '" height="'
          + maskHeight + '" viewBox="0 0 '
          + maskWidth + ' '
          + maskHeight + '"><desc>'
          + maskAlt + '</desc><image width="'
          + maskWidth + '" height="'
          + maskHeight + '" xlink:href="'
          + maskSrc + '" />' +
          '<rect x="' + rectX + '" y="' + rectY + '" height="' + height + '" width="' + width + '" style="fill: #78CD51"/>'
          +
          '<text x="' + textX + '" y="' + textY + '" style="fill: #FAFDF9; stroke: #EEEEEE;  font-size: ' + fontSize + 'px;">' + number + '</text>'
          +
          '</svg>';

        //swap original IMG with SVG
        $(item).after($(svg));

        //clean up
        delete maskSrc, maskWidth, maskHeight, maskAlt, maskTitle, uniqueId, svg;
      });

    }, 800);

    return this;
  };

  $.div = function (a, b) {
    return (a - a % b) / b;
  };
})(jQuery);