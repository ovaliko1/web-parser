﻿$(function() {

  if (typeof Ya == 'undefined') {
    return;
  }

  Ya.share2('share-buttons', {
    content: {
      url: 'http://pullmeout.net/',
      title: '«Pullmeout» - Все объявления об аренде жилья CПб',
      description: "Проверенные объявления от собственников",
      image: 'http://pullmeout.net/static/img/logo64x64.png'
    },
    contentByService: {
      vkontakte: {
        url: 'https://vk.com/pullmeoutrealtyspb',
        title: 'Все объявления об аренде жилья спб',
        description: "Проверенные объявления от собственников",
        image: 'http://pullmeout.net/static/img/logo64x64.png'
      }
    },
    theme: {
      services: 'vkontakte,odnoklassniki,facebook',
      counter: false,
      lang: 'ru',
      limit: 3,
      size: 'm',
      bare: false
    },
    hooks: {
      onshare: function () {
        $.post($('#js-user-counts').data('try-extend-user-access-for-sharing'))
          .done(function () {
            setTimeout(function() {
              Backbone.trigger('refreshUserCounts');
            }, 10000);
          });
      }
    }
  });

});