﻿(function ($) {

  $.fn.loadContent = function (options) {
    var defaults = {
      content: '',
      submitByEnter: false,
      contentContainer: '.js-content-container',
      onCancel: null
    }

    options = $.extend({}, defaults, options);

    var $contentContainer = $(options.contentContainer);
    $contentContainer.hide();

    var templatePlaceholder = this;
    templatePlaceholder.html(options.content);
    templatePlaceholder.show();

    $('.js-cancel-button').click(function() {
      templatePlaceholder.html('');
      $contentContainer.show();
      if (options.onCancel) {
        options.onCancel();
      }
    });

    initSubmitByEnter(templatePlaceholder, options.submitByEnter);

    _.defer(function () {
      $.validator.unobtrusive.parse(templatePlaceholder);
    });
  }

  $.fn.appendWithValidation = function (content) {
    var placeholder = this;
    placeholder.append(content);
    initValidation(placeholder);
  }

  $.fn.inserWithValidation = function (after, content) {
    var $content = $(content);
    $content.hide();
    $(after).after($content);
    $content.show('slow');

    initValidation(this);
  }

  function initValidation(container) {
    _.defer(function () {
      $.validator.unobtrusive.parse(container);
    });
  }

  $.fn.formSubmit = function (blockedContainer, data, customSuccess) {
    blockedContainer = blockedContainer ? blockedContainer : '.js-main-container';
    $(blockedContainer).block();

    var form = $(this);
    var promise = new $.Deferred();
    
    form.ajaxSubmit({
      data: data,
      success: function (data) {
        if (data.HasErrors) {
          if ($.checkForbidden(data)) {
            promise.reject();
            return;
          }

          showErrors(form, data);
          promise.reject();
        } else {
          promise.resolve(data);
          if (!customSuccess) {
            var contentContainer = $('.js-content-container');
            contentContainer.show();
          }
        }
      },
      error: function () {
        promise.reject();
      },
      complete: function() {
        $(blockedContainer).unblock();
      }
    });

    return promise.promise();
  };

  $.postAndValidate = function (url, requestData, blockedContainer) {
    blockedContainer = blockedContainer ? blockedContainer : '.js-main-container';
    $(blockedContainer).block();

    var promise = new $.Deferred();

    $.post(url, requestData)
    .done(function (data) {
      if (data.HasErrors) {
        if ($.checkForbidden(data)) {
          promise.reject();
          return;
        }

        showNotificationErrors(data);
        promise.reject();
      } else {
        promise.resolve(data);
      }
    })
    .fail(function () {
      promise.reject();
    })
    .always(function () {
      $(blockedContainer).unblock();
    });

    return promise.promise();
  };

  var showNotificationErrors = function(data) {
    var errorsContainer = $('.js-error-notification');

    var fieldErrors = [];
    for (var error in data.Errors) {
      if (data.Errors.hasOwnProperty(error)) {
        fieldErrors.push(data.Errors[error].join(', '));
      }
    }
    data.SummaryErrors.push(fieldErrors);
    errorsContainer.showSummaryErrors(data.SummaryErrors);
    errorsContainer.stop(true, true).show().delay(10000).slideUp('slow');
  }

  $.checkForbidden = function (data) {
    if (data.SummaryErrors[0] === 'Forbidden') {
      window.location.reload();
      return true;
    }
    return false;
  }

  $.fn.findMessagesContainer = function (name) {
    var container = $(this).parent().find("[data-valmsg-for='" + escapeAttributeValue(name) + "']");
    return container;
  }

  $.fn.showSummaryErrors = function (summaryErrors) {
    var container = $(this);
    var list = container.find("ul");

    if (list && list.length && summaryErrors && summaryErrors.length) {
      list.empty();
      container.addClass('validation-summary-errors').removeClass('validation-summary-valid');

      $.each(summaryErrors, function (index, message) {
        $('<li />').html(message).appendTo(list);
      });
    }
  }

  $.showErrors = function(form, data) {
    showErrors(form, data);
  }

  function showErrors(form, data) {
    var validator = form.validate();
    validator.showErrors(data.Errors);

    var summaryContainer = $(form).find('[data-valmsg-summary=true]');
    summaryContainer.showSummaryErrors(data.SummaryErrors);

    for (var name in data.Errors) {
      var input = form.find("[name='" + name + "']");
      var messagesContainer = $(input).findMessagesContainer(name);
      messagesContainer.attr('data-valmsg-replace', true);
    }
  }

  $.showGridActionErrors = function(data) {
    if (data.response.HasErrors) {
      showNotificationErrors(data.response);
    }
  }

  function initSubmitByEnter(templatePlaceholder, submitByEnter) {
    if (submitByEnter) {

      templatePlaceholder
        .find('form')
        .on('keyup keypress', function (e) {
          var code = e.keyCode || e.which;
          if (code != 13) {
            return true;
          }

          if (e.target.tagName.toUpperCase() == 'INPUT' &&
              e.target.type.toLowerCase() != 'checkbox') {
            $('.js-submit-button').trigger('click');
          }
          return false;
        });
    }
  }

  function escapeAttributeValue(value) {
    // As mentioned on http://api.jquery.com/category/selectors/
    return value.replace(/([!"#$%&'()*+,./:;<=>?@\[\\\]^`{|}~])/g, "\\$1");
  }

})(jQuery);