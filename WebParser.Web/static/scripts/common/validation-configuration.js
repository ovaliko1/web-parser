﻿(function ($) {

  $.validator.addMethod('requiredif', function (value, element, parameters) {

      var selector = parameters['dependentpropertyselector'];

      // get the target value (as a string, 
      // as that's what actual value will be)
      var targetvalue = parameters['targetvalue'];
      targetvalue = (targetvalue == null ? '' : targetvalue).toString();

      var form = $(element).closest('.js-validateble-form');
      // get the actual value of the target control
      // note - this probably needs to cater for more 
      // control types, e.g. radios
      var control = $(form).find(selector);
      var controltype = control.attr('type');
      
      var actualvalue = '';
      switch (controltype) {
        case 'radio':
        case 'checkbox':
          actualvalue = control.prop('checked') ? control.val() : '';
          break;
        default:
          actualvalue = control.val();
      }

      // if the condition is true, reuse the existing 
      // required field validator functionality
      if (targetvalue === actualvalue) {
        return $.validator.methods.required.call(this, value, element, parameters);
      }

      return true;
    });

  $.validator.unobtrusive.adapters.add('requiredif', ['dependentproperty', 'dependentpropertyselector', 'targetvalue'], function (options) {
    options.rules['requiredif'] = {
      dependentproperty: options.params['dependentproperty'],
      targetvalue: options.params['targetvalue'],
      dependentpropertyselector: options.params['dependentpropertyselector']
    };
    options.messages['requiredif'] = options.message;
  });

  $.validator.setDefaults({
    ignore: '',
    showErrors: customShowErrors
  });

  $.validator.unobtrusive.options = {
    errorPlacement: errorPlacement
  };

  $('body').on('click', '.js-submit-button', function (e) {

    var form = $(this).closest('.js-validateble-form');
    if (!form.length) {
      form = $('.js-validateble-area-container').find('.js-validateble-form');
    }

    form.find('.validation-summary-errors ul').html('');

    var result = true;
    _.each(form, function (currentForm) {
      result &= $(currentForm).data('unobtrusiveValidation').validate();
    });

    if (!result) {
      return;
    }

    $(this).trigger('submitClick');
  });

  function customShowErrors(errorMap) {

    this.errorList = [];
    for (var name in errorMap) {

      var element = this.findByName(name)[0];

      var messages = errorMap[name];
      if (typeof messages === 'string') {
        this.errorList.push({
          message: messages,
          element: element
        });
      } else {

        var container = $(element).findMessagesContainer(name);
        container.attr("data-valmsg-replace", false);

        for (var i = 0; i < messages.length; i++) {
          var message = messages[i];
          this.errorList.push({
            message: message,
            element: this.findByName(name)[0]
          });
        }
      }
    }

    this.defaultShowErrors();
  }

  function errorPlacement(error, inputElement) {
    var container = $(inputElement).findMessagesContainer(inputElement[0].name);
    var replaceAttrValue = container.attr("data-valmsg-replace");
    var replace = replaceAttrValue ? $.parseJSON(replaceAttrValue) !== false : null;

    if (!replace) {
      error.removeClass("input-validation-error").appendTo(container);
    }
  }

})(jQuery);