
const TransportMock = function () {

  this.getParsedArticle = function (parsedArticleId) {
    return $.Deferred(function() {
      this.resolve({
        "id": 71081,
        "ArticleFields": [
          {
            "name": "Объект",
            "value": "Студия  в аренду",
            "isPhone": false,
            "showOnVoteForm": true,
            "isImage": false,
            "recognizedPhones": [
            ]
          },
          {
            "name": "Дата объявления источника",
            "value": "19/02/2017",
            "isPhone": false,
            "showOnVoteForm": false,
            "isImage": false,
            "recognizedPhones": [
            ]
          },
          {
            "name": "Цена",
            "value": "18 000 РУБ.",
            "isPhone": false,
            "showOnVoteForm": true,
            "isImage": false,
            "recognizedPhones": [
            ]
          },
          {
            "name": "Метро",
            "value": "Проспект Ветеранов 3,3 км\nАвтово 3,6 км\nЛенинский проспект 3,9 км\nКировский завод 4,2 км",
            "isPhone": false,
            "showOnVoteForm": false,
            "isImage": false,
            "recognizedPhones": [
            ]
          },
          {
            "name": "Адрес",
            "value": "Ленинский пр-кт, 84к1, Южно-Приморский округ, Санкт-Петербург",
            "isPhone": false,
            "showOnVoteForm": true,
            "isImage": false,
            "recognizedPhones": [
            ]
          },
          {
            "name": "Телефон",
            "value": "\u003cimg src=\u0027https://s3.eu-central-1.amazonaws.com/webparser/6d7c2f51-b8d9-4882-9125-f8554be189af.png\u0027 target=\u0027_blank\u0027 alt=https://s3.eu-central-1.amazonaws.com/webparser/6d7c2f51-b8d9-4882-9125-f8554be189af.png\u003e\u003c/img\u003e",
            "BackgroundColor": "#DE2943",
            "isPhone": true,
            "showOnVoteForm": true,
            "isImage": true,
            "recognizedPhones": [
              {
                "recognizedText": "8 921 79fV16-57\n\n",
                "fixedText": "8921791657",
                "MeanConfidencePercentage": 76
              }
            ]
          },
          {
            "name": "Имя",
            "value": "Надежда",
            "isPhone": false,
            "showOnVoteForm": true,
            "isImage": false,
            "recognizedPhones": [
            ]
          },
          {
            "name": "Описание",
            "value": "Фото реальные, вся бытовая техника: посудомойка, стиральная машина, варочная панель, духовой шкаф.Новый диван.От собственника, без комиссии, с договором.Залог 100%18000 + ку",
            "isPhone": false,
            "showOnVoteForm": false,
            "isImage": false,
            "recognizedPhones": [
            ]
          },
          {
            "name": "Характеристики",
            "value": "Цена:\n18 000 РУБ.  Цена за м²:\n692 РУБ.  Комиссия:\nнет  Залог:\n18 000 РУБ.  Тип:\nКвартира  Комнаты:\nСтудия       Материал здания:\nМонолит  Этаж:\n3/19  Площадь:\n26 м²  Дата обновления объявления:\n19/02/2017  Дата публикации объявления:\n19/02/2017",
            "isPhone": false,
            "showOnVoteForm": false,
            "isImage": false,
            "recognizedPhones": [
            ]
          },
          {
            "name": "Изображения",
            "value": "[\"http://images.domofond.ru/564792358\",\"http://images.domofond.ru/564792375\",\"http://images.domofond.ru/564792382\",\"http://images.domofond.ru/564792388\",\"http://images.domofond.ru/564792395\",\"http://images.domofond.ru/564792400\"]",
            "isPhone": false,
            "showOnVoteForm": false,
            "isImage": true,
            "recognizedPhones": [
            ]
          }
        ],
        "FoundArticleLinks": [
        ],
        "CanBeVoted": true,
        "InBlackList": false
      });
    }).promise();
  }

  this.getUserCounts = function () {
    return $.Deferred(function() {
      this.resolve({
        "userName": "mail@email.ru",
        "goodVotes": 2,
        "badVotes": 0,
        "outdateVotes": 1,
        "accessDateTime": "04-11-2017 15:30",
        "freeDays": 0,
        "maxFreeDaysCount": 1,
        "blackListItemsCount": 13839
      });
    }).promise();
  }

  this.submitVoteConfirmation = function (considerVote) {
    return $.Deferred(function() {
      this.resolve({ "VoteResultType": 1 });
    }).promise();
  }

  this.getParsedArticlesGridDataSource = function() {
    return [
      {
        "Id": 71081,
        "CreatedDate": "\/Date(1487500068060)\/",
        "CreatedDateStr": "19-02-2017 13:27",
        "SiteId": 5,
        "ArticlesGroup": "С",
        "Price": "18 000 РУБ.",
        "Metro": "Проспект Ветеранов",
        "Rating": null,
        "GoodRating": 2,
        "BadRating": 0,
        "OutdateRating": 0,
        "ShowRating": true,
        "Selected": false,
        "InBlackList": false
      },
      {
        "Id": 71080,
        "CreatedDate": "\/Date(1487499737627)\/",
        "CreatedDateStr": "19-02-2017 13:22",
        "SiteId": 5,
        "ArticlesGroup": "К",
        "Price": "14 000 РУБ.",
        "Metro": "Кировский завод",
        "Rating": null,
        "GoodRating": 0,
        "BadRating": 0,
        "OutdateRating": 1,
        "ShowRating": true,
        "Selected": false,
        "InBlackList": false
      },
      {
        "Id": 71079,
        "CreatedDate": "\/Date(1487499737440)\/",
        "CreatedDateStr": "19-02-2017 13:22",
        "SiteId": 5,
        "ArticlesGroup": "К",
        "Price": "10 000 РУБ.",
        "Metro": "Площадь Восстания",
        "Rating": null,
        "GoodRating": 0,
        "BadRating": 0,
        "OutdateRating": 0,
        "ShowRating": false,
        "Selected": false,
        "InBlackList": false
      },
      {
        "Id": 71078,
        "CreatedDate": "\/Date(1487499737284)\/",
        "CreatedDateStr": "19-02-2017 13:22",
        "SiteId": 5,
        "ArticlesGroup": "К",
        "Price": "9 000 РУБ.",
        "Metro": "Международная",
        "Rating": null,
        "GoodRating": 0,
        "BadRating": 0,
        "OutdateRating": 0,
        "ShowRating": false,
        "Selected": false,
        "InBlackList": false
      },
      {
        "Id": 71077,
        "CreatedDate": "\/Date(1487499737127)\/",
        "CreatedDateStr": "19-02-2017 13:22",
        "SiteId": 5,
        "ArticlesGroup": "К",
        "Price": "12 000 РУБ.",
        "Metro": "",
        "Rating": null,
        "GoodRating": 0,
        "BadRating": 0,
        "OutdateRating": 0,
        "ShowRating": false,
        "Selected": false,
        "InBlackList": false
      }
    ];
  }
};

module.exports = TransportMock;
