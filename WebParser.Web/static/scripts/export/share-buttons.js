﻿var shareButtons = {
  init: function(onShare){
    if (typeof Ya == 'undefined') {
      return;
    }

    Ya.share2('share-buttons', {
      content: {
        url: 'http://www.pullmeout.ru/',
        title: '«Pullmeout» - Все объявления об аренде жилья в одном месте!',
        description: "Проверенные объявления от собственников",
        image: 'http://pullmeout.net/static/img/logo64x64.png'
      },
      contentByService: {
        vkontakte: {
          url: 'https://vk.com/pullmeoutrealtyspb',
          title: 'Все объявления об аренде жилья в одном месте!',
          description: "Проверенные объявления от собственников",
          image: 'http://pullmeout.net/static/img/logo64x64.png'
        }
      },
      theme: {
        services: 'vkontakte,odnoklassniki,facebook',
        counter: false,
        lang: 'ru',
        limit: 3,
        size: 'm',
        bare: false
      },
      hooks: {
        onshare: function (e) {
          onShare(e);
        }
      }
    });
  }
};