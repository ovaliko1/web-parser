﻿$(function () {

  var searchType = {
    SingleNode: 1,
    ManyNodes: 2
  }

  application.shared.SearchRulesModel = Backbone.Model.extend({
    defaults: {
      showSingleRule: false,
      searchRules: null
    }
  });

  application.shared.SearchRulesControl = Backbone.View.extend({
    el: null,
    ruleId: 0,
    searchRulesContainerSelector: '.js-search-rules-container',
    searchRulesContainer: null,
  
    initialize: function () {
      this.searchRulesContainer = this.$el.find(this.searchRulesContainerSelector);
      this.setSearchRules();
    },

    events: {
      'click .js-add-searchRule': 'addSearchRule',
      'click .js-remove-searchRule': 'removeSearchRule',

      'change #IsImage': 'isImageChanged',

      'change #IsDateTime': 'dateTimeChanged',
      'change input:radio[name=FoundValueType]': 'foundValueTypeChanged',
      'change input:radio[name=ActionType]': 'actionTypeChanged'
    },

    templates: {
      'searchRuleTemplate': function() { return _.template($("#js-search-rule-template").html()); }
    },

    isImageChanged: function (e) {
      var checkBox = $(e.target);
      var hideShowSection = checkBox.closest('.js-search-rule').find('.js-date-time-section');

      if (checkBox.prop('checked')) {
        hideShowSection.hide();
        hideShowSection.find('#IsDateTime').prop('checked', false);
      } else {
        hideShowSection.show();
      }
    },

    dateTimeChanged: function (e) {
      var checkBox = $(e.target);
      var hideShowSection = checkBox.closest('.js-date-time-section').find('.js-hide-show-section');

      if (checkBox.prop('checked')) {
        hideShowSection.show();
      } else {
        hideShowSection.hide();
      }
    },

    foundValueTypeChanged: function(e) {
      var target = $(e.target);
      var hideShowSection = target.closest('.js-found-value-type-section').find('.js-hide-show-section');

      if (target.val() == 'AttributeContent') {
        hideShowSection.show();
      } else {
        hideShowSection.hide();
      }
    },

    // TODO: Data binding!!!
    actionTypeChanged: function(e) {
      var target = $(e.target);
      var fetchSettingsShowSection = target.closest('.js-search-rule').find('.js-fetch-settings-section');
      var waitControl = target.closest('.js-search-rule').find('.js-wait-control');
      var filedNameControl = target.closest('.js-search-rule').find('.js-field-name-control');
      var otherControlsSection = target.closest('.js-search-rule').find('.js-other-controls-section');

      if (target.val() == 'Wait') {
        fetchSettingsShowSection.hide();
        waitControl.show();

        // TODO: It Is facking shit. Rewrite it!!! Implement NotRequaredIf validation attr
        var selectors = target.closest('.js-search-rule').find('#Selectors');
        if (selectors.val() == null || !selectors.val().trim()) {
          selectors.val('.');
        }

      } else if (target.val() == 'Click') {
        fetchSettingsShowSection.show();

        waitControl.hide();
        filedNameControl.hide();
        otherControlsSection.hide();
      } else {
        fetchSettingsShowSection.show();
        filedNameControl.show();
        otherControlsSection.show();

        waitControl.hide();
      }
    },

    removeSearchRule: function (e) {
      var removeButton = $(e.target);
      var ruleToRemove = removeButton.closest('.js-search-rule');

      ruleToRemove.hide("slow", function () { $(this).remove(); });
    },

    serialize: function () {
      var searchRules = this.$el.find('.js-search-rule');

      if (this.model.get('showSingleRule')) {
        if (searchRules.length == 1) {
          return this.getRuleData(searchRules[0]);
        } else {
          throw new Error('Count of Search Rule Forms not equal 1');
        }
      }

      var data = _.map(searchRules, this.getRuleData);
      return data;
    },

    getRuleData: function (ruleForm) {
      var searchRuleData = $(ruleForm).serializeObject();

      // Hadle ASP MVC checkbox behaviour
      if (searchRuleData.IsDateTime && searchRuleData.IsDateTime.length == 2) {
        searchRuleData.IsDateTime = searchRuleData.IsDateTime[0] == 'true' || searchRuleData.IsDateTime[1] == 'true';
      }

      return searchRuleData;
    }, 

    setSearchRules: function() {
      var singleRuleMode = this.model.get('showSingleRule');
      var searchRules = this.model.get('searchRules');

      if (!searchRules) {
        this.appendSearchRule(singleRuleMode ? this.getLinksToArticlesSearchRuleModel() : this.getDefaultSearchRuleModel());
        return;
      }

      if (singleRuleMode) {
        this.appendSearchRule(this.getLinksToArticlesSearchRuleModel());
        this.setSearchRule(searchRules, this.searchRulesContainer.find('.js-search-rule')[0]);
        return;
      }

      var appendSearchRule = this.appendSearchRule;
      var setSearchRule = this.setSearchRule;
      var correctThis = this;

      _.each(searchRules, function (searchRule) {
        var form = appendSearchRule.call(correctThis, {
              ruleId: correctThis.ruleId++,
              multiRuleMode: true,
              actionType: searchRule.ActionType,
              searchType: searchRule.SearchType,
              foundValueType: searchRule.FoundValueType,
              waitMilseconds: searchRule.WaitMilseconds,
              parsedKeyType: searchRule.ParsedKeyTypeId
            });
        setSearchRule(searchRule, form);
      });
    },

    addSearchRule: function (e) {
      var addButton = $(e.target);
      var target = addButton.closest('.js-search-rule');
      this.insertSearchRule(this.getDefaultSearchRuleModel(), target);
    },

    getDefaultSearchRuleModel: function() {
      return {
        ruleId: this.ruleId++,
        multiRuleMode: true,
        actionType: application.settings.actionType.Fetch,
        searchType: searchType.SingleNode,
        foundValueType: application.settings.foundValueType.TagTextContent,
        parsedKeyType: application.settings.parsedKeyType.Common,
        waitMilseconds: ''
      };
    },

    getLinksToArticlesSearchRuleModel: function () {
      return {
        ruleId: this.ruleId++,
        multiRuleMode: false,
        actionType: application.settings.actionType.Fetch,
        searchType: searchType.ManyNodes,
        parsedKeyType: application.settings.parsedKeyType.Common,
        foundValueType: application.settings.foundValueType.AttributeContent,
        waitMilseconds: ''
      };
    },

    insertSearchRule: function (model, target) {
      var newSearchRule = $(this).expandLazyTepmlate('searchRuleTemplate', model);
      this.searchRulesContainer.inserWithValidation(target, newSearchRule);
      var searchRuleForm = target.next('.js-search-rule');

      this.initWaitMilseconds(searchRuleForm);

      return searchRuleForm;
    },

    appendSearchRule: function (model) {
      var newSearchRule = $(this).expandLazyTepmlate('searchRuleTemplate', model);
      this.searchRulesContainer.appendWithValidation(newSearchRule);
      var searchRuleForm = _.last(this.searchRulesContainer.find('.js-search-rule'));

      this.initWaitMilseconds(searchRuleForm);

      return searchRuleForm;
    },

    initWaitMilseconds: function(formContainer) {
      var waitMilseconds = $(formContainer).find('#WaitMilseconds');
      if (waitMilseconds.length) {
        waitMilseconds.kendoNumericTextBox({
          decimals: 1,
          format: "#",
          min: 1,
          max: 10000,
          step: 100
        });
      } 
    },

    // TODO: rewrite to load model to template
    setSearchRule: function(searchRule, searchRuleForm) {
      var form = $(searchRuleForm);

      var fieldName = form.find('#FieldName');
      var selectors = form.find('#Selectors');
      var replaceStrings = form.find('#ReplaceRegexp');
      var attributeName = form.find('#AttributeName');
      var isImage = form.find('#IsImage');
      var doNotSaveToInternalStorage = form.find('#DoNotSaveToInternalStorage');
      var isDateTime = form.find('#IsDateTime');
      var dateTimeMask = form.find('#DateTimeMask');
      var dateLanguage = form.find('#DateCultureName');
      var filterFunction = form.find('#FilterFunction');
      var backgroundColor = form.find('#BackgroundColor');
      var showOnVoteForm = form.find('#ShowOnVoteForm');
      var hideOnVkPost = form.find('#HideOnVkPost');

      fieldName.val(searchRule.FieldName);
      selectors.val(searchRule.Selectors);
      replaceStrings.val(searchRule.ReplaceRegexp);
      attributeName.val(searchRule.AttributeName);
      dateTimeMask.val(searchRule.DateTimeMask);
      filterFunction.val(searchRule.FilterFunction);
      backgroundColor.val(searchRule.BackgroundColor);
      
      isImage.prop('checked', searchRule.IsImage);
      doNotSaveToInternalStorage.prop('checked', searchRule.DoNotSaveToInternalStorage);
      isDateTime.prop('checked', searchRule.IsDateTime);
      dateLanguage.val(searchRule.DateCultureName);

      showOnVoteForm.prop('checked', searchRule.ShowOnVoteForm);
      hideOnVkPost.prop('checked', searchRule.HideOnVkPost);

      if (searchRule.IsImage) {
        form.find('.js-date-time-section').hide();
      }

      if (searchRule.IsDateTime) {
        form.find('.js-date-time-section').find('.js-hide-show-section').show();
      }

      if (searchRule.FoundValueType == application.settings.foundValueType.AttributeContent) {
        form.find('.js-found-value-type-section').find('.js-hide-show-section').show();
      }
    }
  });

});