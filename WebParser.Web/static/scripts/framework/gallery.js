$(function() {

  if (!$('#js-gallery-template').length) {
    return;
  }

  GalleryView = Backbone.View.extend({
    template: _.template($("#js-gallery-template").html()),

    events: {
      'click a': "showImage"
    },

    initialize: function(options) {
      var imagesData = options.images || [];
      this.imagesCount = imagesData.length;
      this.loadedImagesCount = 0;
      this.images = [];
      imagesData.forEach(this.createModel, this);
    },

    createModel: function(src) {
      var img = new Image();
      var self = this;
      img.onload = function() {
        self.images.push({
          src: src,
          w: this.width,
          h: this.height
        });
        self.loadedImagesCount++;
        if (self.loadedImagesCount === self.imagesCount) {
          self.render();
        }
      };
      img.src = src;
    },

    render: function() {
      this.$el.append(this.template({ images: this.images }));
      return this;
    },

    showImage: function(e) {
      e.preventDefault();
      var index = $(e.currentTarget).data("index");
      this.showPhotoSwiper(index);
    },

    showPhotoSwiper: function(index) {
      var gallery = new PhotoSwipe($(".pswp")[0], PhotoSwipeUI_Default, this.images, {
        index: index,
        cropTop: 0,
        cropBottom: 0,
        cropLeft: 0,
        cropRight: 0
      });
      gallery.init();
    }
  });
});