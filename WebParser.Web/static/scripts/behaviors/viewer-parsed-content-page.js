﻿$(function () {

  if (!$('.js-viwer-parsed-content-view').length) {
    return;
  }

  var ViewerParsedContentView = Backbone.View.extend({
    el: $('.js-viwer-parsed-content-view'),
    parsedArticlesGrid: $('#viewer-parsed-articles').data('kendoGrid'),
    sourceUsersDropDown: $('#source-users-dropdown').data('kendoDropDownList'),
    metroStationMultiSelect: $('#metro-stations').data('kendoMultiSelect'),
    articleFiltersViewModel: null,
    currentPreviewedArticle: null,
    currentPreviewedArticleId: $('#ArticleId').val(),
    filters: null,

    initialize: function () {
      var correctThis = this;

      correctThis.filters = correctThis.getFilters() || {
        cityId: '',
        articleGroups: [],
        metroStations: []
      };

      var parsedArticlesGrid = this.parsedArticlesGrid;
      var sourceUsersDropDown = this.sourceUsersDropDown;
      var metroStationMultiSelect = this.metroStationMultiSelect;
      metroStationMultiSelect.value(this.filters.metroStations);

      sourceUsersDropDown.bind('change', sourceUserChanged);
      metroStationMultiSelect.bind('change', refreshGrid);

      var sourceUser = this.sourceUsersDropDown.dataItems()[this.sourceUsersDropDown.selectedIndex];
      this.articleFiltersViewModel = kendo.observable({
        cityId: sourceUser.get('CityId'),
        articleGroups: getDataSource(sourceUser)
      });

      this.articleFiltersViewModel.bind('change', refreshGrid);
      sourceUsersDropDown.bind('change', refreshMetroStations);
      kendo.bind($('.js-article-filters'), this.articleFiltersViewModel);

      var currentArticlesNotShown = true;
      parsedArticlesGrid.bind('dataBound', function(e) {
        correctThis.selectArticle(correctThis.currentPreviewedArticleId);

        if (currentArticlesNotShown && correctThis.currentPreviewedArticleId) {
          correctThis.viewParsedArticleCore(correctThis, correctThis.currentPreviewedArticleId);
          currentArticlesNotShown = false;
        }
      });

      var articleFiltersViewModel = this.articleFiltersViewModel;
      application.additionalDataHandlers.addHandler('onViewerFilter', function () {

        correctThis.filters.articleGroups = _.map(_.filter(articleFiltersViewModel.articleGroups,
            function(item) {
              return item.checked;
            }),
            function(item) {
              return item.name;
            });

        correctThis.filters.metroStations = metroStationMultiSelect.value();
        correctThis.saveFilters(correctThis.filters);

        var result = {
          articleGroupIds: _.flatten(_.map(_.filter(articleFiltersViewModel.articleGroups,
            function(item) {
              return item.checked;
            }),
            function(item) {
              return item.id.split('_');
            })),
          cityId: sourceUsersDropDown.dataItem().get('CityId'),
          metroStationIds: metroStationMultiSelect.value()
        };
        return result;
      });

      refreshGrid();
      refreshMetroStations();

      function refreshGrid() {
        parsedArticlesGrid.dataSource.page(1);
      }
      function refreshMetroStations() {
        metroStationMultiSelect.dataSource.read({
          cityId: sourceUsersDropDown.dataItem().get('CityId')
        });
      }

      function sourceUserChanged() {
        var sourceUser = sourceUsersDropDown.dataItem(sourceUsersDropDown.select());
        var dataSource = getDataSource(sourceUser);

        articleFiltersViewModel.cityId = sourceUser.get('CityId');
        articleFiltersViewModel.set('articleGroups', dataSource);
      }

      function getDataSource(sourceUser) {
        return _.map(sourceUser.ArticleGroups, function(item) {
          return {
            id: item.Ids.join('_'),
            name: item.Name,
            checked: _.contains(correctThis.filters.articleGroups, item.Name)
          }
        });
      }
    },

    saveFilters: function (filters) {
      localStorage.setItem('viewer:filters', JSON.stringify(filters));
    },

    getFilters: function () {
      return JSON.parse(localStorage.getItem('viewer:filters'));
    },

    events: {
      'click tr[data-uid]': 'viewParsedArticle',
      'click .js-call-button': 'showVoteForm',
      'click .js-vote-button': 'showVoteConfirmation',
      'click .js-confirm-vote': 'confirmVote',
      'click .js-do-not-cosider-vote': 'doNotConsiderVote',
      'click .js-refresh': 'refreshGrid'
    },

    templates: {
      'preview-parsing-result-template': _.template($('#js-preview-parsing-result-template').html()),
      'vote-for-article-template': _.template($('#js-vote-for-article-template').html()),
      'vote-confirmation-template': _.template($('#js-vote-confirmation-template').html()),
      'clicker-caution-template': _.template($('#js-clicker-caution-template').html())
    },

    viewParsedArticle: function(e) {
      e.preventDefault();
      var item = this.parsedArticlesGrid.dataItem($(e.target).closest('tr'));

      this.selectArticle(item.Id);

      var correctThis = this;
      this.viewParsedArticleCore(correctThis, item.Id);
    },

    selectArticle: function (id) {
      var self = this;
      _.each(this.parsedArticlesGrid.dataItems(), function(item) {
        if (item.Id == id) {
          self.parsedArticlesGrid.select($('#viewer-parsed-articles').find('tr[data-uid=' + item.uid + ']'));
        }
      });
    },

    viewParsedArticleCore: function (correctThis, parsedArticleId) {

      application.transport.getParsedArticle(parsedArticleId)
      .done(function (data) {
        correctThis.currentPreviewedArticle = data;
        correctThis.currentPreviewedArticleId = data.Id;
        correctThis.selectArticle(correctThis.currentPreviewedArticleId);

        var content = correctThis.templates['preview-parsing-result-template'](data);
        $('#js-parsed-article-placeholder').html(content);
        $('#js-parsed-article-placeholder img.clip-path').clipPath();
        $('#js-parsed-article-preview').scrollToMe();

        var fieldWithImages = _.find(data.ArticleFields, function (item) { return item.IsImage && !item.IsPhone; });
        if (fieldWithImages) {
          var images = JSON.parse(fieldWithImages.Value);
          if (images && images.length) {
            var view = new GalleryView({ el: $('.gallery-wrapper'), images: images });
          }
        }

        new GalleryView({
          el: $('.full-size-images-gallery-wrapper'),
          images: _.map(data.ImageProcessingResult.FullSizeImages, function(item) {
            return item.ImageUrl;
          })
        });

        new GalleryView({
          el: $('.images-preview-images-gallery-wrapper'),
          images: _.map(data.ImageProcessingResult.ImagesPreview, function (item) {
            return item.ImageUrl;
          })
        });

      });
    },

    showVoteForm: function() {
      var content = this.templates['vote-for-article-template'](this.currentPreviewedArticle);
      $('#js-vote-form-placeholder').loadContent({ content: content });

      $('#js-vote-form-placeholder').scrollToMe();
    },

    showVoteConfirmation: function (e) {
      var content = this.templates['vote-confirmation-template']({
        id: this.currentPreviewedArticle.Id,
        ratingTypeId: $(e.currentTarget).data('rating-type')
      });

      $('#js-vote-confirmation-placeholder').loadContent({
        content: content,
        contentContainer: '#js-vote-form-placeholder'
      });
    },

    showClickerCaution: function (correctThis) {
      var content = correctThis.templates["clicker-caution-template"]({
        id: correctThis.currentPreviewedArticle.Id,
        email: application.settings.contactEmail
      });

      $('#js-clicker-caution-placeholder').loadContent({
        content: content,
        contentContainer: '#js-vote-confirmation-placeholder',
        onCancel: function() {
          correctThis.refreshContent(correctThis);
        }
      });
    },

    confirmVote: function() {
      this.submitVote(true);
    },

    doNotConsiderVote: function () {
      this.submitVote(false);
    },

    submitVote: function (considerVote) {
      var correctThis = this;

      application.transport.submitVoteConfirmation(considerVote)
       .done(function (data) {
          switch (data.VoteResultType) {
            case application.settings.voteResultType.success:
            case application.settings.voteResultType.maxFreeDaysCountReached:
              correctThis.refreshContent(correctThis);

              Backbone.trigger('refreshUserCounts');
              break;

            case application.settings.voteResultType.clickerCaution:
              correctThis.showClickerCaution(correctThis);
              break;

            default:
              break;
          }
       });
    },

    refreshContent: function(correctThis) {
      $('#js-vote-confirmation-placeholder').html('');
      $('#js-vote-form-placeholder').html('');
      $('#js-clicker-caution-placeholder').html('');

      $('.js-content-container').show();

      correctThis.parsedArticlesGrid.dataSource.read();
      correctThis.viewParsedArticleCore(correctThis, correctThis.currentPreviewedArticle.Id);
    },

    refreshGrid: function (e) {
      e.preventDefault();
      this.parsedArticlesGrid.dataSource.read();
    }

  });

  new ViewerParsedContentView();
});