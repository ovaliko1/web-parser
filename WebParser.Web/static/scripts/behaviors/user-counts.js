$(function() {

  if (!$('#js-user-counts').length) {
    return;
  }

  UserCounts = Backbone.View.extend({
    el: $('#js-user-counts'),
    url: $('#js-user-counts').data('get-user-counts-url'),

    viewModel: null,

    initialize: function(options) {
      this.viewModel = kendo.observable({
        data: null,
        freeDaysStyle: function () {
          // width: @((int)((@Model.FreeDays / (decimal)@Model.MaxFreeDaysCount) * 100m))%;
          if (this.data) {
            return 'width: ' + $.div(((this.data.FreeDays / this.data.MaxFreeDaysCount) * 100), 1) + '%';
          }
          return '';
        },
        accessDataTimeString: function() {
          if (this.data) {
            return 'Доступ до: ' + this.data.AccessDateTime;
          }
          return '';
        }
      });

      var self = this;
      this.refreashCounts().done(function() {
        kendo.bind(self.$el, self.viewModel);
      });

      Backbone.on('refreshUserCounts', this.refreashCounts, this);
    },

    refreashCounts: function() {
      var self = this;
      return application.transport.getUserCounts()
        .done(function (data) {
          self.viewModel.set('data', data);
          self.viewModel.trigger('change', { field: 'freeDaysStyle' });
          self.viewModel.trigger('change', { field: 'accessDataTimeString' });
        });
    }
  });

  new UserCounts();
});