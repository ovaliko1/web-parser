﻿$(function () {

  if (!$('.js-black-list-report-view').length) {
    return;
  }

  var BlackListReportView = Backbone.View.extend({
    el: $('.js-black-list-report-view'),
    grid: $('#black-list').data('kendoGrid'),
    previewedId: $('#Id').val(),

    initialize: function () {
      this.grid.bind('save', function (e) {
        e.sender.dataSource.one('change', function(e) {
          this.read();
        });
      });
      
      if (this.previewedId) {
        this.grid.dataSource.filter({
          field: "Id",
          operator: "eq",
          value: this.previewedId
        });
      }
    },

    events: {
    },

    templates: {

    }
  });

  new BlackListReportView();
});