﻿$(function () {

  if (!$('.js-sites-view').length) {
    return;
  }

  var SitesView = Backbone.View.extend({
    el: $('.js-sites-view'),
    sitesGrid: $('#available-sites').data('kendoGrid'),

    initialize: function () {
    },

    events: {
      'click .js-add-site': 'addSite',
      'submitClick .js-submit-button': 'submitSite',
      'click #available-sites .js-edit-item': 'editSite',
      'click #available-sites .js-delete-item': 'deleteSite',
      'click .js-force-run-parse': 'runParsingProcess',
      'change #OneTimeExport': 'onOneTimeExportChanged'
    },

    templates: {
      'site-template': _.template($("#js-site-template").html())
    },

    onOneTimeExportChanged: function(e) {
      var checkBox = $(e.target);
      var hideShowSection = this.$el.find('.js-scheduler-settings-section');

      if (checkBox.prop('checked')) {
        hideShowSection.hide();
      } else {
        hideShowSection.show();
      }
    },

    addSite: function () {
      var siteModel = {
        text: 'Create',
        name: '',
        link: '',
        showAsEdit: false,
        proxyAddress: '',
        proxyUsername: '',
        proxyPassword: '',
        cookieName: '',
        cookieValue: '',
        oneTimeExport: false,
        isEnabled: false,
        webHookUrl: '',
        repeatIntervalInMunutes: '',
        watermarkHeight: '',
        watermarkWidth: '',
        watermarkFontSize: '',
        watermarkRectX: '',
        watermarkRectY: '',
        watermarkTextX: '',
        watermarkTextY: '',
        userAgent: '',
        jobTypeId: application.settings.jobType.Parsing,
        blackListIdentitySiteId: '',
        geoBounds: '',
        useSourceLinkAsContact: '',
        cropImageTop: 0,
        cropImageBottom: 0,
        cropImageLeft: 0,
        cropImageRight: 0,

        action: this.$el.data('create-site-url')
      };

      var content = $(this).expandTepmlate('site-template', siteModel);
      $('#js-site-placeholder').loadContent({
        content: content,
        submitByEnter: true                         
      });
    },

    editSite: function (e) {
      var item = this.sitesGrid.dataItem($(e.target).closest('tr'));
      var siteModel = {
        text: 'Edit',
        id: item.Id,
        name: item.Name,
        link: item.Link,
        showAsEdit: true,
        proxyAddress: item.ProxyAddress,
        proxyUsername: item.ProxyUsername,
        proxyPassword: item.ProxyPassword,
        cookieName: item.CookieName,
        cookieValue: item.CookieValue,
        oneTimeExport: item.OneTimeExport,
        isEnabled: item.IsEnabled,
        webHookUrl: item.WebHookUrl,
        clipTop: item.ClipTop,
        clipBottom: item.ClipBottom,
        repeatIntervalInMunutes: item.RepeatIntervalInMunutes,
        watermarkHeight : item.WatermarkHeight,
        watermarkWidth  : item.WatermarkWidth,
        watermarkFontSize : item.WatermarkFontSize,
        watermarkRectX : item.WatermarkRectX,
        watermarkRectY : item.WatermarkRectY,
        watermarkTextX : item.WatermarkTextX,
        watermarkTextY: item.WatermarkTextY,
        userAgent: item.UserAgent,
        jobTypeId: item.JobTypeId,
        blackListIdentitySiteId: item.BlackListIdentitySiteId,
        geoBounds: item.GeoBounds,
        useSourceLinkAsContact: item.UseSourceLinkAsContact,
        cropImageTop: item.CropImageTop,
        cropImageBottom: item.CropImageBottom,
        cropImageLeft: item.CropImageLeft,
        cropImageRight: item.CropImageRight,

        action: this.$el.data('edit-site-url')
      };

      var content = $(this).expandTepmlate('site-template', siteModel);
      $('#js-site-placeholder').loadContent({
        content: content,
        submitByEnter: true
      });

      var startTimePicker = $('#SchedulerStartTime').data('kendoTimePicker');
      startTimePicker.value(item.SchedulerStartTimeStr);

      var endTimePicker = $('#SchedulerEndTime').data('kendoTimePicker');
      endTimePicker.value(item.SchedulerEndTimeStr);
    },

    deleteSite: function (e) {
      var url = this.$el.data('delete-site-url');
      var item = this.sitesGrid.dataItem($(e.target).closest('tr'));
      var sitesGrid = this.sitesGrid;
      $.postAndValidate(url, { siteId: item.Id })
      .done(function (data) {
        sitesGrid.dataSource.read();
      });
    },

    runParsingProcess: function (e) {
      var url = this.$el.data('force-run-parsing-url');
      var item = this.sitesGrid.dataItem($(e.target).closest('tr'));
      var sitesGrid = this.sitesGrid;
      $.post(url, { siteId: item.Id })
       .done(function (data) {
         sitesGrid.dataSource.read();
       });
    },

    submitSite: function () {
      var sitesGrid = this.sitesGrid;
      $('.js-site-form')
        .formSubmit()
        .done(function (data) {
          $('#js-site-placeholder').html('');
          sitesGrid.dataSource.read();
        });
    }
  });

  new SitesView();
});