﻿$(function () {

  if (!$('.js-article-group-view').length) {
    return;
  }

  var linksFormatType = {
    SpecificLinks: 'SpecificLinks',
    LinksFormat: 'LinksFormat',
    LinksFile: 'LinksFile'
  }

  var ArticlesGroupPageModel = Backbone.Model.extend({
    defaults: {
      'siteId': '-1',
      'linksFormatType': 'SpecificLinks'
    }
  });

  var ArticlesGroupView = Backbone.View.extend({
    el: $('.js-article-group-view'),
    sitesDropDown: $('#sites-dropdown').data('kendoDropDownList'),
    articlesGrid: $('#article-group-grid').data('kendoGrid'),
    articlePlaceholder: $('#js-article-group-placeholder'),
    linksToArticlesRuleView: null,
    articleRulesView: null,
    linksTaggle: null,

    initialize: function() {
      var articleGroupsGrid = this.articlesGrid;
      var model = this.model;
      var sitesDropDown = this.sitesDropDown;
      var articleGroupPlaceholder = this.articlePlaceholder;

      this.sitesDropDown.bind('change', siteChanged);
      this.model.on('change:siteId', onSiteIdChanged);

      function siteChanged() {
        articleGroupPlaceholder.html('');
        var item = sitesDropDown.dataItem(sitesDropDown.select());
        model.set({
          siteId: item ? item.Id : -1
        });
      };

      function onSiteIdChanged() {
        var siteId = this.get('siteId');
        if (siteId && siteId != -1) {
          articleGroupsGrid.dataSource.read();
        } else {
          articleGroupsGrid.dataSource.data([]);
        }
      }

      var currentSiteItem = this.sitesDropDown.dataItem(this.sitesDropDown.select());
      if (currentSiteItem) {
        this.model.set({
          siteId: this.sitesDropDown.dataItem(this.sitesDropDown.select()).Id
        });
      }
    },

    events: {
      'click .js-add-articles-group': 'addArticleGroup',
      'click .js-edit-item': 'editArticleGroup',
      'click .js-delete-item': 'deleteArticlesGroup',
      'click .js-repeat-article-group': 'repeatArticleGroup',
      'submitClick .js-edit-articles-group-result': 'submitArticleGroup',
      'submitClick .js-preview-parsing-result': 'previewParsingResult',
      'change input:radio[name=LinksFormatType]': 'linksFormatSwitched'
    },                                         

    templates: {
      'article-group-template': _.template($('#js-article-group-template').html()),
      'preview-parsing-result-template': _.template($('#js-preview-parsing-result-template').html()),
      'found-article-links-template': _.template($('#js-found-article-links-template').html())
    },

    addArticleGroup: function () {
      var articleGroupModel = {
        text: 'Create',
        siteId: this.model.get('siteId'),
        internalName: '',
        publicName: '',
        showAsEdit: false,
        linksFormat: '',
        startPage: '',
        stepSize: '',
        showSpecificLinks: true,
        showLinksFormat: false,
        showLinksFile: false,
        linksFileId: '',
        vkGroupId: '',
        vkTopicId: '',
        vkTopicGroupId: '',
        spiderTypeId: '',
        linksToArticlesSearchRuleSpiderTypeId: '',
        spiderDoNotLoadImages: false,
        batchSize: 100,
        batchDelayInSeconds: 0,
        downloadFileDelayInMillseconds: 0,
        waitMilliseconds: null,
        twitterConsumerKey: null,
        twitterConsumerSecret: null,
        twitterAccessToken: null,
        twitterAccessTokenSecret: null, 
        action: this.$el.data('create-article-group-url')
      };

      var content = $(this).expandTepmlate('article-group-template', articleGroupModel);
      this.articlePlaceholder.loadContent({ content: content });
      this.initSettingsTabStrip();
      this.initLinks();
      this.initSearchRulesViews(null, null);

      this.initUploadLinksFile();
    },

    editArticleGroup: function (e) {
      var item = this.articlesGrid.dataItem($(e.target).closest('tr'));

      var articleGroupModel = {
        text: 'Edit',
        siteId: this.model.get('siteId'),
        id: item.Id,
        internalName: item.InternalName,
        publicName: item.PublicName,
        showAsEdit: true,
        linksFormat: item.LinksFormat,
        startPage: item.StartPage,
        stepSize: item.StepSize,
        showSpecificLinks: item.Links != null && item.Links.length,
        showLinksFormat:(item.Links == null || !item.Links.length) && item.LinksFileId == null,
        showLinksFile: item.LinksFileId != null,
        linksFileId: item.LinksFileId,
        vkGroupId: item.VkGroupId,
        vkTopicId: item.VkTopicId,
        vkTopicGroupId: item.VkTopicGroupId,
        spiderTypeId: item.SpiderTypeId,
        linksToArticlesSearchRuleSpiderTypeId: item.LinksToArticlesSearchRuleSpiderTypeId,
        spiderDoNotLoadImages: item.SpiderDoNotLoadImages,
        batchSize: item.BatchSize,
        batchDelayInSeconds: item.BatchDelayInSeconds,
        downloadFileDelayInMillseconds: item.DownloadFileDelayInMillseconds,
        waitMilliseconds: item.WaitMilliseconds,
        twitterConsumerKey: item.TwitterConsumerKey,
        twitterConsumerSecret: item.TwitterConsumerSecret,
        twitterAccessToken: item.TwitterAccessToken,
        twitterAccessTokenSecret: item.TwitterAccessTokenSecret,
        action: this.$el.data('edit-article-group-url')
      };

      var content = $(this).expandTepmlate('article-group-template', articleGroupModel);
      this.articlePlaceholder.loadContent({ content: content });
      this.initSettingsTabStrip();
      this.initArticleGroupSettingsTabControls(item);
      this.initSearchRulesViews(item.LinksToArticlesSearchRule, item.ArticleSearchRules);

      this.initUploadLinksFile(item);

      this.model.set('linksFormatType', item.LinksFormat);
    },

    repeatArticleGroup: function (e) {
      var item = this.articlesGrid.dataItem($(e.target).closest('tr'));

      var articleGroupModel = {
        text: 'Repeat',
        siteId: this.model.get('siteId'),
        internalName: '',
        publicName: '',
        showAsEdit: false,
        linksFormat: item.LinksFormat,
        startPage: item.StartPage,
        stepSize: item.StepSize,
        showSpecificLinks: item.Links != null,
        showLinksFormat: item.Links == null,
        showLinksFile: item.LinksFileId != null,
        linksFileId: item.LinksFileId,
        vkGroupId: item.VkGroupId,
        vkTopicId: item.VkTopicId,
        vkTopicGroupId: item.VkTopicGroupId,
        spiderTypeId: item.SpiderTypeId,
        linksToArticlesSearchRuleSpiderTypeId: item.LinksToArticlesSearchRuleSpiderTypeId,
        spiderDoNotLoadImages: item.SpiderDoNotLoadImages,
        batchSize: item.BatchSize,
        batchDelayInSeconds: item.BatchDelayInSeconds,
        downloadFileDelayInMillseconds: item.DownloadFileDelayInMillseconds,
        waitMilliseconds: item.WaitMilliseconds,
        twitterConsumerKey: item.TwitterConsumerKey,
        twitterConsumerSecret: item.TwitterConsumerSecret,
        twitterAccessToken: item.TwitterAccessToken,
        twitterAccessTokenSecret: item.TwitterAccessTokenSecret,
        action: this.$el.data('create-article-group-url')
      };

      var content = $(this).expandTepmlate('article-group-template', articleGroupModel);
      this.articlePlaceholder.loadContent({ content: content });
      this.initSettingsTabStrip();
      this.initArticleGroupSettingsTabControls(item);
      this.initSearchRulesViews(item.LinksToArticlesSearchRule, item.ArticleSearchRules);

      this.initUploadLinksFile(item);

      this.model.set('linksFormatType', item.LinksFormat);
    },

    initUploadLinksFile: function(item) {
      var files = item ? [{ name: item.LinksFileName, size: 0, extension: item.LinksFileExtension }] : [];

      var linksFileUploader = $('#linksFile').kendoUpload({
        async: {
          saveUrl: "ArticlesGroup/UploadLinksFile",
          autoUpload: true
        },
        multiple: false,
        files: files,
        success: function (e) {
          var fileId = e.response.fileId;
          $('#LinksFileId').val(fileId);
        } 
      });
    },

    initArticleGroupSettingsTabControls: function (item) {
      this.initLinks(item.Links);

      var startPageTextBox = $('#StartPage').data('kendoNumericTextBox');
      var stepSizeNumericTextBox = $('#StepSize').data('kendoNumericTextBox');
      startPageTextBox.value(item.StartPage);
      stepSizeNumericTextBox.value(item.StepSize);
    },

    initLinks: function (links) {
      var correctThis = this;
      this.linksTaggle = new Taggle('js-links-control', {
        duplicateTagClass: 'bounce',
        placeholder: 'Enter link',
        hiddenInputName: 'Links',
        tags: links || [],
        preserveCase: true,
        onBeforeTagAdd: function (event, tag) {
          return correctThis.processLinksValidation.call(correctThis, $(event.target), tag);
        }
      });

      $(this.linksTaggle.getInput()).focusout(function () {
        correctThis.showHideLinksValidationError.call(correctThis, false);
      });
    },

    processLinksValidation: function (input, tag) {
      if (this.isFullOrRelativeUrl(tag)) {
        this.showHideLinksValidationError(false);
        return true;
      } else {
        this.showHideLinksValidationError(true, 'The Link field is not a valid fully-qualified http, https, ftp URL or relative URL');
        return false;
      }
    },

    showHideLinksValidationError: function (show, error) {
      var errorPlaceholder = $('#js-links-control').parent().find('.field-validation-valid');
      var input = $(this.linksTaggle.getInput());

      if (show) {
        input.addClass('input-validation-error');
        errorPlaceholder.html('<span for="Links" class="">' + error + '</span>');
      } else {
        input.removeClass('input-validation-error');
        errorPlaceholder.html('');
      }
    },

    isFullOrRelativeUrl: function (value) {
      //TODO: Improve this aproach.
      var regexp = /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
      return regexp.test(value) || regexp.test('http://www.ru' + value);
    },

    initSettingsTabStrip: function() {
      $("#articlesGroupSettings").kendoTabStrip({
        animation: {
          open: {
            duration: 200,
            effects: "expand:vertical"
          }
        }
      });
    },

    initSearchRulesViews: function (linksToArticlesModel, articleFieldsModel) {
      this.linksToArticlesRuleView = new application.shared.SearchRulesControl({
        el: $('.js-links-to-articles-search-rule-view'),
        model: new application.shared.SearchRulesModel({
          showSingleRule: true,
          searchRules: linksToArticlesModel
        })
      });
      
      this.articleRulesView = new application.shared.SearchRulesControl({
        el: $('.js-article-search-rules-view'),
        model: new application.shared.SearchRulesModel({
          showSingleRule: false,
          searchRules: articleFieldsModel
        })
      });
    },

    linksFormatSwitched: function(e) {
      var target = $(e.target);
      var startPageTextBox = $('#StartPage').data('kendoNumericTextBox');
      var stepSizeNumericTextBox = $('#StepSize').data('kendoNumericTextBox');

      switch (target.val()) {
        case linksFormatType.SpecificLinks:
          $('.js-specific-links-section').show();
          $('.js-links-format-section').hide();
          $('.js-upload-links-file').hide();

          startPageTextBox.value('');
          stepSizeNumericTextBox.value('');
          $('#LinksFormat').val('');

          break;

        case linksFormatType.LinksFormat:
          $('.js-links-format-section').show();
          $('.js-specific-links-section').hide();
          $('.js-upload-links-file').hide();
          this.linksTaggle.removeAll();

          break;

        case linksFormatType.LinksFile:
          $('.js-upload-links-file').show();
          $('.js-specific-links-section').hide();
          $('.js-links-format-section').hide();

          this.linksTaggle.removeAll();

          startPageTextBox.value('');
          stepSizeNumericTextBox.value('');
          $('#LinksFormat').val('');

          break;
        
        default:
          break;
      }

      this.model.set('linksFormatType', target.val());
    },

    deleteArticlesGroup: function (e) {
      var url = this.$el.data('delete-articles-group-url');
      var item = this.articlesGrid.dataItem($(e.target).closest('tr'));
      var articleGroupsGrid = this.articlesGrid;
      $.postAndValidate(url, { articlesGroupId: item.Id })
       .done(function(data) {
         articleGroupsGrid.dataSource.read();
       });
    },

    previewParsingResult: function (e) {
      var url = this.$el.data('preview-parsing-result-url');

      var correctThis = this;
      this.submitForm(e, url)
      .done(function (data) {
        var content = correctThis.templates['preview-parsing-result-template'](data);
        var foundLinksContent = correctThis.templates['found-article-links-template'](data);

        $('#js-preview-parsing-result-placeholder').html(content);
        $('#js-preview-parsing-result-placeholder img.clip-path').clipPath();
        $('#js-article-group-links-placeholder').html(foundLinksContent);

        var fieldWithImages = _.find(data.ArticleFields, function (item) { return item.IsImage && !item.IsPhone; });
        if (fieldWithImages) {
          var images = JSON.parse(fieldWithImages.Value);
          if (images && images.length) {
            var view = new GalleryView({ el: $('.gallery-wrapper'), images: images });
          }
        }
      });
    },

    submitArticleGroup: function (e) {
      var articlesGroupGrid = this.articlesGrid;
      var articleGroupPlaceholder = this.articlePlaceholder;
      var url = $('.js-article-group-form').attr('action');

      this.submitForm(e, url)
      .done(function(data) {
        articlesGroupGrid.dataSource.read();
        articleGroupPlaceholder.html('');

        var contentContainer = $('.js-content-container');
        contentContainer.show();
      });
    },

    submitForm: function(e, url) {
      e.preventDefault();

      var promise = new $.Deferred();
      var createArticleForm = $('.js-article-group-form');
      var formData = createArticleForm.serializeObject();

      formData.LinksToArticlesSearchRule = this.linksToArticlesRuleView.serialize();
      formData.ArticleSearchRules = this.articleRulesView.serialize();

      if (!this.linksTaggleIsValid() ||
          !this.articlesSearchRulesIsAdded(formData.ArticleSearchRules)) {
        promise.reject();
        return promise.promise();
      }

      $('.js-article-group-settings-container').block();

      $.post(url, formData)
      .done(function (data) {

        var summaryContainer = $('#js-article-group-placeholder').find('[data-valmsg-summary=true]');

        if (data.HasErrors) {
          summaryContainer.showSummaryErrors(data.SummaryErrors);
          $.showErrors($('.js-article-group-form'), data);

          promise.reject();
        } else {
          summaryContainer.find('ul').html('');
          promise.resolve(data);
        }
      })
      .fail(function(data) {
        promise.reject(data);
      })
      .always(function () {
        $('.js-article-group-settings-container').unblock();
      });

      return promise.promise();
    },

    linksTaggleIsValid: function () {
      if (this.model.get('linksFormatType') != linksFormatType.SpecificLinks || this.linksTaggle.getTagValues().length) {
        return true;
      }

      this.showHideLinksValidationError(true, 'The Links field is required.');
      $('#articlesGroupSettings').data("kendoTabStrip").select(0);
      return false;
    },

    articlesSearchRulesIsAdded: function (articleSearchRules) {
      if (articleSearchRules.length) {
        return true;
      }

      var errorsContainer = $('.js-article-search-rules-view').find('.js-error-notification');
      errorsContainer.showSummaryErrors(['You need to add at least one Search Rule']);
      errorsContainer.stop(true, true).show().delay(3000).slideUp('slow');

      $('#articlesGroupSettings').data("kendoTabStrip").select(2);
      return false;
    }
  });

  var model = null;

  application.additionalDataHandlers.addHandler('onArticleGroupsAdditionalData', function() {
    return {
      siteId: model.get('siteId')
    };
  });

  model = new ArticlesGroupPageModel();
  new ArticlesGroupView({ model: model });
});