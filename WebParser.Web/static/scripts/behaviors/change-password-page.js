﻿$(function () {

  if (!$('.js-change-password-view').length) {
    return;
  }

  var ChangePassword = Backbone.View.extend({
    el: $('.js-change-password-view'),

    initialize: function () {
    },

    events: {
      'submitClick .js-submit-button': 'submitForm'
    },

    templates: {
    },

    submitForm: function () {
      $('.js-success-notification').hide();

      $('.js-change-password-form')
        .formSubmit()
        .done(function (data) {

          $('#OldPassword').val('');
          $('#NewPassword').val('');
          $('#ConfirmPassword').val('');

          $('.js-success-notification').slideDown('slow');
        });
    }
  });

  new ChangePassword();
});