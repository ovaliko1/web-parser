﻿$(function () {

  if (!$('.js-users-view').length) {
    return;
  }

  var UsersView = Backbone.View.extend({
    el: $('.js-users-view'),
    usersGrid: $('#users').data('kendoGrid'),
    viewersGrid: $('#viewers').data('kendoGrid'),
    publisherGrid: $('#publishers').data('kendoGrid'),

    initialize: function() {
    },

    events: {
      'click .js-add-user': 'addUser',
      'click .js-publisher-account': 'addPublisher',
      'submitClick .js-submit-user-button': 'submitUser',
      'submitClick .js-submit-publisher-button': 'submitPublisher',
      'submitClick .js-submit-viewer-button': 'submitViewer',
      'click #users .js-edit-item': 'editUser',
      'click #viewers .js-edit-item': 'editViewer',
      'click #publishers .js-edit-item': 'editPublisher'
    },

    templates: {
      'user-template': _.template($("#js-user-template").html()),
      'viewer-template': _.template($("#js-viewer-template").html()),
      'publisher-template': _.template($("#js-publisher-template").html())
    },

    addUser: function() {
      var model = {
        text: 'Create',
        id: -1,
        email: '',
        showAsEdit: false,
        showUserFields: true,
        nameForViewers: '',
        purchasedAccount: false,
        waitingForPayment: false,
        submitButtonClass: 'js-submit-user-button',
        accessDateTime: '',
        maxSiteCount: '',
        action: this.$el.data('create-user-url')
      };

      this.loadTemplate(model, 'user-template');
      this.initUserCities(null);
    },

    addPublisher: function() {
      var model = {
        text: 'Create',
        id: -1,
        email: '',
        showAsEdit: false,
        submitButtonClass: 'js-submit-publisher-button',
        siteId: '',
        articlesGroupId: '',
        accessDateTime: '',
        action: application.settings.urls.createPublisherUrl
      };

      this.loadTemplate(model, 'publisher-template');

      $('#SiteId').data('kendoDropDownList').bind('change', this.siteChanged);
    },

    editUser: function(e) {
      var item = this.usersGrid.dataItem($(e.target).closest('tr'));
      var model = {
        text: 'Edit',
        id: item.Id,
        email: item.Email,
        showAsEdit: true,
        showUserFields: true,
        nameForViewers: item.NameForViewers,
        purchasedAccount: item.PurchasedAccount,
        waitingForPayment: false,
        submitButtonClass: 'js-submit-user-button',
        accessDateTime: item.AccessDateTimeStr,
        maxSiteCount: item.MaxSiteCount,
        action: this.$el.data('edit-user-url')
      };

      this.loadTemplate(model, 'user-template');
      this.initUserCities(item.CityId);
    },

    editViewer: function(e) {
      var item = this.viewersGrid.dataItem($(e.target).closest('tr'));
      var model = {
        text: 'Edit',
        id: item.Id,
        email: item.Email,
        showAsEdit: true,
        showUserFields: false,
        purchasedAccount: item.PurchasedAccount,
        waitingForPayment: item.WaitingForPayment,
        submitButtonClass: 'js-submit-viewer-button',
        accessDateTime: item.AccessDateTimeStr,
        openingBalance: item.OpeningBalance,
        action: application.settings.urls.editViewerUrl
      };

      this.loadTemplate(model, 'viewer-template');
    },

    editPublisher: function(e) {
      var item = this.publisherGrid.dataItem($(e.target).closest('tr'));
      var model = {
        text: 'Edit',
        id: item.Id,
        email: item.Email,
        showAsEdit: true,
        submitButtonClass: 'js-submit-publisher-button',
        siteId: item.SiteId,
        articlesGroupId: item.ArticlesGroupId,
        accessDateTime: item.AccessDateTimeStr,
        action: application.settings.urls.editPublisherUrl
      };

      this.loadTemplate(model, 'publisher-template');

      $('#SiteId').data('kendoDropDownList').bind('change', this.siteChanged);
    },

    initUserCities: function (cityId) {
      var cities = this.usersGrid.columns
        .filter(function (column) {
          return column.field === 'CityId';
        })
        .map(function (column) {
          return column.values;
        })
      [0];

      var cityDropDownList = $('#CityId').data('kendoDropDownList');
      cityDropDownList.dataSource.data(cities);
      cityDropDownList.value(cityId);
    },

    loadTemplate: function (model, template) {
      var content = $(this).expandTepmlate(template, model);
      $('#js-user-placeholder').loadContent({ content: content });
    },

    siteChanged: function () {
      var dropDown = $('#ArticlesGroupId').data('kendoDropDownList');
      dropDown.value('');
      dropDown.dataSource.read();
    },

    deleteUser: function (e) {
      var url = this.$el.data('delete-site-url');
      var item = this.usersGrid.dataItem($(e.target).closest('tr'));
      var sitesGrid = this.usersGrid;
      $.post(url, { siteId: item.Id })
       .done(function (data) {
         sitesGrid.dataSource.read();
       });
    },

    submitUser: function () {
      this.submit(this.usersGrid);
    },

    submitViewer: function () {
      this.submit(this.viewersGrid);
    },

    submitPublisher: function () {
      this.submit(this.publisherGrid);
    },

    submit: function (grid) {
      $('.js-user-form')
        .formSubmit()
        .done(function (data) {
          $('#js-user-placeholder').html('');
          grid.dataSource.read();
        });
    }
  });

  application.additionalDataHandlers.addHandler('onGetArticleGroupItemsBySite', function () {
    return {
      siteId: $('#SiteId').val()
    };
  });

  new UsersView();
});