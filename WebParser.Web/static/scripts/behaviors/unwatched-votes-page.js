﻿$(function () {

    if (!$('.js-unwatched-votes-report-view').length) {
    return;
  }

  var UnwatchedVotesView = Backbone.View.extend({
    el: $('.js-unwatched-votes-report-view'),
    unwatchedVotesGrid: $('#unwatched-votes').data('kendoGrid'),

    initialize: function () {
    },

    events: {
      'click .k-grid-Confirm': 'confirmVote',
      'click .k-grid-Reject': 'rejectVote',
    },

    templates: {
    },

    confirmVote: function (e) {
      var item = this.unwatchedVotesGrid.dataItem($(e.target).closest('tr'));
      this.saveVoteReview(this.$el.data('confirm-vote-url'), item.Id);
    },

    rejectVote: function (e) {
      var item = this.unwatchedVotesGrid.dataItem($(e.target).closest('tr'));
      this.saveVoteReview(this.$el.data('reject-vote-url'), item.Id);
    },

    saveVoteReview: function (url, voteId) {
      var self = this;
      $.postAndValidate(url, { voteId: voteId })
        .done(function (data) {
          self.unwatchedVotesGrid.dataSource.read();
        });
    }
  });

  new UnwatchedVotesView();
});