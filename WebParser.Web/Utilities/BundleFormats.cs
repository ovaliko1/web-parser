﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebParser.Web.Utilities
{
    public static class BundleFormats
    {
        public static readonly string BaseStyleLinkFormat = @"<link id =""base-style"" href=""{0}"" rel=""stylesheet"" type=""text/css"" />";
        public static readonly string BaseResponsiveLinkFormat = @"<link id =""base-style-responsive"" href=""{0}"" rel=""stylesheet"" type=""text/css"" />";
    }
}