﻿using Kendo.Mvc.UI.Fluent;

namespace WebParser.Web.Utilities
{
    public static class HtmlHelpers
    {
        public static GridBuilder<T> SimpleGrid<T>(this WidgetFactory html) where T : class
        {
            return html
                .Grid<T>()
                .StandardFilters()
                .HtmlAttributes(
                    new { @Class = "table table-striped table-bordered bootstrap-datatable datatable dataTable" })
                .Pageable(x => x.ButtonCount(5).PageSizes(new[] {5, 10, 20, 50}));
        }

        public static GridBuilder<T> StandardFilters<T>(this GridBuilder<T> builder) where T : class
        {
            return builder
                .Filterable(filterable => filterable
                    .Messages(
                        x => x
                            .Clear("Отчистить")
                            .And("И")
                            .Or("ИЛИ")
                            .Filter("Применить"))
                    .Operators(operators => operators
                        .ForString(str => str
                            .Clear()
                            .Contains("Содежит")
                            .IsEqualTo("Равно")
                            .IsNotEqualTo("Не равно")
                            .StartsWith("Начинается с")
                            .DoesNotContain("Не содержит")
                            .EndsWith("Заканчивается на"))
                        .ForDate(date => date
                            .Clear()
                            .IsGreaterThanOrEqualTo("Позже либо равно")
                            .IsEqualTo("Равно")
                            .IsNotEqualTo("Не равно")
                            .IsGreaterThan("Позже")
                            .IsLessThanOrEqualTo("Раньше либо равно")
                            .IsLessThan("Раньше"))
                        .ForNumber(number => number
                            .Clear()
                            .IsLessThanOrEqualTo("Меньше либо равно")
                            .IsGreaterThanOrEqualTo("Больше либо равно")
                            .IsEqualTo("Равно")
                            .IsNotEqualTo("Не равно")
                            .IsGreaterThan("Больше")
                            .IsLessThan("Меньше"))
                        .ForEnums(en => en
                            .Clear()
                            .IsEqualTo("Равно")
                            .IsNotEqualTo("Не равно"))
                    ));
        }
    }
}