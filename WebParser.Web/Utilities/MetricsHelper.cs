﻿using System.Web;
using WebParser.Application.Interfaces.Services.Settings;
using WebParser.Configuration;

namespace WebParser.Web.Utilities
{
    public static class MetricsHelper
    {
        public static HtmlString MetricGoal(string goalName)
        {
            var commonSettings = ContainerConfig.Resolve<ICommonSettings>();
            if (commonSettings.GetRenderMetrics())
            {
                return new HtmlString($"onclick=\"{commonSettings.GetYandexMetrikaCounterName()}.reachGoal('{goalName}'); return true;\"");
            }
            return new HtmlString(string.Empty);
        }

        public static HtmlString MetricGoalForPage(string goalName)
        {
            var commonSettings = ContainerConfig.Resolve<ICommonSettings>();
            if (commonSettings.GetRenderMetrics())
            {
                return
                    new HtmlString(
                        $@"
<script type='text/javascript'>
    window.onload = function() {{
        {commonSettings.GetYandexMetrikaCounterName()}.reachGoal('{goalName}')
    }}
</script>");
            }
            return new HtmlString(string.Empty);
        }
    }
}