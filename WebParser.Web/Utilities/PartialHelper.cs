﻿using System.Web.Mvc;
using System.Web.Mvc.Html;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Common.Mappers;
using WebParser.Controllers.Mappers;
using WebParser.Controllers.ViewModels.ArticleGroup;
using WebParser.Controllers.ViewModels.Shared;
using WebParser.Common.Extensions;
using WebParser.Configuration;
using WebParser.Controllers.ViewModels.User;

namespace WebParser.Web.Utilities
{
    public static class PartialHelper
    {
        public static MvcHtmlString ArticlesGroupTemplate(this HtmlHelper htmlHelper)
        {
            return htmlHelper.Partial("~/Views/ArticlesGroup/_ArticlesGroupTemplate.cshtml", new EditArticlesGroupModel
            {
                SpiderTypes = SpiderTypes.GetItems().ToArray(ViewModelMapper.Map<Item, ItemViewModel>)
            });
        }

        public static MvcHtmlString PublisherTemplate(this HtmlHelper htmlHelper)
        {
            var sites = ContainerConfig.Resolve<ISiteStructureService>().GetAvailableSites().ToArray(x => x.Map<Site, ItemViewModel>());
            return htmlHelper.Partial("~/Views/User/_PublisherTemplate.cshtml", new PublisherItemViewModel
            {
                Sites = sites,
            });
        }

        public static MvcHtmlString EditButton(this HtmlHelper htmlHelper)
        {
            return htmlHelper.EditButton("js-edit-item");
        }

        public static MvcHtmlString EditButton(this HtmlHelper htmlHelper, string buttonJsClass)
        {
            return htmlHelper.Partial("~/Views/Shared/_Edit.cshtml", buttonJsClass);
        }
    }
}