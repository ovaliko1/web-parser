﻿using Newtonsoft.Json;

namespace WebParser.Integration.Vk.Duke.Owin.VkMiddleware.Provider
{
    public class VkApiResponse<T>
    {
        [JsonProperty("response")]
        public T[] Response { get; set; }
    }
}