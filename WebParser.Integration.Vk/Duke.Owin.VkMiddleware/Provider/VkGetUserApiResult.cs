﻿using Newtonsoft.Json;

namespace WebParser.Integration.Vk.Duke.Owin.VkMiddleware.Provider
{
    public class VkGetUserApiResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("screen_name")]
        public string ScreenName { get; set; }

        [JsonProperty("nickname")]
        public string Nickname { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("photo_50")]
        public string Photo50 { get; set; }
    }
}