﻿namespace WebParser.Integration.Vk.Entities
{
    public class VkApartmentDetails
    {
        public int Id { get; set; }

        public string ObjectName { get; set; }

        public string PriceStr { get; set; }

        public string Metro { get; set; }

        public string DistanceToMetroText { get; set; }

        public string DurationToMetroText { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public string Characteristics { get; set; }

        public string[] Images { get; set; }
    }
}