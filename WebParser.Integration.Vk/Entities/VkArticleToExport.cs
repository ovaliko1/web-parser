﻿namespace WebParser.Integration.Vk.Entities
{
    public class VkArticleToExport
    {
        public VkArticlesGroupInfo ArticlesGroupInfo { get; set; }

        public VkApartmentDetails ApartmentDetails { get; set; }
    }
}