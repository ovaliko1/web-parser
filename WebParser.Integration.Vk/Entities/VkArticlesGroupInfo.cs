﻿namespace WebParser.Integration.Vk.Entities
{
    public class VkArticlesGroupInfo
    {
        public int SiteId { get; set; }

        public int UserId { get; set; }

        public string SiteName { get; set; }

        public int ArticleGroupId { get; set; }

        public long VkGroupId { get; set; }

        public string ArticleGroupName { get; set; }

        public long VkTopicId { get; set; }

        public long VkTopicGroupId { get; set; }
    }
}