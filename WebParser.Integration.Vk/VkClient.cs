﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using RestSharp;
using VkNet;
using VkNet.Exception;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Model.RequestParams;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Interfaces.Services.Settings;
using WebParser.Common.Extensions;
using WebParser.Integration.Vk.Entities;
using VkSettings = VkNet.Enums.Filters.Settings;
using WebParser.Application.Interfaces.Services.WebClient;
using Cookie = WebParser.Application.Interfaces.Services.SiteStructure.Cookie;

namespace WebParser.Integration.Vk
{
    internal class VkClient : IVkClient
    {
        private const int DelayBetweenArticlePostInSeconds = 120;
        private const int MaxImagesPerArticle = 5;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ICommonSettings Settings { get; }

        private IWebClient WebClient { get; }

        public VkClient(
            ICommonSettings settings,
            IWebClient webClient)
        {
            Settings = settings;
            WebClient = webClient;
        }

        public async Task Export(VkArticleToExport[] articles, Action<int, int, string> logProgressAction)
        {
            if (articles.Length == 0)
            {
                return;
            }

            var appId = (ulong)Settings.GetVkApplicationId();
            var login = Settings.GetVkLogin();
            var password = Settings.GetVkPassword();
            var settings = VkSettings.Photos | VkSettings.Wall | VkSettings.Groups;

            var api = new VkApi();
            api.Authorize(new ApiAuthParams
            {
                ApplicationId = appId,
                Login = login,
                Password = password,
                Settings = settings,
            });

            var articleGroups = articles.GroupBy(
                x => x.ArticlesGroupInfo,
                new Common.Extensions.EqualityComparer<VkArticlesGroupInfo>((x, y) => x.ArticleGroupId == y.ArticleGroupId, x => x.ArticleGroupId.GetHashCode()));

            var articlePreviewUrlFormat = Settings.GetViewerArticlePreviewUrlFormat();

            foreach (var articleGroup in articleGroups)
            {
                var parsedArticles = articleGroup.ToArray(x => x);
                int successCount = 0;
                foreach (var article in parsedArticles)
                {
                    try
                    {
                        await PostArticle(api, article, articlePreviewUrlFormat);
                        successCount++;
                    }
                    // VkNet bug workaround
                    catch (InvalidOperationException ex)
                    {
                        if (ex.StackTrace.Contains("PostLimitException..ctor") || ex.StackTrace.Contains("CaptchaNeededException..ctor"))
                        {
                            break;
                        }
                        Log.Error("Unhandled Exception during VkClient Export", ex);
                    }
                    catch (PostLimitException)
                    {
                        break;
                    }
                    catch (VkApiException ex)
                    {
                        if (ex.Message.StartsWith("Internal server error: Database problems, try later"))
                        {
                            Log.Warn($"VkApiException: HResult: {ex.HResult} HelpLink: {ex.HelpLink}", ex);
                            return;
                        }
                        Log.Error(
                            $"Unhandled VkApiException during VkClient Export. HResult: {ex.HResult} HelpLink: {ex.HelpLink}",
                            ex);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Unhandled exception during VkClient Export", ex);
                    }

                    await Task.Delay(TimeSpan.FromSeconds(DelayBetweenArticlePostInSeconds));
                }

                logProgressAction?.Invoke(articleGroup.Key.SiteId, articleGroup.Key.UserId,
                    $"VK EXPORT: Posted {successCount}/{parsedArticles.Length} articles to Vk. Article Group Name {articleGroup.Key.ArticleGroupName}, Vk group id: {articleGroup.Key.VkGroupId}, from site {articleGroup.Key.SiteName}");
            }
        }

        private async Task PostArticle(VkApi api, VkArticleToExport article, string articlePreviewUrlFormat)
        {
            var uri = new Uri(string.Format(articlePreviewUrlFormat, article.ApartmentDetails.Id, "vk", "export_to_social_media", $"{article.ArticlesGroupInfo.UserId}_{article.ArticlesGroupInfo.ArticleGroupName}_club{article.ArticlesGroupInfo.VkTopicGroupId}_topic{article.ArticlesGroupInfo.VkTopicId}"));
            var link = new Link
            {
                Uri = uri,
                IsExternal = true,
                Caption = "Все объявления от собственников в одном месте!",
                Button = new LinkButton
                {
                    Title = "Перейти",
                    Uri = new LinkButtonAction
                    {
                        Type = "open_url",
                        Uri = uri,
                    },
                },
                PreviewUrl = uri,
                Title = "Все объявления от собственников в одном месте!",
            };

            var photos = await GetPhotoAttachments(api, article);
            var postBody = GeneratePostBody(article, uri);
            var attachments = photos.Concat(link).ToArray();

            api.Board.CreateComment(new BoardCreateCommentParams
            {
                GroupId = article.ArticlesGroupInfo.VkTopicGroupId,
                TopicId = article.ArticlesGroupInfo.VkTopicId,
                Message = postBody,
                Attachments = attachments,
                FromGroup = true,
            });

            uri = new Uri(string.Format(articlePreviewUrlFormat, article.ApartmentDetails.Id, "vk", "export_to_social_media", $"{article.ArticlesGroupInfo.UserId}_{article.ArticlesGroupInfo.ArticleGroupName}_club{article.ArticlesGroupInfo.VkGroupId}"));
            link.Uri = uri;
            link.Button.Uri.Uri = uri;
            link.PreviewUrl = uri;

            api.Wall.Post(new WallPostParams
            {
                OwnerId = -article.ArticlesGroupInfo.VkGroupId,
                Message = postBody,
                Attachments = attachments,
                FromGroup = true,
            });
        }

        private async Task<MediaAttachment[]> GetPhotoAttachments(VkApi api, VkArticleToExport article)
        {
            var imagesData = new List<FileDetails>(MaxImagesPerArticle);
            foreach (var url in article.ApartmentDetails.Images.Take(MaxImagesPerArticle))
            {
                var file = await WebClient.DownloadFile(article.ArticlesGroupInfo.SiteId, article.ArticlesGroupInfo.UserId, url, url, new Cookie[0]);
                if (file?.FileData?.Length > 0)
                {
                    imagesData.Add(file);
                }
            }
            if (imagesData.Count == 0)
            {
                return new Photo[0];
            }

            var server = api.Photo.GetUploadServer(Settings.GetVkImageUploadAlbumId(), Settings.GetVkImageUploadGroupId());

            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "multipart/form-data");
            for (int i = 0; i < imagesData.Count; i++)
            {
                var imageData = imagesData[i];
                request.AddFile($"file{i + 1}", imageData.FileData, $"image.{imageData.FileExtension}");
            }
            var response = new RestClient(server.UploadUrl).Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                Log.Error($"Failed to upload images to Vk server. status {response.StatusCode} site: {article.ArticlesGroupInfo.SiteName} ArticleGroupName: {article.ArticlesGroupInfo.ArticleGroupName}", response.ErrorException);
                return new Photo[0];
            }

            var photos = api.Photo.Save(new PhotoSaveParams
            {
                GroupId = Settings.GetVkImageUploadGroupId(),
                AlbumId = Settings.GetVkImageUploadAlbumId(),
                SaveFileResponse = response.Content,
            });
            return photos.ToArray();
        }

        private string GeneratePostBody(VkArticleToExport article, Uri link)
        {
            var apartmentDetails = article.ApartmentDetails;

            var metroString =
                $"{apartmentDetails.Metro} {Append(apartmentDetails.DurationToMetroText, " пешком")} {apartmentDetails.DistanceToMetroText}";

            return
$@"
Связаться: {link}

Объект: {apartmentDetails.ObjectName}
Цена: {apartmentDetails.PriceStr}
Метро: {metroString}
Адрес: {apartmentDetails.Address}
Описание: {apartmentDetails.Description}
Характеристики: {apartmentDetails.Characteristics}";
        }

        private string Append(string str, string strToAppend)
        {
            return string.IsNullOrEmpty(str) ? string.Empty : str + strToAppend;
        }
    }
}