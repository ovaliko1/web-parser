﻿using System;
using System.Threading.Tasks;
using WebParser.Integration.Vk.Entities;

namespace WebParser.Integration.Vk
{
    public interface IVkClient
    {
        Task Export(VkArticleToExport[] articles, Action<int, int, string> logProgressAction);
    }
}