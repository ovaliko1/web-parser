﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("WebParser.Integration.Vk")]
[assembly: Guid("1ddb3d22-9967-4fea-8cb0-9395318be1d1")]

[assembly: InternalsVisibleTo("WebParser.Configuration")]
[assembly: InternalsVisibleTo("WebParser.Tests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]