﻿namespace WebParser.DataAccess
{
    public interface IRepository
    {
        DataBaseContext GetContext();
    }
}
