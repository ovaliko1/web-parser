﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using WebParser.DataAccess.Entities;
using WebParser.DataAccess.Entities.Identity;
using WebParser.DataAccess.Migration;

namespace WebParser.DataAccess
{
    public class DataBaseContext : IdentityDbContext<DbUser, DbRole, int, UserLogin, UserRole, UserClaim>
    {
        public DataBaseContext() : base("DataBaseContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataBaseContext, Configuration>());
            //Database.SetInitializer(new DataBaseContextInit());

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Properties<DateTime>()
                        .Configure(c => c.HasColumnType("datetime2"));

            modelBuilder.Entity<DbUser>()
                .HasOptional(x => x.PublisherArticlesGroup)
                .WithMany(x => x.Publishers);

            modelBuilder.Entity<DbRole>();

            modelBuilder.Entity<DbParsedArticle>()
                .HasKey(x => new { x.Id, x.UserId });

            modelBuilder.Entity<DbFile>()
                .HasKey(x => new { x.Id, x.UserId });

            modelBuilder.Entity<DbSite>()
                .HasKey(x => new { x.Id, x.UserId })
                .HasMany(x => x.ParsedArticles)
                .WithOptional(x => x.Site)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DbArticlesGroup>()
                .HasKey(x => new { x.Id, x.UserId });

            modelBuilder.Entity<DbParsingLog>()
                .HasKey(x => new { x.Id, x.UserId });

            modelBuilder.Entity<DbArticleRatingVote>()
                .HasKey(x => new { x.Id, x.UserId});

            modelBuilder.Entity<DbBlackListItem>()
                .HasKey(x => new { x.Id });

            modelBuilder.Entity<DbParsedArticle>().Property(x => x.Latitude).HasPrecision(16, 7);
            modelBuilder.Entity<DbParsedArticle>().Property(x => x.Longitude).HasPrecision(16, 7);

            modelBuilder.Entity<DbCity>();
            modelBuilder.Entity<DbDistrict>();
            modelBuilder.Entity<DbMetroLine>();
            modelBuilder.Entity<DbMetroStation>();
            modelBuilder.Entity<DbWhiteListItem>()
                .HasKey(x => new { x.Id });

            modelBuilder.Entity<DbOpenedContact>()
                .HasKey(x => new { x.Id, x.UserId });

            modelBuilder.Entity<DbCitySeoParameter>();
            modelBuilder.Entity<DbDistrictSeoParameter>();
            modelBuilder.Entity<DbArticleGroupSeoParameter>();
            modelBuilder.Entity<DbAliasSeoParameter>();
            modelBuilder.Entity<DbPhraseSeoParameter>();
            modelBuilder.Entity<DbCountry>();
        }

        static DataBaseContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataBaseContext, Configuration>());
        }

        public static DataBaseContext Create()
        {
            return new DataBaseContext();
        }
    }
}
