﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using WebParser.DataAccess.Entities;

namespace WebParser.DataAccess.Identity
{
    public class AppRoleManager : RoleManager<DbRole, int>
    {
        public AppRoleManager(CustomRoleStore store) : base(store)
        {
        }

        public static AppRoleManager Create(IdentityFactoryOptions<AppRoleManager> options, IOwinContext context)
        {
            return new AppRoleManager(new CustomRoleStore(context.Get<DataBaseContext>()));
        }
    }
}