﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WebParser.DataAccess.Entities;

namespace WebParser.DataAccess.Identity
{
    internal static class IdentityUserHelper
    {
        public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(this DbUser user, UserManager<DbUser, int> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(user, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
