﻿using Microsoft.AspNet.Identity.EntityFramework;
using WebParser.DataAccess.Entities;
using WebParser.DataAccess.Entities.Identity;

namespace WebParser.DataAccess.Identity
{
    public class CustomRoleStore : RoleStore<DbRole, int, UserRole>
    {
        public CustomRoleStore(DataBaseContext context)
            : base(context)
        {
        }
    }
}