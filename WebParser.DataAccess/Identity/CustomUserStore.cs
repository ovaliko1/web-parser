﻿using Microsoft.AspNet.Identity.EntityFramework;
using WebParser.DataAccess.Entities;
using WebParser.DataAccess.Entities.Identity;

namespace WebParser.DataAccess.Identity
{
    public class CustomUserStore : UserStore<DbUser, DbRole, int, UserLogin, UserRole, UserClaim>
    {
        public CustomUserStore(DataBaseContext context)
            : base(context)
        {
        }
    }
}