namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0010OpenedContactRateTypeId : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.OpenedContact ADD RateTypeId INT NULL
GO

UPDATE dbo.OpenedContact SET RateTypeId = 1 -- Openings
GO

ALTER TABLE dbo.OpenedContact ALTER COLUMN RateTypeId INT NOT NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
GO
");
        }
    }
}
