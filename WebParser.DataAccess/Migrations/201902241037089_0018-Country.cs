﻿namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0018Country : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE TABLE dbo.Country (
    Id int NOT NULL IDENTITY(1,1),
    Name nvarchar(50),
    Iso nvarchar(3),
    LanguageCode nvarchar(3),
    CountryRegionCode nvarchar(10),
    CONSTRAINT [PK_dbo.Country] PRIMARY KEY (Id)
)
GO

SET IDENTITY_INSERT dbo.Country ON
INSERT INTO dbo.Country (Id, Name, Iso, LanguageCode, CountryRegionCode)
     VALUES 
        (1, N'Россия', N'RU', N'ru', N'RU'),
        (2, N'България', N'BG', N'bg', N'BG')
SET IDENTITY_INSERT dbo.Country OFF
GO

ALTER TABLE dbo.City ADD CountryId INT NULL
GO

UPDATE dbo.City SET CountryId = 1
GO

ALTER TABLE dbo.City ALTER COLUMN CountryId INT NOT NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
");
        }
    }
}
