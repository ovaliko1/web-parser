namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0007VkTopicGroupId : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE [dbo].[ArticlesGroup] ADD VkTopicGroupId BIGINT NULL
GO

UPDATE [dbo].[ArticlesGroup] SET VkTopicGroupId = VkGroupId
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
ALTER TABLE [dbo].[ArticlesGroup] DROP COLUMN VkTopicGroupId
GO
");
        }
    }
}
