namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0000_Initial : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE TABLE [dbo].[ArticleRatingVote](
    [ArticleId] [int] NOT NULL,
    [ArticleUserId] [int] NOT NULL,
    [UserId] [int] NOT NULL,
    [VotedDate] [datetime2](7) NOT NULL,
    [ArticleRatingTypeId] [int] NOT NULL,
    [ConsideredVote] [bit] NOT NULL,
CONSTRAINT [PK_dbo.ArticleRatingVote] PRIMARY KEY CLUSTERED 
(
    [ArticleId] ASC,
    [ArticleUserId] ASC,
    [UserId] ASC
)) 

GO
 
CREATE TABLE [dbo].[ArticlesGroup](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [SiteId] [int] NOT NULL,
    [Name] [nvarchar](max) NOT NULL,
    [Links] [nvarchar](max) NULL,
    [LinksToArticlesSearchRule] [nvarchar](max) NOT NULL,
    [ArticleSearchRules] [nvarchar](max) NOT NULL,
    [LinksFormat] [nvarchar](max) NULL,
    [StartPage] [int] NULL,
    [StepSize] [int] NULL,
    [LinksFileId] [int] NULL,
    [UserId] [int] NOT NULL,
    [VkGroupId] [bigint] NULL,
    [SpiderTypeId] [int] NOT NULL,
    [BatchSize] [int] NOT NULL,
    [BatchDelayInSeconds] [int] NOT NULL,
    [DownloadFileDelayInMillseconds] [int] NOT NULL,
    [SpiderDoNotLoadImages] [bit] NOT NULL,
    [WaitMilliseconds] [int] NULL,
    [TwitterConsumerKey] [nvarchar](256) NULL,
    [TwitterConsumerSecret] [nvarchar](256) NULL,
    [TwitterAccessToken] [nvarchar](256) NULL,
    [TwitterAccessTokenSecret] [nvarchar](256) NULL,
CONSTRAINT [PK_dbo.ArticlesGroup] PRIMARY KEY CLUSTERED 
(
    [Id] ASC,
    [UserId] ASC
) 
)
GO
 
CREATE TABLE [dbo].[AspNetRoles](
    [Name] [nvarchar](256) NOT NULL,
    [Id] [int] IDENTITY(1,1) NOT NULL,
CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
) 
)
GO
 
CREATE TABLE [dbo].[AspNetUserClaims](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [ClaimType] [nvarchar](max) NULL,
    [ClaimValue] [nvarchar](max) NULL,
    [UserId] [int] NOT NULL,
CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
) 
)
GO
 
CREATE TABLE [dbo].[AspNetUserLogins](
    [LoginProvider] [nvarchar](128) NOT NULL,
    [ProviderKey] [nvarchar](128) NOT NULL,
    [UserId] [int] NOT NULL,
CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
    [LoginProvider] ASC,
    [ProviderKey] ASC,
    [UserId] ASC
) 
)
GO
 
CREATE TABLE [dbo].[AspNetUserRoles](
    [UserId] [int] NOT NULL,
    [RoleId] [int] NOT NULL,
CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
    [UserId] ASC,
    [RoleId] ASC
) 
)
GO
 
CREATE TABLE [dbo].[AspNetUsers](
    [MaxSiteCount] [int] NOT NULL,
    [Email] [nvarchar](256) NULL,
    [EmailConfirmed] [bit] NOT NULL,
    [PasswordHash] [nvarchar](max) NULL,
    [SecurityStamp] [nvarchar](max) NULL,
    [PhoneNumber] [nvarchar](max) NULL,
    [PhoneNumberConfirmed] [bit] NOT NULL,
    [TwoFactorEnabled] [bit] NOT NULL,
    [LockoutEndDateUtc] [datetime2](7) NULL,
    [LockoutEnabled] [bit] NOT NULL,
    [AccessFailedCount] [int] NOT NULL,
    [UserName] [nvarchar](256) NOT NULL,
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [NameForViewers] [nvarchar](256) NULL,
    [AccessDateTime] [datetime2](7) NOT NULL,
    [PurchasedAccount] [bit] NOT NULL,
    [FreeDays] [int] NOT NULL,
    [ClickerCautionCount] [int] NOT NULL,
    [RegistrationIp] [nvarchar](20) NULL,
    [SeenForbidden] [bit] NOT NULL,
    [VkId] [bigint] NULL,
    [SharingMade] [bit] NOT NULL,
    [WaitingForPayment] [bit] NOT NULL,
    [SerializedPaymentConfirmations] [nvarchar](max) NULL,
    [PublisherArticleGroupId] [int] NULL,
    [PublisherArticleGroupUserId] [int] NULL,
    [CreatedDate] [datetime2](7) NOT NULL,
    [PublicId] [uniqueidentifier] NOT NULL,
CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
) 
)
GO
 
CREATE TABLE [dbo].[BlackListItem](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [Identity] [nvarchar](20) NOT NULL,
    [IdentityConfidencePercentage] [int] NOT NULL,
    [CreatedById] [int] NOT NULL,
    [SiteId] [int] NULL,
    [ArticleId] [int] NULL,
    [ArticleUserId] [int] NULL,
    [CreatedDate] [datetime2](7) NOT NULL,
    [ArticleSiteId] [int] NULL,
CONSTRAINT [PK_dbo.BlackListItem] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
) 
)
GO
 
CREATE TABLE [dbo].[File](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [SiteId] [int] NULL,
    [FileKey] [nvarchar](max) NOT NULL,
    [OriginalFileName] [nvarchar](max) NULL,
    [FileExtension] [nvarchar](max) NOT NULL,
    [ContentType] [nvarchar](max) NULL,
    [CreatedDate] [datetime2](7) NOT NULL,
    [FileUrl] [nvarchar](max) NULL,
    [UserId] [int] NOT NULL,
CONSTRAINT [PK_dbo.File] PRIMARY KEY CLUSTERED 
(
    [Id] ASC,
    [UserId] ASC
) 
)
GO
 
CREATE TABLE [dbo].[ParsedArticle](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [SiteId] [int] NULL,
    [ArticlesGroupId] [int] NULL,
    [Link] [nvarchar](max) NULL,
    [CreatedDate] [datetime2](7) NOT NULL,
    [ParsedContent] [nvarchar](max) NULL,
    [UserId] [int] NOT NULL,
    [GoodRating] [int] NOT NULL,
    [BadRating] [int] NOT NULL,
    [OutdateRating] [int] NOT NULL,
    [Metro] [nvarchar](256) NULL,
    [PriceStr] [nvarchar](256) NULL,
    [Identity] [nvarchar](20) NULL,
    [IdentityConfidencePercentage] [int] NOT NULL,
    [IsDuplicate] [bit] NOT NULL,
    [DuplicateGroupId] [uniqueidentifier] NULL,
    [BlackListItemId] [int] NULL,
    [Price] [int] NULL,
    [FormattedAddress] [nvarchar](256) NULL,
    [Longitude] [decimal](19, 4) NULL,
    [Latitude] [decimal](19, 4) NULL,
    [DistanceToMetroText] [nvarchar](256) NULL,
    [DistanceToMetroValue] [int] NULL,
    [DurationToMetroText] [nvarchar](256) NULL,
    [DurationToMetroValue] [int] NULL,
CONSTRAINT [PK_dbo.ParsedArticle] PRIMARY KEY CLUSTERED 
(
    [Id] ASC,
    [UserId] ASC
)
)
GO
 
CREATE TABLE [dbo].[ParsingLog](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [UserId] [int] NOT NULL,
    [Date] [datetime2](7) NOT NULL,
    [SiteId] [int] NULL,
    [Status] [int] NOT NULL,
    [Message] [nvarchar](max) NOT NULL,
CONSTRAINT [PK_dbo.ParsingLog] PRIMARY KEY CLUSTERED 
(
    [Id] ASC,
    [UserId] ASC
) 
)
GO
 
CREATE TABLE [dbo].[Site](
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [Link] [nvarchar](max) NOT NULL,
    [Name] [nvarchar](max) NOT NULL,
    [IsEnabled] [bit] NOT NULL,
    [SchedulerStartHours] [int] NULL,
    [SchedulerStartMinutes] [int] NULL,
    [IsInParsing] [bit] NOT NULL,
    [ProxyAddress] [nvarchar](256) NULL,
    [Cookies] [nvarchar](max) NULL,
    [OneTimeExport] [bit] NOT NULL,
    [NeedStartOneTimeParsing] [bit] NOT NULL,
    [UserId] [int] NOT NULL,
    [WebHookUrl] [nvarchar](256) NULL,
    [SchedulerEndHours] [int] NULL,
    [SchedulerEndMinutes] [int] NULL,
    [RepeatIntervalInMunutes] [int] NULL,
    [WatermarkHeight] [int] NULL,
    [WatermarkWidth] [int] NULL,
    [WatermarkFontSize] [int] NULL,
    [WatermarkRectX] [nvarchar](256) NULL,
    [WatermarkRectY] [nvarchar](256) NULL,
    [WatermarkTextX] [nvarchar](256) NULL,
    [WatermarkTextY] [nvarchar](256) NULL,
    [UserAgent] [nvarchar](256) NULL,
    [ForBlackListJob] [bit] NOT NULL,
    [BlackListIdentitySiteId] [int] NULL,
    [BlackListIdentityUserId] [int] NULL,
    [GeoBounds] [nvarchar](256) NULL,
    [UseSourceLinkAsContact] [bit] NOT NULL,
    [CropImageTop] [int] NOT NULL,
    [CropImageBottom] [int] NOT NULL,
    [CropImageLeft] [int] NOT NULL,
    [CropImageRight] [int] NOT NULL,
CONSTRAINT [PK_dbo.Site] PRIMARY KEY CLUSTERED 
(
    [Id] ASC,
    [UserId] ASC
) 
)
GO

-- Indexes
CREATE NONCLUSTERED INDEX [IX_ArticleId_ArticleUserId] ON [dbo].[ArticleRatingVote]
(
    [ArticleId] ASC,
    [ArticleUserId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[ArticleRatingVote]
(
    [UserId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_LinksFileId_UserId] ON [dbo].[ArticlesGroup]
(
    [LinksFileId] ASC,
    [UserId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_SiteId_UserId] ON [dbo].[ArticlesGroup]
(
    [SiteId] ASC,
    [UserId] ASC
)
GO


CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
    [Name] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
    [UserId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
    [UserId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
    [RoleId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
    [UserId] ASC
)
GO


CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
    [UserName] ASC
)
GO


CREATE NONCLUSTERED INDEX [IX_tbl_BlackListItem_Identity_IdentityConfidencePercentage_SiteId] ON [dbo].[BlackListItem]
(
    [Identity] ASC,
    [IdentityConfidencePercentage] ASC,
    [SiteId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_SiteId_UserId] ON [dbo].[File]
(
    [SiteId] ASC,
    [UserId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_ArticlesGroupId_UserId] ON [dbo].[ParsedArticle]
(
    [ArticlesGroupId] ASC,
    [UserId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_ParsedArticle_ArticlesGroupId] ON [dbo].[ParsedArticle]
(
    [ArticlesGroupId] ASC
)
INCLUDE (     [Id],
    [CreatedDate],
    [UserId],
    [GoodRating],
    [BadRating],
    [OutdateRating]) 
GO


CREATE NONCLUSTERED INDEX [IX_tbl_ParsedArticle_Identity_IdentityConfidencePercentage_CreatedDate_SiteId] ON [dbo].[ParsedArticle]
(
    [Identity] ASC,
    [IdentityConfidencePercentage] ASC,
    [CreatedDate] ASC,
    [SiteId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_UserId_SiteId] ON [dbo].[ParsedArticle]
(
    [UserId] ASC,
    [SiteId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_SiteId_UserId] ON [dbo].[ParsingLog]
(
    [SiteId] ASC,
    [UserId] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[Site]
(
    [UserId] ASC
)
GO

-- Foreign keys
ALTER TABLE [dbo].[ArticleRatingVote] ADD  CONSTRAINT [FK_dbo.ArticleRatingVote_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[ArticleRatingVote] ADD  CONSTRAINT [FK_dbo.ArticleRatingVote_dbo.ParsedArticle_ArticleId_ArticleUserId] FOREIGN KEY([ArticleId], [ArticleUserId])
REFERENCES [dbo].[ParsedArticle] ([Id], [UserId])
GO

ALTER TABLE [dbo].[ArticlesGroup] ADD  CONSTRAINT [FK_dbo.ArticlesGroup_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[ArticlesGroup]  ADD  CONSTRAINT [FK_dbo.ArticlesGroup_dbo.File_LinksFileId_UserId] FOREIGN KEY([LinksFileId], [UserId])
REFERENCES [dbo].[File] ([Id], [UserId])
GO

ALTER TABLE [dbo].[ArticlesGroup] ADD  CONSTRAINT [FK_dbo.ArticlesGroup_dbo.Site_SiteId_UserId] FOREIGN KEY([SiteId], [UserId])
REFERENCES [dbo].[Site] ([Id], [UserId])
GO

ALTER TABLE [dbo].[AspNetUserClaims] ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[AspNetUserLogins] ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[AspNetUserRoles] ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
GO

ALTER TABLE [dbo].[AspNetUserRoles] ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[AspNetUsers] ADD  CONSTRAINT [FK_dbo.AspNetUsers_dbo.ArticlesGroup_PublisherArticleGroupId_PublisherArticleGroupUserId] FOREIGN KEY([PublisherArticleGroupId], [PublisherArticleGroupUserId])
REFERENCES [dbo].[ArticlesGroup] ([Id], [UserId])
GO

ALTER TABLE [dbo].[BlackListItem]  ADD  CONSTRAINT [FK_dbo.BlackListItem_dbo.AspNetUsers_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[BlackListItem] ADD  CONSTRAINT [FK_dbo.BlackListItem_dbo.ParsedArticle_ArticleId_ArticleUserId] FOREIGN KEY([ArticleId], [ArticleUserId])
REFERENCES [dbo].[ParsedArticle] ([Id], [UserId])
GO

ALTER TABLE [dbo].[File] ADD  CONSTRAINT [FK_dbo.File_dbo.Site_SiteId_UserId] FOREIGN KEY([SiteId], [UserId])
REFERENCES [dbo].[Site] ([Id], [UserId])
GO

ALTER TABLE [dbo].[ParsedArticle] ADD  CONSTRAINT [FK_dbo.ParsedArticle_dbo.ArticlesGroup_ArticlesGroupId_UserId] FOREIGN KEY([ArticlesGroupId], [UserId])
REFERENCES [dbo].[ArticlesGroup] ([Id], [UserId])
GO

ALTER TABLE [dbo].[ParsedArticle] ADD  CONSTRAINT [FK_dbo.ParsedArticle_dbo.BlackListItem_BlackListItemId] FOREIGN KEY([BlackListItemId])
REFERENCES [dbo].[BlackListItem] ([Id])
GO

ALTER TABLE [dbo].[ParsedArticle] ADD  CONSTRAINT [FK_dbo.ParsedArticle_dbo.Site_SiteId_UserId] FOREIGN KEY([SiteId], [UserId])
REFERENCES [dbo].[Site] ([Id], [UserId])
GO

ALTER TABLE [dbo].[ParsingLog] ADD  CONSTRAINT [FK_dbo.ParsingLog_dbo.Site_SiteId_UserId] FOREIGN KEY([SiteId], [UserId])
REFERENCES [dbo].[Site] ([Id], [UserId])
GO

ALTER TABLE [dbo].[Site] ADD  CONSTRAINT [FK_dbo.Site_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[Site] ADD  CONSTRAINT [FK_dbo.Site_dbo.Site_BlackListIdentitySiteId_BlackListIdentityUserId] FOREIGN KEY([BlackListIdentitySiteId], [BlackListIdentityUserId])
REFERENCES [dbo].[Site] ([Id], [UserId])
GO

-- Check constaints
ALTER TABLE [dbo].[ArticlesGroup] ADD  CONSTRAINT [CK_tbl_ArticlesGroup_BatchSize] CHECK  (([BatchSize]>(0)))
GO

ALTER TABLE [dbo].[ArticlesGroup] ADD  CONSTRAINT [CK_tbl_ArticlesGroup_Links] CHECK  (([Links] IS NULL AND [LinksFormat] IS NOT NULL AND [StartPage] IS NOT NULL AND [StepSize] IS NOT NULL OR [Links] IS NOT NULL AND [LinksFormat] IS NULL AND [StartPage] IS NULL AND [StepSize] IS NULL))
GO

ALTER TABLE [dbo].[BlackListItem] ADD  CONSTRAINT [CK_tbl_BlackListItem_ArticleSiteId_SiteId_ArticleId] CHECK  (([ArticleId] IS NOT NULL AND [ArticleSiteId] IS NOT NULL AND [SiteId] IS NULL OR [ArticleId] IS NULL AND [ArticleSiteId] IS NULL AND [SiteId] IS NOT NULL))
GO

ALTER TABLE [dbo].[Site] ADD  CONSTRAINT [CK_tbl_Site_SchedulerSettings] CHECK  (([OneTimeExport]=(0) AND [SchedulerStartHours] IS NOT NULL AND [SchedulerStartMinutes] IS NOT NULL AND [SchedulerEndHours] IS NOT NULL AND [SchedulerEndMinutes] IS NOT NULL AND [RepeatIntervalInMunutes] IS NOT NULL OR [OneTimeExport]=(1)))
GO
");
        }
        
        public override void Down()
        {
            // Drop database instead
        }
    }
}
