namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0008PurchasingOpeningContacts : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE [dbo].[ArticleRatingVote] DROP CONSTRAINT [PK_dbo.ArticleRatingVote]
GO

ALTER TABLE [dbo].[ArticleRatingVote] ADD Id INT IDENTITY(1, 1)
GO

ALTER TABLE [dbo].[ArticleRatingVote] ADD CONSTRAINT [PK_dbo.ArticleRatingVote] PRIMARY KEY CLUSTERED 
(
    [Id] ASC,
    [UserId] ASC
)
GO

ALTER TABLE [dbo].[ArticleRatingVote] ALTER COLUMN ArticleId INT NULL
ALTER TABLE [dbo].[ArticleRatingVote] ALTER COLUMN ArticleUserId INT NULL
GO

CREATE TABLE [dbo].OpenedContact(
    Id INT IDENTITY(1,1) NOT NULL,
    UserId INT NOT NULL,
    ArticleId INT NULL,
    ArticleUserId INT NULL,
    DeletedArticleIdentity NVARCHAR(512) NULL,
    DeletedArticleSummary NVARCHAR(512) NULL,
    CreatedDate DATETIME2(7) NOT NULL,
CONSTRAINT [PK_dbo.OpenedContact] PRIMARY KEY CLUSTERED 
(
    [Id] ASC,
    [UserId] ASC
),
CONSTRAINT [FK_dbo.OpenedContact_dbo.ParsedArticle_ArticleId_ArticleUserId] FOREIGN KEY(ArticleId, ArticleUserId) REFERENCES [dbo].[ParsedArticle] (Id, UserId),
CONSTRAINT [FK_dbo.OpenedContact_dbo.AspNetUsers_UserId] FOREIGN KEY(UserId) REFERENCES dbo.AspNetUsers ([Id]),
)
GO

ALTER TABLE [dbo].[AspNetUsers] ADD OpeningBalance INT NULL
GO

UPDATE [dbo].[AspNetUsers] SET OpeningBalance = 0
GO

ALTER TABLE [dbo].[AspNetUsers] ALTER COLUMN OpeningBalance INT NOT NULL
GO

ALTER TABLE [dbo].BlackListItem ADD DeletedArticleIdentity NVARCHAR(20) NULL
ALTER TABLE [dbo].BlackListItem ADD DeletedArticleSummary NVARCHAR(512) NULL

ALTER TABLE [dbo].WhiteListItem ADD DeletedArticleIdentity NVARCHAR(20) NULL
ALTER TABLE [dbo].WhiteListItem ADD DeletedArticleSummary NVARCHAR(512) NULL

ALTER TABLE [dbo].ArticleRatingVote ADD DeletedArticleIdentity NVARCHAR(20) NULL
ALTER TABLE [dbo].ArticleRatingVote ADD DeletedArticleSummary NVARCHAR(512) NULL
GO

ALTER TABLE [dbo].[ArticleRatingVote] ADD CONSTRAINT [CK_tbl_ArticleRatingVote_TargetArticle] CHECK
((ArticleId IS NULL AND ArticleUserId IS NULL AND DeletedArticleIdentity IS NOT NULL AND DeletedArticleSummary IS NOT NULL) OR 
 (ArticleId IS NOT NULL AND ArticleUserId IS NOT NULL AND DeletedArticleIdentity IS NULL AND DeletedArticleSummary IS NULL))
GO

ALTER TABLE [dbo].BlackListItem ADD CONSTRAINT [CK_tbl_BlackListItem_TargetArticle] CHECK
((ArticleId IS NULL AND ArticleUserId IS NULL AND DeletedArticleIdentity IS NOT NULL AND DeletedArticleSummary IS NOT NULL) OR 
 (ArticleId IS NOT NULL AND ArticleUserId IS NOT NULL AND DeletedArticleIdentity IS NULL AND DeletedArticleSummary IS NULL) OR
 (ArticleId IS NULL AND ArticleUserId IS NULL AND DeletedArticleIdentity IS NULL AND DeletedArticleSummary IS NULL)
)
GO

ALTER TABLE [dbo].WhiteListItem ADD CONSTRAINT [CK_tbl_WhiteListItem_TargetArticle] CHECK
((ArticleId IS NULL AND ArticleUserId IS NULL AND DeletedArticleIdentity IS NOT NULL AND DeletedArticleSummary IS NOT NULL) OR 
 (ArticleId IS NOT NULL AND ArticleUserId IS NOT NULL AND DeletedArticleIdentity IS NULL AND DeletedArticleSummary IS NULL) OR
 (ArticleId IS NULL AND ArticleUserId IS NULL AND DeletedArticleIdentity IS NULL AND DeletedArticleSummary IS NULL)
)
GO

ALTER TABLE [dbo].[OpenedContact] ADD CONSTRAINT [CK_tbl_OpenedContact_TargetArticle] CHECK
((ArticleId IS NULL AND ArticleUserId IS NULL AND DeletedArticleIdentity IS NOT NULL AND DeletedArticleSummary IS NOT NULL) OR 
 (ArticleId IS NOT NULL AND ArticleUserId IS NOT NULL AND DeletedArticleIdentity IS NULL AND DeletedArticleSummary IS NULL))
GO

ALTER TABLE [dbo].[AspNetUsers] ADD CONSTRAINT [CK_tbl_AspNetUsers_OpeningBalance] CHECK
([OpeningBalance] >= 0)
GO

CREATE UNIQUE NONCLUSTERED INDEX UQ_tbl_OpenedContact_UserId_ArticleId_ArticleUserId
ON [dbo].[OpenedContact] (UserId, ArticleId, ArticleUserId)
WHERE ArticleId IS NOT NULL AND ArticleUserId IS NOT NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
GO
");
        }
    }
}
