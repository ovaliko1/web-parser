namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0002ParsedArticleFullSizeImages : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.ParsedArticle ADD FullSizeImages NVARCHAR(MAX) NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
ALTER TABLE dbo.ParsedArticle DROP COLUMN FullSizeImages
GO
");
        }
    }
}
