namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0006VkTopicIdAndHasImages : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE [dbo].[ArticlesGroup] ADD VkTopicId BIGINT NULL
GO

ALTER TABLE [dbo].[ParsedArticle] ADD HasImages BIT NULL
GO

UPDATE [dbo].[ParsedArticle] SET HasImages =
CASE FullSizeImages WHEN N'[]' THEN 0 ELSE 1 END
GO

ALTER TABLE [dbo].[ParsedArticle] ALTER COLUMN HasImages BIT NOT NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
ALTER TABLE [dbo].[ArticlesGroup] DROP COLUMN VkTopicId
ALTER TABLE [dbo].[ParsedArticle] DROP COLUMN HasImages
GO
");
        }
    }
}
