namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0013InternalPublicNameArticlesGroup : DbMigration
    {
        public override void Up()
        {
            Sql(@"
EXEC sys.sp_rename 
    @objname = N'dbo.ArticlesGroup.Name', 
    @newname = 'InternalName', 
    @objtype = 'COLUMN'
GO

ALTER TABLE dbo.ArticlesGroup ADD PublicName NVARCHAR(MAX) NULL
GO

UPDATE dbo.ArticlesGroup SET PublicName = InternalName
GO

ALTER TABLE dbo.ArticlesGroup ALTER COLUMN PublicName NVARCHAR(MAX) NOT NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"");
        }
    }
}
