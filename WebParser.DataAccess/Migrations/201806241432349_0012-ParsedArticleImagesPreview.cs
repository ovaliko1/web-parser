namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0012ParsedArticleImagesPreview : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.ParsedArticle ADD ImagesPreview NVARCHAR(MAX) NULL
GO
");
        }

        public override void Down()
        {
            Sql(@"
ALTER TABLE dbo.ParsedArticle DROP COLUMN ImagesPreview
GO
");
        }
    }
}
