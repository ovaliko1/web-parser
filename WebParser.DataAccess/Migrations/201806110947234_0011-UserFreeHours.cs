namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0011UserFreeHours : DbMigration
    {
        public override void Up()
        {
            Sql(@"
EXEC sys.sp_rename 
    @objname = N'dbo.AspNetUsers.FreeDays', 
    @newname = 'FreeHours', 
    @objtype = 'COLUMN'
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
GO
");
        }
    }
}
