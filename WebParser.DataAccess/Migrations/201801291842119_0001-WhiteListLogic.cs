namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0001WhiteListLogic : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE TABLE [dbo].[WhiteListItem]
(
    [Id] [int] IDENTITY(1,1) NOT NULL,
    [Identity] [nvarchar](20) NOT NULL,
    [IdentityConfidencePercentage] [int] NOT NULL,
    [CreatedById] [int] NOT NULL,
    [SiteId] [int] NULL,
    [ArticleId] [int] NULL,
    [ArticleUserId] [int] NULL,
    [CreatedDate] [datetime2](7) NOT NULL,
    [ArticleSiteId] [int] NULL,
    CONSTRAINT [PK_dbo.WhiteListItem] PRIMARY KEY CLUSTERED 
    (
        [Id] ASC
    ) 
)
GO

ALTER TABLE dbo.ParsedArticle ADD WhiteListItemId INT NULL
GO

ALTER TABLE dbo.Site ADD JobTypeId INT NULL
GO

UPDATE dbo.Site SET JobTypeId = CASE WHEN ForBlackListJob = 0 THEN 1 ELSE 2 END
GO

ALTER TABLE dbo.Site ALTER COLUMN JobTypeId INT NOT NULL
GO

ALTER TABLE dbo.Site DROP COLUMN ForBlackListJob
GO

-- Foreign keys
ALTER TABLE [dbo].[WhiteListItem] ADD CONSTRAINT [FK_dbo.WhiteListItem_dbo.AspNetUsers_CreatedById] FOREIGN KEY([CreatedById])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[WhiteListItem] ADD CONSTRAINT [FK_dbo.WhiteListItem_dbo.ParsedArticle_ArticleId_ArticleUserId] FOREIGN KEY([ArticleId], [ArticleUserId])
REFERENCES [dbo].[ParsedArticle] ([Id], [UserId])
GO

ALTER TABLE [dbo].ParsedArticle ADD CONSTRAINT [FK_dbo.ParsedArticle_dbo.WhiteListItem_WhiteListItemId] FOREIGN KEY([WhiteListItemId])
REFERENCES [dbo].WhiteListItem ([Id])
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
ALTER TABLE dbo.WhiteListItem DROP CONSTRAINT [FK_dbo.WhiteListItem_dbo.AspNetUsers_CreatedById]
GO
ALTER TABLE dbo.WhiteListItem DROP CONSTRAINT [FK_dbo.WhiteListItem_dbo.ParsedArticle_ArticleId_ArticleUserId]
GO
ALTER TABLE dbo.ParsedArticle DROP CONSTRAINT [FK_dbo.ParsedArticle_dbo.WhiteListItem_WhiteListItemId]
GO

ALTER TABLE dbo.Site DROP COLUMN JobTypeId
GO

ALTER TABLE dbo.ParsedArticle DROP COLUMN WhiteListItemId
GO

ALTER TABLE dbo.Site ADD ForBlackListJob BIT NULL
GO

UPDATE dbo.Site SET ForBlackListJob = 0
GO

ALTER TABLE dbo.Site ALTER COLUMN ForBlackListJob BIT NOT NULL
GO

DROP TABLE dbo.WhiteListItem
GO
");
        }
    }
}
