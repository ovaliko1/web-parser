namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0020ParsingFailedCountAndReason : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.ParsedArticle ADD ParsingFailedCount INT NULL
ALTER TABLE dbo.ParsedArticle ADD ParsingFailedReason NVARCHAR(256) NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
");
        }
    }
}
