namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0016CityInternalName : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.City ADD InternalName NVARCHAR(50) NULL
GO

UPDATE dbo.City SET InternalName =
CASE Id
WHEN 1 THEN N'spb'
WHEN 2 THEN N'msk'
END
GO

ALTER TABLE dbo.City ALTER COLUMN InternalName NVARCHAR(256) NOT NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"");
        }
    }
}
