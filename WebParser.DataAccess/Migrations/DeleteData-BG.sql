delete cp from [dbo].[CitySeoParameter] cp
inner join dbo.City c on cp.CityId = c.Id
where c.CountryId = 2
go

delete dp from [dbo].[DistrictSeoParameter] dp
inner join dbo.City c on dp.CityId = c.Id
where c.CountryId = 2
go

delete from [dbo].[PhraseSeoParameter]
where CountryId = 2
go

delete ap from [dbo].[AliasSeoParameter] ap
inner join dbo.City c on ap.CityId = c.Id
where c.CountryId = 2
go

delete from [dbo].[ArticleGroupSeoParameter]
where CountryId = 2
go

delete d from [dbo].[District] d
inner join dbo.City c on d.CityId = c.Id
where c.CountryId = 2
GO

delete from [dbo].City
where CountryId = 2
Go

select * from dbo.City