namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0021CityFreeAccess : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.City ADD FreeAccess BIT NULL
GO

UPDATE dbo.City SET FreeAccess = CASE WHEN CountryId = 2 THEN 1 ELSE 0 END
GO

ALTER TABLE dbo.City ALTER COLUMN FreeAccess BIT NOT NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
");
        }
    }
}
