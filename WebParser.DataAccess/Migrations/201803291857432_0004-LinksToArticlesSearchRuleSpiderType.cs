namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0004LinksToArticlesSearchRuleSpiderType : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.ArticlesGroup ADD LinksToArticlesSearchRuleSpiderTypeId INT NULL
GO

UPDATE dbo.ArticlesGroup SET LinksToArticlesSearchRuleSpiderTypeId = 3
GO

ALTER TABLE dbo.ArticlesGroup ALTER COLUMN LinksToArticlesSearchRuleSpiderTypeId INT NOT NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
ALTER TABLE dbo.ArticlesGroup DROP COLUMN LinksToArticlesSearchRuleSpiderTypeId
GO
");
        }
    }
}
