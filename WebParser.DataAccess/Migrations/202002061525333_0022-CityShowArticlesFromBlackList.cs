namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0022CityShowArticlesFromBlackList : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.City ADD ShowArticlesFromBlackList BIT NULL
GO

UPDATE dbo.City SET ShowArticlesFromBlackList = CASE WHEN CountryId = 2 THEN 1 ELSE 0 END
GO

ALTER TABLE dbo.City ALTER COLUMN ShowArticlesFromBlackList BIT NOT NULL
GO

ALTER TABLE dbo.ParsedArticle ALTER COLUMN Longitude DECIMAL(16, 7) NULL
ALTER TABLE dbo.ParsedArticle ALTER COLUMN Latitude DECIMAL(16, 7) NULL
GO
");
        }

        public override void Down()
        {
            Sql(@"
");
        }
    }
}
