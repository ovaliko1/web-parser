namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0019CountryId : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.PhraseSeoParameter ADD CountryId INT NULL
ALTER TABLE dbo.ArticleGroupSeoParameter ADD CountryId INT NULL
GO

UPDATE dbo.PhraseSeoParameter SET CountryId = 1
UPDATE dbo.ArticleGroupSeoParameter SET CountryId = 1
GO

ALTER TABLE dbo.PhraseSeoParameter ALTER COLUMN CountryId INT NOT NULL
ALTER TABLE dbo.ArticleGroupSeoParameter ALTER COLUMN CountryId INT NOT NULL
GO

ALTER TABLE dbo.PhraseSeoParameter ADD CONSTRAINT [FK_dbo.PhraseSeoParameter_dbo.Country_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id])
ALTER TABLE dbo.ArticleGroupSeoParameter ADD CONSTRAINT [FK_dbo.ArticleGroupSeoParameter_dbo.Country_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id])
ALTER TABLE dbo.City ADD CONSTRAINT [FK_dbo.City_dbo.Country_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id])
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
");
        }
    }
}
