namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0009VoteReviewLogic : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.AspNetUsers ADD FreeOpenings INT NULL
GO

UPDATE dbo.AspNetUsers SET FreeOpenings = 0

ALTER TABLE dbo.AspNetUsers ALTER COLUMN FreeOpenings INT NOT NULL
GO

ALTER TABLE dbo.ArticleRatingVote ADD RateTypeId INT NULL
ALTER TABLE dbo.ArticleRatingVote ADD ReviewResultId INT NULL
ALTER TABLE dbo.ArticleRatingVote ADD ReviewerId INT NULL
ALTER TABLE dbo.ArticleRatingVote ADD ReviewingDate DATETIME2(7) NULL
ALTER TABLE dbo.ArticleRatingVote DROP COLUMN ConsideredVote
GO

UPDATE dbo.ArticleRatingVote SET RateTypeId = 2 -- Days
ALTER TABLE dbo.ArticleRatingVote ALTER COLUMN RateTypeId INT NOT NULL
GO

ALTER TABLE dbo.ArticleRatingVote ADD CONSTRAINT [FK_dbo.ArticleRatingVote_dbo.AspNetUsers_ReviewerId] FOREIGN KEY(ReviewerId)
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE dbo.ArticleRatingVote ADD CONSTRAINT [CK_dbo.ArticleRatingVote_ReviewResultId_ReviewerId_ReviewingDate] CHECK
(
 (ReviewResultId IS NULL AND ReviewerId IS NULL AND ReviewingDate IS NULL) OR
 (ReviewResultId IS NOT NULL AND ReviewerId IS NOT NULL AND ReviewingDate IS NOT NULL)
)
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
GO
");
        }
    }
}
