namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0003SiteProxyUsernamePassword : DbMigration
    {
        public override void Up()
        {
            Sql(@"
ALTER TABLE dbo.Site ADD ProxyPassword NVARCHAR(256) NULL
ALTER TABLE dbo.Site ADD ProxyUsername NVARCHAR(256) NULL
GO
");
        }
        
        public override void Down()
        {
            Sql(@"
ALTER TABLE dbo.Site DROP COLUMN ProxyPassword
ALTER TABLE dbo.Site DROP COLUMN ProxyUsername
GO
");
        }
    }
}
