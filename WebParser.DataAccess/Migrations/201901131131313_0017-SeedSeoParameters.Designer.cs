// <auto-generated />
namespace WebParser.DataAccess.Migration
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class _0017SeedSeoParameters : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(_0017SeedSeoParameters));
        
        string IMigrationMetadata.Id
        {
            get { return "201901131131313_0017-SeedSeoParameters"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
