namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0014SpbMetroStations : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[MetroStation] 
([Name]
,[CityId]
,[MetroLineId]
,[SearchKeywords])
VALUES
(N'Новокрестовская', 1, 3, N'Новокрестовская'),
(N'Беговая', 1, 3, N'Беговая')
");

        }
        
        public override void Down()
        {
        }
    }
}
