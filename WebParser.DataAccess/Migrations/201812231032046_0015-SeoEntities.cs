namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0015SeoEntities : DbMigration
    {
        public override void Up()
        {
            Sql(@"
CREATE TABLE [dbo].[District] (
    [Id] [int] NOT NULL IDENTITY(1,1),
    [Name] [nvarchar](256),
    [CityId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.District] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[DistrictSeoParameter] (
    [Id] [int] NOT NULL IDENTITY(1,1),
    [Value] [nvarchar](256),
    [Alias] [nvarchar](256),
    [CityId] [int] NOT NULL,
    [DistrictId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.DistrictSeoParameter] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[CitySeoParameter] (
    [Id] [int] NOT NULL IDENTITY(1,1),
    [Value] [nvarchar](256),
    [Alias] [nvarchar](256),
    [CityId] [int] NOT NULL,
    CONSTRAINT [PK_dbo.CitySeoParameter] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[ArticleGroupSeoParameter] (
    [Id] [int] NOT NULL IDENTITY(1,1),
    [Value] [nvarchar](256),
    [Alias] [nvarchar](256),
    [ArticleGroupInternalName] [nvarchar](256),
    CONSTRAINT [PK_dbo.ArticleGroupSeoParameter] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[AliasSeoParameter] (
    [Id] [int] NOT NULL IDENTITY(1,1),
    [Value] [nvarchar](512),
    [Alias] [nvarchar](512),
    [CityId] [int],
    [DistrictId] [int],
    [PriceFrom] [int],
    [PriceTo] [int],
    [ArticleGroupInternalName] [nvarchar](256),
    CONSTRAINT [PK_dbo.AliasSeoParameter] PRIMARY KEY ([Id])
)
CREATE TABLE [dbo].[PhraseSeoParameter] (
    [Id] [int] NOT NULL IDENTITY(1,1),
    [Value] [nvarchar](256),
    [Alias] [nvarchar](256),
    CONSTRAINT [PK_dbo.PhraseSeoParameter] PRIMARY KEY ([Id])
)
ALTER TABLE [dbo].[District] ADD CONSTRAINT [FK_dbo.District_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[DistrictSeoParameter] ADD CONSTRAINT [FK_dbo.DistrictSeoParameter_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[DistrictSeoParameter] ADD CONSTRAINT [FK_dbo.DistrictSeoParameter_dbo.District_DistrictId] FOREIGN KEY ([DistrictId]) REFERENCES [dbo].[District] ([Id])
ALTER TABLE [dbo].[CitySeoParameter] ADD CONSTRAINT [FK_dbo.CitySeoParameter_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[AliasSeoParameter] ADD CONSTRAINT [FK_dbo.AliasSeoParameter_dbo.City_CityId] FOREIGN KEY ([CityId]) REFERENCES [dbo].[City] ([Id])
ALTER TABLE [dbo].[AliasSeoParameter] ADD CONSTRAINT [FK_dbo.AliasSeoParameter_dbo.District_DistrictId] FOREIGN KEY ([DistrictId]) REFERENCES [dbo].[District] ([Id])
");
        }
        
        public override void Down()
        {
            Sql(@"");
        }
    }
}
