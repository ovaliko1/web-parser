namespace WebParser.DataAccess.Migration
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0005RegistrationIp : DbMigration
    {
        public override void Up()
        {
            Sql(
@"ALTER TABLE dbo.AspNetUsers ALTER COLUMN RegistrationIp NVARCHAR(256) NULL
GO"
);
        }
        
        public override void Down()
        {
            Sql(
@"
UPDATE dbo.AspNetUsers SET RegistrationIp = ''
WHERE LEN(RegistrationIp) > 20
GO

ALTER TABLE dbo.AspNetUsers ALTER COLUMN RegistrationIp NVARCHAR(20) NULL
GO"
);
        }
    }
}
