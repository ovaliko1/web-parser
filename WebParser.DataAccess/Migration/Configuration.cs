﻿using System.Configuration;
using System.Data.Entity.Migrations;
using Microsoft.AspNet.Identity;
using WebParser.Common;
using WebParser.Common.Mappers;
using WebParser.DataAccess.Entities;
using WebParser.DataAccess.Identity;
using Constants = WebParser.Common.Constants;
namespace WebParser.DataAccess.Migration
{
    public sealed class Configuration : DbMigrationsConfiguration<DataBaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataBaseContext context)
        {
            AppUserManager userManager = new AppUserManager(new CustomUserStore(context));
            AppRoleManager roleManager = new AppRoleManager(new CustomRoleStore(context));

            var adminEmail = ConfigurationManager.AppSettings["AdminEmail"];
            string userName = adminEmail;
            string password = "password";
            string email = adminEmail;

            foreach (var role in Roles.AllRoles)
            {
                var createdRole = roleManager.FindByName(role.GetRoleName());
                if (createdRole == null)
                {
                    roleManager.Create(new DbRole(role.GetRoleName()));
                }
            }

            DbUser user = userManager.FindByName(userName);
            if (user == null)
            {
                userManager.Create(new DbUser
                {
                    UserName = userName,
                    Email = email,
                    EmailConfirmed = true,
                    MaxSiteCount = Constants.AdminSiteCount,
                },
                password);
                user = userManager.FindByName(userName);
            }

            if (!userManager.IsInRole(user.Id, Role.Admin.GetRoleName()))
            {
                userManager.AddToRole(user.Id, Role.Admin.GetRoleName());
            }
            if (!userManager.IsInRole(user.Id, Role.User.GetRoleName()))
            {
                userManager.AddToRole(user.Id, Role.User.GetRoleName());
            }
        }
    }
}
