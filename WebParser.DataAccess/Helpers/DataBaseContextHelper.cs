﻿using System.Data;
using System.Data.Entity;

namespace WebParser.DataAccess.Helpers
{
    public static class DataBaseContextHelper
    {
        public static DbContextTransaction BeginTransaction(this DataBaseContext context)
        {
            return context.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public static DbContextTransaction BeginTransaction(this DataBaseContext context, IsolationLevel isolationLevel)
        {
            var transaction = context.Database.BeginTransaction(isolationLevel);
            return transaction;
        }
    }
}