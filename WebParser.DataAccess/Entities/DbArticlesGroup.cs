﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("ArticlesGroup")]
    public class DbArticlesGroup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Key]
        public int UserId { get; set; }

        public int SiteId { get; set; }

        [Required]
        public string InternalName { get; set; }

        [Required]
        public string PublicName { get; set; }

        [Required]
        public string LinksToArticlesSearchRule { get; set; }

        [Required]
        public string ArticleSearchRules { get; set; }

        public string Links { get; set; }

        public string LinksFormat { get; set; }

        public int? StartPage { get; set; }

        public int? StepSize { get; set; }

        // TODO: Add check constraint by links format
        public int? LinksFileId { get; set; }

        public long? VkGroupId { get; set; }

        public long? VkTopicId { get; set; }

        public long? VkTopicGroupId { get; set; }

        public int SpiderTypeId { get; set; }

        public int LinksToArticlesSearchRuleSpiderTypeId { get; set; }

        public bool SpiderDoNotLoadImages { get; set; }

        public int BatchSize { get; set; }

        public int BatchDelayInSeconds { get; set; }

        public int DownloadFileDelayInMillseconds { get; set; }

        public int? WaitMilliseconds { get; set; }

        [StringLength(256)]
        public string TwitterConsumerKey { get; set; }

        [StringLength(256)]
        public string TwitterConsumerSecret { get; set; }

        [StringLength(256)]
        public string TwitterAccessToken { get; set; }

        [StringLength(256)]
        public string TwitterAccessTokenSecret { get; set; }

        [ForeignKey("SiteId, UserId")]
        public virtual DbSite Site{ get; set; }

        [ForeignKey("UserId")]
        public virtual DbUser User{ get; set; }

        [ForeignKey("LinksFileId,UserId")]
        public virtual DbFile LinksFile { get; set; }

        [InverseProperty("ArticlesGroup")]
        public virtual ICollection<DbParsedArticle> ParsedArticles { get; set; }

        [InverseProperty("PublisherArticlesGroup")]
        public virtual ICollection<DbUser> Publishers { get; set; }
    }
}