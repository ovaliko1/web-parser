﻿namespace WebParser.DataAccess.Entities
{
    public interface IDbStainedListItem
    {
        int Id { get; set; }

        string Identity { get; set; }

        int? SiteId { get; set; }
    }
}