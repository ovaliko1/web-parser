﻿namespace WebParser.DataAccess.Entities
{
    public interface IDbHaveTargetArticleId
    {
        int? ArticleId { get; set; }

        int? ArticleUserId { get; set; }

        string DeletedArticleIdentity { get; set; }

        string DeletedArticleSummary { get; set; }
    }
}