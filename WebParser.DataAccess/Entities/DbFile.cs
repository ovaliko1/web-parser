﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("File")]
    public class DbFile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Key]
        public int UserId { get; set; }

        public int? SiteId { get; set; }

        [Required]
        public string FileKey { get; set; }

        public string OriginalFileName { get; set; }

        [Required]
        public string FileExtension { get; set; }

        public string ContentType { get; set; }

        public DateTime CreatedDate { get; set; }

        public string FileUrl { get; set; }

        [ForeignKey("SiteId,UserId")]
        public virtual DbSite Site { get; set; }
    }
}
