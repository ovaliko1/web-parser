﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("Site")]
    public class DbSite
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Key]
        [Required]
        public int UserId { get; set; }

        [Required]
        public string Link { get; set; }

        [Required]
        public string Name { get; set; }

        public bool OneTimeExport { get; set; }

        public bool IsEnabled { get; set; }

        public int? SchedulerStartHours { get; set; }

        public int? SchedulerStartMinutes { get; set; }

        public int? SchedulerEndHours { get; set; }

        public int? SchedulerEndMinutes { get; set; }

        public int? RepeatIntervalInMunutes { get; set; }

        public bool NeedStartOneTimeParsing { get; set; }

        public bool IsInParsing { get; set; }

        [StringLength(256)]
        public string WebHookUrl { get; set; }

        [StringLength(256)]
        public string ProxyAddress { get; set; }

        [StringLength(256)]
        public string ProxyUsername { get; set; }

        [StringLength(256)]
        public string ProxyPassword { get; set; }

        public string Cookies { get; set; }

        public int? WatermarkHeight { get; set; }

        public int? WatermarkWidth { get; set; }

        public int? WatermarkFontSize { get; set; }

        [StringLength(256)]
        public string WatermarkRectX { get; set; }

        [StringLength(256)]
        public string WatermarkRectY { get; set; }

        [StringLength(256)]
        public string WatermarkTextX { get; set; }

        [StringLength(256)]
        public string WatermarkTextY { get; set; }

        [StringLength(256)]
        public string UserAgent { get; set; }

        public int JobTypeId { get; set; }

        public int? BlackListIdentitySiteId { get; set; }

        public int? BlackListIdentityUserId { get; set; }

        [StringLength(256)]
        public string GeoBounds { get; set; }

        public bool UseSourceLinkAsContact { get; set; }

        public int CropImageTop { get; set; }

        public int CropImageBottom { get; set; }

        public int CropImageLeft { get; set; }

        public int CropImageRight { get; set; }

        [ForeignKey("BlackListIdentitySiteId,BlackListIdentityUserId")]
        public virtual DbSite BlackListIdentitySite { get; set; }

        [ForeignKey("UserId")]
        public virtual DbUser User { get; set; }

        [InverseProperty("Site")]
        public virtual ICollection<DbParsedArticle> ParsedArticles { get; set; }
    }
}
