﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;
using WebParser.DataAccess.Entities.Identity;

namespace WebParser.DataAccess.Entities
{
    public class DbUser : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {
        [Range(-1, 1000)]
        public int MaxSiteCount { get; set; }

        [StringLength(256)]
        public string NameForViewers { get; set; }

        public int? CityId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime AccessDateTime { get; set; }

        public bool PurchasedAccount { get; set; }

        public int OpeningBalance { get; set; }

        public int FreeHours { get; set; }

        public int FreeOpenings { get; set; }

        public int ClickerCautionCount { get; set; }

        [StringLength(256)]
        public string RegistrationIp { get; set; }

        public bool SeenForbidden { get; set; }

        public long? VkId { get; set; }

        public bool SharingMade { get; set; }

        public bool WaitingForPayment { get; set; }

        public string SerializedPaymentConfirmations { get; set; }

        public int? PublisherArticleGroupId { get; set; }

        public int? PublisherArticleGroupUserId { get; set; }

        public Guid PublicId { get; set; }

        [ForeignKey("PublisherArticleGroupId,PublisherArticleGroupUserId")]
        public DbArticlesGroup PublisherArticlesGroup { get; set; }

        [ForeignKey("CityId")]
        public DbCity City { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<DbArticleRatingVote> Votes { get; set; }
    }
}
