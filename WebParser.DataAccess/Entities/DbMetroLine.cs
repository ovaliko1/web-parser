﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("MetroLine")]
    public class DbMetroLine
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int CityId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Color { get; set; }

        [ForeignKey("CityId")]
        public virtual DbCity City { get; set; }
    }
}
