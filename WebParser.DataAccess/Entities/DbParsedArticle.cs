﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("ParsedArticle")]
    public class DbParsedArticle
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int? SiteId { get; set; }

        [Key]
        public int UserId { get; set; }

        public int? ArticlesGroupId { get; set; }

        public string Link { get; set; }

        public int? ParsingFailedCount { get; set; }

        [StringLength(256)]
        public string ParsingFailedReason { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ParsedContent { get; set; }

        public int GoodRating { get; set; }

        public int BadRating { get; set; }

        public int OutdateRating { get; set; }

        [StringLength(256)]
        public string Metro { get; set; }

        [StringLength(256)]
        public string PriceStr { get; set; }

        public int? Price { get; set; }

        [StringLength(20)]
        public string Identity { get; set; }

        public int IdentityConfidencePercentage { get; set; }

        public bool IsDuplicate { get; set; }

        public Guid? DuplicateGroupId { get; set; }

        public int? BlackListItemId { get; set; }

        public int? WhiteListItemId { get; set; }

        [StringLength(256)]
        public string FormattedAddress { get; set; }

        public decimal? Longitude { get; set; }

        public decimal? Latitude { get; set; }

        [StringLength(256)]
        public string DistanceToMetroText { get; set; }

        public int? DistanceToMetroValue { get; set; }

        [StringLength(256)]
        public string DurationToMetroText { get; set; }

        public int? DurationToMetroValue { get; set; }

        public string FullSizeImages { get; set; }

        public string ImagesPreview { get; set; }

        public bool HasImages { get; set; }

        [ForeignKey("ArticlesGroupId,UserId")]
        public virtual DbArticlesGroup ArticlesGroup { get; set; }

        [ForeignKey("SiteId,UserId")]
        public virtual DbSite Site { get; set; }

        [ForeignKey("BlackListItemId")]
        public virtual DbBlackListItem BlackListItem { get; set; }

        [ForeignKey("WhiteListItemId")]
        public virtual DbWhiteListItem WhiteListItem { get; set; }
    }
}
