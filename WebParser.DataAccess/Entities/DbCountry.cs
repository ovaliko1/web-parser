﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("Country")]
    public class DbCountry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(3)]
        public string Iso { get; set; }

        [Required]
        [MaxLength(3)]
        public string LanguageCode { get; set; }

        [Required]
        [MaxLength(10)]
        public string CountryRegionCode { get; set; }
    }
}
