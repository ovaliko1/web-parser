﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("OpenedContact")]
    public class DbOpenedContact : IDbHaveTargetArticleId
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Key]
        public int UserId { get; set; }

        public int? ArticleId { get; set; }

        public int? ArticleUserId { get; set; }

        [StringLength(20)]
        public string DeletedArticleIdentity { get; set; }

        [StringLength(512)]
        public string DeletedArticleSummary { get; set; }

        public DateTime CreatedDate { get; set; }

        public int RateTypeId { get; set; }

        [ForeignKey("ArticleId,ArticleUserId")]
        public virtual DbParsedArticle ParsedArticle { get; set; }

        [ForeignKey("UserId")]
        public virtual DbUser User { get; set; }
    }
}
