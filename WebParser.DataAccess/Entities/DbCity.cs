﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("City")]
    public class DbCity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string InternalName { get; set; }

        public int CountryId { get; set; }

        public bool FreeAccess { get; set; }

        public bool ShowArticlesFromBlackList { get; set; }

        [ForeignKey("CountryId")]
        public virtual DbCountry Country { get; set; }
    }
}
