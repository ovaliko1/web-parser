﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("ParsingLog")]
    public class DbParsingLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Key]
        public int UserId { get; set; }

        public DateTime Date { get; set; }

        public int? SiteId { get; set; }

        public int Status { get; set; }

        [Required]
        [MaxLength]
        public string Message { get; set; }

        [ForeignKey("SiteId,UserId")]
        public virtual DbSite Site { get; set; }
    }
}