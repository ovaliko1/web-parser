﻿using Microsoft.AspNet.Identity.EntityFramework;
using WebParser.DataAccess.Entities.Identity;

namespace WebParser.DataAccess.Entities
{
    public class DbRole : IdentityRole<int, UserRole>
    {
        public DbRole()
        {
        }

        public DbRole(string roleName)
        {
            Name = roleName;
        }
    }
}
