﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("MetroStation")]
    public class DbMetroStation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int CityId { get; set; }

        public int MetroLineId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public string SearchKeywords { get; set; }

        [ForeignKey("CityId")]
        public virtual DbCity City { get; set; }

        [ForeignKey("MetroLineId")]
        public virtual DbMetroLine MetroLine { get; set; }
    }
}
