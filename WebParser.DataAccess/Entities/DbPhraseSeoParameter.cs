﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebParser.Common;

namespace WebParser.DataAccess.Entities
{
    [Table("dbo.PhraseSeoParameter")]
    public class DbPhraseSeoParameter : ISeoParameter
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(256)]
        public string Value { get; set; }

        [MaxLength(256)]
        public string Alias { get; set; }

        public int CountryId { get; set; }

        [ForeignKey("CountryId")]
        public DbCountry Country { get; set; }

        public bool UpdateRoutingResult(SeoRoutingResult routingResult)
        {
            return false;
        }
    }
}