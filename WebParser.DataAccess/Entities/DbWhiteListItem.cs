﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebParser.DataAccess.Entities
{
    [Table("WhiteListItem")]
    public class DbWhiteListItem : IDbStainedListItem, IDbHaveTargetArticleId
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Identity { get; set; }

        public int IdentityConfidencePercentage { get; set; }

        public int CreatedById { get; set; }

        public int? SiteId { get; set; }

        public int? ArticleId { get; set; }

        public int? ArticleUserId { get; set; }

        [StringLength(20)]
        public string DeletedArticleIdentity { get; set; }

        [StringLength(512)]
        public string DeletedArticleSummary { get; set; }

        public int? ArticleSiteId { get; set; }

        public DateTime CreatedDate { get; set; }

        [ForeignKey("CreatedById")]
        public DbUser CreatedBy { get; set; }

        [ForeignKey("ArticleId,ArticleUserId")]
        public virtual DbParsedArticle SourceParsedArticle { get; set; }

        [InverseProperty("WhiteListItem")]
        public virtual ICollection<DbParsedArticle> ParsedArticles { get; set; }
   }
}
