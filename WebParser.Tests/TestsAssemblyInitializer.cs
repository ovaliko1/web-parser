﻿using NUnit.Framework;
using WebParser.Configuration;

namespace WebParser.Tests
{
    [SetUpFixture]
    public class TestsAssemblyInitializer
    {
        [OneTimeSetUp]
	    public void RunBeforeAnyTests()
	    {
	        ContainerConfig.InitContainer();
	    }

        [OneTimeTearDown]
	    public void RunAfterAnyTests()
	    {
	    }
    }
}
