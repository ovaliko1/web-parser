﻿using Moq;
using WebParser.Configuration;

namespace WebParser.Tests
{
    public abstract class TestBase
    {
        protected static T Resolve<T>()
        {
            return ContainerConfig.Resolve<T>();
        }

        protected static void RegisterMock<T>(Mock<T> mock) where T : class
        {
            ContainerConfig.RegisterInstance<T>(mock.Object);
        }
    }
}
