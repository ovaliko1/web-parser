﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WebParser.Application.Helpers;

namespace WebParser.Tests.CoreTests
{
    [TestFixture]
    public class UrlHelperTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void UseBaseUrlHostForAbsoluteRelativeUrlTest()
        {
            var baseUrl = "http://rutracker.net/forum/";
            var relativeUrl = "http://rutracker.org/forum/tracker.php?f=2093";

            var result = UrlHelper.ConstructFullUri(baseUrl, relativeUrl, true);

            Assert.AreEqual("http://rutracker.net/forum/tracker.php?f=2093", result.AbsoluteUri);
        }

        [Test]
        public void DoNotUseBaseUrlHostForAbsoluteRelativeUrlTest()
        {
            var baseUrl = "http://rutracker.net/forum/";
            var relativeUrl = "http://rutracker.org/forum/tracker.php?f=2093";

            var result = UrlHelper.ConstructFullUri(baseUrl, relativeUrl);

            Assert.AreEqual("http://rutracker.org/forum/tracker.php?f=2093", result.AbsoluteUri);
        }

        [Test]
        public void ConstructAbsoluteUrlTest()
        {
            var baseUrl = "http://rutracker.net/forum/";
            var relativeUrl = "/forum/tracker.php?f=2093";

            var result = UrlHelper.ConstructFullUri(baseUrl, relativeUrl);

            Assert.AreEqual("http://rutracker.net/forum/tracker.php?f=2093", result.AbsoluteUri);
        }

        [Test]
        public void ConstructAbsoluteUrlWithSlashTest()
        {
            var baseUrl = "http://rutracker.net/forum/";
            var relativeUrl = "forum/tracker.php?f=2093";

            var result = UrlHelper.ConstructFullUri(baseUrl, relativeUrl);

            Assert.AreEqual("http://rutracker.net/forum/tracker.php?f=2093", result.AbsoluteUri);
        }

        [Test]
        public void ConstructAbsoluteUrlWithoutSlashTest()
        {
            var baseUrl = "http://rutracker.net/forum";
            var relativeUrl = "forum/tracker.php?f=2093";

            var result = UrlHelper.ConstructFullUri(baseUrl, relativeUrl);

            Assert.AreEqual("http://rutracker.net/forum/tracker.php?f=2093", result.AbsoluteUri);
        }

        [Test]
        public void ConstructAbsoluteUrlWithStrangePathTest()
        {
            var baseUrl = "http://rutracker.net/forum";
            var relativeUrl = "/tracker.php?f=2093";

            var result = UrlHelper.ConstructFullUri(baseUrl, relativeUrl);

            Assert.AreEqual("http://rutracker.net/forum/tracker.php?f=2093", result.AbsoluteUri);
        }
    }
}
