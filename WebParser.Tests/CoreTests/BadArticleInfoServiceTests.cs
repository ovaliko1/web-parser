﻿using System;
using Moq;
using NUnit.Framework;
using WebParser.Application.Interfaces.Services.Parsing.ParsedContent;
using WebParser.Application.Services.BlackList;
using WebParser.Application.Services.Parsing.BadArticleInfo;
using WebParser.Application.Services.Repository;
using WebParser.Configuration;
using WebParser.DataAccess;
using WebParser.DataAccess.Entities;

namespace WebParser.Tests.CoreTests
{
    [TestFixture]
    public class BadArticleInfoServiceTests : TestBase
    {
        private IBadArticleInfoService BadArticleInfoService { get; set; }

        private Mock<IRepositoryService> RepositoryServiceMock { get; set; }

        private Mock<IBlackListService> BlackListServiceMock { get; set; }

        private Mock<DataBaseContext> DataBaseContextMock { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            RepositoryServiceMock = new Mock<IRepositoryService>();
            BlackListServiceMock = new Mock<IBlackListService>();

            DataBaseContextMock = new Mock<DataBaseContext>();
            DataBaseContextMock.Setup(x => x.SaveChanges());

            RegisterMock(RepositoryServiceMock);
            RegisterMock(BlackListServiceMock);

            BadArticleInfoService = Resolve<IBadArticleInfoService>();
        }

        [Test]
        public void GetBadArticleInfoEmptyIdentityTest()
        {
            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity(), string.Empty);
            Assert.IsNull(result.BlackListItemId);
            Assert.IsNull(result.DuplicateGroupId);
        }


        [Test]
        public void GetBadArticleInfoEmptyIdentityTestLessThanSevenIdentityLength()
        {
            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity("123456", 78), string.Empty);
            Assert.IsNull(result.BlackListItemId);
            Assert.IsNull(result.DuplicateGroupId);
        }

        [Test]
        public void GetBadArticleInfoNotInBlackListWithoutDuplicatesTest()
        {
            BlackListServiceMock
                .Setup(x => x.GetBlackListItemId(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>()))
                .Returns<int?>(null);

            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicate(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns<DbParsedArticle>(null);

            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity("1111111", 100), string.Empty);

            Assert.IsNull(result.BlackListItemId);
            Assert.IsNull(result.DuplicateGroupId);
        }

        [Test]
        public void GetBadArticleInfoInBlackListWithoutDuplicatesTest()
        {
            BlackListServiceMock
                .Setup(x => x.GetBlackListItemId(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>()))
                .Returns(1);

            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicate(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns<DbParsedArticle>(null);

            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity("1111111", 100), string.Empty);

            Assert.AreEqual(1, result.BlackListItemId);
            Assert.IsNull(result.DuplicateGroupId);
        }

        [Test]
        public void GetBadArticleInfoInBlackListWithDuplicatesTest()
        {
            BlackListServiceMock
                .Setup(x => x.GetBlackListItemId(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>()))
                .Returns(1);

            var duplicate = new DbParsedArticle();
            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicate(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(duplicate);

            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity("1111111", 100), string.Empty);

            Assert.AreEqual(1, result.BlackListItemId);
            Assert.AreEqual(1, duplicate.BlackListItemId);
            Assert.IsNotNull(result.DuplicateGroupId);
            Assert.IsNotNull(duplicate.DuplicateGroupId);
            Assert.AreEqual(duplicate.DuplicateGroupId, result.DuplicateGroupId);
            Assert.True(duplicate.IsDuplicate);
        }

        [Test]
        public void GetBadArticleInfoNotInBlackListWithOneDuplicateTest()
        {
            BlackListServiceMock
                .Setup(x => x.GetBlackListItemId(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>()))
                .Returns<int?>(null);

            var duplicate = new DbParsedArticle
            {
                FormattedAddress = "Address1",
            };

            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicate(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(duplicate);
            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicates(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(new[] { duplicate });

            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity("1111111", 100), string.Empty);

            Assert.IsNull(result.BlackListItemId);
            Assert.NotNull(result.DuplicateGroupId);
            Assert.AreEqual(result.DuplicateGroupId, duplicate.DuplicateGroupId);
        }

        [Test]
        public void GetBadArticleInfoNotInBlackListWithSeveralDuplicatesByOneAddressTest()
        {
            BlackListServiceMock
                .Setup(x => x.GetBlackListItemId(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>()))
                .Returns<int?>(null);

            var duplicate1 = new DbParsedArticle
            {
                FormattedAddress = "Address1",
            };
            var duplicate2 = new DbParsedArticle
            {
                FormattedAddress = "Address1",
            };
            var duplicate3 = new DbParsedArticle
            {
                FormattedAddress = "Address1",
            };

            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicate(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(duplicate1);
            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicates(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(new[] { duplicate1, duplicate2, duplicate3 });

            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity("1111111", 100), "Address1");

            Assert.IsNull(result.BlackListItemId);
            Assert.NotNull(result.DuplicateGroupId);

            Assert.AreEqual(result.DuplicateGroupId, duplicate1.DuplicateGroupId);
            Assert.AreEqual(result.DuplicateGroupId, duplicate2.DuplicateGroupId);
            Assert.AreEqual(result.DuplicateGroupId, duplicate3.DuplicateGroupId);

            Assert.IsTrue(duplicate1.IsDuplicate);
            Assert.IsTrue(duplicate2.IsDuplicate);
            Assert.IsTrue(duplicate3.IsDuplicate);

            Assert.IsNull(duplicate1.BlackListItemId);
            Assert.IsNull(duplicate2.BlackListItemId);
            Assert.IsNull(duplicate3.BlackListItemId);
        }

        [Test]
        public void GetBadArticleInfoNotInBlackListWithSeveralDuplicatesByTwoAddressTest()
        {
            BlackListServiceMock
                .Setup(x => x.GetBlackListItemId(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>()))
                .Returns<int?>(null);

            var duplicate1 = new DbParsedArticle
            {
                FormattedAddress = "Address1",
            };
            var duplicate2 = new DbParsedArticle
            {
                FormattedAddress = "Address1",
            };
            var duplicate3 = new DbParsedArticle
            {
                FormattedAddress = "Address2",
            };

            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicate(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(duplicate1);
            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicates(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(new[] { duplicate1, duplicate2, duplicate3 });

            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity("1111111", 100), "Address1");

            Assert.IsNull(result.BlackListItemId);
            Assert.NotNull(result.DuplicateGroupId);

            Assert.AreEqual(result.DuplicateGroupId, duplicate1.DuplicateGroupId);
            Assert.AreEqual(result.DuplicateGroupId, duplicate2.DuplicateGroupId);
            Assert.AreEqual(result.DuplicateGroupId, duplicate3.DuplicateGroupId);

            Assert.IsTrue(duplicate1.IsDuplicate);
            Assert.IsTrue(duplicate2.IsDuplicate);
            Assert.IsTrue(duplicate3.IsDuplicate);

            Assert.IsNull(duplicate1.BlackListItemId);
            Assert.IsNull(duplicate2.BlackListItemId);
            Assert.IsNull(duplicate3.BlackListItemId);
        }

        [Test]
        public void GetBadArticleInfoNotInBlackListWithSeveralDuplicatesByTwoAddressAndThirdNewAddressTest()
        {
            BlackListServiceMock
                .Setup(x => x.GetBlackListItemId(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>()))
                .Returns<int?>(null);
            BlackListServiceMock
                .Setup(x => x.AddToBlackList(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(1);

            var duplicate1 = new DbParsedArticle
            {
                FormattedAddress = "Address1",
            };
            var duplicate2 = new DbParsedArticle
            {
                FormattedAddress = "Address1",
            };
            var duplicate3 = new DbParsedArticle
            {
                FormattedAddress = "Address2",
            };

            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicate(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(duplicate1);
            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicates(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(new[] { duplicate1, duplicate2, duplicate3 });

            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity("1111111", 100), "Address3");

            Assert.AreEqual(1, result.BlackListItemId);
            Assert.NotNull(result.DuplicateGroupId);

            Assert.AreEqual(result.DuplicateGroupId, duplicate1.DuplicateGroupId);
            Assert.AreEqual(result.DuplicateGroupId, duplicate2.DuplicateGroupId);
            Assert.AreEqual(result.DuplicateGroupId, duplicate3.DuplicateGroupId);

            Assert.AreEqual(1, duplicate1.BlackListItemId);
            Assert.AreEqual(1, duplicate2.BlackListItemId);
            Assert.AreEqual(1, duplicate3.BlackListItemId);

            Assert.IsTrue(duplicate1.IsDuplicate);
            Assert.IsTrue(duplicate2.IsDuplicate);
            Assert.IsTrue(duplicate3.IsDuplicate);
        }

        [Test]
        public void GetBadArticleInfoNotInBlackListWithSeveralDuplicatesByThreeAddressTest()
        {
            BlackListServiceMock
                .Setup(x => x.GetBlackListItemId(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>()))
                .Returns<int?>(null);
            BlackListServiceMock
                .Setup(x => x.AddToBlackList(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(1);

            var duplicate1 = new DbParsedArticle
            {
                FormattedAddress = "Address1",
            };
            var duplicate2 = new DbParsedArticle
            {
                FormattedAddress = "Address2",
            };
            var duplicate3 = new DbParsedArticle
            {
                FormattedAddress = "Address3",
            };

            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicate(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(duplicate1);
            RepositoryServiceMock
                .Setup(x => x.GetPreviousDuplicates(It.IsAny<DataBaseContext>(), It.IsAny<ArticleIdentity>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                .Returns(new[] { duplicate1, duplicate2, duplicate3 });

            var result = BadArticleInfoService.GetBadArticleInfo(DataBaseContextMock.Object, 1, 1, new ArticleIdentity("1111111", 100), "Address1");

            Assert.AreEqual(1, result.BlackListItemId);
            Assert.NotNull(result.DuplicateGroupId);

            Assert.AreEqual(result.DuplicateGroupId, duplicate1.DuplicateGroupId);
            Assert.AreEqual(result.DuplicateGroupId, duplicate2.DuplicateGroupId);
            Assert.AreEqual(result.DuplicateGroupId, duplicate3.DuplicateGroupId);

            Assert.AreEqual(1, duplicate1.BlackListItemId);
            Assert.AreEqual(1, duplicate2.BlackListItemId);
            Assert.AreEqual(1, duplicate3.BlackListItemId);

            Assert.IsTrue(duplicate1.IsDuplicate);
            Assert.IsTrue(duplicate2.IsDuplicate);
            Assert.IsTrue(duplicate3.IsDuplicate);
        }
    }
}
