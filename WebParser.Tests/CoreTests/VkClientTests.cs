﻿using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Common;
using WebParser.Configuration;
using WebParser.Integration.Vk;
using WebParser.Integration.Vk.Entities;

namespace WebParser.Tests.CoreTests
{
    [TestFixture(Ignore = "Ignore")]
    public class VkClientTests : TestBase
    {
        private IVkClient VkClient { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            var errorsHandlerMoq = new Mock<IParsingLogger>();
            errorsHandlerMoq.Setup(x => x.Log(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<ParsingLogStatus>(), It.IsAny<string>())).Verifiable();

            VkClient = Resolve<IVkClient>();
        }

        [Test]
        public async Task ExportTest()
        {
            var notPostedField = ParsedValue.CreateLinkToFile(false, "https://www.google.ru/img1", "https://www.google.ru/img2");
            notPostedField.HideOnVkPost = true;

            var articles = new []
            {
                new VkArticleToExport
                {
                    ArticlesGroupInfo = new VkArticlesGroupInfo
                    {
                        VkGroupId = 64786312,
                        VkTopicGroupId = 64786312,
                        VkTopicId = 29874224,
                        ArticleGroupName = "1k",
                        UserId = 113123,
                    },
                    ApartmentDetails = new VkApartmentDetails
                    {
                        Id = 468889,
                        ObjectName = "Объект",
                        PriceStr = "10000 p",
                        Address = "SPb, nevskiy prospekt 12",
                        DurationToMetroText = "24 мин",
                        DistanceToMetroText = "2,0 км",
                        Metro = "Pionerskaya",
                        Characteristics = "Characteristics",
                        Description = "Description",
                        Images = new []
                        {
                            "http://spb.snyat-kvartiru-bez-posrednikov.ru/images/objects/thumbs/object4466844-image12.800x600.nocrop.w2.fa1e0980.jpg",
                            "https://55.img.avito.st/1280x960/4419170255.jpg",
                            "https://39.img.avito.st/1280x960/4419170439.jpg",
                            "https://07.img.avito.st/1280x960/4419170607.jpg",
                            "https://35.img.avito.st/1280x960/4419170835.jpg",
                            "https://35.img.avito.st/1280x960/4419170835.jpg",
                        }
                    }
                }
            };

            await VkClient.Export(articles, null);
        }
    }
}
