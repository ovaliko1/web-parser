﻿using System.Threading.Tasks;
using NUnit.Framework;
using WebParser.Configuration;
using WebParser.Integration.Twitter;
using WebParser.Integration.Twitter.Entities;

namespace WebParser.Tests.CoreTests
{
    [TestFixture(Ignore = "Ignore")]
    public class TwitterClientTests : TestBase
    {
        private ITwitterClient Client { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            Client = Resolve<ITwitterClient>();
        }

        [Test]
        public async Task ExportTest()
        {
            var articles = new[]
            {
                new TwitterArticleToExport
                {
                    ArticlesGroupInfo = new TwitterArticlesGroupInfo
                    {
                        UserId = 113123,
                        ArticleGroupName = "1k",
                        TwitterConsumerKey = "R4soqxdbdmMBJaR9YMytw7YZI",
                        TwitterConsumerSecret = "JQHnY9LQKRos0WI1fhEOKt67ZQyD8Yz3w05ScCLRPARVOLjZG4",
                        TwitterAccessToken = "830746558262046721-Ph2P4SvS1z6M1R0IfrXWVy07oxK8SpX",
                        TwitterAccessTokenSecret = "6schc9bI89bD2OG5nXxwZLlmet5YkW5ueD3asAGJQx8in",
                    },
                    ApartmentDetails = new TwitterApartmentDetails
                    {
                        Id = 1,
                        ObjectName = "Объект",
                        PriceStr = "10000 p",
                        Address = "Спб, nevskiy prospekt 12",
                        DurationToMetroText = "5 мин",
                        DistanceToMetroText = "0.4 км",
                        Metro = "Пионерская",
                        Description = "Описание ывафывафываыавва ывафывафываыавва ывафывафываыавваывафывафываыавваывафывафываыавваывафывафываыавваывафывафываыавваывафывафываыавваывафывафываыавва",
                        Longitude = 30.4899m,
                        Latitude = 59.9121m,
                    }
                }
            };

            await Client.Export(articles, null);
        }
    }
}
