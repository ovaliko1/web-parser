﻿using Newtonsoft.Json;

using NUnit.Framework;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Mappers;
using WebParser.Application.Services.SiteStructure.JsonEntities;
using WebParser.Common;

namespace WebParser.Tests.CoreTests
{
    [TestFixture]
    public class SearchRulesHelperTest : TestBase
    {
        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void ParseTest()
        {
            var data = new SearchRules
            {
                Rules = new[]
                {
                    new SearchRule
                    {
                        FieldName = "name1",
                        SearchType = SearchType.ManyNodes,
                        Selectors = "body div.class1",
                    },
                    new SearchRule
                    {
                        FieldName = "name2",
                        SearchType = SearchType.ManyNodes,
                        Selectors = "body div.class2",
                    },
                }
            };

            var str = JsonConvert.SerializeObject(data);
            var result = str.Deserialize<JsonSearchRules>().ToSearchRules();

            for (int i = 0; i < result.Length; i++)
            {
                var searchRule = result[i];

                Assert.AreEqual(searchRule.FieldName, data.Rules[i].FieldName);
                Assert.AreEqual(searchRule.SearchType, data.Rules[i].SearchType);

                for (int j = 0; j < searchRule.Selectors.Length; j++)
                {
                    var selector = searchRule.Selectors[j];
                    Assert.AreEqual(selector, data.Rules[i].Selectors[j]);
                }
            }
        }
    }
}
