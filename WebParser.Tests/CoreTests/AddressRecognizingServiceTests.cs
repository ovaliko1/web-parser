﻿using System.Threading.Tasks;
using NUnit.Framework;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.Parsing.AddressRecognizing;
using WebParser.Common.Extensions;
using WebParser.Configuration;

namespace WebParser.Tests.CoreTests
{
    [TestFixture(Ignore = "Ignore")]
    public class AddressRecognizingServiceTests : TestBase
    {
        private IAddressRecognizingService AddressRecognizingService { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            AddressRecognizingService = Resolve<IAddressRecognizingService>();
        }

        [Test]
        public async Task RecognizeAddressSaintPAddressTest()
        {
            // geoBounds = "59.833359,30.037643|60.184320,30.607863"
            // Saint-Petersburg
            var site = new Site
            {
                LanguageCode = "ru",
                RegionCode = "RU",
                GeoBounds = "59.833359,30.037643|60.184320,30.607863",
            };
            var result = await AddressRecognizingService.RecognizeAddress("aaa", site);
            Assert.IsNotNull(result);
            Assert.IsNull(result.NearestMetroStation);

            result = await AddressRecognizingService.RecognizeAddress("Санкт-Петербург, Кондратьевский проспект, 68к4", site);
            Assert.IsNotNull(result);
            result = await AddressRecognizingService.RecognizeAddress("Санкт-Петербург, Кондратьевский проспект, 68к4", site);
            Assert.IsNotNull(result);
            result = await AddressRecognizingService.RecognizeAddress("Санкт-Петербург, Кондратьевский проспект, 68к4", site);
            Assert.IsNotNull(result);
            result = await AddressRecognizingService.RecognizeAddress("Санкт-Петербург, Кондратьевский проспект, 68к4", site);
            Assert.IsNotNull(result);
            result = await AddressRecognizingService.RecognizeAddress("Санкт-Петербург, Кондратьевский проспект, 68к4", site);
            Assert.IsNotNull(result);
            result = await AddressRecognizingService.RecognizeAddress("Санкт-Петербург, Кондратьевский проспект, 68к4", site);
            Assert.IsNotNull(result);
            result = await AddressRecognizingService.RecognizeAddress("Санкт-Петербург, Кондратьевский проспект, 68к4", site);
            Assert.IsNotNull(result);

            //Assert.IsNotNull(result.NearestMetroStation);

            result = await AddressRecognizingService.RecognizeAddress("Культуры пр-кт, 22, корп. 2", site);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.NearestMetroStation);
        }

        [Test]
        public async Task RecognizeAddressSofiaAddressTest()
        {
            // geoBounds = "42.602046,23.170534|42.806524,23.493755"
            // Sofia
            var site = new Site
            {
                LanguageCode = "bg",
                RegionCode = "BG",
                GeoBounds = "42.602046,23.170534|42.806524,23.493755",
            };
            var result = await AddressRecognizingService.RecognizeAddress("град София, Център", site);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.NearestMetroStation);

            result = await AddressRecognizingService.RecognizeAddressByLocation("42.69304480142,23.31808766308", result, site);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.NearestMetroStation);
        }
    }
}
