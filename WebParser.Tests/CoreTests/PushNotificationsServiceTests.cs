﻿using System.Threading.Tasks;
using NUnit.Framework;
using WebParser.Application.Services.PushNotifications;
using WebParser.Configuration;

namespace WebParser.Tests.CoreTests
{
    [TestFixture(Ignore = "Ignore")]
    public class PushNotificationsServiceTests : TestBase
    {
        private IPushNotificationsService PushNotificationsService { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            PushNotificationsService = Resolve<IPushNotificationsService>();
        }

        [Test]
        public async Task SendPingNotificationTest()
        {
            var result = await PushNotificationsService.SendPingNotification();
            Assert.IsTrue(result);
        }
    }
}
