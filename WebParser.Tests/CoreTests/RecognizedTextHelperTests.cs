﻿using NUnit.Framework;
using WebParser.Application.Services.Parsing.ImageRecognizing;
using WebParser.Configuration;

namespace WebParser.Tests.CoreTests
{
    [TestFixture]
    public class RecognizedTextHelperTests : TestBase
    {
        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();
        }

        [Test]
        public void ExtractPhoneNumberTest()
        {
            Assert.AreEqual("89217790456", "8 921 779-04-56\n\n".ExtractPhoneNumber());
            Assert.AreEqual("89817574849", "8 981 757-48-A9\n\n".ExtractPhoneNumber());
            Assert.AreEqual("89111042625", "8 911 1t14-26-25\n\n".ExtractPhoneNumber());
            Assert.AreEqual("89062288883", "+7 906 228-88-83".ExtractPhoneNumber());
            Assert.AreEqual("8950414606", "8 950 MI-Mi-Oi\n\n".ExtractPhoneNumber());
            Assert.AreEqual("89112366777", "8 911 236 6T TT\n\n".ExtractPhoneNumber());
            Assert.AreEqual("89219785189", "8 921 97fV51-89\n\n".ExtractPhoneNumber());
        }

        [Test]
        public void ExtractPriceTest()
        {
            Assert.AreEqual(null, "asddasdasd\n\n".ExtractPrice());
            Assert.AreEqual(10000, "10&thinsp;000\n\n".ExtractPrice());
            Assert.AreEqual(10000, "10 тыс. р".ExtractPrice());
            Assert.AreEqual(20000, "20 Тысяч рублей".ExtractPrice());
            Assert.AreEqual(21000, "21 000 РУБ.".ExtractPrice());
            Assert.AreEqual(12000, "12 тр".ExtractPrice());
        }
    }
}
