﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.Parsing;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Application.Services.SiteStructure;
using WebParser.Application.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.WebClient;
using WebParser.Common;
using WebParser.Configuration;
using WebParser.Tests.Helpers;

namespace WebParser.Tests.CoreTests
{
    [TestFixture(Ignore = "Ignore")]
    public class WebCommandTests : TestBase
    {
        private static IWebCommand WebCommand { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            var errorsHandlerMoq = new Mock<IParsingLogger>();
            errorsHandlerMoq.Setup(x => x.Log(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<ParsingLogStatus>(), It.IsAny<string>())).Verifiable(); 
            RegisterMock(errorsHandlerMoq);

            WebCommand = Resolve<IWebCommand>();
        }

        [Test]
        public async Task UploadArticleToWebHookTest()
        {
            var site = new Site
            {
                Link = "https://www.ru",
                WebHookUrl = "http://185.125.218.102/webhooks/receive.json",
            };
            var articlesGroup = new ArticlesGroup
            {
                InternalName = "SubCategoryName",
            };

            var settings = ParseArticleSettings.UploadToThirdParty(site, articlesGroup);

            var parsedArticle = new ParsedArticleResult(new []
            {
                ParsedValue.Create(null, "Header").WithName("Header"),
                ParsedValue.Create(null, "magnet:?xt=urn:btih:6FAA831469681F3ABE0692C16903906036E71CB9&amp;tr=http%3A%2F%2Fbt4.rutracker.cc%2Fann%3Fmagnet").WithName("MagnetLink"),
            });

            await WebCommand.UploadArticleToWebHook(-1, -1, new []{ parsedArticle }, settings);
        }

        [Test]
        public async Task DownloadFileBase64Test()
        {
            var result = await WebCommand.DownloadFile(RandomHelper.Id(100), RandomHelper.Id(100), "http://www.ru", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAAAQCAYAAADkpAq+AAAACXBIWXMAAA7EAAAOxAGVKw4bAAADSklEQVRYhe3Yv2ueVRQH8M8J4SWE8BJqDBF1EH8iHTqK/4GbQ3EMHVwU0arVSZEMjg4OxaGUUmrRgksHEelQigg6CSqolSgiUuNLrTGGWgpeh+e+ePPy/EoW3+E9cMl9n+99zvmec+459z6JlJKZTJ/M/d8EZlIvs8RMqcwSM62SUpLPmVW8h+sY4RwOjfFyYBGn8tpreBuLGVvCRezgRta5Urz7FDaxi09xuM5GXruAk9nGCGcxLPAN/JJtXcBqH6zGThfn3rHp42MfbmXFnMXHuAt34wrONOTzrWz4XjycyZ7O2PO4ijuzrit4ASLiMbyCJ3AHzufRJG/ie9yHB/GHKlEi4ji2cX/m+8NYVxvWII2cs/SOTZePvbkVWdzFYGK37jTsiC17d+4SruX5J4odgmVcyvPzONG002rs/CxXYv49xI08/87eChlitwtrsNPI+QCxafWxL7fyhcs4hgHmsY7LDcpHEwFbwc0iaUsFNl8kbRNH+iamxu4hbNY8X1NV8bn9YBMbrZbzAWLT28dW3sWih1QllvLYwQMNCs/gRN45K6rz5nbGbtWsv5X/3sTRvGu2Vb162MeJ/P7T2Jh4Nj4brk8GpA2r49f0bJ+x6eVjF7fJha/mYC/iNVxoMD7MBrdVZ8BRubTHCWpIzG1Vv17Ldl7C6Z5JWc0c52uwOdUF5Iv9YMWaRs4HiE1vH1t5F4t27C3nIbZ7Bu2w3GJUbaHsxwNs5flIcZvJTo566B+obi9rLWuGcjttw4pdn5C6OLfFpkHXvnxs4l3eyv5MKf1V/P4Hf+snj+KzPP9W1d7GspyfwZcppd8LbF7Ht1RErOAdvJ5S+rV4/lNE3DOh67cuLKUU5ejBmYbYNOhq9bGNWyllUD6IiOciYjkiFvAs3p98ISv/KiLWI2IQEY/gGVVJwiWsR8RCRAxUd/qPMnYqIo5FxGJELKmuqV/X2ch2Hle1kTdSSlcn4HfxYkQsRcQiXladdV1YnbRx3ldsevjYj1tRUuOPua08TmKhofyO4HPVQfcNniywtezotqoFXLS3DRxXfTDu4EMNh2he+6OJduG/ljFQbYaR6lq9gbkurMFOF+fesenysS+3yItnMmUy+1/ZlMq/96CZyNqTI5MAAAAASUVORK5CYII=", new Cookie[0]);

            Assert.IsNotNull(result);
            Assert.AreEqual("image/png", result.ContentType);
            Assert.IsFalse(string.IsNullOrEmpty(result.FileName));
            Assert.IsNotNull(result.FileData);
            Assert.AreEqual(920, result.FileData.Length);
        }

        [Test]
        public async Task DownloadFileNotValidBase64Test()
        {
            var result = await WebCommand.DownloadFile(RandomHelper.Id(100), RandomHelper.Id(100), "https://m.avito.ru/", "https://12.img.avito.st/1280x960/4277138412.jpg", new Cookie[0]);
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task DownloadImageWithSslTlsIssue()
        {
            var result = await WebCommand.DownloadFile(RandomHelper.Id(100), RandomHelper.Id(100), "http://www.ru", "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAAAQCAYAAADkpAq+AAAACXBIWXMAAA7EAAAOxAGVKw4bAAADSklEQVRYhe3Yv2ueVRQH8M8J4SWE8BJqDBF1EH8iHTqK/4GbQ3EMHVwU0arVSZEMjg4OxaGUUmrRgksHEelQigg6CSqolSgiUuNLrTGGWgpeh+e+ePPy/EoW3+E9cMl9n+99zvmec+459z6JlJKZTJ/M/d8EZlIvs8RMqcwSM62SUpLPmVW8h+sY4RwOjfFyYBGn8tpreBuLGVvCRezgRta5Urz7FDaxi09xuM5GXruAk9nGCGcxLPAN/JJtXcBqH6zGThfn3rHp42MfbmXFnMXHuAt34wrONOTzrWz4XjycyZ7O2PO4ijuzrit4ASLiMbyCJ3AHzufRJG/ie9yHB/GHKlEi4ji2cX/m+8NYVxvWII2cs/SOTZePvbkVWdzFYGK37jTsiC17d+4SruX5J4odgmVcyvPzONG002rs/CxXYv49xI08/87eChlitwtrsNPI+QCxafWxL7fyhcs4hgHmsY7LDcpHEwFbwc0iaUsFNl8kbRNH+iamxu4hbNY8X1NV8bn9YBMbrZbzAWLT28dW3sWih1QllvLYwQMNCs/gRN45K6rz5nbGbtWsv5X/3sTRvGu2Vb162MeJ/P7T2Jh4Nj4brk8GpA2r49f0bJ+x6eVjF7fJha/mYC/iNVxoMD7MBrdVZ8BRubTHCWpIzG1Vv17Ldl7C6Z5JWc0c52uwOdUF5Iv9YMWaRs4HiE1vH1t5F4t27C3nIbZ7Bu2w3GJUbaHsxwNs5flIcZvJTo566B+obi9rLWuGcjttw4pdn5C6OLfFpkHXvnxs4l3eyv5MKf1V/P4Hf+snj+KzPP9W1d7GspyfwZcppd8LbF7Ht1RErOAdvJ5S+rV4/lNE3DOh67cuLKUU5ejBmYbYNOhq9bGNWyllUD6IiOciYjkiFvAs3p98ISv/KiLWI2IQEY/gGVVJwiWsR8RCRAxUd/qPMnYqIo5FxGJELKmuqV/X2ch2Hle1kTdSSlcn4HfxYkQsRcQiXladdV1YnbRx3ldsevjYj1tRUuOPua08TmKhofyO4HPVQfcNniywtezotqoFXLS3DRxXfTDu4EMNh2he+6OJduG/ljFQbYaR6lq9gbkurMFOF+fesenysS+3yItnMmUy+1/ZlMq/96CZyNqTI5MAAAAASUVORK5CYII=", new Cookie[0]);

            Assert.IsNotNull(result);
            Assert.AreEqual("image/png", result.ContentType);
            Assert.IsFalse(string.IsNullOrEmpty(result.FileName));
            Assert.IsNotNull(result.FileData);
            Assert.AreEqual(920, result.FileData.Length);

        }
    }
}
