﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using WebParser.Application.Helpers;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.Parsing.SiteContent;
using WebParser.Application.Services.Parsing.SiteContent.Entities;
using WebParser.Application.Services.Parsing.Spider;
using WebParser.Common;
using WebParser.Configuration;

namespace WebParser.Tests.CoreTests
{
    [TestFixture(Ignore = "Ignore")]
    public class SpiderTest : TestBase
    {
        private ISiteContentService SiteContentService { get; set; }

        private ISpider Spider { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            SiteContentService = Resolve<ISiteContentService>();
            Spider = Resolve<ISpider>();
        }

        [Test]
        public void FetchSiteContentQpruTest()
        {
            var data = new[]
            {
                new SearchRule
                {
                    FieldName = "Title",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".center-side h1",
                    FilterFunction = "function(){ return true;}",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
                new SearchRule
                {
                    FieldName = "Image",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".viewport .image",
                    FilterFunction = "function(){ return true;}",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.AttributeContent,
                        AttrName = "src",
                    }
                },
                new SearchRule
                {            
                    ActionType = ActionType.Click,
                    Selectors = ".js-phone-show__link",
                },
                new SearchRule
                {
                    ActionType = ActionType.Wait,
                    WaitMilseconds = 300,
                },
                new SearchRule
                {
                    FieldName = "Controls",
                    SearchType = SearchType.ManyNodes,
                    Selectors = ".control-group .control-label",
                    FilterFunction = "function(){ return true;}",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
                new SearchRule
                {
                    ActionType = ActionType.Wait,
                    WaitMilseconds = 400,
                },
                new SearchRule
                {
                    FieldName = "RelatedAdvs",
                    SearchType = SearchType.ManyNodes,
                    Selectors = "#similar-advs a",
                    FilterFunction = "function(){ return true;}",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.AttributeContent,
                        AttrName = "href",
                    }
                },
            };
               
            var links = new[]
            {
                new Uri("http://realty.kaliningrad.qp.ru/novostroiki/prodau_trehkomnatnuu_kvartiru_4043235"),
                new Uri("http://realty.kaliningrad.qp.ru/novostroiki/prodau_dvuhkomnatnuu_kvartiru_4043229"),
                new Uri("http://realty.kaliningrad.qp.ru/novostroiki/prodau_odnokomnatnuu_kvartiru_4043200"),
            };

            var fetchConfiguration = new FetchConfiguration
            {
                Site = new Site
                {
                    Link = "http://kudago.com",
                    Cookies = new []
                    {
                        new Cookie
                        {
                            Name = "cookie1",
                            Value = "cookie1Value",
                        },
                        new Cookie
                        {
                            Name = "cookie2",
                            Value = "cookie2Value",
                        },
                    }
                },
                Links = links,
                Rules = data,
                SpiderType = SpiderType.SlimerJs
            };

            var result = SiteContentService.FetchArticlesGroupContent(fetchConfiguration);

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Result.Length);

            Assert.AreEqual(4, result.Result[0].ParsedValues.Length);
            Assert.AreEqual(4, result.Result[1].ParsedValues.Length);
            Assert.AreEqual(4, result.Result[2].ParsedValues.Length);
        }

        [Test]
        public void FetchSiteContentQpruPuppeterTest()
        {
            var data = new[]
            {
                new SearchRule
                {
                    FieldName = "Title",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".center-side h1",
                    FilterFunction = "function(){ return true;}",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
                new SearchRule
                {
                    FieldName = "Image",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".viewport .image",
                    FilterFunction = "function(){ return true;}",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.AttributeContent,
                        AttrName = "src",
                    }
                },
                new SearchRule
                {
                    ActionType = ActionType.Click,
                    Selectors = ".js-phone-show__link",
                },
                new SearchRule
                {
                    ActionType = ActionType.Wait,
                    WaitMilseconds = 300,
                },
                new SearchRule
                {
                    FieldName = "Controls",
                    SearchType = SearchType.ManyNodes,
                    Selectors = ".control-group .control-label",
                    FilterFunction = "function(){ return true;}",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
                new SearchRule
                {
                    ActionType = ActionType.Wait,
                    WaitMilseconds = 400,
                },
                new SearchRule
                {
                    FieldName = "RelatedAdvs",
                    SearchType = SearchType.ManyNodes,
                    Selectors = "#similar-advs a",
                    FilterFunction = "function(){ return true;}",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.AttributeContent,
                        AttrName = "href",
                    }
                },
            };

            var links = new[]
            {
                new Uri("http://realty.kaliningrad.qp.ru/novostroiki/prodau_trehkomnatnuu_kvartiru_4043235"),
                new Uri("http://realty.kaliningrad.qp.ru/novostroiki/prodau_dvuhkomnatnuu_kvartiru_4043229"),
                new Uri("http://realty.kaliningrad.qp.ru/novostroiki/prodau_odnokomnatnuu_kvartiru_4043200"),
            };

            var fetchConfiguration = new FetchConfiguration
            {
                Site = new Site
                {
                    Link = "http://kudago.com",
                    Cookies = new[]
                    {
                        new Cookie
                        {
                            Name = "cookie1",
                            Value = "cookie1Value",
                        },
                        new Cookie
                        {
                            Name = "cookie2",
                            Value = "cookie2Value",
                        },
                    },
                    //ProxyAddress = "185.209.112.144:24531",
                },
                Links = links,
                Rules = data,
                SpiderType = SpiderType.Puppeter,
            };

            var result = Spider.FetchSiteContent(fetchConfiguration);

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Items.Length);
        }

        [Test]
        public async Task FetchSiteContentTest()
        {
            var data = new[]
            {
                new SearchRule
                {
                    FieldName = "name1",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".page-title-tagline-wrapper",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
                new SearchRule
                {
                    FieldName = "name2",
                    SearchType = SearchType.ManyNodes,
                    Selectors = ".post-title-link",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.AttributeContent,
                        AttrName = "href",
                    }
                },
                new SearchRule
                {
                    FieldName = "name3",
                    SearchType = SearchType.ManyNodes,
                    Selectors = ".post-big-details-schedule tr td:first-child nobr",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
            };

            var links = new[]
            {
                UrlHelper.ConstructFullUri("http://kudago.com", "/spb/theater/"),
            };

            var fetchConfiguration = new FetchConfiguration
            {
                Site = new Site { Link = "http://kudago.com" },
                Links = links,
                Rules = data,
            };

            var result = await SiteContentService.FetchArticlesContent(fetchConfiguration);

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Result.Length);
        }

        [Test]
        public async Task FetchRutrackerSiteContentTest()
        {
            var data = new[]
            {
                new SearchRule
                {
                    FieldName = "Header",
                    SearchType = SearchType.SingleNode,
                    Selectors = "#topic-title",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
                new SearchRule
                {
                    FieldName = "Body-Text",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".post_body",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
                new SearchRule
                {
                    FieldName = "Body-Html",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".post_body",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagContent,
                    }
                },
                new SearchRule
                {
                    FieldName = "MagnetLink",
                    SearchType = SearchType.SingleNode,
                    Selectors = "#tor-reged .row1 a.magnet-link-16",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.AttributeContent,
                        AttrName = "href",
                    }
                },
                new SearchRule
                {
                    FieldName = "Image",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".post_body img.postImg",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.AttributeContent,
                        AttrName = "src",
                    }
                },
                new SearchRule
                {
                    FieldName = "Size",
                    SearchType = SearchType.SingleNode,
                    Selectors = "#tor-reged .row1 #tor-size-humn",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
            };

            var links = new[] { UrlHelper.ConstructFullUri("http://rutracker.net/", "/forum/viewtopic.php?t=5163189") };
            var fetchConfiguration = new FetchConfiguration
            {
                Site = new Site
                {
                    Link = "http://rutracker.net/",
                    Cookies = new[]
                    {
                        new Cookie
                        {
                            Name = "bb_data",
                            Value = "1-19129974-pse98NuI2VgKcA0jzaF7-0-1460103303-1460103303-2092276282-0",
                        }
                    }
                },
                Links = links,
                Rules = data,
            };

            var result = await SiteContentService.FetchArticlesContent(fetchConfiguration);

            Assert.IsNotNull(result);
        }
    }
}
