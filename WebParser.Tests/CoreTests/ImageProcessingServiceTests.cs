﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Interfaces.Services.Parsing.ImageProcessing;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.Parsing.ImageProcessing;
using WebParser.Common.Extensions;
using WebParser.Configuration;

namespace WebParser.Tests.CoreTests
{
    [TestFixture(Ignore = "Ignore")]
    public class ImageProcessingServiceTests : TestBase
    {
        private IImageProcessingService ImageProcessingService { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            var fileServiceMock = new Mock<IFileService>();
            fileServiceMock
                .Setup(x => x.CreateTemporaryFiles(It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<KeyValuePair<int, FileDetails>[]>()))
                .Returns<int, int?, KeyValuePair<int, FileDetails>[]>((siteId, userId, files) =>
                {
                    return Task.FromResult(files.ToArray(x => new KeyValuePair<int, CreatedFile>(
                    0,
                    new CreatedFile
                    {
                        FileUrl = x.Value.FileExtension,
                        FileData = x.Value.FileData,
                    })));
                });
            RegisterMock(fileServiceMock);

            ImageProcessingService = Resolve<IImageProcessingService>();
        }

        [Test]
        public async Task ProcessImagesTest()
        {
            var links = new[]
            {
                "https://93.img.avito.st/1280x960/3748824293.jpg",
            };

            var site = new Site
            {
                Link = "http://avito.ru",
                CropImageTop = 0,
                CropImageBottom = 0,
                CropImageLeft = 0,
                CropImageRight = 0,
            };

            var result = await ImageProcessingService.ProcessImages(links, site);
            SaveImage("original", result.FullSizeImages[0]);
            SaveImage("original-preview", result.ImagesPreview[0]);

            result = await ImageProcessingService.ProcessImages(links, site.Update(x => x.CropImageBottom = 80));
            SaveImage("cropBottom80", result.FullSizeImages[0]);
            SaveImage("cropBottom80-preview", result.ImagesPreview[0]);

            result = await ImageProcessingService.ProcessImages(links, site.Update(x => x.CropImageBottom = 0).Update(x => x.CropImageTop = 80));
            SaveImage("cropTop80", result.FullSizeImages[0]);
            SaveImage("cropTop80-preview", result.ImagesPreview[0]);

            result = await ImageProcessingService.ProcessImages(links, site.Update(x => x.CropImageTop = 0).Update(x => x.CropImageLeft = 80));
            SaveImage("cropLeft80", result.FullSizeImages[0]);
            SaveImage("cropLeft80-preview", result.ImagesPreview[0]);

            result = await ImageProcessingService.ProcessImages(links, site.Update(x => x.CropImageLeft = 0).Update(x => x.CropImageRight = 80));
            SaveImage("cropRight80", result.FullSizeImages[0]);
            SaveImage("cropRight80-preview", result.ImagesPreview[0]);
        }

        private static void SaveImage(string name, ImageInfo file)
        {
            var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);
            File.WriteAllBytes($"{path}\\{name}.{file.ImageUrl}", file.FileData);
        }
    }
}
