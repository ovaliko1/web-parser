﻿using System.Linq;
using NUnit.Framework;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Application.Services.Parsing.ParsedContent;
using WebParser.Common.MetroStations;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Configuration;
using WebParser.DataAccess;
using Moq;
using WebParser.DataAccess.Entities;
using System.Data.Entity;
using WebParser.Application.Services.Repository;

namespace WebParser.Tests.CoreTests
{
    [TestFixture]
    public class MetroStationsTests : TestBase
    {
        private const int SpbCityId = 1;
        private const int MoscowCityId = 1;

        private static readonly DbMetroStation[] DbMetroStations =
        {
            new DbMetroStation { Id = 1, CityId = SpbCityId, MetroLineId = 1,  MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Девяткино", SearchKeywords = "Девяткино", },
            new DbMetroStation { Id = 2, CityId = SpbCityId, MetroLineId = 1,  MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Гражданский проспект", SearchKeywords =  "Гражданский", },
            new DbMetroStation { Id = 3, CityId = SpbCityId, MetroLineId = 1,  MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Академическая", SearchKeywords =  "Академическая" },
            new DbMetroStation { Id = 4, CityId = SpbCityId, MetroLineId = 1,  MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Политехническая", SearchKeywords =  "Политехническая"},
            new DbMetroStation { Id = 5, CityId = SpbCityId, MetroLineId = 1,  MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Площадь Мужества", SearchKeywords =  "Мужества"},
            new DbMetroStation { Id = 6, CityId = SpbCityId, MetroLineId = 1,  MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Лесная", SearchKeywords =  "Лесная"},
            new DbMetroStation { Id = 7, CityId = SpbCityId, MetroLineId = 1,  MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Выборгская", SearchKeywords =  "Выборгская"},
            new DbMetroStation { Id = 8, CityId = SpbCityId, MetroLineId = 1,  MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Площадь Ленина", SearchKeywords =  "Ленина"},
            new DbMetroStation { Id = 9, CityId = SpbCityId, MetroLineId = 1,  MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Чернышевская", SearchKeywords =  "Чернышевская"},
            new DbMetroStation { Id = 10, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Площадь Восстания", SearchKeywords =  "Восстания"},
            new DbMetroStation { Id = 11, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Владимирская", SearchKeywords =  "Владимирская"},
            new DbMetroStation { Id = 12, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Пушкинская", SearchKeywords =  "Пушкинская"},
            new DbMetroStation { Id = 13, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Технологический инс. I", SearchKeywords =  "Технологический"},
            new DbMetroStation { Id = 14, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Балтийская", SearchKeywords =  "Балтийская"},
            new DbMetroStation { Id = 15, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Нарвская", SearchKeywords =  "Нарвская"},
            new DbMetroStation { Id = 16, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Кировский завод", SearchKeywords =  "Кировский"},
            new DbMetroStation { Id = 17, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Автово", SearchKeywords =  "Автово"},
            new DbMetroStation { Id = 18, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Ленинский проспект", SearchKeywords =  "Ленинский"},
            new DbMetroStation { Id = 19, CityId = SpbCityId, MetroLineId = 1, MetroLine = new DbMetroLine { Id = 1,       Name = "Кировско-Выборгская", Color = "red", }, Name = "Проспект Ветеранов", SearchKeywords =  "Ветеранов"},
            new DbMetroStation { Id = 20, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Парнас", SearchKeywords =  "Парнас"},
            new DbMetroStation { Id = 21, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Проспект Просвещения", SearchKeywords =  "Просвещения"},
            new DbMetroStation { Id = 22, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Озерки", SearchKeywords =  "Озерки"},
            new DbMetroStation { Id = 23, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Удельная", SearchKeywords =  "Удельная"},
            new DbMetroStation { Id = 24, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Пионерская", SearchKeywords =  "Пионерская"},
            new DbMetroStation { Id = 25, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Черная речка", SearchKeywords =  "Чёрная,Черная" },
            new DbMetroStation { Id = 26, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Петроградская", SearchKeywords =  "Петроградская"},
            new DbMetroStation { Id = 27, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Горьковская", SearchKeywords =  "Горьковская"},
            new DbMetroStation { Id = 28, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Невский проспект", SearchKeywords =  "Невский"},
            new DbMetroStation { Id = 29, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Сенная площадь", SearchKeywords =  "Сенная"},
            new DbMetroStation { Id = 30, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Технологический инс. II", SearchKeywords =  "Технологический"},
            new DbMetroStation { Id = 31, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Фрунзенская", SearchKeywords =  "Фрунзенская"},
            new DbMetroStation { Id = 32, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Московские ворота", SearchKeywords =  "Московские"},
            new DbMetroStation { Id = 33, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Электросила", SearchKeywords =  "Электросила"},
            new DbMetroStation { Id = 34, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Парк Победы", SearchKeywords =  "Парк Победы"},
            new DbMetroStation { Id = 35, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Московская", SearchKeywords =  "Московская"},
            new DbMetroStation { Id = 36, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Звездная", SearchKeywords =  "Звёздная,Звездная"},
            new DbMetroStation { Id = 37, CityId = SpbCityId, MetroLineId = 2, MetroLine = new DbMetroLine { Id = 2,  Name = "Московско-Петроградская", Color = "blue", }, Name = "Купчино", SearchKeywords =  "Купчино"},
            new DbMetroStation { Id = 38, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Приморская", SearchKeywords =  "Приморская"},
            new DbMetroStation { Id = 39, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Василеостровская", SearchKeywords =  "Василеостровская"},
            new DbMetroStation { Id = 40, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Гостиный двор", SearchKeywords =  "Гостиный"},
            new DbMetroStation { Id = 41, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Маяковская", SearchKeywords =  "Маяковская"},
            new DbMetroStation { Id = 42, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Пл. Александра Невского I", SearchKeywords =  "Александра"},
            new DbMetroStation { Id = 43, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Елизаровская", SearchKeywords =  "Елизаровская"},
            new DbMetroStation { Id = 44, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Ломоносовская", SearchKeywords =  "Ломоносовская"},
            new DbMetroStation { Id = 45, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Пролетарская", SearchKeywords =  "Пролетарская"},
            new DbMetroStation { Id = 46, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Обухово", SearchKeywords =  "Обухово"},
            new DbMetroStation { Id = 47, CityId = SpbCityId, MetroLineId = 3, MetroLine = new DbMetroLine { Id = 3, Name = "Невско-Василеостровская", Color = "green", }, Name = "Рыбацкое", SearchKeywords =  "Рыбацкое"},
            new DbMetroStation { Id = 48, CityId = SpbCityId, MetroLineId = 4, MetroLine = new DbMetroLine { Id = 4,          Name = "Правобережная", Color = "orange", }, Name = "Спасская", SearchKeywords =  "Спасская"},
            new DbMetroStation { Id = 49, CityId = SpbCityId, MetroLineId = 4, MetroLine = new DbMetroLine { Id = 4,          Name = "Правобережная", Color = "orange", }, Name = "Достоевская", SearchKeywords =  "Достоевская"},
            new DbMetroStation { Id = 50, CityId = SpbCityId, MetroLineId = 4, MetroLine = new DbMetroLine { Id = 4,          Name = "Правобережная", Color = "orange", }, Name = "Лиговский проспект", SearchKeywords =  "Лиговский"},
            new DbMetroStation { Id = 51, CityId = SpbCityId, MetroLineId = 4, MetroLine = new DbMetroLine { Id = 4,          Name = "Правобережная", Color = "orange", }, Name = "Пл. Александра Невского II", SearchKeywords =  "Александра"},
            new DbMetroStation { Id = 52, CityId = SpbCityId, MetroLineId = 4, MetroLine = new DbMetroLine { Id = 4,          Name = "Правобережная", Color = "orange", }, Name = "Новочеркасская", SearchKeywords =  "Новочеркасская"},
            new DbMetroStation { Id = 53, CityId = SpbCityId, MetroLineId = 4, MetroLine = new DbMetroLine { Id = 4,          Name = "Правобережная", Color = "orange", }, Name = "Ладожская", SearchKeywords =  "Ладожская"},
            new DbMetroStation { Id = 54, CityId = SpbCityId, MetroLineId = 4, MetroLine = new DbMetroLine { Id = 4,          Name = "Правобережная", Color = "orange", }, Name = "Проспект Большевиков", SearchKeywords =  "Большевиков"},
            new DbMetroStation { Id = 55, CityId = SpbCityId, MetroLineId = 4, MetroLine = new DbMetroLine { Id = 4,          Name = "Правобережная", Color = "orange", }, Name = "Улица Дыбенко", SearchKeywords =  "Дыбенко"},
            new DbMetroStation { Id = 56, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Комендантский проспект", SearchKeywords =  "Комендантский"},
            new DbMetroStation { Id = 57, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Старая Деревня", SearchKeywords =  "Деревня,Старая"},
            new DbMetroStation { Id = 58, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Крестовский остров", SearchKeywords =  "Крестовский"},
            new DbMetroStation { Id = 59, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Чкаловская", SearchKeywords =  "Чкаловская"},
            new DbMetroStation { Id = 60, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Спортивная", SearchKeywords =  "Спортивная"},
            new DbMetroStation { Id = 61, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Адмиралтейская", SearchKeywords =  "Адмиралтейская"},
            new DbMetroStation { Id = 62, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Садовая", SearchKeywords =  "Садовая"},
            new DbMetroStation { Id = 63, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Звенигородская", SearchKeywords =  "Звенигородская"},
            new DbMetroStation { Id = 64, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Обводный канал", SearchKeywords =  "Обводный"},
            new DbMetroStation { Id = 65, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Волковская", SearchKeywords =  "Волковская"},
            new DbMetroStation { Id = 66, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Бухарестская", SearchKeywords =  "Бухарестская"},
            new DbMetroStation { Id = 67, CityId = SpbCityId, MetroLineId = 5, MetroLine = new DbMetroLine { Id = 5,  Name = "Фрунзенско-Приморская", Color = "purple", }, Name = "Международная", SearchKeywords =  "Международная"},
        };

        private IMetroMapService MetroMapService { get; set; }
        private Mock<IMetroRepositoryService> MetroRepositoryService { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            MetroRepositoryService = new Mock<IMetroRepositoryService>();
            MetroRepositoryService.Setup(x => x.ReadStations()).Returns(DbMetroStations);
            RegisterMock(MetroRepositoryService);

            MetroMapService = Resolve<IMetroMapService>();
        }

        [Test]
        public void ExtractMetroStationsTest()
        {
            var parsedValue = ParsedValue.Create("м. Пл. Восстания");
            var result = MetroMapService.ExtractMetroStations(SpbCityId, parsedValue);

            var expected = MetroMapService.GetStations(SpbCityId).First(x => x.Name == "Площадь Восстания");

            Assert.AreEqual(1, result.Length);
            Assert.AreEqual(expected.Id, result[0].Id);
            Assert.AreEqual(expected.Name, result[0].Name);
        }

        [Test]
        public void ExtractMetroSeveralStationsTest()
        {
            var parsedValue = ParsedValue.Create("Гражданский проспект 1,2 км", "Академическая 2,7 км", "Девяткино 2,9 км", "звезднАя 2,9 км");
            var result = MetroMapService.ExtractMetroStations(SpbCityId, parsedValue);

            var expected1 = MetroMapService.GetStations(SpbCityId).First(x => x.Name == "Гражданский проспект");
            var expected2 = MetroMapService.GetStations(SpbCityId).First(x => x.Name == "Академическая");
            var expected3 = MetroMapService.GetStations(SpbCityId).First(x => x.Name == "Девяткино");
            var expected4 = MetroMapService.GetStations(SpbCityId).First(x => x.Name == "Звездная");

            Assert.AreEqual(4, result.Length);

            var res1 = result.First(x => x.Id == expected1.Id);
            var res2 = result.First(x => x.Id == expected2.Id);
            var res3 = result.First(x => x.Id == expected3.Id);
            var res4 = result.First(x => x.Id == expected4.Id);

            Assert.AreEqual(expected1.Id, res1.Id);
            Assert.AreEqual(expected1.Name, res1.Name);

            Assert.AreEqual(expected2.Id, res2.Id);
            Assert.AreEqual(expected2.Name, res2.Name);

            Assert.AreEqual(expected3.Id, res3.Id);
            Assert.AreEqual(expected3.Name, res3.Name);

            Assert.AreEqual(expected4.Id, res4.Id);
            Assert.AreEqual(expected4.Name, res4.Name);
        }
    }
}
