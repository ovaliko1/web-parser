﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Interfaces.Services.SiteStructure;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.Parsing.ParsingProgress;
using WebParser.Application.Services.Parsing.SiteContent;
using WebParser.Application.Services.Parsing.SiteContent.Entities;
using WebParser.Application.Services.WebClient;
using WebParser.Common;
using WebParser.Common.Extensions;
using WebParser.Configuration;

namespace WebParser.Tests.CoreTests
{
    [TestFixture]
    public class FileDownloaderTests : TestBase
    {
        private IFileDownloader FileDownloader { get; set; }

        private Mock<IWebCommand> WebCommandMock { get; set; }

        private Mock<IFileService> FileServiceMock { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            WebCommandMock = new Mock<IWebCommand>();
            FileServiceMock = new Mock<IFileService>();

            RegisterMock(WebCommandMock);
            RegisterMock(FileServiceMock);

            var errorsHandlerMoq = new Mock<IParsingLogger>();
            errorsHandlerMoq.Setup(x => x.Log(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<ParsingLogStatus>(), It.IsAny<string>())).Verifiable();
            RegisterMock(errorsHandlerMoq);

            FileDownloader = Resolve<IFileDownloader>();
        }

        [Test]
        public async Task DownloadFiles()
        {
            var fieldName1 = "Name1";
            var fieldName2 = "Name2";
            var fieldName3 = "Name3";
            var items = Utilities.CreateArray(new FetchResultItem
            {
                Url = "http://realty.kaliningrad.qp.ru/aaa/bbb",
                Result = Utilities.CreateArray(
                    new FetchFieldValue
                    {
                        Name = fieldName1,
                        Value = "http://www.ru/img1.jpg",
                    },
                    new FetchFieldValue
                    {
                        Name = fieldName2,
                        Values = new string[0],
                    },
                    new FetchFieldValue
                    {
                        Name = fieldName2,
                        Values = new[] { "http://www.ru/img2.jpg", "http://www.ru/img3.jpg" },
                    },
                    new FetchFieldValue
                    {
                        Name = fieldName3,
                        Values = new[] { "http://www.ru/img4.jpg" },
                    })
            });

            var rules = Utilities.CreateArray(
                new SearchRule
                {
                    FieldName = fieldName1,
                    SearchType = SearchType.SingleNode,
                    IsImage = true,
                },
                new SearchRule
                {
                    FieldName = fieldName2,
                    SearchType = SearchType.ManyNodes,
                    IsImage = true,
                },
                new SearchRule
                {
                    FieldName = fieldName3,
                    SearchType = SearchType.ManyNodes,
                    IsImage = true,
                    DoNotSaveToInternalStorage = true,
                });

            WebCommandMock
                .Setup(x => x.DownloadFile(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Cookie[]>()))
                .ReturnsAsync(new FileDetails
                {
                    ContentType = "img/jpg",
                    FileName = "img.jpg",
                    FileData = new[] { new byte(), }
                });

            var file1 = new CreatedFile { FileUrl = Guid.NewGuid() + ".jpg", FileData = new[] { (byte)1 }, };
            var file2 = new CreatedFile { FileUrl = Guid.NewGuid() + ".jpg", FileData = new[] { (byte)2 }, };
            var file3 = new CreatedFile { FileUrl = Guid.NewGuid() + ".jpg", FileData = new[] { (byte)3 }, };

            FileServiceMock
                .Setup(x => x.CreateTemporaryFiles(It.IsAny<int>(), It.IsAny<int?>(), It.IsAny<KeyValuePair<int, FileDetails>[]>()))
                .ReturnsAsync(new[]
                {
                    new KeyValuePair<int, CreatedFile>(1, file1),
                    new KeyValuePair<int, CreatedFile>(3, file2),
                    new KeyValuePair<int, CreatedFile>(3, file3),
                });

            var fetchConfig = new FetchConfiguration
            {
                Site = new Site { Link = "http://realty.kaliningrad.qp.ru/" },
                Rules = rules,
            };

            await FileDownloader.DownloadFiles(items, fetchConfig);

            Assert.AreEqual(file1, items[0].Result[0].CreatedFiles[0]);
            Assert.AreEqual(file1.FileUrl, items[0].Result[0].Value);
            Assert.IsTrue(items[0].Result[0].IsLinkToInternalStorage);

            Assert.AreEqual(0, items[0].Result[1].Values.Length);

            Assert.AreEqual(2, items[0].Result[2].Values.Length);
            Assert.IsTrue(items[0].Result[2].CreatedFiles.Contains(file2));
            Assert.IsTrue(items[0].Result[2].CreatedFiles.Contains(file3));
            Assert.IsTrue(items[0].Result[2].Values.Contains(file2.FileUrl));
            Assert.IsTrue(items[0].Result[2].Values.Contains(file3.FileUrl));

            Assert.IsTrue(items[0].Result[2].IsLinkToInternalStorage);

            Assert.AreEqual(1, items[0].Result[3].Values.Length);
            Assert.AreEqual("http://www.ru/img4.jpg", items[0].Result[3].Values[0]);
            Assert.IsFalse(items[0].Result[3].IsLinkToInternalStorage);
        }
    }
}
