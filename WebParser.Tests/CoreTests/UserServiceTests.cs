﻿using Moq;
using NUnit.Framework;
using WebParser.Application.Interfaces.Services.Settings;
using WebParser.Application.Interfaces.Services.UserManagement;
using WebParser.Application.Services.UserManagement;
using WebParser.Configuration;

namespace WebParser.Tests.CoreTests
{
    [TestFixture]
    public class UserServiceTests : TestBase
    {
        private Mock<ICommonSettings> CommonSettingsMock { get; set; }

        private UserService UserService { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            CommonSettingsMock = new Mock<ICommonSettings>();
            RegisterMock(CommonSettingsMock);

            UserService = (UserService)Resolve<IUserService>();

            CommonSettingsMock.Setup(x => x.GetFirstRateOpeningsCount()).Returns(1);
            CommonSettingsMock.Setup(x => x.GetFirstRateOpeningsPrice()).Returns(150);
            CommonSettingsMock.Setup(x => x.GetSecondRateOpeningsCount()).Returns(5);
            CommonSettingsMock.Setup(x => x.GetSecondRateOpeningsPrice()).Returns(490);
            CommonSettingsMock.Setup(x => x.GetThirdRateOpeningsCount()).Returns(10);
            CommonSettingsMock.Setup(x => x.GetThirdRateOpeningsPrice()).Returns(790);
            CommonSettingsMock.Setup(x => x.GetFourthRateOpeningsCount()).Returns(20);
            CommonSettingsMock.Setup(x => x.GetFourthRateOpeningsPrice()).Returns(990);
            CommonSettingsMock.Setup(x => x.GetFirstRateDaysCount()).Returns(1);
            CommonSettingsMock.Setup(x => x.GetFirstRateDaysPrice()).Returns(390);
            CommonSettingsMock.Setup(x => x.GetSecondRateDaysCount()).Returns(7);
            CommonSettingsMock.Setup(x => x.GetSecondRateDaysPrice()).Returns(690);
            CommonSettingsMock.Setup(x => x.GetThirdRateDaysCount()).Returns(14);
            CommonSettingsMock.Setup(x => x.GetThirdRateDaysPrice()).Returns(890);
            CommonSettingsMock.Setup(x => x.GetFourthRateDaysCount()).Returns(30);
            CommonSettingsMock.Setup(x => x.GetFourthRateDaysPrice()).Returns(990);
        }

        [Test]
        public void TryGetExtendAccessDetailsTestWhenEmptyLabel()
        {
            UserService.ExtendAccessDetails extendAccessDetails;
            string errorMessage;

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong paymentConfirmationLabel: ", errorMessage);
        }

        [Test]
        public void TryGetExtendAccessDetailsTestWhenWrongPublicUserId()
        {
            UserService.ExtendAccessDetails extendAccessDetails;
            string errorMessage;

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("WrongPublicUserId_1_2_3", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong publicUserId in label: WrongPublicUserId", errorMessage);
        }

        [Test]
        public void TryGetExtendAccessDetailsTestWhenWrongRateType()
        {
            UserService.ExtendAccessDetails extendAccessDetails;
            string errorMessage;

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_a_2_3", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong rateType in label: a", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_10_1_3", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong rateType in label: 10", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_-1_1_3", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong rateType in label: -1", errorMessage);
        }

        [Test]
        public void TryGetExtendAccessDetailsTestWhenWrongPrice()
        {
            UserService.ExtendAccessDetails extendAccessDetails;
            string errorMessage;

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_aa_3", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong price in label: aa", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_-1_3", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong price in label: -1", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_0_3", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong price in label: 0", errorMessage);
        }

        [Test]
        public void TryGetExtendAccessDetailsTestWhenWrongItemsCount()
        {
            UserService.ExtendAccessDetails extendAccessDetails;
            string errorMessage;

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_aa", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong itemsCount in label: aa", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_-1", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong itemsCount in label: -1", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_0", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong itemsCount in label: 0", errorMessage);
        }

        [Test]
        public void TryGetExtendAccessDetailsTestWhenNoSuitableItemsCount()
        {
            UserService.ExtendAccessDetails extendAccessDetails;
            string errorMessage;

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_2", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong itemsCount in label. No suitable RateOpeningsCount found: 2", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_2_1_2", 0m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("Wrong itemsCount in label. No suitable RateDaysCount found: 2", errorMessage);
        }

        [Test]
        public void TryGetExtendAccessDetailsTestWhenAmountsNotMatched()
        {
            UserService.ExtendAccessDetails extendAccessDetails;
            string errorMessage;

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_1", 1m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("expectedAmount(150) not matched to actualAmount(1)", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_5", 1m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("expectedAmount(490) not matched to actualAmount(1)", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_10", 1m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("expectedAmount(790) not matched to actualAmount(1)", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_20", 1m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("expectedAmount(990) not matched to actualAmount(1)", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_2_1_1", 1m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("expectedAmount(390) not matched to actualAmount(1)", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_2_1_7", 1m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("expectedAmount(690) not matched to actualAmount(1)", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_2_1_14", 1m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("expectedAmount(890) not matched to actualAmount(1)", errorMessage);

            Assert.IsFalse(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_2_1_30", 1m, out extendAccessDetails, out errorMessage));
            Assert.AreEqual("expectedAmount(990) not matched to actualAmount(1)", errorMessage);
        }

        [Test]
        public void TryGetExtendAccessDetailsTestSuccess()
        {
            UserService.ExtendAccessDetails extendAccessDetails;
            string errorMessage;

            Assert.IsTrue(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_1", 150m, out extendAccessDetails, out errorMessage));
            Assert.IsTrue(string.IsNullOrEmpty(errorMessage));
            Assert.AreEqual(1, extendAccessDetails.OpeningsToAdd);
            Assert.AreEqual(0, extendAccessDetails.DaysToAdd);

            Assert.IsTrue(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_5", 490m, out extendAccessDetails, out errorMessage));
            Assert.IsTrue(string.IsNullOrEmpty(errorMessage));
            Assert.AreEqual(5, extendAccessDetails.OpeningsToAdd);
            Assert.AreEqual(0, extendAccessDetails.DaysToAdd);

            Assert.IsTrue(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_10", 790m, out extendAccessDetails, out errorMessage));
            Assert.IsTrue(string.IsNullOrEmpty(errorMessage));
            Assert.AreEqual(10, extendAccessDetails.OpeningsToAdd);
            Assert.AreEqual(0, extendAccessDetails.DaysToAdd);

            Assert.IsTrue(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_1_1_20", 990m, out extendAccessDetails, out errorMessage));
            Assert.IsTrue(string.IsNullOrEmpty(errorMessage));
            Assert.AreEqual(20, extendAccessDetails.OpeningsToAdd);
            Assert.AreEqual(0, extendAccessDetails.DaysToAdd);

            Assert.IsTrue(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_2_1_1", 390m, out extendAccessDetails, out errorMessage));
            Assert.IsTrue(string.IsNullOrEmpty(errorMessage));
            Assert.AreEqual(0, extendAccessDetails.OpeningsToAdd);
            Assert.AreEqual(1, extendAccessDetails.DaysToAdd);

            Assert.IsTrue(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_2_1_7", 690m, out extendAccessDetails, out errorMessage));
            Assert.IsTrue(string.IsNullOrEmpty(errorMessage));
            Assert.AreEqual(0, extendAccessDetails.OpeningsToAdd);
            Assert.AreEqual(7, extendAccessDetails.DaysToAdd);

            Assert.IsTrue(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_2_1_14", 890m, out extendAccessDetails, out errorMessage));
            Assert.IsTrue(string.IsNullOrEmpty(errorMessage));
            Assert.AreEqual(0, extendAccessDetails.OpeningsToAdd);
            Assert.AreEqual(14, extendAccessDetails.DaysToAdd);

            Assert.IsTrue(UserService.TryGetExtendAccessDetails("057C9719-8AB0-42A0-996D-44EAED806BD2_2_1_30", 990m, out extendAccessDetails, out errorMessage));
            Assert.IsTrue(string.IsNullOrEmpty(errorMessage));
            Assert.AreEqual(0, extendAccessDetails.OpeningsToAdd);
            Assert.AreEqual(30, extendAccessDetails.DaysToAdd);
        }
    }
}
