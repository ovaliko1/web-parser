﻿using System.IO;
using System.Reflection;
using NUnit.Framework;
using WebParser.Application.Interfaces.Services.File;
using WebParser.Application.Services.Parsing.ImageRecognizing;
using WebParser.Common.Extensions;
using WebParser.Configuration;

namespace WebParser.Tests.CoreTests
{
    [TestFixture(Ignore = "Ignore")]
    public class ImageRecognizingServiceTests : TestBase
    {
        private IImageRecognizingService ImageRecognizingService { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            ImageRecognizingService = Resolve<IImageRecognizingService>();
        }

        [Test]
        public void RecognizedImagesTest1()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("contactnumberimage.png")))[0];
            CheckRecognizedResult(result, "8 921 779-04-56\n\n", "89217790456", 82);
        }

        [Test]
        public void RecognizedImagesTest2()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("domofond1.png")))[0];
            CheckRecognizedResult(result, "8 911 97&8400\n\n", "89119788400", 79);
        }

        [Test]
        public void RecognizedImagesTest3()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("domofond2.png")))[0];
            CheckRecognizedResult(result, "8 921 57tF23-23\n\n", "89215702323", 77);
        }

        [Test]
        public void RecognizedImagesTest4()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("contactnumberimage1.png")))[0];
            CheckRecognizedResult(result, "8 981 757-48-A9\n\n", "89817574849", 81);
        }

        [Test]
        public void RecognizedImagesTest5()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("contactnumberimage2.png")))[0];
            CheckRecognizedResult(result, "8 911 1t14-26-25\n\n", "89111042625", 76);
        }

        [Test]
        public void RecognizedImagesTest6()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("contactnumberimage3.png")))[0];
            CheckRecognizedResult(result, "8 904 339-40-06\n\n", "89043394006", 79);
        }

        [Test]
        public void RecognizedImagesTest7()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("contactnumberimage4.png")))[0];
            CheckRecognizedResult(result, "8 921 931-71-A4\n\n", "89219317144", 81);
        }

        [Test]
        public void RecognizedImagesTest8()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("contactnumberimage7.png")))[0];
            CheckRecognizedResult(result, "8 921 642-12-28\n\n", "89216421228", 82);
        }

        [Test]
        public void RecognizedImagesTest9()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("contactnumberimage9.png")))[0];
            CheckRecognizedResult(result, "8 904 5305939\n\n", "89045305939", 76);
        }

        [Test]
        public void RecognizedImagesTest91()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("contactnumberimage11.png")))[0];
            CheckRecognizedResult(result, "8 921 967-26-A9\n\n", "89219672649", 81);
        }
        [Test]
        public void RecognizedImagesTest92()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("contactnumberimage12.png")))[0];
            CheckRecognizedResult(result, "8 921 447-98-AI\n\n", "89214479841", 81);
        }
        [Test]
        public void RecognizedImagesTest93()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("avito1.png")))[0];
            CheckRecognizedResult(result, "8 904 602-09-67\n\n", "89046020967", 95);
        }
        [Test]
        public void RecognizedImagesTest94()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("avito2.png")))[0];
            CheckRecognizedResult(result, "8 905 230-65-11\n\n", "89052306511", 95);
        }

        [Test]
        public void RecognizedImagesTest95()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("avito3.png")))[0];
            CheckRecognizedResult(result, "8 911926-11-18\n\n", "89119261118", 95);
        }

        [Test]
        public void RecognizedImagesTest96()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("vitodom-imagekontakt1.png")))[0];
            CheckRecognizedResult(result, string.Empty, string.Empty, 0);
        }

        [Test]
        public void RecognizedImagesTest97()
        {
            var result = ImageRecognizingService.RecognizedImages(Utilities.CreateArray(GetImage("vitodom-imagekontakt2.png")))[0];
            CheckRecognizedResult(result, "g 904 641 14 49\n\n", "89046411449", 68);
        }

        private static void CheckRecognizedResult(RecognizedImage result, string text, string fixedText, int meanConfidencePercentage)
        {
            Assert.AreEqual(text, result.Text);
            Assert.AreEqual(fixedText, result.Text.ExtractPhoneNumber());
            Assert.AreEqual(meanConfidencePercentage, result.MeanConfidencePercentage);
        }

        private CreatedFile GetImage(string imageName)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream("WebParser.Tests.Resources." + imageName))
            {
                return new CreatedFile
                {
                    FileData = stream.GetBytes(),
                };
            }
        }
    }
}
