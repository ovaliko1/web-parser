﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.DomainEntities.Search.ParsingRule;
using WebParser.Application.Interfaces.Services.SiteStructure.SiteSettings;
using WebParser.Application.Services.Parsing.SiteContent;
using WebParser.Application.Services.Parsing.SiteContent.Entities;
using WebParser.Application.Services.Parsing.Spider;
using WebParser.Application.Services.SiteStructure.SiteSettings;
using WebParser.Common;
using WebParser.Configuration;

namespace WebParser.Tests.CoreTests
{
    [TestFixture]
    public class SiteContentServiceTest : TestBase
    {
        private ISiteContentService SiteContentService { get; set; }

        private Mock<ISpider> SpiderMock { get; set; }

        [SetUp]
        public void SetUp()
        {
            ContainerConfig.InitContainer();

            SpiderMock = new Mock<ISpider>();
            RegisterMock(SpiderMock);

            SiteContentService = Resolve<ISiteContentService>();
        }

        [Test]
        public async Task FetchSiteContentWithDateTimeFieldTest()
        {
            const string fieldName = "Date";

            FetchConfiguration config = new FetchConfiguration
            {
                Site = new Site { Link = "http://realty.kaliningrad.qp.ru/" },
                Rules = new[]
                {
                    new SearchRule
                    {
                        FieldName = fieldName,
                        SearchType = SearchType.SingleNode,
                        Selectors = ".center-side h1",
                        FoundValue = new FoundValue
                        {
                            FoundValueType = FoundValueType.AttributeContent,
                            AttrName = "data",
                        },
                        IsDateTime = true,
                        DateTimeParsingRule = new DateTimeParsingRule
                        {
                            CultureInfo = CultureInfo.InvariantCulture,
                            DateFormat = "dd MM yyyy",
                        },
                        ParsedKeyTypeId = (int)ParsedKeyType.SourceDate,
                    }
                },
                Links = new[] {new Uri("http://realty.kaliningrad.qp.ru/"),},
                IsPreview = true,
            };

            SpiderMock.Setup(x => x.FetchSiteContent(
                    It.IsAny<FetchConfiguration>()))
                .Returns(new FetchResult
                {
                    Items = new[]
                    {
                        new FetchResultItem
                        {
                           Url = "http://realty.kaliningrad.qp.ru/",
                           Result = new []
                           {
                               new FetchFieldValue
                               {
                                   Value = null,
                                   Name = fieldName,
                               }
                           }
                        }
                    }
                });

            var result = await SiteContentService.FetchArticlesContent(config);

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Result.Length);

            var members = result.Result[0].ParsedValues.ToDictionary(x => x.Name, x => x);

            Assert.AreEqual(1, members.Count);

            Assert.IsTrue(members.ContainsKey(fieldName));

            Assert.AreEqual(0, members[fieldName].Values.Length);
            Assert.IsNull(members[fieldName].DateTimeValues);

            SpiderMock.Setup(x => x.FetchSiteContent(
                    It.IsAny<FetchConfiguration>()))
                .Returns(new FetchResult
                {
                    Items = new[]
                    {
                        new FetchResultItem
                        {
                           Url = "http://realty.kaliningrad.qp.ru/",
                           Result = new []
                           {
                               new FetchFieldValue
                               {
                                   Value = "10 10 2000",
                                   Name = fieldName,
                               }
                           }
                        }
                    }
                });

            result = await SiteContentService.FetchArticlesContent(config);

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Result.Length);

            members = result.Result[0].ParsedValues.ToDictionary(x => x.Name, x => x);

            Assert.AreEqual(1, members.Count);

            Assert.IsTrue(members.ContainsKey(fieldName));

            Assert.AreEqual(1, members[fieldName].DateTimeValues.Length);
            Assert.AreEqual(new DateTime(2000, 10, 10), members[fieldName].DateTimeValues[0]);

            Assert.IsNull(members[fieldName].Values);
        }
    }
}
