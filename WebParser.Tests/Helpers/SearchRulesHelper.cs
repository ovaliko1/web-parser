﻿using System.Collections.Generic;
using System.Globalization;
using WebParser.Application.Interfaces.DomainEntities.Search;
using WebParser.Application.Interfaces.DomainEntities.Search.ParsingRule;
using WebParser.Application.Interfaces.Services.Parsing.ParsedResult;
using WebParser.Common;

namespace WebParser.Tests.Helpers
{
    public static class SearchRulesHelper
    {
        public static ParsedValue WithName(this ParsedValue source, string name)
        {
            source.Name = name;
            return source;
        }

        public class ArticleGenerateResult
        {
            public string ArticleContent { get; set; }

            public SearchRules SearchRules { get; set; }
        }
        
        public static ArticleGenerateResult GenerateSearchRulesForArticle(bool withUnderscoreClassName)
        {
            var searchRules = new List<SearchRule>
            {
                new SearchRule
                {
                    FieldName = "Title",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".header-split h1",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent
                    }
                },
                new SearchRule
                {
                    FieldName = "Header",
                    SearchType = SearchType.SingleNode,
                    Selectors = "#content h2",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent
                    }
                },
                new SearchRule
                {
                    FieldName = "Article",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".news-detail-content",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent
                    }
                },
                new SearchRule
                {
                    FieldName = "PostedDate",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".meta span.date",
                    IsDateTime = true,
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent
                    },
                    DateTimeParsingRule = new DateTimeParsingRule
                    {
                        CultureInfo = CultureInfo.CreateSpecificCulture("Ru-ru"),
                        DateFormat = "dd MMMM yyyy"
                    },
                },
                new SearchRule
                {
                    FieldName = "Tags",
                    SearchType = SearchType.ManyNodes,
                    Selectors = ".meta span.section a",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent
                    }
                },
                new SearchRule
                {
                    FieldName = "LeftMenu",
                    SearchType = SearchType.SingleNode,
                    Selectors = "#left-menu",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent,
                    }
                },
            };

            if (withUnderscoreClassName)
            {
                searchRules.Add(
                new SearchRule
                {
                    FieldName = "Title 2",
                    SearchType = SearchType.SingleNode,
                    Selectors = ".header_split h1",
                    FoundValue = new FoundValue
                    {
                        FoundValueType = FoundValueType.TagTextContent
                    }
                });
            }

            return new ArticleGenerateResult
            {
                ArticleContent = Content,
                SearchRules = new SearchRules { Rules = searchRules.ToArray() },
            };
        }

        private const string Content = @"<!DOCTYPE html>
<html lang='ru'>
<head>
	<meta http-equiv='X-UA-Compatible' content='IE=edge' />
	<meta name = 'viewport' content='width=1230' />
	<meta name = 'format-detection' content='telephone=no' />

	<meta property = 'og:title' content='Выдано разрешение на строительство первого этапа ЖК «Морошкино» в Осиновой Роще' />
	<meta property = 'og:type' content='website' />
	<meta property = 'og:url' content='http://vitrinanovostroek.ru/spb/novosti/novosti-kompaniy/vydano-razreshenie-na-stroitelstvo-pervogo-etapa-zhk-moroshkino-v-osinovoy-roshche/' />
	<meta property = 'og:image' content='http://vitrinanovostroek.ru/images/logo.png' />
	<meta property = 'og:description' content='Выдано разрешение на строительство первого этапа ЖК «Морошкино» в Осиновой Роще' />
<script type = 'text/javascript' src='//eyenewton.ru/scripts/callback.min.js' charset='UTF-8' async='async'></script>
<script type = 'text/javascript'>/*<![CDATA[*/var newton_callback_id='5910ebd0dc1c6423c72e7838df7e00c6';/*]]>*/</script>

<!--[if lte IE 8]>
	<script type = 'text/javascript' src='/js/selectivizr.js'></script>
<![endif]-->
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<meta name = 'keywords' content='Выдано разрешение на строительство первого этапа ЖК «Морошкино» в Осиновой Роще' />
<meta name = 'description' content='Выдано разрешение на строительство первого этапа ЖК «Морошкино» в Осиновой Роще' />
<link href = '/bitrix/cache/css/s1/main/kernel_main/kernel_main.css?144582037639563' type='text/css'  rel='stylesheet' />
<link href = '/bitrix/js/main/core/css/core_viewer.min.css?144251765417550' type='text/css'  rel='stylesheet' />
<link href = '/bitrix/cache/css/s1/main/page_b9addcbc074a0a9bde120987319398d6/page_b9addcbc074a0a9bde120987319398d6.css?144582046619120' type='text/css'  rel='stylesheet' />
<link href = '/bitrix/cache/css/s1/main/template_a17d84307c316250effc48be80427f92/template_a17d84307c316250effc48be80427f92.css?144620641590930' type='text/css'  data-template-style='true'  rel='stylesheet' />
<script type = 'text/javascript'>if(!window.BX)window.BX={message:function(mess) { if (typeof mess== 'object') for(var i in mess) BX.message[i] = mess[i]; return true; }
    };</script>
<script type = 'text/javascript'> (window.BX || top.BX).message({'JS_CORE_LOADING':'Загрузка...', 'JS_CORE_NO_DATA':'- Нет данных -', 'JS_CORE_WINDOW_CLOSE':'Закрыть', 'JS_CORE_WINDOW_EXPAND':'Развернуть', 'JS_CORE_WINDOW_NARROW':'Свернуть в окно', 'JS_CORE_WINDOW_SAVE':'Сохранить', 'JS_CORE_WINDOW_CANCEL':'Отменить', 'JS_CORE_H':'ч', 'JS_CORE_M':'м', 'JS_CORE_S':'с', 'JSADM_AI_HIDE_EXTRA':'Скрыть лишние', 'JSADM_AI_ALL_NOTIF':'Показать все', 'JSADM_AUTH_REQ':'Требуется авторизация!', 'JS_CORE_WINDOW_AUTH':'Войти', 'JS_CORE_IMAGE_FULL':'Полный размер'});</script>
<script type = 'text/javascript'> (window.BX || top.BX).message({'JS_CORE_VIEWER_DOWNLOAD':'Скачать', 'JS_CORE_VIEWER_EDIT':'Редактировать', 'JS_CORE_VIEWER_DESCR_AUTHOR':'Автор', 'JS_CORE_VIEWER_DESCR_LAST_MODIFY':'Последние изменения', 'JS_CORE_VIEWER_TOO_BIG_FOR_VIEW':'Файл слишком большой для просмотра', 'JS_CORE_VIEWER_OPEN_WITH_GVIEWER':'Открыть файл в Google Viewer', 'JS_CORE_VIEWER_IFRAME_DESCR_ERROR':'Просмотр файла невозможен из-за просроченной авторизации в Google. Откройте файл в Google Viewer и авторизуйтесь.', 'JS_CORE_VIEWER_IFRAME_PROCESS_SAVE_DOC':'Сохранение документа', 'JS_CORE_VIEWER_IFRAME_UPLOAD_DOC_TO_GOOGLE':'Загрузка документа', 'JS_CORE_VIEWER_IFRAME_CONVERT_ACCEPT':'Конвертировать', 'JS_CORE_VIEWER_IFRAME_CONVERT_DECLINE':'Отменить', 'JS_CORE_VIEWER_IFRAME_CONVERT_TO_NEW_FORMAT':'Документ будет сконвертирован в docx, xls, pptx, так как имеет старый формат.', 'JS_CORE_VIEWER_IFRAME_DESCR_SAVE_DOC':'Сохранить документ?', 'JS_CORE_VIEWER_IFRAME_SAVE_DOC':'Сохранить', 'JS_CORE_VIEWER_IFRAME_DISCARD_DOC':'Отменить изменения', 'JS_CORE_VIEWER_IFRAME_CHOICE_SERVICE_EDIT':'Редактировать с помощью', 'JS_CORE_VIEWER_IFRAME_SET_DEFAULT_SERVICE_EDIT':'Использовать для всех файлов', 'JS_CORE_VIEWER_IFRAME_CHOICE_SERVICE_EDIT_ACCEPT':'Применить', 'JS_CORE_VIEWER_IFRAME_CHOICE_SERVICE_EDIT_DECLINE':'Отменить', 'JS_CORE_VIEWER_IFRAME_UPLOAD_NEW_VERSION_IN_COMMENT':'Загрузил новую версию файла', 'JS_CORE_VIEWER_SERVICE_GOOGLE_DRIVE':'Google Docs', 'JS_CORE_VIEWER_SERVICE_SKYDRIVE':'MS Office Online', 'JS_CORE_VIEWER_IFRAME_CANCEL':'Отмена', 'JS_CORE_VIEWER_IFRAME_DESCR_SAVE_DOC_F':'В одном из окон вы редактируете данный документ. Если вы завершили работу над документом, нажмите \'#SAVE_DOC#\', чтобы загрузить измененный файл на портал.', 'JS_CORE_VIEWER_SAVE':'Сохранить', 'JS_CORE_VIEWER_EDIT_IN_SERVICE':'Редактировать в #SERVICE#', 'JS_CORE_VIEWER_NOW_EDITING_IN_SERVICE':'Редактирование в #SERVICE#', 'JS_CORE_VIEWER_SAVE_TO_OWN_FILES':'Сохранить на Битрикс24.Диск', 'JS_CORE_VIEWER_DOWNLOAD_TO_PC':'Скачать на локальный компьютер', 'JS_CORE_VIEWER_GO_TO_FILE':'Перейти к файлу', 'JS_CORE_VIEWER_DESCR_SAVE_FILE_TO_OWN_FILES':'Файл #NAME# успешно сохранен<br>в папку \'Файлы\\Сохраненные\'', 'JS_CORE_VIEWER_DESCR_PROCESS_SAVE_FILE_TO_OWN_FILES':'Файл #NAME# сохраняется<br>на ваш \'Битрикс24.Диск\'', 'JS_CORE_VIEWER_HISTORY_ELEMENT':'История', 'JS_CORE_VIEWER_VIEW_ELEMENT':'Просмотреть', 'JS_CORE_VIEWER_THROUGH_VERSION':'Версия #NUMBER#', 'JS_CORE_VIEWER_THROUGH_LAST_VERSION':'Последняя версия', 'JS_CORE_VIEWER_DISABLE_EDIT_BY_PERM':'Автор не разрешил вам редактировать этот документ', 'JS_CORE_VIEWER_IFRAME_UPLOAD_NEW_VERSION_IN_COMMENT_F':'Загрузила новую версию файла', 'JS_CORE_VIEWER_IFRAME_UPLOAD_NEW_VERSION_IN_COMMENT_M':'Загрузил новую версию файла', 'JS_CORE_VIEWER_IFRAME_CONVERT_TO_NEW_FORMAT_EX':'Документ будет сконвертирован в формат #NEW_FORMAT#, так как текущий формат #OLD_FORMAT# является устаревшим.', 'JS_CORE_VIEWER_CONVERT_TITLE':'Конвертировать в #NEW_FORMAT#?', 'JS_CORE_VIEWER_CREATE_IN_SERVICE':'Создать с помощью #SERVICE#', 'JS_CORE_VIEWER_NOW_CREATING_IN_SERVICE':'Создание документа в #SERVICE#', 'JS_CORE_VIEWER_SAVE_AS':'Сохранить как', 'JS_CORE_VIEWER_CREATE_DESCR_SAVE_DOC_F':'В одном из окон вы создаете новый документ. Если вы завершили работу над документом, нажмите \'#SAVE_AS_DOC#\', чтобы перейти к добавлению документа на портал.', 'JS_CORE_VIEWER_NOW_DOWNLOAD_FROM_SERVICE':'Загрузка документа из #SERVICE#', 'JS_CORE_VIEWER_EDIT_IN_LOCAL_SERVICE':'Редактировать на моём компьютере', 'JS_CORE_VIEWER_EDIT_IN_LOCAL_SERVICE_SHORT':'Редактировать на #SERVICE#', 'JS_CORE_VIEWER_SERVICE_LOCAL':'моём компьютере', 'JS_CORE_VIEWER_DOWNLOAD_B24_DESKTOP':'Скачать', 'JS_CORE_VIEWER_SERVICE_LOCAL_INSTALL_DESKTOP':'Для эффективного редактирования документов на компьютере, установите десктоп приложение и подключите Битрикс24.Диск', 'JS_CORE_VIEWER_SERVICE_B24_DISK':'Битрикс24.Диск'});</script>
<script type = 'text/javascript'> (window.BX || top.BX).message({'LANGUAGE_ID':'ru', 'FORMAT_DATE':'DD.MM.YYYY', 'FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS', 'COOKIE_PREFIX':'BITRIX_SM', 'SERVER_TZ_OFFSET':'10800', 'SITE_ID':'s1', 'USER_ID':'', 'SERVER_TIME':'1447350093', 'USER_TZ_OFFSET':'0', 'USER_TZ_AUTO':'Y', 'bitrix_sessid':'f1eab138d63dde4ac856c0273a553118'});</script>


<script type = 'text/javascript' src='/bitrix/cache/js/s1/main/kernel_main/kernel_main.js?1445853263238644'></script>
<script type = 'text/javascript' src='https://api-maps.yandex.ru/2.1/?lang=ru_RU'></script>
<script type = 'text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js'></script>
<script type = 'text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js'></script>
<script type = 'text/javascript' src='http://www.panoramio.com/wapi/wapi.js?v=1&amp;hl=ru'></script>
<script type = 'text/javascript' src='/bitrix/js/main/core/core_viewer.min.js?144251765479597'></script>
<script type = 'text/javascript'> BX.setJSList(['/bitrix/js/main/core/core.js?144251775069133', '/bitrix/js/main/core/core_ajax.js?144251765020575', '/bitrix/js/main/core/core_popup.js?144251764928778', '/bitrix/js/main/core/core_fx.js?14425176469592', '/bitrix/js/main/json/json2.min.js?14366500393467', '/bitrix/js/main/core/core_ls.js?14425176507365', '/bitrix/js/main/session.js?14425176462511', '/bitrix/js/main/core/core_window.js?144251765074831', '/bitrix/js/main/utils.js?144251764519858', '/local/templates/.default/components/gag/news/news/bitrix/news.detail/.default/script.js?1436650037113', '/local/templates/.default/components/gag/news/news/bitrix/forum.topic.reviews/.default/script.js?143665003726684', '/js/html5.js?14366500372051', '/js/jquery.carouFredSel-6.2.1.js?143665003791090', '/js/jquery.fancybox.js?143665003747760', '/js/jquery.formstyler.min.js?143665003712148', '/js/main.js?143665003721056', '/bitrix/components/bitrix/search.title/script.js?14425176846196']); </script>
<script type = 'text/javascript'> BX.setCSSList(['/bitrix/js/main/core/css/core.css?14425176462854', '/bitrix/js/main/core/css/core_popup.css?144251765234473', '/local/templates/.default/components/gag/news/news/bitrix/forum.topic.reviews/.default/style.css?143665003717472', '/css/styles.css?144620641390134', '/local/templates/.default/components/bitrix/search.title/head/style.css?1436650039440']); </script>


<script type = 'text/javascript' src='/bitrix/cache/js/s1/main/template_b975239c4e565b41ada6ce8acdbe9eee/template_b975239c4e565b41ada6ce8acdbe9eee.js?1445820376181799'></script>
<script type = 'text/javascript' src='/bitrix/cache/js/s1/main/page_30fd1296aac1c8a567529158649fe69b/page_30fd1296aac1c8a567529158649fe69b.js?144582046627645'></script>

	<title>Выдано разрешение на строительство первого этапа ЖК «Морошкино» в Осиновой Роще</title>
	<link rel = 'icon' href='/favicon.ico' type='image/x-icon'>
    <link rel = 'shortcut icon' href='/favicon.ico' type='image/x-icon'>
</head>
<body class=''>
	<div id = 'panel' style='position: fixed;width:100%;'></div>
	<div id = 'container'>
        <div class='wrap top-banner'>
			<a href = 'http://www.urbanawards.ru/' rel='nofollow' target='_blank' data-bx-noindex='Y'><img width = '1230' alt='u-aw-2015_1230x90.gif' src='http://cdn.vitrinanovostroek.ru/medialibrary/aba/aba846050a11d5c9729c28c663e60d4e/u_aw_2015_1230x90.gif' height='90' title='Urban Awards 2015' align='middle'></a>		</div>
		<div class='wrap'>
			<div class='page-wrapper'>
				<header id = 'header'>
                    <div id='logo'>
						<a href = '/spb/'> Витринановостроек.рф </a>                    </div>

                    <h2>
                    Квартиры в<a href='/spb/novostroiki/'>288</a> новостройках от
<a href='/spb/stroitelnye-kompanii/'>127</a> застройщиков
                   </h2>

					<div class='header-bar'>
						<div class='header-city'>
							<form action = '#'>
    <fieldset>
        <select onchange='SetCity()' id='citySelect'>
							<option id = '22074'> Москва </option>
                            <option selected='selected' id='43'>Санкт-Петербург</option>
					</select>
	</fieldset>
</form>						</div>

						<div class='header-viewed'>
							<a href = '/personal/watched/'> Вы смотрели(0)</a>						</div>

						<div class='header-compare'>
							
<a href = '/personal/sravnit/?action=COMPARE'> Сравнить(0) </a>
                        </div>
                        <div class='header-search'>
							<div id = 'title-search' class='bx_search_container'>
	<fieldset>
		<form action = '/search/index.php'>
            <input id='title-search-input' placeholder='Поиск по сайту' type='text' name='q' value='' size='23' maxlength='50' autocomplete='off' class='text' />
			<input name = 's' type='submit' value='&nbsp;' class='btn'/>
		</form>
	</fieldset>
</div>
<script>
	BX.ready(function()
{
    new JCTitleSearch({
            'AJAX_PAGE' : '/spb/novosti/novosti-kompaniy/vydano-razreshenie-na-stroitelstvo-pervogo-etapa-zhk-moroshkino-v-osinovoy-roshche/',
            'CONTAINER_ID': 'title-search',
            'INPUT_ID': 'title-search-input',
            'MIN_QUERY_LEN': 2
        });
});
</script>

													</div>
					</div>

					<nav id = 'menu'>

    <ul id='left-menu'>
		<li class=''>
			<a href = '/spb/kupit-kvartiry/'> Купить квартиру</a>
		</li>
		<li>
			<a href = '/spb/novostroiki/'> Новостройки </a>
        </li>
        <li>
            <a href='/spb/stroitelnye-kompanii/'>Строительные компании</a>
 		</li>
		<li>
			<a href = '/spb/agentstva-nedvijimosti/'> Агентства недвижимости</a>
 		</li>
		<li  class='active'>
			<a href = '/spb/novosti/'> Новости </a>
        </li>
    </ul>
                                        </nav>
                </header>

                <section id='main' class='clearfix'>
																				<div class='breadcrumbs'><a href = '/' title='Главная'>Главная</a><span>&raquo;</span><a href = '/spb/novosti/' title='Новости'>Новости</a><span>&raquo;</span><span id = 'lastElBr'> Новости компаний</span></div>					
<noindex>
<section class='block specialTop'>
	<div class='spt-left'><button class='spt-left-btn' style='border: none'>&nbsp;</button></div>
	<div class='ccc' style='float: left; overflow: hidden;'>
	<ul class='listing'>
			
		<li>
			<figure><a href = 'http://vitrinanovostroek.ru/spb/novostroiki/docklands/'><img width='170' alt='docklands_200x130.jpg' src='http://cdn.vitrinanovostroek.ru/medialibrary/69a/69abde9a1c537bb0bb222623d103b10d/docklands_200x130.jpg' height='111' title='docklands_200x130.jpg'></a></figure>
<h3><a href = 'http://vitrinanovostroek.ru/spb/novostroiki/docklands/'> Апар - ты в стиле «лофт»</a></h3>
 <a href = ''> </a><a href=''> </a><a href = ''> </a>
      <h4><a href='http://vitrinanovostroek.ru/spb/novostroiki/docklands/'>От 3,5 млн руб.</a><br>
 </h4>		</li>
			
		<li>
			<noindex><figure><a href = 'http://vitrinanovostroek.ru/spb/novostroiki/staraya-krepost/kupit-kvartiru/avis/kupit-kvartiru/' target= '_blank'><img width= '171' alt= 'stkr_170x96px_170x110.jpg' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/0eb/0eb63f67e49c2aa116d93225767c6840/stkr_170x96px_170x110.jpg' height= '111' title= 'stkr_170x96px_170x110.jpg'> </a></figure><a href= 'http://vitrinanovostroek.ru/spb/novostroiki/staraya-krepost/kupit-kvartiru/' rel= 'nofollow' target= '_blank'> </a>
         <!--noindex--><a href= 'http://skmurino.ru/'> </a><!--/ noindex--><h3><a href= 'http://vitrinanovostroek.ru/spb/novostroiki/staraya-krepost/kupit-kvartiru/'></a><a href= 'http://vitrinanovostroek.ru/spb/novostroiki/staraya-krepost/kupit-kvartiru/' rel= 'nofollow' target= '_blank'> ЖК «Старая крепость»</a> </h3>
<h4> 
<!--noindex--><a href = 'http://vitrinanovostroek.ru/spb/novostroiki/staraya-krepost/kupit-kvartiru/' rel= 'nofollow' target= '_blank'> Первый взнос 170 000 р</a><!--/noindex--><a rel = 'nofollow' href= 'http://vitrinanovostroek.ru/spb/novostroiki/staraya-krepost/kupit-kvartiru/' data-bx-noindex= 'Y' target= '_blank'>.</a></h4>
          </noindex>     </li>
         
                 <li>
                     <figure>
         <!--noindex--><a href= 'http://vitrinanovostroek.ru/spb/novostroiki/severnaya-dolina/' rel= 'nofollow' target= '_blank'><img width= '170' alt= 'Северная Долина октябрь 2015' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/82c/82c9598e9d20c1085dc9a5211045bbc5/200x130_1_1_.jpg' height= '111' title= 'Северная Долина октябрь 2015'></a><!--/ noindex--> </figure>
         <h3>
         <!--noindex--><a href= 'http://vitrinanovostroek.ru/spb/novostroiki/severnaya-dolina/' rel= 'nofollow' target= '_blank'> Квартиры у м. Парнас</a><!--/noindex--> </h3>
<h4><a href = 'http://vitrinanovostroek.ru/spb/novostroiki/severnaya-dolina/'> Скидки до 30%!</a></h4>		</li>
			
		<li>
			<noindex><figure> <a rel = 'nofollow' target= '_blank' href= 'http://vitrinanovostroek.ru/spb/stroitelnye-kompanii/navis/kupit-kvartiru/' data-bx-noindex= 'Y'><img width= '170' alt= 'Щегловская-ready-170x110.jpg' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/57d/57daa430e352ae583ada27ad06d21f53/image006_1_.jpg' height= '110' title= 'Щегловская-ready-170x110.jpg'></a>
         <!--noindex--><a target= '_blank' href= ''> </a><!--/ noindex--></figure><a href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us' target= '_blank'> </a><a target= '_blank' href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us'> </a>
         <h3><a rel= 'nofollow' target= '_blank' http:='' vitrinanovostroek.ru= '' spb= '' stroitelnye-kompanii= '' navis= '' kupit-kvartiru= '' '='' data-bx-noindex='Y'>«Щегловская Усадьба»</a> </h3>
<!--noindex--><a target = '_blank' href= ''> </a><!--/ noindex--><a href= '' target= '_blank'> </a><a target= '_blank' href= ''> </a>
         <h4><a href= 'http://vitrinanovostroek.ru/spb/stroitelnye-kompanii/navis/kupit-kvartiru/' rel= 'nofollow' target= '_blank' data-bx-noindex= 'Y'> К </a><a href= 'http://vitrinanovostroek.ru/spb/stroitelnye-kompanii/navis/kupit-kvartiru/' rel= 'nofollow' target= '_blank' data-bx-noindex= 'Y'> вартира за 1,44 млн руб.</a></h4>
 <noindex></noindex></noindex>		</li>
			
		<li>
			<noindex><figure><a href = 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/' rel= 'nofollow' target= '_blank' data-bx-noindex= 'Y'><img width= '170' alt= 'Новая Охта' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/335/3352fbf80eac0aaf7d882a60d5b5ee19/200x130.jpg' height= '111' title= 'Новая Охта'></a></figure>
         <h3><a href= 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/' rel= 'nofollow' target= '_blank' data-bx-noindex= 'Y'> Квартиры от 3,3 млн р.</a></h3>
<h4><a href = 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/' rel= 'nofollow' target= '_blank' data-bx-noindex= 'Y'> Сдача в 3 квартале 2016 г.</a></h4>
 </noindex>		</li>
			
		<li>
			<figure><a href = 'http://vitrinanovostroek.ru/spb/novostroiki/yuntolovo/' rel= 'nofollow' target= '_blank' data-bx-noindex= 'Y'><img width= '170' alt= '200x130_1_170x110.jpg' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/1cd/1cdf8ea56a7a0e3ed8c8739de15ac32e/200x130_1_170x110.jpg' height= '110' title= '200x130_1_170x110.jpg'> </a></figure><a href= 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'> </a>
         <h3><a href= 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'></a><a href= 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8' target= '_blank'> </a>
         <!--noindex--><a href= 'http://vitrinanovostroek.ru/spb/novostroiki/yuntolovo/' rel= 'nofollow' target= '_blank'> Приморский р-н.Парк.</a><!--/noindex--></h3>
<h4><!--noindex--><a href = 'http://vitrinanovostroek.ru/spb/novostroiki/yuntolovo/' rel= 'nofollow' data-bx-noindex= 'Y'> С отделкой от 2.1 млн.<!--noindex--></a></h4>		</li>
		</ul>
	</div>
	<div class='spt-right'><button class='spt-right-btn' style='border: none'>&nbsp;</button></div>
</section>
</noindex>
<script src = '/js/jcarousellite.js'></script>
<script>
	$('.ccc').jCarouselLite({
    btnNext: '.spt-right',
		btnPrev: '.spt-left',
		visible:
    6
    });
</script>					<div class='header-split header_split'>
						<h1>Выдано разрешение на строительство первого этапа ЖК «Морошкино» в Осиновой Роще</h1>
							<form action = '/personal/subscribe/subscr_edit.php'>
                        <input type='hidden'
				name='sf_RUB_ID[]' id='sf_RUB_ID_1' 
					value='1' />
				
	<fieldset>
		<input type = 'text' name='sf_EMAIL' placeholder='Введите ваш email' class='text' />
		<input type = 'submit' name='OK' value='Подписаться' class='btn' />
	</fieldset>
	
</form>					</div>
															
					


<nav class='sub-nav'>
	<ul>
	<li>
					<a href = '/spb/novosti/'> Все </a>
            </li>
            <li>
            <a href='/spb/novosti/video/'>Видео</a>
		</li>
				<li>
			<a href = '/spb/novosti/gorod/'> Город </a>
        </li>
                <li>
            <a href='/spb/novosti/interesnoe/'>Интересное</a>
		</li>
				<li>
			<a href = '/spb/novosti/ipoteka/'> Ипотека </a>
        </li>
                <li>
            <a href='/spb/novosti/meropriyatiya/'>Мероприятия</a>
		</li>
				<li>
			<a href = '/spb/novosti/novosti-kompaniy/'> Новости компаний</a>
		</li>
				<li>
			<a href = '/spb/novosti/rynok-nedvizhimosti/'> Рынок недвижимости</a>
		</li>
		</ul>
</nav><div class='news-detail'>
	<article id = 'content'>
    <div class='meta'>
			<span class='date'>26 Марта 2015</span>
		<span class='section'>
		<a href = '/spb/novosti/novosti-kompaniy/'>
            Новости компаний</a>
	</span>
</div>
	<h2>Выдано разрешение на строительство первого этапа ЖК «Морошкино» в Осиновой Роще</h2>
<br/>


	<div class='news-detail-content'>
					<p style = 'text-align: justify;'>
    <b><a href='http://vitrinanovostroek.ru/spb/stroitelnye-kompanii/normann/'>Компания Normann</a> получила разрешение на строительство первого этапа <a href = 'http://vitrinanovostroek.ru/spb/novostroiki/moroshkino/'> ЖК «Морошкино»</a>, сообщила пресс-служба застройщика.</b><br>
 <br>
     В рамках первого этапа планируется возвести 12-этажный многоквартирный дом со встроенными помещениями и надземными закрытыми автостоянками. В здании расположатся 1068 квартир.Объект общей площадью более 41,4 тыс.кв.м расположится в Буграх.<br>
 <br>
     Всего в рамках проекта запланировано строительство комплекса эконом-класса на 3084 квартир, состоящего из пяти 12-этажных зданий. Преобладать будут студии (66%) и 1-комнатные(34%) квартиры.На территории ЖК «Морошкино» также разместятся детский сад на 150 мест, 4 наземных 5-этажных паркинга на 844 машиноместа, спортивные и игровые площадки.
</p>            </div>


		<br/>
		<br/>
	<p><a href = '/spb/novosti/'> Возврат к списку</a></p>
</article>
<noindex>
<aside id = 'sidebar'>
    <div class='specials-sidebar'>
		<h3>Специальные предложения</h3>
		<ul class='side-specials'>
							<li>
					<figure> 
<!--noindex--><a href = 'http://sevdol.ru/kvartiri_s_otdelkoy_u_metro_ot_1-9/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-9' rel='nofollow' target='_blank'><img width = '170' alt='Северная Долина октябрь 2015' src='http://cdn.vitrinanovostroek.ru/medialibrary/82c/82c9598e9d20c1085dc9a5211045bbc5/200x130_1_1_.jpg' height='111' title='Северная Долина октябрь 2015'></a><!--/noindex--> </figure>
<h3> 
<!--noindex--><a href = 'http://sevdol.ru/kvartiri_s_otdelkoy_u_metro_ot_1-9/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-9' rel='nofollow' target='_blank'>ЖК «Северная долина» на Парнасе</a><!--/noindex--> </h3>
<h4><a href = 'http://sevdol.ru/kvartiri_s_otdelkoy_u_metro_ot_1-9/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-9'> Квартиры с отделкой рядом с Шуваловским парком!</a></h4>				</li>
							<li>
					<noindex><figure> <a rel = 'nofollow' target= '_blank' href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us' data-bx-noindex= 'Y'><img width= '170' alt= 'Щегловская-ready-170x110.jpg' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/57d/57daa430e352ae583ada27ad06d21f53/image006_1_.jpg' height= '110' title= 'Щегловская-ready-170x110.jpg'></a>
  <!--noindex--><a target= '_blank' href= ''> </a><!--/ noindex--></figure><a href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us' target= '_blank'> </a><a target= '_blank' href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us'> </a>
  <h3><a rel= 'nofollow' target= '_blank' href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us' data-bx-noindex= 'Y'>«Щегловская Усадьба»</a> </h3>
<!--noindex--><a target = '_blank' href= ''> </a><!--/ noindex--><a href= '' target= '_blank'> </a><a target= '_blank' href= ''> </a>
  <h4><a rel= 'nofollow' target= '_blank' href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us' data-bx-noindex= 'Y'> Однокомнатная квартира за 1,44 млн руб. Сдача – 4 кв. 2016 г.</a> </h4>
 <noindex></noindex></noindex>				</li>
							<li>
					<script language = 'Javascript'>
  //<!--
  var rnd = Math.round(Math.random() * 67052861);
var ref = 'null';
if (document.referrer) ref = encodeURIComponent(document.referrer);
document.write('<iframe src='http://sedu.adhands.ru/view/?'
+ 'sid=2805&apid=23458&sz=170x210&ref=' + ref
+ '&rnd=' + rnd + '' '
+ ' frameborder='0' vspace='0' hspace='0' width='170' height='210' marginwidth='0''
+ ' marginheight='0' scrolling='no'></iframe>');
//-->
</script>
<noscript>
<iframe src = 'http://sedu.adhands.ru/view/?static=on&sid=2805&apid=23458&sz=170x210&rnd=67052861' frameborder='0' vspace='0' hspace='0' width='170' height='210' marginwidth='0' marginheight='0' scrolling='no'></iframe>
</noscript>				</li>
							<li>
					<noindex><figure><a href = 'http://skmurino.ru/' rel='nofollow' target='_blank'><img width = '171' alt='stkr_170x96px_170x110.jpg' src='http://cdn.vitrinanovostroek.ru/medialibrary/0eb/0eb63f67e49c2aa116d93225767c6840/stkr_170x96px_170x110.jpg' height='111' title='stkr_170x96px_170x110.jpg'> </figure> 
<!--noindex--><a href = 'http://skmurino.ru/'> </a><!--/ noindex--><h3><a href='http://skmurino.ru/'></a><a href = 'http://skmurino.ru/' rel='nofollow' target='_blank'>ЖК «Старая крепость»</a> </h3>
<h4> 
<!--noindex--><a href = 'http://skmurino.ru/' rel='nofollow' target='_blank'>Первый взнос 170 000 руб.Выгодная рассрочка.</a><!--/noindex--> </h4>
 </noindex>				</li>
							<li>
					<figure><a href = 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'><img width= '170' alt= '200x130_1_170x110.jpg' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/1cd/1cdf8ea56a7a0e3ed8c8739de15ac32e/200x130_1_170x110.jpg' height= '110' title= '200x130_1_170x110.jpg'> </a></figure><a href= 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'> </a>
  <h3><a href= 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'></a><a href= 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8' target= '_blank'> </a>
  <!--noindex--><a href= 'http://yuntolovo-spb.ru?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8' rel= 'nofollow' target= '_blank'> Приморский р-н.Парк.</a><!--/noindex--></h3>
<h4><!--noindex--><a href = 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'> С отделкой от 2.1 млн.Дом сдан! Ключи за 15 дней!<!--noindex--></a></h4>				</li>
							<li>
					<noindex><figure><a href = 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/' rel= 'nofollow' target= '_blank' data-bx-noindex= 'Y'><img width= '170' alt= 'Новая Охта' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/335/3352fbf80eac0aaf7d882a60d5b5ee19/200x130.jpg' height= '111' title= 'Новая Охта'></a></figure>
  <h3><a href= 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/'> Квартиры от 3,3 млн р.</a></h3>
<h4><a href = 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/'> Рядом Нева.Готовность - 70%. Cдаем в 3 кв. 2016 г.</a></h4>
 </noindex>				</li>
					</ul>
	</div>
</aside>
</noindex>
	<div class='news'>
			
<ul>
	<li><h3>Новости по теме</h3></li>
		<li id = 'bx_600340932_34437'>
        <span class='date'>10.11.2015</span>
		<span class='section'>
			<a href = '/spb/novosti/novosti-kompaniy/'>
                Новости компаний</a>
		</span>
		<h4><a href = '/spb/novosti/novosti-kompaniy/noch-skidok-ot-lsr/'> Ночь скидок от ЛСР</a></h4>
	</li>
		<li id = 'bx_600340932_34406'>
        <span class='date'>09.11.2015</span>
		<span class='section'>
			<a href = '/spb/novosti/novosti-kompaniy/'>
                Новости компаний</a>
		</span>
		<h4><a href = '/spb/novosti/novosti-kompaniy/obektov-mesyatsa-u-lsr-stalo-bolshe/'>«Объектов месяца» у ЛСР стало больше</a></h4>
	</li>
		<li id = 'bx_600340932_34392'>
           <span class='date'>06.11.2015</span>
		<span class='section'>
			<a href = '/spb/novosti/novosti-kompaniy/'>
                Новости компаний</a>
		</span>
		<h4><a href = '/spb/novosti/novosti-kompaniy/vtb-ne-budet-pokupat-problemnye-dolgi-stroykholdinga-su-155/'> ВТБ не будет покупать проблемные долги стройхолдинга «СУ-155»</a></h4>
	</li>
</ul>

				
		
<ul>
	<li><h3>Последние новости</h3></li>
		<li id = 'bx_3133220990_34535'>
        <span class='date'>12.11.2015</span>
		<span class='section'>
			<a href = '/spb/novosti/gorod/'>
                Город </a>
        </span>
        <h4><a href='/spb/novosti/gorod/gazprom-vkladyvaetsya-v-blagoustroystvo-goroda/'>Газпром вкладывается в благоустройство города</a></h4>
	</li>
		<li id = 'bx_3133220990_34534'>
        <span class='date'>12.11.2015</span>
		<span class='section'>
			<a href = '/spb/novosti/rynok-nedvizhimosti/'>
                Рынок недвижимости</a>
		</span>
		<h4><a href = '/spb/novosti/rynok-nedvizhimosti/predlozheniya-kvartir-ekonom-klassa-v-gorode-snizhaetsya-a-v-oblasti-rastet/'> Предложения квартир эконом-класса в городе снижается, а в области растет</a></h4>
	</li>
		<li id = 'bx_3133220990_34533'>
        <span class='date'>12.11.2015</span>
		<span class='section'>
			<a href = '/spb/novosti/interesnoe/'>
                Интересное </a>
        </span>
        <h4><a href='/spb/novosti/interesnoe/rekonstruiruyut-shkolu-v-volosovskom-rayone/'>Реконструируют школу в Волосовском районе</a></h4>
	</li>
</ul>
		</div>
</div>

<section class='block recent-comments'>

<script type = 'text/javascript'>
var smallEngLettersReg = new Array(/e'/g, /ch/g, /sh/g, /yo/g, /jo/g, /zh/g, /yu/g, /ju/g, /ya/g, /ja/g, /a/g, /b/g, /v/g, /g/g, /d/g, /e/g, /z/g, /i/g, /j/g, /k/g, /l/g, /m/g, /n/g, /o/g, /p/g, /r/g, /s/g, /t/g, /u/g, /f/g, /h/g, /c/g, /w/g, /~/g, /y/g, /'/g);
var smallRusLetters = new Array('э', 'ч', 'ш', 'ё', 'ё', 'ж', 'ю', 'ю', 'я', 'я', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'щ', 'ъ', 'ы', 'ь');

var capitEngLettersReg = new Array(
    / Ch / g, / Sh / g,
    / Yo / g, / Zh / g,
    / Yu / g, / Ya / g,
    / E'/g, /CH/g, /SH/g, /YO/g, /JO/g, /ZH/g, /YU/g, /JU/g, /YA/g, /JA/g, /A/g, /B/g, /V/g, /G/g, /D/g, /E/g, /Z/g, /I/g, /J/g, /K/g, /L/g, /M/g, /N/g, /O/g, /P/g, /R/g, /S/g, /T/g, /U/g, /F/g, /H/g, /C/g, /W/g, /Y/g);
var capitRusLetters = new Array(
    'Ч', 'Ш',
    'Ё', 'Ж',
    'Ю', 'Я',
    'Э', 'Ч', 'Ш', 'Ё', 'Ё', 'Ж', 'Ю', 'Ю', '\Я', '\Я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Щ', 'Ы');

var smallRusLettersReg = new Array(/ э / g, / ч / g, / ш / g, / ё / g, / ё / g,/ ж / g, / ю / g, / ю / g, / я / g, / я / g, / а / g, / б / g, / в / g, / г / g, / д / g, / е / g, / з / g, / и / g, / й / g, / к / g, / л / g, / м / g, / н / g, / о / g, / п / g, / р / g, / с / g, / т / g, / у / g, / ф / g, / х / g, / ц / g, / щ / g, / ъ / g, / ы / g, / ь / g);
var smallEngLetters = new Array('e', 'ch', 'sh', 'yo', 'jo', 'zh', 'yu', 'ju', 'ya', 'ja', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'w', '~', 'y', ''');

var capitRusLettersReg = new Array(
    / Ч(?=[^ А - Я]) / g, / Ш(?=[^ А - Я]) / g,
    / Ё(?=[^ А - Я]) / g, / Ж(?=[^ А - Я]) / g,
    / Ю(?=[^ А - Я]) / g, / Я(?=[^ А - Я]) / g,
    / Э / g, / Ч / g, / Ш / g, / Ё / g, / Ё / g, / Ж / g, / Ю / g, / Ю / g, / Я / g, / Я / g, / А / g, / Б / g, / В / g, / Г / g, / Д / g, / Е / g, / З / g, / И / g, / Й / g, / К / g, / Л / g, / М / g, / Н / g, / О / g, / П / g, / Р / g, / С / g, / Т / g, / У / g, / Ф / g, / Х / g, / Ц / g, / Щ / g, / Ъ / g, / Ы / g, / Ь / g);
var capitEngLetters = new Array(
    'Ch', 'Sh',
    'Yo', 'Zh',
    'Yu', 'Ya',
    'E', 'CH', 'SH', 'YO', 'JO', 'ZH', 'YU', 'JU', 'YA', 'JA', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'W', '~', 'Y', ''');
</script></section>


<noindex>
<section class='block specialMiddle'>
	<h2>Скидки на квартиры в новостройках</h2>
	<ul class='listing'>
			
		<li>
			<figure><a href = 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'><img width='170' alt='200x130_1_170x110.jpg' src='http://cdn.vitrinanovostroek.ru/medialibrary/1cd/1cdf8ea56a7a0e3ed8c8739de15ac32e/200x130_1_170x110.jpg' height='110' title='200x130_1_170x110.jpg'> </a></figure><a href = 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'> </a>
    <h3><a href='http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'></a><a href = 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8' target='_blank'> </a> 
<!--noindex--><a href = 'http://yuntolovo-spb.ru?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8' rel='nofollow' target='_blank'>Приморский р-н.Парк.</a><!--/noindex--></h3>
<h4><!--noindex--><a href = 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'> С отделкой от 2.1 млн.Дом сдан! Ключи за 15 дней!<!--noindex--></a></h4>		</li>
			
		<li>
			<figure> 
<!--noindex--><a href = 'http://sevdol.ru/kvartiri_s_otdelkoy_u_metro_ot_1-9/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-9' rel='nofollow' target='_blank'><img width = '170' alt='Северная Долина октябрь 2015' src='http://cdn.vitrinanovostroek.ru/medialibrary/82c/82c9598e9d20c1085dc9a5211045bbc5/200x130_1_1_.jpg' height='111' title='Северная Долина октябрь 2015'></a><!--/noindex--> </figure>
<h3> 
<!--noindex--><a href = 'http://sevdol.ru/kvartiri_s_otdelkoy_u_metro_ot_1-9/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-9' rel='nofollow' target='_blank'>Квартиры у м.Парнас</a><!--/noindex--> </h3>
<h4><a href = 'http://sevdol.ru/kvartiri_s_otdelkoy_u_metro_ot_1-9/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-9'> Отделка.Большие кухни.Шуваловский парк.Скидки до 30%!</a></h4>		</li>
			
		<li>
			<script language = 'Javascript'>
   //<!--
   var rnd = Math.round(Math.random() * 80532420);
var ref = 'null';
if (document.referrer) ref = encodeURIComponent(document.referrer);
document.write('<iframe src='http://sedu.adhands.ru/view/?'
+ 'sid=2805&apid=23446&sz=170x210&ref=' + ref
+ '&rnd=' + rnd + '' '
+ ' frameborder='0' vspace='0' hspace='0' width='170' height='210' marginwidth='0''
+ ' marginheight='0' scrolling='no'></iframe>');
//-->
</script>
<noscript>
<iframe src = 'http://sedu.adhands.ru/view/?static=on&sid=2805&apid=23446&sz=170x210&rnd=80532420' frameborder='0' vspace='0' hspace='0' width='170' height='210' marginwidth='0' marginheight='0' scrolling='no'></iframe>
</noscript>		</li>
			
		<li>
			<noindex><figure><a href = 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/' rel='nofollow' target='_blank' data-bx-noindex='Y'><img width = '170' alt='Новая Охта' src='http://cdn.vitrinanovostroek.ru/medialibrary/335/3352fbf80eac0aaf7d882a60d5b5ee19/200x130.jpg' height='111' title='Новая Охта'></a></figure>
<h3><a href = 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/'> Квартиры от 3,3 млн р.</a></h3>
<h4><a href = 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/'> Рядом Нева.Готовность - 70%. Cдаем в 3 кв. 2016 г.</a></h4>
 </noindex>		</li>
		</ul>
</section>
</noindex>
						<section class='introSeo'>
							<div class='intro-text'>
								<h2>Выдано разрешение на строительство первого этапа ЖК «Морошкино» в Осиновой Роще</h2>
<p>Выдано разрешение на строительство первого этапа ЖК «Морошкино» в Осиновой Роще</h2>						
							</div>
							<div class='intro-pic'>
								<noindex><a href = 'http://vitrinanovostroek.ru/spb/novostroiki/tri-kita/' target='_blank'><img width = '300' alt='Три Кита - 300*250' src='/upload/medialibrary/667/6671b5863d181f864c68a22198e4b3dc.jpg' height='250' title='Три Кита - 300*250'></a></noindex>							</div>
						</section>
						
						<div class='rasp'></div>
						
<noindex>
<section class='block special'>
	<h2>Специальные предложения</h2>
	<ul class='listing'>
			
		<li>
			<figure> 
<!--noindex--><a href = 'http://sevdol.ru/kvartiri_s_otdelkoy_u_metro_ot_1-9/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-9' rel='nofollow' target='_blank'><img width = '170' alt='Северная Долина октябрь 2015' src='http://cdn.vitrinanovostroek.ru/medialibrary/82c/82c9598e9d20c1085dc9a5211045bbc5/200x130_1_1_.jpg' height='111' title='Северная Долина октябрь 2015'></a><!--/noindex--> </figure>
<h3> 
<!--noindex--><a href = 'http://sevdol.ru/kvartiri_s_otdelkoy_u_metro_ot_1-9/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-9' rel='nofollow' target='_blank'>ЖК «Северная долина» на Парнасе</a><!--/noindex--> </h3>
<h4><a href = 'http://sevdol.ru/kvartiri_s_otdelkoy_u_metro_ot_1-9/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-9'> Квартиры с отделкой рядом с Шуваловским парком!</a></h4>		</li>
			
		<li>
			<noindex><figure> <a rel = 'nofollow' target= '_blank' href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us' data-bx-noindex= 'Y'><img width= '170' alt= 'Щегловская-ready-170x110.jpg' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/57d/57daa430e352ae583ada27ad06d21f53/image006_1_.jpg' height= '110' title= 'Щегловская-ready-170x110.jpg'></a>
  <!--noindex--><a target= '_blank' href= ''> </a><!--/ noindex--></figure><a href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us' target= '_blank'> </a><a target= '_blank' href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us'> </a>
  <h3><a rel= 'nofollow' target= '_blank' href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us' data-bx-noindex= 'Y'>«Щегловская Усадьба»</a> </h3>
<!--noindex--><a target = '_blank' href= ''> </a><!--/ noindex--><a href= '' target= '_blank'> </a><a target= '_blank' href= ''> </a>
  <h4><a rel= 'nofollow' target= '_blank' href= 'http://an-metry.ru/novostroiki/shcheglovskaya-usadba/?utm_source=vitrinanovostroek.ru&utm_medium=statica&utm_campaign=nov_15&utm_term=spb&utm_content=tgb_sch_us' data-bx-noindex= 'Y'> Однокомнатная квартира за 1,44 млн руб. Сдача – 4 кв. 2016 г.</a> </h4>
 <noindex></noindex></noindex>		</li>
			
		<li>
			<script language = 'Javascript'>
  //<!--
  var rnd = Math.round(Math.random() * 67052861);
var ref = 'null';
if (document.referrer) ref = encodeURIComponent(document.referrer);
document.write('<iframe src='http://sedu.adhands.ru/view/?'
+ 'sid=2805&apid=23458&sz=170x210&ref=' + ref
+ '&rnd=' + rnd + '' '
+ ' frameborder='0' vspace='0' hspace='0' width='170' height='210' marginwidth='0''
+ ' marginheight='0' scrolling='no'></iframe>');
//-->
</script>
<noscript>
<iframe src = 'http://sedu.adhands.ru/view/?static=on&sid=2805&apid=23458&sz=170x210&rnd=67052861' frameborder='0' vspace='0' hspace='0' width='170' height='210' marginwidth='0' marginheight='0' scrolling='no'></iframe>
</noscript>		</li>
			
		<li>
			<noindex><figure><a href = 'http://skmurino.ru/' rel='nofollow' target='_blank'><img width = '171' alt='stkr_170x96px_170x110.jpg' src='http://cdn.vitrinanovostroek.ru/medialibrary/0eb/0eb63f67e49c2aa116d93225767c6840/stkr_170x96px_170x110.jpg' height='111' title='stkr_170x96px_170x110.jpg'> </figure> 
<!--noindex--><a href = 'http://skmurino.ru/'> </a><!--/ noindex--><h3><a href='http://skmurino.ru/'></a><a href = 'http://skmurino.ru/' rel='nofollow' target='_blank'>ЖК «Старая крепость»</a> </h3>
<h4> 
<!--noindex--><a href = 'http://skmurino.ru/' rel='nofollow' target='_blank'>Первый взнос 170 000 руб.Выгодная рассрочка.</a><!--/noindex--> </h4>
 </noindex>		</li>
			
		<li>
			<figure><a href = 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'><img width= '170' alt= '200x130_1_170x110.jpg' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/1cd/1cdf8ea56a7a0e3ed8c8739de15ac32e/200x130_1_170x110.jpg' height= '110' title= '200x130_1_170x110.jpg'> </a></figure><a href= 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'> </a>
  <h3><a href= 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'></a><a href= 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8' target= '_blank'> </a>
  <!--noindex--><a href= 'http://yuntolovo-spb.ru?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8' rel= 'nofollow' target= '_blank'> Приморский р-н.Парк.</a><!--/noindex--></h3>
<h4><!--noindex--><a href = 'http://yuntolovo-spb.ru/?utm_source=vitrinanovostroek.ru&utm_medium=tgb&utm_campaign=1-8'> С отделкой от 2.1 млн.Дом сдан! Ключи за 15 дней!<!--noindex--></a></h4>		</li>
			
		<li>
			<noindex><figure><a href = 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/' rel= 'nofollow' target= '_blank' data-bx-noindex= 'Y'><img width= '170' alt= 'Новая Охта' src= 'http://cdn.vitrinanovostroek.ru/medialibrary/335/3352fbf80eac0aaf7d882a60d5b5ee19/200x130.jpg' height= '111' title= 'Новая Охта'></a></figure>
  <h3><a href= 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/'> Квартиры от 3,3 млн р.</a></h3>
<h4><a href = 'http://newdom.spb.ru/prodazha-kvartir-v-zhk-na-oxte/'> Рядом Нева.Готовность - 70%. Cдаем в 3 кв. 2016 г.</a></h4>
 </noindex>		</li>
		</ul>
</section>
</noindex>						
					<div class='clearfix'></div>
				</section>
			</div>
		</div>
	</div>
	<footer id = 'footer'>
        <div class='wrap'>
			<nav class='bottom-menu'>
				
	<ul id = 'left-menu'>
        <li  class=''>
			<a href = '/spb/kupit-kvartiry/'> Купить квартиру</a>
		</li>
		<li>
			<a href = '/spb/novostroiki/'> Новостройки </a>
        </li>
        <li>
            <a href='/spb/stroitelnye-kompanii/'>Строительные компании</a>
 		</li>
		<li>
			<a href = '/spb/agentstva-nedvijimosti/'> Агентства недвижимости</a>
 		</li>
		<li  class='active'>
			<a href = '/spb/novosti/'> Новости </a>
        </li>
    </ul>
                                </nav>

            <div class='footer-copyright'>
				Витринановостроек.рф &copy; 2015,<br />
Все, что нужно знать о новостройках<br /><br />
				<!--LiveInternet counter--><script type = 'text/javascript'><!--
document.write('<a href='//www.liveinternet.ru/click' ' +
'target=_blank><img src='//counter.yadro.ru/hit?t50.1;r' +
escape(document.referrer) + ((typeof(screen) == 'undefined') ? '' :
';s' + screen.width + '*' + screen.height + '*' + (screen.colorDepth ?
screen.colorDepth : screen.pixelDepth)) + ';u' + escape(document.URL) +
';' + Math.random() +
'' alt='' title='LiveInternet' ' +
'border='0' width='31' height='31'><\/a>')
//--></script><!--/LiveInternet-->
<!--Yandex.Metrika counter -->
<script type = 'text/javascript'>
(function(d, w, c) {
    (w[c] = w[c] || []).push(function()
{
    try
    {
        w.yaCounter26758041 = new Ya.Metrika({id:26758041,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
    }
    catch (e) { }
});

    var n = d.getElementsByTagName('script')[0],
        s = d.createElement('script'),
        f = function() { n.parentNode.insertBefore(s, n); };
    s.type = 'text/javascript';
    s.async = true;
    s.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//mc.yandex.ru/metrika/watch.js';

    if (w.opera == '[object Opera]') {
        d.addEventListener('DOMContentLoaded', f, false);
    } else { f(); }
})(document, window, 'yandex_metrika_callbacks');
</script>
<noscript><div><img src = '//mc.yandex.ru/watch/26758041' style='position:absolute; left:-9999px;' alt='' /></div></noscript>
<!-- /Yandex.Metrika counter -->
			</div>

			<div class='footer-menu'>
				<ul>
		<li>
		<a href = '/about/'> О проекте</a>
	</li>
		<li>
		<a href = '/contacts/'> Контакты </a>
    </li>
        <li>
        <a href='/personal/watched/'>Вы смотрели</a>
 	</li>
	</ul><ul>	<li>
		<a href = '/advertise/'> Реклама </a>
     </li>
         <li>
         <a href= '/partners/'> Партнерам </a>
     </li>
         <li>
         <a href= '/advertise/add.php'> Добавить агентство</a>
 	</li>
	</ul><ul>	<li>
		<a href = '/error/'> Нашли ошибку?</a>
	</li>
		<li>
		<a href = '/sitemap.php'> Карта сайта</a>
 	</li>
		<li>
		<a href = '/personal/subscribe/subscr_edit.php'> Подписаться на новости</a>
	</li>
</ul>				
			</div>

			<ul class='social'>
				<li class='fb'>
										</li>
				<li class='tw'>
									</li>
				<li class='vk'>
									</li>
				<li class='rss'>
					<a href = '/spb/novosti/rss/'></a>                </li>
            </ul>
        </div>
    </footer>
    <div class='jqmWindow' id='popup-map'>
		<div class='jqmContent'>
			<div id = 'map-popup-window'></div>
        </div>
        <a href='javascript:void(0);' class='jqmClose' title='Закрыть окно'></a>
	</div>
		<div id = 'bx-composite-banner'></div>
</body>
</html> ";
    }
}
