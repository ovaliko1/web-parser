﻿using System;

namespace WebParser.Tests.Helpers
{
    public static class RandomHelper
    {
        private static readonly Random Random = new Random();

        public static int Id(int maxValue)
        {
            return Random.Next(1, maxValue);
        }
    }
}
